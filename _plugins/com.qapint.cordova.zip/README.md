Cordova Zip/Unzip plugin for Android and iOS
=============================================================

Installation
---------------------

    cordova plugin add https://git.qapint.com/phonegap-plugins/cordova-zip-plugin.git

Usage
---------------------

    zip.unzip(<source zip>, <destination dir>, [<successCallback>, [<errorCallback>, [<progressCallback>]]]);
    zip.zip(<content folder>, <resulted zip>, [<successCallback>, [<errorCallback>, [<progressCallback>]]]);

Install from local location
-------------------------------------------

    git clone git@git.qapint.com:phonegap-plugins/cordova-zip-plugin.git com.qapint.cordova.zip
    cordova plugin add com.qapint.cordova.zip --searchpath PATH_WHERE_CLONED_FOLDER_LOCATED --noregistry


Both source and destination arguments can be URLs obtained from the HTML File
interface or absolute paths to files on the device.

The progressCallback argument will be executed whenever a new ZipEntry
has been extracted. E.g.:

    var progressCallback = function(progressEvent) {
        $( "#progressbar" ).progressbar("value", Math.round((progressEvent.loaded / progressEvent.total) * 100));
    };

The values `loaded` and `total` are the number of files processed and total. 

