package com.qapint.cordova.zip;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import android.net.Uri;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaResourceApi;
import org.apache.cordova.CordovaResourceApi.OpenForReadResult;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

public class Zip extends CordovaPlugin {

    private static final String LOG_TAG = "Zip";
    private static final int BUFFER_SIZE = 2048;

    @Override
    public boolean execute(String action, CordovaArgs args, final CallbackContext callbackContext) throws JSONException {
        if ("unzip".equals(action)) {
            unzip(args, callbackContext);
            return true;
        } else if ("zip".equals(action)) {
            zip(args, callbackContext);
            return true;
        }
        return false;
    }

    private void unzip(final CordovaArgs args, final CallbackContext callbackContext) {
        this.cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                unzipSync(args, callbackContext);
            }
        });
    }

    private void zip(final CordovaArgs args, final CallbackContext callbackContext) {
        this.cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                zipSync(args, callbackContext);
            }
        });
    }

    // Can't use DataInputStream because it has the wrong endian-ness.
    private static int readInt(InputStream is) throws IOException {
        int a = is.read();
        int b = is.read();
        int c = is.read();
        int d = is.read();
        return a | b << 8 | c << 16 | d << 24;
    }

    private void zipSync(CordovaArgs args, CallbackContext callbackContext) {
        BufferedInputStream origin = null;
        ZipOutputStream outStream = null;

        try {
            String inputDirectory = args.getString(0);
            String zipFileName = args.getString(1);

            Uri inputUri = getUriForArg(inputDirectory);
            Uri zipUri = getUriForArg(zipFileName);

            CordovaResourceApi resourceApi = webView.getResourceApi();
            File inputDir = resourceApi.mapUriToFile(inputUri);
            File destFile = resourceApi.mapUriToFile(zipUri);
            List<File> filesList = getListFiles(inputDir);

            ProgressEvent progress = new ProgressEvent();
            progress.setTotal(filesList.size());
            progress.setLoaded(0);
            updateProgress(callbackContext, progress);

            outStream = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(destFile)));
            byte data[] = new byte[BUFFER_SIZE];

            for (File nextFile : filesList) {
                origin = new BufferedInputStream(new FileInputStream(nextFile), BUFFER_SIZE);
                ZipEntry zipEntry = new ZipEntry(nextFile.getAbsolutePath().replace(inputDir.getAbsolutePath() + "/", ""));
                outStream.putNextEntry(zipEntry);
                int count;
                while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1) {
                    outStream.write(data, 0, count);
                }
                origin.close();
                progress.addLoaded(1);
                updateProgress(callbackContext, progress);
            }

            outStream.close();
            callbackContext.success();
        } catch (Exception e) {
            callbackContext.error(e.getMessage());
            Log.e(LOG_TAG, e.getMessage());
            e.printStackTrace();
        } finally {
            if (origin != null) {
                try {
                    origin.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
            }
            if (outStream != null) {
                try {
                    outStream.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
            }
        }
    }

    private List<File> getListFiles(File parentDir) {
        ArrayList<File> inFiles = new ArrayList<File>();
        File[] files = parentDir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                inFiles.addAll(getListFiles(file));
            } else {
                inFiles.add(file);
            }
        }
        return inFiles;
    }

    private void unzipSync(CordovaArgs args, CallbackContext callbackContext) {
        InputStream inputStream = null;
        try {
            String zipFileName = args.getString(0);
            String outputDirectory = args.getString(1);

            Uri zipUri = getUriForArg(zipFileName);
            Uri outputUri = getUriForArg(outputDirectory);

            CordovaResourceApi resourceApi = webView.getResourceApi();

            File tempFile = resourceApi.mapUriToFile(zipUri);
            if (tempFile == null || !tempFile.exists()) {
                String errorMessage = "Zip file does not exist";
                callbackContext.error(errorMessage);
                Log.e(LOG_TAG, errorMessage);
                return;
            }

            File outputDir = resourceApi.mapUriToFile(outputUri);
            outputDirectory = outputDir.getAbsolutePath();
            outputDirectory += outputDirectory.endsWith(File.separator) ? "" : File.separator;
            if (!outputDir.exists() && !outputDir.mkdirs()) {
                String errorMessage = "Could not create output directory";
                callbackContext.error(errorMessage);
                Log.e(LOG_TAG, errorMessage);
                return;
            }

            OpenForReadResult zipFile = resourceApi.openForRead(zipUri);
            ZipInputStream zis = getZipInputStream(zipFile);

            byte[] buffer = new byte[32 * 1024];
            boolean anyEntries = false;
            int totalFiles = 0;
            while (zis.getNextEntry() != null) {
                totalFiles++;
            }

            ProgressEvent progress = new ProgressEvent();
            progress.setLoaded(0);
            progress.setTotal(totalFiles);
            updateProgress(callbackContext, progress);

            zipFile = resourceApi.openForRead(zipUri);
            zis = getZipInputStream(zipFile);
            inputStream = zis;
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                anyEntries = true;
                String compressedName = ze.getName();

                if (ze.isDirectory()) {
                    File dir = new File(outputDirectory + compressedName);
                    dir.mkdirs();
                } else {
                    File file = new File(outputDirectory + compressedName);
                    file.getParentFile().mkdirs();
                    if (file.exists() || file.createNewFile()) {
                        Log.w("Zip", "extracting: " + file.getPath());
                        FileOutputStream fout = new FileOutputStream(file);
                        int count;
                        while ((count = zis.read(buffer)) != -1) {
                            fout.write(buffer, 0, count);
                        }
                        fout.close();
                    }

                }
                progress.addLoaded(1);
                updateProgress(callbackContext, progress);
                zis.closeEntry();
            }
            progress.setLoaded(progress.getTotal());
            updateProgress(callbackContext, progress);

            if (anyEntries)
                callbackContext.success();
            else
                callbackContext.error("Bad zip file");
        } catch (Exception e) {
            callbackContext.error(e.getMessage());
            Log.e(LOG_TAG, e.getMessage());
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, e.getMessage());
                }
            }
        }
    }

    private ZipInputStream getZipInputStream(OpenForReadResult zipFile) throws IOException {
        InputStream inputStream = new BufferedInputStream(zipFile.inputStream);
        inputStream.mark(10);
        int magic = readInt(inputStream);

        if (magic != 875721283) { // CRX identifier
            inputStream.reset();
        } else {
            // CRX files contain a header. This header consists of:
            //  * 4 bytes of magic number
            //  * 4 bytes of CRX format version,
            //  * 4 bytes of public key length
            //  * 4 bytes of signature length
            //  * the public key
            //  * the signature
            // and then the ordinary zip data follows. We skip over the header before creating the ZipInputStream.
            readInt(inputStream); // version == 2.
            int pubkeyLength = readInt(inputStream);
            int signatureLength = readInt(inputStream);
            inputStream.skip(pubkeyLength + signatureLength);
        }
        return new ZipInputStream(inputStream);
    }

    private void updateProgress(CallbackContext callbackContext, ProgressEvent progress) throws JSONException {
        PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, progress.toJSONObject());
        pluginResult.setKeepCallback(true);
        callbackContext.sendPluginResult(pluginResult);
    }

    private Uri getUriForArg(String arg) {
        CordovaResourceApi resourceApi = webView.getResourceApi();
        Uri tmpTarget = Uri.parse(arg);
        return resourceApi.remapUri(
                tmpTarget.getScheme() != null ? tmpTarget : Uri.fromFile(new File(arg)));
    }

    private static class ProgressEvent {
        private long loaded;
        private long total;

        public long getLoaded() {
            return loaded;
        }

        public void setLoaded(long loaded) {
            this.loaded = loaded;
        }

        public void addLoaded(long add) {
            this.loaded += add;
        }

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

        public JSONObject toJSONObject() throws JSONException {
            return new JSONObject(
                    "{loaded:" + loaded +
                            ",total:" + total + "}");
        }
    }
}
