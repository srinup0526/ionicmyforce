package com.qapint.cordova.presentationviewer;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.artifex.mupdfviewer.MuPDFActivity;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class PresentationViewer extends CordovaPlugin {
    protected static final String LOG_TAG = "Presentation View";
    private static final String EVENT_DID_LOAD = "DID_LOAD";
    private static final String EVENT_ON_COMPLETE = "COMPLETE";
    private static final String EVENT_ON_FOLDED = "FOLDED";
    private static final String EVENT_ON_UNFOLDED = "UNFOLDED";
    private static final String EVENT_ON_OPEN_CONTACT = "OPEN_CONTACT";
    private CallbackContext callbackContext = null;
    private PresentationView presentationView = null;

    @Override
    public boolean execute(final String action, JSONArray args, final CallbackContext callbackContext){
        if ("openPresentation".equals(action)) {
            this.callbackContext = callbackContext;
            return this.openPresentation(args);
        } else if ("closePresentation".equals(action)) {
            return this.closePresentation();
        } else if ("getKPI".equals(action)) {
            this.callbackContext = callbackContext;
            this.getKPI();
        } else if ("foldPresentation".equals(action)) {
            if (presentationView != null && presentationView.isShowing()) {
                try {
                    if(!presentationView.isSuspendedKPI && args.getBoolean(0)){
                        cordova.getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                presentationView.toggleKPIState();
                            }
                        });
                    }
                    presentationView.dismiss();
                    sendResult(PluginResult.Status.OK, EVENT_ON_FOLDED);
                    return true;
                } catch (Exception e) {
                    sendResult(PluginResult.Status.ERROR, e.getMessage());
                    return false;
                }
            } else {
                sendResult(PluginResult.Status.ERROR, "Presentation wasn't opened");
                return false;
            }
        } else if ("unfoldPresentation".equals(action)) {
            if (presentationView != null && !presentationView.isShowing()) {
                try {
                    final boolean isKpiResume = args.getBoolean(0);
                     cordova.getActivity().runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             if(presentationView.isSuspendedKPI && isKpiResume) {
                                 presentationView.toggleKPIState();
                             }
                             presentationView.show();
                             sendResult(PluginResult.Status.OK, EVENT_ON_UNFOLDED);
                         }
                     });
                    return true;
                } catch (Exception e) {
                    sendResult(PluginResult.Status.ERROR, e.getMessage());
                    return false;
                }
            } else {
                sendResult(PluginResult.Status.ERROR, "Presentation wasn't opened");
                return false;
            }
        }else{
            sendResult(PluginResult.Status.INVALID_ACTION, "");
            return false;
        }
        return true;
    }

    private boolean openPresentation(JSONArray commandParams){
        String result = "";
        try {
            if (presentationView != null && presentationView.isShowing()) {
                sendResult(PluginResult.Status.ERROR, "Presentation viewer is already open");
                return false;
            }
            JSONObject params = commandParams.getJSONObject(0);
            if (params.has("index")) {
                JSONObject translations = params.has("translations") ? params.getJSONObject("translations") : new JSONObject();
                boolean showPersonalDataButton = params.has("showPersonalDataButton") && params.getBoolean("showPersonalDataButton");
                if (params.has("structure")) {
                    result = this.showWebPage(params.getString("index") + "?structure=" + params.getString("structure"), translations, showPersonalDataButton);
                } else {
                    result = this.showWebPage(params.getString("index"), translations, showPersonalDataButton);
                }
            }

            if (result.length() > 0) {
                sendResult(PluginResult.Status.ERROR, result);
                return false;
            } else {
                sendResult(PluginResult.Status.OK, "Show presentation");
                return true;
            }
        } catch (Exception e) {
            sendResult(PluginResult.Status.INVALID_ACTION, result);
            return false;
        }
    }

    private boolean closePresentation(){
        if (presentationView != null) {
            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    presentationView.clearKpi(); // clear KPI from storage
                }
            });

            try {
                presentationView.dismiss();
                sendResult(PluginResult.Status.OK, "Presentation closed");
                return true;
            } catch (Exception e) {
                sendResult(PluginResult.Status.ERROR, e.getMessage());
                return false;
            }
        } else {
            sendResult(PluginResult.Status.ERROR, "Presentation wasn't opened");
            return false;
        }
    }

    private void getKPI(){
        cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                presentationView.getKpi(new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String kpiString) {
                        kpiString = kpiString == null ? "" : unescapeJson(kpiString);
                        callbackContext.success(kpiString);
                    }
                });
            }
        });
    }

    private String unescapeJson(String jsonString){
        return jsonString.replace("\"{", "{").replace("}\"", "}").replace("\\\"", "\"");
    }

    private void sendResult(PluginResult.Status status, String data){
        PluginResult result = new PluginResult(status, data);
        result.setKeepCallback(true);
        callbackContext.sendPluginResult(result);
    }

    private String showWebPage(final String url, final JSONObject translations, final Boolean showPersonalDataButton) {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                presentationView = new PresentationView(webView.getContext());
                presentationView.setEventDelegate(new PresentationView.IEventDelegate() {
                    @Override
                    public void onComplete() {
                        if (callbackContext != null) {
                            sendResult(PluginResult.Status.OK, EVENT_ON_COMPLETE);
                        }
                    }
                    @Override
                    public void onOpenContact() {
                        if (callbackContext != null) {
                            sendResult(PluginResult.Status.OK, EVENT_ON_OPEN_CONTACT);
                        }
                    }
                });
                presentationView.setTranslations(translations);
                presentationView.setContactButtonVisibility(showPersonalDataButton);
                presentationView.setWebClient(new PresentationViewClient(cordova));
                presentationView.viewByUrl(url);
            }
        });
        return "";
    }

    private void openPdf(String path) {
        Uri fileUri = Uri.parse(path);
        Intent intent = new Intent(webView.getContext(), MuPDFActivity.class);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(fileUri);
        webView.getContext().startActivity(intent);
    }

    private Hashtable<String, String> getUrlQuery(URL url) throws UnsupportedEncodingException {
        Hashtable<String, String> queryParams = new Hashtable<String, String>();
        String query = url.getQuery();
        if(query != null && !query.isEmpty()){
            String[] pairs = query.split("&");
            for (String pair : pairs) {
                ArrayList<String> keyValue = new ArrayList<String>(Arrays.asList(pair.split("=")));
                if(keyValue.size() >= 1){
                    if(keyValue.size() == 1){
                        keyValue.add("");
                    }
                    for(int i = 0; i < keyValue.size(); i++){
                        keyValue.set(i, URLDecoder.decode(keyValue.get(i), "UTF-8"));
                    }
                    queryParams.put(keyValue.get(0), keyValue.get(1));
                }
            }
        }
        return queryParams;
    }

    private boolean shouldOpenLinkExternal(String urlString){
        try{
            URL url = new URL(urlString);
            Hashtable<String, String> queryParams = getUrlQuery(url);
            return url.getProtocol().startsWith("mailto:") || url.getProtocol().startsWith("market:") || queryParams.containsKey("openExternal") || queryParams.containsKey("openInSafari");
        }catch(MalformedURLException malformedUrlException){
            Log.e(LOG_TAG, malformedUrlException.getMessage());
            return false;
        }catch(UnsupportedEncodingException unsupportedEncodingException){
            Log.e(LOG_TAG, unsupportedEncodingException.getMessage());
            return false;
        }catch(Exception exception){
            Log.e(LOG_TAG, exception.getMessage());
            return false;
        }
    }

    private class PresentationViewClient extends WebViewClient {
        CordovaInterface ctx;

        private PresentationViewClient(CordovaInterface mContext) {
            ctx = mContext;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (callbackContext != null) {
                sendResult(PluginResult.Status.OK, EVENT_DID_LOAD);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url){
            if(url.startsWith("file") && url.endsWith(".pdf")){
                openPdf(url);
                return true;
            }else if(shouldOpenLinkExternal(url)){
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                cordova.getActivity().startActivity(intent);
                return true;
            }else{
                return super.shouldOverrideUrlLoading(view, url);
            }
        }
    }
}
