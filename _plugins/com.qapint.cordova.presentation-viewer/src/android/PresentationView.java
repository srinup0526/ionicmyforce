package com.qapint.cordova.presentationviewer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.*;
import android.webkit.ValueCallback;
import android.webkit.WebViewClient;
import android.widget.*;
import com.qapint.Mylan.crm_mobile.R;
import org.json.JSONObject;

public class PresentationView extends Dialog implements View.OnClickListener {
    private static final int theme = android.R.style.Theme_Black_NoTitleBar_Fullscreen;
    private Context context = null;
    private PresentationWebView presentationWebView = null;
    private FrameLayout presentationViewLayout = null;
    private RelativeLayout panel = null;
    private LinearLayout suspendOverlay = null;
    private Button btnComplete = null;
    private Button btnSuspend = null;
    private ImageButton btnContact = null;
    private TextView tvSuspendedLabel = null;
    private JSONObject translations;
    public boolean isSuspendedKPI;
    private IEventDelegate eventDelegate;

    public PresentationView(Context context) {
        super(context, theme);
        this.context = context;
        presentationWebView = new PresentationWebView(getContext(), theme);
        initLayout();
        initView();
        isSuspendedKPI = false;
        initGestureDetector();
    }

    private void initLayout() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        presentationViewLayout = (FrameLayout) inflater.inflate(R.layout.presentation_view_layout, null);
        LinearLayout viewWrap = (LinearLayout) presentationViewLayout.findViewById(R.id.viewWrap);
        viewWrap.addView(presentationWebView);
        LinearLayout.LayoutParams linearLayoutLayoutParams = (LinearLayout.LayoutParams) presentationWebView.getLayoutParams();
        linearLayoutLayoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        presentationWebView.setLayoutParams(linearLayoutLayoutParams);
        panel = (RelativeLayout) presentationViewLayout.findViewById(R.id.toggablePanel);
        btnComplete = (Button) presentationViewLayout.findViewById(R.id.btnComplete);
        btnSuspend = (Button) presentationViewLayout.findViewById(R.id.btnSuspend);
        btnContact = (ImageButton) presentationViewLayout.findViewById(R.id.btnContact);
        tvSuspendedLabel = (TextView) presentationViewLayout.findViewById(R.id.tvSuspendedLabel);
        suspendOverlay = (LinearLayout) presentationViewLayout.findViewById(R.id.suspendOverlay);
        btnSuspend.setOnClickListener(this);
        btnContact.setOnClickListener(this);
        btnComplete.setOnClickListener(this);
    }

    public void setEventDelegate(IEventDelegate delegate) {
        eventDelegate = delegate;
    }

    public void setTranslations(JSONObject translations){
        this.translations = translations;
        setCompleteCaption();
        setSuspendButtonText();
        setSuspendedLabel();
    }

    private void setCompleteCaption(){
        String caption = translations.optString("Complete", "Complete");
        btnComplete.setText(caption);
    }

    private void setSuspendedLabel(){
        String caption = translations.optString("SuspendedLabel", "Suspended");
        tvSuspendedLabel.setText(caption);
    }

    public void setWebClient(WebViewClient vClient) {
        presentationWebView.setWebViewClient(vClient);
    }

    private void initView() {
        setContentView(presentationViewLayout);
        setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                return !(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP);
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes(lp);
    }

    private void initGestureDetector() {
        final GestureDetector gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                int panelVisibilityState = panel.getVisibility() == View.VISIBLE ? View.INVISIBLE : View.VISIBLE;
                if(isSuspendedKPI && panelVisibilityState == View.INVISIBLE){
                    return true;
                }
                panel.setVisibility(panelVisibilityState);
                return true;
            }
        });
        presentationWebView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if(action == MotionEvent.ACTION_DOWN){
                    if(panel.getVisibility() == View.VISIBLE && !isSuspendedKPI){
                        panel.setVisibility(View.INVISIBLE);
                    }
                }
                return gestureDetector.onTouchEvent(event);
            }
        });
    }

    public void viewByUrl(String url) {
        presentationWebView.load("javascript: if(!window.executeMethod){window.executeMethod = function(){return ''}}; window.addEventListener('load', function(){window.document.body.style.width = window.screen.width + 'px';window.document.body.style.height = window.screen.height + 'px';});");
        presentationWebView.load(url);
        show();
    }

    public void getKpi(ValueCallback<String> resultCallback) {
        if(isSuspendedKPI){
          resumeKPI();
        }
        presentationWebView.invokeMethodWithName("suspend");
        presentationWebView.executeMethodWithName("getKPI", resultCallback);
    }

    public void toggleKPIState(){
        if(isSuspendedKPI){
            resumeKPI();
            panel.setVisibility(View.INVISIBLE);
            suspendOverlay.setVisibility(View.INVISIBLE);
        }else{
            suspendKPI();
            suspendOverlay.setVisibility(View.VISIBLE);
        }
        isSuspendedKPI = !isSuspendedKPI;
        setSuspendButtonText();
    }

    public void setSuspendButtonText(){
        String caption;
        if(isSuspendedKPI){
            caption = translations.optString("Resume", "Resume");
        }else{
            caption = translations.optString("Pause", "Pause");
        }
        btnSuspend.setText(caption);
    }

    public void suspendKPI() {
        presentationWebView.invokeMethodWithName("suspend");
    }

    public void resumeKPI() {
        presentationWebView.invokeMethodWithName("proceed");
    }

    public void clearKpi() {
        presentationWebView.executeMethodWithName("clearStorage", null);
    }

    public void setContactButtonVisibility(boolean isVisible){
        btnContact.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnSuspend:
                toggleKPIState();
                break;
            case R.id.btnComplete:
                if(eventDelegate != null){
                    eventDelegate.onComplete();
                }
                break;
            case R.id.btnContact:
                if(eventDelegate != null){
                    eventDelegate.onOpenContact();
                }
                break;
        }
    }

    public interface IEventDelegate{
        void onComplete();
        void onOpenContact();
    }
}

