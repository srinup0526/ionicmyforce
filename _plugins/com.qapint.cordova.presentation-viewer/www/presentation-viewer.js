var exec = require('cordova/exec'),
		pluginName = "PresentationViewer";

function PresentationViewer(){}

PresentationViewer.events = {
	didLoad: 'DID_LOAD',
	complete: 'COMPLETE',
  didFolded: 'FOLDED',
  didUnfolded: 'UNFOLDED',
  openContact: 'OPEN_CONTACT'
};

PresentationViewer.prototype.open = function(openParams, success, fail){
  openParams.showPersonalDataButton = openParams.showPersonalDataButton || false;
	exec(success, fail, pluginName, 'openPresentation', [openParams]);
};

PresentationViewer.prototype.close = function(success, fail){
	exec(success, fail, pluginName, 'closePresentation', []);
};

PresentationViewer.prototype.fold = function(shouldSuspendKPI, success, fail){
  shouldSuspendKPI = shouldSuspendKPI == undefined ? true : shouldSuspendKPI;
  exec(success, fail, pluginName, 'foldPresentation', [shouldSuspendKPI]);
};

PresentationViewer.prototype.unfold = function(shouldResumeKPI, success, fail){
  shouldResumeKPI = shouldResumeKPI == undefined ? true : shouldResumeKPI;
  exec(success, fail, pluginName, 'unfoldPresentation', [shouldResumeKPI]);
};

PresentationViewer.prototype.getKPI = function(success, fail){
	exec(success, fail, pluginName, 'getKPI', []);
};

module.exports = PresentationViewer
