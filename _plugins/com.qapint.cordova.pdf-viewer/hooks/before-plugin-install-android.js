console.log("Start com.qapint.cordova.pdf-viewer plugin android before-install script");

var path = require('path');
var shelljs;

try {
    shelljs = require('shelljs');

} catch(e) {
    console.log('The node package shelljs is required to use this script. Run \'npm install shelljs@0.7.0\' before running this script.');
    process.exit(1);
}


var pluginAndroidSrc = path.join('plugins', 'com.qapint.cordova.pdf-viewer', 'src', 'android', 'src');
var configFilePath = path.join('config.xml');
var configXmlString = shelljs.cat(configFilePath).toString();
var appRegexFilter = new RegExp('com+(\\.[a-zA-Z0-9-_]+)+');
var appBundleId = configXmlString.match(appRegexFilter)[0];


shelljs.ls(pluginAndroidSrc + '/*.java').forEach(function(file) {
  shelljs.sed('-i', 'com.qapint.crm_mobile_mylan', appBundleId, file);
});

console.log("Stop com.qapint.cordova.pdf-viewer plugin android before-install script");
