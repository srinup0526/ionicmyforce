#Viseven

## Build project
Run `npm install` to install node dependencies
Run `npm run visevenPrepareConfig` to configure project (change app name, bundle id to be valid with viseven provision profiles)

### IOS
Run `npm run visevenBuildIOS` to add ios platform, setup Xcode project and build ios
Run `npm run ionic prepare ios` to prepare your changes before building app manually from Xcode

Recommended to run ionic cli commands through npm script to avoid conflicting versions of ionic.


### Attention
`build_scripts/viseven-prepare-config-xml.sh` used by Jenkins.
Do not push changes in config.xml by this script as project also develop by another team.
