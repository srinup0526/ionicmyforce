console.log("Start com.qapint.cordova.presentation-viewer plugin android before-install script");

var path = require('path');
var shelljs;

try {
    shelljs = require('shelljs');

} catch(e) {
    console.log('The node package shelljs is required to use this script. Run \'npm install shelljs@0.7.0\' before running this script.');
    process.exit(1);
}


var pluginAndroidSrc = path.join('plugins', 'com.qapint.cordova.presentation-viewer', 'src', 'android');
var configFilePath = path.join('config.xml');
var configXmlString = shelljs.cat(configFilePath).toString();
var appRegexFilter = new RegExp('(?<=id=").*?(?=")');
var appBundleId = configXmlString.match(appRegexFilter)[0];


shelljs.ls(pluginAndroidSrc + '/*.java').forEach(function(file) {
  shelljs.sed('-i', 'com.qapint.Mylan.crm_mobile', appBundleId, file);
});

console.log("Stop com.qapint.cordova.presentation-viewer plugin android before-install script");
