//
//  PresentationViewController.h
//  AbbottMobile
//
//  Created by Alexander Voronov on 4/8/14.
//  Copyright (c) 2014 Qap, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PresentationView.h"

@protocol PresentationViewControllerDelegate;

@interface PresentationViewController : UIViewController <UIWebViewDelegate, UIGestureRecognizerDelegate>
@property (nonatomic) NSDictionary* translations;
@property (nonatomic) BOOL isKPISuspended;
@property (nonatomic, retain) IBOutlet PresentationView *webView;
@property (nonatomic, assign) id<PresentationViewControllerDelegate>delegate;
@property (nonatomic, retain) IBOutlet UIView *toolbarMode;
@property (nonatomic, retain) IBOutlet UIButton *btnComplete;
@property (nonatomic, retain) IBOutlet UIButton *btnKPIStateCollectionToggle;
@property (retain, nonatomic) IBOutlet UIView *suspendOverlay;
@property (retain, nonatomic) IBOutlet UILabel *suspendedLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnContact;
- (void)setTranslationsObject:(NSDictionary*) translations;
- (void)loadURL:(NSString *)url;
- (void)resetPresentationView;
- (NSString *)getKPI;
- (void)clearKPI;
- (void)toggleKpiCollectionState;

@end


@protocol PresentationViewControllerDelegate <NSObject>

- (void)presentationViewControllerDidFinishLoading:(PresentationViewController *)presentationViewController;
- (void)presentationViewController:(PresentationViewController *)presentationViewController didFailLoadingWithError:(NSError *)error;
- (void)presentationViewControllerOnComplete:(PresentationViewController *)presentationViewController;
- (void)presentationViewControllerOnShowContact:(PresentationViewController *)presentationViewController;

@end
