webpackJsonp([212],{

/***/ 1577:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataChangeRequestsContactCardPageModule", function() { return DataChangeRequestsContactCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__data_change_requests_contact_card__ = __webpack_require__(417);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DataChangeRequestsContactCardPageModule = /** @class */ (function () {
    function DataChangeRequestsContactCardPageModule() {
    }
    DataChangeRequestsContactCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__data_change_requests_contact_card__["a" /* DataChangeRequestsContactCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__data_change_requests_contact_card__["a" /* DataChangeRequestsContactCardPage */]),
            ],
        })
    ], DataChangeRequestsContactCardPageModule);
    return DataChangeRequestsContactCardPageModule;
}());

//# sourceMappingURL=data-change-requests-contact-card.module.js.map

/***/ })

});
//# sourceMappingURL=212.js.map