import {Entity} from './base/Entity';
import {ProductInPortfolioScheme} from './../models/scheme/ProductInPortfolioScheme';

export class ProfileProductInPortfolio extends Entity {
  id: string;
  productSfId: string;
  portfolioSfId: string;
  startDate: string;
  endDate: string;
  brandName: string;
  
  constructor(responseRecord) {
    super(ProductInPortfolioScheme.fields, responseRecord);
  }
}
