import {Entity} from './base/Entity';
import {DocumentScheme} from './scheme/DocumentScheme';


export class Document extends Entity {
  id: string;
  name: string;
  body: string;
  folderId: string;
  typeOfDocument: string;
  url: string;
  description: string;

  constructor(responseRecord) {
    super(DocumentScheme.fields, responseRecord);
  }
}
