import {Entity} from './base/Entity';
import {ProductPresentationScheme} from './../models/scheme/ProductPresentationScheme';

export class ProductPresentation extends Entity {
  id: string;
  product: string;
  productName: string;
  presentation: string;
  presentationName: string;

  constructor(responseRecord) {
    super(ProductPresentationScheme.fields, responseRecord);
  }
}
