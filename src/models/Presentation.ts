import {Entity} from './base/Entity';
import {PresentationScheme} from "./scheme/PresentationScheme";


export class Presentation extends Entity {
  static RETINA_ICON = 'icon@2x.png';
  static ICON = 'icon.png';

  id: string;
  name: string;
  description: string;
  availableVersion: number;
  url: string;
  iconPath: string;
  currentVersion: number;
  lastModifiedDate: string;
  lastSyncDate: string;

  constructor(responseRecord) {
    super(PresentationScheme.fields, responseRecord);
    this.currentVersion = this.currentVersion || 0;
  }

  wasDownloaded() {
    return this.currentVersion > 0;
  }

  hasUpdate() {
    return this.availableVersion > this.currentVersion;
  }
}
