import { Entity } from './base/Entity';
import { CoachingPlanScheme } from './scheme/CoachingPlanScheme';
import { Utils } from '../utils/Utils';


export class CoachingPlan extends Entity {
  id: string;
  name: string;
  month: string;
  year: string;
  employeeCode: string;
  employeeName: string;
  division: string;
  designation: string;
  dateofjoining: string;
  filledBy: string;
  coachingDate: string;
  dateofFilling: string;


  constructor(responseRecord) {
    super(CoachingPlanScheme.fields, responseRecord);
  }

  public getJoinDateFormat()
  {
    return Utils.originalDate(this.dateofjoining);
  }

}
