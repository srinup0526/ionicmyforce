import {Entity} from './base/Entity';
import {TourPlanningEntityScheme} from './scheme/TourPlanningEntityScheme';


export class TourPlanningEntity extends Entity {
  id: string;


  constructor(responseRecord) {
    super(TourPlanningEntityScheme.fields, responseRecord);
  }
}
