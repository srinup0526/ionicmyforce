declare var ForceSObject:any;

function getter(prop:any, get:any):any {
  return Object.defineProperty(ForceSObject.prototype, prop, {get, configurable: true})
}

function setter(prop:any, set:any):any {
  return Object.defineProperty(ForceSObject.prototype, prop, {set, configurable: true})
}



export class Entity extends ForceSObject {
  table: string = '';
  sfdcTable: string = '';
  description: string = 'Entity';

  sfdc: any = null;             // Model.sfdc.reference will return e.g. 'Reference__c'
  isToLabel: any = null;        // Model.isToLabel.Reference__c or Model.isToLabel['Reference__c'] will return true or false
  indexSpec: string = null;        // Model.indexSpec will return e.g. [{path:'Name',type:'string'}]
  searchFields: string = null;     // Model.searchFields will return e.g. ['Name', 'Address']
  sfdcFields: string = null;       // Model.sfdcFields will return e.g. ['Name', 'Address', 'Reference__c']
  uploadableFields: string = null; // Model.uploadableFields will return e.g. ['Name', 'Address', 'Reference__c']
  excludableFields: any = null; // Model.excludableFields will return e.g. {'GlobalPriority__c': 'isTradeModuleEnabled'}
  includableFields: any = null; // Model.includableFields will return e.g. {'Patient_Profile_1__c': 'isPortfolioSellingModuleEnabled'}

  fieldlist(method:any)
  {
    return this.sfdcFields;
  }
  
  convertJoinedFieldToLocal (record:any, field:any){
    let joinedFields =  field.sfdc.split('.')
    let sfdcValue = record[joinedFields.shift()]
    joinedFields.forEach(joinedField => {
      sfdcValue = sfdcValue?[joinedField]:'';
    })
    record[field.sfdc] = sfdcValue || record[field.sfdc]
  }
    

  mapModel(){
    console.log("method called");
    let field, _i, _len, _ref;
      _ref = this.fullSchema();
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        field = _ref[_i];
        this._mapField(field);
      }
    console.log("this.fullSchema()",this.fullSchema());
    this._generateSfdcFields(this.fullSchema())
    this._generateSfdcPropertiesByLocal(this.fullSchema())
    this._generateIndexSpec(this.fullSchema())
    let indexSpec = this._generateIndexSpec(this.fullSchema());
    console.log("indexSpec",indexSpec);
    this._generateSearchFields(this.fullSchema())
    this._generateIsToLabel(this.fullSchema())
    this._generateUploadableFields(this.fullSchema())
    this._generateExcludableFields(this.fullSchema())
    this._generateIncludableFields(this.fullSchema())
  }
    
  
  fullSchema()
  {
    var schema;
      schema = this.schema();
      if (this.hasSearchable()) {
        schema.push({
          local: 'searchData',
          indexWithType: 'string'
        });
      }
      return schema;
  }
    
  
  hasSearchable()
  {
    return !!this.searchableSchema().length
  }
  
  searchableSchema()
  {
    let schema = this.schema();
    console.log("schema",schema);
    return schema.filter((field => { return field.hasOwnProperty('search') && field.search == true }))
  }

  _mapField(field) {
    console.log("field",field);
    this._generateProperty(field)
  }

  _generateSfdcFields (schema)
  {
    console.log("schema",schema);
    return this.sfdcFields != null ? this.sfdcFields : this.sfdcFields = schema.filter((field => field.hasOwnProperty('sfdc'))).map((field => field.sfdc))
  }

  _generateProperty(field) {
      getter(field.local, function() {
        var _ref;
        if (field.hasOwnProperty('sfdc')) {
          return (_ref = this.attributes[field.sfdc]) != null ? _ref : this[field.sfdc];
        } else {
          return this.attributes[field.local];
        }
      });
      return setter(field.local, function(val) {
        if (field.hasOwnProperty('sfdc')) {
          return this.attributes[field.sfdc] = val;
        } else {
          return this.attributes[field.local] = val;
        }
      });
    };
    

  _generateSfdcPropertiesByLocal(schema){
      if (!this.sfdc) {
        this.sfdc = {};
        return schema.forEach((function(_this) {
          return function(field) {
            if (field.hasOwnProperty('sfdc')) {
              return _this.sfdc[field.local] = field.sfdc;
            }
          };
        })(this));
      }
    };

  _generateIndexSpec(schema) {
    console.log("this.indexspec",this.indexSpec)
      return this.indexSpec != null ? this.indexSpec : this.indexSpec = schema.filter(field => {
        console.log("field at indexspec",field);
        return field.hasOwnProperty('indexWithType' || (field.hasOwnProperty('search') && field.search === true));
      }).map(field => {
          return {
            'path': this._valueOfField(field),
            'type': field.indexWithType
          };
        })
    };

  _generateSearchFields(schema) {
      return this.searchFields = this.searchableSchema().map((function(_this) {
        return function(field) {
          return _this._valueOfField(field);
        };
      })(this));
    };

  _valueOfField(field) {
    console.log("field at _valueOfField", field)
      if (field.hasOwnProperty('sfdc')) {
        return field.sfdc;
      } else {
        return field.local;
      }
    };

  _generateIsToLabel(schema) {
      if (this.isToLabel == null) {
        this.isToLabel = {};
        return schema.forEach((function(_this) {
          return function(field) {
            return _this.isToLabel[field.sfdc] = field.hasOwnProperty('toLabel') && field.toLabel === true;
          };
        })(this));
      }
    };

  _generateUploadableFields(schema) {
      return this.uploadableFields != null ? this.uploadableFields : this.uploadableFields = schema.filter(function(field) {
        return field.hasOwnProperty('upload') && field.upload === true;
      }).map((function(_this) {
        return function(field) {
          return _this._valueOfField(field);
        };
      })(this));
    };

  _generateExcludableFields(schema) {
      var fields;
      if (this.excludableFields == null) {
        this.excludableFields = {};
      }
      fields = schema.filter(function(field) {
        return field.hasOwnProperty('exclude') && field.exclude;
      });
      return fields != null ? fields.forEach((function(_this) {
        return function(field) {
          return _this.excludableFields[_this._valueOfField(field)] = field.exclude;
        };
      })(this)) : void 0;
    }

  _generateIncludableFields(schema) {
      var fields;
      if (this.includableFields == null) {
        this.includableFields = {};
      }
      fields = schema.filter(function(field) {
        return field.hasOwnProperty('include') && field.include;
      });
      return fields != null ? fields.forEach((function(_this) {
        return function(field) {
          return _this.includableFields[_this._valueOfField(field)] = field.include;
        };
      })(this)) : void 0;
    };

}