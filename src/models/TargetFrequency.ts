import {Entity} from './base/Entity';
import {TargetFrequencyScheme} from './scheme/TargetFrequencyScheme'
import {Utils} from "../utils/Utils";


export class TargetFrequency extends Entity {
    public id: string;
    public marketingCycleSfId: string;
    public targetSfId: string;
    public customerSfId: string;
    public isActive: string;
    public actualCallsCount: string;
    public mcTargetFrequency: string;
    public isPharmacist: string;
    public isPrimary: string;
    public targetCycleFrequency: string;
    public priority: string;
    public lastCallReportDate: string;
    public medrepId: string;
    public medrepFirstName: string;
    public medrepLastName: string;
   
  constructor(responseRecord) {
    super(TargetFrequencyScheme.fields, responseRecord);
  }

  lastCallReportDateFormated(): string {
    if (this.lastCallReportDate){
      return Utils.dotFormatDate(this.lastCallReportDate)
    }
    else {
      return '';
    }
  }

  medrepFullName(): string {
    return `${this.medrepLastName || ''} ${this.medrepFirstName || ''}`;
  }

  atCalls(): string {
    return `${this.actualCallsCount || 0}/${this.targetCycleFrequency || 0}`;
  }

  lastCall() {
    const lastCall = this.lastCallReportDateFormated();

    if(lastCall) {
      return `${lastCall} <br/> ${this.medrepFullName()}`;
    }
    return '';
  }
}
