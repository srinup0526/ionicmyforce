import {Entity} from './base/Entity';
import {SurveyScheme} from './scheme/SurveyScheme';
import { Utils } from '../utils/Utils';



export class Survey extends Entity {
  id: string;
  surveyname: string;
  startdate: string;
  enddate: string;
  isActive: string;
  createddate: string;
  pharmaProduct: string;
  productAtcClass: string;
  bu: string;
  currency: string;


  constructor(responseRecord) {
    super(SurveyScheme.fields, responseRecord);
  }

  public getCreatedDateTimeInFormat() {
    return Utils.formatDateTimeWithBreak(this.createddate);
  }
}
