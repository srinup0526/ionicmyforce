import {Entity} from './base/Entity';
import {ScenarioScheme} from './scheme/ScenarioScheme';


export class Scenario extends Entity {
  static TYPE = {
    "CORE_VERSION": "CoreVersion",
    "OTHER_VERSION": "OtherVersion"
  };

  id: string;
  name: string;
  cobaltVersion: string;
  typeOfVersion: string;
  structure: string;

  constructor(responseRecord) {
    super(ScenarioScheme.fields, responseRecord);
  }
}
