import {Entity} from './base/Entity';
import {OrderScheme} from './scheme/OrderScheme';
import {Utils} from "../utils/Utils";


export class Order extends Entity {
  id: string;
  name: string;
  accountSfId: string;
  externalAccountId: string;
  remoteAccountName: string;
  remoteAccountRecordType: string;
  specialprice: string;
  accountName: string;
  status: string;
  billindAddress: string;
  dateTimeCreated: string;
  dateTimeSentToDistributor: string;
  notes: string;
  orderSalesAmount: number;
  orderTotalQuantity: number;
  shippingAddress: string;
  signature: string;
  recordTypeId: string;
  recordType: string;
  remoteRecordType: string;
  sampleno: string;
  pono: string;
  newAddress: string;
  remarks: string;
  noncustomer: string;
  _soupEntryId: string;


  constructor(responseRecord) {
    super(OrderScheme.fields, responseRecord);
  }
  
  public getOrderDateTimeInFormat() {
    return Utils.formatDateTimeWithBreak(this.dateTimeCreated);
  }
  
  public getAccountColumnData(delimiter = '<br/>') {
    return [
      this.remoteAccountName,
      this.remoteAccountRecordType
    ]
      .filter((item) => !!item)
      .join(delimiter);
  }
}
