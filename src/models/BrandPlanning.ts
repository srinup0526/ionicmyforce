import {Entity} from './base/Entity';
import {BrandPlanningScheme} from './scheme/BrandPlanningScheme';



export class BrandPlanning extends Entity {
  static APPROVE_STATUS_VALUE = 'Approve';
  static getCycleNameFromQuarter(quarter: number) {
    return `Cycle${quarter}`;
  }

  id: string;
  name: string;
  customer: string;
  year: string;
  cycle: string;
  focusBrand1: string;
  focusBrand2: string;
  focusBrand3: string;
  focusBrand4: string;

  constructor(responseRecord) {
    super(BrandPlanningScheme.fields, responseRecord);
  }


  get focusedBrandIds(): string[] {
    return [this.focusBrand1, this.focusBrand2, this.focusBrand3, this.focusBrand4]
      .filter(focusBrand => focusBrand);
  }
}
