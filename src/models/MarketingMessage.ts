import {Entity} from './base/Entity';
import {MarketingMessageScheme} from './scheme/MarketingMessageScheme';


export class MarketingMessage extends Entity {
  id: string;
  name: string;
  produtSfId: string;
  status: string;
  currencyIsoCode: string;

  constructor(responseRecord) {
    super(MarketingMessageScheme.fields, responseRecord);
  }
}
