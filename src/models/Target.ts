import {Entity} from './base/Entity';
import {TargetScheme} from './scheme/TargetScheme';


export class Target extends Entity {
    id: string;
    medrepSfId: string;
    medrepBusinessUnit: string;
    
  constructor(responseRecord) {
    super(TargetScheme.fields, responseRecord);
  }
}
