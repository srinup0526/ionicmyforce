import {Entity} from './base/Entity';
import {ProductSegmentationScheme} from './scheme/ProductSegmentationScheme';


export class ProductSegmentation extends Entity {
  public id: string;
  public accountId: string;
  public isActive: string;
  public phProductId: string;
  public phProductName: string;
  public segmentation1: string;
  public segmentation2: string;
  

  constructor(responseRecord) {
    super(ProductSegmentationScheme.fields, responseRecord);
  }
}
