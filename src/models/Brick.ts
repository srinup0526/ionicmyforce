import {Entity} from './base/Entity';
import {BrickScheme} from './scheme/BrickScheme';


export class Brick extends Entity {
  id: string;
  name: string;
  shortDescription: string;

  constructor(responseRecord) {
    super(BrickScheme.fields, responseRecord);
  }
}
