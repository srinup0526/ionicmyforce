import {Entity} from './base/Entity';
import {PEAbbottAttendeeScheme} from './../models/scheme/PEAbbottAttendeeScheme';

export class PEAbbottAttendee extends Entity {
  id: string;
  pharmaEventSfId: string;
  attendeeSfId: string;
  clmToolId: string;
  

  constructor(responseRecord) {
    super(PEAbbottAttendeeScheme.fields, responseRecord);
  }
}
