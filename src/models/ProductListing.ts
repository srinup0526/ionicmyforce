import {Entity} from './base/Entity';
import {ProductListingScheme} from './../models/scheme/ProductListingScheme';

export class ProductListing extends Entity {
  id: string;
  accountId: string;
  isActive: string;
  phProductId: string;
  phProductName: string;
  status: string;

  constructor(responseRecord) {
    super(ProductListingScheme.fields, responseRecord);
  }
}
