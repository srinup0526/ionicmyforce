import {Entity} from './base/Entity';
import {IqviaTaskAdjustmentScheme} from './scheme/IqviaTaskAdjustmentScheme';

export class IqviaTaskAdjustment extends Entity {
  id: string;
  callReport: string;
  clmToolId: string;
  organization: string;
  question: string;
  task: string;
  numberRealValue: string;
  stringRealValue: string;
  sku: string;
  skuName: string;

  constructor(responseRecord) {
    super(IqviaTaskAdjustmentScheme.fields, responseRecord);
  }
}
