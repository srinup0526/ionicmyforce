import {Entity} from './base/Entity';
import {PEAttendeeScheme} from './../models/scheme/PEAttendeeScheme';

export class PEAttendee extends Entity {
  id: string;
  pharmaEventSfId: string;
  attendeeSfId: string;
  clmToolId: string;
  specialty: string;
  organization: string;
  organizationName: string;
  organizationCity: string;
  organizationPhone: string;
  organizationCountry: string;
  organizationAddress: string;
  typeOfHospital: string;
  signature: string;
  attHadLunch: string;
  noncustomer: string;
  confirmHCPCount: string;
  nonOrganization: string;

  constructor(responseRecord) {
    super(PEAttendeeScheme.fields, responseRecord);
  }
}
