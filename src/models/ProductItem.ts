import {Entity} from './base/Entity';
import {ProductItemScheme} from './../models/scheme/ProductItemScheme';

export class ProductItem extends Entity {
  id: string;
  description: string;
  name: string;
  imsSkuCode: string;
  parallelImport: string;
  sales1MonthAgo: string;
  sales2MonthAgo: string;
  sales3MonthAgo: string;
  sales4MonthAgo: string;
  salesYTDC: string;
  price:number;
  productBrandName: string;
  productDetailCode: string;
  productTypeDetect: string;
  productAtcClass:string;
  productDivision:string;

  constructor(responseRecord) {
    super(ProductItemScheme.fields, responseRecord);
  }
}
