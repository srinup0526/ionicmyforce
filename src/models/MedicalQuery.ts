import {Entity} from './base/Entity';
import {MedicalQueryScheme} from './scheme/MedicalQueryScheme';
import { Utils } from '../utils/Utils';


export class MedicalQuery extends Entity {
  id: string;
  createdDateTime: string;
  customer: string;
  noncustomer: string;
  specialty: string;
  remoteCustomerName: string;
  customerName: string;
  organization: string;
  remoteOrganizationName: string;
  organizationName: string;
  remoteEmail: string;
  email: string;
  remoteMobile: string;
  mobile: string;
  medicalContact: string;
  remoteMedicalContactName: string;
  medicalContactName: string;
  pharmaProduct: string;
  remotePharmaProductName: string;
  pharmaProductName: string;
  queryDescription: string;
  status: string;
  typeofquery: string;
  user: string;
  remoteUserName: string;
  userName: string;
  mylanMedicalContactPersons: string;
  mylanMedicalContactPersonEmails: string;


  constructor(responseRecord) {
    super(MedicalQueryScheme.fields, responseRecord);
  }

  public getProductName()
  {
    console.log("this.pharmaProductName",this.pharmaProductName);
    return  `${this.pharmaProductName || ''}`;
  }

  public getCustomerName()
  {
    return `${this.remoteCustomerName || ''} `;
  }

  public getMedicalContactName()
  {
    return `${this.remoteMedicalContactName || ''}`;
  }
  
  public getCreatedDateTimeInFormat() {
    return Utils.formatDateTimeWithBreak(this.createdDateTime);
  }
}
