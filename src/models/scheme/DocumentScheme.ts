import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class DocumentScheme {
  static readonly table: string = 'Document';
  static readonly sfdcTable: string = 'Document';
  static readonly description: string = 'Document';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    body: new Field('Body', 'body', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    folderId: new Field('FolderId', 'folderId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    typeOfDocument: new Field('Type', 'typeOfDocument', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    url: new Field('Url', 'url', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    description: new Field('Description', 'description', new FieldOption()
      .setIndexWithType('string')),

    shortDescription: new Field('Short_Description__c', 'shortDescription', new FieldOption()
      .setIndexWithType('string')),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(DocumentScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(DocumentScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(DocumentScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(DocumentScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(DocumentScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(DocumentScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(DocumentScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(DocumentScheme.fields);
  }
}
