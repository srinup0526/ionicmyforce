import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import {SchemeParser} from './SchemeParser';


export class CallReportScheme {
  static readonly table: string = 'CallReport';
  static readonly sfdcTable: string = 'Call_Report__c';
  static readonly description: string = 'Call Report';
  
  static readonly TYPE_APPOINTMENT: string = 'Appointment';
  static readonly TYPE_ONE_TO_ONE: string = '1:1';
  static readonly STATUS_SAVED: string = 'Saved';
  static readonly Statuses: {
    Saved: 'Saved'
    Submitted: 'Submitted'
  };
  static readonly PORTFOLIO_PRESENTATION_REMINDER_YES: string = 'Yes';
  static readonly PORTFOLIO_PRESENTATION_REMINDER_NO: string = 'No';
  static readonly MESSAGES_IN_PRODUCT_NUMBER: 3;
  static readonly MAX_PRODUCTS_NUMBER: 10;
  static readonly APPROVAL_STATUS_VALIDATED: string = 'Validated';
  
  static readonly specialty: any;
  static readonly targetFrequency: any;
  static readonly attachedMTP: any;
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
    
    organizationSfId: new Field('Organisation__c', 'organizationSfId', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),
    
    contactSfId: new Field('Contact1__c', 'contactSfId', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),
    
    mtp: new Field('MTP__c', 'mtp', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()
      .setInclude('isMtpModuleEnabled')),
    
    dateTimePlanned: new Field('Date_Time_Planned__c', 'dateTimePlanned', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    indCustomer: new Field('Customer__c', 'indCustomer', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()
      .setInclude('isIndiaUser')),

    dateTimeVisitStart: new Field('Date_Time_Actual__c', 'dateTimeVisitStart', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),
    
    dateTimeVisitEnd: new Field('Date_time_Visit_End__c', 'dateTimeVisitEnd', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),
    
    createdFromMobile: new Field('Created_from_Mobile__c', 'createdFromMobile', new FieldOption()
      .setUploadState()),
    
    createdOffline: new Field('Created_Offline__c', 'createdOffline', new FieldOption()
      .setUploadState()),
    
    isTargetCall: new Field('Is_Target_Call__c', 'isTargetCall', new FieldOption()),
    
    type: new Field('Type__c', 'type', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),
    
    recordTypeId: new Field('RecordTypeId', 'recordTypeId', new FieldOption()
      .setUploadState()),
    
    duration: new Field('Duration__c', 'duration', new FieldOption()
      .setUploadState()),
    
    targetPriority: new Field('Target_Priority__c', 'targetPriority', new FieldOption()),
    
    coachingVisit: new Field('Coaching_Visit__c', 'coachingVisit', new FieldOption()
      .setUploadState()),

    activityType: new Field('Activity_type__c', 'activityType', new FieldOption()
    .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),
      
    coachingVisitUserSfid: new Field('Coaching_Visit_User__c', 'coachingVisitUserSfid', new FieldOption()
      .setUploadState()),
    
    userSfid: new Field('User__c', 'userSfid', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
    
    generalComments: new Field('GeneralComments__c', 'generalComments', new FieldOption()
      .setUploadState()),
    
    callObjective: new Field('Call_Objective__c', 'callObjective', new FieldOption()
      .setUploadState()),
    
    nextCallObjective: new Field('Next_Call_Objective__c', 'nextCallObjective', new FieldOption()
      .setUploadState()),
    
    promotionalItemsPrio1: new Field('Promotional_Items_Prio_1__c', 'promotionalItemsPrio1', new FieldOption()
      .setUploadState()),
    
    prio1ProductSfid: new Field('Prio_1_Product__c', 'prio1ProductSfid', new FieldOption()
      .setUploadState()),
    
    noteForPrio1: new Field('Note_for_Prio_1__c', 'noteForPrio1', new FieldOption()
      .setUploadState()),
    
    prio1MarketingMessage1: new Field('Prio_1_Marketing_Message_1__c', 'prio1MarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio1MarketingMessage2: new Field('Prio_1_Marketing_Message_2__c', 'prio1MarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio1MarketingMessage3: new Field('Prio_1_Marketing_Message_3__c', 'prio1MarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio1MarketingMessage4: new Field('Prio_1_Marketing_Message_4__c', 'prio1MarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio1MarketingMessage5: new Field('Prio_1_Marketing_Message_5__c', 'prio1MarketingMessage5', new FieldOption()
      .setUploadState()),
    
    prio1ReactionsToMarketingMessage1: new Field('Prio_1_Reactions_To_Marketing_Message_1__c', 'prio1ReactionsToMarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio1ReactionsToMarketingMessage2: new Field('Prio_1_Reactions_To_Marketing_Message_2__c', 'prio1ReactionsToMarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio1ReactionsToMarketingMessage3: new Field('Prio_1_Reactions_To_Marketing_Message_3__c', 'prio1ReactionsToMarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio1ReactionsToMarketingMessage4: new Field('Prio_1_Reactions_To_Marketing_Message_4__c', 'prio1ReactionsToMarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio1ReactionsToMarketingMessage5: new Field('Prio_1_Reactions_To_Marketing_Message_5__c', 'prio1ReactionsToMarketingMessage5', new FieldOption()
      .setUploadState()),
    
    patientProfile1: new Field('Patient_Profile_1__c', 'patientProfile1', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    prio1Classification: new Field('Prio1_Classification__c', 'prio1Classification', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    promotionalItemsPrio2: new Field('Promotional_Items_Prio_2__c', 'promotionalItemsPrio2', new FieldOption()
      .setUploadState()),
    
    prio2ProductSfid: new Field('Prio_2_Product__c', 'prio2ProductSfid', new FieldOption()
      .setUploadState()),
    
    noteForPrio2: new Field('Note_for_Prio_2__c', 'noteForPrio2', new FieldOption()
      .setUploadState()),
    
    prio2MarketingMessage1: new Field('Prio_2_Marketing_Message_1__c', 'prio2MarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio2MarketingMessage2: new Field('Prio_2_Marketing_Message_2__c', 'prio2MarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio2MarketingMessage3: new Field('Prio_2_Marketing_Message_3__c', 'prio2MarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio2MarketingMessage4: new Field('Prio_2_Marketing_Message_4__c', 'prio2MarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio2MarketingMessage5: new Field('Prio_2_Marketing_Message_5__c', 'prio2MarketingMessage5', new FieldOption()
      .setUploadState()),
    
    prio2ReactionsToMarketingMessage1: new Field('Prio_2_Reactions_To_Marketing_Message_1__c', 'prio2ReactionsToMarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio2ReactionsToMarketingMessage2: new Field('Prio_2_Reactions_To_Marketing_Message_2__c', 'prio2ReactionsToMarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio2ReactionsToMarketingMessage3: new Field('Prio_2_Reactions_To_Marketing_Message_3__c', 'prio2ReactionsToMarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio2ReactionsToMarketingMessage4: new Field('Prio_2_Reactions_To_Marketing_Message_4__c', 'prio2ReactionsToMarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio2ReactionsToMarketingMessage5: new Field('Prio_2_Reactions_To_Marketing_Message_5__c', 'prio2ReactionsToMarketingMessage5', new FieldOption()
      .setUploadState()),
    
    patientProfile2: new Field('Patient_Profile_2__c', 'patientProfile2', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    prio2Classification: new Field('Prio2_Classification__c', 'prio2Classification', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    promotionalItemsPrio3: new Field('Promotional_Items_Prio_3__c', 'promotionalItemsPrio3', new FieldOption()
      .setUploadState()),
    
    prio3ProductSfid: new Field('Prio_3_Product__c', 'prio3ProductSfid', new FieldOption()
      .setUploadState()),
    
    noteForPrio3: new Field('Note_for_Prio_3__c', 'noteForPrio3', new FieldOption()
      .setUploadState()),
    
    prio3MarketingMessage1: new Field('Prio_3_Marketing_Message_1__c', 'prio3MarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio3MarketingMessage2: new Field('Prio_3_Marketing_Message_2__c', 'prio3MarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio3MarketingMessage3: new Field('Prio_3_Marketing_Message_3__c', 'prio3MarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio3MarketingMessage4: new Field('Prio_3_Marketing_Message_4__c', 'prio3MarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio3MarketingMessage5: new Field('Prio_3_Marketing_Message_5__c', 'prio3MarketingMessage5', new FieldOption()
      .setUploadState()),
    
    prio3ReactionsToMarketingMessage1: new Field('Prio_3_Reactions_To_Marketing_Message_1__c', 'prio3ReactionsToMarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio3ReactionsToMarketingMessage2: new Field('Prio_3_Reactions_To_Marketing_Message_2__c', 'prio3ReactionsToMarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio3ReactionsToMarketingMessage3: new Field('Prio_3_Reactions_To_Marketing_Message_3__c', 'prio3ReactionsToMarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio3ReactionsToMarketingMessage4: new Field('Prio_3_Reactions_To_Marketing_Message_4__c', 'prio3ReactionsToMarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio3ReactionsToMarketingMessage5: new Field('Prio_3_Reactions_To_Marketing_Message_5__c', 'prio3ReactionsToMarketingMessage5', new FieldOption()
      .setUploadState()),
    
    patientProfile3: new Field('Patient_Profile_3__c', 'patientProfile3', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    prio3Classification: new Field('Prio3_Classification__c', 'prio3Classification', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    promotionalItemsPrio4: new Field('Promotional_Items_Prio_4__c', 'promotionalItemsPrio4', new FieldOption()
      .setUploadState()),
    
    prio4ProductSfid: new Field('Prio_4_Product__c', 'prio4ProductSfid', new FieldOption()
      .setUploadState()),
    
    noteForPrio4: new Field('Note_for_Prio_4__c', 'noteForPrio4', new FieldOption()
      .setUploadState()),
    
    prio4MarketingMessage1: new Field('Prio_4_Marketing_Message_1__c', 'prio4MarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio4MarketingMessage2: new Field('Prio_4_Marketing_Message_2__c', 'prio4MarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio4MarketingMessage3: new Field('Prio_4_Marketing_Message_3__c', 'prio4MarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio4MarketingMessage4: new Field('Prio_4_Marketing_Message_4__c', 'prio4MarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio4MarketingMessage5: new Field('Prio_4_Marketing_Message_5__c', 'prio4MarketingMessage5', new FieldOption()
      .setUploadState()),
    
    prio4ReactionsToMarketingMessage1: new Field('Prio_4_Reactions_To_Marketing_Message_1__c', 'prio4ReactionsToMarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio4ReactionsToMarketingMessage2: new Field('Prio_4_Reactions_To_Marketing_Message_2__c', 'prio4ReactionsToMarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio4ReactionsToMarketingMessage3: new Field('Prio_4_Reactions_To_Marketing_Message_3__c', 'prio4ReactionsToMarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio4ReactionsToMarketingMessage4: new Field('Prio_4_Reactions_To_Marketing_Message_4__c', 'prio4ReactionsToMarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio4ReactionsToMarketingMessage5: new Field('Prio_4_Reactions_To_Marketing_Message_5__c', 'prio4ReactionsToMarketingMessage5', new FieldOption()
      .setUploadState()),
    
    patientProfile4: new Field('Patient_Profile_4__c', 'patientProfile4', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    prio4Classification: new Field('Prio4_Classification__c', 'prio4Classification', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    promotionalItemsPrio5: new Field('Promotional_Items_Prio_5__c', 'promotionalItemsPrio5', new FieldOption()
      .setUploadState()),
    
    prio5ProductSfid: new Field('Prio_5_Product__c', 'prio5ProductSfid', new FieldOption()
      .setUploadState()),
    
    noteForPrio5: new Field('Note_for_Prio_5__c', 'noteForPrio5', new FieldOption()
      .setUploadState()),
    
    prio5MarketingMessage1: new Field('Prio_5_Marketing_Message_1__c', 'prio5MarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio5MarketingMessage2: new Field('Prio_5_Marketing_Message_2__c', 'prio5MarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio5MarketingMessage3: new Field('Prio_5_Marketing_Message_3__c', 'prio5MarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio5MarketingMessage4: new Field('Prio_5_Marketing_Message_4__c', 'prio5MarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio5MarketingMessage5: new Field('Prio_5_Marketing_Message_5__c', 'prio5MarketingMessage5', new FieldOption()
      .setUploadState()),
    
    prio5ReactionsToMarketingMessage1: new Field('Prio_5_Reactions_To_Marketing_Message_1__c', 'prio5ReactionsToMarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio5ReactionsToMarketingMessage2: new Field('Prio_5_Reactions_To_Marketing_Message_2__c', 'prio5ReactionsToMarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio5ReactionsToMarketingMessage3: new Field('Prio_5_Reactions_To_Marketing_Message_3__c', 'prio5ReactionsToMarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio5ReactionsToMarketingMessage4: new Field('Prio_5_Reactions_To_Marketing_Message_4__c', 'prio5ReactionsToMarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio5ReactionsToMarketingMessage5: new Field('Prio_5_Reactions_To_Marketing_Message_5__c', 'prio5ReactionsToMarketingMessage5', new FieldOption()
      .setUploadState()),
    
    patientProfile5: new Field('Patient_Profile_5__c', 'patientProfile5', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    prio5Classification: new Field('Prio5_Classification__c', 'prio5Classification', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    promotionalItemsPrio6: new Field('Promotional_Items_Prio_6__c', 'promotionalItemsPrio6', new FieldOption()
      .setUploadState()),
    
    prio6ProductSfid: new Field('Prio_6_Product__c', 'prio6ProductSfid', new FieldOption()
      .setUploadState()),
    
    noteForPrio6: new Field('Note_for_Prio_6__c', 'noteForPrio6', new FieldOption()
      .setUploadState()),
    
    prio6MarketingMessage1: new Field('Prio_6_Marketing_Message_1__c', 'prio6MarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio6MarketingMessage2: new Field('Prio_6_Marketing_Message_2__c', 'prio6MarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio6MarketingMessage3: new Field('Prio_6_Marketing_Message_3__c', 'prio6MarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio6MarketingMessage4: new Field('Prio_6_Marketing_Message_4__c', 'prio6MarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio6MarketingMessage5: new Field('Prio_6_Marketing_Message_5__c', 'prio6MarketingMessage5', new FieldOption()
      .setUploadState()),
    
    prio6ReactionsToMarketingMessage1: new Field('Prio_6_Reactions_To_Marketing_Message_1__c', 'prio6ReactionsToMarketingMessage1', new FieldOption()
      .setUploadState()),
    
    prio6ReactionsToMarketingMessage2: new Field('Prio_6_Reactions_To_Marketing_Message_2__c', 'prio6ReactionsToMarketingMessage2', new FieldOption()
      .setUploadState()),
    
    prio6ReactionsToMarketingMessage3: new Field('Prio_6_Reactions_To_Marketing_Message_3__c', 'prio6ReactionsToMarketingMessage3', new FieldOption()
      .setUploadState()),
    
    prio6ReactionsToMarketingMessage4: new Field('Prio_6_Reactions_To_Marketing_Message_4__c', 'prio6ReactionsToMarketingMessage4', new FieldOption()
      .setUploadState()),
    
    prio6ReactionsToMarketingMessage5: new Field('Prio_6_Reactions_To_Marketing_Message_5__c', 'prio6ReactionsToMarketingMessage5', new FieldOption()
      .setUploadState()),
    
    patientProfile6: new Field('Patient_Profile_6__c', 'patientProfile6', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    prio6Classification: new Field('Prio6_Classification__c', 'prio6Classification', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    // prio7ProductSfid: new Field('Prio_7_Product__c', 'prio7ProductSfid', new FieldOption()
    //   .setUploadState()),
    
    // noteForPrio7: new Field('Note_for_Prio_7__c', 'noteForPrio7', new FieldOption()
    //   .setUploadState()),
    
    // prio7MarketingMessage1: new Field('Prio_7_Marketing_Message_1__c', 'prio7MarketingMessage1', new FieldOption()
    //   .setUploadState()),
    
    // prio7MarketingMessage2: new Field('Prio_7_Marketing_Message_2__c', 'prio7MarketingMessage2', new FieldOption()
    //   .setUploadState()),
    
    // prio7MarketingMessage3: new Field('Prio_7_Marketing_Message_3__c', 'prio7MarketingMessage3', new FieldOption()
    //   .setUploadState()),
    
    // prio7MarketingMessage4: new Field('Prio_7_Marketing_Message_4__c', 'prio7MarketingMessage4', new FieldOption()
    //   .setUploadState()),
    
    // prio7MarketingMessage5: new Field('Prio_7_Marketing_Message_5__c', 'prio7MarketingMessage5', new FieldOption()
    //   .setUploadState()),
    
    // prio7ReactionsToMarketingMessage1: new Field('Prio_7_Reactions_To_Marketing_Message_1__c', 'prio7ReactionsToMarketingMessage1', new FieldOption()
    //   .setUploadState()),
    
    // prio7ReactionsToMarketingMessage2: new Field('Prio_7_Reactions_To_Marketing_Message_2__c', 'prio7ReactionsToMarketingMessage2', new FieldOption()
    //   .setUploadState()),
    
    // prio7ReactionsToMarketingMessage3: new Field('Prio_7_Reactions_To_Marketing_Message_3__c', 'prio7ReactionsToMarketingMessage3', new FieldOption()
    //   .setUploadState()),
    
    // prio7ReactionsToMarketingMessage4: new Field('Prio_7_Reactions_To_Marketing_Message_4__c', 'prio7ReactionsToMarketingMessage4', new FieldOption()
    //   .setUploadState()),
    
    // prio7ReactionsToMarketingMessage5: new Field('Prio_7_Reactions_To_Marketing_Message_5__c', 'prio7ReactionsToMarketingMessage5', new FieldOption()
    //   .setUploadState()),
    
    // patientProfile7: new Field('Patient_Profile_7__c', 'patientProfile7', new FieldOption()
    //   .setInclude('isPortfolioSellingModuleEnabled')
    //   .setUploadState()),
    
    // prio7Classification: new Field('Prio7_Classification__c', 'prio7Classification', new FieldOption()
    //   .setInclude('isPortfolioSellingModuleEnabled')
    //   .setUploadState()),
    
    // prio8ProductSfid: new Field('Prio_8_Product__c', 'prio8ProductSfid', new FieldOption()
    //   .setUploadState()),
    
    // noteForPrio8: new Field('Note_for_Prio_8__c', 'noteForPrio8', new FieldOption()
    //   .setUploadState()),
    
    // prio8MarketingMessage1: new Field('Prio_8_Marketing_Message_1__c', 'prio8MarketingMessage1', new FieldOption()
    //   .setUploadState()),
    
    // prio8MarketingMessage2: new Field('Prio_8_Marketing_Message_2__c', 'prio8MarketingMessage2', new FieldOption()
    //   .setUploadState()),
    
    // prio8MarketingMessage3: new Field('Prio_8_Marketing_Message_3__c', 'prio8MarketingMessage3', new FieldOption()
    //   .setUploadState()),
    
    // prio8MarketingMessage4: new Field('Prio_8_Marketing_Message_4__c', 'prio8MarketingMessage4', new FieldOption()
    //   .setUploadState()),
    
    // prio8MarketingMessage5: new Field('Prio_8_Marketing_Message_5__c', 'prio8MarketingMessage5', new FieldOption()
    //   .setUploadState()),
    
    // prio8ReactionsToMarketingMessage1: new Field('Prio_8_Reactions_To_Marketing_Message_1__c', 'prio8ReactionsToMarketingMessage1', new FieldOption()
    //   .setUploadState()),
    
    // prio8ReactionsToMarketingMessage2: new Field('Prio_8_Reactions_To_Marketing_Message_2__c', 'prio8ReactionsToMarketingMessage2', new FieldOption()
    //   .setUploadState()),
    
    // prio8ReactionsToMarketingMessage3: new Field('Prio_8_Reactions_To_Marketing_Message_3__c', 'prio8ReactionsToMarketingMessage3', new FieldOption()
    //   .setUploadState()),
    
    // prio8ReactionsToMarketingMessage4: new Field('Prio_8_Reactions_To_Marketing_Message_4__c', 'prio8ReactionsToMarketingMessage4', new FieldOption()
    //   .setUploadState()),
    
    // prio8ReactionsToMarketingMessage5: new Field('Prio_8_Reactions_To_Marketing_Message_5__c', 'prio8ReactionsToMarketingMessage5', new FieldOption()
    //   .setUploadState()),
    
    // patientProfile8: new Field('Patient_Profile_8__c', 'patientProfile8', new FieldOption()
    //   .setInclude('isPortfolioSellingModuleEnabled')
    //   .setUploadState()),
    
    // prio8Classification: new Field('Prio8_Classification__c', 'prio8Classification', new FieldOption()
    //   .setInclude('isPortfolioSellingModuleEnabled')
    //   .setUploadState()),
    
    // prio9ProductSfid: new Field('Prio_9_Product__c', 'prio9ProductSfid', new FieldOption()
    //   .setUploadState()),
    
    // noteForPrio9: new Field('Note_for_Prio_9__c', 'noteForPrio9', new FieldOption()
    //   .setUploadState()),
    
    // prio9MarketingMessage1: new Field('Prio_9_Marketing_Message_1__c', 'prio9MarketingMessage1', new FieldOption()
    //   .setUploadState()),
    
    // prio9MarketingMessage2: new Field('Prio_9_Marketing_Message_2__c', 'prio9MarketingMessage2', new FieldOption()
    //   .setUploadState()),
    
    // prio9MarketingMessage3: new Field('Prio_9_Marketing_Message_3__c', 'prio9MarketingMessage3', new FieldOption()
    //   .setUploadState()),
    
    // prio9MarketingMessage4: new Field('Prio_9_Marketing_Message_4__c', 'prio9MarketingMessage4', new FieldOption()
    //   .setUploadState()),
    
    // prio9MarketingMessage5: new Field('Prio_9_Marketing_Message_5__c', 'prio9MarketingMessage5', new FieldOption()
    //   .setUploadState()),
    
    // prio9ReactionsToMarketingMessage1: new Field('Prio_9_Reactions_To_Marketing_Message_1__c', 'prio9ReactionsToMarketingMessage1', new FieldOption()
    //   .setUploadState()),
    
    // prio9ReactionsToMarketingMessage2: new Field('Prio_9_Reactions_To_Marketing_Message_2__c', 'prio9ReactionsToMarketingMessage2', new FieldOption()
    //   .setUploadState()),
    
    // prio9ReactionsToMarketingMessage3: new Field('Prio_9_Reactions_To_Marketing_Message_3__c', 'prio9ReactionsToMarketingMessage3', new FieldOption()
    //   .setUploadState()),
    
    // prio9ReactionsToMarketingMessage4: new Field('Prio_9_Reactions_To_Marketing_Message_4__c', 'prio9ReactionsToMarketingMessage4', new FieldOption()
    //   .setUploadState()),
    
    // prio9ReactionsToMarketingMessage5: new Field('Prio_9_Reactions_To_Marketing_Message_5__c', 'prio9ReactionsToMarketingMessage5', new FieldOption()
    //   .setUploadState()),
    
    // patientProfile9: new Field('Patient_Profile_9__c', 'patientProfile9', new FieldOption()
    //   .setInclude('isPortfolioSellingModuleEnabled')
    //   .setUploadState()),
    
    // prio9Classification: new Field('Prio9_Classification__c', 'prio9Classification', new FieldOption()
    //   .setInclude('isPortfolioSellingModuleEnabled')
    //   .setUploadState()),
    
    // prio10ProductSfid: new Field('Prio_10_Product__c', 'prio10ProductSfid', new FieldOption()
    //   .setUploadState()),
    
    // noteForPrio10: new Field('Note_for_Prio_10__c', 'noteForPrio10', new FieldOption()
    //   .setUploadState()),
    
    // prio10MarketingMessage1: new Field('Prio_10_Marketing_Message_1__c', 'prio10MarketingMessage1', new FieldOption()
    //   .setUploadState()),
    
    // prio10MarketingMessage2: new Field('Prio_10_Marketing_Message_2__c', 'prio10MarketingMessage2', new FieldOption()
    //   .setUploadState()),
    
    // prio10MarketingMessage3: new Field('Prio_10_Marketing_Message_3__c', 'prio10MarketingMessage3', new FieldOption()
    //   .setUploadState()),
    
    // prio10MarketingMessage4: new Field('Prio_10_Marketing_Message_4__c', 'prio10MarketingMessage4', new FieldOption()
    //   .setUploadState()),
    
    // prio10MarketingMessage5: new Field('Prio_10_Marketing_Message_5__c', 'prio10MarketingMessage5', new FieldOption()
    //   .setUploadState()),
    
    // prio10ReactionsToMarketingMessage1: new Field('Prio_10_Reactions_To_Marketing_Message_1__c', 'prio10ReactionsToMarketingMessage1', new FieldOption()
    //   .setUploadState()),
    
    // prio10ReactionsToMarketingMessage2: new Field('Prio_10_Reactions_To_Marketing_Message_2__c', 'prio10ReactionsToMarketingMessage2', new FieldOption()
    //   .setUploadState()),
    
    
    // prio10ReactionsToMarketingMessage3: new Field('Prio_10_Reactions_To_Marketing_Message_3__c', 'prio10ReactionsToMarketingMessage3', new FieldOption()
    //   .setUploadState()),
    
    
    // prio10ReactionsToMarketingMessage4: new Field('Prio_10_Reactions_To_Marketing_Message_4__c', 'prio10ReactionsToMarketingMessage4', new FieldOption()
    //   .setUploadState()),
    
    // prio10ReactionsToMarketingMessage5: new Field('Prio_10_Reactions_To_Marketing_Message_5__c', 'prio10ReactionsToMarketingMessage5', new FieldOption()
    //   .setUploadState()),
    
    // patientProfile10: new Field('Patient_Profile_10__c', 'patientProfile10', new FieldOption()
    //   .setInclude('isPortfolioSellingModuleEnabled')
    //   .setUploadState()),
    
    // prio10Classification: new Field('Prio10_Classification__c', 'prio10Classification', new FieldOption()
    //   .setInclude('isPortfolioSellingModuleEnabled')
    //   .setUploadState()),
    
    signature: new Field('Signature__c', 'signature', new FieldOption()
      .setUploadState()),
    
    signatureDate: new Field('Signature_taken__c', 'signatureDate', new FieldOption()
      .setUploadState()),
    
    callWithIPad: new Field('CallWithIPad__c', 'callWithIPad', new FieldOption()
      .setUploadState()),
    
    realCallDuration: new Field('RealCallDuration__c', 'realCallDuration', new FieldOption()
      .setUploadState()),
    
    patientSupportProgram: new Field('Patient_Support_Program__c', 'patientSupportProgram', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    portfolioFeedback: new Field('Portfolio_Feedback__c', 'portfolioFeedback', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    patientSupportProgramComments: new Field('Patient_Support_Program_Comments__c', 'patientSupportProgramComments', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    fullPortfolioPresentationReminder: new Field('Full_Portfolio_Presentation_Reminder__c', 'fullPortfolioPresentationReminder', new FieldOption()
      .setInclude('isPortfolioSellingModuleEnabled')
      .setUploadState()),
    
    userFirstName: new Field('User__r.FirstName', 'userFirstName', new FieldOption()),
    
    userLastName: new Field('User__r.LastName', 'userLastName', new FieldOption()),
    
    remoteContactFirstName: new Field('Contact1__r.FirstName', 'remoteContactFirstName', new FieldOption()),
    
    remoteContactLastName: new Field('Contact1__r.LastName', 'remoteContactLastName', new FieldOption()),
    
    contactRecordType: new Field('Contact1__r.Account.RecordType.Name', 'contactRecordType', new FieldOption()),

    indCustomerRecordType: new Field('Customer__r.RecordType.Name', 'indCustomerRecordType', new FieldOption()
      .setInclude('isIndiaUser')),

    remoteCustomerName: new Field('Customer__r.Name', 'remoteCustomerName', new FieldOption()
      .setInclude('isIndiaUser')),

    remoteBU: new Field('Customer__r.BU__c', 'remoteBU', new FieldOption()
      .setInclude('isIndiaUser')),

    indCustomerBU: new Field('', 'indCustomerBU', new FieldOption()
      .setInclude('isIndiaUser')),
    
    contactFirstName: new Field('', 'contactFirstName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    contactLastName: new Field('', 'contactLastName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    remoteOrganizationName: new Field('Organisation__r.Name', 'remoteOrganizationName', new FieldOption()),
    
    organizationName: new Field('', 'organizationName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    organizationCity: new Field('Organisation__r.BillingCity', 'organizationCity', new FieldOption()),
    
    organizationAddress: new Field('Organisation__r.BillingStreet', 'organizationAddress', new FieldOption()),
    
    atCalls: new Field('', 'atCalls', new FieldOption()
      .setIndexWithType('string')),
    
    typeOfVisit: new Field('Type_of_visit__c', 'typeOfVisit', new FieldOption()
      .setUploadState()),
    
    isSandbox: new Field('', 'isSandbox', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
    
    clmToolId: new Field('CLM_Tool_Id__c', 'clmToolId', new FieldOption()
      .setUploadState()),
    
    managerComments: new Field('Manager_Comments__c', 'managerComments', new FieldOption()),
    
    jointVisitParticipants: new Field('Joint_Visit_Participants__c', 'jointVisitParticipants', new FieldOption()
      .setUploadState()),
    
    status: new Field('Call_Report_Status__c', 'status', new FieldOption()
      .setUploadState()),
    
    isCoachingVisitActivities: new Field('Is_Coaching_Visit_Activities__c', 'isCoachingVisitActivities', new FieldOption()
      .setUploadState()),
    
    isInviteToEvent: new Field('Is_Invite_To_Event__c', 'isInviteToEvent', new FieldOption()
      .setUploadState()),
    
    isOneToOneCalls: new Field('Is_One_To_One_Calls__c', 'isOneToOneCalls', new FieldOption()
      .setUploadState()),
    
    convertedFromAppointment: new Field('Is_Converted_From_Appointment__c', 'convertedFromAppointment', new FieldOption()
      .setUploadState()),
    
    customerStatus: new Field('Customer_Status__c', 'customerStatus', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
    
    cnTypeOfVisit: new Field('CN_Type_of_visit__c', 'cnTypeOfVisit', new FieldOption()
      .setInclude('isChinaUser')
      .setUploadState()),
    
    cnPurposeOfVisit: new Field('CN_Purpose_of_visit__c', 'cnPurposeOfVisit', new FieldOption()
      .setInclude('isChinaUser')
      .setUploadState()),
    
    contactSpecialty: new Field('', 'contactSpecialty', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    contactPriority: new Field('', 'contactPriority', new FieldOption()
      .setIndexWithType('string')
      .setSearchState())
    
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(CallReportScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(CallReportScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(CallReportScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(CallReportScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(CallReportScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(CallReportScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(CallReportScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(CallReportScheme.fields);
  }
}
