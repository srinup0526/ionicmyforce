import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class OrderScheme {
  static readonly table: string = 'OrderManagement';
  static readonly sfdcTable: string = 'Order__c';
  static readonly description: string = 'OrderManagement';

  static readonly EDITABLE_STATUSES: Array<string> = ['Draft', 'Rejected'];
  static readonly APPROVAL_STATUS_DRAFT: string = 'Draft';
  static readonly APPROVAL_STATUS_SUBMITTED_OFFLINE: string = 'Submitted Offline';
  static readonly APPROVAL_STATUS_SENT_DISTRIBUTOR: string = 'Sent to Distributor';
  static readonly APPROVAL_STATUS_RECALLED: string = 'Recalled';
  static readonly APPROVAL_STATUS_SUBMITTED: string = 'Submitted';
  static readonly APPROVAL_STATUS_PENDING: string = 'Pending';
  static readonly APPROVAL_STATUS_VALIDATED: string = 'Validated';
  static readonly APPROVAL_STATUS_APPROVED: string = 'Approved';
  static readonly APPROVAL_STATUS_REJECTED: string = 'Rejected';
  static readonly APPROVAL_STATUS_FLM: string = 'Awaiting FLM Approval';
  static readonly APPROVAL_STATUS_NSM: string = 'Awaiting PM / NSM Approval';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    accountSfId: new Field('Account__c', 'accountSfId', new FieldOption()
      .setUploadState()),

    remoteAccountName: new Field('Account__r.Name', 'remoteAccountName', new FieldOption()),
  
    remoteAccountRecordType: new Field('Account__r.RecordType.Name', 'remoteAccountRecordType', new FieldOption()),
    
    // externalAccountId: new Field('Account__r.externalId', 'externalAccountId', new FieldOption()),

    specialprice: new Field('Special_Price__c', 'specialprice', new FieldOption()
      .setUploadState()),

    accountName: new Field('', 'accountName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    status: new Field('Status__c', 'status', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    billindAddress: new Field('Billing_Address__c', 'billindAddress', new FieldOption()
      .setIndexWithType('string')),

    dateTimeCreated: new Field('Date_Time_Created__c', 'dateTimeCreated', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),

    dateTimeSentToDistributor: new Field('Date_Time_Sent_to_Distributor__c', 'dateTimeSentToDistributor', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    notes: new Field('Notes__c', 'notes', new FieldOption()
      .setUploadState()),

    orderSalesAmount: new Field('Order_Sales_Amount__c', 'orderSalesAmount', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),

    orderTotalQuantity: new Field('Order_Total_Quantity__c', 'orderTotalQuantity', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),

    shippingAddress: new Field('Shipping_Address__c', 'shippingAddress', new FieldOption()
      .setIndexWithType('string')),

    signature: new Field('Signature__c', 'signature', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    recordTypeId: new Field('RecordTypeId', 'recordTypeId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),

    recordType: new Field('', 'recordType', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    remoteRecordType: new Field('RecordType.Name', 'remoteRecordType', new FieldOption()),

    sampleno: new Field('Sample_No__c', 'sampleno', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    pono: new Field('PO_No__c', 'pono', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    newAddress: new Field('New_Address__c', 'newAddress', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    remarks: new Field('Remarks__c', 'remarks', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    noncustomer: new Field('Non_Customer__c', 'noncustomer', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState())
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(OrderScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(OrderScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(OrderScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(OrderScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(OrderScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(OrderScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(OrderScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(OrderScheme.fields);
  }
}
