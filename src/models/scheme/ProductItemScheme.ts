import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ProductItemScheme {
  static readonly table = 'ProductItem';
  static readonly sfdcTable = 'Product_Items__c';
  static readonly description = 'Product Item';

  static readonly PRODUCT_TYPE = 'Product';
 

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    description: new Field('Description__c', 'description', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
    .setSearchState()
    .setIndexWithType('string')),

    imsSkuCode: new Field('IMS_sku_code__c', 'imsSkuCode', new FieldOption()),

    parallelImport: new Field('Parallel_Import__c', 'parallelImport', new FieldOption()),

    // sales1MonthAgo: new Field('Sales_1_month_ago__c', 'sales1MonthAgo', new FieldOption()),

    // sales2MonthAgo: new Field('Sales_2_months_ago__c', 'sales2MonthAgo', new FieldOption()),

    // sales3MonthAgo: new Field('Sales_3_months_ago__c', 'sales3MonthAgo', new FieldOption()),

    // sales4MonthAgo: new Field('Sales_4_months_ago__c', 'sales4MonthAgo', new FieldOption()),

    // salesYTDC: new Field('Sales_YTD__c', 'salesYTDC', new FieldOption()),

    productBrandName: new Field('Product_Brand_Name__c', 'productBrandName', new FieldOption()),

    productDetailCode: new Field('Product_Detail_Code__c', 'productDetailCode', new FieldOption()),

    price: new Field('Unit_Price__c', 'price', new FieldOption()
      .setInclude('isIndiaUser')),

    productTypeDetect: new Field('Product_Type_detect__c', 'productTypeDetect', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    productAtcClass: new Field('Product_ATC_Class__c', 'productAtcClass', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setInclude('isIndiaUser')),

    productDivision: new Field('Product_Division__c', 'productDivision', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setInclude('isIndiaUser')),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(ProductItemScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ProductItemScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ProductItemScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(ProductItemScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(ProductItemScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(ProductItemScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(ProductItemScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(ProductItemScheme.fields);
  }
}
