import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class PEAttendeeScheme {
  static readonly table: string = 'PEAttendee';
  static readonly sfdcTable: string = 'Luncheon_Attendances__c';
  static readonly description: string = 'PE Attendee';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    pharmaEventSfId: new Field('Pharma_Event__c', 'pharmaEventSfId', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()
    .setSearchState()),

    attendeeSfId: new Field('Attendee__c', 'attendeeSfId', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()
    .setSearchState()),

    clmToolId: new Field('CLM_Tool_Id__c', 'clmToolId', new FieldOption()
    .setUploadState()),

    specialty: new Field('Specialty_Therapeutic_Area__c', 'specialty', new FieldOption()
    .setUploadState()),

    organization: new Field('Hospital_Name__c', 'organization', new FieldOption()
    .setUploadState()),

    organizationName: new Field('Hospital_Name__r.Name', 'organizationName', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()),

    organizationCity: new Field('Hospital_Name__r.BillingCity', 'organizationCity', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()),

    organizationPhone: new Field('Hospital_Name__r.Phone', 'organizationPhone', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()),

    organizationCountry: new Field('Hospital_Name__r.BillingCountry', 'organizationCountry', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()),


    organizationAddress: new Field('Hospital_Name__r.BillingStreet', 'organizationAddress', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()),

    typeOfHospital: new Field('Type_of_Hospital__c', 'typeOfHospital', new FieldOption()
    .setUploadState()),

    signature: new Field('Signature__c', 'signature', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()),

    attHadLunch: new Field('Whether_attendee_had_lunch__c', 'attHadLunch', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()),

    noncustomer: new Field('Non_Customer_data__c', 'noncustomer', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()),

    confirmHCPCount: new Field('ConfirmHCPCount__c', 'confirmHCPCount', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    nonOrganization: new Field('Non_Organsation__c', 'nonOrganization', new FieldOption()
    .setUploadState())  

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(PEAttendeeScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(PEAttendeeScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(PEAttendeeScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(PEAttendeeScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(PEAttendeeScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(PEAttendeeScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(PEAttendeeScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(PEAttendeeScheme.fields);
  }
  organizationNameAndAddress(){
    "${this.organizationName ? ''} <br/> ${this.organizationAddress ? ''} ${this.organizationCity ? ''}"
  }
  organizationNameWithAddress(){
    "${this.organizationName ? ''} - ${this.organizationAddress ? ''} ${this.organizationCity ? ''}"
  }

  getContact(){
    // unless this.contact
    //   const ContactsCollection = require 'models/bll/contacts-collection'
    //   contactsCollection = new ContactsCollection
    //   contactsCollection.fetchEntityById(this.attendeeSfId);
    // else
    //   $.when(this.contact)
  }

  getOrganization(){
//   unless @organizationName
//     organizationCollection = require 'models/bll/organizations-collection'
//     organizationCollection = new organizationCollection
//     organizationCollection.fetchEntityById @organization
//   else
//     $.when @organizationName
  }
}
