import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class TotScheme {
  static readonly table: string = 'Tot';
  static readonly sfdcTable: string = 'Time_off_Territory__c';
  static readonly description: string = 'Time-Off-Territory';
  static readonly TYPE_OPEN: string = 'Open';
  static readonly TYPE_NONE: string = 'None';
  static readonly TYPE_CLOSED: string = 'Closed';
  static readonly TYPE_SUBMIT: string = 'Submit';
  static readonly TYPE_Title: string = 'TOT';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    userSfId: new Field('OwnerId', 'userSfId', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

      createdOffline: new Field('Created_Offline__c', 'createdOffline', new FieldOption()
      .setSearchState()
      .setUploadState()),

      userLastName: new Field('Owner.LastName', 'userLastName', new FieldOption()
      .setSearchState()),

      userFirstName: new Field('Owner.FirstName', 'userFirstName', new FieldOption()
      .setSearchState()),

      allDay: new Field('All_Day__c', 'allDay', new FieldOption()
      .setUploadState()),

      startDate: new Field('Start_Date__c', 'startDate', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

      endDate: new Field('End_Date__c', 'endDate', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

      firstQuarterEvent: new Field('Type_First_Quarter__c', 'firstQuarterEvent', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

      secondQuarterEvent: new Field('Type_Second_Quarter__c', 'secondQuarterEvent', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

      thirdQuarterEvent: new Field('Type_Third_Quarter__c', 'thirdQuarterEvent', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

      fourthQuarterEvent: new Field('Type_Fourth_Quarter__c', 'fourthQuarterEvent', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

      type: new Field('Type__c', 'type', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

      description: new Field('Description__c', 'description', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

      isSubmittedForApproval: new Field('IsSubmittedForApproval__c', 'isSubmittedForApproval', new FieldOption()
      .setIndexWithType('string')),

      clmToolId: new Field('CLM_Tool_Id__c', 'clmToolId', new FieldOption()
      .setUploadState()),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(TotScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(TotScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(TotScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(TotScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(TotScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(TotScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(TotScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(TotScheme.fields);
  }

}
