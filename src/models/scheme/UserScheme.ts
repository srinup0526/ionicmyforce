import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class UserScheme {
  static readonly table: string = 'User';
  static readonly sfdcTable: string = 'User';
  static readonly description: string = 'User';

  static readonly CHINA_CURRENCY_ISO_CODE: string = 'CNY';
  static readonly BULGARIA_CURRENCY_ISO_CODE: string = 'BGN';

  static readonly PROFILES_GROUPS: {
    MEDICAL_REP: 'Medical Rep'
    FLM: 'Frontline Manager',
    NSM: 'National Sales Manager'
  };

  static activeRow: string = null;
  static activeRows: Array<string> = [];

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    email: new Field('Email', 'email', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    name: new Field('Name', 'name', new FieldOption().setIndexWithType('string')),

    managerId: new Field('Manager.Id', 'managerId', new FieldOption().setIndexWithType('string')),

    managerFirstName: new Field('Manager.FirstName', 'managerFirstName', new FieldOption()),

    managerLastName: new Field('Manager.LastName', 'managerLastName', new FieldOption()),

    firstName: new Field('FirstName', 'firstName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),

    lastName: new Field('LastName', 'lastName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),

    currency: new Field('DefaultCurrencyIsoCode', 'currency', new FieldOption().setIndexWithType('string')),

    businessUnit: new Field('Business_Unit__c', 'businessUnit', new FieldOption()),

    userRoleId: new Field('UserRoleId', 'userRoleId', new FieldOption().setIndexWithType('string')),

    jointVisitType: new Field('JointVisitType__c', 'jointVisitType', new FieldOption().setIndexWithType('string')),

    callReportValidationExcempted: new Field('Is_Exempted_in_Call_Report_Validation__c', 'callReportValidationExcempted', new FieldOption()),

    pinCode: new Field('ATC_Class__c', 'pinCode', new FieldOption().setIndexWithType('string')),

    targetPercentWithAvg: new Field('Average_and_Achievement_Percentage__c', 'targetPercentWithAvg', new FieldOption()
      .setIndexWithType('string')),

    isActive: new Field('IsActive', 'isActive', new FieldOption().setIndexWithType('string')),

    typeofquery: new Field('Type_of_Medical_Query__c', 'typeofquery', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    DayOff1: new Field('Day_Off_1__c', 'DayOff1', new FieldOption()),

    DayOff2: new Field('Day_Off_2__c', 'DayOff2', new FieldOption()),

    profileId: new Field('', 'profileId', new FieldOption()),

    profileName: new Field('', 'profileName', new FieldOption()),

    atcClass: new Field('', 'atcClass', new FieldOption()),

    isLocked: new Field('', 'isLocked', new FieldOption()),

    pinAttemptsCnt: new Field('', 'pinAttemptsCnt', new FieldOption()),

    userCountry: new Field('', 'userCountry', new FieldOption()
      .setIndexWithType('string')
      .setSearchState())
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(UserScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(UserScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(UserScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(UserScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(UserScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(UserScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(UserScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(UserScheme.fields);
  }
}
