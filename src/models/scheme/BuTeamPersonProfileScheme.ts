import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class BuTeamPersonProfileScheme {
  static readonly table: string = 'BuTeamPersonProfile';
  static readonly sfdcTable: string = 'BU_Team_Person_Profile__c';
  static readonly description: string = 'Bu Team Person Profile';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    organizationSfid: new Field('Account__c', 'organizationSfid', new FieldOption()
      .setIndexWithType('string')),

    businessUnit: new Field('Business_Unit__c', 'businessUnit', new FieldOption()
      .setIndexWithType('string')),

    priority: new Field('Priority__c', 'priority', new FieldOption()
      .setIndexWithType('string')),

    specialty: new Field('Specialty__c', 'specialty', new FieldOption()
      .setIndexWithType('string')),

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(BuTeamPersonProfileScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(BuTeamPersonProfileScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(BuTeamPersonProfileScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(BuTeamPersonProfileScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(BuTeamPersonProfileScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(BuTeamPersonProfileScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(BuTeamPersonProfileScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(BuTeamPersonProfileScheme.fields);
  }
}
