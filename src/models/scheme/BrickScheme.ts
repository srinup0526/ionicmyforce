import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class BrickScheme {
  static readonly table: string = 'Brick';
  static readonly sfdcTable: string = 'Account';
  static readonly description: string = 'Brick';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    shortDescription: new Field('Short_Description__c', 'shortDescription', new FieldOption()
      .setIndexWithType('string')),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(BrickScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(BrickScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(BrickScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(BrickScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(BrickScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(BrickScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(BrickScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(BrickScheme.fields);
  }
}
