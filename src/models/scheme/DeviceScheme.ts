import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class DeviceScheme {
  static readonly table: string = 'Device';
  static readonly sfdcTable: string = 'Mobile_Devices__c';
  static readonly description: string = 'Device';

  static fields = {
      id: new Field('Id', 'id', new FieldOption()),

      deviceId: new Field('Device_ID__c', 'deviceId', new FieldOption()
        .setIndexWithType('string')
        .setSearchState()
        .setUploadState()),

      erased: new Field('Erased__c', 'erased', new FieldOption().setUploadState()),

      requestErase: new Field('Request_Erase__c', 'requestErase', new FieldOption()),

      lastSyncronisation: new Field('Last_Syncronization__c', 'lastSyncronisation', new FieldOption().setUploadState()),

      lastUserSfid: new Field('Last_User__c', 'lastUserSfid', new FieldOption().setUploadState()),

      model: new Field('Model__c', 'model', new FieldOption().setUploadState()),

      osVersion: new Field('OS_Version__c', 'osVersion', new FieldOption().setUploadState()),

      version: new Field('Version__c', 'version', new FieldOption().setUploadState()),

      lastDebugLog: new Field('Last_Debug_Log__c', 'lastDebugLog', new FieldOption().setUploadState()),

      lastModifiedDate: new Field('LastModifiedDate', 'lastModifiedDate', new FieldOption().setIndexWithType('string')),

      lastSyncDate: new Field('', 'lastSyncDate', new FieldOption().setIndexWithType('string')),

      searchData: new Field('', 'searchData', new FieldOption().setIndexWithType('string'))
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(DeviceScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(DeviceScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(DeviceScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(DeviceScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(DeviceScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(DeviceScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(DeviceScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(DeviceScheme.fields);
  }
}
