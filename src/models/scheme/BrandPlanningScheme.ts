import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import {SchemeParser} from './SchemeParser';


export class BrandPlanningScheme {
  static readonly table: string = 'BrandPlanning';
  static readonly sfdcTable: string = 'BrandPlanning__c';
  static readonly description: string = 'BrandPlanning';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    //TODO for some reason reference have not accountId, so will find by contactId
    customer: new Field('Customer__r.Contact__c', 'customer', new FieldOption()
      .setIndexWithType('string')),

    year: new Field('BranMatrix_Cycle__r.Year__c', 'year', new FieldOption()
      .setIndexWithType('string')),

    cycle: new Field('BranMatrix_Cycle__r.Cycle__c', 'cycle', new FieldOption()
      .setIndexWithType('string')),

    status: new Field('BranMatrix_Cycle__r.Status__c', 'status', new FieldOption()
      .setIndexWithType('string')),

    focusBrand1: new Field('Focus_Brand_1__c', 'focusBrand1', new FieldOption()),

    focusBrand2: new Field('Focus_Brand_2__c', 'focusBrand2', new FieldOption()),

    focusBrand3: new Field('Focus_Brand_3__c', 'focusBrand3', new FieldOption()),

    focusBrand4: new Field('Focus_Brand_4__c', 'focusBrand4', new FieldOption()),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(BrandPlanningScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(BrandPlanningScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(BrandPlanningScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(BrandPlanningScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(BrandPlanningScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(BrandPlanningScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(BrandPlanningScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(BrandPlanningScheme.fields);
  }
}
