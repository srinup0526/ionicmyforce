import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import {SchemeParser} from './SchemeParser';


export class ProductSegmentationScheme {
  static readonly table: string = 'ProductSegmentation';
  static readonly sfdcTable: string = 'Product_Segmentation__c';
  static readonly description: string = 'Product Segmentation';
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
    
    accountId: new Field('Account__c', 'accountId', new FieldOption()
      .setIndexWithType('string')),
    
    isActive: new Field('isActive__c', 'isActive', new FieldOption()
      .setIndexWithType('string')),
    
    phProductId: new Field('PhProduct__c', 'phProductId', new FieldOption()),
    
    phProductName: new Field('PhProduct__r.Name', 'phProductName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),
    
    segmentation1: new Field('Segmentation_1__c', 'segmentation1', new FieldOption()
      .setIndexWithType('string')),
    
    segmentation2: new Field('Segmentation_2__c', 'segmentation2', new FieldOption()
      .setIndexWithType('string')),
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(ProductSegmentationScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ProductSegmentationScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ProductSegmentationScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(ProductSegmentationScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(ProductSegmentationScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(ProductSegmentationScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(ProductSegmentationScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(ProductSegmentationScheme.fields);
  }
}
