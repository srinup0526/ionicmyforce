import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class SalesPlanningListScheme {
  static readonly table: string = 'SalesPlanningList';
  static readonly sfdcTable: string = 'Sales_Planning_List__c';
  static readonly description: string = 'Sales Planning List';
  
  static readonly STATUS_ACTIVE: string = 'Active';
  

  
  static activeRow: string = null;
  static activeRows: Array<string> = [];
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
  
    class: new Field('Class__c', 'class', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    customer1: new Field('Customer1__c', 'customer1', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    doctorfirstname: new Field('Doctor_s_First_Name__c', 'doctorfirstname', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    doctorlasttname: new Field('Doctor_s_Last_Name__c', 'doctorlasttname', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),
  
    DoctorName: new Field('Doctor_s_Name__c', 'DoctorName', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),
  
    customer: new Field('Customer__c', 'customer', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()
    .setUploadState()),

    frequency: new Field('Frequency__c', 'frequency', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    infieldAct: new Field('In_Field_Activity__c', 'infieldAct', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    salesplanning: new Field('Sales_Planning__c', 'salesplanning', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()
    .setUploadState()),
  
    speciality: new Field('Speciality__c', 'speciality', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    totalOrderValue: new Field('Total_Orderd_Value__c', 'totalOrderValue', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    totalProductPlanned: new Field('Total_Product_Planned_del__c', 'totalProductPlanned', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    type: new Field('Type__c', 'type', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    view: new Field('View__c', 'view', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),
   
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(SalesPlanningListScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(SalesPlanningListScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(SalesPlanningListScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(SalesPlanningListScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(SalesPlanningListScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(SalesPlanningListScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(SalesPlanningListScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(SalesPlanningListScheme.fields);
  }
}
