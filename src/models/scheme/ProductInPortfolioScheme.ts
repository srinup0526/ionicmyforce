import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ProductInPortfolioScheme {
  static readonly table = 'ProductInPortfolio';
  static readonly sfdcTable = 'Product_in_Porfolio__c';
  static readonly description = 'Product In Portfolio';
 

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

   

    productSfId: new Field('Pharma_Product__c', 'productSfId', new FieldOption()),

    portfolioSfId: new Field('Product_Portfolio__c', 'portfolioSfId', new FieldOption()),

    startDate: new Field('Start_Date__c', 'startDate', new FieldOption()),

    endDate: new Field('End_Date__c', 'endDate', new FieldOption()),

    brandName: new Field('Global_Brand_Name__c', 'brandName', new FieldOption()),

   
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(ProductInPortfolioScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ProductInPortfolioScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ProductInPortfolioScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(ProductInPortfolioScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(ProductInPortfolioScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(ProductInPortfolioScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(ProductInPortfolioScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(ProductInPortfolioScheme.fields);
  }
}
