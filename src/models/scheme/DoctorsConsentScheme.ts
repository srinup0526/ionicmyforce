import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class DoctorsConsentScheme {
  static readonly table: string = 'DoctorsConsent';
  static readonly sfdcTable: string = 'Consent__c';
  static readonly description: string = 'DoctorsConsent';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    firstname: new Field('First_Name__c', 'firstname', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    lastname: new Field('Last_Name__c', 'lastname', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    email: new Field('E_mail_address__c', 'email', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    cellphonenumber: new Field('Cell_phone_number__c', 'cellphonenumber', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    doctorid: new Field('Doctor_ID_code__c', 'doctorid', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    organization: new Field('Name_of_organization__c', 'organization', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    organizationid: new Field('Organisation_ID__c', 'organizationid', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    signature: new Field('Signature__c', 'signature', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    speciality: new Field('Specialty__c', 'speciality', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    brick: new Field('Brick_Name__c', 'brick', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    nonOrganization: new Field('Non_Organisation__c', 'nonOrganization', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    country: new Field('Country__c', 'country', new FieldOption()
      .setIndexWithType('string'))
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(DoctorsConsentScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(DoctorsConsentScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(DoctorsConsentScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(DoctorsConsentScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(DoctorsConsentScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(DoctorsConsentScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(DoctorsConsentScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(DoctorsConsentScheme.fields);
  }
}
