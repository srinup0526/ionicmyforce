import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class PatchScheme {
  static readonly table: string = 'Patch';
  static readonly sfdcTable: string = 'Patch__c';
  static readonly description: string = 'Patch';
  static readonly TYPE_Title: string = 'Patch';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(PatchScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(PatchScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(PatchScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(PatchScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(PatchScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(PatchScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(PatchScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(PatchScheme.fields);
  }
}
