import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class SurveyQuestionnaireScheme {
  static readonly table: string = 'SurveyQuestionnaire';
  static readonly sfdcTable: string = 'SurveyQuestionnaire__c';
  static readonly description: string = 'SurveyQuestionnaire';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    question: new Field('Question__c', 'question', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    picklistvalue: new Field('Picklist_Value__c', 'picklistvalue', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    type: new Field('Type__c', 'type', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    surveyid: new Field('Survey__c', 'surveyid', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    sortorder: new Field('Sort_Order__c', 'sortorder', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    noncustomervalue: new Field('Non_Customer__c', 'noncustomervalue', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    isthisquestionrequire: new Field('Required__c', 'isthisquestionrequire', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),


  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(SurveyQuestionnaireScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(SurveyQuestionnaireScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(SurveyQuestionnaireScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(SurveyQuestionnaireScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(SurveyQuestionnaireScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(SurveyQuestionnaireScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(SurveyQuestionnaireScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(SurveyQuestionnaireScheme.fields);
  }
}
