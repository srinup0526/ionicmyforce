import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class RedFlagScheme {
  static readonly table:string = 'RedFlag';
  static readonly sfdcTable:string = 'Red_Flag__c';
  static readonly description:string = 'RedFlag';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),


    redFlagType: new Field('RedFlagType__c', 'redFlagType', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      customerName: new Field('CustomerName__c', 'customerName', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      email: new Field('Contact_Email__c', 'email', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      contactNumber: new Field('Contact_Number__c', 'contactNumber', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      description: new Field('Description__c', 'description', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
     

      CallReportId: new Field('CallReportId__c', 'CallReportId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      photo: new Field('Photo__c', 'photo', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(RedFlagScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(RedFlagScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(RedFlagScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(RedFlagScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(RedFlagScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(RedFlagScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(RedFlagScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(RedFlagScheme.fields);
  }

}
