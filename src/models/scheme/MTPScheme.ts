import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class MTPScheme {
  static readonly table: string = 'MTP';
  static readonly sfdcTable: string = 'MTP__c';
  static readonly description: string = 'MTP';

  static monthValuesList: Array<string> = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  static editableStatuses: Array<string> = ['Draft', 'Rejected'];

  static allowCloningStatuses: Array<string> = ['Draft', 'Rejected', 'Approved'];

  static readonly APPROVAL_STATUS_DRAFT: string= 'Draft';

  static readonly APPROVAL_STATUS_SUBMITTED_OFFLINE: string= 'Submitted Offline';
  static readonly APPROVAL_STATUS_RECALLED: string= 'Recalled';
  static readonly APPROVAL_STATUS_PENDING: string= 'Pending';
  static readonly APPROVAL_STATUS_VALIDATED: string= 'Validated';
  static readonly APPROVAL_STATUS_APPROVED: string= 'Approved';
  static readonly APPROVAL_STATUS_REJECTED: string= 'Rejected';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    __locally_created__: new Field('', '__locally_created__', new FieldOption()
      .setIndexWithType('string')),

    startDate: new Field('Start_Date__c', 'startDate', new FieldOption()
      .setIndexWithType('string')),

    endDate: new Field('End_Date__c', 'endDate', new FieldOption()
      .setIndexWithType('string')),

    createdDate: new Field('CreatedDate', 'createdDate', new FieldOption()
      .setIndexWithType('string')),

    monthYearKey: new Field('', 'monthYearKey', new FieldOption()
      .setIndexWithType('string')),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')),

    month: new Field('Month__c', 'month', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    year: new Field('Year__c', 'year', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    status: new Field('Status__c', 'status', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    medRep: new Field('Med_Rep__c', 'medRep', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    currency: new Field('CurrencyIsoCode', 'currency', new FieldOption()
      .setIndexWithType('string'))

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(MTPScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(MTPScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(MTPScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(MTPScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(MTPScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(MTPScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(MTPScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(MTPScheme.fields);
  }
}
