import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class MedicalCommentScheme {
  static readonly table: string = 'MedicalComment';
  static readonly sfdcTable: string = 'Medical_Comment__c';
  static readonly description: string = 'Medical Comment';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    commentDescription: new Field('Comment_Description__c', 'commentDescription', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    commentCreatedDateTime: new Field('Creation_Date_Time__c', 'commentCreatedDateTime', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    commentFeedback: new Field('Feedback__c', 'commentFeedback', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    mqId: new Field('Medical_Query__c', 'mqId', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    createdBy: new Field('CreatedById', 'createdBy', new FieldOption()
      .setIndexWithType('string')),

    remoteCreatedByName: new Field('CreatedBy.Name', 'remoteCreatedByName', new FieldOption()
      .setIndexWithType('string')),

    createdUserName: new Field('Created_By_Name__c', 'createdUserName', new FieldOption()
      .setIndexWithType('string'))
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(MedicalCommentScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(MedicalCommentScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(MedicalCommentScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(MedicalCommentScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(MedicalCommentScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(MedicalCommentScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(MedicalCommentScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(MedicalCommentScheme.fields);
  }
}
