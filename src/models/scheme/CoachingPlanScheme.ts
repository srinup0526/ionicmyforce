import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class CoachingPlanScheme {
  static readonly table: string = 'CoachingPlan';
  static readonly sfdcTable: string = 'Development_Plan_CEE__c';
  static readonly description: string = 'Coaching Plan';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    month: new Field('Month__c', 'month', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    year: new Field('Year__c', 'year', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    employeeCode: new Field('Employee_Code__c', 'employeeCode', new FieldOption()
      .setIndexWithType('string')),

    employeeName: new Field('Employee_Name__c', 'employeeName', new FieldOption()
      .setIndexWithType('string')),

    division: new Field('Division__c', 'division', new FieldOption()
      .setIndexWithType('string')),

    designation: new Field('Designation__c', 'designation', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    dateofjoining: new Field('Date_Of_Joining__c', 'dateofjoining', new FieldOption()
      .setIndexWithType('string')),

    filledBy: new Field('Createdby.Name', 'filledBy', new FieldOption()),

    coachingDate: new Field('Date_of_Filing_the_Coaching_Plan__c', 'coachingDate', new FieldOption()
      .setIndexWithType('string'))

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(CoachingPlanScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(CoachingPlanScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(CoachingPlanScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(CoachingPlanScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(CoachingPlanScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(CoachingPlanScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(CoachingPlanScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(CoachingPlanScheme.fields);
  }
}
