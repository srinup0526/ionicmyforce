import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class FaqScheme {
  static readonly table: string = 'FAQ';
  static readonly sfdcTable: string = 'FAQ__c';
  static readonly description: string = 'FAQ';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    answer: new Field('Answer__c', 'answer', new FieldOption()),

    question: new Field('Question__c', 'question', new FieldOption()),

    order: new Field('Order__c', 'order', new FieldOption()
      .setIndexWithType('string')),

    currencyIsoCode: new Field('CurrencyIsoCode', 'currencyIsoCode', new FieldOption()
      .setIndexWithType('string')),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(FaqScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(FaqScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(FaqScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(FaqScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(FaqScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(FaqScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(FaqScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(FaqScheme.fields);
  }
}
