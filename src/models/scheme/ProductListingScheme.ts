import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ProductListingScheme {
  static readonly table = 'ProductListing';
  static readonly sfdcTable = 'ProductListing__c';
  static readonly description = 'Product Listing';
  

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    accountId: new Field('Account__c', 'accountId', new FieldOption()
      .setIndexWithType('string')),

      isActive: new Field('isActive__c', 'isActive', new FieldOption()
      .setIndexWithType('string')),

      phProductId: new Field('PharmaProduct__c', 'phProductId', new FieldOption()),
      phProductName: new Field('PharmaProduct__r.Name', 'phProductName', new FieldOption()),
      status: new Field('Status__c', 'status', new FieldOption()),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(ProductListingScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ProductListingScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ProductListingScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(ProductListingScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(ProductListingScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(ProductListingScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(ProductListingScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(ProductListingScheme.fields);
  }
}
