import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class TourPlanningEntityScheme {
  static readonly description = 'Tour planning entity';

  static readonly visitOrderNumber= 0;
  static readonly visitStartTime:any;
  static readonly visitEndTime:any;
  static readonly isChecked:boolean=false;
  static fields = {};
}
