import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class AttachmentScheme {
  static readonly table: string = 'Attachment';
  static readonly sfdcTable: string = 'Attachment';
  static readonly description: string = 'Attachment';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    body: new Field('Body', 'body', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    typeOfAttachment: new Field('ContentType', 'typeOfAttachment', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    description: new Field('Description', 'description', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    parentId: new Field('ParentId', 'parentId', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(AttachmentScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(AttachmentScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(AttachmentScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(AttachmentScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(AttachmentScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(AttachmentScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(AttachmentScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(AttachmentScheme.fields);
  }
}
