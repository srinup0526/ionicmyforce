import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class PEEventExpenseScheme {
  static readonly table: string = 'PEEventExpense';
  static readonly sfdcTable: string = 'Event_Costs__c';
  static readonly description: string = 'PE Event Expense';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()),

    recordTypeId: new Field('RecordTypeId', 'recordTypeId', new FieldOption()
    .setUploadState()),

    pharmaEventSfId: new Field('Pharma_Event__c', 'pharmaEventSfId', new FieldOption()
    .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),

      costOfEachMeal: new Field('Cost_of_each_meal__c', 'costOfEachMeal', new FieldOption()
      .setUploadState()),

      costOfEachMealUSD: new Field('Cost_of_Meal_per_person_USD__c', 'costOfEachMealUSD', new FieldOption()
      .setUploadState()),

      noOfHcps: new Field('No_of_HCPs__c', 'noOfHcps', new FieldOption()
      .setUploadState()),

      noOfMylanStaff: new Field('No_of_Mylan_staff__c', 'noOfMylanStaff', new FieldOption()
      .setUploadState()),

      totalNoOfAttendees: new Field('Total_of_Attendees__c', 'totalNoOfAttendees', new FieldOption()),

      totalCost: new Field('Total_Cost_In_Local__c', 'totalCost', new FieldOption()
      .setUploadState()),


      totalCostUSD: new Field('Total_Cost_USD__c', 'totalCostUSD', new FieldOption()
      .setUploadState()),

      actualOtherExpensesText1: new Field('Actual_OE_Text_1__c', 'actualOtherExpensesText1', new FieldOption()
      .setSearchState()
      .setUploadState()),

      actualOtherExpensesText2: new Field('Actual_OE_Text_2__c', 'actualOtherExpensesText2', new FieldOption()
      .setSearchState()
      .setUploadState()),

      actualOtherExpensesText3: new Field('Actual_OE_Text_3__c', 'actualOtherExpensesText3', new FieldOption()
      .setSearchState()
      .setUploadState()),

      actualOtherExpensesText4: new Field('Actual_OE_Text_4__c', 'actualOtherExpensesText4', new FieldOption()
      .setSearchState()
      .setUploadState()),

      actualOtherExpensesText5: new Field('Actual_OE_Text_5__c', 'actualOtherExpensesText5', new FieldOption()
      .setSearchState()
      .setSearchState()),

      actualOtherExpensesLC1: new Field('Actual_OE_Local_1__c', 'actualOtherExpensesLC1', new FieldOption()
      .setUploadState()),

      actualOtherExpensesLC2: new Field('Actual_OE_Local_2__c', 'actualOtherExpensesLC2', new FieldOption()
      .setUploadState()),

      actualOtherExpensesLC3: new Field('Actual_OE_Local_3__c', 'actualOtherExpensesLC3', new FieldOption()
      .setUploadState()),

      actualOtherExpensesLC4: new Field('Actual_OE_Local_4__c', 'actualOtherExpensesLC4', new FieldOption()
      .setUploadState()),

      actualOtherExpensesLC5: new Field('Actual_OE_Local_5__c', 'actualOtherExpensesLC5', new FieldOption()
      .setUploadState()),

      actualOtherExpensesUSD1: new Field('Actual_OE_USD_1__c', 'actualOtherExpensesUSD1', new FieldOption()
      .setUploadState()),

      actualOtherExpensesUSD2: new Field('Actual_OE_USD_2__c', 'actualOtherExpensesUSD2', new FieldOption()
      .setUploadState()),

      actualOtherExpensesUSD3: new Field('Actual_OE_USD_3__c', 'actualOtherExpensesUSD3', new FieldOption()
      .setUploadState()),

      actualOtherExpensesUSD4: new Field('Actual_OE_USD_4__c', 'actualOtherExpensesUSD4', new FieldOption()
      .setUploadState()),

      actualOtherExpensesUSD5: new Field('Actual_OE_USD_5__c', 'actualOtherExpensesUSD5', new FieldOption()
      .setUploadState()),

      actualTotalCostofEventLC: new Field('Total_Cost_of_Event_Local_Currency__c', 'actualTotalCostofEventLC', new FieldOption()
      .setUploadState()),

      actualTotalCostofEventUSD: new Field('Total_Cost_of_Event_USD__c', 'actualTotalCostofEventUSD', new FieldOption()
      .setUploadState()),

      totalNumberOfHCPs: new Field('totalNumberOfHCPs__c', 'totalNumberOfHCPs', new FieldOption()
      .setUploadState()),

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(PEEventExpenseScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(PEEventExpenseScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(PEEventExpenseScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(PEEventExpenseScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(PEEventExpenseScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(PEEventExpenseScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(PEEventExpenseScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(PEEventExpenseScheme.fields);
  }
}
