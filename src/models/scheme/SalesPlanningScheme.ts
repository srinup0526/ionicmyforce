import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class SalesPlanningScheme {
  static readonly table: string = 'SalesPlanning';
  static readonly sfdcTable: string = 'Sales_Planning__c';
  static readonly description: string = 'Sales Planning';
  
  static readonly STATUS_ACTIVE: string = 'Active';

  static readonly APPROVAL_STATUS_SUBMITTED_OFFLINE: string = 'Submitted Offline';
  static readonly APPROVAL_STATUS_SUBMITTED: string = 'Submitted';
  

  

  
  static activeRow: string = null;
  static activeRows: Array<string> = [];
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption().setIndexWithType('string').setSearchState()),
  
    month: new Field('Month__c', 'month', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
  
    status: new Field('Status__c', 'status', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    view: new Field('View__c', 'view', new FieldOption()
      .setIndexWithType('string')),

    year: new Field('Year__c', 'year', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    createdBy: new Field('CreatedBy.Name', 'createdBy', new FieldOption()
      .setIndexWithType('string')),
 
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(SalesPlanningScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(SalesPlanningScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(SalesPlanningScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(SalesPlanningScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(SalesPlanningScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(SalesPlanningScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(SalesPlanningScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(SalesPlanningScheme.fields);
  }
}
