import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ContactScheme {
  static readonly table: string = 'Contact';
  static readonly sfdcTable: string = 'Contact';
  static readonly description: string = 'Contact';
  
  static readonly STATUS_ACTIVE: string = 'Active';
  
  static readonly SPECIALTY_FIELD_NAME: string = 'specialty';
  static readonly BU_SPECIALTY_FIELD_NAME: string = 'buSpecialty';
  static readonly PRIORITY_FIELD_NAME: string = 'priority';
  static readonly SUBTYPE_FIELD_NAME: string = 'Person_Type__c';
  
  static activeRow: string = null;
  static activeRows: Array<string> = [];
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
  
    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    firstName: new Field('FirstName', 'firstName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    lastName: new Field('LastName', 'lastName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    jobTitle: new Field('C_Job_Title__c', 'jobTitle', new FieldOption()
      .setToLabelState()),
  
    accountId: new Field('Account.Id', 'accountId', new FieldOption()
      .setIndexWithType('string')),
  
    recordType: new Field('Account.RecordType.Name', 'recordType', new FieldOption()
      .setIndexWithType('string')),
  
    subtype: new Field('Person_Type__c', 'subtype', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    status: new Field('Account.Status__c', 'status', new FieldOption()),
  
    organizationSfId: new Field('AccountId', 'organizationSfId', new FieldOption()),

    organizationName: new Field('', 'organizationName', new FieldOption()),

    remoteOrganizationName: new Field('Account.Name', 'organizationName', new FieldOption()),
  
    gender: new Field('Gender__c', 'gender', new FieldOption()),

    existingCustomerName: new Field('', 'existingCustomerName', new FieldOption()),
  
    yearOfGraduation: new Field('Year_of_Graduation__c', 'yearOfGraduation', new FieldOption()),
  
    mobilePhone: new Field('MobilePhone', 'mobilePhone', new FieldOption()
      .setIndexWithType('string')),
  
    homePhone: new Field('HomePhone', 'homePhone', new FieldOption()),
  
    email: new Field('Email', 'email', new FieldOption()),
  
    kol: new Field('KOL__c', 'kol', new FieldOption()
      .setToLabelState()),
  
    description: new Field('Description', 'description', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    priority: new Field('', 'priority', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    isTargetCustomer: new Field('', 'isTargetCustomer', new FieldOption()
      .setIndexWithType('string')),
  
    lastDateTargetFrequency: new Field('', 'lastDateTargetFrequency', new FieldOption()),
  
    buSpecialty: new Field('', 'buSpecialty', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    specialty: new Field('', 'specialty', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    remoteSpecialty: new Field('Account.C_Specialty_1__c', 'remoteSpecialty', new FieldOption()
      .setToLabelState()),
  
    hcpStatus: new Field('CN_HcpStatus__c', 'hcpStatus', new FieldOption()
      .setInclude('isChinaUser')),
  
    statusDescription: new Field('CN_StatusDescription__c', 'statusDescription', new FieldOption()
      .setInclude('isChinaUser')),
  
    physicianId: new Field('CN_PhysicianId__c', 'physicianId', new FieldOption()
      .setInclude('isChinaUser')),
  
    namedDepartment: new Field('CN_NamedDepartment__c', 'namedDepartment', new FieldOption()
      .setInclude('isChinaUser')),
  
    standardDepartment: new Field('CN_StandardDepartment__c', 'standardDepartment', new FieldOption()
      .setInclude('isChinaUser')),
  
    administrative: new Field('CN_Administrative__c', 'administrative', new FieldOption()
      .setInclude('isChinaUser')),
  
    professionalTitle: new Field('CN_ProfessionalTitle__c', 'professionalTitle', new FieldOption()
      .setInclude('isChinaUser')),
  
    academicTitle: new Field('CN_AcademicTitle__c', 'academicTitle', new FieldOption()
      .setInclude('isChinaUser')),
  
    hcpType: new Field('CN_HcpType__c', 'hcpType', new FieldOption()
      .setInclude('isChinaUser')),
  
    legalMedicalLicence: new Field('legal_medical_licence__c', 'legalMedicalLicence', new FieldOption()
      .setInclude('isChinaUser')),
  
    workingTime: new Field('CN_WorkingTime__c', 'workingTime', new FieldOption()
      .setInclude('isChinaUser')),
  
    targetDoctor: new Field('CN_TargetDoctor__c', 'targetDoctor', new FieldOption()
      .setInclude('isChinaUser')),
  
    cnKol: new Field('CN_KOL__c', 'cnKol', new FieldOption()
      .setInclude('isChinaUser')),
  
    kolProduct1: new Field('CN_KolProduct1__c', 'kolProduct1', new FieldOption()
      .setInclude('isChinaUser')),
  
    kolProduct2: new Field('CN_KolProduct2__c', 'kolProduct2', new FieldOption()
      .setInclude('isChinaUser')),
  
    speaker: new Field('CN_Speaker__c', 'speaker', new FieldOption()
      .setInclude('isChinaUser')),
  
    speakerProduct1: new Field('CN_SpeakerProduct1__c', 'speakerProduct1', new FieldOption()
      .setInclude('isChinaUser')),
  
    speakerProduct2: new Field('CN_SpeakerProduct2__c', 'speakerProduct2', new FieldOption()
      .setInclude('isChinaUser')),
  
    socialOrganizationAndTitle: new Field('CN_SocialOrganizationAndTitle__c', 'socialOrganizationAndTitle', new FieldOption()
      .setInclude('isChinaUser')),
  
    specialtyDescription: new Field('CN_SpecialtyDescription__c', 'specialtyDescription', new FieldOption()
      .setInclude('isChinaUser')),
  
    hcpDescriptionIntroduction: new Field('CN_HcpDescriptionIntroduction__c', 'hcpDescriptionIntroduction', new FieldOption()
      .setInclude('isChinaUser')),
  
    territoryCode: new Field('Account.CN_TerritoryCode__c', 'territoryCode', new FieldOption()
      .setInclude('isChinaUser')),
  
    mylanProductList: new Field('MylanProductList__c', 'mylanProductList', new FieldOption()
      .setInclude('isChinaUser')),
  
    nonMylanProductList: new Field('NonMylanProductList__c', 'nonMylanProductList', new FieldOption()
      .setInclude('isChinaUser')),
  
    workplace: new Field('', 'workplace', new FieldOption()),
  
    redFlag: new Field('Is_Red_Flag__c', 'redFlag', new FieldOption()
      .setIndexWithType('string')
      .setSearchState())
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(ContactScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ContactScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ContactScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(ContactScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(ContactScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(ContactScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(ContactScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(ContactScheme.fields);
  }
}
