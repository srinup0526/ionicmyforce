import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';

export class PharmaEventScheme {
  static readonly table: string = 'PharmaEvent';
  static readonly sfdcTable: string = 'Events__c';
  static readonly description: string = 'Pharma Event';

  static readonly references: any = null;
  static readonly targetReferences: any = null;
  static readonly editableStatuses=['Draft', 'Pre-Event Rejected'];
  //static readonly peAttendeesCollection:any= new PEAttendeesCollection;
  static readonly APPROVAL_STATUS_APPROVED: string = 'Approved';
  static readonly APPROVAL_STATUS_REJECTED: string = 'Pre-Event Rejected';

  static readonly APPROVAL_STATUS_DRAFT: string = 'Draft';
  static readonly APPROVAL_STATUS_SUBMITTED: string = 'Submitted';
  static readonly APPROVAL_STATUS_SUBMITTED_OFFLINE: string = 'Submitted Offline';
  static readonly APPROVAL_STATUS_CANCELLED: string = 'Cancelled';
  static readonly notEditableStages= ['Cancelled','Closed'];
  static readonly APPROVAL_STATUS_PENDING_EVALUATION: string = 'Pending Evaluation by Compliance';
  static readonly APPROVAL_STATUS_COMPLETED: string = 'Completed';
  static readonly APPROVAL_STATUS_CLOSED: string = 'Closed';
  static readonly APPROVAL_STATUS_AWAITING_FLM_APPROVAL: string = 'Awaiting FLM Approval';
  static readonly APPROVAL_STATUS_AWAITING_NSM_APPROVAL: string = 'Awaiting NSM Approval';
  static readonly APPROVAL_STATUS_AWAITING_COMPLIANCE_APPROVAL: string = 'Awaiting Final Compliance Approval';
  static readonly APPROVAL_STATUS_NSM_APPROVED: string = 'NSM Approved';
  static readonly APPROVAL_STATUS_AWAITING_COUNTRY_MANAGER_APPROVAL: string = 'Awaiting Country Manager Approval';
  static readonly APPROVAL_STATUS_POSTEVENT_REJECTED: string = 'Post-Event Rejected';
  static readonly TYPE_Title: string = 'PharmaEvent';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    ownerSfid: new Field('OwnerId', 'ownerSfid',new FieldOption()
    .setUploadState()),

    createdOffline: new Field('Created_Offline__c', 'createdOffline', new FieldOption()
    .setUploadState()),

    remoteOwnerFirstName: new Field('Owner.FirstName', 'remoteOwnerFirstName',new FieldOption()),

    remoteOwnerLastName: new Field('Owner.LastName', 'remoteOwnerLastName',new FieldOption()),

    ownerFirstName: new Field('', 'ownerFirstName',new FieldOption()
    .setIndexWithType('string')
      .setSearchState()),

      ownerLastName: new Field('', 'ownerLastName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
      eventName: new Field('Name', 'eventName',new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),
  
      eventType: new Field('Type_of_Event__c', 'eventType', new FieldOption()
        .setIndexWithType('string')
        .setUploadState()
        .setSearchState()),

      location: new Field('Location__c', 'location',new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),

      startDate: new Field('Start_Date__c', 'startDate', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      endDate: new Field('End_Date__c', 'endDate', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      closedDate: new Field('Closed_Date_Time__c', 'closedDate', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

      closedDateDiff: new Field('Closed_Date_Difference__c', 'closedDateDiff',new FieldOption()),

      stage: new Field('Stage__c', 'stage', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()
    .setUploadState()),

    discussionType: new Field('DiscussionType__c', 'discussionType', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()
    .setUploadState()),

    plannedBudget: new Field('Estimated_Budget__c', 'plannedBudget',new FieldOption()
    .setUploadState()),

    plannedBudgetNew: new Field('Planned_Budget__c', 'plannedBudgetNew', new FieldOption()
    .setUploadState()),

    plannedParticipants: new Field('Planned_number_of_participants__c', 'plannedParticipants', new FieldOption()
    .setUploadState()),

    status: new Field('Status__c', 'status', new FieldOption()
    .setIndexWithType('string')
    .setUploadState()
    .setSearchState()),

    businessUnit: new Field('Business_Unit__c', 'businessUnit',new FieldOption()
    .setUploadState()),

    objectives: new Field('Objectives__c', 'objectives', new FieldOption()
  .setUploadState()),

  agenda: new Field('Agenda__c', 'agenda',new FieldOption()
    .setUploadState()),

    speakers: new Field('Speaker_s__c', 'speakers', new FieldOption()
  .setUploadState()),

  evaluation: new Field('Evaluation__c', 'evaluation',new FieldOption()
    .setUploadState()),

    productPrio1SfId: new Field('Product_Prio1__c', 'productPrio1SfId', new FieldOption()
  .setUploadState()),

  productPrio2SfId: new Field('Product_Prio2__c', 'productPrio2SfId',new FieldOption()
    .setUploadState()),

    productPrio3SfId: new Field('Product_Prio3__c', 'productPrio3SfId', new FieldOption()
  .setUploadState()),


  productPrio4SfId: new Field('Product_Prio_4__c', 'productPrio4SfId', new FieldOption()
  .setUploadState()),

  clmToolId: new Field('CLM_Tool_Id__c', 'clmToolId',new FieldOption()
    .setUploadState()),

    tgEForm: new Field('TG_and_E_Form__c', 'tgEForm', new FieldOption()),


    invoiceReceiptOne: new Field('Invoice_Receipt__c', 'invoiceReceiptOne',new FieldOption()),

    invoiceReceiptTwo: new Field('Invoice_Receipt1__c', 'invoiceReceiptTwo', new FieldOption()),

    invoiceReceiptThree: new Field('Invoice_Receipt2__c', 'invoiceReceiptThree',new FieldOption()),

    invoiceReceiptFour: new Field('Invoice_Receipt3__c', 'invoiceReceiptFour', new FieldOption()),

    attendanceSheet: new Field('Attendance_Sheet__c', 'attendanceSheet',new FieldOption()),

    attendanceSheetOne: new Field('Attendance_Sheet_1__c', 'attendanceSheetOne', new FieldOption()),

    summaryOfEvent: new Field('Summary_of_the_event__c', 'summaryOfEvent',new FieldOption()
    .setUploadState()),

    noOfHcps: new Field('No_of_HCPs__c', 'noOfHcps', new FieldOption()
  .setUploadState()),

  noOfMylanStaff: new Field('No_of_Mylan_Staff__c', 'noOfMylanStaff',new FieldOption()
    .setUploadState()),

    costOfEachMeal: new Field('Cost_of_each_meal__c', 'costOfEachMeal', new FieldOption()
  .setUploadState()),


  costOfEachMealUSD: new Field('Cost_of_Meal_per_person_USD__c', 'costOfEachMealUSD', new FieldOption()
  .setUploadState()),

  TGEData: new Field('', 'TGEData',new FieldOption()),

  nameOfThePresenter: new Field('Name_of_the_Presenter__c', 'nameOfThePresenter', new FieldOption()
  .setIndexWithType('string')
  .setUploadState()
  .setSearchState()),

  mylanParticipant: new Field('Mylan_Participants__c', 'mylanParticipant', new FieldOption()
  .setIndexWithType('string')
  .setUploadState()
  .setSearchState()),
  
  mylanParticipantEmails: new Field('Mylan_Participants_Emails__c', 'mylanParticipantEmails', new FieldOption()
  .setIndexWithType('string')
  .setUploadState()
  .setSearchState()),

  typeOfHospital: new Field('Type_of_Hospital__c', 'typeOfHospital', new FieldOption()
  .setUploadState()),

  plannedBudgetUSD: new Field('Budget__c', 'plannedBudgetUSD', new FieldOption()
  .setUploadState()),

  otherExpensesCondition: new Field('Any_Other_Expenses__c', 'otherExpensesCondition', new FieldOption()
  .setUploadState()),

  plannedOtherExpensesText1: new Field('Planned_OE_Text_1__c', 'plannedOtherExpensesText1', new FieldOption()
  .setIndexWithType('string')
  .setUploadState()),

  plannedOtherExpensesText2: new Field('Planned_OE_Text_2__c', 'plannedOtherExpensesText2', new FieldOption()
  .setIndexWithType('string')
  .setUploadState()),

  plannedOtherExpensesText3: new Field('Planned_OE_Text_3__c', 'plannedOtherExpensesText3', new FieldOption()
  .setIndexWithType('string')
  .setUploadState()),

  plannedOtherExpensesText4: new Field('Planned_OE_Text_4__c', 'plannedOtherExpensesText4', new FieldOption()
  .setIndexWithType('string')
  .setUploadState()),

  plannedOtherExpensesText5: new Field('Planned_OE_Text_5__c', 'plannedOtherExpensesText5', new FieldOption()
  .setIndexWithType('string')
  .setUploadState()),

  plannedOtherExpensesLC1: new Field('Planned_OE_Local_1__c', 'plannedOtherExpensesLC1', new FieldOption()
  .setUploadState()),

  plannedOtherExpensesLC2: new Field('Planned_OE_Local_2__c', 'plannedOtherExpensesLC2', new FieldOption()
  .setUploadState()),

  plannedOtherExpensesLC3: new Field('Planned_OE_Local_3__c', 'plannedOtherExpensesLC3', new FieldOption()
  .setUploadState()),
  plannedOtherExpensesLC4: new Field('Planned_OE_Local_4__c', 'plannedOtherExpensesLC4', new FieldOption()
  .setUploadState()),

  plannedOtherExpensesLC5: new Field('Planned_OE_Local_5__c', 'plannedOtherExpensesLC5', new FieldOption()
  .setUploadState()),

  plannedOtherExpensesUSD1: new Field('Planned_OE_USD_1__c', 'plannedOtherExpensesUSD1', new FieldOption()
  .setUploadState()),

  plannedOtherExpensesUSD2: new Field('Planned_OE_USD_2__c', 'plannedOtherExpensesUSD2', new FieldOption()
  .setUploadState()),

  plannedOtherExpensesUSD3: new Field('Planned_OE_USD_3__c', 'plannedOtherExpensesUSD3', new FieldOption()
  .setUploadState()),

  plannedOtherExpensesUSD4: new Field('Planned_OE_USD_4__c', 'plannedOtherExpensesUSD4', new FieldOption()
  .setUploadState()),

  plannedOtherExpensesUSD5: new Field('Planned_OE_USD_5__c', 'plannedOtherExpensesUSD5', new FieldOption()
  .setUploadState()),

  plannedTotalCostofEventLC: new Field('Total_Cost_of_Event_Local_Currency__c', 'plannedTotalCostofEventLC', new FieldOption()
  .setUploadState()),

  plannedTotalCostofEventUSD: new Field('Total_Cost_of_Event_USD__c', 'plannedTotalCostofEventUSD', new FieldOption()
  .setUploadState()),

  //Fields for China
  organizer: new Field('CN_Organizer__c', 'organizer', new FieldOption()
  .setInclude('isChinaUser')),

  region: new Field('CN_Region__c', 'region', new FieldOption()
  .setInclude('isChinaUser')),

  workPlace: new Field('CN_WorkPlace__c', 'workPlace', new FieldOption()
  .setInclude('isChinaUser')),

  regionGroup: new Field('CN_RegionGroup__c', 'regionGroup', new FieldOption()
  .setInclude('isChinaUser')),

  product: new Field('CN_Product__c', 'product', new FieldOption()
  .setInclude('isChinaUser')
  .setIndexWithType('string')
  .setSearchState()),

  venue: new Field('CN_Venue__c', 'venue', new FieldOption()
  .setInclude('isChinaUser')
  .setIndexWithType('string')
  .setSearchState()),
  
  purposeAndContent: new Field('CN_PurposeAndContent__c', 'purposeAndContent', new FieldOption()
  .setInclude('isChinaUser')
  .setIndexWithType('string')
  .setSearchState()),

  timeBegin: new Field('CN_TimeBegin__c', 'timeBegin', new FieldOption()
  .setInclude('isChinaUser')
  .setIndexWithType('string')
  .setSearchState()),

  timeEnd: new Field('CN_TimeEnd__c', 'timeEnd', new FieldOption()
  .setInclude('isChinaUser')
  .setIndexWithType('string')
  .setSearchState()),

  timeBeginReal: new Field('CN_TimeBeginReal__c', 'timeBeginReal', new FieldOption()
  .setInclude('isChinaUser')),

  timeEndReal: new Field('CN_TimeEndReal__c', 'timeEndReal', new FieldOption()
  .setInclude('isChinaUser')),

  internalAttendeeNumber: new Field('CN_InternalAttendeeNumber__c', 'internalAttendeeNumber', new FieldOption()
  .setInclude('isChinaUser')),

  externalAttendeeNumber: new Field('CN_ExternalAttendeeNumber__c', 'externalAttendeeNumber', new FieldOption()
  .setInclude('isChinaUser')),

  totalBudget: new Field('CN_TotalBudget__c', 'totalBudget', new FieldOption()
  .setInclude('isChinaUser')
  .setIndexWithType('string')
  .setSearchState()),

  totalExpense: new Field('CN_TotalExpense__c', 'totalExpense', new FieldOption()
  .setInclude('isChinaUser')),

  remark: new Field('CN_Remark__c', 'remark', new FieldOption()
  .setInclude('isChinaUser')),

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(PharmaEventScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(PharmaEventScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(PharmaEventScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(PharmaEventScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(PharmaEventScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(PharmaEventScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(PharmaEventScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(PharmaEventScheme.fields);
  }

}
