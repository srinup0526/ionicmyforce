import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class PEAbbottAttendeeScheme {
  static readonly table: string = 'PEAbbottAttendee';
  static readonly sfdcTable: string = 'Pharma_Event_Abbott_Attendees__c';
  static readonly description: string = 'PE Abbott Attendee';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    pharmaEventSfId: new Field('Pharma_Event__c', 'pharmaEventSfId', new FieldOption()
    .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),

      attendeeSfId: new Field('AbbottAttendeeName__c', 'attendeeSfId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),

      clmToolId: new Field('CLM_Tool_Id__c', 'clmToolId', new FieldOption()
      .setUploadState()),

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(PEAbbottAttendeeScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(PEAbbottAttendeeScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(PEAbbottAttendeeScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(PEAbbottAttendeeScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(PEAbbottAttendeeScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(PEAbbottAttendeeScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(PEAbbottAttendeeScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(PEAbbottAttendeeScheme.fields);
  }
  organizationNameAndAddress(){
    "${this.organizationName ? ''} <br/> ${this.organizationAddress ? ''} ${this.organizationCity ? ''}"
  }
  organizationNameWithAddress(){
    "${this.organizationName ? ''} - ${this.organizationAddress ? ''} ${this.organizationCity ? ''}"
  }
}
