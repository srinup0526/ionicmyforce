import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class FaqAttachmentScheme {
  static readonly table: string = 'FaqAttachment';
  static readonly sfdcTable: string = 'Attachment';
  static readonly description: string = 'FAQ Attachment';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    body: new Field('Body', 'body', new FieldOption()),

    bodyLength: new Field('BodyLength', 'bodyLength', new FieldOption()),

    contentType: new Field('ContentType', 'contentType', new FieldOption()),

    description: new Field('Description', 'description', new FieldOption()),

    isPrivate: new Field('IsPrivate', 'isPrivate', new FieldOption()),

    title: new Field('Name', 'title', new FieldOption()),

    ownerId: new Field('OwnerId', 'ownerId', new FieldOption()),

    attachedByName: new Field('CreatedBy.Name', 'attachedByName', new FieldOption()),

    parentId: new Field('ParentId', 'parentId', new FieldOption()
      .setIndexWithType('string')),

    lastModify: new Field('LastModifiedDate', 'lastModify', new FieldOption()),

    localParentId: new Field('', 'localParentId', new FieldOption()
      .setIndexWithType('string')),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(FaqAttachmentScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(FaqAttachmentScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(FaqAttachmentScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(FaqAttachmentScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(FaqAttachmentScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(FaqAttachmentScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(FaqAttachmentScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(FaqAttachmentScheme.fields);
  }
}
