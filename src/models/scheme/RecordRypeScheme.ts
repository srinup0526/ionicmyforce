import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class RecordTypeScheme {
  static readonly table: string = 'RecordType';
  static readonly sfdcTable: string = 'RecordType';
  static readonly description: string = 'Record Type';

  static readonly orderObject: string = 'Order__C';


  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption().setIndexWithType('string').setSearchState()),

    developerName: new Field('DeveloperName', 'developerName', new FieldOption().setIndexWithType('string')
      .setSearchState()),

    objectType: new Field('SobjectType', 'objectType', new FieldOption().setIndexWithType('string').setSearchState()),

    isActive: new Field('IsActive', 'isActive', new FieldOption().setIndexWithType('string')),

    lastModifiedDate: new Field('LastModifiedDate', 'lastModifiedDate', new FieldOption().setIndexWithType('string')),

    lastSyncDate: new Field('', 'lastSyncDate', new FieldOption().setIndexWithType('string')),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(RecordTypeScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(RecordTypeScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(RecordTypeScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(RecordTypeScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(RecordTypeScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(RecordTypeScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(RecordTypeScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(RecordTypeScheme.fields);
  }
}
