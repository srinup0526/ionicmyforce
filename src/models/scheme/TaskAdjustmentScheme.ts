import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class TaskAdjustmentScheme {
  static readonly table:string = 'TaskAdjustment';
  static readonly sfdcTable:string = 'TM_TaskAdjustment__c';
  static readonly description:string = 'Task Adjustment';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    callReportSfId: new Field('CallReport__c', 'callReportSfId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      numberRealValue: new Field('NumberRealValue__c', 'numberRealValue', new FieldOption()
      .setUploadState()),

      productItemSfId: new Field('SKU__c', 'productItemSfId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      stringRealValue: new Field('StringRealValue__c', 'stringRealValue', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      remotePromotionTaskSfId: new Field('PromotionTask_Account__r.Promotion_Task__c', 'remotePromotionTaskSfId', new FieldOption()),
     

      promotionTaskAccountSfId: new Field('PromotionTask_Account__c', 'promotionTaskAccountSfId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

      promotionTaskSfId: new Field('', 'promotionTaskSfId', new FieldOption()
      .setIndexWithType('string')),

      isModifiedInTrade: new Field('', 'isModifiedInTrade', new FieldOption()),

      isModifiedInCall: new Field('', 'isModifiedInTrade', new FieldOption()),

      clmToolId: new Field('CLM_Tool_Id__c', 'clmToolId', new FieldOption()
      .setUploadState()),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(TaskAdjustmentScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(TaskAdjustmentScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(TaskAdjustmentScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(TaskAdjustmentScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(TaskAdjustmentScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(TaskAdjustmentScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(TaskAdjustmentScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(TaskAdjustmentScheme.fields);
  }
}
