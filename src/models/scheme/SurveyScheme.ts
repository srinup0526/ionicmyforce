import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class SurveyScheme {
  static readonly table: string = 'Survey';
  static readonly sfdcTable: string = 'Survey__c';
  static readonly description: string = 'Survey';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    surveyname: new Field('Name', 'surveyname', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    surveytype: new Field('Survey_type__c', 'surveytype', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    startdate: new Field('Start_Date__c', 'startdate', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    enddate: new Field('End_Date__c', 'enddate', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    isActive: new Field('Active__c', 'isActive', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    createddate: new Field('CreatedDate', 'createddate', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    pharmaProduct: new Field('Pharma_Product__c', 'pharmaProduct', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    productAtcClass: new Field('Product_ATC_Class__c', 'productAtcClass', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    bu: new Field('Business_Unit__c', 'bu', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    currency: new Field('Currency__c', 'currency', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),


  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(SurveyScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(SurveyScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(SurveyScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(SurveyScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(SurveyScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(SurveyScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(SurveyScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(SurveyScheme.fields);
  }
}
