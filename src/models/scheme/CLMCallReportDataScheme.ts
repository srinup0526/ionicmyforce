import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class CLMCallReportDataScheme {
  static readonly table: string = 'CLMCallReportData';
  static readonly sfdcTable: string = 'Clm_CallReportData__c';
  static readonly description: string = 'CLM Call Report Data';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    kpiSrcJson: new Field('KpiSrcJson__c', 'kpiSrcJson', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    timeOnPresentation: new Field('TimeOnPresentation__c', 'timeOnPresentation', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    timeOnSlides: new Field('TimeOnSlides__c', 'timeOnSlides', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    callReportId: new Field('CallReport__c', 'callReportId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    productId: new Field('Product__c', 'productId', new FieldOption()
      .setUploadState()),

    productName: new Field('Product__r.Name', 'productName', new FieldOption()),

    presentationId: new Field('Presentation__c', 'presentationId', new FieldOption()
      .setUploadState()),

    presentationName: new Field('Presentation__r.Name', 'presentationName', new FieldOption()),

    clmToolId: new Field('CLM_Tool_Id__c', 'clmToolId', new FieldOption()
      .setUploadState()),


  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(CLMCallReportDataScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(CLMCallReportDataScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(CLMCallReportDataScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(CLMCallReportDataScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(CLMCallReportDataScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(CLMCallReportDataScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(CLMCallReportDataScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(CLMCallReportDataScheme.fields);
  }

  
}
