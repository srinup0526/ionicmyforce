import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ReferenceScheme {
  static readonly table: string = 'Reference';
  static readonly sfdcTable: string = 'Reference__c';
  static readonly description: string = 'Reference';
  
  static readonly STATUS_INACTIVE: string = '9';
  static readonly STATUS_ACTIVE: string = '3';
  static readonly STATUS_NEW: string = '0';
  
  static activeRow: string = null;
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
    
    name: new Field('Customer__r.Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    contactSfId: new Field('Customer__c', 'contactSfId', new FieldOption().setIndexWithType('string')),
    
    organizationSfId: new Field('Organisation__c', 'organizationSfId', new FieldOption()
      .setIndexWithType('string')),
    
    contactAccountId: new Field('Customer__r.Account.Id', 'contactAccountId', new FieldOption()
      .setIndexWithType('string')),
    
    contactRecordType: new Field('Customer__r.Account.RecordType.Name', 'contactRecordType', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    remoteSubtype: new Field('Customer__r.Person_Type__c', 'remoteSubtype', new FieldOption()),
    
    subtype: new Field('', 'subtype', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    isPrimary: new Field('Primary__c', 'isPrimary', new FieldOption()),
    
    status: new Field('C_Status_Reference__c', 'status', new FieldOption().setIndexWithType('string')),
    
    organizationName: new Field('Organisation__r.Name', 'organizationName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    organizationCity: new Field('Organisation__r.BillingCity', 'organizationCity', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    organizationPhone: new Field('Organisation__r.Phone', 'organizationPhone', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    organizationCountry: new Field('Organisation__r.BillingCountry', 'organizationCountry', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    organizationAddress: new Field('Organisation__r.BillingStreet', 'organizationAddress', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    organizationBrick: new Field('Organisation__r.Brick__c', 'organizationBrick', new FieldOption()
      .setIndexWithType('string')),
    
    contactFirstName: new Field('Customer__r.FirstName', 'contactFirstName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    contactLastName: new Field('Customer__r.LastName', 'contactLastName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    priority: new Field('', 'priority', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    buSpecialty: new Field('', 'buSpecialty', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    specialty: new Field('', 'specialty', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    atCalls: new Field('', 'atCalls', new FieldOption().setIndexWithType('string')),
    
    lastCall: new Field('', 'lastCall', new FieldOption().setIndexWithType('string'))
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(ReferenceScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ReferenceScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ReferenceScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(ReferenceScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(ReferenceScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(ReferenceScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(ReferenceScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(ReferenceScheme.fields);
  }
}
