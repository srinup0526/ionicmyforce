import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class PatchCustomerReferenceScheme {
  static readonly table: string = 'PatchCustomerReference';
  static readonly sfdcTable: string = 'Customer_Reference__c';
  static readonly description: string = 'Patch Customer Reference';
  
  
  static activeRow: string = null;
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
    
    name: new Field('Customer_Name__c', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    customerSfId: new Field('Customer__c', 'customerSfId', new FieldOption().setIndexWithType('string')),
    
    organizationSfId: new Field('Hospital_Pharmacy__c', 'organizationSfId', new FieldOption()
      .setIndexWithType('string')),

    organizationName: new Field('Hospital_Pharmacy_Name__c', 'organizationName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    recordType: new Field('Type__c', 'recordType', new FieldOption()
      .setIndexWithType('string')),
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(PatchCustomerReferenceScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(PatchCustomerReferenceScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(PatchCustomerReferenceScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(PatchCustomerReferenceScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(PatchCustomerReferenceScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(PatchCustomerReferenceScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(PatchCustomerReferenceScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(PatchCustomerReferenceScheme.fields);
  }
}
