import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class JointVisitPlanScheme {
  static readonly table: string = 'JointVisitPlan';
  static readonly sfdcTable: string = 'Joint_Visit_CEE__c';
  static readonly description: string = 'Joint Visit Plan';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    status: new Field('Status__c', 'status', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    medrep: new Field('MedRep__c', 'medrep', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    medrepName:new Field('MedRep__r.Name', 'medrepName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    flm: new Field('FLM__c', 'flm', new FieldOption()
      .setIndexWithType('string')),

    flmName: new Field('FLM__r.Name', 'flmName', new FieldOption()
      .setIndexWithType('string')),

    coachingdate: new Field('Coaching_Date__c', 'coachingdate', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    scoretotal: new Field('Score_Total__c', 'scoretotal', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    developmentplan: new Field('Development_Plan__c', 'developmentplan', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    developmentPlanName: new Field('Development_Plan__r.Name', 'developmentPlanName', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(JointVisitPlanScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(JointVisitPlanScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(JointVisitPlanScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(JointVisitPlanScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(JointVisitPlanScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(JointVisitPlanScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(JointVisitPlanScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(JointVisitPlanScheme.fields);
  }
}
