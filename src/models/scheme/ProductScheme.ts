import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ProductScheme {
  static readonly table = 'Product';
  static readonly sfdcTable = 'Pharma_Product__c';
  static readonly description = 'Product';
  static readonly TYPE_LOCAL = 'Local';

  static activeRow: string = null;
  static activeRows: Array<string> = [];

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')),

    atcClass: new Field('ATC_Class__c', 'atcClass', new FieldOption()
      .setIndexWithType('string')),

    presentationId: new Field('Presentation__c', 'presentationId', new FieldOption()),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(ProductScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ProductScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ProductScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(ProductScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(ProductScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(ProductScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(ProductScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(ProductScheme.fields);
  }
}
