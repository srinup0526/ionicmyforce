import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import {SchemeParser} from './SchemeParser';


export class IqviaQuestionScheme {
  static readonly table: string = 'IQVIAQuestion';
  static readonly sfdcTable: string = 'IQVIA_Question__c';
  static readonly description: string = 'IQVIA Question';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    question: new Field('Name', 'question', new FieldOption()),

    description: new Field('Description__c', 'description', new FieldOption()),

    isActive: new Field('isActive__c', 'isActive', new FieldOption()
      .setIndexWithType('string')),

    order: new Field('Order__c', 'order', new FieldOption()
      .setIndexWithType('string')),

    picklistValue: new Field('Picklist_Value__c', 'picklistValue', new FieldOption()),

    type: new Field('Type__c', 'type', new FieldOption())
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(IqviaQuestionScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(IqviaQuestionScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(IqviaQuestionScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(IqviaQuestionScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(IqviaQuestionScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(IqviaQuestionScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(IqviaQuestionScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(IqviaQuestionScheme.fields);
  }
}
