import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class SalesPlanningProductScheme {
  static readonly table: string = 'SalesPlanningProduct';
  static readonly sfdcTable: string = 'Sales_Planning_Product__c';
  static readonly description: string = 'Sales Planning Product';
  
  static readonly STATUS_ACTIVE: string = 'Active';
  

  
  static activeRow: string = null;
  static activeRows: Array<string> = [];
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
  
    plannedQuantity: new Field('Planned_Quantity__c', 'plannedQuantity', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    price: new Field('Price_PI__c', 'price', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
  
    product: new Field('Product_Name__c', 'product', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    productName: new Field('Product_Name__r.Name', 'productName', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),
  
    salesPlanningList: new Field('Sales_Planning_List__c', 'salesPlanningList', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),
  
    total: new Field('Total__c', 'total', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),
  
    totalPrice: new Field('Total_Price__c', 'totalPrice', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),
  
   
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(SalesPlanningProductScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(SalesPlanningProductScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(SalesPlanningProductScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(SalesPlanningProductScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(SalesPlanningProductScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(SalesPlanningProductScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(SalesPlanningProductScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(SalesPlanningProductScheme.fields);
  }
}
