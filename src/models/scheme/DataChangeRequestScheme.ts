import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class DataChangeRequestScheme {
  static readonly table: string = 'DataChangeRequest';
  static readonly sfdcTable: string = 'DCR__c';
  static readonly description: string = 'DataChangeRequest';
  
  static readonly TYPES = {
    CONTACT: "Contact and workplace",
    ORGANIZATION: "Organization",
    REFERENCE: "Workplace",
    SEGMENTATION: "Segmentation"
  };

  static readonly STATUSES = {
    DRAFT: "Draft",
    NEW: "New",
    PROCESSED_REJECTED: "Processed: Rejected",
    PROCESSED_CONFIRMED: "Processed: Confirmed",
    SENT_TO_SALESFORCE: "Sent to Salesforce",
  };

  static readonly OPERATION_TYPES = {
    INSERT: 'insert',
    UPDATE: 'update',
    DELETE: 'delete'
  };
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
  
    dcrOrganizationCNTerritoryCode: new Field('DCR_Organization_CN_TerritoryCode__c', 'dcrOrganizationCNTerritoryCode', new FieldOption()
      .setUploadState()),
  
    dcrContactCJobTitle: new Field('DCR_Contact_C_JobTitle__c', 'dcrContactCJobTitle', new FieldOption()
      .setUploadState()),
  
    dcrContactCNAcademicTitle: new Field('DCR_Contact_CN_AcademicTitle__c', 'dcrContactCNAcademicTitle', new FieldOption()
      .setUploadState()),
  
    dcrContactCNAdministrative: new Field('DCR_Contact_CN_Administrative__c', 'dcrContactCNAdministrative', new FieldOption()
      .setUploadState()),
  
    dcrContactCNDescriptionIntroduction: new Field('DCR_Contact_CN_DescriptionIntroduction__c', 'dcrContactCNDescriptionIntroduction', new FieldOption()
      .setUploadState()),
  
    dcrContactCNHcpStatus: new Field('DCR_Contact_CN_HCpStatus__c', 'dcrContactCNHcpStatus', new FieldOption()
      .setUploadState()),
  
    dcrContactCNHcpType: new Field('DCR_Contact_CN_HCpType__c', 'dcrContactCNHcpType', new FieldOption()
      .setUploadState()),
  
    dcrContactCNKOL: new Field('DCR_Contact_CN_KOL__c', 'dcrContactCNKOL', new FieldOption()
      .setUploadState()),
  
    dcrContactCNKolProduct1: new Field('DCR_Contact_CN_KOLProduct1__c', 'dcrContactCNKolProduct1', new FieldOption()
      .setUploadState()),
  
    dcrContactCNKolProduct2: new Field('DCR_Contact_CN_KOLProduct2__c', 'dcrContactCNKolProduct2', new FieldOption()
      .setUploadState()),
  
    dcrContactCNNamedDepartment: new Field('DCR_Contact_CN_NamedDepartment__c', 'dcrContactCNNamedDepartment', new FieldOption()
      .setUploadState()),
  
    dcrContactCNPhysicianId: new Field('DCR_Contact_CN_PhysicianId__c', 'dcrContactCNPhysicianId', new FieldOption()
      .setUploadState()),
  
    dcrContactCNProfessionalTitle: new Field('DCR_Contact_CN_ProfessionalTitle__c', 'dcrContactCNProfessionalTitle', new FieldOption()
      .setUploadState()),
  
    dcrContactCNSocialOrgAndTitle: new Field('DCR_Contact_CN_SocialOrgAndTitle__c', 'dcrContactCNSocialOrgAndTitle', new FieldOption()
      .setUploadState()),
  
    dcrContactCNSpeaker: new Field('DCR_Contact_CN_Speaker__c', 'dcrContactCNSpeaker', new FieldOption()
      .setUploadState()),
  
    dcrContactCNSpeakerProduct1: new Field('DCR_Contact_CN_SpeakerProduct1__c', 'dcrContactCNSpeakerProduct1', new FieldOption()
      .setUploadState()),
  
    dcrContactCNSpeakerProduct2: new Field('DCR_Contact_CN_SpeakerProduct2__c', 'dcrContactCNSpeakerProduct2', new FieldOption()
      .setUploadState()),
  
    dcrContactCNSpecialtyDescription: new Field('DCR_Contact_CN_SpecialtyDescription__c', 'dcrContactCNSpecialtyDescription', new FieldOption()
      .setUploadState()),
  
    dcrContactCNStandardDepartment: new Field('DCR_Contact_CN_StandardDepartment__c', 'dcrContactCNStandardDepartment', new FieldOption()
      .setUploadState()),
  
    dcrContactCNStatusDescription: new Field('DCR_Contact_CN_StatusDescription__c', 'dcrContactCNStatusDescription', new FieldOption()
      .setUploadState()),
  
    dcrContactCNWorkingTime: new Field('DCR_Contact_CN_WorkingTime__c', 'dcrContactCNWorkingTime', new FieldOption()
      .setUploadState()),
  
    dcrContactDescription: new Field('DCR_Contact_Description__c', 'dcrContactDescription', new FieldOption()
      .setUploadState()),
  
    dcrContactEmail: new Field('DCR_Contact_Email__c', 'dcrContactEmail', new FieldOption()
      .setUploadState()),
  
    dcrContactFirstName: new Field('DCR_Contact_FirstName__c', 'dcrContactFirstName', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),
  
    dcrContactGender: new Field('DCR_Contact_Gender__c', 'dcrContactGender', new FieldOption()
      .setUploadState()),
  
    dcrContactHomePhone: new Field('DCR_Contact_HomePhone__c', 'dcrContactHomePhone', new FieldOption()
      .setUploadState()),
  
    dcrContactId: new Field('DCR_Contact_Id__c', 'dcrContactId', new FieldOption()
      .setUploadState()),
  
    dcrContactInclinationToMylan: new Field('DCR_Contact_InclinationToMylan__c', 'dcrContactInclinationToMylan', new FieldOption()
      .setUploadState()),
  
    dcrContactKOL: new Field('DCR_Contact_KOL__c', 'dcrContactKOL', new FieldOption()
      .setUploadState()),
  
    dcrContactLastName: new Field('DCR_Contact_LastName__c', 'dcrContactLastName', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),
  
    dcrContactLegalMedicalLicence: new Field('DCR_Contact_Legal_medical_licence__c', 'dcrContactLegalMedicalLicence', new FieldOption()
      .setUploadState()),
  
    dcrContactMobilePhone: new Field('DCR_Contact_MobilePhone__c', 'dcrContactMobilePhone', new FieldOption()
      .setUploadState()),
  
    dcrContactName: new Field('DCR_Contact_Name__c', 'dcrContactName', new FieldOption()
      .setUploadState()),
  
    dcrContactNumberOfPatientsPerDay: new Field('DCR_Contact_NumberOfPatientsPerDay__c', 'dcrContactNumberOfPatientsPerDay', new FieldOption()
      .setUploadState()),
  
    dcrContactPersonType: new Field('DCR_Contact_PersonType__c', 'dcrContactPersonType', new FieldOption()
      .setUploadState()),
  
    dcrContactSpecialty: new Field('DCR_Contact_C_Specialty1__c', 'dcrContactSpecialty', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),
  
    dcrContactYearOfGraduation: new Field('DCR_Contact_YearOfGraduation__c', 'dcrContactYearOfGraduation', new FieldOption()
      .setUploadState()),
  
    dcrDateOfRequest: new Field('DCR_Date_Of_Request__c', 'dcrDateOfRequest', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
  
    dcrOrganizationBillingCity: new Field('DCR_Organization_BillingCity__c', 'dcrOrganizationBillingCity', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationBillingCountry: new Field('DCR_Organization_BillingCountry__c', 'dcrOrganizationBillingCountry', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationBillingPostalCode: new Field('DCR_Organization_BillingPostalCode__c', 'dcrOrganizationBillingPostalCode', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationBillingState: new Field('DCR_Organization_BillingState__c', 'dcrOrganizationBillingState', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationBillingStreet: new Field('DCR_Organization_BillingStreet__c', 'dcrOrganizationBillingStreet', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationBrick: new Field('DCR_Organization_Brick__c', 'dcrOrganizationBrick', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCJuridicGroup: new Field('DCR_Organization_C_JuridicGroup__c', 'dcrOrganizationCJuridicGroup', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCNAlias: new Field('DCR_Organization_CN_Alias__c', 'dcrOrganizationCNAlias', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCNHospitalGrade: new Field('DCR_Organization_CN_HospitalGrade__c', 'dcrOrganizationCNHospitalGrade', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCNHospitalLevel: new Field('DCR_Organization_CN_HospitalLevel__c', 'dcrOrganizationCNHospitalLevel', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCNId: new Field('DCR_Organization_CN_Id__c', 'dcrOrganizationCNId', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCNProperty: new Field('DCR_Organization_CN_Property__c', 'dcrOrganizationCNProperty', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCNStatusDescription: new Field('DCR_Organization_CN_Status_Description__c', 'dcrOrganizationCNStatusDescription', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCNTargetHospital: new Field('DCR_Organization_CN_TargetHospital__c', 'dcrOrganizationCNTargetHospital', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCNType: new Field('DCR_Organization_CN_Type__c', 'dcrOrganizationCNType', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCSpecialty1: new Field('DCR_Organization_C_Specialty1__c', 'dcrOrganizationCSpecialty1', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationCSpecialty2: new Field('DCR_Organization_C_Specialty2__c', 'dcrOrganizationCSpecialty2', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationExternal_Id: new Field('DCR_Organization_External_Id__c', 'dcrOrganizationExternal_Id', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationId: new Field('DCR_Organization_Id__c', 'dcrOrganizationId', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationName: new Field('DCR_Organization_Name__c', 'dcrOrganizationName', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),
  
    dcrOrganizationPhone: new Field('DCR_Organization_Phone__c', 'dcrOrganizationPhone', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationPriority: new Field('DCR_Organization_Priority__c', 'dcrOrganizationPriority', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationRecordType: new Field('DCR_Organization_RecordType__c', 'dcrOrganizationRecordType', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationStatus: new Field('DCR_Organization_Status__c', 'dcrOrganizationStatus', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationSubtype: new Field('DCR_Organization_Subtype__c', 'dcrOrganizationSubtype', new FieldOption()
      .setUploadState()),
  
    dcrOrganizationNumberOfPatientsPerDay: new Field('DCR_Organization_NumberOfPatientsPerDay__c', 'dcrOrganizationNumberOfPatientsPerDay', new FieldOption()
      .setUploadState()),
  
    dcrReferenceCStatus: new Field('DCR_Reference_C_Status__c', 'dcrReferenceCStatus', new FieldOption()
      .setUploadState()),
  
    dcrReferenceId: new Field('DCR_Reference_Id__c', 'dcrReferenceId', new FieldOption()
      .setUploadState()),
  
    dcrReferencePrimary: new Field('DCR_Reference_Primary__c', 'dcrReferencePrimary', new FieldOption()
      .setUploadState()),
  
    dcrStatus: new Field('DCR_Status__c', 'dcrStatus', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),
  
    dcrType: new Field('DCR_Type__c', 'dcrType', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()
      .setSearchState()),
  
    dcrProductSegmentationSegmentation1: new Field('DCR_ProductSegmentation_Segmentation_1__c', 'dcrProductSegmentationSegmentation1', new FieldOption()
      .setUploadState()
      .setInclude('isChinaUser')),
  
    dcrProductSegmentationSegmentation2: new Field('DCR_ProductSegmentation_Segmentation_2__c', 'dcrProductSegmentationSegmentation2', new FieldOption()
      .setUploadState()
      .setInclude('isChinaUser')),
  
    dcrProductSegmentationPhProduct: new Field('DCR_ProductSegmentation_PhProduct__c', 'dcrProductSegmentationPhProduct', new FieldOption()
      .setUploadState()
      .setInclude('isChinaUser')),
  
    dcrProductSegmentationAccount: new Field('DCR_ProductSegmentation_Account__c', 'dcrProductSegmentationAccount', new FieldOption()
      .setUploadState()
      .setInclude('isChinaUser')),
  
    dcrProductSegmentationId: new Field('DCR_ProductSegmentation_Id__c', 'dcrProductSegmentationId', new FieldOption()
      .setUploadState()
      .setInclude('isChinaUser')),
  
    lastModifiedDate: new Field('LastModifiedDate', 'lastModifiedDate', new FieldOption()
      .setIndexWithType('string')),
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(DataChangeRequestScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(DataChangeRequestScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(DataChangeRequestScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(DataChangeRequestScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(DataChangeRequestScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(DataChangeRequestScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(DataChangeRequestScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(DataChangeRequestScheme.fields);
  }
}
