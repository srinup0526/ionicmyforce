import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ProductPresentationScheme {
  static readonly table = 'Product_Presentation';
  static readonly sfdcTable = 'Product_Presentation__c';
  static readonly description = 'Product Presentation';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    product: new Field('Pharma_Product__c', 'product', new FieldOption()
      .setIndexWithType('string')),

    productName: new Field('Pharma_Product__r.Name', 'productName', new FieldOption()
      .setIndexWithType('string')),

    presentation: new Field('Presentation__c', 'presentation', new FieldOption()
      .setIndexWithType('string')),

    presentationName: new Field('Presentation__r.Name', 'presentationName', new FieldOption()),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(ProductPresentationScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ProductPresentationScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ProductPresentationScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(ProductPresentationScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(ProductPresentationScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(ProductPresentationScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(ProductPresentationScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(ProductPresentationScheme.fields);
  }
}
