import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ProductListingHistoryScheme {
  static readonly table = 'ProductListingHistory';
  static readonly sfdcTable = 'ProductListing__History';
  static readonly description = 'Product Listing History';
  static readonly FIELD_ACTION_CREATED = 'created';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    createdById: new Field('CreatedById', 'createdById', new FieldOption()),

      createdByName: new Field('CreatedBy.Name', 'createdByName', new FieldOption()),

      createdDate: new Field('CreatedDate', 'createdDate', new FieldOption()
      .setIndexWithType('string')),
      changedField: new Field('Field', 'changedField', new FieldOption()
      .setIndexWithType('string')),
      isDeleted: new Field('IsDeleted', 'isDeleted', new FieldOption()),
      newValue: new Field('NewValue', 'newValue', new FieldOption()),
      oldValue: new Field('OldValue', 'oldValue', new FieldOption()),
      parentId: new Field('ParentId', 'parentId', new FieldOption()
      .setIndexWithType('string')),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(ProductListingHistoryScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ProductListingHistoryScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ProductListingHistoryScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(ProductListingHistoryScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(ProductListingHistoryScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(ProductListingHistoryScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(ProductListingHistoryScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(ProductListingHistoryScheme.fields);
  }
}
