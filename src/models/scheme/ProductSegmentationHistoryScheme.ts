import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ProductSegmentationHistoryScheme {
  static readonly table:string = 'ProductSegmentationHistory';
  static readonly sfdcTable:string = 'Product_Segmentation__History';
  static readonly description:string = 'Product Segmentation History';
  static readonly FIELD_ACTION_CREATED:string = 'created';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),


    createdById: new Field('CreatedById', 'createdById', new FieldOption()),

    createdByName: new Field('CreatedBy.Name', 'createdByName', new FieldOption()),

    createdDate: new Field('CreatedDate', 'createdDate', new FieldOption()
      .setIndexWithType('string')),

      changedField: new Field('Field', 'changedField', new FieldOption()
      .setIndexWithType('string')),

      isDeleted: new Field('IsDeleted', 'isDeleted', new FieldOption()),
     

      newValue: new Field('NewValue', 'newValue', new FieldOption()),

      oldValue: new Field('OldValue', 'oldValue', new FieldOption()),

      parentId: new Field('ParentId', 'parentId', new FieldOption()
      .setIndexWithType('string')),
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(ProductSegmentationHistoryScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ProductSegmentationHistoryScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ProductSegmentationHistoryScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(ProductSegmentationHistoryScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(ProductSegmentationHistoryScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(ProductSegmentationHistoryScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(ProductSegmentationHistoryScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(ProductSegmentationHistoryScheme.fields);
  }
  isCreatedAction(){
    // return this.changedField == this.ProductSegmentationHistory.FIELD_ACTION_CREATED;
  }
}
