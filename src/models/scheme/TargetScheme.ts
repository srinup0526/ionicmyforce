import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class TargetScheme {
  static readonly table: string = 'Target';
  static readonly sfdcTable: string = 'Target__c';
  static readonly description: string = 'Target';
  

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    medrepSfId: new Field('MedRep__c', 'medrepSfId', new FieldOption()
      .setIndexWithType('string')),

      medrepBusinessUnit: new Field('MedRep__r.Business_Unit__c', 'medrepBusinessUnit', new FieldOption()),

 
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(TargetScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(TargetScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(TargetScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(TargetScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(TargetScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(TargetScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(TargetScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(TargetScheme.fields);
  }

}
