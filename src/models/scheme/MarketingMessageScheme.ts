import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class MarketingMessageScheme {
  static readonly table: string = 'MarketingMessage';
  static readonly sfdcTable: string = 'Pharma_Product_Messages__c';
  static readonly description: string = 'Marketing Message';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('Complete_Marketing_Message__c', 'name', new FieldOption()
      .setIndexWithType('string')),

    produtSfId: new Field('Pharma_Product__c', 'produtSfId', new FieldOption()
      .setIndexWithType('string')),

    status: new Field('Status__c', 'status', new FieldOption()
      .setIndexWithType('string')),

    currencyIsoCode: new Field('CurrencyIsoCode', 'currencyIsoCode', new FieldOption()
      .setIndexWithType('string'))

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(MarketingMessageScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(MarketingMessageScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(MarketingMessageScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(MarketingMessageScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(MarketingMessageScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(MarketingMessageScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(MarketingMessageScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(MarketingMessageScheme.fields);
  }
}
