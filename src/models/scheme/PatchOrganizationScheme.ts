import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class PatchOrganizationScheme {
  static readonly table: string = 'PatchOrganization';
  static readonly sfdcTable: string = 'Patch_Customer__c';
  static readonly description: string = 'Patch Organization';

  static readonly RECORD_TYPE_FIELD_NAME: string = 'recordType';
  static readonly PATCH_FIELD_NAME: string = 'Patch__c';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    patchName: new Field('Patch__r.Name', 'patchName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    contactId: new Field('Contact__c', 'contactId', new FieldOption()
    .setSearchState()),

    contactName: new Field('Contact__r.Name', 'contactName', new FieldOption()
    .setSearchState()),

    class: new Field('Class__c', 'class', new FieldOption()
    .setSearchState()),

    frequency: new Field('Frequency__c', 'frequency', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    recordType: new Field('', 'recordType', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    recordTypeId: new Field('RecordType.Id', 'recordTypeId', new FieldOption()),

    remoteRecordType: new Field('RecordType.Name', 'remoteRecordType', new FieldOption()),

    patchId: new Field('Patch__c', 'patchId', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    city: new Field('City__c', 'city', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    status: new Field('Status__c', 'status', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    accountName: new Field('Account__r.Name', 'accountName', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    accountId: new Field('Account__c', 'accountId', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),


    patchStation: new Field('Patch__r.Station__c', 'patchStation', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    state: new Field('State__c', 'state', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

    speciality: new Field('Speciality__c', 'speciality', new FieldOption()
    .setIndexWithType('string')
    .setSearchState()),

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(PatchOrganizationScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(PatchOrganizationScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(PatchOrganizationScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(PatchOrganizationScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(PatchOrganizationScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(PatchOrganizationScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(PatchOrganizationScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(PatchOrganizationScheme.fields);
  }

}
