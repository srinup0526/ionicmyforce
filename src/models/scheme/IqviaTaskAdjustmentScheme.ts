import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import {SchemeParser} from './SchemeParser';


export class IqviaTaskAdjustmentScheme {
  static readonly table: string = 'IQVIATaskAdjustment';
  static readonly sfdcTable: string = 'IQVIA_Task_Adjustment__c';
  static readonly description: string = 'IQVIA Task Adjustment';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    callReport: new Field('Call_Report__c', 'callReport', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    clmToolId: new Field('CLM_Tool_Id__c', 'clmToolId', new FieldOption()
      .setUploadState()),

    organization: new Field('Organization__c', 'organization', new FieldOption()
      .setUploadState()),

    question: new Field('IQVIA_Question__c', 'question', new FieldOption()
      .setUploadState()),

    task: new Field('IQVIA_Task__c', 'task', new FieldOption()
      .setUploadState()),

    numberRealValue: new Field('NumberRealValue__c', 'numberRealValue', new FieldOption()
      .setUploadState()),

    stringRealValue: new Field('StringRealValue__c', 'stringRealValue', new FieldOption()
      .setUploadState()),

    sku: new Field('SKU__c', 'sku', new FieldOption()
      .setUploadState()),

    skuName: new Field('SKU__r.Name', 'skuName', new FieldOption())
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(IqviaTaskAdjustmentScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(IqviaTaskAdjustmentScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(IqviaTaskAdjustmentScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(IqviaTaskAdjustmentScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(IqviaTaskAdjustmentScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(IqviaTaskAdjustmentScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(IqviaTaskAdjustmentScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(IqviaTaskAdjustmentScheme.fields);
  }
}
