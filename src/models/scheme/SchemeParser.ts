export class SchemeParser {

  static getUploadableFields(fields) {
    return Object.keys(fields)
      .filter((fieldKey) => {
        return fields[fieldKey].options.upload;
      })
      .map((fieldKey) => SchemeParser._valueOfField(fields[fieldKey]));
  }

  static getIndexSpec(fields) {
    return Object.keys(fields)
      .filter((fieldKey) => fields[fieldKey].options.indexWithType || fields[fieldKey].options.search)
      .map((fieldKey) => {
        return {
          path: SchemeParser._valueOfField(fields[fieldKey]),
          type: fields[fieldKey].options.indexWithType || 'string'
        };
      })
      .concat(SchemeParser.generateLocalIndex());
  }

  static _valueOfField(field) {
    if (field.sfdc) {
      return field.sfdc;
    }

    return field.local;
  }

  static getSfdcFields(fields) {
    return Object.keys(fields)
      .filter((localFieldKey) => {
        return !!fields[localFieldKey].sfdc;
      })
      .map((localFieldKey) => {
        return fields[localFieldKey].sfdc;
      });
  }

  static generateLocalIndex() {
    return ['create', 'update', 'delete'].map((localAction) => ({
      path: `__locally_${localAction}d__`,
      type: 'string'
    }));
  }

  static getExcludableFields(fields) {
    return Object.keys(fields)
      .filter((fieldKey) => {
        return fields[fieldKey].options.exclude;
      })
      .reduce((excludableFields, fieldKey) => {

        excludableFields[SchemeParser._valueOfField(fields[fieldKey])] = fields[fieldKey].options.exclude;

        return excludableFields;
      }, {});
  }

  static getIncludableFields(fields) {
    return Object.keys(fields)
      .filter((fieldKey) => {
        return fields[fieldKey].options.include;
      })
      .reduce((includableFields, fieldKey) => {

        includableFields[SchemeParser._valueOfField(fields[fieldKey])] = fields[fieldKey].options.include;

        return includableFields;
      }, {});
  }

  static isToLabel(fields) {
    return Object.keys(fields)
      .reduce((isToLabel, field) => {
        isToLabel[fields[field].sfdc] = fields[field].options.toLabel;
        return isToLabel;
      }, {});
  }

  static hasSearchable(fields): boolean {
    return !!SchemeParser.searchableSchema(fields).length;
  }

  static searchableSchema(fields) {
    return Object.keys(fields)
      .filter((localFieldKey) => {
        return fields[localFieldKey].options.search;
      })
      .map((localFieldKey) => {
        return fields[localFieldKey];
      });
  }
}
