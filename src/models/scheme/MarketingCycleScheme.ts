import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class MarketingCycleScheme {
  static readonly table: string = 'MarketingCycle';
  static readonly sfdcTable: string = 'Marketing_Cycle__c';
  static readonly description: string = 'Marketing Cycle';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
  
    planingCycleName: new Field('Name', 'planingCycleName', new FieldOption()),
  
    cycleName: new Field('Marketing_Cycle_Name__c', 'cycleName', new FieldOption()),
  
    startDate: new Field('Start_Date__c', 'startDate', new FieldOption()
      .setIndexWithType('string')),
  
    endDate: new Field('End_Date__c', 'endDate', new FieldOption()
      .setIndexWithType('string')),
  
    currencyIsoCode: new Field('CurrencyIsoCode', 'currencyIsoCode', new FieldOption()
      .setIndexWithType('string'))
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(MarketingCycleScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(MarketingCycleScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(MarketingCycleScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(MarketingCycleScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(MarketingCycleScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(MarketingCycleScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(MarketingCycleScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(MarketingCycleScheme.fields);
  }
}
