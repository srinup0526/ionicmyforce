import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class OrganizationScheme {
  static readonly table: string = 'Organization';
  static readonly sfdcTable: string = 'Account';
  static readonly description: string = 'Organization';

  static readonly references: any = null;
  static readonly targetReferences: any = null;
  
  static readonly RECORD_TYPE_FIELD_NAME: string = 'recordType';
  static readonly SUBTYPE_FIELD_NAME: string = 'Subtype__c';
  static readonly PRIORITY_FIELD_NAME: string = 'Priority__c';
  
  static activeRow: string = null;

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
    
    externalId: new Field('External_Id__c', 'externalId',new FieldOption()),
    
    name: new Field('Name', 'name', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),
    
    brickSfId: new Field('Brick__c', 'brickSfId',new FieldOption()),
    
    status: new Field('Status__c', 'status',new FieldOption()),
    
    subtype: new Field('Subtype__c', 'subtype',new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),
    
    recordType: new Field('', 'recordType', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),
    recordTypeId: new Field('RecordType.Id', 'recordTypeId', new FieldOption()),
    remoteRecordType: new Field('RecordType.Name', 'remoteRecordType', new FieldOption()),
    country: new Field('BillingCountry', 'country', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),
    city: new Field('BillingCity', 'city', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),
    state: new Field('BillingState', 'state', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),
    address: new Field('BillingStreet', 'address', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),
    postalCode: new Field('BillingPostalCode', 'postalCode', new FieldOption()),
    phone: new Field('Phone', 'phone', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),
    specialty1: new Field('C_Specialty_1__c', 'specialty1', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setToLabelState()
    ),
    specialty2: new Field('C_Specialty_2__c', 'specialty2', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setToLabelState()
    ),
    isPersonAccount: new Field('IsPersonAccount', 'isPersonAccount', new FieldOption()
      .setIndexWithType('string')
    ),
    juridicGroup: new Field('C_Juridic_Group__c', 'juridicGroup', new FieldOption()
      .setIndexWithType('string')
      .setInclude('isJuridicGroupEnabled')
    ),
    globalPriority: new Field('Priority__c', 'globalPriority', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
    ),
    organizationId: new Field('CN_OrganizationId__c', 'organizationId', new FieldOption()
      .setInclude('isChinaUser')
    ),
    alias: new Field('CN_Alias__c', 'alias', new FieldOption()
      .setInclude('isChinaUser')
    ),
    statusDescription: new Field('CN_StatusDescription__c', 'statusDescription', new FieldOption()
      .setInclude('isChinaUser')
    ),
    organizationType: new Field('CN_OrganizationType__c', 'organizationType', new FieldOption()
      .setInclude('isChinaUser')
    ),
    organizationProperty: new Field('CN_OrganizationProperty__c', 'organizationProperty', new FieldOption()
      .setInclude('isChinaUser')
    ),
    hospitalGrade: new Field('CN_HospitalGrade__c', 'hospitalGrade', new FieldOption()
      .setInclude('isChinaUser')
    ),
    hospitalLevel: new Field('CN_HospitalLevel__c', 'hospitalLevel', new FieldOption()
      .setInclude('isChinaUser')
    ),
    targetHospital: new Field('CN_TargetHospital__c', 'targetHospital', new FieldOption()
      .setInclude('isChinaUser')
    ),
    territoryCode: new Field('CN_TerritoryCode__c', 'territoryCode', new FieldOption()
      .setInclude('isChinaUser')
    )
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(OrganizationScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(OrganizationScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(OrganizationScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(OrganizationScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(OrganizationScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(OrganizationScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(OrganizationScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(OrganizationScheme.fields);
  }
}
