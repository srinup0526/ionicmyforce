import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class OrderLineScheme {
  static readonly table: string = 'OrderLineItems';
  static readonly sfdcTable: string = 'Order_Line_Item__c';
  static readonly description: string = 'OrderLineItems';
  
  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
  
    orderId: new Field('Order__c', 'orderId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
  
    quantity: new Field('Quantity__c', 'quantity', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
  
    salesAmount: new Field('Sales_Amount__c', 'salesAmount', new FieldOption()
      .setIndexWithType('string')),
  
    salesPrice: new Field('Sales_Price__c', 'salesPrice', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
  
    SKUName: new Field('SKU_Name__c', 'SKUName', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),
  
    productLot: new Field('SKU_Name__r.Name', 'productLot', new FieldOption()),
  
    sales1MonthAgo: new Field('SKU_Name__r.Sales_1_month_ago__c', 'sales1MonthAgo', new FieldOption()),
    
    sales2MonthAgo: new Field('SKU_Name__r.Sales_2_months_ago__c', 'sales2MonthAgo', new FieldOption()),
    
    sales3MonthAgo: new Field('SKU_Name__r.Sales_3_months_ago__c', 'sales3MonthAgo', new FieldOption()),
    
    sales4MonthAgo: new Field('SKU_Name__r.Sales_4_months_ago__c', 'sales4MonthAgo', new FieldOption()),
    
    salesYTDC: new Field('SKU_Name__r.Sales_YTD__c', 'salesYTDC', new FieldOption())
  };
  
  static get indexSpec() {
    return SchemeParser.getIndexSpec(OrderLineScheme.fields);
  }
  
  static get uploadableFields() {
    return SchemeParser.getUploadableFields(OrderLineScheme.fields);
  }
  
  static get sfdcFields() {
    return SchemeParser.getSfdcFields(OrderLineScheme.fields);
  }
  
  static get excludableFields() {
    return SchemeParser.getExcludableFields(OrderLineScheme.fields);
  }
  
  static get includableFields() {
    return SchemeParser.getIncludableFields(OrderLineScheme.fields);
  }
  
  static get isToLabel() {
    return SchemeParser.isToLabel(OrderLineScheme.fields);
  }
  
  static hasSearchable() {
    return SchemeParser.hasSearchable(OrderLineScheme.fields);
  }
  
  static searchableSchema() {
    return SchemeParser.searchableSchema(OrderLineScheme.fields);
  }
}
