import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import {SchemeParser} from './SchemeParser';


export class IqviaAccountSkuTaskScheme {
  static readonly table: string = 'IQVIAAccountSKUTask';
  static readonly sfdcTable: string = 'IQVIA_Account_SKU_Task__c';
  static readonly description: string = 'IQVIA Account SKU Task';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    startDate: new Field('Start_Date__c', 'startDate', new FieldOption()
      .setIndexWithType('string')),

    endDate: new Field('End_Date__c', 'endDate', new FieldOption()
      .setIndexWithType('string')),

    task: new Field('IQVIA_Task__c', 'task', new FieldOption()),

    taskName: new Field('IQVIA_Task_Name__c', 'taskName', new FieldOption()
      .setIndexWithType('string')),

    organization: new Field('Organisation__c', 'organization', new FieldOption()
      .setIndexWithType('string')),

    sku: new Field('SKU__c', 'sku', new FieldOption()),

    skuName: new Field('SKU_Name__c', 'skuName', new FieldOption()
      .setIndexWithType('string')),

    salesRep: new Field('Sales_Rep__c', 'salesRep', new FieldOption())
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(IqviaAccountSkuTaskScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(IqviaAccountSkuTaskScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(IqviaAccountSkuTaskScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(IqviaAccountSkuTaskScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(IqviaAccountSkuTaskScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(IqviaAccountSkuTaskScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(IqviaAccountSkuTaskScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(IqviaAccountSkuTaskScheme.fields);
  }
}
