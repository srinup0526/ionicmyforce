import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class SurveyQuestionnaireDetailScheme {
  static readonly table: string = 'SurveyQuestionnaireDetail';
  static readonly sfdcTable: string = 'SurveyQuestionnaireDetail__c';
  static readonly description: string = 'SurveyQuestionnaireDetail';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    accountid: new Field('Account__c', 'accountid', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    customerId: new Field('Customer__c', 'customerId', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    surveyquestionnaireid: new Field('Survey_Questionnaire__c', 'surveyquestionnaireid', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    realvalue: new Field('Real_Value__c', 'realvalue', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    question: new Field('Question__c', 'question', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    stringrealvalue: new Field('StringRealValue__c', 'stringrealvalue', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    numberrealvalue: new Field('NumberRealValue__c', 'numberrealvalue', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    surveyid: new Field('Survey__c', 'surveyid', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    noncustomervalue: new Field('Non_Customer__c', 'noncustomervalue', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    remoteCustomerName: new Field('Customer__r.Name', 'remoteCustomerName', new FieldOption()
      .setIndexWithType('string')),

    cusomerName: new Field('', 'cusomerName', new FieldOption()
      .setIndexWithType('string')),


  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(SurveyQuestionnaireDetailScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(SurveyQuestionnaireDetailScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(SurveyQuestionnaireDetailScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(SurveyQuestionnaireDetailScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(SurveyQuestionnaireDetailScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(SurveyQuestionnaireDetailScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(SurveyQuestionnaireDetailScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(SurveyQuestionnaireDetailScheme.fields);
  }
}
