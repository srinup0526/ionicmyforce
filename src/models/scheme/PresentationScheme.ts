import {Field} from '../base/Field';
import {FieldOption} from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class PresentationScheme {
  static readonly table: string = 'Presentation';
  static readonly sfdcTable: string = 'Presentation__c';
  static readonly description: string = 'Presentation';

  static fields = {
    id: new Field('Id', 'id', new FieldOption().setIndexWithType('string')),

    name: new Field('Name', 'name', new FieldOption()),

    description: new Field('Description__c', 'description', new FieldOption()),

    availableVersion: new Field('Version__c', 'availableVersion', new FieldOption()),

    url: new Field('DownloadUrl__c', 'url', new FieldOption()),

    iconPath: new Field('', 'iconPath', new FieldOption()),

    currentVersion: new Field('', 'currentVersion', new FieldOption()),

    active: new Field('Active__c', 'active', new FieldOption()
      .setIndexWithType('string')),

    lastModifiedDate: new Field('LastModifiedDate', 'lastModifiedDate', new FieldOption()
      .setIndexWithType('string')),

    lastSyncDate: new Field('', 'lastSyncDate', new FieldOption()
      .setIndexWithType('string'))
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(PresentationScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(PresentationScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(PresentationScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(PresentationScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(PresentationScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(PresentationScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(PresentationScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(PresentationScheme.fields);
  }
}
