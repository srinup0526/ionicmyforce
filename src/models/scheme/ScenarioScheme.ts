import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class ScenarioScheme {
  static readonly table: string = 'Scenario';
  static readonly description: string = 'Scenario';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    name: new Field('', 'name', new FieldOption()
      .setIndexWithType('string')),

    cobaltVersion: new Field('', 'cobaltVersion', new FieldOption()),

    typeOfVersion: new Field('', 'typeOfVersion', new FieldOption()),

    structure: new Field('', 'structure', new FieldOption()),

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(ScenarioScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(ScenarioScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(ScenarioScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(ScenarioScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(ScenarioScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(ScenarioScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(ScenarioScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(ScenarioScheme.fields);
  }
}
