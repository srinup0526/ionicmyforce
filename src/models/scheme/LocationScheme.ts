import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class LocationScheme {
  static readonly table: string = 'Location';
  static readonly sfdcTable: string = 'Geo_Location__c';
  static readonly description: string = 'Location';

  static readonly LOCATION_TRACKER_STATUS_ON:string = 'On';
  static readonly LOCATION_TRACKER_STATUS_OFF:string = 'Off';
  static readonly LOCATION_REQUEST_RESULT_OK:string = 'OK';
  static readonly LOCATION_REQUEST_RESULT_PERMISSION_DENIED:string = 'PERMISSION_DENIED';
  static readonly LOCATION_REQUEST_RESULT_POSITION_UNAVAILABLE:string = 'POSITION_UNAVAILABLE';
  static readonly LOCATION_REQUEST_RESULT_TIMEOUT:string = 'TIMEOUT';
  static readonly LOCATION_TYPE_START:string = 'Start';
  static readonly LOCATION_TYPE_END:string = 'End';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    callReport: new Field('CallReport__c', 'callReport', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    datetime: new Field('DateTime__c', 'datetime', new FieldOption()
      .setUploadState()),

    gpsStatus: new Field('GPSStatus__c', 'gpsStatus', new FieldOption()
      .setUploadState()),

    latitude: new Field('Location__latitude__s', 'latitude', new FieldOption()
      .setUploadState()),

    longitude: new Field('Location__longitude__s', 'longitude', new FieldOption()
      .setUploadState()),

    type: new Field('Type__c', 'type', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    result: new Field('Result__c', 'result', new FieldOption()
      .setUploadState())
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(LocationScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(LocationScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(LocationScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(LocationScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(LocationScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(LocationScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(LocationScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(LocationScheme.fields);
  }
}
