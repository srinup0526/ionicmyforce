import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';


export class MedicalQueryScheme {
  static readonly table: string = 'MedicalQuery';
  static readonly sfdcTable: string = 'Medical_Query__c';
  static readonly description: string = 'Medical Query';

  static editableStatuses: Array<string> = ['Draft', 'Submitted to rep', 'Rejected'];
  static readonly APPROVAL_STATUS_DRAFT: string = 'Draft';
  static readonly APPROVAL_STATUS_SUBMITTED_OFFLINE: string = 'Submitted Offline';
  static readonly APPROVAL_STATUS_SUBMITTED_TO_REP: string = 'Submitted to rep';
  static readonly APPROVAL_STATUS_SUBMITTED_TO_MEDICAL: string = 'Submitted to medical';
  static readonly APPROVAL_STATUS_CLOSED: string = 'Closed';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),

    createdDateTime: new Field('Creation_Date_Time__c', 'createdDateTime', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    customer: new Field('Customer__c', 'customer', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    noncustomer: new Field('Non_Customer__c', 'noncustomer', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    specialty: new Field('MQ_Speciality__c', 'specialty', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    remoteCustomerName: new Field('Customer__r.Name', 'remoteCustomerName', new FieldOption()),

    customerName: new Field('', 'customerName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    organization: new Field('Organization__c', 'organization', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    remoteOrganizationName: new Field('Organization__r.Name', 'remoteOrganizationName', new FieldOption()),

    organizationName: new Field('', 'organizationName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    remoteEmail: new Field('Customer__r.Email', 'remoteEmail', new FieldOption()),

    email: new Field('Email1__c', 'email', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    remoteMobile: new Field('Customer__r.Phone', 'remoteMobile', new FieldOption()),

    mobile: new Field('', 'mobile', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    medicalContact: new Field('Mylan_Medical_Contact__c', 'medicalContact', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    remoteMedicalContactName: new Field('Mylan_Medical_Contact__r.Name', 'remoteMedicalContactName', new FieldOption()),

    medicalContactName: new Field('', 'medicalContactName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    pharmaProduct: new Field('Pharma_Product__c', 'pharmaProduct', new FieldOption()
      .setIndexWithType('string')
      .setUploadState()),

    remotePharmaProductName: new Field('Pharma_Product__r.Name', 'remotePharmaProductName', new FieldOption()),

    pharmaProductName: new Field('', 'pharmaProductName', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()),

    queryDescription: new Field('Query_description__c', 'queryDescription', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    status: new Field('Status__c', 'status', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    typeofquery: new Field('Type_of_Medical_Query__c', 'typeofquery', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    user: new Field('User__c', 'user', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),
    
    remoteUserName: new Field('User__r.Name', 'remoteUserName', new FieldOption()),

    userName: new Field('', 'userName', new FieldOption()),

    mylanMedicalContactPersons: new Field('Mylan_Medical_Contact_Persons__c', 'mylanMedicalContactPersons', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

    mylanMedicalContactPersonEmails: new Field('Mylan_Medical_Contact_Persons_Email__c', 'mylanMedicalContactPersonEmails', new FieldOption()
      .setIndexWithType('string')
      .setSearchState()
      .setUploadState()),

  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(MedicalQueryScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(MedicalQueryScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(MedicalQueryScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(MedicalQueryScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(MedicalQueryScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(MedicalQueryScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(MedicalQueryScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(MedicalQueryScheme.fields);
  }
}
