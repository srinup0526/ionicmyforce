import { Field } from '../base/Field';
import { FieldOption } from '../base/FieldOption';
import { SchemeParser } from './SchemeParser';
import { Utils } from '../../utils/Utils';

export class TargetFrequencyScheme {
  static readonly table:string = 'TargetFrequency';
  static readonly sfdcTable:string = 'TF__c';
  static readonly description:string = 'Target Frequency';

  static fields = {
    id: new Field('Id', 'id', new FieldOption()),
  
    marketingCycleSfId: new Field('Marketing_Cycle__c', 'marketingCycleSfId', new FieldOption()
      .setIndexWithType('string')),
  
    targetSfId: new Field('Target__c', 'targetSfId', new FieldOption()),
  
    customerSfId: new Field('Customer__c', 'customerSfId', new FieldOption()
      .setIndexWithType('string')),
  
    isActive: new Field('Active_TMF__c', 'isActive', new FieldOption()),
  
    actualCallsCount: new Field('Actual_Calls__c', 'actualCallsCount', new FieldOption()),
  
    mcTargetFrequency: new Field('MC_Target_Frequency_c__c', 'mcTargetFrequency', new FieldOption()),
  
    isPharmacist: new Field('IsPharmacist__c', 'isPharmacist', new FieldOption()),
  
    priority: new Field('Priority__c', 'priority', new FieldOption()),
  
    targetCycleFrequency: new Field('Target_Cycle_Frequency__c', 'targetCycleFrequency', new FieldOption()),
  
    lastCallReportDate: new Field('Last_Call_Report_Date__c', 'lastCallReportDate', new FieldOption()
      .setIndexWithType('string')),
  
    medrepId: new Field('Target__r.MedRep__r.Id', 'medrepId', new FieldOption()
      .setIndexWithType('string')),
  
    medrepFirstName: new Field('Target__r.MedRep__r.FirstName', 'medrepFirstName', new FieldOption()),
  
    medrepLastName: new Field('Target__r.MedRep__r.LastName', 'medrepLastName', new FieldOption())
  };

  static get indexSpec() {
    return SchemeParser.getIndexSpec(TargetFrequencyScheme.fields);
  }

  static get uploadableFields() {
    return SchemeParser.getUploadableFields(TargetFrequencyScheme.fields);
  }

  static get sfdcFields() {
    return SchemeParser.getSfdcFields(TargetFrequencyScheme.fields);
  }

  static get excludableFields() {
    return SchemeParser.getExcludableFields(TargetFrequencyScheme.fields);
  }

  static get includableFields() {
    return SchemeParser.getIncludableFields(TargetFrequencyScheme.fields);
  }

  static get isToLabel() {
    return SchemeParser.isToLabel(TargetFrequencyScheme.fields);
  }

  static hasSearchable() {
    return SchemeParser.hasSearchable(TargetFrequencyScheme.fields);
  }

  static searchableSchema() {
    return SchemeParser.searchableSchema(TargetFrequencyScheme.fields);
  }
}
