import {Entity} from './base/Entity';
import {ContactScheme} from './scheme/ContactScheme';
import {Reference} from "./Reference";
import {ReferenceCollection} from "../collections/references/ReferenceCollection";
import {OrganizationsCollection} from "../collections/OrganizationsCollection";


export class Contact extends Entity {
  public id: string;
  public name: string;
  public firstName: string;
  public lastName: string;
  public jobTitle: string;
  public accountId: string;
  public recordType: string;
  public subtype: string;
  public status: string;
  public organizationSfId: string;
  public organizationName: string;
  public gender: string;
  public yearOfGraduation: string;
  public mobilePhone: string;
  public homePhone: string;
  public email: string;
  public kol: string;
  public description: string;
  public priority: string;
  public isTargetCustomer: string;
  public lastDateTargetFrequency: string;
  public buSpecialty: string;
  public specialty: string;
  public remoteSpecialty: string;
  public hcpStatus: string;
  public statusDescription: string;
  public physicianId: string;
  public namedDepartment: string;
  public standardDepartment: string;
  public administrative: string;
  public professionalTitle: string;
  public academicTitle: string;
  public hcpType: string;
  public legalMedicalLicence: string;
  public workingTime: string;
  public targetDoctor: string;
  public cnKol: string;
  public kolProduct1: string;
  public kolProduct2: string;
  public speaker: string;
  public speakerProduct1: string;
  public speakerProduct2: string;
  public socialOrganizationAndTitle: string;
  public specialtyDescription: string;
  public hcpDescriptionIntroduction: string;
  public territoryCode: string;
  public mylanProductList: string;
  public nonMylanProductList: string;
  public workplace: string;
  public redFlag: string;
  public references: Array<Reference>;

  constructor(responseRecord) {
    super(ContactScheme.fields, responseRecord);
  }
  
  public fullName(): string {
    return [
      this.lastName,
      this.firstName
    ]
    .filter(field => !!field)
    .join(' ');
  }
  
  public getIdsForTableColumn() {
    const cssClass = ~ContactScheme.activeRows.indexOf(this.id) ? 'active' : '';
    
    return `<span class="check-box ${cssClass}"><span class="arrow"></span><span class="contact-id">${this.id}</span></span>  `;
  }

  public getIdForTableColumn() {
    const cssClass = ContactScheme.activeRow == this.id ? 'active' : '';

    return `<span class="check-box ${cssClass}"><span class="arrow"></span><span class="contact-id">${this.id}</span></span>  `;
  }
  
  public getOrganizationByReference(): Promise<any> {
    let organizationsCollection = new OrganizationsCollection();
    
    return this.getReferences()
      .then((references) => {
        if(references.length == 1 && references[0].organizationSfId) {
          return references[0].organizationSfId;
        }
        
        return this.getOrganizationIdByPrimary(references);
      })
      .then((organizationId) => {
        if(organizationId) {
          return organizationsCollection.fetchEntityById(organizationId);
        }
      })
  }
  
  public getReferences() {
    let fieldsWithValues = {};
    let referenceCollection = new ReferenceCollection();
    
    fieldsWithValues[referenceCollection.scheme.fields.contactSfId.sfdc] = this.id;
    
    return referenceCollection.fetchAllWhere(fieldsWithValues)
      .then(response => referenceCollection.getAllEntitiesFromResponse(response))
      .then((references) => {
        this.references = references;
        
        return references;
      });
  }
  
  private getOrganizationIdByPrimary(references) {
    let primaryReferences = references.filter((reference) => {
      return reference.isPrimary;
    });
    
    if(primaryReferences.length == 1 && primaryReferences[0].organizationSfId){
      return primaryReferences[0].organizationSfId;
    }
  }
}
