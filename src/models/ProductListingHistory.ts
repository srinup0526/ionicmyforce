import {Entity} from './base/Entity';
import {ProductListingHistoryScheme} from './../models/scheme/ProductListingHistoryScheme';

export class ProductListingHistory extends Entity {
  id: string;
  createdById: string;
  createdByName: string;
  createdDate: string;
  changedField: string;
  isDeleted:string;
  newValue: string;
  oldValue: string;
  parentId:string;

  constructor(responseRecord) {
    super(ProductListingHistoryScheme.fields, responseRecord);
  }
}
