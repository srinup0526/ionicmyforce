import {Entity} from './base/Entity';
import {OrderLineScheme} from './scheme/OrderLineScheme';


export class OrderLine extends Entity {
  public id: string;
  public orderId: string;
  public quantity: string;
  public salesAmount: string;
  public salesPrice: string;
  public SKUName: string;
  public productLot: string;
  public sales1MonthAgo: string;
  public sales2MonthAgo: string;
  public sales3MonthAgo: string;
  public sales4MonthAgo: string;
  public salesYTDC: string;
  
  
  constructor(responseRecord) {
    super(OrderLineScheme.fields, responseRecord);
  }
}
