import { Entity } from './base/Entity';
import { JointVisitPlanScheme } from './scheme/JointVisitPlanScheme';
import { Utils } from '../utils/Utils';


export class JointVisitPlan extends Entity {
  id: string;
  name: string;
  status: string;
  medrep: string;
  medrepName: string;
  flm: string;
  flmName: string;
  coachingdate: string;
  scoretotal: string;
  developmentplan: string;
  developmentPlanName: string;


  constructor(responseRecord) {
    super(JointVisitPlanScheme.fields, responseRecord);
  }

  public getJoinDateFormat()
  {
    return Utils.originalDate(this.coachingdate);
  }

}
