import {Entity} from './base/Entity';
import {RedFlagScheme} from './scheme/RedFlagScheme';


export class RedFlag extends Entity {
  id: string;
  redFlagType: string;
  customerName: string;
  email: string;
  contactNumber: string;
  description: string;
  CallReportId: string;
  photo: string;


  constructor(responseRecord) {
    super(RedFlagScheme.fields, responseRecord);
  }
}
