import {Entity} from './base/Entity';
import {FaqAttachmentScheme} from './scheme/FaqAttachmentScheme';

export class FaqAttachment extends Entity {
  id: string;
  body: string;
  bodyLength: string;
  contentType: string;
  description: string;
  isPrivate: string;
  title: string;
  ownerId: string;
  attachedByName: string;
  parentId: string;
  lastModify: string;
  localParentId: string;

  constructor(responseRecord) {
    super(FaqAttachmentScheme.fields, responseRecord);
  }
}
