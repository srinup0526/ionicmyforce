import {Entity} from './base/Entity';
import {ProductScheme} from './../models/scheme/ProductScheme';

export class Product extends Entity {
  id: string;
  name: string;
  atcClass: string;
  presentationId: string;

  constructor(responseRecord) {
    super(ProductScheme.fields, responseRecord);
  }

  public getName() {
    return [
      this.name
    ]
    .filter(field => !!field)
    .join(' ');
  }

  public getIdForTableColumn() {
    const cssClass = ~ProductScheme.activeRows.indexOf(this.id) ? 'active' : '';
    console.log("cssClass",cssClass);
    
    return `<span class="check-box ${cssClass}"><span class="arrow"></span><span class="contact-id">${this.id}</span></span>  `;
  }
}
