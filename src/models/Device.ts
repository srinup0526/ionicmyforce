import {Entity} from './base/Entity';
import {DeviceScheme} from './scheme/DeviceScheme';


export class Device extends Entity {
  public id: string;
  public deviceId: string;
  public erased: string;
  public requestErase: string;
  public lastSyncronisation: string;
  public lastUserSfid: string;
  public model: string;
  public osVersion: string;
  public version: string;
  public lastDebugLog: string;

  constructor(responseRecord) {
    super(DeviceScheme.fields, responseRecord);
  }
}
