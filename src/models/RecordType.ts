import {Entity} from './base/Entity';
import {RecordTypeScheme} from './scheme/RecordRypeScheme';


export class RecordType extends Entity {
  public id: string;
  public name: string;
  public developerName: string;
  public description: string;
  public sobjectRecordType: string;
  public isActive: string;
  public objectType:string;
  public isAvailable: string;
  public isPersonType: string;
  public lastModifiedDate: string;
  public lastSyncDate: string;

  constructor(responseRecord) {
    super(RecordTypeScheme.fields, responseRecord);
  }
}
