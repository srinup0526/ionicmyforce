import {Entity} from './base/Entity';
import {OrganizationScheme} from './scheme/OrganizationScheme';


export class Organization extends Entity {
  id: string;
  externalId: string;
  name: string;
  brickSfId: string;
  status: string;
  subtype: string;
  recordType: string;
  recordTypeId: string;
  remoteRecordType: string;
  country: string;
  city: string;
  state: string;
  address: string;
  postalCode: string;
  phone: string;
  specialty1: string;
  specialty2: string;
  isPersonAccount: string;
  juridicGroup: string;
  globalPriority: string;
  organizationId: string;
  alias: string;
  statusDescription: string;
  organizationType: string;
  organizationProperty: string;
  hospitalGrade: string;
  hospitalLevel: string;
  targetHospital: string;
  territoryCode: string;


  constructor(responseRecord) {
    super(OrganizationScheme.fields, responseRecord);
  }

  nameAndAddress() {
    return `${this.name} ${this.fullAddress()}`;
  }

  fullAddress() {
    return [
      this.address,
      this.city,
      this.state
    ]
      .filter((field) => !!field)
      .join(' ');
  }
  
  getFullAdressForOrder() {
    return [
      this.address,
      this.city,
      this.state,
      this.postalCode,
      this.country
    ]
      .filter((field) => !!field)
      .join(' ');
  }
  
  getIdForTableColumn() {
    const cssClass = OrganizationScheme.activeRow == this.id ? 'active' : '';
    
    return `<span class="check-box ${cssClass}"><span class="arrow"></span><span class="account-id">${this.id}</span></span>  `;
  }
 
  
  getReferences() {
    // if @references then $.when @references
    // else
    //   collection = if Settings.getInstance().isChinaUser() then new NonTargetReferencesCollection() else new ReferencesCollection()
    //   fieldsWithValues = {}
    //   fieldsWithValues[collection.model.sfdc.organizationSfId] = @id
    //   collection.fetchAllWhere(fieldsWithValues)
    //   .then((response) => collection.getAllEntitiesFromResponse response)
    //   .then((@references) => @references)
  }

  getProductSegmentations() {
    // ProductSegmentationsCollection = require('models/bll/product-segmentations-collection');
    // productSegmentationsCollection = new ProductSegmentationsCollection();
    // fieldsWithValues = {}
    // fieldsWithValues[productSegmentationsCollection.model.sfdc.accountId] = @id
    // productSegmentationsCollection.fetchAllWhere(fieldsWithValues)
    // .then productSegmentationsCollection.getAllEntitiesFromResponse
  }

  getListingProductByStatus() {
    // ProductListingsCollection = require('models/bll/product-listings-collection');
    // productListingsCollection = new ProductListingsCollection();
    // fieldsWithValues = {}
    // fieldsWithValues[productListingsCollection.model.sfdc.accountId] = @id
    // productListingsCollection.fetchAllWhere(fieldsWithValues)
    // .then productListingsCollection.getAllEntitiesFromResponse
  }

  getActivitiesInMarketingCycle() {
    // ConfigurationManager = require 'db/configuration-manager'
    // ConfigurationManager.getConfig('numberOfMonthsForCalls')
    // .then (numberOfMonthsForCalls) =>
    //   if numberOfMonthsForCalls is 0 and marketingCycle?
    //     @_getActivitiesByStartEndDate marketingCycle.startDate, marketingCycle.endDate
    //   else
    //     @_getActivitiesByStartEndDate moment().add(-numberOfMonthsForCalls, 'month'), moment()
  }

  _getActivitiesByStartEndDate() {
    // collection = new CallReportsCollection
    // organisationValue = {}
    // startDateValue = {}
    // endDateValue = {}
    // organisationValue[collection.model.sfdc.organizationSfId] = @id
    // startDateValue[collection.model.sfdc.dateTimeVisitStart] = Utils.originalStartOfDate startDate
    // endDateValue[collection.model.sfdc.dateTimeVisitStart] = Utils.originalEndOfDate endDate
    // query = new Query().selectFrom(collection.model.table).where(organisationValue).and().where(startDateValue, Query.GRE).and().where(endDateValue, Query.LRE).orderBy([collection.model.sfdc.dateTimeVisitStart])
    // collection.fetchWithQuery(query).then collection.getAllEntitiesFromResponse
  }
  getActivities() {
    // if @activities then $.when @activities
    // else
    //   collection = new CallReportsCollection
    //   fieldsWithValues = {}
    //   fieldsWithValues[collection.model.sfdc.organizationSfId] = @id
    //   collection.fetchAllWhere(fieldsWithValues)
    //   .then((response) => collection.getAllEntitiesFromResponse response)
    //   .then((@activities) => @activities)
  }

  getTargetReferences() {
    // if @targetReferences then $.when @targetReferences
    // else
    //   collection = new TargetReferencesCollection
    //   organisationValue = {}
    //   organisationValue[collection.model.sfdc.organizationSfId] = @id
    //   collection.fetchAllWhere(organisationValue)
    //   .then(collection.getAllEntitiesFromResponse)
    //   .then((@targetReferences) => @targetReferences)
  }

  hasAnyTargetReferences() {
    // @getTargetReferences().then (@targetReferences) => @targetReferences?.length > 0
  }
}
