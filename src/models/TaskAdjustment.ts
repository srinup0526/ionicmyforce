import {Entity} from './base/Entity';
import {TaskAdjustmentScheme} from './scheme/TaskAdjustmentScheme'


export class TaskAdjustment extends Entity {
  id: string;
  callReportSfId: string;
  numberRealValue: string;
  productItemSfId: string;
  stringRealValue: string;
  remotePromotionTaskSfId: string;
  promotionTaskAccountSfId: string;
  promotionTaskSfId: string;
  isModifiedInTrade: string;
  isModifiedInCall: string;
  clmToolId:string;

  constructor(responseRecord) {
    super(TaskAdjustmentScheme.fields, responseRecord);
  }
}
