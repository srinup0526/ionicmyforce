import {Entity} from './base/Entity';
import {MedicalCommentScheme} from './scheme/MedicalCommentScheme';


export class MedicalComment extends Entity {
  id: string;
  commentDescription: string;
  commentCreatedDateTime: string;
  commentFeedback: string;
  mqId: string;
  createdBy: string;
  remoteCreatedByName: string;
  createdUserName:string;

  constructor(responseRecord) {
    super(MedicalCommentScheme.fields, responseRecord);
  }
}
