import { FieldOption } from './FieldOption';
import { Entity } from './Entity';


export class Field {
  public sfdc: string;
  public local: string;
  public options: FieldOption;
  private context: Entity;

  constructor(sfdc: string, local: string, options: FieldOption) {
    this.sfdc = sfdc;
    this.local = local;
    this.options = options;
  }
}
