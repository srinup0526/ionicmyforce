
export class FieldOption {
  public indexWithType: string;
  public search: boolean;
  public upload: boolean;
  public include: string;
  public exclude: string;
  public toLabel: boolean;

  constructor() {}

  setIndexWithType(type: string) {
    this.indexWithType = type;
    return this;
  }

  setSearchState() {
    this.search = true;
    return this;
  }

  setUploadState() {
    this.upload = true;
    return this;
  }

  setInclude(condition: string) {
    this.include = condition;
    return this;
  }

  setExclude(condition: string) {
    this.exclude = condition;
    return this;
  }

  setToLabelState() {
    this.toLabel = true;
    return this;
  }
}
