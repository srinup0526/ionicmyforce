import {Field} from './Field';

declare let Force;


export class Entity {

  public static readonly DEFAULT_FIELDS = [
    '_soupEntryId',
    '_soupLastModifiedDate',
    '__local__',
    '__locally_created__',
    '__locally_deleted__',
    '__locally_updated__',
    'attributes',
    'sobjectType'
  ];

  public dynamicFields: Array<string> = [];

  public attributes = {};

  public fields: { [field: string]: Field; };


  constructor(scheme, responseRecord) {
    this.generateConcreteProperties(scheme, responseRecord);
    this.generateDefaultProperties(responseRecord);
  }

  public sync(method, entity, options) {
    const forceSObject = new Force.SObject();
    return forceSObject.sync(method, entity, options);
  }

  private generateConcreteProperties(scheme, record) {
    Object.keys(scheme).forEach((key) => {
      if(record[key] == null || record[key] == undefined) {
        this[key] = record[scheme[key].sfdc];
      } else {
        this[key] = record[key];
      }
    });
  }

  private generateDefaultProperties(record) {
    Entity.DEFAULT_FIELDS.forEach((key) => {
      this[key] = record[key];
    });
  }
}
