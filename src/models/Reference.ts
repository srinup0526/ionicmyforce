import { Entity } from './base/Entity';
import { ReferenceScheme } from './scheme/ReferenceScheme';


export class Reference extends Entity {
  public id: string;
  public name: string;
  public contactSfId: string;
  public organizationSfId: string;
  public contactAccountId: string;
  public contactRecordType: string;
  public remoteSubtype: string;
  public subtype: string;
  public isPrimary: string;
  public status: string;
  public organizationName: string;
  public organizationCity: string;
  public organizationPhone: string;
  public organizationCountry: string;
  public organizationAddress: string;
  public organizationBrick: string;
  public contactFirstName: string;
  public contactLastName: string;
  public priority: string;
  public buSpecialty: string;
  public specialty: string;
  public atCalls: string;
  public lastCall: string;
  
  public contact;
  public organization;
  
  constructor(responseRecord) {
    super(ReferenceScheme.fields, responseRecord);
    
    this.contact = null;
    this.organization = null;
  }
  
  public getIdForTableColumn() {
    const cssClass = ReferenceScheme.activeRow == this.id ? 'active' : '';
    
    return `<span class="check-box ${cssClass}"><span class="arrow"></span><span class="reference-id">${this.id}</span></span>  `;
  }
  
  public organizationNameAndAddress() {
    return `${this.organizationName || ''} <br/> ${this.organizationAddress || ''} ${this.organizationCity || ''}`;
  }
  
  public organizationNameWithAddress() {
    return  `${this.organizationName || ''} - ${this.organizationAddress || ''} ${this.organizationCity || ''}`;
  }
  
  public contactFullName() {
    return `${this.contactLastName || ''} ${this.contactFirstName || ''}`;
  }
  
  public contactFullNameWithType() {
    return `${this.contactLastName || ''} ${this.contactFirstName || ''} <br/> ${this.contactRecordType || ''}`;
  }
  
  public contactFullNameHongkong() {
    return `${this.contactFirstName || ''} ${this.contactLastName || ''}`;
  }
  
  public isActive() {
    return this.status == ReferenceScheme.STATUS_ACTIVE;
  }
  
  public getStatus() {
    // TODO: after Reference Picklist creating
    // ReferencePicklistManager = require 'db/picklist-managers/reference-picklist-manager'
    // new ReferencePicklistManager().getStatusLabelByValue this.status
  }

  // TODO: assign on 'didFinishDownloading'
  public getContact() {
    if(!this.contact) {
      // ContactsCollection = require 'models/bll/contacts-collection'
      // contactsCollection = new ContactsCollection
      // return contactsCollection.fetchEntityById this.contactSfId
    }
    
    return Promise.resolve(this.contact);
  }
  
  public getProductSegmentations() {
    // TODO: after add model && scheme && collection ProductSegmentations
    // ProductSegmentationsCollection = require('models/bll/product-segmentations-collection');
    // productSegmentationsCollection = new ProductSegmentationsCollection();
    // fieldsWithValues = {};
    // fieldsWithValues[productSegmentationsCollection.model.sfdc.accountId] = this.contactAccountId;
    // productSegmentationsCollection.fetchAllWhere(fieldsWithValues)
    //   .then productSegmentationsCollection.getAllEntitiesFromResponse
  }
  
  public getOrganization() {
    if (this.organization) {
      return Promise.resolve(this.organization);
    }
  
    // TODO: after add model && scheme && collection Organizations
    // OrganizationsCollection = require('models/bll/organizations-collection');
    // collection = new OrganizationsCollection();
    //
    // collection.fetchEntityById(this.organizationSfId)
    //   .then (this.organization) => this.organization
  }
}
