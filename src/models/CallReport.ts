import {Entity} from './base/Entity';
import {CallReportScheme} from './scheme/CallReportScheme';
import {UsersCollection} from '../collections/UsersCollection';
import { ContactsCollection } from '../collections/ContactsCollection';
import { Utils } from './../utils/Utils';
import { ProductsCollection } from '../collections/ProductsCollection';



export class CallReport extends Entity {
  contactCollection:ContactsCollection;
  id: string;
  organizationSfId: string;
  contactSfId: string;
  mtp: string;
  dateTimePlanned: string;
  dateTimeVisitStart: string;
  dateTimeVisitEnd: string;
  createdFromMobile: boolean;
  createdOffline: boolean;
  isTargetCall: string;
  type: string;
  recordTypeId: string;
  duration: string;
  activityType:string;
  targetPriority: string;
  coachingVisit: string;
  coachingVisitUserSfid: string;
  userSfid: string;
  generalComments: string;
  callObjective: string;
  nextCallObjective: string;
  promotionalItemsPrio1: string;
  prio1ProductSfid: string;
  noteForPrio1: string;
  indCustomer:string;
  indCustomerRecordType:string;

  prio1MarketingMessage1: string;
  prio1MarketingMessage2: string;
  prio1MarketingMessage3: string;
  prio1MarketingMessage4: string;
  prio1MarketingMessage5: string;
  prio1ReactionsToMarketingMessage1: string;
  prio1ReactionsToMarketingMessage2: string;
  prio1ReactionsToMarketingMessage3: string;
  prio1ReactionsToMarketingMessage4: string;
  prio1ReactionsToMarketingMessage5: string;
  patientProfile1: string;
  prio1Classification: string;


  promotionalItemsPrio2: string;
  prio2ProductSfid: string;
  noteForPrio2: string;
  prio2MarketingMessage1: string;
  prio2MarketingMessage2: string;
  prio2MarketingMessage3: string;
  prio2MarketingMessage4: string;
  prio2MarketingMessage5: string;
  prio2ReactionsToMarketingMessage1: string;
  prio2ReactionsToMarketingMessage2: string;
  prio2ReactionsToMarketingMessage3: string;
  prio2ReactionsToMarketingMessage4: string;
  prio2ReactionsToMarketingMessage5: string;
  patientProfile2: string;
  prio2Classification: string;

  promotionalItemsPrio3: string;
  prio3ProductSfid: string;
  noteForPrio3: string;
  prio3MarketingMessage1: string;
  prio3MarketingMessage2: string;
  prio3MarketingMessage3: string;
  prio3MarketingMessage4: string;
  prio3MarketingMessage5: string;
  prio3ReactionsToMarketingMessage1: string;
  prio3ReactionsToMarketingMessage2: string;
  prio3ReactionsToMarketingMessage3: string;
  prio3ReactionsToMarketingMessage4: string;
  prio3ReactionsToMarketingMessage5: string;
  patientProfile3: string;
  prio3Classification: string;

  promotionalItemsPrio4: string;
  prio4ProductSfid: string;
  noteForPrio4: string;
  prio4MarketingMessage1: string;
  prio4MarketingMessage2: string;
  prio4MarketingMessage3: string;
  prio4MarketingMessage4: string;
  prio4MarketingMessage5: string;
  prio4ReactionsToMarketingMessage1: string;
  prio4ReactionsToMarketingMessage2: string;
  prio4ReactionsToMarketingMessage3: string;
  prio4ReactionsToMarketingMessage4: string;
  prio4ReactionsToMarketingMessage5: string;
  patientProfile4: string;
  prio4Classification: string;


  promotionalItemsPrio5: string;
  prio5ProductSfid: string;
  noteForPrio5: string;
  prio5MarketingMessage1: string;
  prio5MarketingMessage2: string;
  prio5MarketingMessage3: string;
  prio5MarketingMessage4: string;
  prio5MarketingMessage5: string;
  prio5ReactionsToMarketingMessage1: string;
  prio5ReactionsToMarketingMessage2: string;
  prio5ReactionsToMarketingMessage3: string;
  prio5ReactionsToMarketingMessage4: string;
  prio5ReactionsToMarketingMessage5: string;
  patientProfile5: string;
  prio5Classification: string;

  promotionalItemsPrio6: string;
  prio6ProductSfid: string;
  noteForPrio6: string;
  prio6MarketingMessage1: string;
  prio6MarketingMessage2: string;
  prio6MarketingMessage3: string;
  prio6MarketingMessage4: string;
  prio6MarketingMessage5: string;
  prio6ReactionsToMarketingMessage1: string;
  prio6ReactionsToMarketingMessage2: string;
  prio6ReactionsToMarketingMessage3: string;
  prio6ReactionsToMarketingMessage4: string;
  prio6ReactionsToMarketingMessage5: string;
  patientProfile6: string;
  prio6Classification: string;

  promotionalItemsPrio7: string;
  prio7ProductSfid: string;
  noteForPrio7: string;
  prio7MarketingMessage1: string;
  prio7MarketingMessage2: string;
  prio7MarketingMessage3: string;
  prio7MarketingMessage4: string;
  prio7MarketingMessage5: string;
  prio7ReactionsToMarketingMessage1: string;
  prio7ReactionsToMarketingMessage2: string;
  prio7ReactionsToMarketingMessage3: string;
  prio7ReactionsToMarketingMessage4: string;
  prio7ReactionsToMarketingMessage5: string;
  patientProfile7: string;
  prio7Classification: string;

  promotionalItemsPrio8: string;
  prio8ProductSfid: string;
  noteForPrio8: string;
  prio8MarketingMessage1: string;
  prio8MarketingMessage2: string;
  prio8MarketingMessage3: string;
  prio8MarketingMessage4: string;
  prio8MarketingMessage5: string;
  prio8ReactionsToMarketingMessage1: string;
  prio8ReactionsToMarketingMessage2: string;
  prio8ReactionsToMarketingMessage3: string;
  prio8ReactionsToMarketingMessage4: string;
  prio8ReactionsToMarketingMessage5: string;
  patientProfile8: string;
  prio8Classification: string;

  promotionalItemsPrio9: string;
  prio9ProductSfid: string;
  noteForPrio9: string;
  prio9MarketingMessage1: string;
  prio9MarketingMessage2: string;
  prio9MarketingMessage3: string;
  prio9MarketingMessage4: string;
  prio9MarketingMessage5: string;
  prio9ReactionsToMarketingMessage1: string;
  prio9ReactionsToMarketingMessage2: string;
  prio9ReactionsToMarketingMessage3: string;
  prio9ReactionsToMarketingMessage4: string;
  prio9ReactionsToMarketingMessage5: string;
  patientProfile9: string;
  prio9Classification: string;

  promotionalItemsPrio10: string;
  prio10ProductSfid: string;
  noteForPrio10: string;
  prio10MarketingMessage1: string;
  prio10MarketingMessage2: string;
  prio10MarketingMessage3: string;
  prio10MarketingMessage4: string;
  prio10MarketingMessage5: string;
  prio10ReactionsToMarketingMessage1: string;
  prio10ReactionsToMarketingMessage2: string;
  prio10ReactionsToMarketingMessage3: string;
  prio10ReactionsToMarketingMessage4: string;
  prio10ReactionsToMarketingMessage5: string;
  patientProfile10: string;
  prio10Classification: string;

  signature: string;
  signatureDate: string;
  callWithIPad: boolean;
  realCallDuration: number;
  patientSupportProgram: string;
  portfolioFeedback: string;
  patientSupportProgramComments: string;
  fullPortfolioPresentationReminder: string;
  userFirstName: string;

  userLastName: string;
  remoteContactFirstName: string;
  remoteContactLastName: string;
  contactRecordType: string;
  contactFirstName: string;
  contactLastName: string;
  remoteOrganizationName: string;
  organizationName: string;
  organizationCity: string;

  organizationAddress: string;
  atCalls: any;
  typeOfVisit: string;
  isSandbox: boolean;
  clmToolId: string;
  managerComments: string;
  jointVisitParticipants: string;
  status: string;
  isInviteToEvent: string;
  isOneToOneCalls: string;
  convertedFromAppointment: string;
  customerStatus: string;
  cnTypeOfVisit: string;
  cnPurposeOfVisit: string;
  coachingVisitUser: any;
  isTargetCustomer:any;
  
  public specialty;
  

  constructor(responseRecord) {
    super(CallReportScheme.fields, responseRecord);
    this.contactCollection = new ContactsCollection()
    this.specialty = null;
  }

  userFullName()
  {
    //console.log("userFullName",`${this.userLastName || ''} ${this.userFirstName || ''}`);
    return `${this.userLastName || ''} ${this.userFirstName || ''}`;
  }

  contactFullName()
  {
    return `${this.contactLastName || ''} ${this.contactFirstName || ''}`;
  }

  organizationNameAndAddress()
  {
    //console.log("organizationNameAndAddress",`${this.organizationName || ''} </br> ${this.organizationAddress || ''} ${this.organizationCity || ''}`);
    return `${this.organizationName || ''} </br> ${this.organizationAddress || ''} ${this.organizationCity || ''}`;
  }

  getCoachingVisitUser()
  {
    if(this.coachingVisitUser) { return this.coachingVisitUser; } 
    else
    {
      return new UsersCollection().fetchEntityById(this.coachingVisitUserSfid)
      .then(coachingVisitUser=> { 
        //console.log("coachingVisitUser data:",coachingVisitUser); 
        return Promise.resolve(coachingVisitUser);});
    }
  }

  getProductName() {
    return new ProductsCollection().fetchEntityById(this.prio1ProductSfid)
    .then(product=> { 
      //console.log("product data:",product); 
        return Promise.resolve(product);});
  }


  public getAppointmentDateTimePlannedInFormat() {
    return Utils.formatDateTimeWithBreak(this.dateTimePlanned);
  }

  public getCallReortDateTimeActualInFormat() {
    return Utils.formatDateTimeWithBreak(this.dateTimeVisitStart);
  }

  // getIsTargetCustomer()
  // {
  //   if(this.isTargetCustomer) { return this.isTargetCustomer; } 
  //   else
  //   {
  //     return new ContactsCollection().fetchEntityById(this.contactSfId)
  //     .then(contact=> { return contact});
  //   }
  // }

  public getSpecialty()
  {
    if(this.specialty) { return this.isTargetCustomer; } 
    else
    {
      console.log("this.contactSfId",this.contactSfId);
      return this.contactCollection.fetchEntityById(this.contactSfId)
      .then(contact=> { return Promise.resolve(contact.buSpecialty);});
    }
  }

  public getPriority()
  {
    // if(CallReportScheme.specialty) { return this.isTargetCustomer; } 
    // else
    // {
     // return "A";
      console.log("this.contactSfId",this.contactSfId);
      return this.contactCollection.fetchEntityById(this.contactSfId)
      .then(contact=> { return Promise.resolve(contact.priority);});
    //}
  }

  // # TODO: assign on 'didFinishDownloading'
  // getContact: =>
  //   if @contact then $.when @contact
  //   else
  //     ContactsCollection = require 'models/bll/contacts-collection'
  //     contactsCollection = new ContactsCollection
  //     contactsCollection.fetchEntityById(@contactSfId)
  //     .then (@contact) => @contact

  // getCoachingVisitUserByField: (userField) =>
  //   if @[userField] then $.when @[userField]
  //   else
  //     UsersCollection = require 'models/bll/users-collection'
  //     new UsersCollection().fetchEntityById(@[userField])
  //     .then (user) =>
  //       @[userField] = user
  //       @[userField]

  // isAvailableToEdit: =>
  //   not @status or @status is CallReport.Statuses.Saved

  // changeStatusToSubmitted: =>
  //   @status = CallReport.Statuses.Submitted

  // getMTP: =>
  //   if @attachedMTP then $.when @attachedMTP
  //   else
  //     MTPCollection = require 'models/bll/mtp-collection'
  //     mtpCollection = new MTPCollection
  //     mtpCollection.fetchEntityById @mtp
  //     .then (@attachedMTP) => @attachedMTP

  // isDeleteAllow: =>
  //   @getMTP()
  //   .then => not @attachedMTP or @attachedMTP?.isAvailableToDeleteEventFromMTP()


  // getCnTypeOfVisitPickListValue: =>
  //   @_getChinaPickListLabelByValue(CallReport.sfdc.cnTypeOfVisit, @cnTypeOfVisit);


  // getCnPurposeOfVisitPickListValue: =>
  //   @_getChinaPickListLabelByValue(CallReport.sfdc.cnPurposeOfVisit, @cnPurposeOfVisit);


  // _getChinaPickListLabelByValue: (pickListName, value) =>
  //   if value
  //     CallReportChinaPickListManager = require('db/picklist-managers/call-report-china-picklist-manager');
  //     callReportChinaPickListManager = new CallReportChinaPickListManager();
  //     return callReportChinaPickListManager.getLabelByValue(pickListName, value);
  //   else
  //     return $.when('');

}
