import {Entity} from './base/Entity';
import {ProductSegmentationScheme} from './scheme/ProductSegmentationScheme';


export class ProductSegmentationHistory extends Entity {
  public id: string;
  public createdById: string;
  public createdByName: string;
  public createdDate: string;
  public changedField: string;
  public isDeleted: string;
  public newValue: string;
  public oldValue: string;
  public parentId: string;

  constructor(responseRecord) {
    super(ProductSegmentationScheme.fields, responseRecord);
  }
}
