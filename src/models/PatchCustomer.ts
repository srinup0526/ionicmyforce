import {Entity} from './base/Entity';
import {PatchCustomerScheme} from './scheme/PatchCustomerScheme';


export class PatchCustomer extends Entity {
    public id: string;
    public patchName: string;
    public contactId: string;
    public contactName: string;
    public remoteContactName: string;
    public class: string;
    public frequency: string;
    public recordType: string;
    public patchId: string;
    public city: string;
    public status: string;
    public patchStation: string;
    public state: string;
    public speciality: string;


  constructor(responseRecord) {
    super(PatchCustomerScheme.fields, responseRecord);
  }


  getCustomerName()
  {
    return `${this.remoteContactName}`;
  }

 

}
