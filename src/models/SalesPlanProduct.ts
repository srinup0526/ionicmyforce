import {Entity} from './base/Entity';
import { SalesPlanningProductScheme } from './scheme/SalesPlanningProductScheme';


export class SalesPlanProduct extends Entity {
  id: string;
  plannedQuantity: string;
  price: string;
  productName: string;
  product: string;
  salesPlanningList: string;
  total: string;
  totalPrice: string;
  _soupEntryId:number;


  constructor(responseRecord) {
    super(SalesPlanningProductScheme.fields, responseRecord);
  }
}
