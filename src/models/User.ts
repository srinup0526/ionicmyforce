import {Entity} from './base/Entity';
import {UserScheme} from './scheme/UserScheme';


export class User extends Entity {
  id: string;
  email: string;
  name: string;
  managerId: string;
  managerFirstName: string;
  managerLastName: string;
  firstName: string;
  lastName: string;
  currency: string;
  businessUnit: string;
  userRoleId: string;
  jointVisitType: string;
  callReportValidationExcempted: string;
  pinCode: string;
  targetPercentWithAvg: string;
  isActive: string;
  typeofquery: string;
  DayOff1: string;
  DayOff2: string;
  profileId: string;
  profileName: string;
  atcClass: string;
  isLocked: string;
  pinAttemptsCnt: string;
  userCountry: string;

  constructor(responseRecord) {
    super(UserScheme.fields, responseRecord);
  }

  fullName() {
    return `${this.lastName || ''} ${this.firstName || ''}`;
  }

  public getName() {
    return [
      this.lastName,
      this.firstName
    ]
    .filter(field => !!field)
    .join(' ');
  }

  public getIdForTableColumn() {
    const cssClass = ~UserScheme.activeRows.indexOf(this.id) ? 'active' : '';
    console.log("cssClass",cssClass);
    
    return `<span class="check-box ${cssClass}"><span class="arrow"></span><span class="contact-id">${this.id}</span></span>  `;
  }

  getFirstAndLastNames() {
    return `${this.firstName || ''} ${this.lastName || ''}`;
  }

  getDayOffByIndex(index) {
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    return days[index];
  }

  isProfileMedicalRep() {
    return this.profileName && !!~this.profileName.indexOf(UserScheme.PROFILES_GROUPS.MEDICAL_REP);
  }

  isProfileFLM() {
    return this.profileName && !!~this.profileName.indexOf(UserScheme.PROFILES_GROUPS.FLM);
  }

  isProfileNSM() {
    return this.profileName && !!~this.profileName.indexOf(UserScheme.PROFILES_GROUPS.NSM);
  }

  isChinaUser() {
    return this.currency === UserScheme.CHINA_CURRENCY_ISO_CODE;
  }
}
