import {Entity} from './base/Entity';
import {BuTeamPersonProfileScheme} from './scheme/BuTeamPersonProfileScheme';


export class BuTeamPersonProfile extends Entity {
  id: string;
  organizationSfid: string;
  businessUnit: string;
  priority: string;
  specialty: string;


  constructor(responseRecord) {
    super(BuTeamPersonProfileScheme.fields, responseRecord);
  }
}
