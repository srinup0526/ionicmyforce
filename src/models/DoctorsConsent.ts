import {Entity} from './base/Entity';
import {DoctorsConsentScheme} from './scheme/DoctorsConsentScheme';


export class DoctorsConsent extends Entity {
  id: string;
  firstname: string;
  lastname: string;
  email: string;
  cellphonenumber: string;
  doctorid: string;
  organization: string;
  organizationid: string;
  signature: string;
  speciality: string;
  existingCustomerName:string;
  brick: string;
  nonOrganization: string;
  country: string;

  constructor(responseRecord) {
    super(DoctorsConsentScheme.fields, responseRecord);
  }
  public contactFullNameWithType() {
    return `${this.firstname || ''} ${this.lastname || '' } ${this.existingCustomerName ||  ''} `;
  }
}
