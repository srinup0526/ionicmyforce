import {Entity} from './base/Entity';
import { SalesPlanningScheme } from './scheme/SalesPlanningScheme';


export class SalesPlan extends Entity {
  id: string;
  month: string;
  status: string;
  view: string;
  year: string;

  constructor(responseRecord) {
    super(SalesPlanningScheme.fields, responseRecord);
  }
}
