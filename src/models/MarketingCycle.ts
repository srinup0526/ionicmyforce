import {Entity} from './base/Entity';
import {MarketingCycleScheme} from './scheme/MarketingCycleScheme';


export class MarketingCycle extends Entity {
  id: string;
  planingCycleName: string;
  cycleName: string;
  startDate: string;
  endDate: string;
  currencyIsoCode: string;

  constructor(responseRecord) {
    super(MarketingCycleScheme.fields, responseRecord);
  }
}
