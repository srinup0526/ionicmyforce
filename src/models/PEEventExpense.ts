import {Entity} from './base/Entity';
import {PEEventExpenseScheme} from './../models/scheme/PEEventExpenseScheme';

export class PEEventExpense extends Entity {
  id: string;
  name: string;
  recordTypeId: string;
  pharmaEventSfId: string;
  costOfEachMeal: string;
  costOfEachMealUSD: string;
  noOfHcps: string;
  noOfMylanStaff: string;
  totalNoOfAttendees: string;
  totalCost: string;
  totalCostUSD: string;
  actualOtherExpensesText1: string;
  actualOtherExpensesText2: string;
  actualOtherExpensesText3: string;
  actualOtherExpensesText4: string;
  actualOtherExpensesText5: string;
  actualOtherExpensesLC1: string;
  actualOtherExpensesLC2: string;
  actualOtherExpensesLC3: string;
  actualOtherExpensesLC4: string;
  actualOtherExpensesLC5: string;
  actualOtherExpensesUSD1: string;
  actualOtherExpensesUSD2: string;
  actualOtherExpensesUSD3: string;
  actualOtherExpensesUSD4: string;
  actualOtherExpensesUSD5: string;
  actualTotalCostofEventLC: string;
  actualTotalCostofEventUSD: string;
  totalNumberOfHCPs: string;

  constructor(responseRecord) {
    super(PEEventExpenseScheme.fields, responseRecord);
  }
}
