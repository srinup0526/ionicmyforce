import { Entity } from './base/Entity';
import { PatchCustomerReferenceScheme } from './scheme/PatchCustomerReferenceScheme';


export class PatchCustomerReference extends Entity {
  public id: string;
  public name: string;
  public customerSfId: string;
  public organizationSfId: string;
  public organizationName: string;
  public recordType: string;
  
  public contact;
  public organization;
  
  constructor(responseRecord) {
    super(PatchCustomerReferenceScheme.fields, responseRecord);
    
    this.contact = null;
    this.organization = null;
  }
}
