import {Entity} from './base/Entity';
import {LocationScheme} from './scheme/LocationScheme';


export class Location extends Entity {
  id: string;
  callReport: string;
  datetime: string;
  gpsStatus: string;
  latitude: string;
  longitude: string;
  type: string;
  result: string;

  constructor(responseRecord) {
    super(LocationScheme.fields, responseRecord);
  }
}
