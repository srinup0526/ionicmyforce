import {Entity} from './base/Entity';
import {IqviaAccountSkuTaskScheme} from './scheme/IqviaAccountSkuTaskScheme';

export class IqviaAccountSkuTask extends Entity {
  id: string;
  startDate: string;
  endDate: string;
  task: string;
  taskName: string;
  organization: string;
  sku: string;
  skuName: string;
  salesRep: string;

  constructor(responseRecord) {
    super(IqviaAccountSkuTaskScheme.fields, responseRecord);
  }
}
