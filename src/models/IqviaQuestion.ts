import {Entity} from './base/Entity';
import {IqviaQuestionScheme} from './scheme/IqviaQuestionScheme';

export class IqviaQuestion extends Entity {
  id: string;
  question: string;
  description: string;
  isActive: string;
  order: string;
  picklistValue: string;
  type: string;
  picklistOptions: string[] = [];

  constructor(responseRecord) {
    super(IqviaQuestionScheme.fields, responseRecord);

    if(this.type === 'picklist' && this.picklistValue){
      this.picklistOptions = this.picklistValue.split('\n');
    }
  }
}
