import {Entity} from './base/Entity';
import {SurveyQuestionnaireDetailScheme} from './scheme/SurveyQuestionnaireDetailScheme';
import { ContactsCollection } from '../collections/ContactsCollection';



export class SurveyQuestionnaireDetail extends Entity {
  id: string;
  accountid: string;
  customerId: string;
  surveyquestionnaireid: string;
  realvalue: string;
  question: string;
  stringrealvalue: string;
  numberrealvalue: number;
  surveyid:string;
  noncustomervalue:string;
  contactCollection:ContactsCollection;
  remoteCustomerName:string;
  customerName:string;


  constructor(responseRecord) {
    super(SurveyQuestionnaireDetailScheme.fields, responseRecord);
    this.contactCollection = new ContactsCollection();
  }

  public getAnswer() {
    return `${this.realvalue || ''} ${this.stringrealvalue || ''} ${this.numberrealvalue || ''}`;
  }

  public getCustomer() {
    return this.contactCollection.fetchEntityById(this.customerId)
      .then(contact=> { return Promise.resolve(contact.name);});
  }
}
