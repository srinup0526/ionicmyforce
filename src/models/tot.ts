import { Entity } from './base/Entity';
import { TotScheme } from './scheme/TotScheme';
import { Utils } from '../utils/Utils'



export class Tot extends Entity {
    public id: string;
    public userSfId: string;
    public createdOffline: string;
    public userLastName: string;
    public userFirstName: string;
    public allDay: string;
    public startDate: string;
    public endDate: string;
    public firstQuarterEvent: string;
    public secondQuarterEvent: string;
    public thirdQuarterEvent: string;
    public fourthQuarterEvent: string;
    public type: string;
    public description: string;
    public isSubmittedForApproval: string;
    public clmToolId:string;


  constructor(responseRecord) {
    super(TotScheme.fields, responseRecord);
  }

  public userFullName() {
    return `${this.userFirstName || ''} <br/> ${this.userLastName || ''}`;
  }

  public allDayTrueOrNot()
  {
    if(this.allDay)
    {
      return '<span class="all-day"></span>';
    }
  }

  public getStartDateFormat()
  {
    return Utils.originalDate(this.startDate);
  }

  public getEndDateFormat()
  {
    return Utils.originalDate(this.endDate);
  }

}
