import {Entity} from './base/Entity';
import {MTPScheme} from './scheme/MTPScheme';


export class MTP extends Entity {
  id: string;
  name: string;
  __locally_created__: string;
  startDate: string;
  endDate: string;
  createdDate: string;
  monthYearKey: string;
  month: string;
  year: string;
  status: string;
  medRep: string;
  currency: string;

  constructor(responseRecord) {
    super(MTPScheme.fields, responseRecord);
  }

  isAvailableToSendForApproval()
  {
  	return this.status in [MTPScheme.APPROVAL_STATUS_DRAFT, MTPScheme.APPROVAL_STATUS_REJECTED];
  }
  isAvailableToDelete()
  {
  	return this.status in [MTPScheme.APPROVAL_STATUS_DRAFT, MTPScheme.APPROVAL_STATUS_REJECTED];	
  }

  isAvailableToDeleteEventFromMTP()
  {
  	return this.status in [MTPScheme.APPROVAL_STATUS_DRAFT, MTPScheme.APPROVAL_STATUS_REJECTED, MTPScheme.APPROVAL_STATUS_APPROVED];
  }
  changeApprovalStatus()
  {
  	if(this.isAvailableToSendForApproval())
  	{
  		this.status = MTPScheme.APPROVAL_STATUS_SUBMITTED_OFFLINE;

  	}
  	else
  	{
		this.status = MTPScheme.APPROVAL_STATUS_DRAFT;  		
  	}
  }

  isAlreadyInApproval()
  {
  	return this.status in [MTPScheme.APPROVAL_STATUS_PENDING, MTPScheme.APPROVAL_STATUS_VALIDATED, MTPScheme.APPROVAL_STATUS_APPROVED];
  }

  isApproved()
  {
  	return this.status==MTPScheme.APPROVAL_STATUS_APPROVED;
  }
}
