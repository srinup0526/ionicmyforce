import {Entity} from './base/Entity';
import {AttachmentScheme} from './scheme/AttachmentScheme';


export class Attachment extends Entity {
  id: string;
  name: string;
  body: string;
  typeOfAttachment: string;
  description: string;
  parentId: string;


  constructor(responseRecord){
    super(AttachmentScheme.fields, responseRecord);
  }
}
