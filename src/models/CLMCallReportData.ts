import {Entity} from './base/Entity';
import {CLMCallReportDataScheme} from './scheme/CLMCallReportDataScheme';


export class CLMCallReportData extends Entity {
  id: string;
  name: string;
  kpiSrcJson: string;
  timeOnPresentation: string;
  timeOnSlides: string;
  callReportId: string;
  productId: string;
  productName: string;
  presentationId: string;
  presentationName: string;
  clmToolId: string;

  constructor(responseRecord) {
    super(CLMCallReportDataScheme.fields, responseRecord);
  }

  static _getParsedKPIJSONData() {
  	let slidesData;
    try
    {
      slidesData = JSON.parse('${this.kpiSrcJson}');
    }

    catch(error)
    {
      slidesData = {};
      console.warn(error);
    }

    finally
    {
      return slidesData;
    }
  }

}
