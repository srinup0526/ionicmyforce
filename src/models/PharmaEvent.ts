import {Entity} from './base/Entity';
import {PharmaEventScheme} from './scheme/PharmaEventScheme';
import { Utils } from './../utils/Utils';

export class PharmaEvent extends Entity {
  id: string;
  ownerSfid: string;
  createdOffline: string;
  remoteOwnerFirstName: string;
  remoteOwnerLastName: string;
  ownerFirstName: string;
  ownerLastName: string;
  eventName: string;
  eventType: string;
  location: string;
  startDate: string;
  endDate: string;
  closedDate: string;
  closedDateDiff: string;
  stage: string;
  discussionType: string;
  plannedBudget: string;
  plannedBudgetNew: string;
  plannedParticipants: string;
  status: string;
  businessUnit: string;
  objectives: string;
  agenda: string;
  speakers: string;
  evaluation: string;
  productPrio1SfId: string;
  productPrio2SfId: string;
  productPrio3SfId: string;
  productPrio4SfId: string;
  clmToolId: string;
  tgEForm: string;
  invoiceReceiptOne: string;
  invoiceReceiptTwo: string;
  invoiceReceiptThree: string;
  invoiceReceiptFour: string;
  attendanceSheet: string;
  attendanceSheetOne: string;
  summaryOfEvent: string;
  noOfHcps: string;
  noOfMylanStaff: string;
  costOfEachMeal: string;
  costOfEachMealUSD: string;
  TGEData: string;
  nameOfThePresenter: string;
  mylanParticipant: string;
  mylanParticipantEmails: string;
  typeOfHospital: string;
  plannedBudgetUSD: string;
  otherExpensesCondition: string;
  plannedOtherExpensesText1: string;
  plannedOtherExpensesText2: string;
  plannedOtherExpensesText3: string;
  plannedOtherExpensesText4: string;
  plannedOtherExpensesText5: string;
  plannedOtherExpensesLC1: string;
  plannedOtherExpensesLC2: string;
  plannedOtherExpensesLC3: string;
  plannedOtherExpensesLC4: string;
  plannedOtherExpensesLC5: string;
  plannedOtherExpensesUSD1: string;
  plannedOtherExpensesUSD2: string;
  plannedOtherExpensesUSD3: string;
  plannedOtherExpensesUSD4: string;
  plannedOtherExpensesUSD5: string;
  plannedTotalCostofEventLC: string;
  plannedTotalCostofEventUSD: string;
  
  organizer: string;
  region: string;
  workPlace: string;
  regionGroup: string;
  product: string;
  venue: string;
  purposeAndContent: string;
  timeBegin: string;
  timeEnd: string;
  timeBeginReal: string;
  timeEndReal: string;
  internalAttendeeNumber: string;
  externalAttendeeNumber: string;
  totalBudget: string;
  totalExpense: string;
  remark: string;
 
  constructor(responseRecord) {
    super(PharmaEventScheme.fields, responseRecord);
  }
  isAvailableToSendForApproval(){
    // this.status in (PharmaEvent.APPROVAL_STATUS_DRAFT, PharmaEvent.APPROVAL_STATUS_REJECTED)

    // _user: null
  }
  ownerFullName(){
    "${this.scheme.fields.ownerLastName ? ''} ${this.scheme.fields.ownerFirstName ? ''}"
  }

  public getDateTimePlannedInFormat() {
    return Utils.formatDateTimeWithBreak(this.startDate);
  }

  _relatedCriteria(model, field){
    // const relationField = model.sfdc[field]
    // const relationCriteria = {}
    // if (this.id.indexOf('local') is 0)
    //   relationCriteria[relationField] = this.attributes._soupEntryId
    // else
    //   relationCriteria[relationField] = this.id
    // this.relationCriteria
  }
  changeApprovalStatus(){
    // @status =
    // if @isAvailableToSendForApproval()
    //   PharmaEvent.APPROVAL_STATUS_SUBMITTED_OFFLINE
    // else
    //   PharmaEvent.APPROVAL_STATUS_DRAFT
  }

  _relatedPEAttendeesCriteria(){
    // @_relatedCriteria PharmaEvent.peAttendeesCollection.model, 'pharmaEventSfId'
  }
  _relatedPEAbbottAttendeesCriteria(){
     //this._relatedCriteria(PharmaEvent.peAbbottAttendeesCollection.model, 'pharmaEventSfId') 
}
_fetchAttendees(collection, criteria){
     collection.fetchAllWhere(criteria,false) ;
}
_fetchNotDeletedAttendees(collection, criteria){
collection.fetchAllWhere(criteria);
}

getOwner(){
    // if (this._user then $.when this._user)
    //     else 
    //     (this.PharmaEvent.usersCollection.fetchEntityById(this.ownerSfid).then(response=>{ (this._user= (this._user))});
}
fetchAllPEAttendees(){
    // this._fetchAttendees()
    // .then(response=>{PharmaEvent.peAttendeesCollection,  this._relatedPEAttendeesCriteria()});
}
fetchAllPEAbbottAttendees(){
    // this._fetchAttendees()
    // .then(response=>{PharmaEvent.peAbbottAttendeesCollection, this._relatedPEAbbottAttendeesCriteria()});
}
fetchNotDeletedPEAttendees(){
    // this._fetchNotDeletedAttendees()
    // .then(response=>{PharmaEvent.peAttendeesCollection, this._relatedPEAttendeesCriteria()}) ;
}
fetchNotDeletedPEAbbottAttendees(){
// this._fetchNotDeletedAttendees()
// .then(response=>{PharmaEvent.peAbbottAttendeesCollection, this._relatedPEAbbottAttendeesCriteria()}) 
 }
 isEditable(){
//    SforceDataContext.activeUser()
//     .then ((activeUser) => {this.ownerSfid == activeUser.id});
 }
}
