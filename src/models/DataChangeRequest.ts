import {Entity} from './base/Entity';
import {DataChangeRequestScheme} from './scheme/DataChangeRequestScheme';
import {Utils} from "../utils/Utils";


export class DataChangeRequest extends Entity {
  public id: string;
  public dcrOrganizationCNTerritoryCode: string;
  public dcrContactCJobTitle: string;
  public dcrContactCNAcademicTitle: string;
  public dcrContactCNAdministrative: string;
  public dcrContactCNDescriptionIntroduction: string;
  public dcrContactCNHcpStatus: string;
  public dcrContactCNHcpType: string;
  public dcrContactCNKOL: string;
  public dcrContactCNKolProduct1: string;
  public dcrContactCNKolProduct2: string;
  public dcrContactCNNamedDepartment: string;
  public dcrContactCNPhysicianId: string;
  public dcrContactCNProfessionalTitle: string;
  public dcrContactCNSocialOrgAndTitle: string;
  public dcrContactCNSpeaker: string;
  public dcrContactCNSpeakerProduct1: string;
  public dcrContactCNSpeakerProduct2: string;
  public dcrContactCNSpecialtyDescription: string;
  public dcrContactCNStandardDepartment: string;
  public dcrContactCNStatusDescription: string;
  public dcrContactCNWorkingTime: string;
  public dcrContactDescription: string;
  public dcrContactEmail: string;
  public dcrContactFirstName: string;
  public dcrContactGender: string;
  public dcrContactHomePhone: string;
  public dcrContactId: string;
  public dcrContactInclinationToMylan: string;
  public dcrContactKOL: string;
  public dcrContactLastName: string;
  public dcrContactLegalMedicalLicence: string;
  public dcrContactMobilePhone: string;
  public dcrContactName: string;
  public dcrContactNumberOfPatientsPerDay: string;
  public dcrContactPersonType: string;
  public dcrContactSpecialty: string;
  public dcrContactYearOfGraduation: string;
  public dcrDateOfRequest: string;
  public dcrOrganizationBillingCity: string;
  public dcrOrganizationBillingCountry: string;
  public dcrOrganizationBillingPostalCode: string;
  public dcrOrganizationBillingState: string;
  public dcrOrganizationBillingStreet: string;
  public dcrOrganizationBrick: string;
  public dcrOrganizationCJuridicGroup: string;
  public dcrOrganizationCNAlias: string;
  public dcrOrganizationCNHospitalGrade: string;
  public dcrOrganizationCNHospitalLevel: string;
  public dcrOrganizationCNId: string;
  public dcrOrganizationCNProperty: string;
  public dcrOrganizationCNStatusDescription: string;
  public dcrOrganizationCNTargetHospital: string;
  public dcrOrganizationCNType: string;
  public dcrOrganizationCSpecialty1: string;
  public dcrOrganizationCSpecialty2: string;
  public dcrOrganizationExternal_Id: string;
  public dcrOrganizationId: string;
  public dcrOrganizationName: string;
  public dcrOrganizationPhone: string;
  public dcrOrganizationPriority: string;
  public dcrOrganizationRecordType: string;
  public dcrOrganizationStatus: string;
  public dcrOrganizationSubtype: string;
  public dcrOrganizationNumberOfPatientsPerDay: string;
  public dcrReferenceCStatus: string;
  public dcrReferenceId: string;
  public dcrReferencePrimary: string;
  public dcrStatus: string;
  public dcrType: string;
  public dcrProductSegmentationSegmentation1: string;
  public dcrProductSegmentationSegmentation2: string;
  public dcrProductSegmentationPhProduct: string;
  public dcrProductSegmentationAccount: string;
  public dcrProductSegmentationId: string;
  public lastModifiedDate: string;
  
  
  constructor(responseRecord) {
    super(DataChangeRequestScheme.fields, responseRecord);
  }
  
  contactName(): string {
    return `${this.dcrContactLastName || ''} ${this.dcrContactFirstName || ''}`;
  }
  
  dateOfRequestStr(): string {
    if(this.dcrDateOfRequest) {
      return Utils.formatDateTime(this.dcrDateOfRequest);
    }
    
    return '';
  }
  
  isDraftStatus(): boolean {
    return this.dcrStatus == DataChangeRequestScheme.STATUSES.DRAFT;
  }
  
  isContactWithReferenceChangeRequest(): boolean {
    return this.dcrType == DataChangeRequestScheme.TYPES.CONTACT;
  }
  
  isOrganizationChangeRequest(): boolean {
    return this.dcrType == DataChangeRequestScheme.TYPES.ORGANIZATION;
  }
  
  isReferenceChangeRequest(): boolean {
    return this.dcrType == DataChangeRequestScheme.TYPES.REFERENCE;
  }
  
  isProductSegmentationChangeRequest(): boolean {
    return this.dcrType == DataChangeRequestScheme.TYPES.SEGMENTATION;
  }
}
