import {Entity} from './base/Entity';
import {SurveyQuestionnaireScheme} from './scheme/SurveyQuestionnaireScheme';


export class SurveyQuestionnaire extends Entity {
  id: string;
  question: string;
  picklistvalue: string;
  type: string;
  surveyid: string;
  sortorder: string;
  noncustomervalue: string;
  isthisquestionrequire: string;


  constructor(responseRecord) {
    super(SurveyQuestionnaireScheme.fields, responseRecord);
  }
}
