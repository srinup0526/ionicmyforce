import {Entity} from './base/Entity';
import {PatchScheme} from './scheme/PatchScheme';


export class Patch extends Entity {
  id: string;
  name: string;

  constructor(responseRecord) {
    super(PatchScheme.fields, responseRecord);
  }
}
