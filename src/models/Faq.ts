import {Entity} from './base/Entity';
import {FaqScheme} from './scheme/FaqScheme';

export class FAQ extends Entity {
  id: string;
  answer: string;
  question: string;
  order: string;
  currencyIsoCode: string;
  attachments: Array<any>;

  constructor(responseRecord) {
    super(FaqScheme.fields, responseRecord);

    this.attachments = [];
  }

  public setAttachments(attachments): void {
    this.attachments = attachments;
  }

  public getAttachments(): Array<any> {
    return this.attachments;
  }
  public hasAttachments(): boolean {
    return this.attachments.length > 0;
  }
}
