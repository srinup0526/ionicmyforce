import {Entity} from './base/Entity';
import { SalesPlanningListScheme } from './scheme/SalesPlanningListScheme';


export class SalesPlanList extends Entity {
  id: string;
  class: string;
  customer1: string;
  doctorfirstname: string;
  doctorlasttname: string;
  DoctorName: string;
  customer: string;
  frequency: string;
  infieldAct: string;
  salesplanning: string;
  speciality: string;
  totalOrderValue: string;
  totalProductPlanned: string;
  type: string;
  view: string;


  constructor(responseRecord) {
    super(SalesPlanningListScheme.fields, responseRecord);
  }
}
