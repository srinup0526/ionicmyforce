import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddluncheonmeetingsPage } from './addluncheonmeetings';

@NgModule({
  declarations: [
    AddluncheonmeetingsPage,
  ],
  imports: [
    IonicPageModule.forChild(AddluncheonmeetingsPage),
  ],
})
export class AddluncheonmeetingsPageModule {}
