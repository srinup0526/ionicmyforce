import {Component, ViewChild } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {OrderCardPage} from "../order-card/order-card";
import {LazyTableComponent} from './../../../components/table/lazy-table/lazy-table';
import {LazyTableOption} from './../../../components/table/lazy-table/LazyTableOption';
import {OrderScheme} from "../../../models/scheme/OrderScheme";
import {Query} from "../../../services/common/Query";
import {OrdersCollection} from "../../../collections/OrdersCollection";
import {OrdersTypeList} from "./OrdersTypeList";
import {LocalizationManager} from "../../../services/common/localizations/LocalizationManager";
import {ConfigurationManager} from '../../../services/db/ConfigurationManager';
import {TableHeaderItemOption} from "../../../components/table/table-header/TableHeaderItemOption";
import {PicklistPopup} from "../../../components/popups/PicklistPopup";
import {OrderTypeDatasource} from "../../../services/db/picklist-managers/datasource/OrderTypeDatasource";
import {Order} from "../../../models/Order";
import { Settings, SettingsImpl } from './../../../services/db/Settings';
import {OrderCardCreatePage} from "../order-card-create/order-card-create";
import {OrderCardCreateSamplePage} from "../order-card-create-sample/order-card-create-sample";


@IonicPage()
@Component({
  selector: 'page-order-list',
  templateUrl: 'order-list.html',
})
export class OrderListPage {
  public count:number = 0;
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: OrdersCollection;
  public settings: SettingsImpl;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private ordersCollection: OrdersCollection,
              private alertCtrl: AlertController,
              private localizationManager: LocalizationManager,
              private orderTypeDatasource: OrderTypeDatasource) {
  
    this.collection = this.ordersCollection;
  
    this.searchString = '';
    this.settings = Settings.getInstance();
  }
  
  public onChangeQueryHandler(query: Query): void {
    this.setOrderCount();
  }
  
  public onTapRowHandler(event, record) {
    this.gotoOrderDetail({ "row": record});
    event.stopPropagation();
  }
  
  public ionViewDidEnter() {}
  
  public ngAfterContentInit() {}
  
  public ionViewWillEnter() {
    if(this.lazyTable.isTableInited) {
      return this.lazyTable.reloadTable();
    }
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
    this.setMainTableOptions()
      .then(() => {
        this.lazyTable.setTableOptions(this.tableOptions);
        this.lazyTable.reloadTable();
      });
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onTapCreateOrderHandler() {
    this.showCreateOrderPopup();
  }
  
  public isAddBtnActive(): boolean {
    const orderManagementSettings = this.settings.getOrderSampleManagementSettings();
    
    if(!orderManagementSettings){
      return true;
    }
    
    return !!orderManagementSettings.SalesOrder || !!orderManagementSettings.SampleOrder;
  }
  
  private gotoCreateOrder(option) {
    this.navCtrl.push(OrderCardCreatePage, option);
  }
  
  private gotoCreateSampleOrder(option) {
    this.navCtrl.push(OrderCardCreateSamplePage, option);
  }
  
  private gotoOrderDetail(option) {
    this.navCtrl.push(OrderCardPage, option);
  }
  
  private setOrderCount(): number {
    const countQuery = this.collection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);
    
    return this.collection.runCountQuery(query.query)
      .then((count) => {
        return this.count = count || 0;
      })
  }
  
  private setMainTableOptions(): Promise<any>{
    return this.getPicklistForOrderRecordType()
      .then((orderRecordType) => {
        return this.tableOptions = {
          headerItems: [
            {
              title: 'common.names.DateTimeCreated',
              fields: [OrderScheme.fields.dateTimeCreated],
              isSortable: true,
              isAsc: true,
              isActive: false,
              modelFunction: 'getOrderDateTimeInFormat'
            },
            {
              title: 'common.names.CustomerName',
              fields: [OrderScheme.fields.accountName],
              cssClass: ['call-report'],
              modelFunction: 'getAccountColumnData'
            },
            {
              title: 'common.names.Status',
              fields: [OrderScheme.fields.status],
              isSortable: true,
              isAsc: true,
              isActive: false
            },
            {
              title: 'common.names.OrderRecordType',
              fields: [OrderScheme.fields.recordTypeId],
              isSortable: true,
              isAsc: true,
              isActive: false,
              picklistValues: orderRecordType
            },
            {
              title: 'common.names.OrderTotalQuantity',
              fields: [OrderScheme.fields.orderTotalQuantity],
              isSortable: true,
              isAsc: true,
              isActive: false,
              isNumber: true
            },
            {
              title: 'common.names.OrderSalesAmount',
              fields: [OrderScheme.fields.orderSalesAmount],
              isSortable: true,
              isAsc: true,
              isActive: false,
              isNumber: true
            }
          ],
          rowHandler: this.onTapRowHandler.bind(this),
          batchSize: 100
        };
      });
    
  }
  
  private getPicklistForOrderRecordType(): Promise<{[key: string]: string}> {
    return  OrdersTypeList.localizedResources(this.localizationManager, true)
      .then((types) => {
        const orderSampleRecordTypeId = this.settings.getOrderSamplesRecordTypeId();
        
        const listForColumn = types.reduce((all, type) => {
          all[type.id] = type.description;
        
          return all;
        }, {});
        
        // by default
        listForColumn[''] = listForColumn[orderSampleRecordTypeId];
      
        return listForColumn;
      });
  }
  
  private showCreateOrderPopup(): Promise<any> {
    const orderSampleRecordTypeId = this.settings.getOrderSamplesRecordTypeId();
    
    
    const picklistPopup = new PicklistPopup(this.alertCtrl, this.orderTypeDatasource);
    
    return picklistPopup.showPopup()
      .then((orderType) => {
        if (orderType) {
          
          let order = new Order({
            recordType: orderType.description,
            remoteRecordType: orderType.description,
            recordTypeId: orderType.id ? orderType.id  : orderSampleRecordTypeId,
            status: OrderScheme.APPROVAL_STATUS_DRAFT
          });
  
          if(orderSampleRecordTypeId == orderType.id) {
            this.gotoCreateSampleOrder({ "row": order });
          } else {
            this.gotoCreateOrder({ "row": order });
          }
          console.log(orderType);
        }
        
        return orderType;
      });
  }
  
}
