
import {LocalizationManager} from "../../../services/common/localizations/LocalizationManager";
import { Settings } from './../../../services/db/Settings';


export class OrdersTypeList {
  
  static salesOrder() {
    const orderSalesRecordTypeId = Settings.getInstance().getOrderSalesRecordTypeId();
    
    return {
      id: orderSalesRecordTypeId,
      developerName:'Sales_Order',
      objectName:'SalesOrder',
      description: 'OrdersManagement.ordersTypePopup.salesOrder'
    };
  }
  
  static sampleOrder() {
    const orderSampleRecordTypeId = Settings.getInstance().getOrderSamplesRecordTypeId();
    
    return {
      id: orderSampleRecordTypeId,
      developerName:'Sample_Order',
      objectName:'SampleOrder',
      description: 'OrdersManagement.ordersTypePopup.sampleOrder'
    };
  }
  
  static isSalesExist(): boolean {
    const orderManagementSettings = Settings.getInstance().getOrderSampleManagementSettings();
  
    if(!orderManagementSettings) {
      return true;
    }
    
    return !!orderManagementSettings.SalesOrder;
  }
  
  static isSampleExist(): boolean {
    const orderManagementSettings = Settings.getInstance().getOrderSampleManagementSettings();
    
    if(!orderManagementSettings) {
      return true;
    }
    
    return !!orderManagementSettings.SampleOrder;
  }
  
  static resources(force: boolean = false) {
    let resources = [];

    if(OrdersTypeList.isSalesExist() || force) {
      resources.push(this.salesOrder())
    }
    if(OrdersTypeList.isSampleExist() || force) {
      resources.push(this.sampleOrder())
    }
    
    return resources;
  }
  
  static localizedResources(localizationManager: LocalizationManager, force?: boolean): Promise<Array<{
    id: number,
    developerName: string,
    objectName: string,
    description: string
  }>> {
    const resources = OrdersTypeList.resources(force);
    const labels = resources.map((resource) => resource.description);
    
    return localizationManager.getSeveralLocales(labels)
      .then((localizedLabels) => {
        return resources.map((resource) => {
          resource.description = localizedLabels[resource.description];
          
          return resource;
        });
      })
  }
}
