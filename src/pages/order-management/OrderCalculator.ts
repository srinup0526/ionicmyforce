export class OrderCalculator{
  
  constructor(){}
  
  static calculateAmount(quantity, listPrice): number {
    const validatedQuantity = parseInt(quantity);
    
    return OrderCalculator.round(validatedQuantity * listPrice);
  }
  
  static calulateOrder(orderLines, order) {
    let orderTotalQuantity = 0;
    let orderSalesAmount = 0;
    
    orderLines.forEach((orderline) => {
      orderSalesAmount += orderline.salesAmount;
      orderTotalQuantity += parseInt(orderline.quantity);
    });

    order.orderSalesAmount = OrderCalculator.round(orderSalesAmount);
    order.orderTotalQuantity = OrderCalculator.round(orderTotalQuantity);

    return order;
  }
  
  static round(num): number {
    return Math.round(num * 100) / 100;
  }
}
