import {ViewChild, ChangeDetectorRef} from '@angular/core';
import {
  NavController, NavParams, ToastController,
  AlertController, Navbar, Platform
} from 'ionic-angular';
import {Order} from "../../models/Order";
import {OrderScheme} from "../../models/scheme/OrderScheme";
import {Utils} from "../../utils/Utils";
import {OrderItemListComponent} from "../../components/order-management/order-item-list/order-item-list";
import {OrderSampleItemListComponent} from "../../components/order-management/order-sample-item-list/order-sample-item-list";
import {OrderCalculator} from "./OrderCalculator";
import {ConfigurationManager} from '../../services/db/ConfigurationManager';
import {Loader} from './../../services/common/Loader';
import {OrderLine} from "../../models/OrderLine";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {OrderCardPage} from "./order-card/order-card";
import {Settings, SettingsImpl} from "../../services/db/Settings";
import {OrderAccountsPage} from "./order-accounts/order-accounts";
import {ConfirmPopup} from "../../components/popups/ConfirmPopup";
import {OrdersTypeList} from "./order-list/OrdersTypeList";
import moment from 'moment';


export abstract class OrderDetail {
  public order: Order;
  public Utils: Utils;

  public isSubmitButtonEnable: boolean;
  public isSaveButtonEnable: boolean;
  public isDeleteButtonEnable: boolean;
  public isSpecialPrice: boolean;
  public isSignatureWrapperEnable: boolean;
  public dateTimeCreated: string;
  public settings: SettingsImpl;
  public unregisterBackButtonAction: any;

  @ViewChild(OrderSampleItemListComponent) orderSampleItemList: OrderSampleItemListComponent;
  @ViewChild(OrderItemListComponent) orderItemList: OrderItemListComponent;
  @ViewChild('navbar') navBar: Navbar;

  protected orderItemListComponent: OrderItemListComponent | OrderSampleItemListComponent;
  protected config;
  protected isChanged: boolean;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              public localizationManager: LocalizationManager,
              public loader: Loader,
              public alertCtrl: AlertController,
              public platform: Platform,
              private changeDetectorRef: ChangeDetectorRef) {

    this.Utils = Utils;
    this.order = new Order({});
    this.settings = Settings.getInstance();

    this.hideButtons();
    this.resetChanges();
  }


  public ngOnInit() {
  }

  public ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  public ionViewDidLoad() {
    this.initParams();
    this.initializeBackButtonCustomHandler();
  }

  public initializeBackButtonCustomHandler(): void {
    this.navBar.backButtonClick = this.backButtonClick.bind(this);

    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      this.backButtonClick.bind(this),
      101
    );
  }

  public initParams() {
    this.order = this.navParams.data['row'];
    this.isSpecialPrice = this.isSpecialPriceActive();
    this.order.signature = this.order.signature || '';

    if (!this.order.remoteRecordType) {
      this.setRecordTypeById(this.order.recordTypeId);
    }

    return this.loader.run(this.init.bind(this));
  }

  public maxDate() {
    const afterDays = 90;

    return moment().add(afterDays, 'days').format("YYYY-MM-DD");
  }

  public minDate() {
    const beforeDays = 10;

    return moment().subtract(beforeDays, 'days').format("YYYY-MM-DD");
  }

  protected init(): Promise<any> {
    return this.setConfig();
  }

  protected checkAndToggleAllBtn() {
    this.checkButtonsVisibility();
    this.toggleSignatureEditing();
    this.toggleListComponentEditing();
    this.changeDetectorRef.markForCheck();
  }

  protected checkButtonsVisibility(): void {
    if (this.isOrderEditable()) {
      return this.showButtons();
    }

    return this.hideButtons();
  }

  protected toggleListComponentEditing(): void {
    if (this.isOrderEditable()) {
      return this.orderItemListComponent.enableEditing();
    }

    return this.orderItemListComponent.disableEditing();
  }

  private hideButtons(): void {
    this.isSubmitButtonEnable = false;
    this.isSaveButtonEnable = false;
    this.isDeleteButtonEnable = false;
  }

  private showButtons(): void {
    this.isSubmitButtonEnable = true;
    this.isSaveButtonEnable = true;
    this.isDeleteButtonEnable = true;
  }

  private toggleSignatureEditing() {
    const orderLine = this.orderItemListComponent.getItems();

    this.isSignatureWrapperEnable = !!orderLine.length;
  }

  private isOrderEditable() {
    return ~OrderScheme.EDITABLE_STATUSES.indexOf(this.order.status);
  }

  public isSalesOrder(): boolean {
    return this.settings.getOrderSalesRecordTypeId() == this.order.recordTypeId;
  }

  public isSampleOrder(): boolean {
    return this.settings.getOrderSamplesRecordTypeId() == this.order.recordTypeId;
  }

  protected setOrderItemsComponent() {
    if (this.isSalesOrder()) {
      this.orderItemListComponent = this.orderItemList;
    } else {
      this.orderItemListComponent = this.orderSampleItemList;
    }
  }

  private setConfig(): Promise<any> {
    return ConfigurationManager.getConfig()
      .then((config) => {
        this.config = config;
      });
  }

  protected recalculateOrder(orderLines: Array<OrderLine>) {
    const order = OrderCalculator.calulateOrder(orderLines, this.order);

    this.order = order;
  }

  protected validate(): Promise<boolean> {
    return this.getValidateMsg()
      .then((msg) => {
        if (msg) {
          return this.showValidateToast(msg)
            .then(() => {
              return !msg;
            });
        }
        return !msg;
      });
  }

  protected getValidateMsg(): Promise<string> {
    return this.localizationManager.getSeveralLocales([
      'card.Tot.ToastMessage.ToastHeader',
      'card.Order.ToastMessage.RequiredAccountName',
      'card.Order.ToastMessage.RequiredorderDateTime',
      'card.Order.ToastMessage.RequiredorderAccountNonCustomer',
      'card.Order.ToastMessage.RequiredorderEither',
      'card.Order.ToastMessage.RequiredSignature'
    ]).then((localization) => {

      const toastHeader = `${localization['card.Tot.ToastMessage.ToastHeader']} \n`;
      let toastBody = '';

      if (!this.order.dateTimeCreated) {
        toastBody += `${localization['card.Order.ToastMessage.RequiredorderDateTime']}\n`;
      }

      if (!this.order.accountSfId && !this.order.noncustomer) {
        toastBody += `${localization['card.Order.ToastMessage.RequiredorderAccountNonCustomer']}\n`;
      }

      if (this.order.noncustomer && this.order.accountSfId) {
        toastBody = `${localization['card.Order.ToastMessage.RequiredorderEither']}\n`;
      }

      const isDataValid = toastBody.length == 0;

      if (isDataValid) {
        return '';
      }

      return toastHeader + toastBody;
    });
  }

  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton: true,
      cssClass: "toastClass",
      duration: 5000,
    });
    return toast.present();
  }

  protected forwardAndGotoEditPage(option): Promise<any> {
    return this.navCtrl.pop()
      .then(() => {
        return this.gotoOrderDetail(option);
      });
  }

  protected gotoOrderDetail(option): Promise<any> {
    return this.navCtrl.push(OrderCardPage, option);
  }

  protected isSpecialPriceActive(): boolean {
    const sampleManagementSettings = Settings.getInstance().getOrderSampleManagementSettings();

    if (!sampleManagementSettings) {
      return false;
    }

    return !!sampleManagementSettings.SpecialPrice;
  }

  public onTapAccountBtnHandler() {
    this.gotoAccountPage();
  }

  private gotoAccountPage() {
    this.navCtrl.push(OrderAccountsPage, {
      accountId: this.order.accountSfId,
      orderTypeId: this.order.recordTypeId,
      callback: this.setAccount.bind(this)
    })
  }

  private setAccount(account) {
    this.order.externalAccountId = account.externalId;
    this.order.accountSfId = account.id;
    this.order.remoteAccountName = account.name;
    this.order.remoteAccountRecordType = account.remoteRecordType;
    this.order.billindAddress = account.getFullAdressForOrder();
  }

  private setRecordTypeById(recordTypeById: string): Promise<any> {
    return OrdersTypeList.localizedResources(this.localizationManager, true)
      .then((types) => {
        let type = types.filter((type: any) => {
          return type.id == recordTypeById;
        })[0];

        if (type) {
          this.order.remoteRecordType = type.description;
        }
      })
  }

  protected backConfirmPopup() {
    const popup: ConfirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);

    return popup.showPopup({
      title: 'card.ConfirmationPopup.SaveChanges.Caption',
      yesLabel: 'common.buttons.YesBtn',
      noLabel: 'common.buttons.NoBtn'
    });
  }

  protected hasChanges() {
    return this.isChanged;
  }

  protected dataChanged() {
    this.isChanged = true;
  }

  protected resetChanges() {
    this.isChanged = false;
  }

  protected backButtonClick(event) {
    if (this.hasChanges() || (this.orderItemListComponent && this.orderItemListComponent.hasChanges())) {
      return this.backConfirmPopup()
        .then((result) => {
          if (result.doAction) {
            return this.loader.run(this.validateAndSaveOrder.bind(this))
          }

          return this.goBack();
        })
    }

    return this.goBack();
  }

  protected goBack() {
    return this.navCtrl.pop();
  }

  abstract validateAndSaveOrder();

  protected isSameFillFields(current, startBackup, schemeFields) {
    return Object.keys(schemeFields)
      .some((localKey) => {
        if (typeof current[localKey] == "number" || typeof startBackup[localKey] == "number") {
          return parseFloat(current[localKey]) != parseFloat(startBackup[localKey]);
        }

        if (current[localKey] || startBackup[localKey]) {
          return current[localKey] != startBackup[localKey];
        }

        return !!current[localKey] != !!startBackup[localKey];
      })
  }
}
