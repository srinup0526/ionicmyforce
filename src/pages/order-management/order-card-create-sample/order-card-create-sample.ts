import {ChangeDetectorRef, Component} from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Platform } from 'ionic-angular';
import { OrderDetail } from '../OrderDetail';
import {Loader} from './../../../services/common/Loader';
import {OrderScheme} from './../../../models/scheme/OrderScheme';
import {Utils} from "../../../utils/Utils";
import {Order} from "../../../models/Order";
import {OrdersCollection} from "../../../collections/OrdersCollection";
import {LocalizationManager} from "../../../services/common/localizations/LocalizationManager";


@IonicPage()
@Component({
  selector: 'page-order-card-create-sample',
  templateUrl: 'order-card-create-sample.html',
})
export class OrderCardCreateSamplePage extends OrderDetail {
  public orderStartBackup: Order;
  
  constructor(navCtrl: NavController,
              navParams: NavParams,
              loader: Loader,
              localizationManager: LocalizationManager,
              toastCtrl: ToastController,
              alertCtrl: AlertController,
              platform: Platform,
              private ordersCollection: OrdersCollection,
              changeDetectorRef: ChangeDetectorRef) {
    
    super(navCtrl, navParams, toastCtrl, localizationManager, loader, alertCtrl, platform, changeDetectorRef);
  }
  
  public onTapSaveBtnHandler() {
    this.loader.run(this.validateAndSaveOrder.bind(this));
  }
  
  public validateAndSaveOrder() {
    return this.validate()
      .then((isValid) => {
        if(isValid) {
          return this.saveOrder()
            .then((order) => {
              return this.forwardAndGotoEditPage({row: order});
            })
        }
      })
  }
  
  public initParams() {
    return super.initParams()
      .then(() => {
        this.orderStartBackup = new Order(this.order);
      })
  }
  
  protected init(): Promise<any> {
    return super.init()
      .then(() => {
        this.initOrder();
      })
  }
  
  private initOrder() {
    const dateTimeCreated = new Date();
    
    this.order.status = OrderScheme.APPROVAL_STATUS_DRAFT;
    this.order.dateTimeCreated = dateTimeCreated.toISOString();
  }
  
  private saveOrder(){
    this.order.orderSalesAmount = 0;
    this.order.orderTotalQuantity = 0;
    this.order.dateTimeCreated =  Utils.originalDateTime(new Date(this.order.dateTimeCreated));
    
    return this.ordersCollection.createEntity(this.order)
      .then((record) => this.ordersCollection.parseModel(record));
  }
  
  protected hasChanges() {
    return this.isSameFillFields(this.order, this.orderStartBackup, OrderScheme.fields);
  }
}
