import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderCardCreateSamplePage } from './order-card-create-sample';

@NgModule({
  declarations: [
    OrderCardCreateSamplePage,
  ],
  imports: [
    IonicPageModule.forChild(OrderCardCreateSamplePage),
  ],
})
export class OrderCardCreateSamplePageModule {}
