import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderAccountsPage } from './order-accounts';

@NgModule({
  declarations: [
    OrderAccountsPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderAccountsPage),
  ],
})
export class OrderAccountsPageModule {}
