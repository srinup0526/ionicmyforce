import {ChangeDetectorRef, Component} from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Platform } from 'ionic-angular';
import { OrderDetail } from '../OrderDetail';
import {Loader} from './../../../services/common/Loader';
import {OrderScheme} from './../../../models/scheme/OrderScheme';
import {Utils} from "../../../utils/Utils";
import {LocalizationManager} from "../../../services/common/localizations/LocalizationManager";
import {OrdersCollection} from "../../../collections/OrdersCollection";
import {Order} from "../../../models/Order";



@IonicPage()
@Component({
  selector: 'page-order-card-create',
  templateUrl: 'order-card-create.html',
})
export class OrderCardCreatePage extends OrderDetail{
  public orderStartBackup: Order;
  
  constructor(navCtrl: NavController,
              navParams: NavParams,
              toastCtrl: ToastController,
              localizationManager: LocalizationManager,
              loader: Loader,
              alertCtrl: AlertController,
              platform: Platform,
              private ordersCollection: OrdersCollection,
              changeDetectorRef: ChangeDetectorRef) {
    
    super(navCtrl, navParams, toastCtrl, localizationManager, loader, alertCtrl, platform, changeDetectorRef);
  }
  
  public onTapSaveBtnHandler() {
    this.loader.run(this.validateAndSaveOrder.bind(this));
  }
  
  public initParams() {
    return super.initParams()
      .then(() => {
        this.orderStartBackup = new Order(this.order);
      })
  }
  
  protected init(): Promise<any> {
    return super.init()
      .then(() => {
        this.initOrder();
      })
  }
  
  public validateAndSaveOrder() {
    return this.validate()
      .then((isValid) => {
        if(isValid) {
          return this.saveOrder()
            .then((order) => {
              return this.forwardAndGotoEditPage({row: order});
            })
        }
      })
  }
  
  private initOrder() {
    const dateTimeCreated = new Date();
    
    this.order.status = OrderScheme.APPROVAL_STATUS_DRAFT;
    this.order.dateTimeCreated = dateTimeCreated.toISOString();
  }
  
  private saveOrder(){
    this.order.orderSalesAmount = 0;
    this.order.orderTotalQuantity = 0;
    this.order.dateTimeCreated =  Utils.originalDateTime(new Date(this.order.dateTimeCreated));
    
    return this.ordersCollection.createEntity(this.order)
      .then((record) => this.ordersCollection.parseModel(record));
  }
  
  protected hasChanges() {
    return this.isSameFillFields(this.order, this.orderStartBackup, OrderScheme.fields);
  }
}
