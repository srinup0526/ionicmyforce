import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderCardCreatePage } from './order-card-create';

@NgModule({
  declarations: [
    OrderCardCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(OrderCardCreatePage),
  ],
})
export class OrderCardCreatePageModule {}
