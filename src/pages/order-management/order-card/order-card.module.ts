import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrderCardPage } from './order-card';

@NgModule({
  declarations: [
    OrderCardPage
  ],
  imports: [
    IonicPageModule.forChild(OrderCardPage),
  ]
})
export class OrderCardPageModule {}
