import {Component, ViewChild, Output, ChangeDetectorRef} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Loading,
  ModalController,
  ModalOptions,
  ToastController,
  AlertController,
  Platform
} from 'ionic-angular';
import {Order} from "../../../models/Order";
import {OrderScheme} from "../../../models/scheme/OrderScheme";
import {Utils} from "../../../utils/Utils";
import {OrderItemListComponent} from "../../../components/order-management/order-item-list/order-item-list";
import {OrderSampleItemListComponent} from "../../../components/order-management/order-sample-item-list/order-sample-item-list";
import {CreateOrderLinePopupComponent} from "../../../components/order-management/create-order-line-popup/create-order-line-popup";
import {OrderCalculator} from "./../OrderCalculator";
import {ConfigurationManager} from '../../../services/db/ConfigurationManager';
import {Loader} from './../../../services/common/Loader';
import {OrderLine} from "../../../models/OrderLine";
import {OrderDetail} from "../OrderDetail";
import {LocalizationManager} from "../../../services/common/localizations/LocalizationManager";
import {OrdersLineCollection} from "../../../collections/OrdersLineCollection";
import {OrdersCollection} from "../../../collections/OrdersCollection";
import {SignaturePage} from "../../signature/signature";
import {ConfirmPopup} from "../../../components/popups/ConfirmPopup";


@IonicPage()
@Component({
  selector: 'page-order-card',
  templateUrl: 'order-card.html',
})
export class OrderCardPage extends OrderDetail {
  constructor(navCtrl: NavController,
              navParams: NavParams,
              toastCtrl: ToastController,
              localizationManager: LocalizationManager,
              loader: Loader,
              alertCtrl: AlertController,
              platform: Platform,
              private orderLinesCollections: OrdersLineCollection,
              private ordersCollection: OrdersCollection,
              changeDetectorRef: ChangeDetectorRef) {
    
    super(navCtrl, navParams, toastCtrl, localizationManager, loader, alertCtrl, platform, changeDetectorRef);
  }
  
  public onTapSubmitBtnHandler() {
    this.submitForAproval();
  }
  
  public onTapDeleteBtnHandler() {
    this.removeOrder();
  }
  
  public onTapSignatureHandler() {
    this.navCtrl.push(SignaturePage, {callback: this.setSignature.bind(this)})
  }

  public onTapSaveBtnHandler() {
    this.loader.run(this.validateAndSaveOrder.bind(this))
  }
  
  public onChangeLineHandler(orderLines: Array<OrderLine>) {
    this.recalculateOrder(orderLines);
    this.checkPageFieldsVisibilities();
  }

  public setImageSrc(signatureImg: string): string {
    return 'data:image/jpeg;base64,' + signatureImg;
  }
  
  protected init(): Promise<any> {
    return super.init()
      .then(() => {
        this.setOrderItemsComponent();
  
        return this.orderItemListComponent.loadItems();
      })
      .then((orderLine) => {
        this.recalculateOrder(orderLine || []);
      })
      .then(() => {
        this.checkPageFieldsVisibilities();
      })
  }

  protected getValidateMsg(): Promise<string> {
    return this.localizationManager.getSeveralLocales([
      'card.Tot.ToastMessage.ToastHeader',

      'card.Order.ToastMessage.RequiredSignature'
    ]).then((localization) => {

      const toastHeader = `${localization['card.Tot.ToastMessage.ToastHeader']} \n`;
      let toastBody = '';

      if(!this.order.signature) {
        toastBody = `${localization['card.Order.ToastMessage.RequiredSignature']}\n`;
      }

      const isDataValid = toastBody.length == 0;

      if(isDataValid) {
        return '';
      }

      return toastHeader + toastBody;
    });
  }
  
  private checkPageFieldsVisibilities(): void{
    const orderLine = this.orderItemListComponent.getItems();
    
    this.checkAndToggleAllBtn();
    
    if(!orderLine.length) {
      this.isSaveButtonEnable = false;
      this.isSubmitButtonEnable = false;
    }
  }

  private setSignature(dataUrl: string) {
    this.order.signature = dataUrl;
    this.dataChanged();
  }
  
  private saveOrder(){
   const deletedItems = this.orderItemListComponent
     .getDeletedItemsIds();
   
   const orderLines = this.orderItemListComponent
      .getItems();
    
   this.recalculateOrder(orderLines);
   
   return this.orderLinesCollections.deleteLinesByIds(deletedItems)
     .then(() => this.ordersCollection.updateEntity(this.order))
     .then((orders) => {
      let order = orders[0];
      
      this.order = this.ordersCollection.parseModel(order);
  
      const orderId = this.getOrderId();

      return this.orderLinesCollections.saveOrderLines(orderId.toString(), orderLines);
     })
     .then(() => {
      this.resetChanges();
      this.orderItemListComponent.resetChanges();
     })
  }
  
  private getOrderId() {
    if(this.ordersCollection.cache && !this.ordersCollection.cache.isLocalId(this.order.id || '')){
      return this.order.id;
    }
    
    return this.order._soupEntryId;
  }

  private removeOrder(): Promise<any> {
    return this.showDeleteConfirmPopup()
      .then((confirmResult: { doAction: boolean }) => {
        if (confirmResult.doAction) {
          return this.loader.run(this.doRemoveOrder.bind(this));
        }
      });
  }

  private doRemoveOrder(): Promise<any> {
    return this.navCtrl.pop()
      .then(() => {
        return this.ordersCollection.removeEntity(this.order);
      })
  }

  private showConfirmApprovalPopup(): Promise<{ doAction: boolean }> {
    const popup: ConfirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);

    return popup.showPopup({
      title: 'card.ConfirmationPopup.SubmitForApproval.Caption',
      message: 'card.ConfirmationPopup.SubmitForApproval.Question',
      yesLabel: 'common.buttons.YesBtn',
      noLabel: 'common.buttons.NoBtn'
    });
  }

  private showDeleteConfirmPopup(): Promise<{ doAction: boolean }> {
    const popup: ConfirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);

    return popup.showPopup({
      title: 'OrdersManagement.ConfirmationPopup.DeleteItem.Caption',
      message: 'OrdersManagement.ConfirmationPopup.DeleteItem.Question',
      yesLabel: 'common.buttons.YesBtn',
      noLabel: 'common.buttons.NoBtn'
    });
  }

  private submitForAproval(): Promise<any> {
    return this.showConfirmApprovalPopup()
      .then((confirmResult: { doAction: boolean }) => {
        if (confirmResult.doAction) {
          return this.loader.run(this.doSubmitForAproval.bind(this));
        }
      });
  }

  private doSubmitForAproval() {
    return this.validate()
      .then((isValid) => {
        if (isValid) {
          this.setApprovalStatus();

          return this.saveOrder()
            .then(this.checkAndToggleAllBtn.bind(this));
        }
      })
  }

  private setApprovalStatus(){
    if(this.settings.getOrderSalesRecordTypeId() == this.order.recordTypeId) {
      return this.setApprovalStatusForSales();
    }

    return this.setApprovalStatusForSample();
  }

  private setApprovalStatusForSales(){
    if(this.isSpecialPriceActive()) {
      return this.order.status = OrderScheme.APPROVAL_STATUS_SUBMITTED_OFFLINE;
    }

    return this.order.status = OrderScheme.APPROVAL_STATUS_APPROVED;
  }

  private setApprovalStatusForSample(){
    return this.order.status = OrderScheme.APPROVAL_STATUS_SUBMITTED_OFFLINE;
  }

  public validateAndSaveOrder(): Promise<any>{
    return this.validate()
      .then((isValid) => {
        if(isValid) {
          return this.saveOrder()
            .then(() => this.goBack());
        }
      })
  }
}
