import { Component, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import {ReferenceScheme} from "../../models/scheme/ReferenceScheme";
import {ReferenceCollection} from "../../collections/references/ReferenceCollection";


@IonicPage()
@Component({
  selector: 'page-reference-select-list',
  templateUrl: 'reference-select-list.html',
})
export class ReferenceSelectListPage {
  static readonly CSS_ACTIVE_CLASS = 'active';
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: ReferenceCollection;
  private callback: any;

  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private referenceCollection: ReferenceCollection) {

    this.setTableOptions();
    this.setTableCollection();

    this.searchString = '';
    this.callback = navParams.data['callback'];

    ReferenceScheme.activeRow = navParams.data['contactId'];

    this.updateSelectedElement(ReferenceScheme.activeRow);
  }

  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable()
      .then(() => {
        this.updateSelectedElement(ReferenceScheme.activeRow);
      })
  }

  public onChangeQueryHandler(searchBar: any) {
    this.updateSelectedElement(ReferenceScheme.activeRow);
  }

  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;

    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public onTapRowHandler(event, record): void {
    ReferenceScheme.activeRow = record.id;

    this.updateSelectedElement(record.id);

    Promise.resolve(this.runCallback(record))
  }

  private setTableOptions() {
    this.tableOptions = {
      headerItems: [
        {
          title: '',
          fields: [ReferenceScheme.fields.id],
          modelFunction: 'getIdForTableColumn'
        },
        {
          title: 'common.names.Name',
          fields: [ReferenceScheme.fields.name],
          isSortable: true,
          isAsc: true,
          isActive: true
        }
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
  }

  private setTableCollection() {
    return this.collection = this.referenceCollection;
  }

  private runCallback(record) {
    return this.callback(record);
  }

  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }

  private updateSelectedElement(productId) {
    const element = document.querySelectorAll('.contact-table table-row .cell:first-of-type .reference-id');

    [].slice.call(element)
      .filter((item) => {
        this.resetActiveElement(item.parentElement);

        return item.innerText == productId;
      })
      .map((item) => this.setActiveElement(item.parentElement))
  }

  private resetActiveElement(element): void {
    if (element && element.classList.contains(ReferenceSelectListPage.CSS_ACTIVE_CLASS)) {
      element.classList.remove(ReferenceSelectListPage.CSS_ACTIVE_CLASS);
    }
  }

  private setActiveElement(element): void {
    if (element) {
      element.classList.add(ReferenceSelectListPage.CSS_ACTIVE_CLASS);
    }
  }

}
