import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReferenceSelectListPage } from './reference-select-list';

@NgModule({
  declarations: [
    ReferenceSelectListPage,
  ],
  imports: [
    IonicPageModule.forChild(ReferenceSelectListPage),
  ],
})
export class ReferenceSelectListPageModule {}
