import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReferenceEditCardPage } from './reference-edit-card';

@NgModule({
  declarations: [
    ReferenceEditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(ReferenceEditCardPage),
  ],
})
export class ReferenceEditCardPageModule {}
