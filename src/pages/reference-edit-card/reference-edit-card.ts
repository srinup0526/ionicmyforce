import {Component, ViewChild, ElementRef} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertOptions, Select, ToastController, Platform, AlertController} from 'ionic-angular';
import {Contact} from "../../models/Contact";
import {Settings, SettingsImpl} from './../../services/db/Settings';
import {OrganizationListSelectPage} from "../organization-list-select/organization-list-select";
import {Organization} from "../../models/Organization";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {DataChangeRequestHelper} from "../data-change-requests/DataChangeRequestHelper";
import {Reference} from "../../models/Reference";
import {ContactListSelectPage} from "../contact-list-select/contact-list-select";
import {DataChangeRequestRefStatusPicklistDatasource} from "../../services/db/picklist-managers/datasource/DataChangeRequestRefStatusPicklistDatasource";
import {ReferenceScheme} from "../../models/scheme/ReferenceScheme";
import {BackHandlers} from "../../utils/BackHandlers";
import {Loader} from "../../services/common/Loader";
import {OrganizationsCollection} from "../../collections/OrganizationsCollection";
import {ContactsCollection} from "../../collections/ContactsCollection";


@IonicPage()
@Component({
  selector: 'page-reference-edit-card',
  templateUrl: 'reference-edit-card.html',
})
export class ReferenceEditCardPage extends BackHandlers{
  public settings: SettingsImpl;
  public reference: Reference;
  public referenceStartBackup: Reference;
  public organization: Organization;
  public contact: Contact;
  public statuses: Array<any>;
  public filterOptions: AlertOptions;

  @ViewChild('description') descriptionArea;
  @ViewChild('organizationAddress') organizationAddressArea;
  @ViewChild('organizationName') organizationNameArea;

  @ViewChild('statusSelect') statusSelect: Select;

  constructor(navCtrl: NavController,
              localizationManager: LocalizationManager,
              loader: Loader,
              alertCtrl: AlertController,
              platform: Platform,
              private organizationsCollection: OrganizationsCollection,
              private contactsCollection: ContactsCollection,
              private navParams: NavParams,
              private toastCtrl: ToastController,
              private dataChangeRequestHelper: DataChangeRequestHelper,
              private dcrRefStatusPicklistDatasource: DataChangeRequestRefStatusPicklistDatasource) {
    
    super(navCtrl, localizationManager, loader, alertCtrl, platform);
    
    this.reference = new Reference({});
    this.organization = new Organization({});
    this.contact = new Contact({});
    this.settings = Settings.getInstance();

    this.filterOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };
  }

  public onTapSaveBtnHandler() {
    this.save();
  }

  public onChangeStatusHandler(status) {
    this.reference.status = status.id;

    this.statusSelect.close();
  }

  public onTapCustomerBtnHandler() {
    this.gotoContactPage();
  }

  public onTapOrganizationBtnHandler() {
    this.gotoOrganizationPage();
  }

  public ionViewDidLoad() {
    this.reference = this.navParams.data['row'];
  
    this.loader.run(this.initData.bind(this))
      .then(() => {
        return this.setStartBackup();
      });
    
    super.ionViewDidLoad();
  }

  private gotoContactPage() {
    this.navCtrl.push(ContactListSelectPage, {
			contactId: this.contact.id,
      callback: this.setContact.bind(this)
    })
  }
  
  private loadPicklist() {
    this.dcrRefStatusPicklistDatasource.getItems()
      .then((items) => {
        this.statuses = items;
      
        this.setStatusByDefault();
      })
  }

  private gotoOrganizationPage() {
    this.navCtrl.push(OrganizationListSelectPage, {
      organizationId: this.reference.organizationSfId,
      callback: this.setOrganization.bind(this)
    })
  }

  private setOrganization(organization) {
    this.organization = organization;
    this.reference.organizationSfId = this.organization.id;
    this.reference.organizationName = this.organization.name;
    this.reference.organizationCity = this.organization.city;
    this.reference.organizationCountry = this.organization.country;
    this.reference.organizationAddress = this.organization.address;
  }

  private setContact(contact) {
    this.contact = contact;
    this.reference.contactAccountId = this.contact.accountId;
    this.reference.specialty = this.contact.specialty;
    this.reference.contactFirstName = this.contact.lastName;
    this.reference.contactLastName = this.contact.firstName;
  }

  protected validate(): Promise<any> {
    return this.localizationManager.getSeveralLocales([
      'card.ToastMessage.RequiredFieldsHeader',
      'card.ToastMessage.RequiredContact',
      'card.ToastMessage.RequiredOrganization'
    ])
      .then((localization) => {
        return new Promise((resolve, reject) => {
          const toastHeader = `${localization['card.ToastMessage.RequiredFieldsHeader']}: \n`;
          let toastBody = '';

          if (!this.reference.contactAccountId) {
            toastBody += ` ${localization['card.ToastMessage.RequiredContact']}\n`;
          }

          if (!this.reference.organizationSfId) {
            toastBody += ` ${localization['card.ToastMessage.RequiredOrganization']}\n`;
          }

          const isDataValid = toastBody.length == 0;

          if (isDataValid) {
            return resolve();
          }

          return reject(new Error(toastHeader + toastBody));
        });
      });
  }
  
  protected validateAndSaveOrder(): Promise<any> {
    return this.save();
  }
  
  protected hasChanges(){
    return this.isSameFillFields(this.reference, this.referenceStartBackup, ReferenceScheme.fields);
  }

  private save(): Promise<any> {
    return this.validate()
      .then(() => this.doSave())
      .then(() => this.goBack())
      .catch((error: Error) => {
        return this.showValidateToast(error.message);
      })
  }

  private doSave() {
    return this.dataChangeRequestHelper.createReference(this.reference);
  }

  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton: true,
      cssClass: "toastClass",
      duration: 5000,
    });

    return toast.present();
  }
  
  private initData(): Promise<any> {
    return Promise.all([
      this.loadPicklist(),
      this.fetchOrganization(),
      this.fetchContact()
    ])
  }
  
  private fetchContact(): Promise<any> {
    if(!this.reference.contactAccountId){
      return Promise.resolve();
    }
  
    return this.contactsCollection.getContactByAccountId(this.reference.contactAccountId)
      .then((contact) => {
        if(contact) {
          this.contact = contact;
          this.reference.specialty = this.contact.specialty;
          this.reference.contactFirstName = this.contact.lastName;
          this.reference.contactLastName = this.contact.firstName;
        }
      })
  }
  
  private fetchOrganization(): Promise<any> {
    if(!this.reference.organizationSfId){
      return Promise.resolve();
    }
    
    return this.organizationsCollection.fetchEntityById(this.reference.organizationSfId)
      .then((organization) => {
        if(organization){
          this.organization = organization;
          this.reference.organizationName = this.organization.name;
          this.reference.organizationCity = this.organization.city;
          this.reference.organizationCountry = this.organization.country;
          this.reference.organizationAddress = this.organization.address;
        }
      })
  }
  
  private setStatusByDefault() {
    let newStatus = this.statuses.filter((status) => status.id == ReferenceScheme.STATUS_NEW)[0];
  
    if(newStatus) {
      return this.reference.status = newStatus.id;
    }
  
    if(!this.reference.status) {
      this.reference.status = this.statuses[0].id;
    }
  }
  
  private setStartBackup() {
    this.referenceStartBackup = new Reference(this.reference);
  }
}
