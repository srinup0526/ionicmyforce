import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SingleUserListPage } from './single-user-list';

@NgModule({
  declarations: [
    SingleUserListPage,
  ],
  imports: [
    IonicPageModule.forChild(SingleUserListPage),
  ],
})
export class SingleUserListPageModule {}
