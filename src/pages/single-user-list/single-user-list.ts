import { Component, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { UsersCollection } from './../../collections/UsersCollection';
import { CoachingUsersCollection } from './../../collections/CoachingUsersCollection';
import { Settings, SettingsImpl } from './../../services/db/Settings';
import { UserScheme } from './../../models/scheme/UserScheme';


@IonicPage()
@Component({
  selector: 'page-single-user-list',
  templateUrl: 'single-user-list.html',
})
export class SingleUserListPage {
  static readonly CSS_ACTIVE_CLASS = 'active';
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: CoachingUsersCollection;
  private callback: any;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private usersCollection: UsersCollection,
              private coachingUsersCollection:CoachingUsersCollection
              ) {
  
    this.setTableOptions();
    this.setTableCollection();
    
    this.searchString = '';
    this.callback = navParams.data['callback'];
    
    UserScheme.activeRow = navParams.data['coaching-user-id']?navParams.data['coaching-user-id'].id:{};
    
    this.updateSelectedElement(UserScheme.activeRow);
  }
  
  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
    
    this.lazyTable.reloadTable()
      .then(() => {
        this.updateSelectedElement(UserScheme.activeRow);
      })
  }

  public onChangeQueryHandler(searchBar: any) {
    this.updateSelectedElement(UserScheme.activeRow);
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onTapRowHandler(event, record): void {
    UserScheme.activeRow = record.id;
    
    this.updateSelectedElement(record.id);
    
    Promise.resolve(this.runCallback(record))
  }
  
  private setTableOptions() {
    this.tableOptions = {
      headerItems: [
        {
          title: '',
          fields: [UserScheme.fields.id],
          modelFunction: 'getIdForTableColumn'
        },
        {
          title: 'common.names.User',
          fields: [UserScheme.fields.lastName, UserScheme.fields.firstName],
          isSortable: true,
          isAsc: true,
          isActive: true,
          modelFunction:'fullName'
        }
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
  }
  
  private setTableCollection() {
    this.collection = this.coachingUsersCollection;
  }
  
  private runCallback(record) {
    return this.callback(record);
  }
  
  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }
  
  private updateSelectedElement(productId) {
    const element = document.querySelectorAll('.user-single-table table-row .cell:first-of-type .contact-id');
    
    [].slice.call(element)
      .filter((item) => {
        this.resetActiveElement(item.parentElement);
        
        return item.innerText == productId;
      })
      .map((item) => this.setActiveElement(item.parentElement))
  }
  
  private resetActiveElement(element): void {
    if (element && element.classList.contains(SingleUserListPage.CSS_ACTIVE_CLASS)) {
      element.classList.remove(SingleUserListPage.CSS_ACTIVE_CLASS);
    }
  }
  
  private setActiveElement(element): void {
    if (element) {
      element.classList.add(SingleUserListPage.CSS_ACTIVE_CLASS);
    }
  }
}
