import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CallReportDetailsPage } from './call-report-details';

@NgModule({
  declarations: [
    CallReportDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CallReportDetailsPage),
  ],
})
export class CallReportDetailsPageModule {}
