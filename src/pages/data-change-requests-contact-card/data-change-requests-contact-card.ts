import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Settings, SettingsImpl } from '../../services/db/Settings';
import {DataChangeRequest} from "../../models/DataChangeRequest";
import {Organization} from "../../models/Organization";
import {Loader} from "../../services/common/Loader";
import {OrganizationsCollection} from "../../collections/OrganizationsCollection";
import {OrganizationdetailsPage} from "../organizationdetails/organizationdetails";
import {ConfirmPopup} from "../../components/popups/ConfirmPopup";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {DataChangeRequestsCollection} from "../../collections/DataChangeRequestsCollection";
import {ProductsCollection} from "../../collections/ProductsCollection";
import {ContactChinaPicklistManager} from "../../services/db/picklist-managers/ContactChinaPicklistManager";
import {ContactScheme} from "../../models/scheme/ContactScheme";


@IonicPage()
@Component({
  selector: 'page-data-change-requests-contact-card',
  templateUrl: 'data-change-requests-contact-card.html',
})
export class DataChangeRequestsContactCardPage {
  
  public isChinaUser: boolean;
  public isDeleteBtnEnable: boolean;
  public dcr: DataChangeRequest;
  public organization: Organization;
  
  public productValues: {[key: string]: string};
  public hcpStatusPicklistValues: {[key: string]: string};
  public statusDescriptionPicklistValues: {[key: string]: string};
  public genderPicklistValues: {[key: string]: string};
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private loader: Loader,
              private alertCtrl: AlertController,
              private localizationManager: LocalizationManager,
              private organizationsCollection: OrganizationsCollection,
              private productsCollection: ProductsCollection,
              private contactChinaPicklistManager: ContactChinaPicklistManager,
              private dataChangeRequestsCollection: DataChangeRequestsCollection) {
    
    const settings = Settings.getInstance();
    
    this.isDeleteBtnEnable = false;
    this.isChinaUser = settings.isChinaUser();
    this.dcr = new DataChangeRequest({});
    this.organization = new Organization({});
    
    this.productValues = {};
    this.hcpStatusPicklistValues = {};
    this.statusDescriptionPicklistValues = {};
    this.genderPicklistValues = {};
  }
  
  public onTapDeleteBtnHandler() {
    this.showDeleteConfirmPopup()
      .then((result) => {
        if(result.doAction) {
          return this.deleteDcr()
            .then(() => this.goBack());
        }
      })
  }
  
  public onTapOrganizationLinkHandler() {
    this.gotoOrganizationDetail();
  }

  public ionViewDidLoad() {
    this.initParams();
  }
  
  public initParams() {
    this.dcr = this.navParams.data['row'];

    return this.loader.run(this.init.bind(this));
  }
  
  private init() {
    this.initDeleteBtn();
    
    return this.fetchOrganization()
      .then(() => {
        if(this.isChinaUser) {
          return this.fetchAndSetProducts()
            .then(() => this.fetchPicklists())
        }
      })
  }
  
  private initDeleteBtn() {
    this.isDeleteBtnEnable = this.dcr.isDraftStatus();
  }
  
  private fetchOrganization() {
    if(!this.dcr.dcrOrganizationId) {
      return Promise.resolve();
    }
    
    return this.organizationsCollection.fetchEntityById(this.dcr.dcrOrganizationId)
      .then(this.setOrganization.bind(this))
  }
  
  private setOrganization(organization) {
    if(organization){
      this.organization = organization;
    }
  }
  
  private gotoOrganizationDetail() {
    this.navCtrl.push(OrganizationdetailsPage, { "row": this.dcr.dcrOrganizationId });
  }

  private showDeleteConfirmPopup(): Promise<{ doAction: boolean }> {
    const confirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);

    return confirmPopup.showPopup({
      title: 'card.DCR.ConfirmationPopup.DeleteItem.Caption',
      message: 'card.DCR.ConfirmationPopup.DeleteItem.Message',
      yesLabel: 'common.buttons.YesBtn',
      noLabel: 'common.buttons.NoBtn'
    });
  }

  private deleteDcr(): Promise<any> {
    return this.dataChangeRequestsCollection.removeEntity(this.dcr);
  }

  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }
  
  private fetchAndSetProducts(): Promise<any> {
    return this.productsCollection.fetchAll()
      .then((response) => this.productsCollection.getAllEntitiesFromResponse(response))
      .then((products) => products.reduce((all, product) => {
        all[product.id] =  product.name;
        return all;
      }, {}))
      .then((result) => {
        this.productValues = result;
      })
  }
  
  private fetchPicklists(): Promise<any> {
    return this.contactChinaPicklistManager.getPickLists()
      .then((picklist) => {
        this.genderPicklistValues = this.convertPicklist(picklist[ContactScheme.fields.gender.sfdc] || []);
        this.statusDescriptionPicklistValues = this.convertPicklist(picklist[ContactScheme.fields.statusDescription.sfdc] || []);
        this.hcpStatusPicklistValues = this.convertPicklist(picklist[ContactScheme.fields.hcpStatus.sfdc] || []);
      })
  }
  
  private convertPicklist(sfPicklist: Array<{value: string, label: string}>): {[key: string]: string}{
    return sfPicklist.reduce((all, product) => {
      all[product.value] =  product.label;
      return all;
    }, {})
  }
}

