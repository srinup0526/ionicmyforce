import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataChangeRequestsContactCardPage } from './data-change-requests-contact-card';

@NgModule({
  declarations: [
    DataChangeRequestsContactCardPage,
  ],
  imports: [
    IonicPageModule.forChild(DataChangeRequestsContactCardPage),
  ],
})
export class DataChangeRequestsContactCardPageModule {}
