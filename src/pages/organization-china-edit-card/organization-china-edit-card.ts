import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertOptions, Select, ToastController, AlertController, Platform} from 'ionic-angular';
import {Organization} from "../../models/Organization";
import {OrganizationRecordTypePicklistDatasource} from "../../services/db/picklist-managers/datasource/OrganizationRecordTypePicklistDatasource";
import {DataChangeRequestHelper} from "../data-change-requests/DataChangeRequestHelper";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {OrganizationChinaPickListManager} from "../../services/db/picklist-managers/OrganizationChinaPickListManager";
import {OrganizationScheme} from "../../models/scheme/OrganizationScheme";
import {BackHandlers} from "../../utils/BackHandlers";
import {Loader} from "../../services/common/Loader";
import {PickListDatasource} from "../../services/db/picklist-managers/base/PicklistDatasource";


@IonicPage()
@Component({
  selector: 'page-organization-china-edit-card',
  templateUrl: 'organization-china-edit-card.html',
})
export class OrganizationChinaEditCardPage extends BackHandlers{
  public filterOptions: AlertOptions;
  public organization: Organization;
  
  public organizationStartBackup: Organization;
  
  public organizationRecordTypeValues: Array<{id: string, description: string}>;
  public organizationStatusValues: Array<{id: string, description: string}>;
  public organizationStatusDescriptionValues: Array<{id: string, description: string}>;
  public organizationTypeValues: Array<{id: string, description: string}>;
  public organizationPropertyValues: Array<{id: string, description: string}>;
  public organizationHospitalGradeValues: Array<{id: string, description: string}>;
  public organizationTargetHospitalValues: Array<{id: string, description: string}>;
  
  public OrganizationScheme;
  
  @ViewChild('recordTypeSelect') recordTypeSelect: Select;
  @ViewChild('juridicGroupsSelect') juridicGroupsSelect: Select;
  @ViewChild('statusesSelect') statusesSelect: Select;
  
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private dataChangeRequestHelper: DataChangeRequestHelper,
              private orgRecordTypePicklistDatasource: OrganizationRecordTypePicklistDatasource,
              private organizationChinaPickListManager: OrganizationChinaPickListManager,
              localizationManager: LocalizationManager,
              loader: Loader,
              alertCtrl: AlertController,
              platform: Platform) {
  
    super(navCtrl, localizationManager, loader, alertCtrl, platform);
    
    this.filterOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };
    
    this.organization = new Organization({});
    this.organizationStartBackup = new Organization({});
    
    this.organizationRecordTypeValues = [];
    this.organizationStatusValues = [];
    this.organizationStatusDescriptionValues = [];
    this.organizationTypeValues = [];
    this.organizationPropertyValues = [];
    this.organizationHospitalGradeValues = [];
    this.organizationTargetHospitalValues = [];
    
    this.OrganizationScheme = OrganizationScheme;
  }
  
  public ionViewDidLoad() {
    this.organization = this.navParams.data['row'];
  
    this.loader.run(this.initPicklists.bind(this))
      .then(() => {
        return this.setStartBackup();
      });
  
    super.ionViewDidLoad();
  }
  
  public onTapSaveBtnHandler() {
    this.save();
  }
  
  public setDataAndCloseSelect(localField, value, ref) {
    this.organization[localField] = value;
    ref.close();
  }
  
  protected validateAndSaveOrder(): Promise<any> {
    return this.save();
  }
  
  protected hasChanges() {
    return this.isSameFillFields(this.organization, this.organizationStartBackup, OrganizationScheme.fields);
  }
  
  private initPicklists() {
    return Promise.all([
      this.fetchRecordTypes(),
      this.fetchPicklists()
    ]);
  }
  
  private validate(): Promise<any> {
    const validationFields = this.getLocalizationKeysForRequiredFields();
    const localizationKeys = this.getLocalizationHeaderKey();
    const localizationFieldsKeys = Object
      .keys(validationFields)
      .map((fieldLocalId) => {
        return validationFields[fieldLocalId];
      });
    
    
    return this.localizationManager.getSeveralLocales(localizationKeys.concat(localizationFieldsKeys))
      .then((localization) => {
        return new Promise((resolve, reject) => {
          const toastHeader = `${localization['common.alerts.NoRequiredFields']} \n`;
          
          const toastBody = Object
            .keys(validationFields)
            .filter((fieldLocalId) => !this.organization[fieldLocalId])
            .map((fieldLocalId) => localization[validationFields[fieldLocalId]])
            .join('\n - ');
          
          
          const isDataValid = toastBody.length == 0;
          
          if (isDataValid) {
            return resolve();
          }
          
          return reject(new Error(toastHeader + '- ' + toastBody));
        });
      });
  }
  
  private getLocalizationHeaderKey(): Array<string> {
    return [
      'common.alerts.NoRequiredFields'
    ];
  }
  
  private getLocalizationKeysForRequiredFields(): { [key: string]: string } {
    let validationFields = {};
    
    validationFields[OrganizationScheme.fields.name.local] = 'common.names.Name';
    validationFields[OrganizationScheme.fields.status.local] = 'common.names.Status';
    validationFields[OrganizationScheme.fields.statusDescription.local] = 'common.names.StatusDescription';
    validationFields[OrganizationScheme.fields.recordTypeId.local] = 'common.names.Type';
    validationFields[OrganizationScheme.fields.organizationType.local] = 'common.names.OrganizationType';
    validationFields[OrganizationScheme.fields.organizationProperty.local] = 'common.names.OrganizationProperty';
    validationFields[OrganizationScheme.fields.hospitalGrade.local] = 'common.names.HospitalGrade';
    validationFields[OrganizationScheme.fields.targetHospital.local] = 'common.names.TargetHospital';
    validationFields[OrganizationScheme.fields.territoryCode.local] = 'common.names.TerritoryCode';
    
    return validationFields;
  }
  
  private save(): Promise<any> {
    return this.validate()
      .then(() => this.doSave())
      .then(() => this.goBack())
      .catch((error: Error) => {
        return this.showValidateToast(error.message);
      })
  }
  
  private doSave() {
    return this.dataChangeRequestHelper.createOrganization(this.organization);
  }
  
  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton: true,
      cssClass: "toastClass",
      duration: 5000,
    });
    
    return toast.present();
  }
  
  private fetchRecordTypes(){
    this.orgRecordTypePicklistDatasource.getItems()
      .then((items) => {
        this.organizationRecordTypeValues = items;
        this.organization.recordType = items[0].id;
      });
  }
  
  private fetchPicklists(): Promise<any> {
    return this.organizationChinaPickListManager.getPickLists()
      .then((picklist) => {
  
        const pickListDatasource = new PickListDatasource(this.localizationManager);
  
        return Promise.all([
          pickListDatasource.preparePickListItems(picklist[OrganizationScheme.fields.status.sfdc] || []),
          pickListDatasource.preparePickListItems(picklist[OrganizationScheme.fields.statusDescription.sfdc] || []),
          pickListDatasource.preparePickListItems(picklist[OrganizationScheme.fields.organizationType.sfdc] || []),
          pickListDatasource.preparePickListItems(picklist[OrganizationScheme.fields.organizationProperty.sfdc] || []),
          pickListDatasource.preparePickListItems(picklist[OrganizationScheme.fields.hospitalGrade.sfdc] || []),
          pickListDatasource.preparePickListItems(picklist[OrganizationScheme.fields.targetHospital.sfdc] || [])
        ])
          .then(([
                   organizationStatusValues,
                   organizationStatusDescriptionValues,
                   organizationTypeValues,
                   organizationPropertyValues,
                   organizationHospitalGradeValues,
                   organizationTargetHospitalValues
                 ]) => {
            this.organizationStatusValues = organizationStatusValues;
            this.organizationStatusDescriptionValues = organizationStatusDescriptionValues;
            this.organizationTypeValues = organizationTypeValues;
            this.organizationPropertyValues = organizationPropertyValues;
            this.organizationHospitalGradeValues = organizationHospitalGradeValues;
            this.organizationTargetHospitalValues = organizationTargetHospitalValues;
          });
      })
  }
  
  private setStartBackup() {
    this.organizationStartBackup = new Organization(this.organization);
  }
}
