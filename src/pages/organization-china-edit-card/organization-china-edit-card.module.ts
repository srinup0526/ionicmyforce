import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganizationChinaEditCardPage } from './organization-china-edit-card';

@NgModule({
  declarations: [
    OrganizationChinaEditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganizationChinaEditCardPage),
  ],
})
export class OrganizationChinaEditCardPageModule {}
