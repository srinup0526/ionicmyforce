import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ModalOptions, LoadingController, AlertOptions } from 'ionic-angular';
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import {ProductSegmentationsCollection} from "../../collections/ProductSegmentationsCollection";
import {Query} from "../../services/common/Query";
import {ProductSegmentationScheme} from "../../models/scheme/ProductSegmentationScheme";
import {ReferenceSelectPopupComponent} from "../../components/reference-select-popup/reference-select-popup";
import {Reference} from "../../models/Reference";
import {SegmentationEditCardPage} from "../segmentation-edit-card/segmentation-edit-card";
import {ProductSegmentation} from "../../models/ProductSegmentation";


@IonicPage()
@Component({
  selector: 'page-data-change-requests-segmentation-list',
  templateUrl: 'data-change-requests-segmentation-list.html',
})
export class DataChangeRequestsSegmentationListPage {
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: ProductSegmentationsCollection;
  public settings: SettingsImpl;
  public reference: Reference;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  
  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private modalCtrl: ModalController,
              private productSegmentationsCollection: ProductSegmentationsCollection) {
    
    this.settings = Settings.getInstance();
    
    this.tableOptions = {
      headerItems: [
        {
          title: 'common.names.PharmaProduct',
          fields: [ProductSegmentationScheme.fields.phProductName],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Segmentation1',
          fields: [ProductSegmentationScheme.fields.segmentation1],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Segmentation2',
          fields: [ProductSegmentationScheme.fields.segmentation2],
          isSortable: true,
          isAsc: true,
          isActive: false
        }
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
    
    this.collection = this.productSegmentationsCollection;
    
    this.searchString = '';
  }
  
  public ngAfterContentInit(): void {
    this.presentReferenceModal({})
  }
  
  public onTapCreateSegmentationHandler(): void {
    this.gotoSegmentationCreatePage(new ProductSegmentation({}));
  }
  
  public onSearchHandler(searchBar: any): void {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onChangeQueryHandler(query: Query): void {
  }
  
  public onTapRowHandler(event, record) {
    this.gotoSegmentationCreatePage(record);
    event.stopPropagation();
  }
  
  public ionViewWillEnter(): void {
    if(this.lazyTable.isTableInited) {
      this.lazyTable.reloadTable();
    }
  }
  
  private initTable() {
    let query: string = this.getCustomQueryForTable();
  
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
    this.lazyTable.setCustomQuery(query);
  
    this.lazyTable.reloadTable();
  }
  
  private getCustomQueryForTable(): string {
    return this.productSegmentationsCollection.getSegmentationByAccountIdQuery(this.reference.contactAccountId);
  }
  
  private gotoSegmentationCreatePage(productSegmentation: ProductSegmentation) {
    this.navCtrl.push(SegmentationEditCardPage, { reference: this.reference, row: productSegmentation });
  }
  
  private presentReferenceModal(componentOptions): Promise<any> {
    const popupOptions: ModalOptions = {
      cssClass: 'reference-list-modal-popup',
      enterAnimation: 'modal-fade-in',
      leaveAnimation: 'modal-fade-out '
    };
    
    const profileModal = this.modalCtrl.create(ReferenceSelectPopupComponent, componentOptions, popupOptions);
    
    profileModal.onDidDismiss((options: { reference: Reference }) => {
      if(!options || !options.reference) {
        return this.goBack();
      }
      
      this.setActiveReference(options.reference);
      this.initTable();
    });
    
    return profileModal.present();
  }
  
  private setActiveReference(reference: Reference) {
    this.reference = reference;
  }
  
  private goBack() {
    return this.navCtrl.pop();
  }
}
