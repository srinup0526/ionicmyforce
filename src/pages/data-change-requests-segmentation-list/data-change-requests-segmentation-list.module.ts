import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataChangeRequestsSegmentationListPage } from './data-change-requests-segmentation-list';

@NgModule({
  declarations: [
    DataChangeRequestsSegmentationListPage,
  ],
  imports: [
    IonicPageModule.forChild(DataChangeRequestsSegmentationListPage),
  ],
})
export class DataChangeRequestsSegmentationListPageModule {}
