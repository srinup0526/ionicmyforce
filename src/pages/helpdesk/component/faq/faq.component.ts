import {Component, OnInit} from '@angular/core';
import {FaqService} from './../../../../services/modules/faq';
import {AttachmentsManager} from './../../../../services/common/attachments/AttachmentsManager';
import {EmailManager} from './../../../../services/common/EmailManager';
import {FAQ} from './../../../../models/Faq';
import {EmailComposerOptions} from '@ionic-native/email-composer';


@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html'
})
export class FaqComponent implements OnInit {

  public list: Array<FAQ>;

  constructor(private faqService: FaqService,
              private attachmentsManager: AttachmentsManager,
              private emailManager: EmailManager) {
    this.setQuestionsAndAnswers();
  }

  ngOnInit() {}

  public onTapSubmitIssueBtnHandler() {
    this.submitIssue();
  }

  public onTapAttachmentIconHandler(attachment) {
    this.openAttachment(attachment);
  }

  public getAttachments(item: FAQ) {
    return item.getAttachments();
  }

  private setQuestionsAndAnswers(): void {
    this.faqService.fetchFAQ()
      .then((list: Array<FAQ>) => {
        this.list = list;
      });
  }

  private openAttachment(attachment): void {
    this.attachmentsManager.openAttachment(attachment);
  }

  private submitIssue(): void {
    const emailOption: EmailComposerOptions = {
      to: ['India.ITSC@mylan.in', 'India_EM_Commercial_IT@mylan.com', 'GIDC.L2Support_CRMApplication@mylan.com'],
      subject: 'MyForce Issue',
      body: 'Please assign the ticket to GLOBAL IT APP SFDC <br/><br/><br/>',
      isHtml: true
    };

    this.emailManager.sendMail(emailOption);
  }
}




