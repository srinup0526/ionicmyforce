import { LogManager } from '../../../../services/common/LogManager';
import { EmailComposerOptions } from '@ionic-native/email-composer';
import { EmailManager } from './../../../../services/common/EmailManager';
import {Injectable} from "@angular/core";

@Injectable()
export class EmailSender {
  
  constructor(private emailManager: EmailManager,
              private logManager: LogManager){}
  
  public sendLogData(): void {
    this.logManager.getLogForSupport()
      .then((logData: string) => {
        const emailOption: EmailComposerOptions = {
          subject: 'MyForce log',
          body: logData,
          isHtml: true
        };
      
        return this.emailManager.sendMail(emailOption);
      });
  }
}

