import {Component, OnInit} from '@angular/core';
import {DeviceManager} from '../../../../services/common/DeviceManager';
import {LogManager} from '../../../../services/common/LogManager';


@Component({
  selector: 'app-last-log',
  templateUrl: './last-log.component.html'
})
export class LastLogComponent implements OnInit {

  public deviceId: string;
  public appVersion: string;
  public osVersion: string;
  public model: string;
  public logData: Array<string>;

  constructor(private logManager: LogManager) {
  }

  ngOnInit() {
    this.setDeviceData();
    this.setLogData();
  }

  private setDeviceData(): void {
    this.deviceId = DeviceManager.deviceId();
    this.appVersion = DeviceManager.appVersion();
    this.osVersion = DeviceManager.osVersion();
    this.model = DeviceManager.deviceModel();
  }

  private setLogData(): Promise<string> {
    return this.logManager.getLastDebugLog()
      .then((logData: string) => {
        if (!logData) {
          return '';
        }

        return this.prepareLogData(logData);
      });
  }

  private prepareLogData(logData: string) {
    this.logData = logData.split(LogManager.lineSeparator);
  }
}
