import {AlertController, Alert, AlertOptions} from 'ionic-angular';
import {DatabaseManager} from './../../../../services/db/DatabaseManager';
import {LocalizationManager} from './../../../../services/common/localizations/LocalizationManager';


export class DatabaseCleaner {
  
  constructor(private alertCtrl: AlertController,
              private databaseManager: DatabaseManager,
              private localizationManager: LocalizationManager) {
  }
  
  public clear(): void {
    this.startCleaningWithPopups();
  }
  
  private startCleaningWithPopups(): void {
    this.showPreCleaningAlert()
      .then((result) => {
        if (!result.doAction) {
          return;
        }
      
        return this.showWaitingAlert()
          .then((alert) => {
            return this.databaseManager.clearDatabase()
              .then(() => {
                alert.dismiss()
                  .then(() => {
                    this.showFinishClearAlert();
                  });
              })
              .catch((error) => {
                console.error(error);
                alert.dismiss();
              });
          })
      });
  }
  
  private async showPreCleaningAlert(): Promise<any> {
    const title = await this.localizationManager.promiseLocale('helpdesk.ConfirmationPopup.Caption');
    const message = await this.localizationManager.promiseLocale('helpdesk.ConfirmationPopup.Question');
    const yesLabel = await this.localizationManager.promiseLocale('common.buttons.YesBtn');
    const noLabel = await this.localizationManager.promiseLocale('common.buttons.NoBtn');
    
    return new Promise((resolve, reject) => {
      const options: AlertOptions = {
        title: title,
        message: message,
        cssClass: 'popup alert confirm',
        buttons: [
          {
            text: yesLabel,
            cssClass: 'yes',
            role: 'done',
            handler: () => {
              resolve({
                doAction: true
              });
            }
          },
          {
            text: noLabel,
            cssClass: 'no',
            role: 'cancel',
            handler: () => {
              resolve({
                doAction: false
              });
            }
          }
        ]
      };
      
      const alert = this.alertCtrl.create(options);
      
      alert.present();
    });
  }
  
  private async showWaitingAlert(): Promise<Alert> {
    const title = await this.localizationManager.promiseLocale('helpdesk.ClearingDatabaseMessage');
    const message = await this.localizationManager.promiseLocale('pendingPopup.PleaseWaitMessage');
    
    const options: AlertOptions = {
      title: title,
      message: message,
      cssClass: 'popup alert wait',
      buttons: [],
      enableBackdropDismiss: false
    };
    
    const alert = this.alertCtrl.create(options);
    
    alert.present();
    
    return alert;
  }
  
  private async showFinishClearAlert(): Promise<Alert> {
    const title: string = await this.localizationManager.promiseLocale('helpdesk.ClearingDatabaseMessage');
    const okLabel: string = await this.localizationManager.promiseLocale('common.buttons.OkBtn');
    
    const options: AlertOptions = {
      title: title,
      message: 'Database cleared successfully',
      cssClass: 'popup alert single-button',
      buttons: [
        {
          text: okLabel,
          role: 'cancel'
        }
      ]
    };
    
    const alert = this.alertCtrl.create(options);
    
    alert.present();
    
    return alert;
  }
}
