import { Component } from '@angular/core';
import { IonicPage, AlertController, NavParams } from 'ionic-angular';
import { DatabaseCleaner } from './component/database-cleaner/database-cleaner';
import { DatabaseManager } from './../../services/db/DatabaseManager';
import { EmailManager } from './../../services/common/EmailManager';
import { EmailSender } from './component/email-sender/email-sender';
import { LocalizationManager } from './../../services/common/localizations/LocalizationManager';


enum ACTIVE_TABS {
  FAQ = 'faq',
  LAST_LOG = 'last-log',
}

@IonicPage()
@Component({
  selector: 'page-helpdesk',
  templateUrl: 'helpdesk.html',
})
export class HelpdeskPage {
  static readonly ACTIVE_TABS = ACTIVE_TABS;
  
  readonly ACTIVE_TABS = ACTIVE_TABS;
  
  public activeTab: ACTIVE_TABS;
  
  constructor(private emailManager: EmailManager,
              private databaseManager: DatabaseManager,
              private emailSender: EmailSender,
              private localizationManager: LocalizationManager,
              private alertCtrl: AlertController,
              private navParams: NavParams) {
    
    this.activeTab = this.ACTIVE_TABS.LAST_LOG;
  }
  
  ngOnInit() {
    this.activeTab = this.navParams.data['activeTab'] || this.ACTIVE_TABS.LAST_LOG;
  }
  
  public onTapClearDatabaseHandler(): void {
    this.clearDatabase();
  }
  
  public onTapSendLogHandler(): void {
    this.sendLogData();
  }
  
  private sendLogData(): void {
    this.emailSender.sendLogData();
  }
  
  private clearDatabase(): void {
    const databaseCleaner: DatabaseCleaner = new DatabaseCleaner(this.alertCtrl, this.databaseManager, this.localizationManager);
    
    databaseCleaner.clear();
  }
}
