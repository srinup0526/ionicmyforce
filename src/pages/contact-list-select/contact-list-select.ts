import { Component, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import {ContactScheme} from "../../models/scheme/ContactScheme";
import {ContactsCollection} from "../../collections/ContactsCollection";


@IonicPage()
@Component({
  selector: 'page-contact-list-select',
  templateUrl: 'contact-list-select.html',
})
export class ContactListSelectPage {
  static readonly CSS_ACTIVE_CLASS = 'active';
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: ContactsCollection;
  private callback: any;

  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private contactCollection: ContactsCollection) {

    this.setTableOptions();
    this.setTableCollection();

    this.searchString = '';
    this.callback = navParams.data['callback'];

    ContactScheme.activeRow = navParams.data['contactId'];

    this.updateSelectedElement(ContactScheme.activeRow);
  }

  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable()
      .then(() => {
        this.updateSelectedElement(ContactScheme.activeRow);
      })
  }

  public onChangeQueryHandler(searchBar: any) {
    this.updateSelectedElement(ContactScheme.activeRow);
  }

  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;

    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public onTapRowHandler(event, record): void {
    ContactScheme.activeRow = record.id;

    this.updateSelectedElement(record.id);

    Promise.resolve(this.runCallback(record))
  }

  private setTableOptions() {
    this.tableOptions = {
      headerItems: [
        {
          title: '',
          fields: [ContactScheme.fields.id],
          modelFunction: 'getIdForTableColumn'
        },
        {
          title: 'common.names.Name',
          fields: [ContactScheme.fields.name],
          isSortable: true,
          isAsc: true,
          isActive: true
        },
        {
          title: 'common.names.Type',
          fields: [ContactScheme.fields.recordType],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Specialty',
          fields: [ContactScheme.fields.specialty],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Phone',
          fields: [ContactScheme.fields.mobilePhone],
          isSortable: true,
          isAsc: true,
          isActive: false
        }
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
  }

  private setTableCollection() {
    return this.collection = this.contactCollection;
  }

  private runCallback(record) {
    return this.callback(record);
  }

  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }

  private updateSelectedElement(productId) {
    const element = document.querySelectorAll('.contact-table table-row .cell:first-of-type .contact-id');

    [].slice.call(element)
      .filter((item) => {
        this.resetActiveElement(item.parentElement);

        return item.innerText == productId;
      })
      .map((item) => this.setActiveElement(item.parentElement))
  }

  private resetActiveElement(element): void {
    if (element && element.classList.contains(ContactListSelectPage.CSS_ACTIVE_CLASS)) {
      element.classList.remove(ContactListSelectPage.CSS_ACTIVE_CLASS);
    }
  }

  private setActiveElement(element): void {
    if (element) {
      element.classList.add(ContactListSelectPage.CSS_ACTIVE_CLASS);
    }
  }

}
