import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactListSelectPage } from './contact-list-select';

@NgModule({
  declarations: [
    ContactListSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactListSelectPage),
  ],
})
export class ContactListSelectPageModule {}
