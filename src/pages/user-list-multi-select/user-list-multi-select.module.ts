import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserListMultiSelectPage } from './user-list-multi-select';

@NgModule({
  declarations: [
    UserListMultiSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(UserListMultiSelectPage),
  ],
})
export class UserMultiListSelectPageModule {}
