import { Component, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { UserScheme } from './../../models/scheme/UserScheme';
import {UsersCollection} from "../../collections/UsersCollection";
import {ContactdetailsPage} from "../contactdetails/contactdetails";
import {Contact} from "../../models/Contact";


@IonicPage()
@Component({
  selector: 'page-user-list-multi-select',
  templateUrl: 'user-list-multi-select.html',
})
export class UserListMultiSelectPage {
  static readonly CSS_ACTIVE_CLASS = 'active';
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: UsersCollection;
  private callback: any;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private userCollection: UsersCollection) {
    
    this.setTableOptions();
    this.setTableCollection();
    
    this.searchString = '';
    this.callback = navParams.data['callback'];
    
    UserScheme.activeRows = navParams.data['ids'] || [];
  }
  
  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
    
    this.lazyTable.reloadTable()
      .then(() => {
        this.updateSelectedElement(UserScheme.activeRows);
      })
  }
  
  public onChangeQueryHandler(searchBar: any) {
    this.updateSelectedElement(UserScheme.activeRows);
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onTapRowHandler(event, record): void {
    const selectedIndexOnActiveRows = UserScheme.activeRows.indexOf(record.id);
    
    if(~selectedIndexOnActiveRows) {
      UserScheme.activeRows.splice(selectedIndexOnActiveRows, 1);
    } else {
      UserScheme.activeRows.push(record.id);
    }
    
    this.updateSelectedElement(UserScheme.activeRows);
    
    Promise.resolve(this.runCallback(UserScheme.activeRows))
  }
  
  private setTableOptions() {
    this.tableOptions = {
      headerItems: [
        {
          title: '',
          fields: [UserScheme.fields.id],
          modelFunction: 'getIdForTableColumn'
        },
        //{
        //   title: 'common.names.BUSpecialty',
        //   fields: [ContactScheme.fields.buSpecialty],
        //   isSortable: false,
        //   isAsc: true,
        //   isActive: false
        // },
        {
          title: 'common.names.User',
          fields: [UserScheme.fields.lastName, UserScheme.fields.firstName],
          isSortable: false,
          isAsc: true,
          modelFunction: 'fullName',
          isActive: false,
          // handler: (event, contact) => {
          //   this.gotoContact(contact);
            
          //   event.stopPropagation();
          // }
        },
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
  }
  
  private setTableCollection() {
    return this.collection = this.userCollection;
  }
  
  private runCallback(record) {
    return this.callback(record);
  }
  
  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }
  
  private updateSelectedElement(activeIds) {
    const elements = document.querySelectorAll('.user-table table-row .cell:first-of-type .contact-id');
    
    [].slice.call(elements)
      .filter((item) => {
        this.resetActiveElement(item.parentElement);
        
        return ~activeIds.indexOf(item.innerText);
      })
      .map((item) => this.setActiveElement(item.parentElement));
  }
  
  private resetActiveElement(element): void {
    if (element && element.classList.contains(UserListMultiSelectPage.CSS_ACTIVE_CLASS)) {
      element.classList.remove(UserListMultiSelectPage.CSS_ACTIVE_CLASS);
    }
  }
  
  private setActiveElement(element): void {
    if (element) {
      element.classList.add(UserListMultiSelectPage.CSS_ACTIVE_CLASS);
    }
  }
  
  private gotoContact(contact: Contact): Promise<any> {
    return this.navCtrl.push(ContactdetailsPage, { "row": contact});
  }
}
