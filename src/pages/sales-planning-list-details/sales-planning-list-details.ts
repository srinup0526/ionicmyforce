import {Component, ElementRef, ViewChild} from '@angular/core';
import {
	IonicPage, NavController, NavParams, LoadingController, ModalController, PopoverController ,AlertOptions,
	Select, ToastController, AlertController
} from 'ionic-angular';
import { SalesplanfilterComponent } from '../../components/salesplanfilter/salesplanfilter'
import { SalesplanestimatedComponent } from '../../components/salesplanestimated/salesplanestimated';
import { SalesplanmodelPage } from '../salesplanmodel/salesplanmodel'
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { SalesPlanningListCollection } from '../../collections/SalesPlanningListCollection';
import { SalesPlanningListScheme } from '../../models/scheme/SalesPlanningListScheme'
import {FilterPanelComponent} from "../../components/filter/filter-panel/filter-panel";
import {Query} from "../../services/common/Query";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { from } from 'rxjs';
import { SalesPlanProduct } from '../../models/SalesPlanProduct';
import { SalesPlan } from '../../models/SalesPlan';
import { SalesPlanningScheme } from '../../models/scheme/SalesPlanningScheme';
import { SalesPlanningCollection } from '../../collections/SalesPlanningCollection';
import { PatchCustomerCollection } from '../../collections/PatchCustomerCollection';

import moment from 'moment';

import { SalesPlanningProductCollection } from '../../collections/SalesPlanningProductCollection';

@Component({
	selector: 'page-sales-planning-list-details',
	templateUrl: 'sales-planning-list-details.html',
})
export class SalesPlanningListDetailsPage {
	public isSearchbarOpened = false;
	public contactsCount:any = 0;

	targetNontarget:string = "tc";
	salesPlan:any;
	references: Array<any> = [];
    
	public tableOptions: LazyTableOption;
	public customCondition: string;
	public filterWhereCondition: any;
	public filterPanelFields: any;
	public searchString: string;
	public search: string;
	public collection: SalesPlanningListCollection;
	public filterOptions: AlertOptions;
	public settings: SettingsImpl;
    hideSubmitButton: boolean = false;
    public selectedData: any;
	public customerList: any;

	selectedRecords: any;

	@ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
	@ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
	@ViewChild('filterType') filterTypeComponent: Select;
	@ViewChild('mainContent') mainContent: ElementRef;
	items: any;
	// @ViewChild(SalesplanfilterComponent) filterPanel: SalesplanfilterComponent;
	// @ViewChild('mainContent') mainContent: ElementRef;
	constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
		public salesPlanningListCollection: SalesPlanningListCollection,
		public patchCustomerCollection: PatchCustomerCollection,
		public popoverCtrl: PopoverController,
		private toastCtrl: ToastController,
		public alertCtrl: AlertController,
		public salesplanCollection: SalesPlanningCollection) {

		//this.salesPlanningListCollection.modifyResponse(navParams.data['row'].id);
		
	let selectedRowData = this.navParams.get('row');
	this.salesPlan = this.navParams.get('row') || {};
	console.log("slected and filled data",selectedRowData);
	console.log("slected and filled id",selectedRowData.id);
	console.log("slected and filled salesplanning id",selectedRowData.salesplanning);
	this.salesPlanningListCollection.fetchEntityById(selectedRowData.id)
		// .then(records=>{ return this.salesPlanningListCollection.getAllEntitiesFromResponse(records);})
		.then(res=>{
			console.log("Selected customer records",res);
			this.selectedRecords = res;
			console.log("Selected customer records based on id",this.selectedRecords);
		})


	if(selectedRowData.status == "Approved"){
		this.hideSubmitButton = false;
	   }else if(selectedRowData.status == "New" || selectedRowData.status == "Rejected" ){
		this.hideSubmitButton = true;
		}
	   else{
		   this.hideSubmitButton = false;
	   }
		this.settings = Settings.getInstance();

		const headerItems = [
		{
			title: 'common.SalesPlanning.type',
			fields: [SalesPlanningListScheme.fields.class],
			isSortable: true,
			isAsc: true,
			isActive: false,
			picklistValues: {},

		},
		{
			title: 'common.SalesPlanning.totalproductsplanned',
			fields: [SalesPlanningListScheme.fields.totalProductPlanned],
			isSortable: true,
			isAsc: true,
			isActive: false,
			picklistValues: {}
		},
		{
			title: 'common.SalesPlanning.totalordervalue',
			fields: [SalesPlanningListScheme.fields.totalOrderValue],
			isSortable: true,
			isAsc: true,
			isActive: false
		},
		{
			title: 'common.SalesPlanning.contactname',
			fields: [SalesPlanningListScheme.fields.DoctorName],
			isSortable: true,
			isAsc: true,
			isActive: false
		}
		];

		this.tableOptions = {
			headerItems: headerItems,
			rowHandler: (event, reference) => {
				// this.gotoContactDetail(reference);
			},
			batchSize: 100
		};

		this.searchString = '';

		this.collection = this.salesPlanningListCollection;
		console.log("customer salesplan per month", this.collection);

		this.filterOptions = {
			cssClass: 'popup alert list without-header',
			buttons: []
		};

		// customer list from Patch Collection

		this.patchCustomerCollection.fetchAll()
		.then(records=>{ return this.patchCustomerCollection.getAllEntitiesFromResponse(records);})
		.then(res=>{
			console.log("res",res);
			this.customerList = res;
			console.log("Customer list in sales planning >>>> ",this.customerList);
		})
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SalesplanningPage');
		
	}


	public onChangeQueryHandler(query: Query): void {
		this.setReferenceCount();
	}

	public onChangeContactTypeHandler(type) {
		// if(type == 'tc') {
			this.collection = this.salesPlanningListCollection;
			// }else{
				//   this.collection = this.salesPlanningListCollection;
				// }

				// this.targetNontarget = type;

				this.filterTypeComponent.close();

				this.lazyTable.setCollection(this.collection);
				this.lazyTable.reloadTable();
			}

			public onSwipeHandler(event){
				switch (event.offsetDirection) {
					case 2: this.filterPanel.hidePanel(); break;
					case 4: this.filterPanel.showPanel(); break;
				}
			}

			public onFilterChangeHandler(filterData): void {
				this.lazyTable.reloadTableByFilterData(filterData);
			}

			public onSearchHandler(searchBar: any) {
				this.searchString = searchBar.value || '';
				this.lazyTable.setSearchString(this.searchString);
				this.lazyTable.reloadTable();
			}

			public onClearHandler(ev: any): void {
				ev.target.value = '';
				this.searchString = ev.target.value;

				this.lazyTable.setSearchString(this.searchString);
				this.lazyTable.reloadTable();
			}

			customerSubmitAction(){
				console.log('Submitted salesplan of specified month....');
			}
			itemselected(customer){
				console.log("Selected Item", customer);

				console.log("customer Name", customer.contactName);
                console.log("customer status", customer.status);
                
				const startOfMonth = moment().startOf('month').format('YYYY-MM-DD hh:mm');
				const endOfMonth   = moment().endOf('month').format('YYYY-MM-DD hh:mm');
				var startDate = moment(startOfMonth).add(24,'d');
				var endDate = moment(startOfMonth).add(1,'M').add(10,'d');
				console.log("startDate",startDate);
				console.log("endDate",endDate); 
				console.log("startDate <= currentDate",startDate <= currentDate);
				console.log("currentDate >= endDate",currentDate >= endDate);
				var currentDate = moment();
				if(startDate <= currentDate && currentDate >= endDate ){
				// if(!(currentDate>=startDate)  || (currentDate <= endDate) ){
					let toast = this.toastCtrl.create({
						message: 'It is available only after 25th of every month to till next month 10th',
						duration: 3000,
						position: 'middle'
					});

					toast.onDidDismiss(() => {
						console.log('Dismissed toast');
					});

					toast.present();
				}
				else{
					console.log("customer.id",customer.id);
					console.log("this.salesPlan.id",this.salesPlan.id);
					console.log("this.salesPlan.id.contains('local_')",this.salesPlan.id.includes('local_'));
					let salesPlanId = this.salesPlan.id.includes('local_')?this.salesPlan._soupEntryId:this.salesPlan.id;
					this.salesPlanningListCollection.fetchEntityByCustomer(customer.id,salesPlanId)
					.then(salesPlanData=>{
						console.log('sales plan data existing', salesPlanData);
						if(salesPlanData.records.length>0)
						{
							console.log('Data rxisted redirected to popup with existing data')
							const popover = this.popoverCtrl.create(SalesplanestimatedComponent, {"customer":customer, "salesplanlist": salesPlanData.records[0]}, { cssClass: "popuporderBackground" });
							popover.present({
								//  ev: myEvent
							});
						}
						else
						{	
							this.salesPlanningListCollection.createEntity({
								customer:customer.id,
								salesplanning:salesPlanId
							})
							.then((record) => this.salesPlanningListCollection.parseModel(record))
							//.then(res=> this.salesPlanningListCollection.getEntityFromResponse(res))
							.then(salesPlanList=>{
								console.log("salesPlanList",salesPlanList);
								const popover = this.popoverCtrl.create(SalesplanestimatedComponent, {"customer":customer, "salesplanlist":salesPlanList}, { cssClass: "popuporderBackground" });
								popover.present({
									//  ev: myEvent
								});
							})
							
							
						}
					})
					
				}
			}
			submitApproval(){
				let confirm = this.alertCtrl.create({
				  title: `Please confirm..?`,
				  message: `Do you want Submit it for approval?`,
				  buttons: [
					{
					  text: 'No',
					  handler: () => {
			  
						confirm.dismiss();
					  }
					},
					{
					  text: 'Yes',
					  handler: () => {
						this.salesPlan.status = SalesPlanningScheme.APPROVAL_STATUS_SUBMITTED_OFFLINE;
						let success = (items) => {console.log("items",items);
					  }
					 
					  let failure = (error) => console.error(`Soup Upsert Error: ${error}`);
					  console.log("this sales paln",this.salesPlan);
					  this.salesplanCollection.updateEntity(this.salesPlan)
					   
					}
					}
				  ]
				});
			  
				confirm.present();
				 }
			public ionViewDidEnter(): void {}

			public ionViewWillEnter(): void {}

			public ngAfterContentInit() {
				this.lazyTable.setTableOptions(this.tableOptions);
				this.lazyTable.setSearchString(this.searchString);
				this.lazyTable.setCollection(this.collection);

				this.lazyTable.reloadTable();

				this.setCssClassForChina();
			}

			private setCssClassForChina() {
				if(this.settings.isChinaUser()) {
					this.mainContent.nativeElement.classList.add('china');
				}
			}

			private setReferenceCount(): number {
				const countQuery = this.collection.getCountQuery();
				const query = this.lazyTable.getTableQueryWithConditions(countQuery);

				return this.collection.runCountQuery(query.query)
				.then((count) => {
					return this.contactsCount = count || 0;
				})
			}

		}

