import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesPlanningListDetailsPage } from './sales-planning-list-details';

@NgModule({
  declarations: [
    SalesPlanningListDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesPlanningListDetailsPage),
  ],
})
export class SalesPlanningListDetailsPageModule {}
