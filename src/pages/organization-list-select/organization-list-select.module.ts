import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganizationListSelectPage } from './organization-list-select';

@NgModule({
  declarations: [
    OrganizationListSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganizationListSelectPage),
  ],
})
export class OrganizationListSelectPageModule {}
