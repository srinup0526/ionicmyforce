import { Component, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { OrganizationsCollection } from './../../collections/OrganizationsCollection';
import { Settings, SettingsImpl } from './../../services/db/Settings';
import { OrganizationScheme } from './../../models/scheme/OrganizationScheme';


@IonicPage()
@Component({
  selector: 'page-organization-list-select',
  templateUrl: 'organization-list-select.html',
})
export class OrganizationListSelectPage {
  static readonly CSS_ACTIVE_CLASS = 'active';
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: OrganizationsCollection;
  private callback: any;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private organizationsCollection: OrganizationsCollection) {
    
    this.setTableOptions();
    this.setTableCollection();
    
    this.searchString = '';
    this.callback = navParams.data['callback'];
    
    OrganizationScheme.activeRow = navParams.data['organizationId'];
    
    this.updateSelectedElement(OrganizationScheme.activeRow);
  }
  
  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
    
    this.lazyTable.reloadTable()
      .then(() => {
        this.updateSelectedElement(OrganizationScheme.activeRow);
      })
  }
  
  public onChangeQueryHandler(searchBar: any) {
    this.updateSelectedElement(OrganizationScheme.activeRow);
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onTapRowHandler(event, record): void {
    OrganizationScheme.activeRow = record.id;
    
    this.updateSelectedElement(record.id);
    
    Promise.resolve(this.runCallback(record))
  }
  
  private setTableOptions() {
    this.tableOptions = {
      headerItems: [
        {
          title: '',
          fields: [OrganizationScheme.fields.id],
          modelFunction: 'getIdForTableColumn'
        },
        {
          title: 'common.names.Name',
          fields: [OrganizationScheme.fields.name],
          isSortable: true,
          isAsc: true,
          isActive: true
        },
        {
          title: 'common.names.Type',
          fields: [OrganizationScheme.fields.recordType],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.BillingAddress',
          fields: [OrganizationScheme.fields.city, OrganizationScheme.fields.address],
          isSortable: true,
          isAsc: true,
          isActive: false,
          modelFunction: 'fullAddress'
        },
        {
          title: 'common.names.Phone',
          fields: [OrganizationScheme.fields.phone],
          isSortable: true,
          isAsc: true,
          isActive: false
        }
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
  }
  
  private setTableCollection() {
    return this.collection = this.organizationsCollection;
  }
  
  private runCallback(record) {
    return this.callback(record);
  }
  
  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }
  
  private updateSelectedElement(productId) {
    const element = document.querySelectorAll('.account-table table-row .cell:first-of-type .account-id');
    
    [].slice.call(element)
      .filter((item) => {
        this.resetActiveElement(item.parentElement);
        
        return item.innerText == productId;
      })
      .map((item) => this.setActiveElement(item.parentElement))
  }
  
  private resetActiveElement(element): void {
    if (element && element.classList.contains(OrganizationListSelectPage.CSS_ACTIVE_CLASS)) {
      element.classList.remove(OrganizationListSelectPage.CSS_ACTIVE_CLASS);
    }
  }
  
  private setActiveElement(element): void {
    if (element) {
      element.classList.add(OrganizationListSelectPage.CSS_ACTIVE_CLASS);
    }
  }
}
