import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PharmadetailsPage } from '../pharmadetails/pharmadetails';
import { AddpharmaeventPage } from '../addpharmaevent/addpharmaevent';
import { AppointmentDetailsPage } from '../appointment-details/appointment-details';
import { IndiaAppointmentDetailsPage } from '../../pages/india-appointment-details/india-appointment-details';
import { IndiaCallreportDetailsPage } from '../../pages/india-callreport-details/india-callreport-details'


import { CallReportDetailsPage } from '../call-report-details/call-report-details';
import { OrganizationdetailsPage } from '../organizationdetails/organizationdetails';

import { PharmaEventScheme } from  '../../models/scheme/PharmaEventScheme';
import { PharmaEventsCollection } from  '../../collections/PharmaEventsCollection';
import { TableHeaderItemOption } from './../../components/table/table-header/TableHeaderItemOption';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { FilterPanelComponent } from "../../components/filter/filter-panel/filter-panel";
import { Settings, SettingsImpl } from '../../services/db/Settings';

import { CallReportScheme } from  '../../models/scheme/CallReportScheme';
import { AppointmentsCollection } from '../../collections/CallReportsCollection/AppointmentsCollection';
import { AppointmentsPastCollection } from '../../collections/CallReportsCollection/AppointmentsPastCollection';
import { AppointmentsTodayCollection } from '../../collections/CallReportsCollection/AppointmentsTodayCollection';
import { AppointmentsTomorrowCollection } from '../../collections/CallReportsCollection/AppointmentsTomorrowCollection';
import { CallReportsCollection } from '../../collections/CallReportsCollection/CallReportsCollection';
import { CallsCollection } from '../../collections/CallReportsCollection/CallsCollection';
import { CallsTodayCollection } from '../../collections/CallReportsCollection/CallsTodayCollection';
/**
* Generated class for the ActivitiesPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-india-activities',
  templateUrl: 'india-activities.html',
})
export class IndiaActivitiesPage {
  showNewTable: boolean = false;
  appointmentModel:string='';
  public pharmaCount:any = 0;
  public tableOptions: LazyTableOption;
  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: any;
  public settings: SettingsImpl;
  appointmentHeaderItems : any;
  pharmaEventHeaderItems : any;
  callReportHeaderItems : any;
  public getMod :any;
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('mainContent') mainContent: ElementRef;


  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public pharmaEventCollection: PharmaEventsCollection,
    public appointmentsCollection: AppointmentsCollection,
    public appointmentsPastCollection: AppointmentsPastCollection,
    public appointmentsTodayCollection: AppointmentsTodayCollection,
    public appointmentsTomorrowCollection: AppointmentsTomorrowCollection,
    public callReportsCollection: CallReportsCollection,
    public callsCollection: CallsCollection,
    public callsTodayCollection: CallsTodayCollection,
    ){
    this.getMod = "mod1";
    this.showNewTable = false;
    this.settings = Settings.getInstance();
    this.appointmentModel = "mod1";
    this.appointmentHeaderItems = [
    {
      title: 'common.names.DateTime',
      fields: [CallReportScheme.fields.dateTimePlanned],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {},
      modelFunction:'getAppointmentDateTimePlannedInFormat'
    },
    {
      title: 'common.names.Customer',
      fields: [CallReportScheme.fields.contactFirstName, CallReportScheme.fields.contactLastName],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {},
      modelFunction: 'contactFullName'
    },
    {
      title: 'common.names.BUSpecialty',
      fields: [CallReportScheme.fields.indCustomerBU],
      isSortable: false,
      isAsc: true,
      isActive: false

    },
    {
      title: 'common.names.Organization',
      fields: [CallReportScheme.fields.organizationName, CallReportScheme.fields.organizationAddress, CallReportScheme.fields.organizationCity],
      isSortable: false,
      isAsc: true,
      isActive: false,
      modelFunction:'organizationNameAndAddress'
    },
    {
      title: 'common.names.User',
      fields: [CallReportScheme.fields.userLastName,CallReportScheme.fields.userFirstName ],
      isSortable: false,
      isAsc: true,
      isActive: false,
      modelFunction:'userFullName'
    }
    ];

    this.callReportHeaderItems = [
    {
      title: 'common.names.DateTimeVisitStart',
      fields: [CallReportScheme.fields.dateTimeVisitStart],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {},
      modelFunction:'getCallReortDateTimeActualInFormat'
    },
    {
      title: 'common.names.Customer',
      fields: [CallReportScheme.fields.contactFirstName, CallReportScheme.fields.contactLastName],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {},
      modelFunction: 'contactFullName'
    },
    {
      title: 'common.names.BUSpecialty',
      fields: [CallReportScheme.fields.indCustomerBU],
      isSortable: false,
      isAsc: true,
      isActive: false,
    },
    {
      title: 'common.names.Status',
      fields: [CallReportScheme.fields.status],
      isSortable: false,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.User',
      fields: [CallReportScheme.fields.userLastName,CallReportScheme.fields.userFirstName ],
      isSortable: false,
      isAsc: true,
      isActive: false,
      modelFunction:'userFullName'
    },

    ];

    this.tableOptions = {
      headerItems: this.appointmentHeaderItems,
      rowHandler: (event, reference) => {
        this.itemTapped(reference,CallReportScheme.TYPE_APPOINTMENT);
      },
      batchSize: 100
    };

    this.searchString = '';
    this.collection = this.appointmentsTodayCollection;
  }

  onChangeQueryHandler() {

  }

  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable();
  }

  ionViewDidLoad() : void
  {


  }

  loadlazyTable(headerItemsname : any, collectionname : any,type:string) {
    this.tableOptions = {
      headerItems: headerItemsname,
      rowHandler: (event, reference) => {
        this.itemTapped(reference,type);
      },
      batchSize: 100
    };

    this.searchString = '';
    this.collection = collectionname;
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(collectionname);

    this.lazyTable.reloadTable();

  }


  modo(value: string){
    this.getMod = value;
    if(value == "mod1") {
      this.showNewTable = false;
      console.log("value dropdown",value);
      this.loadlazyTable(this.appointmentHeaderItems, this.appointmentsTodayCollection,CallReportScheme.TYPE_APPOINTMENT);
    } else if(value == "mod2") {
      console.log("value dropdown",value);
      this.showNewTable = false;
      this.loadlazyTable(this.appointmentHeaderItems, this.appointmentsCollection,CallReportScheme.TYPE_APPOINTMENT);
    }
    else if(value == "mod3") {
      console.log("value dropdown",value);
      this.showNewTable = false;
      this.loadlazyTable(this.appointmentHeaderItems, this.appointmentsPastCollection,CallReportScheme.TYPE_APPOINTMENT);
    }
    else if(value == "mod4") {
      console.log("value dropdown",value);
      this.showNewTable = false;
      this.loadlazyTable(this.appointmentHeaderItems, this.appointmentsTomorrowCollection,CallReportScheme.TYPE_APPOINTMENT);
    }else if(value == "mod5") {
      console.log("value dropdown",value);
      this.showNewTable = false;
      this.loadlazyTable(this.callReportHeaderItems, this.callsCollection,CallReportScheme.TYPE_ONE_TO_ONE);
    } else if(value == "mod6") {
      console.log("value dropdown",value);
      this.showNewTable = false;
      this.loadlazyTable(this.callReportHeaderItems, this.callsTodayCollection,CallReportScheme.TYPE_ONE_TO_ONE);
    }
  }

  onSearch(searchBar: any) {
    console.log("search text",searchBar)
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();

  }

  onSearchPharma(searchBar: any) {
    console.log(searchBar);
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  itemTapped(row,type) {
    console.log("type",type);
    console.log("row",row);
    if(type==CallReportScheme.TYPE_APPOINTMENT)
    {
      this.navCtrl.push(IndiaAppointmentDetailsPage,{"row":row})
    }
    else if(type==CallReportScheme.TYPE_ONE_TO_ONE)
    {
      this.navCtrl.push(IndiaCallreportDetailsPage,{"row":row})
    }
  }

  public ionViewWillEnter(): void {
    this.lazyTable.reloadTable();
  }
}
