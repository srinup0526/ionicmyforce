import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndiaActivitiesPage } from './india-activities';

@NgModule({
  declarations: [
    IndiaActivitiesPage,
  ],
  imports: [
    IonicPageModule.forChild(IndiaActivitiesPage),
  ],
})
export class IndiaActivitiesPageModule {}
