import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndiaCallreportDetailsPage } from './india-callreport-details';

@NgModule({
  declarations: [
    IndiaCallreportDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(IndiaCallreportDetailsPage),
  ],
})
export class IndiaCallreportDetailsPageModule {}
