import { Component, ViewChild,Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams,PopoverController, ToastController , AlertController} from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
declare var cordova: any;
import { OAuth, DataService } from 'forcejs'
//import { PopoverpresentationComponent } from '../../components/popoverpresentation/popoverpresentation';
import { Storage } from '@ionic/storage';
import { EditCallReportPage } from '../edit-call-report/edit-call-report';
import { UsersCollection } from '../../collections/UsersCollection';
import { ProductsCollection } from '../../collections/ProductsCollection';
import { MarketingMessagesCollection } from '../../collections/MarketingMessagesCollection';
import { CallReportsCollection } from '../../collections/CallReportsCollection/CallReportsCollection';

import {
  addMinutes,
} from 'date-fns';
import * as Constants from '../../services/constants';
import moment from 'moment';

import { OrganizationdetailsPage } from '../organizationdetails/organizationdetails';
import { ContactdetailsPage } from '../contactdetails/contactdetails';
import { AddRedFlagPage } from '../add-red-flag/add-red-flag';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';

/**
 * Generated class for the CallReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-india-callreport-details',
  templateUrl: 'india-callreport-details.html',
})
export class IndiaCallreportDetailsPage {
  callReport:any;
  Media : any;
  soupEntryId:string=null;
  message:string = "";
  productName1:any="";
  productName2:any="";
  productName3:any="";
  productName4:any="";
  productName5:any="";
  productName6:any="";
  redFlagRequired:boolean = false;

  productMessage1:any="";
  productMessage2:any="";
  productMessage3:any="";
  productMessage4:any="";
  productMessage5:any="";
  productMessage6:any="";
  productMessage7:any="";
  productMessage8:any="";
  productMessage9:any="";
  productMessage10:any="";
  productMessage11:any="";
  productMessage12:any="";
  productMessage13:any="";
  productMessage14:any="";
  productMessage15:any="";
  productMessage16:any="";
  productMessage17:any="";
  productMessage18:any="";
  productMessage19:any="";
  productMessage20:any="";
  productMessage21:any="";
  productMessage22:any="";
  productMessage23:any="";
  productMessage24:any="";
  productMessage25:any="";
  productMessage26:any="";
  productMessage27:any="";
  productMessage28:any="";
  productMessage29:any="";
  productMessage30:any="";

  marketingMessages:any;
  marketingMessages1:any;
  marketingMessages2:any;
  marketingMessages3:any;
  marketingMessages4:any;
  marketingMessages5:any;

  noteForPrio1:any="";
  noteForPrio2:any="";
  noteForPrio3:any="";
  noteForPrio4:any="";
  noteForPrio5:any="";
  noteForPrio6:any="";
  shownGroup = null;
  displayHeader:any="";
  displayCard1:any="true";
  displayCard2:any="true";
  displayCard3:any="true";
  displayCard4:any="true";
  displayCard5:any="true";
  displayCard6:any="true";
  PharmaMessage:any;
  callreportBackup:any;
  contact: any;
  Reference:any;
  CallReport:any;
  Organisation:any;
  pharmaProducts:any;
  excludedProducts:any = [];
  pharmaProductsId:any;
  row:any;
  coachingVisit:any;
  dateTimePlanned:any;
  Customer_Name__c:any;
  Org_Name__c:any;
  ActivitesName:'Activites';
  activeUser:any;
  JointVisitParticipants:any = [];
  CoachingVisitUser:any;
  saveAppointmentData: {};
  contactInformation:any;
  CoachingVisitUsers:any;
  DateTimeVisitStart:any;

  Duration:string = "30";
  @ViewChild("card1") CardContent:any;
  @ViewChild("card2") CardContent2:any;
  @ViewChild("card3") CardContent3:any;
  @ViewChild("card4") CardContent4:any;
  @ViewChild("card5") CardContent5:any;
  @ViewChild("card6") CardContent6:any;
  
  // customerName: String;
  // organisationName:String;
  typeofvisit: any;
  PharmaMessageBackup: any;
  loggedInUserDetails:any;
  public serviceOnline:any;
  callObjectives:any="";
  callComments:any;
  nextCallObjective:any;
  users:any;
  coachingPicklistOptions:any = [];
  typeOfVisitPicklistOptions:any = [];
  durationPickListOptions:any = [];
  config:any;

  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }
  constructor(public storage:Storage, 
    public toastCtrl: ToastController, 
    public navCtrl: NavController,
    public renderer:Renderer,
    private popoverController: PopoverController,
     public navParams: NavParams,
     public service:SmartstoreServiceProvider,
     public usersCollection: UsersCollection,
     public productsCollection: ProductsCollection,
     public marketingMessagesCollection: MarketingMessagesCollection,
     public alertCtrl: AlertController,
     public callReportsCollection: CallReportsCollection) {
    let oauth = OAuth.createInstance();
    oauth.login()
    .then(oauthResult => {
      this.serviceOnline = DataService.createInstance(oauthResult);
    })
   // this.storage.get("currentUserDetails").then(data => { this.loggedInUserDetails = data; console.log("UserData",this.loggedInUserDetails); });
   this.CallReport = this.navParams.data['row'];
    console.log("row",this.navParams.data['row']);
    this.callObjectives  = this.CallReport.callObjective;
    this.callComments = this.CallReport.generalComments;
    this.nextCallObjective = this.CallReport.nextCallObjective;

    this.CoachingVisitUser = this.CallReport.getCoachingVisitUser();
    if(this.CallReport.coachingVisitUserSfid) {
      this.usersCollection.fetchEntityById(this.CallReport.coachingVisitUserSfid) .then((callReport) => {
        console.log("coachingVisitUserSfid",callReport);
       // this.CoachingVisitUser = callReport.name;
      })
    }

    this.productName1 = this.CallReport.getProductName();
    console.log("productName1", this.productName1);
    this.loadDependentData();
    this.loadConfigData();
   
    this.storage.get(Constants.CallReport+'PicklistValues').then(picklistValues=>{
    console.log("picklistValues",picklistValues);

    if(this.CallReport.prio1ProductSfid) {
      this.displayHeader = "true";
      this.productName1 = this.getProductName(this.CallReport.prio1ProductSfid);
      this.CallReport.noteForPrio1 ? this.noteForPrio1 =  this.CallReport.noteForPrio1 : "";
      var namedata =  Promise.resolve(this.getMessageName(this.CallReport.prio1MarketingMessage1));
      console.log("this.getMessageName",namedata);
      this.CallReport.prio1MarketingMessage1 ? this.productMessage1 =  this.getMessageName(this.CallReport.prio1MarketingMessage1) : "";
      this.CallReport.prio1MarketingMessage2 ? this.productMessage2 = this.getMessageName(this.CallReport.prio1MarketingMessage2) : "";
      this.CallReport.prio1MarketingMessage3 ? this.productMessage3 = this.getMessageName(this.CallReport.prio1MarketingMessage3) : "";
      this.CallReport.prio1MarketingMessage4 ? this.productMessage4 = this.getMessageName(this.CallReport.prio1MarketingMessage4) : "";
      this.CallReport.prio1MarketingMessage5 ? this.productMessage5 = this.getMessageName(this.CallReport.prio1MarketingMessage5) : "";
      console.log("display card",this.productMessage1,this.productMessage2, this.productMessage3, this.productMessage4, this.productMessage5 )
      if(this.productMessage1 == "" && this.productMessage2 == "" && this.productMessage3 == "" && this.productMessage4 == "" && this.productMessage5 == "" && this.noteForPrio1 == "") {
        this.displayCard1 = "false";
      }
    }
     
    if(this.CallReport.prio2ProductSfid) {
      this.displayHeader = "true";
      this.productName2 =this.getProductName(this.CallReport.prio2ProductSfid);
      this.CallReport.noteForPrio2 ? this.noteForPrio2 =  this.CallReport.noteForPrio2 : "";
      this.CallReport.prio2MarketingMessage1 ? this.productMessage6 = this.getMessageName(this.CallReport.prio2MarketingMessage1) : "";
      this.CallReport.prio2MarketingMessage2 ? this.productMessage7 = this.getMessageName(this.CallReport.prio2MarketingMessage2) : "";
      this.CallReport.prio2MarketingMessage3 ? this.productMessage8 = this.getMessageName(this.CallReport.prio2MarketingMessage3) : "";
      this.CallReport.prio2MarketingMessage4 ? this.productMessage9 = this.getMessageName(this.CallReport.prio2MarketingMessage4) : "";
      this.CallReport.prio2MarketingMessage5 ? this.productMessage10 = this.getMessageName(this.CallReport.prio2MarketingMessage5) : "";
      if(this.productMessage6 == "" && this.productMessage7 == "" && this.productMessage8 == "" && this.productMessage9 == "" && this.productMessage10 == "" && this.noteForPrio2 == "") {
        this.displayCard2 = "false";
      }   
    }
      
    if(this.CallReport.prio3ProductSfid) {
      this.displayHeader = "true";
      this.productName3 = this.getProductName(this.CallReport.prio3ProductSfid);
      this.CallReport.noteForPrio3 ? this.noteForPrio3 =  this.CallReport.noteForPrio3 : "";
      this.CallReport.prio3MarketingMessage1 ? this.productMessage11 = this.getMessageName(this.CallReport.prio3MarketingMessage1) : "";
      this.CallReport.prio3MarketingMessage2 ? this.productMessage12 = this.getMessageName(this.CallReport.prio3MarketingMessage2) : "";
      this.CallReport.prio3MarketingMessage3 ? this.productMessage13 = this.getMessageName(this.CallReport.prio3MarketingMessage3) : "";
      this.CallReport.prio3MarketingMessage4 ? this.productMessage14 = this.getMessageName(this.CallReport.prio3MarketingMessage4) : "";
      this.CallReport.prio3MarketingMessage5 ? this.productMessage15 = this.getMessageName(this.CallReport.prio3MarketingMessage5) : "";
      if(this.productMessage11 == "" && this.productMessage12 == "" && this.productMessage13 == "" && this.productMessage14 == "" && this.productMessage15 == "" && this.noteForPrio3 == "") {
        this.displayCard3 = "false";
      }
    }
    
      
    if(this.CallReport.prio4ProductSfid) {
      this.displayHeader = "true";
      this.productName4 = this.getProductName(this.CallReport.prio4ProductSfid);
      this.CallReport.noteForPrio4 ? this.noteForPrio4 =  this.CallReport.noteForPrio4 : "";
      this.CallReport.prio4MarketingMessage1 ? this.productMessage16 = this.getMessageName(this.CallReport.prio4MarketingMessage1) : "";
      this.CallReport.prio4MarketingMessage2 ? this.productMessage17 = this.getMessageName(this.CallReport.prio4MarketingMessage2) : "";
      this.CallReport.prio4MarketingMessage3 ? this.productMessage18 = this.getMessageName(this.CallReport.prio4MarketingMessage3) : "";
      this.CallReport.prio4MarketingMessage4 ? this.productMessage19 = this.getMessageName(this.CallReport.prio4MarketingMessage4) : "";
      this.CallReport.prio4MarketingMessage5 ? this.productMessage20 = this.getMessageName(this.CallReport.prio4MarketingMessage5) : "";
      if(this.productMessage16 == "" && this.productMessage17 == "" && this.productMessage18 == "" && this.productMessage19 == "" && this.productMessage20 == "" && this.noteForPrio4 == "") {
        this.displayCard4 = "false";
      }
    }
      
    if(this.CallReport.prio5ProductSfid) {
      this.displayHeader = "true";
      this.productName5 = this.getProductName(this.CallReport.prio5ProductSfid);
      this.CallReport.noteForPrio5 ? this.noteForPrio5 =  this.CallReport.noteForPrio5 : "";
      this.CallReport.prio5MarketingMessage1 ? this.productMessage21 = this.getMessageName(this.CallReport.prio5MarketingMessage1) : "";
      this.CallReport.prio5MarketingMessage2 ? this.productMessage22 = this.getMessageName(this.CallReport.prio5MarketingMessage2) : "";
      this.CallReport.prio5MarketingMessage3 ? this.productMessage23 = this.getMessageName(this.CallReport.prio5MarketingMessage3) : "";
      this.CallReport.prio5MarketingMessage4 ? this.productMessage24 = this.getMessageName(this.CallReport.prio5MarketingMessage4) : "";
      this.CallReport.prio5MarketingMessage5 ? this.productMessage25 = this.getMessageName(this.CallReport.prio5MarketingMessage5) : "";
      console.log("display card",this.productMessage21,this.productMessage22, this.productMessage23, this.productMessage24, this.productMessage25 )
      if(this.productMessage21 == "" && this.productMessage22 == "" && this.productMessage23 == "" && this.productMessage24 == "" && this.productMessage25 == "" && this.noteForPrio5 == "") {
        this.displayCard5 = "false";
      }
    }
      
    if(this.CallReport.prio6ProductSfid) {
      this.displayHeader = "true";
      this.productName6 = this.getProductName(this.CallReport.prio6ProductSfid);
      this.CallReport.noteForPrio6 ? this.noteForPrio6 =  this.CallReport.noteForPrio6 : "";
      this.CallReport.prio6MarketingMessage1 ? this.productMessage26 = this.getMessageName(this.CallReport.prio6MarketingMessage1) : "";
      this.CallReport.prio6MarketingMessage2 ? this.productMessage27 = this.getMessageName(this.CallReport.prio6MarketingMessage2) : "";
      this.CallReport.prio6MarketingMessage3 ? this.productMessage28 = this.getMessageName(this.CallReport.prio6MarketingMessage3) : "";
      this.CallReport.prio6MarketingMessage4 ? this.productMessage29 = this.getMessageName(this.CallReport.prio6MarketingMessage4) : "";
      this.CallReport.prio6MarketingMessage5 ? this.productMessage30 = this.getMessageName(this.CallReport.prio6MarketingMessage5) : "";
      if(this.productMessage26 == "" && this.productMessage27 == "" && this.productMessage28 == "" && this.productMessage29 == "" && this.productMessage30 == "" && this.noteForPrio6 == "") {
        this.displayCard6 = "false";
      }
    }
  
  })
    let Id = this.CallReport.hasOwnProperty('meta')?this.CallReport.meta.appointmentEvents.Id:this.CallReport.Id;
    let _soupEntryId = this.CallReport.hasOwnProperty('meta')?this.CallReport.meta.appointmentEvents._soupEntryId:this.CallReport._soupEntryId;
    this.soupEntryId = _soupEntryId;
    this.loadAllUsers(),
    this.loadPharmamessages(),
    this.loadproducts()
    .then(data => {
      console.log("data",data);
      console.log("this.excludedProducts",this.excludedProducts);
    });
    // this.getEntityByIdorSoupEntryId(Id,_soupEntryId)
    // .then(res=>{
    //   console.log("Object Info",res);
    // })
   
  }

  getProductName(ProductSfid) {
    return ProductSfid;
    // return this.productsCollection.fetchEntityById(ProductSfid) .then((callReportProduct) => {
    //   console.log("callReport",callReportProduct);
    //  return callReportProduct.name;
    // })
  }

  getMessageName(marketingMessageSource) {
    return marketingMessageSource;
    // return this.marketingMessagesCollection.fetchEntityById(marketingMessageSource) .then((callReportMessage) => {
    //   console.log("callReportMessage",callReportMessage.name);
    //   callReportMessage.name;
    // })
  }

  loadDependentData()
  {
    return SforceDataContext.getActiveUser()
    .then(activeUser=>{
      console.log("activeUser",activeUser);
      this.activeUser = activeUser;
    })
  }

  loadConfigData()
  {
    return ConfigurationManager.getConfig()
    .then(config=>{
      console.log("config",config);
      this.config = config;
     
      this.redFlagRequired = config.sampleManagementSettings.isItRedFlagRequired;
      this.durationPickListOptions = config.tourPlanningSettings?config.tourPlanningSettings.duration.split(","):"";
    })
  }

  setCallObjectiveInfo(customerId:string){
    // this.service.getCallListForSpecificContact(customerId).then(data=>{
    //   console.log("data",data);
    //   this.callObjectives = data.records[0][0].Next_Call_Objective__c;
    // })
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };

  setJointVisitParticipants(participants:string)
  {
    if(participants!=null)
    {
      let participantArray = participants.split(';');
      console.log("participantArray",participantArray);
      let array = [];
      console.log("this.JointVisitParticipantUsers",this.JointVisitParticipants)
      array = this.users.filter((user) => {
        console.log("user",user);
        console.log("participantArray.indexOf(user.Name)",participantArray.indexOf(user.Name));
        console.log("user.Name in participantArray",user.Name in participantArray);
        return (participantArray.indexOf(user.Name)>-1);
      });
      let finalParticipants = [];
      array.forEach(val=>{
        finalParticipants.push(val);
      })
      console.log("array",array);
      console.log("finalParticipants",finalParticipants);
      this.JointVisitParticipants = array;

    }
  }

  ionViewDidLoad() {
    console.log("information loaded")
  }
  loadAllUsers() {
    return this.service.loadAllUsers()
      .then(results => {
        console.log('Users',results);
        this.users = results.records;
        this.CoachingVisitUsers = results.records;
      })
  }

  tappedRedFlag(reference:any)
  {
    console.log("this.CallReport",this.CallReport);
    this.service.getContactReference(this.CallReport.Contact1__c)
    .then(results => {

      this.CallReport = results.records[0];
      this.navCtrl.push(AddRedFlagPage,{'row':this.CallReport});
    });
  }

  getCoachingBasedUsers(coachingVisist : string)
  {
    console.log("coachingVisist",coachingVisist);

    if(coachingVisist!='')
    {
      this.CoachingVisitUsers = this.users.filter((user) => {
        console.log("coachingVisist.toUpperCase()",coachingVisist.toUpperCase())
        return (user.JointVisitType__c == coachingVisist.toUpperCase() && user.DefaultCurrencyIsoCode== this.loggedInUserDetails.DefaultCurrencyIsoCode);
      })
    }
    else
    {
      this.CoachingVisitUsers = this.users;
    }
  }

  getContact(id: string) {
    this.service.getContact(id)
      .then(results => {

        this.contact = results.records[0];
      })
  }
  getContactReference(id: string) {
    this.service.getContactReference(id)
      .then(results => {

        this.CallReport = results.records[0];
        console.log("refContactdetails",this.CallReport);
      })
    }
  loadCallReports() {

    return this.service.loadCallReports()
      .then(results => {
        console.log('callreport',results);
        this.callReport = results.records;
        
        this.callreportBackup = results.records;
      })
  }
  getOrganization(id: string) {
    this.service.getOrganization(id)
    .then(results => {
      this.Organisation = results.records[0];
      console.log("OrganisationDetails",this.Organisation);
    })
  }
  // saveCallReport()
  // {
  //   let saveCallReport = {
  //   };
  //   saveCallReport["Created_Offline__c"] = true;
  //   saveCallReport["Date_Time_Planned__c"] = this.dateTimePlanned;
  //   saveCallReport["Date_Time_Actual__c"]=this.DateTimeVisitStart;
  //   saveCallReport["Date_time_Visit_End__c"]=addMinutes(new Date(this.DateTimeVisitStart),30);
  //   saveCallReport["Organisation__c"] = this.CallReport.Organisation__c;
  //   saveCallReport["Organisation__r"] = this.Organisation?this.Organisation:this.CallReport.Organisation__r;
  //   saveCallReport["Contact1__c"] = this.CallReport.Contact1__c;
  //   saveCallReport["Contact1__r"] = this.contact?this.contact:this.CallReport.Contact1__r;
  //   saveCallReport["User__r"] = this.CallReport.User__r;
  //   saveCallReport["User__c"] = this.loggedInUserDetails.Id
  //   saveCallReport["Type_of_visit__c"] = this.typeofvisit;
  //   saveCallReport["_soupEntryId"] = this.soupEntryId?this.soupEntryId:null;
  //   saveCallReport["Duration__c"] = this.Duration?parseInt(this.Duration):30;
  //   saveCallReport["Type__c"] = Constants.oneToone;
  //   saveCallReport["RecordTypeId"] = this.config?this.config.callReportRecordTypeId:"01230000000IgPP";
  //   saveCallReport["Coaching_Visit__c"] = this.coachingVisit;
  //   saveCallReport["Coaching_Visit_User__c"] = this.CoachingVisitUser;
  //   console.log("typeof (this.JointVisitParticipants)",typeof (this.JointVisitParticipants));
  //   // if(typeof (this.JointVisitParticipants)!=undefined && this.JointVisitParticipants.length>0)
  //   // {
  //   //   saveCallReport["Joint_Visit_Participants__c"] = this.JointVisitParticipants.join(";");
  //   // }
  //   //saveCallReport["Call_Objective__c"] = this.callObjectives;
  //   let jointParticipantsNamedArray = [];
  //   if(typeof (this.JointVisitParticipants)!=undefined && this.JointVisitParticipants.length>0)
  //   {
  //      this.JointVisitParticipants.forEach(jointVisitParticipant=>{
  //        jointParticipantsNamedArray.push(jointVisitParticipant.Name);
  //      });
  //     console.log("jointParticipantsNamedArray",jointParticipantsNamedArray);
  //     saveCallReport["Joint_Visit_Participants__c"] =  jointParticipantsNamedArray.join(";");
  //   }
  //   saveCallReport['attributes'] = {type: Constants.CallReport}
  //   saveCallReport["Call_Report_Status__c"] = "Saved";
  //   saveCallReport["__locally_created__"] = false;
  //   saveCallReport["__locally_updated__"] = true;
  //   saveCallReport["__locally_deleted__"] = false;
  //   saveCallReport["__local__"] = true;
  //   saveCallReport["Prio_1_Product__c"]=this.productName1
  //   saveCallReport["Prio_2_Product__c"]=this.productName2
  //   saveCallReport["Prio_3_Product__c"]=this.productName3
  //   saveCallReport["Prio_4_Product__c"]=this.productName4
  //   saveCallReport["Prio_5_Product__c"]=this.productName5
  //   saveCallReport["Prio_6_Product__c"]=this.productName6


  //   saveCallReport["Prio_1_Marketing_Message_1__c"]=this.productMessage1
  //   saveCallReport["Prio_1_Marketing_Message_2__c"]=this.productMessage2
  //   saveCallReport["Prio_1_Marketing_Message_3__c"]=this.productMessage3
  //   saveCallReport["Prio_1_Marketing_Message_4__c"]=this.productMessage4
  //   saveCallReport["Prio_1_Marketing_Message_5__c"]=this.productMessage5

  //   saveCallReport["Prio_2_Marketing_Message_1__c"]=this.productMessage6
  //   saveCallReport["Prio_2_Marketing_Message_2__c"]=this.productMessage7
  //   saveCallReport["Prio_2_Marketing_Message_3__c"]=this.productMessage8
  //   saveCallReport["Prio_2_Marketing_Message_4__c"]=this.productMessage9
  //   saveCallReport["Prio_2_Marketing_Message_5__c"]=this.productMessage10

  //   saveCallReport["Prio_3_Marketing_Message_1__c"]=this.productMessage11
  //   saveCallReport["Prio_3_Marketing_Message_2__c"]=this.productMessage12
  //   saveCallReport["Prio_3_Marketing_Message_3__c"]=this.productMessage13
  //   saveCallReport["Prio_3_Marketing_Message_4__c"]=this.productMessage14
  //   saveCallReport["Prio_3_Marketing_Message_5__c"]=this.productMessage15

  //   saveCallReport["Prio_4_Marketing_Message_1__c"]=this.productMessage16
  //   saveCallReport["Prio_4_Marketing_Message_2__c"]=this.productMessage17
  //   saveCallReport["Prio_4_Marketing_Message_3__c"]=this.productMessage18
  //   saveCallReport["Prio_4_Marketing_Message_4__c"]=this.productMessage19
  //   saveCallReport["Prio_4_Marketing_Message_5__c"]=this.productMessage20

  //   saveCallReport["Prio_5_Marketing_Message_1__c"]=this.productMessage21
  //   saveCallReport["Prio_5_Marketing_Message_2__c"]=this.productMessage22
  //   saveCallReport["Prio_5_Marketing_Message_3__c"]=this.productMessage23
  //   saveCallReport["Prio_5_Marketing_Message_4__c"]=this.productMessage24
  //   saveCallReport["Prio_5_Marketing_Message_5__c"]=this.productMessage25

  //   saveCallReport["Prio_6_Marketing_Message_1__c"]=this.productMessage26
  //   saveCallReport["Prio_6_Marketing_Message_2__c"]=this.productMessage27
  //   saveCallReport["Prio_6_Marketing_Message_3__c"]=this.productMessage28
  //   saveCallReport["Prio_6_Marketing_Message_4__c"]=this.productMessage29
  //   saveCallReport["Prio_6_Marketing_Message_5__c"]=this.productMessage30

    


  //   // saveCallReport["Prio_1_Marketing_Message_1__c"]="Elidel can help keep patients flare-free for longer periods of time."
  //   console.log("saveCallReport",saveCallReport);
  //   let success = (data) => {
  //     console.log("Data Upserted successfully and created data is",data);
  //     this.navCtrl.pop();

  //   }
  //   let failure = (error) => {
  //     console.log("Data Upsertion having an issue",error);
  //   }
  //  // this.saveCallOnline();
  //   this.smartStore().upsertSoupEntries(Constants.CallReport, [saveCallReport], success, failure);
    
  // }

  // saveCallOnline()
  // {
    
  //   let saveCallReport = {
  //   };
  //   saveCallReport["Created_Offline__c"] = true;
  //   saveCallReport["Date_Time_Actual__c"]=this.DateTimeVisitStart;
  //   //saveCallReport["Date_time_Visit_End__c"]=new Date();
  //   saveCallReport["Organisation__c"] = this.CallReport.Organisation__c;
  //   saveCallReport["Contact1__c"] = this.CallReport.Contact1__c;
  //   saveCallReport["User__c"] = this.loggedInUserDetails.Id
  //   saveCallReport["Type_of_visit__c"] = this.typeofvisit;
    
  //   saveCallReport["Duration__c"] = this.Duration?parseInt(this.Duration):30;
  //   saveCallReport["Type__c"] = "1:1";
  //   saveCallReport["Id"] = this.CallReport.Id;
  //   saveCallReport["RecordTypeId"] = this.config?this.config.callReportRecordTypeId:"01230000000IgPP";
  //   saveCallReport["Coaching_Visit__c"] = this.coachingVisit;
  //   saveCallReport["Coaching_Visit_User__c"] = this.CoachingVisitUser;
  //   if(typeof (this.JointVisitParticipants)!=undefined && this.JointVisitParticipants.length>0)
  //   {
  //     saveCallReport["Joint_Visit_Participants__c"] = this.JointVisitParticipants.join(";");
  //   }
  //   //saveCallReport["Call_Objective__c"] = this.callObjectives;
  //   saveCallReport['attributes'] = {type: Constants.CallReport}
  //   saveCallReport["Call_Report_Status__c"] = "Saved";
  //   saveCallReport["Prio_1_Product__c"]=this.productName1
  //   saveCallReport["Prio_2_Product__c"]=this.productName2
  //   saveCallReport["Prio_3_Product__c"]=this.productName3
  //   saveCallReport["Prio_4_Product__c"]=this.productName4
  //   saveCallReport["Prio_5_Product__c"]=this.productName5
  //   saveCallReport["Prio_6_Product__c"]=this.productName6


  //   saveCallReport["Prio_1_Marketing_Message_1__c"]=this.productMessage1
  //   saveCallReport["Prio_1_Marketing_Message_2__c"]=this.productMessage2
  //   saveCallReport["Prio_1_Marketing_Message_3__c"]=this.productMessage3
  //   saveCallReport["Prio_1_Marketing_Message_4__c"]=this.productMessage4
  //   saveCallReport["Prio_1_Marketing_Message_5__c"]=this.productMessage5

  //   saveCallReport["Prio_2_Marketing_Message_1__c"]=this.productMessage6
  //   saveCallReport["Prio_2_Marketing_Message_2__c"]=this.productMessage7
  //   saveCallReport["Prio_2_Marketing_Message_3__c"]=this.productMessage8
  //   saveCallReport["Prio_2_Marketing_Message_4__c"]=this.productMessage9
  //   saveCallReport["Prio_2_Marketing_Message_5__c"]=this.productMessage10

  //   saveCallReport["Prio_3_Marketing_Message_1__c"]=this.productMessage11
  //   saveCallReport["Prio_3_Marketing_Message_2__c"]=this.productMessage12
  //   saveCallReport["Prio_3_Marketing_Message_3__c"]=this.productMessage13
  //   saveCallReport["Prio_3_Marketing_Message_4__c"]=this.productMessage14
  //   saveCallReport["Prio_3_Marketing_Message_5__c"]=this.productMessage15

  //   saveCallReport["Prio_4_Marketing_Message_1__c"]=this.productMessage16
  //   saveCallReport["Prio_4_Marketing_Message_2__c"]=this.productMessage17
  //   saveCallReport["Prio_4_Marketing_Message_3__c"]=this.productMessage18
  //   saveCallReport["Prio_4_Marketing_Message_4__c"]=this.productMessage19
  //   saveCallReport["Prio_4_Marketing_Message_5__c"]=this.productMessage20

  //   saveCallReport["Prio_5_Marketing_Message_1__c"]=this.productMessage21
  //   saveCallReport["Prio_5_Marketing_Message_2__c"]=this.productMessage22
  //   saveCallReport["Prio_5_Marketing_Message_3__c"]=this.productMessage23
  //   saveCallReport["Prio_5_Marketing_Message_4__c"]=this.productMessage24
  //   saveCallReport["Prio_5_Marketing_Message_5__c"]=this.productMessage25

  //   saveCallReport["Prio_6_Marketing_Message_1__c"]=this.productMessage26
  //   saveCallReport["Prio_6_Marketing_Message_2__c"]=this.productMessage27
  //   saveCallReport["Prio_6_Marketing_Message_3__c"]=this.productMessage28
  //   saveCallReport["Prio_6_Marketing_Message_4__c"]=this.productMessage29
  //   saveCallReport["Prio_6_Marketing_Message_5__c"]=this.productMessage30
  //   console.log("callArray",saveCallReport);
  //   this.serviceOnline.update("Call_Report__c",saveCallReport)
  //   .then(results=>{
  //     console.log("successfully inserted CallReport",results);
  //   },function(err){
  //     console.log("error",err);
  //   });
  // } 


  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
 
  loadMedia(){
    return this.service.loadMedia()
    .then(results => {
      console.log("media",results);
      this.Media=results.records;
    })
  }

  resetProductsList()
  {
    if(this.excludedProducts.length>0)
    {
      this.pharmaProducts = this.pharmaProducts.filter(i=>{
        console.log("i.Id",i.Id);
        console.log("this.excludedProducts",this.excludedProducts);
        if(this.excludedProducts.indexOf(i.Id) <= -1)
        {
          return i;
        }
      })
    }
  }
 
    loadPharmamessages(){
      return this.service.loadPharmamessages()
      .then(results => {
        console.log("PharmaMessage",results);
        this.PharmaMessage=results.records;
        this.PharmaMessageBackup=results.records;
      })
    }
    loadproducts(){
      return this.service.loadproducts()
      .then(results => {
        console.log("loadproducts",results);
        this.pharmaProducts=results.records;
      })
    }
    getpharmaproductId(id: string) {
      this.service.getpharmaproductId(id)
        .then(results => {
  
          this.pharmaProductsId = results.records[0];
          console.log("pharmaProductsId",this.pharmaProductsId);
        })
       
    }
    presentationPopover(myEvent) {
   
   // let popover= this.popoverController.create(PopoverpresentationComponent);
   //  popover.present({
   //    ev: myEvent
   //  });
    }

    openPresentations()
    {
      alert("This functionality work in progress for now")
    }

    // validateCallReport()
    // {
    //   if(this.validateCallInputData())
    //   {
    //     this.saveCallReport()
    //   }
    //   else
    //   {
    //     const toast = this.toastCtrl.create({
    //       message: this.message,
    //       showCloseButton:true,
    //       cssClass:"toastClass",
    //       position:'middle',
    //       duration:3000,
    //     });
    //     toast.present();
    //   }
    // }

    // validateCallInputData()
    // {
    //   let valid:boolean = true;
    //   this.message = "";
    //   if(this.productName1=='' || this.productName1==null)
    //   {
    //     valid = false;
    //     this.message+=" First Product Mandatory";
    //   }
    //   if(this.productMessage1=='' || this.productMessage1==null)
    //   {
    //     valid = false;
    //     this.message+=" First Product Marketing Key Message Mandatory";
    //   }
    //   // if((this.dateTimePlanned=='' || this.dateTimePlanned==null))
    //   // {
    //   //   valid = false;
    //   //   this.message+="Date Time Planned Mandatory";
    //   // }
    //   if((this.DateTimeVisitStart =='' || this.DateTimeVisitStart==null))
    //   {
    //     valid = false;
    //     this.message+=" Date Time Visit Can not be Empty";
    //   }
    //   if(new Date(this.dateTimePlanned)>=new Date(this.DateTimeVisitStart))
    //   {
    //     valid = false;
    //     this.message+=" Planned Date Can not be grater than the Visit Start Date";
    //   }

    //   // if(new Date(this.DateTimeVisitStart)>=new Date(this.CallReport.Date_time_Visit_End__c))
    //   // {
    //   //   valid = false;
    //   //   this.message+="Date Time Visit Start Can not be grater than the Visit End Date";
    //   // }

    //   if(this.coachingVisit!=null && (this.CoachingVisitUser=='' || this.CoachingVisitUser==null))
    //   {
    //     valid = false;
    //     this.message+=" Coaching Visit User Mandatory to select when coaching visit selected ";
    //   }

    //   return valid;
    // }


    navigateToOrganisationPage(org:any){
      console.log("row",org);
      let row = {};
      row["Id"] = org.Organisation__c;
      this.navCtrl.push(OrganizationdetailsPage, { "row": row });
    }

    navigateToContactPage(con:any){
      let row = {};
      row["Customer__c"] = con.Contact1__c;
      console.log("row",row);
      this.navCtrl.push(ContactdetailsPage, { "row": row });
    }
    
    editCallReport(){
      this.navCtrl.push(EditCallReportPage,{'row':this.CallReport});
    }

    deleteCallReport() {
      let confirmDelete = this.alertCtrl.create({
        title: `Delete Call Report?`,
        message: `Are you sure you want to delete`,
        buttons: [
          {
            text: 'No',
            handler: () => {
  
              confirmDelete.dismiss();
            }
          },
          {
            text: 'Yes',
            handler: () => {
              console.log("this.CallReport",this.CallReport);
              this.callReportsCollection.removeEntity(this.CallReport)
                .then((result) => {
  
                   this.navCtrl.pop();
             
                });
            } 
          }
        ]
      });
  
      confirmDelete.present();
    }
}


