import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConvertCallReportPage } from './convert-call-report';

@NgModule({
  declarations: [
    ConvertCallReportPage,
  ],
  imports: [
    IonicPageModule.forChild(ConvertCallReportPage),
  ],
})
export class CallReportPageModule {}
