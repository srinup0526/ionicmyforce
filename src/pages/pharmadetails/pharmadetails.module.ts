import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PharmadetailsPage } from './pharmadetails';

@NgModule({
  declarations: [
    PharmadetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PharmadetailsPage),
  ],
})
export class PharmadetailsPageModule {}
