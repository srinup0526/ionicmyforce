import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController ,AlertController,ToastController} from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { Storage } from '@ionic/storage';
import { ActivitiesPage } from '../activities/activities';
import moment from 'moment';
import * as Constants from '../../services/constants';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PETypeEventPickListDatasource } from '../../services/db/picklist-managers/datasource/PETypeEventPickListDatasource';
import { PEStagePickListDatasource } from '../../services/db/picklist-managers/datasource/PEStagePickListDatasource'
import { ReferenceCollection } from './../../collections/references/ReferenceCollection';
import { ProductsCollection } from './../../collections/ProductsCollection';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { FilterPanelComponent } from "../../components/filter/filter-panel/filter-panel";
import {Query} from "../../services/common/Query";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { PharmaEvent } from '../../models/PharmaEvent';
import { PEAttendee } from '../../models/PEAttendee';
import { PharmaEventsCollection } from '../../collections/PharmaEventsCollection';
import { PEAttendeesCollection } from '../../collections/PEAttendeesCollection';
import { PharmaEventScheme } from '../../models/scheme/PharmaEventScheme';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import {AttendeesListSelectPage} from "../attendees-list-select/attendees-list-select";
import {ProductListSelectPage} from "../product-list-select/product-list-select";
import {ContactsCollection} from "../../collections/ContactsCollection";
import {Loader} from "../../services/common/Loader";
import { PEAttendeeScheme } from '../../models/scheme/PEAttendeeScheme';


declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-pharmadetails',
  templateUrl: 'pharmadetails.html',
})
export class PharmadetailsPage {
  PharmaEventid:any;
  PharmaEvent:any;
  productsData:any;
  speakersData:any;
  attendessData:any = [];
  selectedPharmaProducts:any = [];
  selectedProductsData:any = [];
  selectedAttendeesData:any = [];
  selectedSpeakersData:any = [];
  loggedInUserDetails:any;
  pharameventvalidation:any;
  message = "";
  stagePicklistOptions:any = [];
  typeOfEventPicklistOptions:any = [];
  viewEnteredDateTime:any;
  attendeesList:any;
  productList:any;
  dropdownList = [];
  selectedItems = [];
  eventName : any = "";
  dropdownSettings = {};
  startDate:any = new Date().toISOString();
  endDate: any = new Date().toISOString();
  evaluation : any = "";
  eventType : any = "";
  speakers : any = "";
  objectives : any = "";
  stage : any = "";
  location : any = "";
  agenda : any = "";
  pharmaEventRecord : any;
  public activeUser:any;
  public picklistDataSource:any;
  public picklistStageSource:any;
  public pharmaCount:any = 0;

  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: PharmaEventsCollection;
  public settings: SettingsImpl;
  public typeFilterOptions:string = 'all';
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('mainContent') mainContent: ElementRef;
  
  
  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }

  public selectedAttendeesStringList : string = "";
  private selectedAttendeesIds: Array<string> = [];

  public selectedProductStringList : string = "";
  private selectedProductIds: Array<string> = [];

  Referenceteam:Array<{ Id: string,Customer__c:any, Account: any,Specialty1__c:any,Customer_Name__c: any,Org_Name__c:any,Organisation__c:any; Org_Brick_Name__c:any;}>;
 // PharmaEvent: Array<{ Name: any, OwnerId: any, Owner: any, Status__c:any, Type_of_Event__c:any, Stage__c:any, Start_Date__c:any,Location__c:any }>;
  constructor(public toastCtrl: ToastController,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController, 
    public storage: Storage,
    public navParams: NavParams,
    private service: SmartstoreServiceProvider,
    private peTypeEventPickListDatasource : PETypeEventPickListDatasource,
    private peStagePickListDatasource : PEStagePickListDatasource,
    private pharmaEventsCollection : PharmaEventsCollection,
    private peAttendeesCollection : PEAttendeesCollection,
    private contactsCollection : ContactsCollection,
    private productsCollection : ProductsCollection,
    private loader : Loader) {
    this.settings = Settings.getInstance();
    SforceDataContext.getActiveUser()
    .then((currentUser) => {
      console.log("currentUser",currentUser);
      this.activeUser = currentUser;
    });
  
  this.viewEnteredDateTime = moment();

  this.peTypeEventPickListDatasource.getItems()
  .then(items=>{
    console.log("peTypeEventPickListDatasource",items);
    this.picklistDataSource = items;
  })

  this.peStagePickListDatasource.getItems()
  .then(items=>{
    console.log("peStagePickListDatasource",items);
    this.picklistStageSource = items;
  })

  const attendeesCollection: ReferenceCollection = new ReferenceCollection();
  const productItemsCollection: ProductsCollection = new ProductsCollection();

  attendeesCollection
    .fetchAll()
    .then((records) => {
      return attendeesCollection.getAllEntitiesFromResponse(records);
    })
    .then((users) => {
      console.log(users);
      this.attendeesList = users;
      
    });

    productItemsCollection
    .fetchAll()
    .then((records) => {
      return productItemsCollection.getAllEntitiesFromResponse(records);
    })
    .then((users) => {
      console.log(users);
      this.productList = users;
      
    });
       
    ConfigurationManager.getConfig()
    .then(config=>{
      console.log("config",config);
      this.pharameventvalidation = config.pharmaEventSettings;
    }) 
  }

  itemTapped(row: string) {
    console.log("totdetailspagerow",row)
    //this.navCtrl.push(TotdetailsPage, { "row": row });

  }

  public onTapAttendeeHandler() {		
    this.gotoAttendeePage();		
  }

  public onTapProductHandler() {	
    this.gotoProductPage();	
  }


  ionViewDidLoad() {
    let row= this.navParams.data['row'];
    console.log('ionViewDidLoad pharmadetails', row);
    
    this.pharmaEventsCollection.fetchEntityById(this.navParams.data['row'].id).then(peRes=>{
      console.log("pharmaEventRecord",peRes);
      this.pharmaEventRecord = peRes;
      this.startDate = this.pharmaEventRecord.startDate;
      this.endDate = this.pharmaEventRecord.endDate;
      this.evaluation = this.pharmaEventRecord.evaluation;
      this.eventType = this.pharmaEventRecord.eventType;
      this.speakers = this.pharmaEventRecord.speakers;
      this.objectives = this.pharmaEventRecord.objectives;
      this.stage = this.pharmaEventRecord.stage;
      this.location = this.pharmaEventRecord.location;
      this.agenda = this.pharmaEventRecord.agenda;
      this.selectedProductIds.push(this.pharmaEventRecord.productPrio1SfId);
      this.selectedProductIds.push(this.pharmaEventRecord.productPrio2SfId);
      this.selectedProductIds.push(this.pharmaEventRecord.productPrio3SfId);
      this.selectedProductIds.push(this.pharmaEventRecord.productPrio4SfId);
      this.eventName = this.pharmaEventRecord.eventName;

      this.productsCollection.fetchForProductIds(this.selectedProductIds)
        .then((product) => {
          this.selectedProductStringList = product
            .map((products) => products.getName())
            .join(', ');
        })
    });
    console.log("_soupEntryId",this.navParams.data['row']._soupEntryId);
    const fieldValues = {};
    fieldValues[PEAttendeeScheme.fields.pharmaEventSfId.sfdc] = this.navParams.data['row']._soupEntryId;
    this.peAttendeesCollection.fetchAllWhere(fieldValues)
    .then(attendeeData=>{
      console.log("attendeeData direct",attendeeData);
    })
    this.peAttendeesCollection.fetchByPEId(this.navParams.data['row'].id,this.navParams.data['row']._soupEntryId).then(attendeeRecord => {
      console.log("attendeeRecord",attendeeRecord);
      const attendeeIdsData = [];
      attendeeRecord.map(attendee=>{
        attendeeIdsData.push(attendee.attendeeSfId)
      });
      this.setAttendees(attendeeIdsData);
      
     // this.selectedAttendeesData = attendeeRecord.pharmaEventSfId
    })
  }

  loadReference() {
    return this.service.loadReference()
      .then(results => {
        console.log(results);
        this.attendessData = results.records;
      })
  }

  loadReferenceCollection() {

    return this.service.loadReference()
      .then(results => {
        console.log('references',results);
        this.speakersData = results.records;
        
        
      })
  }

  loadAllUsers() {
    return this.service.loadReference()
      .then(results => {
        console.log('Users',results);
        this.attendessData = results.records;
      })
  }
  selectedProducts(selectedItems:any)
  {  
    console.log("selected products",selectedItems);
    this.selectedProducts = selectedItems;
  }

  savePharmaEvent(){
  
    let pharmaEvent = new PharmaEvent({
      ownerSfid : this.activeUser.id,
      createdOffline : true,
      ownerFirstName:  this.activeUser.firstName,
      ownerLastName:  this.activeUser.lastName,
      eventType : this.eventType,
      stage : this.stage,
      startDate : this.startDate,
      endDate : this.endDate,
      eventName : this.eventName,
      location : this.location,
      status : PharmaEventScheme.APPROVAL_STATUS_DRAFT,
      speakers : this.speakers,
      objectives : this.objectives,
      agenda : this.agenda,
      evaluation : this.evaluation,
      productPrio1SfId : undefined,
      productPrio2SfId : undefined,
      productPrio3SfId : undefined,
      productPrio4SfId : undefined
    });

    console.log("this.selectedProductsData.length",this.selectedProductIds)
    for(var i=1;i<= this.selectedProductIds.length; i++) {
      i==1?pharmaEvent.productPrio1SfId = this.selectedProductIds[i] : undefined; 
      i==2?pharmaEvent.productPrio2SfId = this.selectedProductIds[i] : undefined; 
      i==3?pharmaEvent.productPrio3SfId = this.selectedProductIds[i] : undefined; 
      i==4?pharmaEvent.productPrio4SfId = this.selectedProductIds[i] : undefined; 
    }

    console.log("pharmaEvent",pharmaEvent);
    this.pharmaEventsCollection.updateEntity(pharmaEvent)
    .then(createdData=>{
      console.log("createdData Pharma Event",createdData);
      let success = (data) => {
        console.log("Data Upserted successfully and created data is",pharmaEvent.id, data);
        //this.navParams.get("parentPage").reloadTOT();

        for(var i=1; i<= this.selectedAttendeesIds.length; i++) {
          let saveAttendee = new PEAttendee({
            pharmaEventSfId : pharmaEvent.id,
            attendeeSfId : this.selectedAttendeesIds[i]
          });

          this.peAttendeesCollection.updateEntity(saveAttendee)
          .then(createdAttendee=>{
            console.log("createdAttendee",createdAttendee);
          })
        }

        this.navCtrl.pop();
        const toast = this.toastCtrl.create({
          message: "Update pharma event Created Successfully!",
          showCloseButton:true,
          cssClass:"toastClass",
          position:'middle',
          duration:3000,
        });
        toast.present();
        
        
      }
      success(createdData);
    }) 
}

private gotoAttendeePage() {
  this.navCtrl.push(AttendeesListSelectPage, {
    ids: this.selectedAttendeesIds,
    callback: this.setAttendees.bind(this)
  })
}
private gotoProductPage() {
  this.navCtrl.push(ProductListSelectPage, {
    ids: this.selectedProductIds,
    callback: this.setProduct.bind(this)
  })
}

private setAttendees(attendeesIds) {
  this.selectedAttendeesIds = attendeesIds;
  this.selectedAttendeesStringList = this.selectedAttendeesIds.join(', ');
  
  this.setAttandeesListToView();
}

private setProduct(productIds) {
  this.selectedProductIds = productIds;
  this.selectedProductStringList = this.selectedProductIds.join(', ');
  
  this.setProductListToView();
}

private setAttandeesListToView() {
  if(this.selectedAttendeesIds.length) {
    return this.loader.run(() => {
      return this.contactsCollection.fetchForContactIds(this.selectedAttendeesIds)
        .then((attendees) => {
          this.selectedAttendeesStringList = attendees
            .map((attendee) => attendee.fullName())
            .join(', ');
        })
    })
  }

  this.selectedAttendeesStringList = '';
}

setProductListToView() {
  if(this.selectedProductIds.length) {
    return this.loader.run(() => {
      return this.productsCollection.fetchForProductIds(this.selectedProductIds)
        .then((product) => {
          this.selectedProductStringList = product
            .map((products) => products.getName())
            .join(', ');
        })
    })
  }

  this.selectedProductStringList = '';
}

  validateInputData()
{
  console.log("message",this.message);
  console.log("this.validateInput",this.validateInput());
  if(this.validateInput())
  {
    this.savePharmaEvent();
  }
  else
  {
    const toast = this.toastCtrl.create({
      message: this.message,
      showCloseButton:true,
      cssClass:"toastClass",
      duration:3000,
      position:'middle'
    });
    toast.present();
  }
  
}


validateInput()
  {
    var isValid = true;
    this.message = "";
    console.log("this.startDate",this.pharameventvalidation);
    if((this.startDate=='' || this.startDate==null))
    {
      isValid = false;
      this.message+=" \n Start Date Mandatory";
    }
    if((this.endDate=='' || this.endDate==null))
    {
      isValid = false;
      this.message+=" \n End Date Mandatory";
    }
    if(this.pharameventvalidation.EventName && (this.eventName=='' || this.eventName==null) )
    {
      isValid = false;
      this.message+=" \n Event Name Mandatory ";
    }
    if(moment(this.startDate) < this.viewEnteredDateTime)
    {
      isValid = false;
      this.message+=" \n Start date Value cannot be less than the current Date ";
    }
    if(moment(this.endDate) <= this.viewEnteredDateTime)
    {
      isValid = false;
      this.message+=" \n End date Value cannot be less than the current Date ";
    }
    if(this.pharameventvalidation.EventName && (this.evaluation=='' || this.evaluation==null ))
    {
      isValid = false;
      this.message+=" \n Evaluation Mandatory";
    }
    if(this.pharameventvalidation.TypeOfEvent && (this.eventType=='' || this.eventType==null ))
    {
      isValid = false;
      this.message+=" \n TypeOfEvent Mandatory";
    }
    if(this.pharameventvalidation.Products && (this.selectedProductsData && (this.selectedProductsData.length<=0)))
    {
      isValid = false;
      this.message+=" \n Products Mandatory";
    }
    if(this.selectedProductIds.length > 4)
    {
      isValid = false;
      this.message+=" \n Can not choose more than 4 products";
    }
    if(this.pharameventvalidation.Speakers && (this.speakers=='' || this.speakers==null))
    {
      isValid = false;
      this.message+=" \n Speakers Mandatory";
    }
    if(this.pharameventvalidation.Objectives && (this.objectives=='' || this.objectives==null))
    {
      isValid = false;
      this.message+=" \n Objectives Mandatory";
    }
    return isValid;  
  }
  
  selectedAttendeesList(id:string,soupEntryId:string)
  {  
    // this.service.getPharmaBasedAttendees(id,soupEntryId).then(res=>{
    //   console.log("res",res);
    //   if(res.records.length>0)
    //   {
    //      res.records.forEach(val=>{
    //       console.log("val",val);
    //       this.selectedAttendeesData.push(val[0]);
    //       this.selectedAttendeesBkup.push(val[0].Attendee__c);
    //     })

    //   }
    //   console.log("this.selectedAttendeesData",this.selectedAttendeesData);
    // });
  }

submitApproval(){
  
  let confirm = this.alertCtrl.create({
    title: `Please confirm..?`,
    message: `Do you want Submit it for approval?`,
    buttons: [
      {
        text: 'No',
        handler: () => {
          confirm.dismiss();
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.PharmaEvent.Status__c = "Submitted Offline";
          let success = (items) => {console.log("items",items);
        }
        let failure = (error) => console.error(`Soup Upsert Error: ${error}`);
        console.log("this.PharmaEvent",this.PharmaEvent);
        this.service.smartStore().upsertSoupEntries(Constants.PharmaEvent, [this.PharmaEvent], success, failure)       
        }
      }
    ]
  });

    confirm.present();
  }

   recallApproval(){
  
    let confirm = this.alertCtrl.create({
      title: `Please confirm..?`,
      message: `Do you want recall it from approval?`,
      buttons: [
        {
          text: 'No',
          handler: () => {
            confirm.dismiss();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.PharmaEvent.Status__c = "Draft";
            let success = (items) => {console.log("items",items);
          }
          let failure = (error) => console.error(`Soup Upsert Error: ${error}`);
          console.log("this.PharmaEvent",this.PharmaEvent);
          this.service.smartStore().upsertSoupEntries(Constants.PharmaEvent, [this.PharmaEvent], success, failure)     
          }
        }
      ]
    });
  
    confirm.present();
     }

     deletePharmaEventdetails(id: string,_soupEntryId:string) {
      let confirmDelete = this.alertCtrl.create({
        title: `Delete PharmaEvent?`,
        message: `Are you sure you want to delete`,
        buttons: [
          {
            text: 'No',
            handler: () => {
  
              confirmDelete.dismiss();
            }
          },
          {
            text: 'Yes',
            handler: () => {
              console.log("this.pharmaEventRecord",this.pharmaEventRecord);
              this.pharmaEventsCollection.removeEntity(this.pharmaEventRecord)
                .then((result) => {
                   this.navCtrl.setRoot(ActivitiesPage);           
                });
            } 
          }
        ]
      });
  
      confirmDelete.present();
    }
}
