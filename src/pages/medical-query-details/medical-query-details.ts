import { Component, ViewChild, TemplateRef } from '@angular/core';
import { IonicPage, NavController,  NavParams ,AlertController , ToastController} from 'ionic-angular';
import { MedicalQueryPage } from '../medical-query/medical-query';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { MQPickListDatasource } from '../../services/db/picklist-managers/datasource/MQPickListDatasource';
import { UsersCollection } from '../../collections/UsersCollection';
import { MedicalQuery } from "../../models/MedicalQuery";
import { MedicalQueryScheme } from "../../models/scheme/MedicalQueryScheme";
import { Utils} from '../../utils/Utils';
import { MedicalQueriesCollection } from "../../collections/MedicalQueriesCollection/MedicalQueriesCollection";
import { MedicalCommentsCollection } from '../../collections/MedicalCommentsCollection';
import { ContactdetailsPage } from '../contactdetails/contactdetails';
import { OrganizationdetailsPage } from '../organizationdetails/organizationdetails';
import { LocalizationManager } from "../../services/common/localizations/LocalizationManager";
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { MedicalComment } from "../../models/MedicalComment";


/**
* Generated class for the MedicalQueryDetailsPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-medical-query-details',
  templateUrl: 'medical-query-details.html',
})
export class MedicalQueryDetailsPage {
  @ViewChild('addComment') modalContent: TemplateRef<any>;
  modalData: {
    action: string;
    event: string;
  };
  modalTitle:string = "Comment";
  targetDetailsName : any
  loggedInUserDetails: any;
  commentText : any = "";
  message = "";
  medicalQueryRecord:any;
  status = MedicalQueryScheme.APPROVAL_STATUS_DRAFT;
  config:any;
  querySubmittedToRep:string = MedicalQueryScheme.APPROVAL_STATUS_SUBMITTED_TO_REP;
  public configData:any;
  public customers:any;
  today: any;
  pickListValues: any;
  medicalCommentData:any = [];
  medicalQuery:MedicalQuery;
  constructor(public navCtrl: NavController, 
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public mqPickListDatasource:MQPickListDatasource,
    public usersCollection:UsersCollection,
    private medicalQueriesCollection : MedicalQueriesCollection,
    private localizationManager: LocalizationManager,
    private medicalCommentsCollection:MedicalCommentsCollection,
    private modal: NgbModal) {
    // this.row = this.navParams.data['row']
    this.today = Date.now();
    this.medicalQueryRecord = this.navParams.data['row'];
    SforceDataContext.getActiveUser()
    .then(activeUser=>{
      console.log("activeUser",activeUser);
      this.loggedInUserDetails = activeUser;
    })
    if(this.navParams.data['row']) {
      this.medicalQueriesCollection.fetchEntityById(this.navParams.data['row'].id).then(mqRes=>{
        console.log("MedicalQueryRecord",mqRes);
        this.medicalQueryRecord = mqRes;
      })
    }
    this.medicalCommentsCollection.getMqByIds(this.navParams.data['row'].id,this.navParams.data['row']._soupEntryId)
    .then(medicalCommentData=>{
      console.log("medicalCommentData",medicalCommentData);
      this.medicalCommentData = medicalCommentData;

    })
  }

  ionViewDidLoad():void {}

  saveMedQuery(){
    console.log("media query");
    this.localizationManager.getSeveralLocales([
      'common.MedicalQuery.ConfirmationPopup.SubmitItem.Caption',
      'common.MedicalQuery.ConfirmationPopup.SubmitItem.Question'])
    .then(localization=>{
      let confirmUpdate = this.alertCtrl.create({
      title: `${localization['common.MedicalQuery.ConfirmationPopup.SubmitItem.Caption']}`,
      message: `${localization['common.MedicalQuery.ConfirmationPopup.SubmitItem.Question']}`,
      buttons: [
      {
        text: 'No',
        handler: () => {

          confirmUpdate.dismiss();
        }
      },
      {
        text: 'Yes',
        handler: () => {
          console.log("update media query");
          this.medicalQueryRecord.status = MedicalQueryScheme.APPROVAL_STATUS_SUBMITTED_TO_MEDICAL;
          this.medicalQueriesCollection.updateEntity(this.medicalQueryRecord)
          .then(res=>{
            console.log("res",res);
            this.navCtrl.pop();
            const toast = this.toastCtrl.create({
              message: "Medical Query Updated Successfully!",
              showCloseButton:true,
              cssClass:"toastClass",
              position:'middle',
              duration:3000,
            });
            toast.present();
          })
        } 
      }
      ]
    });
    confirmUpdate.present();
    })
  }

  public reloadMedicalCommentData()
  {
    this.medicalCommentsCollection.getMqByIds(this.navParams.data['row'].id,this.navParams.data['row']._soupEntryId)
    .then(medicalCommentData=>{
      console.log("medicalCommentData",medicalCommentData);
      this.medicalCommentData = medicalCommentData;

    })
  }

  deleteMedicalquerydetails(id: string,_soupEntryId:string) {

    let confirmDelete = this.alertCtrl.create({
      title: `Delete Medicalquery?`,
      message: `Are you sure you want to delete this item?`,
      buttons: [
      {
        text: 'No',
        handler: () => {

          confirmDelete.dismiss();
        }
      },
      {
        text: 'Yes',
        handler: () => {

          this.medicalQueriesCollection.removeEntity(this.medicalQueryRecord)
          .then((result) => {

            this.navCtrl.setRoot(MedicalQueryPage);

          });
        } 
      }
      ]
    });

    confirmDelete.present();
  }
  public submitMedicalQuery(id: string,_soupEntryId:string){
    //this.validateInputData();
    this.saveMedQuery();
  }

  public onTapDisAgreeHandler(action, event)
  {
    console.log("action",action);
    console.log("event",event);
    //this.modalData = { "event", "action" };
   // this.modalTitle = 'comment';
    this.targetDetailsName = "";
    let ngbModalOptions: NgbModalOptions = {
      backdrop : 'static',
      keyboard : false,
      size : 'lg',
      centered:true
    };
    this.modal.open(this.modalContent, ngbModalOptions);
  }

  public submitComment() {
    console.log("Submit btn");

    if(this.commentText == "" || this.commentText == null) {
      this.message+="\n Comment Mandatory";

      this.message = this.message.startsWith('\n')?this.message.substring(2):this.message;
      const toast = this.toastCtrl.create({
         message: this.message,
         showCloseButton:true,
         cssClass:"toastClass",
         duration:7000,
       });
       toast.present();
    } else {
      let medicalComment = new MedicalComment({
        commentDescription : this.commentText,
        mqId : this.navParams.data['row'].id,
        createdBy:this.loggedInUserDetails.id
      });
      this.medicalCommentsCollection.createEntity(medicalComment).then(mqComment=>{
        console.log("comment created successfully.");
        this.message = this.message.startsWith('\n')?this.message.substring(2):this.message;
        const toast = this.toastCtrl.create({
          message: "Comment added successfully.",
          showCloseButton:true,
          cssClass:"toastClass",
          duration:7000,
        });
        toast.present();
        this.reloadMedicalCommentData();
        this.modal.dismissAll();
      })
    }


  }

  closePopup() {
    this.modal.dismissAll();
  }

  public onTapAgreeHandler()
  {
    this.queryStatusUpdateToClose();
  }

  private queryStatusUpdateToClose()
  {
    this.medicalQueryRecord.status = MedicalQueryScheme.APPROVAL_STATUS_CLOSED;
    this.medicalQueriesCollection.updateEntity(this.medicalQueryRecord)
    .then(res=>{
      console.log("res",res);
      this.navCtrl.pop();
    })
  }

  public onTapCustomerClickHandler()
  {
    this.navCtrl.push(ContactdetailsPage,{contactSfId:this.medicalQueryRecord.customer});
  }

  public onTapOrganizationClickHandler()
  {
    this.navCtrl.push(OrganizationdetailsPage,{organizationSfId:this.medicalQueryRecord.organization});
  }

}
