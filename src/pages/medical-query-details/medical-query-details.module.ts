import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedicalQueryDetailsPage } from './medical-query-details';

@NgModule({
  declarations: [
    MedicalQueryDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MedicalQueryDetailsPage),
  ],
})
export class MedicalQueryDetailsPageModule {}
