import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesplanningPage } from './salesplanning';

@NgModule({
  declarations: [
    SalesplanningPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesplanningPage),
  ],
})
export class SalesplanningPageModule {}
