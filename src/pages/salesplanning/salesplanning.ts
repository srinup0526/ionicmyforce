import {Component, ElementRef, ViewChild} from '@angular/core';
import {
  IonicPage, NavController, NavParams, LoadingController, ModalController, PopoverController ,AlertOptions,
  Select, ToastController, NavOptions
} from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { SalesPlanningCollection } from '../../collections/SalesPlanningCollection';
import { SalesPlanningScheme } from '../../models/scheme/SalesPlanningScheme'
import { FilterPanelComponent } from "../../components/filter/filter-panel/filter-panel";
import { Query } from "../../services/common/Query";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { from } from 'rxjs';
import { PopoverComponent } from '../../components/popover/popover';
import { SalesPlanningListDetailsPage } from '../../pages/sales-planning-list-details/sales-planning-list-details'
import { SalesplanpopoverComponent } from '../../components/salesplanpopover/salesplanpopover';


@Component({
  selector: 'page-salesplanning',
  templateUrl: 'salesplanning.html',
})
export class SalesplanningPage {
  public isSearchbarOpened = false;
  public salesPlanCount:any = 0;
  
  public tableOptions: LazyTableOption;
  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: SalesPlanningCollection;
  public filterOptions: AlertOptions;
  public settings: SettingsImpl;

  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('filterType') filterTypeComponent: Select;
  @ViewChild('mainContent') mainContent: ElementRef;
  items: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
    public salesPlanningCollection: SalesPlanningCollection,
    public popoverCtrl: PopoverController,
    private toastCtrl: ToastController) {

//Table hearders

this.settings = Settings.getInstance();
    
    const headerItems = [
        {
          title: 'common.SalesPlanning.SalesPlanningName',
          fields: [SalesPlanningScheme.fields.name],
          isSortable: true,
          isAsc: true,
          isActive: false,
          picklistValues: {},
          
        },
        {
          title: 'common.SalesPlanning.Month',
          fields: [SalesPlanningScheme.fields.month],
          isSortable: true,
          isAsc: true,
          isActive: false,
          picklistValues: {}
        },
        {
          title: 'common.SalesPlanning.Year',
          fields: [SalesPlanningScheme.fields.year],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.SalesPlanning.OwnerName',
          fields: [SalesPlanningScheme.fields.createdBy],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.SalesPlanning.Status',
          fields: [SalesPlanningScheme.fields.status],
          isSortable: true,
          isAsc: true,
          isActive: false
        }
        ];
    
    this.tableOptions = {
      headerItems: headerItems,
      rowHandler: (event, reference) => {
        console.log("event sales ",event);
        console.log("event data sales",reference);
        this.gotoSalesPlanDetailsPage(reference);
      },
      batchSize: 100
    };
    
    this.searchString = '';

    this.collection = this.salesPlanningCollection;
    
     this.filterOptions = {
       cssClass: 'popup alert list without-header',
       buttons: []
     };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SalesplanningPage');
  }

  // Adding add button task related information and methods
  public onTapAddBtnHandler(): void {
    this.presentPopover();
  }
  private presentPopover(): Promise<any> {
    let popover = this.popoverCtrl.create(SalesplanpopoverComponent,{"parentPage": this});
    let options: NavOptions = {};
  
    popover.onDidDismiss(this.onDismissCreatePopoverHandler.bind(this));
    
    return popover.present(options);
  }
  public onDismissCreatePopoverHandler(): void {
    this.reloadTable();
  }
  private reloadTable(): void {
    this.lazyTable.reloadTable();
  }

  //end task 
public onChangeQueryHandler(query: Query): void {
  this.setSalesPlanningCount();
}

private gotoSalesPlanDetailsPage(reference:any)
{
  this.navCtrl.push(SalesPlanningListDetailsPage, { "row": reference });
}

public onSwipeHandler(event){
  switch (event.offsetDirection) {
    case 2: this.filterPanel.hidePanel(); break;
    case 4: this.filterPanel.showPanel(); break;
  }
}

public onFilterChangeHandler(filterData): void {
  this.lazyTable.reloadTableByFilterData(filterData);
}

public onSearchHandler(searchBar: any) {
  this.searchString = searchBar.value || '';
  this.lazyTable.setSearchString(this.searchString);
  this.lazyTable.reloadTable();
}

public onClearHandler(ev: any): void {
  ev.target.value = '';
  this.searchString = ev.target.value;

  this.lazyTable.setSearchString(this.searchString);
  this.lazyTable.reloadTable();
}

public ionViewDidEnter(): void {}

public ionViewWillEnter(): void {}

public ngAfterContentInit() {
  this.lazyTable.setTableOptions(this.tableOptions);
  this.lazyTable.setSearchString(this.searchString);
  this.lazyTable.setCollection(this.collection);

  this.lazyTable.reloadTable();
}

private setSalesPlanningCount(): number {
  const countQuery = this.collection.getCountQuery();
  const query = this.lazyTable.getTableQueryWithConditions(countQuery);

  return this.collection.runCountQuery(query.query)
    .then((count) => {
      return this.salesPlanCount = count || 0;
    })
}




}
