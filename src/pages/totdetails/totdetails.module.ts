import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TotdetailsPage } from './totdetails';

@NgModule({
  declarations: [
    TotdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TotdetailsPage),
  ],
})
export class TotdetailsPageModule {}
