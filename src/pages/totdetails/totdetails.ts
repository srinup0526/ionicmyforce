import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { Storage } from '@ionic/storage';
import { TotPage } from '../tot/tot';
import { AddtotPage } from '../addtot/addtot';
import { TotsCollection} from '../../collections/tots-collection/tots-collection';
//import { TotPage } from '../tot/tot';
//import { Identifiers } from '@angular/compiler';
/**
 * Generated class for the TotdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-totdetails',
  templateUrl: 'totdetails.html',
})
export class TotdetailsPage {
  TOTid:any={};
  TOT:any;
  loggedInUserDetails:any;
  TOTRecord:any;
  TOTDesc:any;
  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              public storage: Storage,
              public navParams: NavParams,
              private service: SmartstoreServiceProvider,
              private totCollection:TotsCollection) {
    this.storage.get("currentUserDetails").then(data => { this.loggedInUserDetails = data; console.log("UserData",this.loggedInUserDetails); });
   
  }

  ionViewDidLoad() {
    
    // this.getTOTId(this.navParams.data['id']);
    let row= this.navParams.data['row'];
    console.log('ionViewDidLoad TotdetailsPage', row);
    
    this.totCollection.fetchEntityById(this.navParams.data['row'].id).then(totRes=>{
      console.log("totResponse",totRes);
      this.TOTRecord = totRes;
      this.TOTDesc = this.TOTRecord.description;
    })

    // this.getTOTIdandSoupEntryId(row.id, row._soupEntryId);
    // this.loadTot();
  }
  // getTOTId(id: string) {
  //   this.service.getTotId(id)
  //     .then(results => {

  //       this.TOTid = results.records[0];
  //       console.log("totdetails",this.TOTid);
  //     })
     
  // }
  loadTot() {

    return this.service.loadTot()
      .then(results => {
        console.log('TOT',results);
        this.TOT = results.records;
     
       })
      }
      
  getTOTIdandSoupEntryId( id: string,_soupEntryId:string){
    this.service.getTOTIdandSoupEntryId(id,_soupEntryId)
    .then(results => {
      console.log("recordsdetail",results,results.records[0] );
      this.TOTid = results.records[0][0];
      console.log("TOTiddetails",this.TOTid);
    })
  }
  deleteTOTdetails(id: string,_soupEntryId:string) {
    // let totDetailsRow = this.TOT;
    let confirmDelete = this.alertCtrl.create({
      title: `Delete TOT?`,
      message: `Are you sure you want to delete`,
      buttons: [
        {
          text: 'No',
          handler: () => {

            confirmDelete.dismiss();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log("this.TOTRecord",this.TOTRecord);
            this.totCollection.removeEntity(this.TOTRecord)
              .then((result) => {

                 this.navCtrl.pop();
           
              });
          } 
        }
      ]
    });

    confirmDelete.present();
  }

  editTOT(){
    this.navCtrl.push(AddtotPage,{'row':this.TOTRecord});
  }
}
