import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatchdetailsPage } from './patchdetails';

@NgModule({
  declarations: [
    PatchdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PatchdetailsPage),
  ],
})
export class PatchdetailsPageModule {}
