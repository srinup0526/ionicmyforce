import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { PatchCustomerCollection} from '../../collections/PatchCustomerCollection';
import { PatchCustomer } from '../../models/PatchCustomer'
import { PatchCustomerReferenceCollection } from '../../collections/PatchCustomerReferenceCollection';



@IonicPage()
@Component({
  selector: 'page-patchdetails',
  templateUrl: 'patchdetails.html',
})
export class PatchdetailsPage {
  row:any;
  references:any;
  patchCustomer:PatchCustomer;
  constructor(
              public navCtrl: NavController,
              public navParams: NavParams,
              public service:SmartstoreServiceProvider,
              public patchCustomerCollection:PatchCustomerCollection,
              public patchCustomerReferenceCollection:PatchCustomerReferenceCollection) {
    this.patchCustomer = this.navParams.data['row'];
    console.log(" this.patchCustomer",this.patchCustomer);
    this.patchCustomerReferenceCollection.fetchByCustomerId(this.patchCustomer.id).then(res=> {
      console.log("patchCustomerReferenceCollection",res);
      return this.references = res
    });
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatchdetailsPage');
  //  this.getPatchIdandSoupEntryId(this.row.Id, this.row._soupEntryId);
  }
  getPatchIdandSoupEntryId( id: string,_soupEntryId:string){
    this.patchCustomerCollection.fetchEntityById(id)
    .then(results => {
      console.log("results",results);
    })
  }
  
}
