import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours,
  getMonth,
  getYear,
  addMinutes
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  DAYS_OF_WEEK
} from 'angular-calendar';
import { Storage } from '@ionic/storage'
import { IonicPage, NavController, Select, AlertController, LoadingController } from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.service';



interface AppointmentEvents {
  Customer?:any;
  Id?: string;
  Contact1__c?:string;
  Organisation__c?:string;
  MTP__c?:string;
  Duration__c?:string;
  Type__c:string;
  User__c?:string;
  Brick__c?:string;
  CallDate__c?:Date;
  _soupEntryId:number;
  Call_Report_Status__c?:string;
  Contact1__r?:{FirstName:string;
  LastName?:string;
  Name?:string;
  Priority__c?:string;
  Solvay_Specialty__c?:string;
  Specialty__c?:string;
  };
  Organisation__r?:{BillingAddress:string;
  Name?:string;
  };
  User__r?:{Name:string;};
  Date_Time_Actual__c?:Date;
  Date_Time_Planned__c:Date;
  Date_time_Visit_End__c?:Date;
  Coaching_Visit__c?:string;
  Joint_Visit__c?:string;
  Call_Objective__c?:string;
  Tot?:{
  OwnerId?:string;
  Id?:string;
  Owner?:any;
  Start_Date__c?:Date;
  Type_First_Quarter__c?:string;
  Type_Fourth_Quarter__c?:string;
  Type_Second_Quarter__c? :string;
  Type_Third_Quarter__c?:string;
  End_Date__c?:Date;
  All_Day__c?:boolean;
  Type__c?:string;
  },
  PE?:{

  }
  
}


const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'page-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'calendar.html'
})
//styleUrls: ['styles.css'],
export class CalendarPage {

  
  currentUserDetails:any;
  excludeArray:any=[];
  WeekArray:string[] = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
  WeekOffArray:any = [];
  isAddEventOpened:boolean = false;
  filterOptions:any;
  contacts:any;
  targetContacts:any;
  nonTargetContacts:any;
  eventsBkup:any = [];
  targetDetailsName:string = "Appointment"
  externalEvents: Array<CalendarEvent<{ appointmentEvents: AppointmentEvents }>> = [];
  public MTPCollection: any = [];


  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  @ViewChild('eventDetails') eventDetails: TemplateRef<any>;
  @ViewChild('mySelect') mySelect: Select;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [
    {
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'A 3 day event',
      color: colors.red,
      actions: this.actions,
      allDay: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    },
    {
      start: startOfDay(new Date()),
      title: 'An event with no end date',
      color: colors.yellow,
      actions: this.actions
    },
    {
      start: subDays(endOfMonth(new Date()), 3),
      end: addDays(endOfMonth(new Date()), 3),
      title: 'A long event that spans 2 months',
      color: colors.blue,
      allDay: true
    },
    {
      start: addHours(startOfDay(new Date()), 2),
      end: new Date(),
      title: 'A draggable and resizable event',
      color: colors.yellow,
      actions: this.actions,
      resizable: {
        beforeStart: true,
        afterEnd: true
      },
      draggable: true
    }
  ];

  activeDayIsOpen: boolean = true;

  excludeDays: number[] = [];
  weekStartsOn = DAYS_OF_WEEK.SUNDAY;

  constructor(public loadingCtrl:LoadingController,public alertCtrl: AlertController, public navCtrl: NavController,private modal: NgbModal, public storage:Storage, public service: SmartstoreServiceProvider, private confirmationDialogService: ConfirmationDialogService,) {

  	this.filterOptions = [{key:"Appointment",value:"Appointments"},{key:"1:1",value:"Call Reports"},{key:"PE",value:"Pharma Events"},{key:"TOT",value:"Time Off Territory"}];
    this.contacts = [{key:"targetContacts",value:"Target Contacts"},{key:"nonTargetContacts",value:"Non Target Contacts"}];
    this.service.getAllContactReferences("Yes")
    .then(result =>{
      this.loadExternalEvents(result.records);
    })
    .catch(err => { console.log("err",err)})
    this.service.getAllContactReferences("No")
    .then(result =>{
      this.loadExternalNonTargetEvents(result.records);
    })
    .catch(err => { console.log("err at nontarget",err)})
    .then (data => {
      this.filterByContacts("targetContacts");
    })
    this.loadMtps();

  }

  ionViewDidLoad(){
  	console.log("loaded after dom ready");
  	this.storage.get("currentUserDetails").then(currentUserDetails=>{

  		this.currentUserDetails = currentUserDetails;
  		console.log("excludeDays",this.excludeDays);
  		this.excludeDays.push(this.WeekArray.indexOf(this.currentUserDetails.Day_Off_1__c))
		this.excludeDays.push(this.WeekArray.indexOf(this.currentUserDetails.Day_Off_2__c))
		console.log("excludeDays",this.excludeDays);
		this.refresh.next();
		console.log("excludeDays",this.WeekArray.indexOf(this.currentUserDetails.Day_Off_1__c))
		console.log("excludeDays",this.WeekArray.indexOf(this.currentUserDetails.Day_Off_2__c))
  	})
  	
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    // if (isSameMonth(date, this.viewDate)) {
    //   this.viewDate = date;
    //   if (
    //     (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
    //     events.length === 0
    //   ) {
    //     this.activeDayIsOpen = false;
    //   } else {
    //     this.activeDayIsOpen = true;
    //   }
    // }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    console.log("action",action);
    console.log("event",event);
    this.modalData = { event, action };
    this.targetDetailsName = ""
    let ngbModalOptions: NgbModalOptions = {
      backdrop : 'static',
      keyboard : false,
      size : 'lg',
      centered:true
    };
    this.modal.open(this.modalContent, ngbModalOptions);
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      }
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  addEventClicked(){
  	this.isAddEventOpened = true;
  	this.mySelect.open();
  }

  cloneCliked(){


  	
  }


  gotoEventCreationPage(eventType:any){
  	console.log("eventType",eventType);
  	switch (eventType) {
        case 'appointment':
            alert("Selected Case Number is Appointment");
            break;
        case 'tot':
            alert("Selected Case Number is TOT");
            break;
        case 'pe':
            alert("Selected Case Number is pe");
            break;
        case 'TOT':
            alert("Selected Case Number is TOT");
            break;
        default:

        }

  }


  loadExternalEvents(records:any)
  {
  	let eventData = [];
  	let userData = this.currentUserDetails;
  	if(records.length>0)
  	{ 
  		records.forEach(function(val,i){

  			let dataEvent = {
  				Id: "",
  				Contact1__c:val[0].Customer__c,
  				Organisation__c:val[0].Organisation__c,
  				MTP__c:"",
  				Duration__c:"30",
  				Type__c:"Appointment",
  				User__c:userData.Id,
  				Brick__c:"",
  				CallDate__c:new Date(),
  				Call_Report_Status__c:"",
  				Contact1__r:val[0].Customer__r,
  				Organisation__r:val[0].Organisation__r,
  				User__r:userData,
  				Date_Time_Actual__c:new Date(),
  				Date_Time_Planned__c:new Date(),
  				Date_time_Visit_End__c:new Date(),
  				Coaching_Visit__c:"",
  				Customer:val[0].Customer__c,
  				Joint_Visit__c:"",
  				Call_Objective__c:"",
  				Tot:{
  					OwnerId:"",
  					Id:"",
  					Owner:"",
  					Start_Date__c:null,
  					Type_First_Quarter__c:"",
  					Type_Fourth_Quarter__c:"",
  					Type_Second_Quarter__c :"",
  					Type_Third_Quarter__c:"",
  					End_Date__c:null,
  					All_Day__c:false
  				},
  				PE:{
  					OwnerId:"",
  					Id:"",
  					Owner:"",
  					Start_Date__c:null,
  					Type_First_Quarter__c:"",
  					Type_Fourth_Quarter__c:"",
  					Type_Second_Quarter__c :"",
  					Type_Third_Quarter__c:"",
  					End_Date__c:null,
  					All_Day__c:false
  				},
  			}
  			let appointmentEvents =  dataEvent;
  			let info = {
  				title: val[0].Customer__r.Name ,
  				start: new Date(),
  				end: new Date(),
  				draggable:true,
  				meta: {
  					appointmentEvents
  				}
  			}
  			eventData.push(info);

  			console.log("targetContacts",eventData);
  		});
  		this.targetContacts = eventData;


  	}
  	else{
  		this.targetContacts = [];
  	}
  	console.log("targetContacts",eventData);
  	this.externalEvents = [...eventData];
  }


loadExternalNonTargetEvents(records:any)
{
    let evntInfo = [];
    if(records.length>0)
    { 
      let userData = this.currentUserDetails;
      records.forEach(function(val,i){

        let dataEvent = {
          Id: "",
          Contact1__c:val[0].Customer__c,
          Organisation__c:val[0].Organisation__c,
          MTP__c:"",
          Duration__c:"30",
          Type__c:"Appointment",
          User__c:userData.Id,
          Brick__c:"",
          CallDate__c:new Date(),
          Call_Report_Status__c:"",
          Contact1__r:val[0].Customer__r,
          Organisation__r:val[0].Organisation__r,
          User__r:userData,
          Date_Time_Actual__c:new Date(),
          Date_Time_Planned__c:new Date(),
          Date_time_Visit_End__c:new Date(),
          Coaching_Visit__c:"",
          Customer:val[0].Customer__c,
          Joint_Visit__c:"",
          Call_Objective__c:"",
          Tot:{
              OwnerId:"",
              Id:"",
              Owner:"",
              Start_Date__c:null,
              Type_First_Quarter__c:"",
              Type_Fourth_Quarter__c:"",
              Type_Second_Quarter__c :"",
              Type_Third_Quarter__c:"",
              End_Date__c:null,
              All_Day__c:false,
              Type__c:"",
            },
            PE:{
              OwnerId:"",
              Id:"",
              Owner:"",
              Start_Date__c:null,
              Type_First_Quarter__c:"",
              Type_Fourth_Quarter__c:"",
              Type_Second_Quarter__c :"",
              Type_Third_Quarter__c:"",
              End_Date__c:null,
              All_Day__c:false
            },
        }
        let appointmentEvents = dataEvent;
        let info = {
          title: val[0].Customer__r.Name,
          start: new Date(),
          end: new Date(),
          draggable:true,
          meta: {
            appointmentEvents
          }
        }
        evntInfo.push(info);
      });
      console.log("nonTargetContacts",evntInfo);
      this.nonTargetContacts = [...evntInfo];
    }
    else
    {
      this.nonTargetContacts = [];
    }
    console.log("this.externalEvents",this.externalEvents);
  }


  filterByContacts(modelData:string){
  	if(modelData!='targetContacts')
  	{
  		this.externalEvents= [...this.nonTargetContacts];
  	}
  	else
  	{
  		this.externalEvents= [...this.targetContacts];
  	}
  }

  loadMtps()
  {
    this.service.loadMtp()
      .then(results => {
        console.log('MTP',results);
        results.records.forEach(data=>{
          let monthWithYear = getMonth(data.Start_Date__c)+"_"+getYear(data.Start_Date__c);
          console.log("monthWithYear",monthWithYear);
          this.MTPCollection.push(monthWithYear);

        })
      })
  }


  ionViewWillEnter()
  {
    let loader = this.loadingCtrl.create({
      content: 'Please wait...getting contacts',
      dismissOnPageChange: true,
      duration:5000
     })
     loader.present();
    this.events = [];
    this.service.loadCallReports()
    .then(
      results => {
        console.log("results",results);
        results.records.map((appointmentEvents) => {
        	console.log("appointmentEvents",appointmentEvents);
          let appId = typeof(appointmentEvents.Id)!=undefined?appointmentEvents.Id:"";
          let dataEvent = {
            Id: appId,
            Contact1__c:appointmentEvents.Contact1__c,
            Organisation__c:appointmentEvents.Organisation__c,
            MTP__c:appointmentEvents.MTP__c,
            Duration__c:appointmentEvents.Duration__c,
            Type__c:appointmentEvents.Type__c,
            User__c:appointmentEvents.User__c,
            Brick__c:appointmentEvents.Brick__c,
            _soupEntryId:appointmentEvents._soupEntryId,
            Tot:{
              OwnerId:"",
              Id:"",
              Owner:"",
              Start_Date__c:null,
              Type_First_Quarter__c:"",
              Type_Fourth_Quarter__c:"",
              Type_Second_Quarter__c :"",
              Type_Third_Quarter__c:"",
              End_Date__c:null,
              All_Day__c:false,
              Type__c:""
            },
            PE:{
              OwnerId:"",
              Id:"",
              Owner:"",
              Start_Date__c:null,
              Type_First_Quarter__c:"",
              Type_Fourth_Quarter__c:"",
              Type_Second_Quarter__c :"",
              Type_Third_Quarter__c:"",
              End_Date__c:null,
              All_Day__c:false
            },
            CallDate__c:new Date(appointmentEvents.CallDate__c),
            Call_Report_Status__c:appointmentEvents.Call_Report_Status__c,
            Contact1__r:appointmentEvents.Contact1__r,
            Organisation__r:appointmentEvents.Organisation__r,
            User__r:appointmentEvents.User__r,
            Date_Time_Actual__c:new Date(appointmentEvents.Date_Time_Actual__c),
            Date_Time_Planned__c:new Date(appointmentEvents.Date_Time_Planned__c),
            Date_time_Visit_End__c:new Date(appointmentEvents.Date_time_Visit_End__c),
            Coaching_Visit__c:appointmentEvents.Coaching_Visit__c,
            Customer:appointmentEvents.Contact1__r.Name,
            Joint_Visit__c:appointmentEvents.Joint_Visit__c,
            Call_Objective__c:appointmentEvents.Call_Objective__c,
          } 
          appointmentEvents = dataEvent;
          let startDate:any = (appointmentEvents.Type__c=="1:1" && appointmentEvents.Date_Time_Actual__c!=null)?appointmentEvents.Date_Time_Actual__c:appointmentEvents.Date_Time_Planned__c;
          let endDate:any = (appointmentEvents.Type__c=="1:1" && appointmentEvents.Date_time_Visit_End__c!=null)?appointmentEvents.Date_time_Visit_End__c:addMinutes(appointmentEvents.Date_Time_Planned__c,30);
          console.log("Id",appointmentEvents.Id);
          console.log("startDate",startDate);
          console.log("endDate",endDate);
          let isEventDraggable = appointmentEvents.Type__c=='Appointment'?true:false;
          let info = {
              title: 'Appt',
              start: startDate,
              end: endDate,
              draggable: isEventDraggable,
              meta: {
                appointmentEvents
              }
            }
          this.events.push(info);
        });
        loader.dismiss();
        this.events = [...this.events];
        console.log("this.events",this.events);
      })
    this.service.loadPharmaEvent()
    .then(
      results => {
        console.log("results",results);
        results.records.map((appointmentEvents) => {

          let dataEvent = {
            Id: appointmentEvents.Id,
            Contact1__c:"",
            Organisation__c:"",
            MTP__c:"",
            Duration__c:"",
            Type__c:"PE",
            User__c:appointmentEvents.OwnerId,
            Brick__c:"",
            _soupEntryId:appointmentEvents._soupEntryId,
            CallDate__c:null,
            Call_Report_Status__c:"",
            Contact1__r:{
              FirstName:"",
              LastName:"",
              Name:"",
              Priority__c:"",
              Solvay_Specialty__c:"",
              Specialty__c:""
            },
            Organisation__r:{
              BillingAddress:"",
              Name:"",
            },
            User__r:{Name:""},
            Date_Time_Actual__c:null,
            Date_Time_Planned__c:null,
            Date_time_Visit_End__c:null,
            Coaching_Visit__c:"",
            Customer:"",
            Joint_Visit__c:"",
            Call_Objective__c:"",
            Tot:{
              OwnerId:"",
              Id:"",
              Owner:"",
              Start_Date__c:null,
              Type_First_Quarter__c:"",
              Type_Fourth_Quarter__c:"",
              Type_Second_Quarter__c :"",
              Type_Third_Quarter__c:"",
              End_Date__c:null,
              All_Day__c:false,
              Type__c:""
            },
            PE:{
              OwnerId:appointmentEvents.OwnerId,
              Id:appointmentEvents.Id,
              Owner:appointmentEvents.Owner,
              Start_Date__c:appointmentEvents.Start_Date__c,
              End_Date__c:appointmentEvents.End_Date__c,
              All_Day__c:appointmentEvents.OwnerId,
              Stage__c:appointmentEvents.Stage__c,
              Location__c:appointmentEvents.Location__c,
              Status__c:appointmentEvents.Status__c,
              Type_of_Event__c:appointmentEvents.Type_of_Event__c,
              _soupEntryId:appointmentEvents._soupEntryId,
            },
          }
          let startDate = new Date(appointmentEvents.Start_Date__c);
          let endDate = new Date(appointmentEvents.End_Date__c); 
          appointmentEvents = dataEvent;
          let info = {
              title: 'PE',
              start: startDate,
              end: endDate,
              meta: {
                appointmentEvents
              }
            }
          this.events.push(info);
        });
        this.events = [...this.events];
      })
    this.service.loadTot()
    .then(
      results => {
        results.records.map((appointmentEvents) => {
          let dataEvent = {
            Id: appointmentEvents.Id,
            Contact1__c:"",
            Organisation__c:"",
            MTP__c:"",
            Duration__c:"",
            Type__c:"TOT",
            User__c:"",
            Brick__c:"",
            CallDate__c:null,
            Call_Report_Status__c:"",
            _soupEntryId:appointmentEvents._soupEntryId,
            Contact1__r:{
              FirstName:"",
              LastName:"",
              Name:"",
              Priority__c:"",
              Solvay_Specialty__c:"",
              Specialty__c:""
            },
            Organisation__r:{
              BillingAddress:"",
              Name:"",
            },
            User__r:{Name:""},
            Date_Time_Actual__c:null,
            Date_Time_Planned__c:null,
            Date_time_Visit_End__c:null,
            Coaching_Visit__c:"",
            Customer:"",
            Joint_Visit__c:"",
            Call_Objective__c:"",
            Tot:{
              OwnerId:appointmentEvents.OwnerId,
              Id:appointmentEvents.Id,
              Owner:appointmentEvents.Owner,
              Start_Date__c:appointmentEvents.Start_Date__c,
              Type_First_Quarter__c:appointmentEvents.Type_First_Quarter__c,
              Type_Fourth_Quarter__c:appointmentEvents.Type_Fourth_Quarter__c,
              Type_Second_Quarter__c :appointmentEvents.Type_Second_Quarter__c,
              Type_Third_Quarter__c:appointmentEvents.Type_Third_Quarter__c,
              End_Date__c:appointmentEvents.End_Date__c,
              All_Day__c:appointmentEvents.All_Day__c,
              Type__c:appointmentEvents.Type__c,
            },
            PE:{
              OwnerId:"",
              Id:"",
              Owner:"",
              Start_Date__c:null,
              Type_First_Quarter__c:"",
              Type_Fourth_Quarter__c:"",
              Type_Second_Quarter__c :"",
              Type_Third_Quarter__c:"",
              End_Date__c:null,
              All_Day__c:false
            },
          }
          let startDate = new Date(appointmentEvents.Start_Date__c);
          let endDate = new Date(appointmentEvents.End_Date__c);  
          appointmentEvents = dataEvent;
          let info = {
              title: 'TOT',
              start: startDate,
              end: endDate,
              meta: {
                appointmentEvents
              }
            }
          this.events.push(info);
        });
        this.events = [...this.events];
        this.eventsBkup = this.events;
      })
  }


  gotoDetailsPage(event:any)
  {
  	// if((event.meta.appointmentEvents.Type__c).toLowerCase() == "appointment")
  	// {
  	// 	this.navCtrl.push(AppointmentDetailsPage, {row:event});
  	// }
  	// else if((event.meta.appointmentEvents.Type__c).toLowerCase() == "1:1")
  	// {
  	// 	this.navCtrl.push(CallReportDetailsPage, {row:event});
  	// }
  	// else if((event.meta.appointmentEvents.Type__c).toLowerCase() == "pe")
  	// {
  	// 	this.navCtrl.push(PharmadetailsPage, {row:event});
  	// }
  	// else if((event.meta.appointmentEvents.Type__c).toLowerCase  () == "tot")
  	// {
  	// 	this.navCtrl.push(TotdetailsPage, {row:event});
  	// }
  }

  async gotoStartPage(event:any)
  {
  	// if((event.meta.appointmentEvents.Type__c).toLowerCase() == "appointment")
  	// {
  	// 	this.service.getContactReference(event.meta.appointmentEvents.Contact1__c)
  	// 	.then(results=>{
  	// 		console.log("results",results);
  	// 		if(results.records.length>0)
  	// 		{
  	// 			this.navCtrl.push(CallReportPage,{row:results.records[0]});
  	// 		}
  	// 		else
  	// 		{
  	// 			this.presentAlert();
  	// 		}
  	// 	})
  	// }
  	// else
  	// {
  	// 	this.service.getContactReference(event.meta.appointmentEvents.Contact1__c)
  	// 	.then(results=>{
  	// 		console.log("results",results);
  	// 		if(results.records.length>0)
  	// 		{
  	// 			this.navCtrl.push(CallReportDetailsPage,{row:results.records[0]});
  	// 		}
  	// 		else
  	// 		{
  	// 			this.presentAlert();
  	// 		}
  	// 	})
  	// }
  }
  deleteAppointment(event:any)
  {
  	this.confirmationDialogService.confirm('Please confirm..', 'Are you sure, want to delete this appointment ?')
  	.then((confirmed) => console.log('User confirmed:', confirmed))
  	.catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }



}