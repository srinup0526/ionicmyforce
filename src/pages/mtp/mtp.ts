import { Component, ViewChild, ElementRef } from '@angular/core';
import {
  IonicPage, NavController, NavParams, AlertController, PopoverController, ViewController, ToastController,
  LoadingController, AlertOptions, NavOptions
} from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PopoverComponent } from '../../components/popover/popover';
import { MtpdetailsPage } from '../mtpdetails/mtpdetails';


import { MTPScheme } from  '../../models/scheme/MTPScheme';
import { MTPCollection } from  '../../collections/MTPCollection';
import { TableHeaderItemOption } from './../../components/table/table-header/TableHeaderItemOption';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { FilterPanelComponent } from "../../components/filter/filter-panel/filter-panel";
import {Query} from "../../services/common/Query";
import { Settings, SettingsImpl } from '../../services/db/Settings';

/**
 * Generated class for the MtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var cordova: any;


@IonicPage()
@Component({
  selector: 'page-mtp',
  templateUrl: 'mtp.html',
})

export class MtpPage {
  public rows : any;
  public newrows : any;
  public MTPBackup: any;
  public MTP: any;
  public Mtp: any;
  public MTPName='MTP';
  public mtpCount:any = 0;
  public tableOptions: LazyTableOption;
  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: MTPCollection;
  public settings: SettingsImpl;
  public typeFilterOptions:string = 'all';
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('mainContent') mainContent: ElementRef;
  message = "";
  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }
  constructor(public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public popoverController:PopoverController,
    public navParams: NavParams,
    public mtpCollection:MTPCollection) {
      this.settings = Settings.getInstance();
    const headerItems = [
        {
          title: 'common.names.Month',
          fields: [MTPScheme.fields.month],
          isSortable: true,
          isAsc: true,
          isActive: false,
          picklistValues: {},
        },
        {
          title: 'common.names.Year',
          fields: [MTPScheme.fields.year],
          isSortable: true,
          isAsc: true,
          isActive: false,
          picklistValues: {}
        },
        {
          title: 'common.names.StartDate',
          fields: [MTPScheme.fields.startDate],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.EndDate',
          fields: [MTPScheme.fields.endDate],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Status',
          fields: [MTPScheme.fields.status],
          isSortable: true,
          isAsc: true,
          isActive: false
        }
        ];
    
    this.tableOptions = {
      headerItems: headerItems,
      rowHandler: (event, reference) => {
        this.itemTapped(reference);
      },
      batchSize: 100
    };
    
    this.searchString = '';

    this.collection = this.mtpCollection;
    }

    public onSearchHandler(searchBar: any) {
      console.log("searchBar",searchBar);
      this.searchString = searchBar.value || '';
      this.lazyTable.setSearchString(this.searchString);
      this.lazyTable.reloadTable();
    }
    
    public onClearHandler(ev: any): void {
      ev.target.value = '';
      this.searchString = ev.target.value;
    
      this.lazyTable.setSearchString(this.searchString);
      this.lazyTable.reloadTable();
    }
    
    public onTapAddBtnHandler(): void {
      this.presentPopover();
    }
    
    public onDismissCreatePopoverHandler(): void {
      this.reloadTable();
    }
    
    private reloadTable(): void {
      this.lazyTable.reloadTable();
    }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad MtpPage');
    // this.loadMtp()
    // .then(response=>
    //   {
    //     this.rows=(this.MTP);
    //     console.log("rowsMTP",this.rows);
    
    //   })
    //   this.newrows=(this.rows);
    //   console.log("new rows",this.newrows);
    //   this.rows = this.navParams.get('Mtp') || {};
    
  }

  ionViewWillEnter() {
    this.lazyTable.reloadTable();
    console.log("ionviewwillenter call");
    this.setReferenceCount();
    console.log("ionviewwillenter call this.mtpCount",this.mtpCount);
  }

  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable();
    this.setReferenceCount();
  }

  private setReferenceCount(): number {
    const countQuery = this.collection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);

    return this.collection.runCountQuery(query.query)
      .then((count) => {
        console.log("count",count);
        return this.mtpCount = count || 0;
      })
  }

  public onChangeQueryHandler(query: Query): void {
    this.setReferenceCount();
  }

  onSearch(ev: any) {
    console.log(ev)
    this.rows = this.MTPBackup;
    //this.initializeItems();
    // set val to the value of the searchbar
    const val = ev.target.value;
    console.log(val);
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.rows = this.rows.filter((contact) => {
        return (contact.Month__c.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
        (contact.Year__c && contact.Year__c.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
        (contact.Start_Date__c && contact.Start_Date__c.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
        (contact.End_Date__c && contact.End_Date__c.toLowerCase().indexOf(val.toLowerCase()) > -1) ||
        (contact.Status__c && contact.Status__c.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
  
  private presentPopover(): Promise<any> {
    let popover = this.popoverController.create(PopoverComponent,{"parentPage": this});
    let options: NavOptions = {};
  
    popover.onDidDismiss(this.onDismissCreatePopoverHandler.bind(this));
    
    return popover.present(options);
  }
  
  startDateEndDate(){

  }
  itemTapped(row: string) {
   console.log("rowmtp",row)
    this.navCtrl.push(MtpdetailsPage, { "row": row, parentPage:this });

  }

  reloadMethod()
  {
    console.log("reload method", this.lazyTable);
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
    this.lazyTable.reloadTable();
  }

}
