import {
  Component,
  ViewChild,
  TemplateRef,ElementRef
} from '@angular/core';
import {
  endOfMonth,
  isSameDay,
  isSameMonth,
  addMinutes,
  format,
  startOfMonth,
  getMonth,
  // getDay,
  getYear,
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
   CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  DAYS_OF_WEEK,
  CalendarEventTitleFormatter
} from 'angular-calendar';
import {  MenuController } from 'ionic-angular/index';
import { IonicPage, NavController, Select, ToastController ,AlertOptions} from 'ionic-angular';
import { OAuth, DataService } from 'forcejs'
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.service';
import { AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as Constants from '../../services/constants';

import { DatePipe } from '@angular/common';
import moment from 'moment';
import {ReferenceCollection} from "../../collections/references/ReferenceCollection";
import { ReferenceScheme } from './../../models/scheme/ReferenceScheme';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import {FilterPanelComponent} from "../../components/filter/filter-panel/filter-panel";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import {TargetReferencesCollection} from "../../collections/references/TargetReferencesCollection";
import {NonTargetReferencesCollection} from "../../collections/references/NonTargetReferencesCollection";
import {BrickPickListDatasource} from "../../services/db/picklist-managers/datasource/BrickPicklistDatasource";
import {Query} from "../../services/common/Query";
import { PatchCustomerScheme } from './../../models/scheme/PatchCustomerScheme';
import { PatchCustomerCollection } from '../../collections/PatchCustomerCollection'
import { PatchPicklistDatasource } from '../../services/db/picklist-managers/datasource/PatchPicklistDatasource';
import { CallsCollection } from '../../collections/CallReportsCollection/CallsCollection';
import { AppointmentsCollection } from '../../collections/CallReportsCollection/AppointmentsCollection';
import { CallReportsCollection } from '../../collections/CallReportsCollection/CallReportsCollection'
import { CallReportScheme } from './../../models/scheme/CallReportScheme';
import { TotScheme } from './../../models/scheme/TotScheme';
import { PharmaEventScheme } from './../../models/scheme/PharmaEventScheme';
import { MTPScheme } from './../../models/scheme/MTPScheme';
import { IfStmt } from '@angular/compiler';
import { TotsCollection } from '../../collections/tots-collection/tots-collection';
import { PharmaEventsCollection } from  '../../collections/PharmaEventsCollection';
import { MTPCollection } from  '../../collections/MTPCollection';
import { CallReport } from '../../models/CallReport';
import { Tot } from '../../models/tot';
import { PharmaEvent } from '../../models/PharmaEvent';
import { Utils } from '../../utils/Utils';
import { CallReportPage } from '../call-report/call-report';
import { CallReportDetailsPage } from '../call-report-details/call-report-details';
import { EditCallReportPage } from '../edit-call-report/edit-call-report';
import { ContactPage } from '../contact1/contact';
import { AddtotPage } from '../addtot/addtot';
import { AddpharmaeventPage } from '../addpharmaevent/addpharmaevent';
import { MTP } from '../../models/MTP';
import { SforceDataContext } from '../../collections/SforceDataContext';
//import { CustomEventTitleFormatter } from '../../providers/ custom-event-title-formatter.provider';

interface AppointmentEvents {
  
}


export class CustomEventTitleFormatter extends CalendarEventTitleFormatter {

  weekTooltip(event: CalendarEvent, title: string) {
    if ((!event.meta.appointment) && (!event.meta.pe) && (!event.meta.tot)) {
      return super.weekTooltip(event, title);
    }
  }

  dayTooltip(event: CalendarEvent, title: string) {
    if ((!event.meta.appointment) && (!event.meta.pe) && (!event.meta.tot)){
      return super.dayTooltip(event, title);
    }
  }
  week(event: CalendarEvent, title: string): string {
    if((!event.meta.appointment) && (!event.meta.pe) && (!event.meta.tot))
    {
      return super.week(event,title);
    }
    else
    {
      let eventTilte = '<div style="border-top:0.5px solid black;margin-bottom: 0.5px;border-bottom:0.5px solid black;">';
      let start = format(event.start, 'h:mm a');
      event.meta.appointment.type=='1:1'?eventTilte+='<span><img src="assets/imgs/call.png" style="width:25px;height: 20px;"></span><small style="font-size: 8px;">'+start+'</small>':'';
      event.meta.appointment.type=='Appointment'?eventTilte+='<span><img src="assets/imgs/appt-bw.png" style="width:25px;height: 20px;"></span><small style="font-size: 8px;">'+start+'</small>':'';
      event.meta.appointment.type=='TOT'?eventTilte+='<span><img src="assets/imgs/tot.png" style="width:25px;height: 20px;"></span><small style="font-size: 8px;">'+start+'</small>':'';
      event.meta.appointment.type=='PE'?eventTilte+='<span><img src="assets/imgs/pe.png" style="width:25px;height: 20px;"></span><small style="font-size: 8px;">'+start+'</small>':'';

    eventTilte+= '</div>';
      return eventTilte;
    }
  }

  day(event: CalendarEvent, title: string): string {
    if(!event.meta.appointment)
    {
      return super.day(event,title);
    }
    else
    {
      let eventTilte = '<div style="border-top:0.5px solid black;margin-bottom: 0.5px;border-bottom:0.5px solid black;">';
      let start = format(event.start, 'h:mm a');
      event.meta.appointment.appointment.type=='1:1'?eventTilte+='<span><img src="assets/imgs/call.png" style="width:25px;height: 20px;"></span><small style="font-size: 8px;">'+start+'</small>':'';
      event.meta.appointment.appointment.type=='Appointment'?eventTilte+='<span><img src="assets/imgs/appt-bw.png" style="width:25px;height: 20px;"></span><small style="font-size: 8px;">'+start+'</small>':'';
      event.meta.appointment.type=='TOT'?eventTilte+='<span><img src="assets/imgs/tot.png" style="width:25px;height: 20px;"></span><small style="font-size: 8px;">'+start+'</small>':'';
      event.meta.appointment.type=='PE'?eventTilte+='<span><img src="assets/imgs/pe.png" style="width:25px;height: 20px;"></span><small style="font-size: 8px;">'+start+'</small>':'';

    eventTilte+= '</div>';
      return eventTilte;
    }
  }
}

@IonicPage()
@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter
    }
  ]
})
export class CalendarPage {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  @ViewChild('eventDetails') eventDetails: TemplateRef<any>;
  @ViewChild('mySelect') mySelect: TemplateRef<any>;
  @ViewChild('eventFilterOptions') eventFilterOptions: TemplateRef<any>;
  @ViewChild('eventSelect') eventSelect: Select;

  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('filterType') filterTypeComponent: Select;
  @ViewChild('mainContent') mainContent: ElementRef;
  dynamicClass:string = 'col-sm-12';
  view: CalendarView = CalendarView.Month;
  

  CalendarView = CalendarView;

  viewDate: Date = new Date();
  filterOptions: any = [];
  eventFilterOptionsList: any = [
                                  {name:'APPOINTMENT', checked:false},
                                  {name:'TOT', checked:false},
                                  {name:'1:1', checked:false},
                                  {name:'PE', checked:false}
                                 ];
  public tableOptions: LazyTableOption;
  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: any;
  public patchCustomer:any;
  public rows: any;
  public patchcollection: PatchCustomerCollection;
  public TOTdata:any;
  public PEdata:any;
  public MTPdata:any;
  public filterOptionsCal: AlertOptions;
  public settings: SettingsImpl;
  targetNontarget:string = "tc";
  public contactsCount:any = 0;
  public patchCount:any = 0;
  references: Array<any> = [];
  isIndiamodule:boolean= false;
  activeDayIsOpen: boolean = true;
 // targetDetailsName:string = Constants.Appointment
  public appointmentsdata:any;
  public callsdata:any;
  public activeUser:any;
  public referenceCollectionType : any;
  public MTPdataCollection: any = [];
  newlyCreatedMtp:string;
  missedCount: number = 0;
  modalTitle:any;
  targetDetailsName:string = CallReportScheme.TYPE_APPOINTMENT;
  MonthArray:string[] = ['January','Febraury','March','April','May','June','July','August','September','October','November','December'];
  modalData: {
    action: string;
    event: Array<CalendarEvent<{ tot?:Tot, pe?:PharmaEvent, calls?:CallReport, appointment?:CallReport }>>;
  };
  events: Array<CalendarEvent<{ tot?:Tot, pe?:PharmaEvent, calls?:CallReport, appointment?:CallReport}>> = [];
  externalEvents: any = [];


  constructor( public toastCtrl: ToastController,
    private referenceCollection: ReferenceCollection,
    private targetReferencesCollection: TargetReferencesCollection,
    private nonTargetReferencesCollection: NonTargetReferencesCollection,
    public patchCustomerCollection:PatchCustomerCollection,
    private brickPickListDatasource: BrickPickListDatasource,
    private patchPicklistDatasource: PatchPicklistDatasource,
    public callsCollection: CallsCollection,
    public totCollection:TotsCollection,
    public pharmaEventCollection: PharmaEventsCollection,
    public appointmentsCollection: AppointmentsCollection,
    public mtpCollection:MTPCollection,
    public loadingCtrl: LoadingController,public storage:Storage, 
    public callReportsCollection:CallReportsCollection,
    public alertCtrl: AlertController, private confirmationDialogService: ConfirmationDialogService, 
    public navCtrl: NavController,private modal: NgbModal, private menuCtrl: MenuController) {

      this.filterOptions = [{key:"Appointment",value:"Appointments"},{key:"1:1",value:"Call Reports"},{key:"PE",value:"Pharma Events"},{key:"TOT",value:"Time Off Territory"}];
      this.settings = Settings.getInstance();
      const headerItems = [
        {
          title: 'common.names.Customer',
          fields: [PatchCustomerScheme.fields.contactName],
          isSortable: true,
          isAsc: true,
          isActive: false,
          modelFunction:'getCustomerName',
          cssClass:'drag-active',
          dropData:"{event: row}"
        },
      ];
      this.tableOptions = {
        headerItems: headerItems,
        rowHandler: (event, patchCustomer) => {
          this.itemTapped(patchCustomer);
        },
        batchSize: 100
      };
    this.searchString = '';
    
   // this.onChangeContactTypeHandler();
   this.collection = this.referenceCollection;
   // console.log("collection",this.collection);


    //this.externalEvents= [...this.rows];
     this.filterOptions = {
       cssClass: 'popup alert list without-header',
       buttons: []
     };
    
     SforceDataContext.getActiveUser()
     .then((currentUser) => {
       console.log("currentUser",currentUser);
       this.activeUser = currentUser;
     });
       
      // })

      this.referenceCollectionType = "tc";
      this.targetReferencesCollection.fetchAll()
        .then(res=> this.targetReferencesCollection.getAllEntitiesFromResponse(res))
        .then(res=>{
          this.externalEvents = res;
        })
    }
    ionViewDidLoad():void {}
    ionViewWillEnter():void{

let loader = this.loadingCtrl.create({
  content: 'Please wait, getting info',
 duration:1000
 })
 loader.present();
 let callData = [];
 let peData = [];
 let TOTData = [];
 this.events = [];
this.loadAppointmentcollection()
.then(results=>{
this.missedCount = 0;
console.log("results",results);
results.map((appointment) => {
 console.log("appointment",appointment);
 let appId = appointment.hasOwnProperty('Id')?appointment.id:"";
 console.log("appId",appId);

 let dataEvent = {
   type:appointment.type,
     appointment:appointment
}


let startDate:any = (appointment.type=="1:1" && appointment.dateTimeVisitStart!=null)?appointment.dateTimeVisitStart:appointment.dateTimePlanned;
let endDate:any = (appointment.type=="1:1" && appointment.dateTimeVisitStart!=null)?new Date(appointment.dateTimeVisitStart): new Date(addMinutes(appointment.dateTimePlanned,30));
console.log("startDate at Calls",startDate);
console.log("endDate at Calls",endDate);
let apptTitle = appointment.type=='Appointment'?"Appt":"1:1";
(apptTitle=='Appt' && moment(appointment.dateTimePlanned)< moment())?this.missedCount++:'';
let isEventDraggable = appointment.type=='Appointment'?true:false;
appointment = dataEvent;
let info = {
    title: apptTitle,
    start: startDate,
    end: endDate,
    draggable: isEventDraggable,
    meta: {
     appointment:appointment
   }
 }
this.events.push(info);
callData.push(info);

})
loader.dismiss();
this.events = [...this.events];
console.log("this.events",this.events);
})

this.loadPECollection()
.then(results=>{
 this.missedCount = 0;
 console.log("results",results);
 results.map((pe) => {
   let dataEvent = {
    type:"PE",
    pe:pe
  }

           let startDate = moment(pe.startDate).toDate();
           let endDate = moment(pe.endDate).toDate();
           console.log("startDate at PE",startDate);
           console.log("endDate at PE",endDate);
           pe = dataEvent;
  let info = {
    title: 'PE',
               start: startDate,
               end: endDate,
               meta: {
                 pe:pe
               }
   }
  this.events.push(info);
  peData.push(info);

 })
 this.events = [...this.events];
}),
this.loadtotCollection()
.then(results=>{
 this.missedCount = 0;
 console.log("results",results);
 results.map((tot) => {
   let dataEvent = {
    type:"TOT",
    tot:tot
  }

           let startDate = moment(tot.startDate).toDate();
           let endDate = moment(tot.endDate).toDate();
           let allDayVal = tot.allDay?true:false;
           console.log("startDate at TOT",startDate);
           console.log("endDate at TOT",endDate);
           tot = dataEvent;
  let info = {
    title: 'TOT',
              start: startDate,
              end: endDate,
              allDay:allDayVal,
              meta: {
                tot:tot
               }
   }
  this.events.push(info);
  TOTData.push(info);

 })
 this.events = [...this.events];
})
   

}
    ionViewDidEnter():void {}

    onFilterChangeHandler(filterData) {
      console.log("filter event",filterData);
      console.log("filter event", filterData[0].fields["Organisation__r.Brick__c"], this.referenceCollectionType);

      if(this.referenceCollectionType == 'tc') {
        this.targetReferencesCollection.fetchAll()
        .then(res=> this.targetReferencesCollection.getAllEntitiesFromResponse(res))
        .then(res=>{
          this.externalEvents = res;
          if(filterData[0].fields["Organisation__r.Brick__c"]) {
            this.externalEvents = this.externalEvents.filter(val=> {
              console.log("filter target",val.organizationBrick, filterData[0].fields["Organisation__r.Brick__c"]);
              return val.organizationBrick == filterData[0].fields["Organisation__r.Brick__c"]
            } );  
          }
        })
      }else{
        this.nonTargetReferencesCollection.fetchAll()
        .then(res=> this.nonTargetReferencesCollection.getAllEntitiesFromResponse(res))
        .then(res=>{
          this.externalEvents = res;
          if(filterData[0].fields["Organisation__r.Brick__c"]) {
            this.externalEvents = this.externalEvents.filter(val=> {
              console.log("filter nontarget",val.organizationBrick, filterData[0].fields["Organisation__r.Brick__c"]);
              return val.organizationBrick == filterData[0].fields["Organisation__r.Brick__c"]
            } );  
          }
        })
      }


      

    
    }

    public onChangeQueryHandler(query: Query): void {
      this.setPatchCustomerCount();
      
    }
    handleEvent(action: string, event: Array<CalendarEvent<{ tot?:Tot, pe?:PharmaEvent, calls?:CallReport, appointment?:CallReport }>>): void {
      console.log("action",action);
      console.log("event",event);
      this.modalData = { event, action };
      this.setModalTitle(event);
      this.targetDetailsName = "";
      let ngbModalOptions: NgbModalOptions = {
        backdrop : 'static',
        keyboard : false,
        size : 'lg',
        centered:true
      };
      this.modal.open(this.modalContent, ngbModalOptions);
    }
    
  setModalTitle(event:any)
  {
    switch (event.title) {
      case "Appt":
        this.modalTitle = CallReportScheme.TYPE_APPOINTMENT;
        break;
      case "1:1":
        this.modalTitle = CallReportScheme.TYPE_ONE_TO_ONE;
        break;
      case "TOT":
        this.modalTitle = TotScheme.TYPE_Title;
        break;
      case "PE":
        this.modalTitle = PharmaEventScheme.TYPE_Title;
        break;
      default:
        this.modalTitle = CallReportScheme.TYPE_APPOINTMENT;
        break;
    }
  }
    public onChangeContactTypeHandler(type) {
      this.referenceCollectionType = type;
      if(type == 'tc') {
        this.targetReferencesCollection.fetchAll()
        .then(res=> this.targetReferencesCollection.getAllEntitiesFromResponse(res))
        .then(res=>{
          this.externalEvents = res;
        })
      }else{
        this.nonTargetReferencesCollection.fetchAll()
        .then(res=> this.nonTargetReferencesCollection.getAllEntitiesFromResponse(res))
        .then(res=>{
          this.externalEvents = res;
        })
      }
      
      this.targetNontarget = type;
      
      this.filterTypeComponent.close();
      
      // this.lazyTable.setCollection(this.collection);
      // this.lazyTable.reloadTable();
    }
    
    public onSwipeHandler(event){
      switch (event.offsetDirection) {
        case 2: this.filterPanel.hidePanel(); break;
        case 4: this.filterPanel.showPanel(); break;
      }
    }
    private setReferenceCount(): number {
      const countQuery = this.collection.getCountQuery();
      const query = this.lazyTable.getTableQueryWithConditions(countQuery);
  
      return this.collection.runCountQuery(query.query)
        .then((count) => {
          return this.contactsCount = count || 0;
        })
    }
  
    private setPatchCustomerCount(): number {
      const countQuery = this.collection.getCountQuery();
      const query = this.lazyTable.getTableQueryWithConditions(countQuery);
  
      return this.collection.runCountQuery(query.query)
        .then((count) => {
          return this.patchCount = count || 0;
        })
    }
    itemTapped(row) {
      console.log("rowpatch",row)
      // this.navCtrl.push(PatchdetailsPage, { "row": row, parentPage:this });
   
     }
public loadAppointmentcollection(){
    return  this.appointmentsCollection.fetchAll()
      .then(data=>{ return  this.appointmentsCollection.getAllEntitiesFromResponse(data);})
      .then(appointment=>{
        this.appointmentsdata=appointment;
        console.log("appointmentdata",this.appointmentsdata);
        return appointment;
       
      })
     }
public loadCallcollection(){
      this.callsCollection.fetchAll()
      .then(data=>{ return  this.callsCollection.getAllEntitiesFromResponse(data);})
      .then(call=>{
        this.callsdata=call;
        console.log("callsdata",this.callsdata); 
      })
     }

public loadtotCollection(){
  return this.totCollection.fetchAll()
      .then(data=>{ return  this.totCollection.getAllEntitiesFromResponse(data);})
      .then(Tot=>{
        this.TOTdata=Tot;
        console.log("TOTdata",this.TOTdata);
        return Tot;
      })
}
public loadPECollection(){
  return this.pharmaEventCollection.fetchAll()
      .then(data=>{ return  this.pharmaEventCollection.getAllEntitiesFromResponse(data);})
      .then(PE=>{
        this.PEdata=PE;
        console.log("PEdata",this.PEdata); 
        return PE;
      })
}
public loadMTPCollection(){
  this.mtpCollection.fetchAll()
      .then(data=>{ return  this.mtpCollection.getAllEntitiesFromResponse(data);})
      .then(MTP=>{
        let monthWithYear = getMonth(MTP.startDate)+"_"+getYear(MTP.startDate);
        console.log("monthWithYear",monthWithYear);
        this.MTPdataCollection.push(monthWithYear);
      })
}

dayClicked({ date, events }: { date: Date; events: Array<CalendarEvent<{ tot?:Tot, pe?:PharmaEvent, calls?:CallReport, appointment?:CallReport }> >}): void {
  if (isSameMonth(date, this.viewDate)) {
    if (
      (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
      events.length === 0
    ) {
      this.activeDayIsOpen = false;
    } else {
      this.activeDayIsOpen = true;
    }
    this.viewDate = date;
  }
}
closeOpenMonthViewDay() {
  this.activeDayIsOpen = false;
}
setView(view: CalendarView) {
  this.view = view;
}
eventTimesChanged({
  event,
  newStart,
  newEnd
}: CalendarEventTimesChangedEvent): void {
  this.events = this.events.map(iEvent => {
    if (iEvent === event) {
      return {
        ...event,
        start: newStart,
        end: newEnd
      };
    }
    return iEvent;
  });
  this.handleEvent('Dropped or resized', [event]);
}

presentAlert() {
  console.log('Alert Event');
   const alert = this.alertCtrl.create({
    title: 'Alert!',
    subTitle: 'Customer not available, please contact the administrator!',
    buttons: ['OK']
  });
  alert.present();
}
async gotoStartPage(event:any)
{
  console.log("event",event);
  if((event.meta.appointment.appointment.type).toLowerCase() == "appointment")
  {
        if(moment()>=moment(event.start))
        {
          this.navCtrl.push(EditCallReportPage,{row:event.meta.appointment.appointment});
        }
        else
        {
          const toast = this.toastCtrl.create({
            message: "Not able to convert the future calls",
            showCloseButton:true,
            cssClass:"toastClass",
            duration:3000,
          });
          toast.present();
        }
   
  }
  else
  {
        this.navCtrl.push(CallReportDetailsPage,{row:event.meta.appointment.appointment});
    
  }
}

deleteAppointment(eventToDelete: CalendarEvent<{ appointment: CallReport }>)
{
  this.confirmationDialogService.confirm('Please confirm..', 'Are you sure, want to delete this appointment ?')
  .then((confirmed) => console.log('User confirmed:', confirmed))
  .then(res=>{
    this.events = this.events.filter(event => event !== eventToDelete);
  })
  .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
}

deleteTOT(eventToDelete: CalendarEvent<{ tot: Tot }>)
{
  this.confirmationDialogService.confirm('Please confirm..', 'Are you sure, want to delete this TOT ?')
  .then((confirmed) => console.log('User confirmed:', confirmed))
  .then(res=>{
    this.events = this.events.filter(event => event !== eventToDelete);
  })
  .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
}

onEventType(evntType:string)
{
  console.log("evntType",evntType)
  if(evntType=="Appointment")
  {
    this.navCtrl.push(ContactPage);
  }
}

gotoEventCreationPage(eventType:string){
  if(eventType=="APPOINTMENT") {
    this.navCtrl.push(ContactPage);
  }
  else if(eventType=='TOT')
  {
    this.navCtrl.push(AddtotPage);
  }
  else if(eventType=='PE')
  {
    this.navCtrl.push(AddpharmaeventPage);
  }
}
eventFilter()
{
  let ngbModalOptions: NgbModalOptions = {
    backdrop : 'static',
    size : 'sm',
    centered:true
  };
  this.modal.open(this.eventFilterOptions, ngbModalOptions);
}
eventDropped({
  event,
  newStart,
  newEnd,
  allDay
}: CalendarEventTimesChangedEvent): void {
  console.log("event",event)
  console.log("newStart",newStart);
  console.log("newEnd",newEnd);
  console.log("allDay",allDay);
  let droppedMonth = getMonth(newStart);
  let droppedYear = getYear(newStart);
  console.log("droppedMonth",droppedMonth);
  console.log("droppedYear",droppedYear);
  let droppedYearMonth = droppedMonth+"_"+droppedYear;
  this.mtpCollection.findMTPForMonth(this.MonthArray[droppedMonth],droppedYear)
  .then(resData=>{
    console.log("resData",resData);
    if(resData.length>0)
    {
      this.addEvent(event,newStart,newEnd,allDay);
    }
    else{
      this.confirmationDialogService.confirm('Please confirm..', 'Chosen Date, Dont have MTP, Do you want to  create to proceed','Create',"Cancel",'lg')
      .then((confirmed) => {
        console.log('User confirmed:', confirmed)
        this.createMtp(droppedYearMonth).then((info)=>{
          console.log("Info",info);
          this.addEvent(event,newStart,newEnd,allDay);
        })
    });
  }
})
  console.log("this.events",this.events);

  if (this.view === 'month') {
    this.viewDate = newStart;
    this.activeDayIsOpen = true;
  }
}
addEvent(event:any,newStart:any,newEnd:any,allDay:boolean) {
console.log("addevent",event);
  let saveAppData =new CallReport({
     contactSfId:event.contactSfId,
    organizationSfId:event.organizationSfId,
    duration:"30",
    type:CallReportScheme.TYPE_APPOINTMENT,
    userSfid:this.activeUser.id,
    contactFirstName:event.contactFirstName,
    contactLastName:event.contactLastName,
    contactRecordType:event.contactRecordType,
    name:event.name,
    contactSpecialty:event.contactSpecialty,
    organizationName:event.organizationName,
    userFirstName:event.userFirstName,
    jointVisitParticipants:event.jointVisitParticipants,
    status:null,
    dateTimeVisitStart:null,
    dateTimePlanned:new Date(newStart),
    dateTimeVisitEnd:null,
    coachingVisit:"",
    callObjective:"",
    createdOffline:true
  });
  let eventInfo = {
    title: 'Appt',
    start: newStart,
    end: new Date(addMinutes(newStart,30)),
    meta: {
      appointment:saveAppData
    }
  };
   this.callReportsCollection.createEntity(saveAppData);
    console.log("eventInfo",eventInfo);
    if (typeof allDay !== 'undefined') {
      event.allDay = allDay;
    }
    event.start = newStart;
      if (newEnd) {
        event.end = newEnd;
      }
      this.events.push(eventInfo);
      this.events = [...this.events];
}
createMtp(monthYear:any)
{
  let splitedValues = monthYear.split("_");
  let month = splitedValues[0];
  let year = splitedValues[1];
  let monthText = this.MonthArray[month];
  let monthName = this.MonthArray[parseInt(month)-1];
  console.log("splitedValues",splitedValues);
 
  console.log("monthName",monthName);
  console.log("month",month);
  console.log("year",year);
  let mtp = new MTP({
    createdOffline: true,
    medRep: this.activeUser.id,
    userLastName: this.activeUser.firstName,
    userFirstName: this.activeUser.lastName,
    startDate: Utils.firstDateCurrentMonth(monthName, year),
    endDate: Utils.endDateCurrentMonth(monthName, year),
    month: month,
    year: year,
    status: "Draft"
  });

  console.log("mtp",mtp);
  return this.mtpCollection.fetchAll()
  .then(res=> {return this.mtpCollection.getAllEntitiesFromResponse(res)})
  .then(mtpData=>{
    console.log("mtpData",mtpData);
    return mtpData;
  })
  
}
}