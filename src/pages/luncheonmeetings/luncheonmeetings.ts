import { Component,ViewChild, ElementRef } from '@angular/core';
import {
  IonicPage, NavController, NavParams, LoadingController, ModalController, PopoverController ,AlertOptions,
  Select, ToastController
} from 'ionic-angular';
import { PharmaEventScheme } from  '../../models/scheme/PharmaEventScheme';
import { PharmaEventsCollection } from  '../../collections/PharmaEventsCollection';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { Query } from "../../services/common/Query";
import { FilterPanelComponent } from "../../components/filter/filter-panel/filter-panel";
import { AddluncheonmeetingsPage } from '../addluncheonmeetings/addluncheonmeetings';
import { LuncheondetailsPage } from '../luncheondetails/luncheondetails';
/**
 * Generated class for the LuncheonmeetingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-luncheonmeetings',
  templateUrl: 'luncheonmeetings.html',
})
export class LuncheonmeetingsPage {
  luncheonMeetingsHeaderItems : any;
  public collection: any;
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public filterOptions: AlertOptions;
  public lunchonMeetingsCount:any = 0;
  

  public typeFilterOptions:string = 'Draft';
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('filterType') filterTypeComponent: Select;
  @ViewChild('mainContent') mainContent: ElementRef;
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public pharmaEventCollection: PharmaEventsCollection) {

    this.luncheonMeetingsHeaderItems = [
      {
        title: 'common.names.Status',
        fields: [PharmaEventScheme.fields.status],
        isSortable: true,
        isAsc: true,
        isActive: false,
        picklistValues: {},
      },
      {
        title: 'common.names.EventName',
        fields: [PharmaEventScheme.fields.eventName],
        isSortable: true,
        isAsc: true,
        isActive: false
      },
      {
        title: 'common.names.TypeOfEvent',
        fields: [PharmaEventScheme.fields.eventType],
        isSortable: true,
        isAsc: true,
        isActive: false
      },
      {
        title: 'common.names.StartDate',
        fields: [PharmaEventScheme.fields.startDate],
        isSortable: true,
        isAsc: true,
        isActive: false,
        modelFunction:'getDateTimePlannedInFormat'
      },
      {
        title: 'common.names.Location',
        fields: [PharmaEventScheme.fields.location],
        isSortable: true,
        isAsc: true,
        isActive: false
      }
      ];
  
      this.tableOptions = {
        headerItems: this.luncheonMeetingsHeaderItems,
        rowHandler: (event, reference) => {
          this.itemTapped(reference);
        },
        batchSize: 100
      };
  
      this.searchString = '';
      this.collection = this.pharmaEventCollection;
 
      this.filterOptions = {
        cssClass: 'popup alert list without-header',
        buttons: []
      };
  
      
  }

  public onChangeStatusTypeHandler(type) {
    console.log("type",type);
  
    console.log("this.totOpenCollection",this.pharmaEventCollection);
  
    if(type == 'Draft') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }
    else if(type == 'Submitted Offline') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }
    else if(type == 'Submitted') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    } else if(type == 'Awaiting FLM Approval') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }else if(type == 'Awaiting NSM Approval') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }
    else if(type == 'Awaiting Compliance Approval') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }
    else if(type == 'Approved') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }  else if(type == 'Rejected') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }
    else if(type == 'Rejected By FLM') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }
    else if(type == 'Rejected By NSM') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    } else if(type == 'Rejected By Compliance') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }
    else if(type == 'Pending Evolution By Compliance Team"') {
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }else{
      this.collection = this.pharmaEventCollection;
      this.lazyTable.setSearchString(type);
      this.lazyTable.reloadTable();
    }

    this.typeFilterOptions = type;

    this.filterTypeComponent.close();

    this.lazyTable.setCollection(this.collection);
    this.lazyTable.reloadTable();
  }
  public onChangeQueryHandler(query: Query): void {
    this.setReferenceCount();
  }
  public onFilterChangeHandler(filterData): void {
    this.lazyTable.reloadTableByFilterData(filterData);
  }

  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;

    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LuncheonmeetingsPage');
  }
  public ionViewDidEnter(): void {}

public ionViewWillEnter(): void {}

public ngAfterContentInit() {
  this.lazyTable.setTableOptions(this.tableOptions);
  this.lazyTable.setSearchString(this.searchString);
  this.lazyTable.setCollection(this.collection);
  this.lazyTable.reloadTable();
}

private setReferenceCount(): number {
  const countQuery = this.collection.getCountQuery();
  const query = this.lazyTable.getTableQueryWithConditions(countQuery);

  return this.collection.runCountQuery(query.query)
    .then((count) => {
      return this.lunchonMeetingsCount = count || 0;
    })
}
openModal() {
  // this.navCtrl.push(AddluncheonmeetingsPage,{"parentPage": this});
  this.navCtrl.push(AddluncheonmeetingsPage);
}
itemTapped(row: string) {
  console.log("totdetailspagerow",row)
  this.navCtrl.push(LuncheondetailsPage, { "row": row });

}
}
