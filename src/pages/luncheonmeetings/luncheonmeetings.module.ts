import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LuncheonmeetingsPage } from './luncheonmeetings';

@NgModule({
  declarations: [
    LuncheonmeetingsPage,
  ],
  imports: [
    IonicPageModule.forChild(LuncheonmeetingsPage),
  ],
})
export class LuncheonmeetingsPageModule {}
