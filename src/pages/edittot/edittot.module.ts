import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EdittotPage } from './edittot';

@NgModule({
  declarations: [
    EdittotPage,
  ],
  imports: [
    IonicPageModule.forChild(EdittotPage),
  ],
})
export class EdittotPageModule {}
