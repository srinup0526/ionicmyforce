import {Component, ElementRef, ViewChild} from '@angular/core';
import {
  IonicPage, NavController, NavParams, LoadingController, ModalController, AlertOptions,
  Select
} from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SurveydetailsPage } from '../surveydetails/surveydetails';
/**
* Generated class for the SurveyPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { ReferenceScheme } from './../../models/scheme/ReferenceScheme';
import { SurveyScheme } from './../../models/scheme/SurveyScheme';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { SurveyCollection } from '../../collections/SurveyCollection';
import {Query} from "../../services/common/Query";
import { Settings, SettingsImpl } from '../../services/db/Settings';

@IonicPage()
@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
})
export class SurveyPage {

  public isSearchbarOpened = false;
  public surveysCount:any = 0;

  targetNontarget:string = "tc";

  references: Array<any> = [];

  public tableOptions: LazyTableOption;
  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: SurveyCollection;
  public filterOptions: AlertOptions;
  public settings: SettingsImpl;
  Survey:any;
  SurveyBackup:any;
  rows:any;
  surveyRecords: any;

  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild('filterType') filterTypeComponent: Select;
  @ViewChild('mainContent') mainContent: ElementRef;

  constructor(public loadingCtrl: LoadingController,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public service: SmartstoreServiceProvider,
    public sureveyCollection: SurveyCollection,
    public ngxDatatablemodule: NgxDatatableModule) {
    this.settings = Settings.getInstance();

    const headerItems = [
    {
      title: 'common.names.CreatedDate',
      fields: [SurveyScheme.fields.createddate],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {},
      modelFunction:'getCreatedDateTimeInFormat'
    },
    {
      title: 'common.names.Name',
      fields: [SurveyScheme.fields.surveyname],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {}
    },
    {
      title: 'common.names.SurveyType',
      fields: [SurveyScheme.fields.surveytype],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.StartDate',
      fields: [SurveyScheme.fields.startdate],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.EndDate',
      fields: [SurveyScheme.fields.enddate],
      isSortable: true,
      isAsc: true,
      isActive: false
    }];

    this.tableOptions = {
      headerItems: headerItems,
      rowHandler: (event, reference) => {
        this.itemTapped(reference);
      },
      batchSize: 100
    };

    this.searchString = '';

    this.collection = this.sureveyCollection;
    console.log("this.collection",this.collection);
    this.filterOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };


  }

  ionViewDidLoad():void {}
  ionViewWillEnter():void{}
  itemTapped(survey:any){
    console.log("selected row information ",survey);
    this.navCtrl.push(SurveydetailsPage, { "row": survey });
  }

  public onChangeQueryHandler(query: Query): void {
    this.setSurveyCount();
  }

  public onChangeContactTypeHandler(type) {

    this.collection = this.sureveyCollection;


    this.targetNontarget = type;

    this.filterTypeComponent.close();

    this.lazyTable.setCollection(this.collection);
    console.log("this.collection",this.collection);
    this.lazyTable.reloadTable();
  }
  public onFilterChangeHandler(filterData): void {
    this.lazyTable.reloadTableByFilterData(filterData);
  }

  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;

    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable();
  }

  private setSurveyCount(): number {
    const countQuery = this.collection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);

    return this.collection.runCountQuery(query.query)
    .then((count) => {
      return this.surveysCount = count || 0;
    })
  }
}
