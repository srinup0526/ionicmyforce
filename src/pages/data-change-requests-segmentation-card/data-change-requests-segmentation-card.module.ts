import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataChangeRequestsSegmentationCardPage } from './data-change-requests-segmentation-card';

@NgModule({
  declarations: [
    DataChangeRequestsSegmentationCardPage,
  ],
  imports: [
    IonicPageModule.forChild(DataChangeRequestsSegmentationCardPage),
  ],
})
export class DataChangeRequestsSegmentationCardPageModule {}
