import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Settings, SettingsImpl } from '../../services/db/Settings';
import {DataChangeRequest} from "../../models/DataChangeRequest";
import {Loader} from "../../services/common/Loader";
import {ConfirmPopup} from "../../components/popups/ConfirmPopup";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {DataChangeRequestsCollection} from "../../collections/DataChangeRequestsCollection";
import {ProductsCollection} from "../../collections/ProductsCollection";
import {Contact} from "../../models/Contact";
import {ContactdetailsPage} from "../contactdetails/contactdetails";
import {ContactsCollection} from "../../collections/ContactsCollection";
import {ProductSegmentationPickListManager} from "../../services/db/picklist-managers/ProductSegmentationPickListManager";
import {ProductSegmentationScheme} from "../../models/scheme/ProductSegmentationScheme";


@IonicPage()
@Component({
  selector: 'page-data-change-requests-segmentation-card',
  templateUrl: 'data-change-requests-segmentation-card.html',
})
export class DataChangeRequestsSegmentationCardPage {
  public isDeleteBtnEnable: boolean;
  public dcr: DataChangeRequest;
  public contact: Contact;
  
  public productValues: {[key: string]: string};
  public segmentationFirstValues: {[key: string]: string};
  public segmentationSecondValues: {[key: string]: string};

  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private loader: Loader,
              private alertCtrl: AlertController,
              private localizationManager: LocalizationManager,
              private contactsCollection: ContactsCollection,
              private productsCollection: ProductsCollection,
              private productSegmentationPickListManager: ProductSegmentationPickListManager,
              private dataChangeRequestsCollection: DataChangeRequestsCollection) {
    
    this.isDeleteBtnEnable = false;
    this.dcr = new DataChangeRequest({});
    this.contact = new Contact({});
    
    this.productValues = {};
    this.segmentationFirstValues = {};
    this.segmentationSecondValues = {};
  }
  
  public onTapDeleteBtnHandler() {
    this.showDeleteConfirmPopup()
      .then((result) => {
        if(result.doAction) {
          return this.deleteDcr()
            .then(() => this.goBack());
        }
      })
  }
  
  public onTapContactLinkHandler() {
    this.gotoContactDetail();
  }
  
  public ionViewDidLoad() {
    this.initParams();
  }
  
  public initParams() {
    this.dcr = this.navParams.data['row'];
    
    return this.loader.run(this.init.bind(this));
  }
  
  private init() {
    this.initDeleteBtn();
    
    return this.fetchContact()
      .then(() => {
          return this.fetchAndSetProducts()
            .then(() => this.fetchPicklists())
      })
  }
  
  private initDeleteBtn() {
    this.isDeleteBtnEnable = this.dcr.isDraftStatus();
  }
  
  private fetchContact() {
    return this.contactsCollection.getContactByAccountId(this.dcr.dcrContactId)
      .then(this.setContact.bind(this))
  }
  
  private setContact(contact) {
    this.contact = contact;
  }
  
  private gotoContactDetail() {
    this.navCtrl.push(ContactdetailsPage, { "row": this.dcr.dcrContactId });
  }
  
  private showDeleteConfirmPopup(): Promise<{ doAction: boolean }> {
    const confirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);
    
    return confirmPopup.showPopup({
      title: 'card.DCR.ConfirmationPopup.DeleteItem.Caption',
      message: 'card.DCR.ConfirmationPopup.DeleteItem.Message',
      yesLabel: 'common.buttons.YesBtn',
      noLabel: 'common.buttons.NoBtn'
    });
  }
  
  private deleteDcr(): Promise<any> {
    return this.dataChangeRequestsCollection.removeEntity(this.dcr);
  }
  
  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }
  
  private fetchAndSetProducts(): Promise<any> {
    return this.productsCollection.fetchAll()
      .then((response) => this.productsCollection.getAllEntitiesFromResponse(response))
      .then((products) => products.reduce((all, product) => {
        all[product.id] =  product.name;
        return all;
      }, {}))
      .then((result) => {
        this.productValues = result;
      })
  }
  
  private fetchPicklists(): Promise<any> {
    return this.productSegmentationPickListManager.getPickLists()
      .then((picklist) => {
        this.segmentationFirstValues = this.convertPicklist(picklist[ProductSegmentationScheme.fields.segmentation1.sfdc] || []);
        this.segmentationSecondValues = this.convertPicklist(picklist[ProductSegmentationScheme.fields.segmentation2.sfdc] || []);
      })
  }
  
  private convertPicklist(sfPicklist: Array<{value: string, label: string}>): {[key: string]: string}{
    return sfPicklist.reduce((all, product) => {
      all[product.value] =  product.label;
      return all;
    }, {})
  }
}
