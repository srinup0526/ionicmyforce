import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {DataChangeRequest} from "../../models/DataChangeRequest";
import {Loader} from "../../services/common/Loader";
import {ConfirmPopup} from "../../components/popups/ConfirmPopup";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {DataChangeRequestsCollection} from "../../collections/DataChangeRequestsCollection";
import {DataChangeRequestPickListManager} from "../../services/db/picklist-managers/DataChangeRequestPickListManager";
import {Organization} from "../../models/Organization";
import {Contact} from "../../models/Contact";
import {OrganizationdetailsPage} from "../organizationdetails/organizationdetails";
import {ContactsCollection} from "../../collections/ContactsCollection";
import {OrganizationsCollection} from "../../collections/OrganizationsCollection";
import {ContactdetailsPage} from "../contactdetails/contactdetails";


@IonicPage()
@Component({
  selector: 'page-data-change-requests-reference-card',
  templateUrl: 'data-change-requests-reference-card.html',
})
export class DataChangeRequestsReferenceCardPage {
  public isDeleteBtnEnable: boolean;
  public dcr: DataChangeRequest;
  public organization: Organization;
  public contact: Contact;
  public statusPicklistValue;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dataChangeRequestsCollection: DataChangeRequestsCollection,
              private loader: Loader,
              private alertCtrl: AlertController,
              private localizationManager: LocalizationManager,
              private contactsCollection: ContactsCollection,
              private organizationsCollection: OrganizationsCollection,
              private dataChangeRequestPickListManager: DataChangeRequestPickListManager) {
  
    this.isDeleteBtnEnable = false;
    this.statusPicklistValue = '';
    
    this.dcr = new DataChangeRequest({});
    this.contact = new Contact({});
    this.organization = new Organization({});
    
  }
  
  public onTapDeleteBtnHandler() {
    this.showDeleteConfirmPopup()
      .then((result) => {
        if (result.doAction) {
          return this.deleteDcr()
            .then(() => this.goBack());
        }
      })
  }
  
  public onTapContactLinkHandler() {
    this.gotoContactDetail();
  }
  
  public onTapOrganizationLinkHandler() {
    this.gotoOrganizationDetail();
  }
  
  public ionViewDidLoad() {
    this.initParams();
  }
  
  public initParams() {
    this.dcr = this.navParams.data['row'];
    
    return this.loader.run(this.init.bind(this));
  }
  
  private init() {
    this.initDeleteBtn();

    return this.fetchPicklists()
      .then(this.setPicklist.bind(this))
      .then(this.fetchContacts.bind(this))
      .then(this.setContacts.bind(this))
      .then(this.fetchOrganization.bind(this))
      .then(this.setOrganization.bind(this));
  }
  
  private fetchPicklists() {
    return this.dataChangeRequestPickListManager.getStatusLabelByValue(this.dcr.dcrReferenceCStatus);
  }
  
  private fetchContacts() {
    return this.contactsCollection.getContactByAccountId(this.dcr.dcrContactId);
  }
  
  private fetchOrganization() {
    return this.organizationsCollection.fetchEntityById(this.dcr.dcrOrganizationId);
  }
  
  private setContacts(contact) {
    this.contact = contact;
  }
  
  private setOrganization(organization) {
    this.organization = organization;
  }
  
  private setPicklist(picklistValue) {
    this.statusPicklistValue = picklistValue;
  }
  
  private initDeleteBtn() {
    this.isDeleteBtnEnable = this.dcr.isDraftStatus();
  }
  
  private gotoOrganizationDetail() {
    this.navCtrl.push(OrganizationdetailsPage, { "id": this.dcr.dcrOrganizationId });
  }
  
  private gotoContactDetail() {
    this.navCtrl.push(ContactdetailsPage, { "id": this.dcr.dcrContactId });
  }
  
  private showDeleteConfirmPopup(): Promise<{ doAction: boolean }> {
    const confirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);
    
    return confirmPopup.showPopup({
      title: 'card.DCR.ConfirmationPopup.DeleteItem.Caption',
      message: 'card.DCR.ConfirmationPopup.DeleteItem.Message',
      yesLabel: 'common.buttons.YesBtn',
      noLabel: 'common.buttons.NoBtn'
    });
  }
  
  private deleteDcr(): Promise<any> {
    return this.dataChangeRequestsCollection.removeEntity(this.dcr);
  }
  
  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }
}
