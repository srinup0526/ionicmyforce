import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataChangeRequestsReferenceCardPage } from './data-change-requests-reference-card';

@NgModule({
  declarations: [
    DataChangeRequestsReferenceCardPage,
  ],
  imports: [
    IonicPageModule.forChild(DataChangeRequestsReferenceCardPage),
  ],
})
export class DataChangeRequestsReferenceCardPageModule {}
