import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndiaAppointmentDetailsPage } from './india-appointment-details';

@NgModule({
  declarations: [
    IndiaAppointmentDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(IndiaAppointmentDetailsPage),
  ],
})
export class IndiaAppointmentDetailsPageModule {}
