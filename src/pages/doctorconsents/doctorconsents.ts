import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { DoctorConsentCollection } from "../../collections/DoctorConsentCollection";
import { OrganizationsCollection } from '../../collections/OrganizationsCollection';
import { ReferenceCollection } from '../../collections/references/ReferenceCollection';
import { DoctorsConsentScheme } from "../../models/scheme/DoctorsConsentScheme";
import { DoctorsConsent } from '../../models/DoctorsConsent';
//import { Query } from "../../services/common/Query";
import { SignaturePage } from "../signature/signature";
import {ReferenceSelectListPage} from "../reference-select-list/reference-select-list";
import {OrganizationListSelectPage} from "../organization-list-select/organization-list-select";
import {Organization} from "../../models/Organization";
import {Contact} from "../../models/Contact";
import {ContactsCollection} from "../../collections/ContactsCollection";
import { BricksCollection } from "../../collections/BricksCollection";
import {BrickPickListDatasource} from "../../services/db/picklist-managers/datasource/BrickPicklistDatasource";
//import {OrderDetail} from "../order-management/OrderDetail";
import {Loader} from "../../services/common/Loader";
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { DISABLED } from '@angular/forms/src/model';

/**
 * Generated class for the DoctorconsentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-doctorconsents',
  templateUrl: 'doctorconsents.html',
})
export class DoctorconsentsPage {
  //public doctorConsentCollection: DoctorConsentCollection;
  public searchString : string = "";
  public settings: SettingsImpl;
  public customers:any;
  public contact: Contact;
  public organization1: Organization;
  public bcollection: BricksCollection;
  public tableOptions: LazyTableOption;
  

  organizationlist:any;
  consent: DoctorsConsent;
  message = "";
  firstname:string="";
  lastname:string="";
  email:string="";
  cellphonenumber:string="";
  doctorid:string="";
  organization:any="";
  organizationid:any="";
  signature:string="";
  speciality:string="";
  brick:string="";
  reference:any;
  DoctorsConsent:any={};//First_Name__c, Last_Name__c, E_mail_address__c, Cell_phone_number__c, Doctor_ID_code__c, Name_of_organization__c, Organisation_ID__c, Signature__c, Specialty__c, Brick_Name__c, Non_Organisation__c, Country__c
  doctorConsent:any;

  private selectedAttendeesIds: Array<string> = [];
  public selectedAttendeesStringList : string = "";
  protected dataChanged() {
    this.isChanged = true;
  }
  protected isChanged: boolean;
  private setSignature(dataUrl: string) {
    this.consent.signature = dataUrl;
    this.dataChanged();
  }

  // private setAttandeesListToView() {
  //   if(this.selectedAttendeesIds.length) {
  //     return this.loader.run(() => {
  //       return this.contactsCollection.fetchForContactIds(this.selectedAttendeesIds)
  //         .then((attendees) => {
  //           this.selectedAttendeesStringList = attendees
  //             .map((attendee) => attendee.fullName())
  //             .join(', ');
  //             console.log("Selected Customer: "+this.selectedAttendeesStringList);
  //             console.log("Customer List", attendees);
  //             console.log("Organization ID", attendees[0].organizationSfId);
  //             console.log("Speciality", attendees[0].specialty);
  //             this.organizationid = attendees[0].organizationSfId;
  //             this.speciality = attendees[0].specialty;
  //         })
  //     })
  //   }
  //   this.selectedAttendeesStringList = '';
  // }
  constructor( 
    public navCtrl: NavController,
    public navParams: NavParams,
    public doctorConsentCollection : DoctorConsentCollection,
    public organizationsCollection : OrganizationsCollection,
    public referenceCollection:ReferenceCollection,
    private contactsCollection : ContactsCollection,
    private loader : Loader,
    public toastCtrl: ToastController,
    private brickPickListDatasource: BrickPickListDatasource,
    private bricksCollection: BricksCollection
    ) {
    this.reference = this.reference || {};
    this.bcollection= this.bricksCollection;
    this.settings = Settings.getInstance();
    this.organization1 = new Organization({});
    this.consent = new DoctorsConsent({});
    this.referenceCollection.fetchAll()
    .then(records=>{ return this.referenceCollection.getAllEntitiesFromResponse(records);})
    .then(res=>{
      console.log("res",res);
      this.customers = res;
      console.log("Customer list",this.customers);
    })
    this.organizationsCollection.fetchAll()
    .then(records=>{ return this.organizationsCollection.getAllEntitiesFromResponse(records);})
    .then(res=>{
      console.log("organization res",res);
      this.organizationlist = res;
    })
    
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad DoctorconsentsPage');
  }

  saveDoctorConsents(){
    if(this.navParams.data['row']) {
      this.doctorConsent = this.navParams.data['row'];
      this.doctorConsent.firstname = this.firstname;
      this.doctorConsent.lastname =  this.lastname;
      this.doctorConsent.email=  this.email;
      this.doctorConsent.cellphonenumber= this.cellphonenumber;
      this.doctorConsent.doctorid = this.doctorid;
      this.doctorConsent.organization = this.organization;
      this.doctorConsent.organizationid =  this.organizationid;
      this.doctorConsent.signature =  this.signature;
      this.doctorConsent.speciality =  this.speciality;
      this.doctorConsent.brick =  this.brick;
      console.log("Doctor Consent data<><><><>",this.doctorConsent);
      // this.doctorConsentCollection.updateEntity(this.consent)
      // .then(createdData=>{
      //   console.log("createdData",createdData);
      //   let success = (data) => {
      //     console.log("Data Upserted successfully and created data is",data)
      //     this.navCtrl.pop();
      //     const toast = this.toastCtrl.create({
      //       message: "Doctors Consent Updated Successfully!",
      //       showCloseButton:true,
      //       cssClass:"toastClass",
      //       position:'middle',
      //       duration:3000,
      //     });
      //     toast.present();
      //   }
      //   success(createdData);
      // }) 
    } 
    else {
      let consent = new DoctorsConsent({
        createdOffline: true,
        firstname : this.firstname,
        lastname :  this.lastname,
        email: this.email,
        cellphonenumber: this.cellphonenumber,
        doctorid : this.doctorid,
        organization :  this.organization,
        organizationid :  this.organizationid,
        signature : this.signature,
        speciality :  this.speciality,
        brick : this.brick,
        isSubmittedForApproval: false,
        existingCustomerName:this.reference?this.reference.name:''
      });
      this.doctorConsentCollection.createEntity(consent)
      .then(createdData=>
        this.doctorConsentCollection.parseModel(createdData))
        .then(createdData=>{
          console.log("createdData",createdData);
        let success = (data) => {
          console.log("Doctors Consent successfully and created data is",data);
          this.navCtrl.pop();
          const toast = this.toastCtrl.create({
            message: "Doctors Consent Created Successfully!",
            showCloseButton:true,
            cssClass:"toastClass",
            position:'bottom',
            duration:3000,
          });
          toast.present();
        }
        success(createdData);
        })
        
    }
 }


  validateInput()
  {
    var isValid = true;
    this.message = "";
    console.log("check validation",this.firstname , this.lastname , this.speciality, this.organization);
    // if(this.firstname) {
    //    if(!this.lastname) {
    //      isValid = false;
    //      this.message+="First Name is Mandatory";
    //    }
    //  } else {
    //   if((this.firstname == "None" || this.firstname == "") && (this.lastname == "None" || this.lastname == "") && (this.speciality == "None" || this.speciality == "")) {
    //     isValid = false;
    //     this.message+="Mandatory";
    //   }
    //  }
    //  if(!this.reference && !this.organization1 && !this.firstname && !this.lastname)
    //  {
    //   isValid = false;
    //   this.message+=" Please Select/Enter Customer Name";
    //  }
    //  else if(this.reference && this.organization1 && this.firstname && this.lastname)
    //  {
    //   isValid = false;
    //   this.message+=" Please Enter only New Customer or Choose Existing Customer";
    //  }
     if(this.speciality == "" || this.speciality == null) {
      isValid = false;
      this.message+="Speciality is Mandatory";
     }
    if(!this.reference.name){
      if(this.firstname == "" || this.firstname == null) {
        isValid = false;
        this.message+=" First Name is Mandatory  ";
       }
      if(this.lastname == "" || this.lastname == null) {
        isValid = false;
        this.message+="Last Name is Mandatory  ";
      }
      if(this.organization == "" || this.organization == null){
        isValid = false;
        this.message+="Organization is Mandatory  ";
      }
      if(this.speciality == "" || this.speciality == null) {
        isValid = false;
        this.message+="Speciality is Mandatory  ";
      }
    }
    else if(this.reference.name && this.firstname && this.lastname &&  this.organization){
      isValid = false;
      this.message+="  Please Enter only New Customer or Choose Existing Customer";
    }
    return isValid;  
  }


  validateInputData()
  {
    console.log("message",this.message); 
    if(this.validateInput())
    {
      this.saveDoctorConsents();
    }
    else
    {
      this.message = this.message.startsWith('\n')?this.message.substr(2):this.message;
      console.log("this.message",this.message);
      const toast = this.toastCtrl.create({
        message: this.message,
        showCloseButton:true,
        cssClass:"toastClass",
        duration:7000,
        position:'bottom',
      });
      toast.present();
    }
  }

  public onTapSignatureHandler() {
    this.navCtrl.push(SignaturePage, {callback: this.setSignature.bind(this)})
  }
  public setImageSrc(signatureImg: string): string {
    return 'data:image/jpeg;base64,' + signatureImg;
  }
  public onTapAttendeeHandler() {		
    this.gotoAttendeePage();		
  }
  private gotoAttendeePage() {
    this.navCtrl.push(ReferenceSelectListPage, {
      contactId: this.reference,
      callback: this.setCustomer.bind(this)
    })
  }
  private setCustomer(reference) {
    console.log("attendeesIds",reference);
    this.reference = reference;
    this.organizationid = reference.organizationSfId;
    this.speciality = reference.specialty;
    this.organizationsCollection.fetchEntityById(reference.organizationSfId)
    .then(organization=>{
      this.organization1 = organization;
    })
    //this.setAttandeesListToView();
  }
  private gotoAccountPage() {
    this.navCtrl.push(OrganizationListSelectPage, {
      //organizationId: this.contact.organizationSfId,
      callback: this.setAccount.bind(this)
    })
  }
  public onTapAccountBtnHandler() {
    this.gotoAccountPage();
  }
  private setAccount(organization) {
    console.log("organization",organization);
    this.organization1 = organization;
    //this.contact.organizationSfId = this.organization.id;
  }
  isReadonly() {
    return this.isReadonly;   //return true/false 
  }
  onChangeHandler(value, event) {
    console.log(value);
    if(value == "yes"){
      //alert('yes is working');
    }else{
      alert('Please select yes');
    }
  }
}
