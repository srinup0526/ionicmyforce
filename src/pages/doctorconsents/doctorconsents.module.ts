import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorconsentsPage } from './doctorconsents';

@NgModule({
  declarations: [
    DoctorconsentsPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorconsentsPage),
  ],
})
export class DoctorconsentsPageModule {}
