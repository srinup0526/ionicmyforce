import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';

declare var JPEGEncoder: any;


@IonicPage()
@Component({
  selector: 'page-signature',
  templateUrl: 'signature.html',
})
export class SignaturePage {
  public isSaveBtnEnabled: boolean;

  private signaturePadOptions: any;
  private callback: any;

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private elementRef: ElementRef) {

    this.signaturePadOptions= {};
    this.disableSaveBtn();
  }

  public onTapSaveBtnHandler(): void {
    this.saveSignature();
  }

  public onDrawCompleteHandler(): void {
    this.enableSaveBtn();
  }

  public onTapClearBtnHandler(): void {
    this.disableSaveBtn();
    this.clearDraw()
  }

  ionViewDidEnter() {
    const elementParams = this.elementRef.nativeElement.getBoundingClientRect();

    this.callback = this.navParams.data['callback'];

    this.signaturePad.clear();

    this.signaturePadOptions = {
      'minWidth': 5,
      'canvasWidth': elementParams.width,
      'canvasHeight': elementParams.height
    };
    this.signaturePad.resizeCanvas();
  }

  private clearDraw(): void {
    this.signaturePad.clear();
  }

  private saveSignature(): Promise<any> {
    return this.navCtrl.pop()
      .then(() => {
         let base64 = this.getDataUrl();

         return this.resizeSignature(base64)
           .then((imgData) => {
             return this.callback(imgData);
           })
      })
  }

  private getDataUrl(): string {
    return this.signaturePad.toDataURL("image/png");
  }

  private resizeSignature(signatureBase64) {
    return new Promise((resolve, reject) => {
      const scaledWidth = 400;
      const scaledHeight = 200;

      const canvas = document.createElement('canvas');
      canvas.width = scaledWidth;
      canvas.height = scaledHeight;

      const context = canvas.getContext('2d');

      const image = new Image();

      image.onload = () => {
        context.fillStyle = 'white';
        context.fillRect(0, 0, scaledWidth, scaledHeight);
        context.drawImage(image, 0, 0, scaledWidth, scaledHeight);

        const encoder = new JPEGEncoder();

        let imageData = encoder.encode(context.getImageData(0, 0, canvas.width, canvas.height));

        imageData = imageData.replace('data:image/jpeg;base64,', '');

        resolve(imageData);
      };

      image.src = signatureBase64;
    });
  }

  private disableSaveBtn() {
    this.isSaveBtnEnabled = false;
  }

  private enableSaveBtn() {
    this.isSaveBtnEnabled = true;
  }

}
