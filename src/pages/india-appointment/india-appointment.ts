import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController, ToastController, Navbar } from 'ionic-angular';
import { CallReport } from '../../models/CallReport';
import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { UsersCollection } from '../../collections/UsersCollection';
import { CallReportsCollection } from '../../collections/CallReportsCollection/CallReportsCollection';
import { CallReportCoachingVisitPickListDatasource } from '../../services/db/picklist-managers/datasource/CallReportCoachingVisitPickListDatasource';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import moment from 'moment';
import { Utils } from '../../utils/Utils';
import { SingleUserListPage } from "../single-user-list/single-user-list";
import {UserListMultiSelectPage} from "../user-list-multi-select/user-list-multi-select";
import {Loader} from "../../services/common/Loader";
import { PatchdetailsPage } from '../patchdetails/patchdetails'







// import { FormBuilder, FormGroup, Validators } from '@angular/forms';

/**
* Generated class for the AppointmentPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-india-appointment',
  templateUrl: 'india-appointment.html',
})
export class IndiaAppointmentPage {
  CoachingVisitUsers:any;
  loggedInUserDetails:any;
  RecordTypeId:string = "";
  JointVisitParticipantUsers:any = [];
  message:string = "";
  duration:any;
  patchCustomer:any = [];
  coachingPicklistOptions:any = [];
  config:any;
  durationPickListOptions:any = [];
  callObjective:any = "";
  callReport:CallReport;
  @ViewChild('navbar') navBar: Navbar;

  public selectedUserSingleStringList : string = "";
  private selectedUserSingleIds: any;

  public selectedUserMultiStringList : string = "";
  private selectedUserMultiIds: Array<string> = [];

  constructor(public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public usersCollection:UsersCollection,
    public callReportsCollection:CallReportsCollection,
    private callReportCoachingVisitPickListDatasource: CallReportCoachingVisitPickListDatasource,
    private loader : Loader) {
    this.callReport = new CallReport({});
    this.patchCustomer = this.navParams.data["row"];
    console.log("patchcustomer",this.patchCustomer);
    this.callReport.coachingVisit="";
    this.callReport.dateTimePlanned = new Date().toISOString();
    SforceDataContext.getActiveUser().then(data => { this.loggedInUserDetails = data; console.log("UserData",this.loggedInUserDetails); });
    this.callReportCoachingVisitPickListDatasource.getItems()
    .then((picklistItems) => {
      this.coachingPicklistOptions = picklistItems;
    });
    ConfigurationManager.getConfig().then(config => { this.config = config; 
      this.durationPickListOptions = config.tourPlanningSettings?config.tourPlanningSettings.duration.split(","):"";
      console.log("config",this.config); 
      this.RecordTypeId = config?config.indiaAppointmentRecordTypeId:'';
    });
    this.loadAllUsers();
  }
  ionViewDidLoad():void {}

  ionViewWillEnter():void {}

  getCoachingBasedUsers(coachingVisist : string){}

  validateInputData()
  {
    console.log("message",this.message);
    console.log("this.validateInput",this.validateInput());
    if(this.validateInput())
    {
      this.saveAppointment();
    }
    else
    {
      const toast = this.toastCtrl.create({
        message: this.message,
        showCloseButton:true,
        cssClass:"toastClass",
        duration:3000,
      });
      toast.present();
    }

  }

  public onTapUserHandler() {		
    this.gotoUserPage();		
  }

  public onTapMultiUserHandler() {		
    this.gotoMultiUserPage();		
  }

  private gotoUserPage() {
    this.navCtrl.push(SingleUserListPage, {
      "coaching-user-id": this.selectedUserSingleIds,
      callback: this.setUser.bind(this)
    })
  }

  private gotoMultiUserPage() {
    this.navCtrl.push(UserListMultiSelectPage, {
      ids: this.selectedUserMultiIds,
      callback: this.setMultiUser.bind(this)
    })
  }

  private setMultiUser(userIds) {
    this.selectedUserMultiIds = userIds;
    this.selectedUserMultiStringList = this.selectedUserMultiIds.join('; ');

    this.setUserMultiListToView();
  }

  private setUserMultiListToView() {
    if(this.selectedUserMultiIds.length) {
      return this.loader.run(() => {
        return this.usersCollection.fetchForUserIds(this.selectedUserMultiIds)
        .then((users) => {
          this.selectedUserMultiStringList = users
          .map((user) => user.fullName())
          .join('; ');
        })
      })
    }

    this.selectedUserMultiStringList = '';
  }

  private setUser(userIds) {
    console.log("userIds",userIds);
    this.selectedUserSingleIds = userIds;
    this.selectedUserSingleStringList = userIds.fullName();
    //this.selectedUserSingleStringList = this.selectedUserSingleIds.join(', ');

    //this.setUserListToView();
  }

  private setUserListToView() {
    if(this.selectedUserSingleIds.length) {
      return this.loader.run(() => {
        return this.usersCollection.fetchForUserIds(this.selectedUserSingleIds)
        .then((users) => {
          this.selectedUserSingleStringList = users
          .map((user) => user.fullName())
          .join('; ');
        })
      })
    }

    this.selectedUserSingleStringList = '';
  }

  validateInput()
  {
    var isValid = true;
    this.message = "";
    if(this.callReport.coachingVisit && !this.selectedUserSingleStringList)
    {
      isValid = false;
      this.message+="\n Coaching Visit User Mandatory";
    }
    if(!this.callReport.dateTimePlanned)
    {
      isValid = false;
      this.message+="\n Date Time Planned Can not be empty ";
    }
    if(new Date(this.callReport.dateTimePlanned) <= new Date())
    {
      isValid = false;
      this.message+="\n Date Time Planned Value cannot be less than the current Date ";
    }
    return isValid;  
  }

  loadAllUsers():Promise<void> {
    return this.usersCollection.fetchAll()
    .then(res=>{ return this.usersCollection.getAllEntitiesFromResponse(res);})
    .then(users => {
      console.log('Users',users);
      this.CoachingVisitUsers = users;
      this.JointVisitParticipantUsers = users.filter((user) => {
        return (user.currency == this.loggedInUserDetails.currency);
      });
      console.log("this.JointVisitParticipantUsers",this.JointVisitParticipantUsers);
    })
  }

  saveAppointment(){
    console.log("new Date(this.dateTimePlanned)",new Date(this.callReport.dateTimePlanned));
    let coachingUserID = "";
    if(this.selectedUserSingleIds)
      coachingUserID = this.selectedUserSingleIds.id;

    const appointment = new CallReport({
      createdOffline:true,
      dateTimePlanned: Utils.originalDateTime(new Date(this.callReport.dateTimePlanned)),
      organizationSfId:this.patchCustomer.accountId,
      remoteOrganizationName:this.patchCustomer.accountName,
      organizationName:this.patchCustomer.accountName,
      indCustomer:this.patchCustomer.id,
      contactFirstName:this.patchCustomer.contactName,
      userLastName:this.loggedInUserDetails.lastName,
      userSfid:this.loggedInUserDetails.id,
      duration:this.patchCustomer.duration,
      type:CallReportScheme.TYPE_APPOINTMENT,
      recordTypeId:this.RecordTypeId,
      coachingVisit:this.callReport.coachingVisit,
      status:null,
      coachingVisitUserSfid:coachingUserID,
      jointVisitParticipants:this.selectedUserMultiStringList,
      callObjective:this.patchCustomer.callObjective,
      contactSpecialty:this.patchCustomer.specialty,
      attributes:{type: CallReportScheme.table}
    })
    console.log("appointment",appointment);
    this.callReportsCollection.createEntity(appointment)
    .then(res=>{
      console.log("res",res);
      const toast = this.toastCtrl.create({
        message: "Appointment Saved Successfully!",
        showCloseButton:true,
        cssClass:"toastClass",
        duration:3000,
      });
      toast.present();
      this.navCtrl.pop();
    })
  }
  navigateToOrganisationPage(reference:any){
    console.log("reference",reference);
    this.navCtrl.push(PatchdetailsPage, { "row": reference });
  }

  navigateToContactPage(reference:any){
    console.log("reference",reference);
    this.navCtrl.push(PatchdetailsPage, { "row": reference });
  }

  public maxDate() {
    const afterDays = 90;

    return moment().add(afterDays, 'days').format("YYYY-MM-DD");
  }

  public minDate() {
    const beforeDays = 10;

    return moment().subtract(beforeDays, 'days').format("YYYY-MM-DD");
  }
}