import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndiaAppointmentPage } from './india-appointment';

@NgModule({
  declarations: [
    IndiaAppointmentPage,
  ],
  imports: [
    IonicPageModule.forChild(IndiaAppointmentPage),
  ],
})
export class IndiaAppointmentPageModule {}
