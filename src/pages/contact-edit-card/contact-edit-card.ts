import {Component, ViewChild, ElementRef} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertOptions, Select, ToastController, AlertController, Platform} from 'ionic-angular';
import {Contact} from "../../models/Contact";
import {Settings, SettingsImpl} from './../../services/db/Settings';
import {OrganizationListSelectPage} from "../organization-list-select/organization-list-select";
import {Organization} from "../../models/Organization";
import {ContactKolPicklistDatasource} from "../../services/db/picklist-managers/datasource/ContactKolPicklistDatasource";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {DataChangeRequestHelper} from "../data-change-requests/DataChangeRequestHelper";
import {BackHandlers} from "../../utils/BackHandlers";
import {Loader} from "../../services/common/Loader";
import {ContactScheme} from "../../models/scheme/ContactScheme";
import {OrganizationsCollection} from "../../collections/OrganizationsCollection";

@IonicPage()
@Component({
  selector: 'page-contact-edit-card',
  templateUrl: 'contact-edit-card.html',
})
export class ContactEditCardPage extends BackHandlers {
  
  public settings: SettingsImpl;
  public contact: Contact;
  public contactStartBackup: Contact;
  public organization: Organization;
  public kols: Array<any>;
  public filterOptions: AlertOptions;
  
  @ViewChild('description') descriptionArea;
  @ViewChild('organizationAddress') organizationAddressArea;
  @ViewChild('organizationName') organizationNameArea;
  
  @ViewChild('kolSelect') kolSelect: Select;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private dataChangeRequestHelper: DataChangeRequestHelper,
              private contactKolPicklistDatasource: ContactKolPicklistDatasource,
              private organizationsCollection: OrganizationsCollection,
              localizationManager: LocalizationManager,
              loader: Loader,
              alertCtrl: AlertController,
              platform: Platform) {
  
    super(navCtrl, localizationManager, loader, alertCtrl, platform);
    
    this.contact = new Contact({});
    this.contactStartBackup = new Contact({});
    
    this.organization = new Organization({});
    this.settings = Settings.getInstance();
    
    this.filterOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };
  }
  
  public onTapSaveBtnHandler() {
    this.save();
  }
  
  public onChangeKolHandler(kol) {
    this.contact.kol = kol.id;
    
    this.kolSelect.close();
  }
  
  public onTapAccountBtnHandler() {
    this.gotoAccountPage();
  }
  
  public ionViewDidLoad() {
    this.contact = this.navParams.data['row'];
  
    this.loader.run(this.initData.bind(this))
      .then(() => {
        return this.setStartBackup();
      });
  
    super.ionViewDidLoad();
  }
  
  public isShowContactDescriptionEnable(): boolean {
    return !!this.settings.showContactDescription || !!this.contact.description;
  }
  
  protected validate(): Promise<any> {
    return this.localizationManager.getSeveralLocales([
      'common.alerts.NoRequiredFields',
      'common.names.Organization',
      'common.names.PhysicianFirstName',
      'common.names.PhysicianLastName'
    ])
      .then((localization) => {
        return new Promise((resolve, reject) => {
          const toastHeader = `${localization['common.alerts.NoRequiredFields']} \n`;
          let toastBody = '';
          
          if (!this.contact.organizationSfId) {
            toastBody += ` - ${localization['common.names.Organization']}\n`;
          }
          
          if (!this.contact.firstName) {
            toastBody += ` - ${localization['common.names.PhysicianFirstName']}\n`;
          }
          
          if (!this.contact.lastName) {
            toastBody += ` - ${localization['common.names.PhysicianLastName']}\n`;
          }
          
          const isDataValid = toastBody.length == 0;
          
          if (isDataValid) {
            return resolve();
          }
          
          return reject(new Error(toastHeader + toastBody));
        });
      });
  }
  
  protected validateAndSaveOrder(): Promise<any> {
    return this.save();
  }
  
  protected hasChanges() {
    return this.isSameFillFields(this.contact, this.contactStartBackup, ContactScheme.fields);
  }
  
  private save(): Promise<any> {
    return this.validate()
      .then(() => this.doSave())
      .then(() => this.goBack())
      .catch((error: Error) => {
        return this.showValidateToast(error.message);
      })
  }
  
  private doSave() {
    return this.dataChangeRequestHelper.createContact(this.contact, this.organization);
  }
  
  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton: true,
      cssClass: "toastClass",
      duration: 5000,
    });
  
    return toast.present();
  }
  
  private gotoAccountPage() {
    this.navCtrl.push(OrganizationListSelectPage, {
      organizationId: this.contact.organizationSfId,
      callback: this.setAccount.bind(this)
    })
  }
  
  private setAccount(organization) {
    this.organization = organization;
    this.contact.organizationSfId = this.organization.id;
  }
  
  private initData() {
    return Promise.all([
      this.loadPicklist(),
      this.fetchOrganization()
    ])
  }
  
  private fetchOrganization(): Promise<any> {
    if(!this.contact.id) {
      return Promise.resolve();
    }
    
    return this.contact.getOrganizationByReference()
      .then((organization) => {
        if(organization) {
         this.organization = organization;
        }
      })
  }
  
  private loadPicklist() {
    return this.contactKolPicklistDatasource.getItems()
      .then((items) => {
        this.kols = items;
        
        if(!this.contact.kol) {
          this.contact.kol = items[0].id;
        }
      })
  }
  
  private setStartBackup() {
    this.contactStartBackup = new Contact(this.contact);
  }
}
