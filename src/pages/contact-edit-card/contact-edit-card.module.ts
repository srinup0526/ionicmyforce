import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactEditCardPage } from './contact-edit-card';

@NgModule({
  declarations: [
    ContactEditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactEditCardPage),
  ],
})
export class ContactEditCardPageModule {}
