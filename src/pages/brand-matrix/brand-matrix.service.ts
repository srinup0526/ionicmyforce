import {Injectable} from '@angular/core';
import {PresentationsCollection} from "../../collections/PresentationsCollection";
import {Presentation} from "../../models/Presentation";
import {PresentationFileManager} from "../media/PresentationFileManager";
import {PresentationViewer} from "../media/PresentationViewer";
import {Alert, AlertController, AlertOptions} from "ionic-angular";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {PresentationLoader} from "../media/PresentationLoader";
import {PresentationsLoadManager} from "../media/PresentationLoaderManager";
import {ProductPresentation} from "../../models/ProductPresentation";
import {ProductPresentationsCollection} from "../../collections/ProductPresentationsCollection";
import {BrandPlanning} from "../../models/BrandPlanning";
import {ListPopup} from "../../components/popups/ListPopup";
import {PresentationScheme} from "../../models/scheme/PresentationScheme";

declare let _;

@Injectable({
  providedIn: 'root'
})
export class BrandMatrixService {
  private _presentationList: Presentation[];
  private _productPresentationList: ProductPresentation[];
  private _uniqProductPresentations: ProductPresentation[];
  private _kpi: any;
  private activeProductId: string;
  private activeFocusBrandId: string;

  constructor(private presentationsCollection: PresentationsCollection,
              private productPresentationsCollection: ProductPresentationsCollection,
              private presentationFileManager: PresentationFileManager,
              private presentationViewer: PresentationViewer,
              private presentationsLoadManager: PresentationsLoadManager,
              private localizationManager: LocalizationManager,
              private alertCtrl: AlertController) {
    this._presentationList = [];
    this._productPresentationList = [];
    this.resetKPI();
  }


  resetKPI() {
    this._kpi = {};
  }
  
  collectKPI(presentationId: string, productId: string, jsonString: string) {
    if(!this._kpi[productId]) {
      this._kpi[productId] = {};
    }
    
    if(!this._kpi[productId][presentationId]) {
      this._kpi[productId][presentationId] = [];
    }
    
    this._kpi[productId][presentationId].push(jsonString);
  }


  get kpi() {
    return Object.keys(this._kpi)
      .map((productId) => ({
        productId: productId,
        presentations: Object.keys(this._kpi[productId])
          .map((presentationId) => ({
            presentationId: presentationId,
            kpis: this._kpi[productId][presentationId]
        }))
      }));
  }


  get isKPIExists(): boolean {
    return Object.keys(this.kpi).length > 0;
  }


  initFocusBrandPresentationList(): Promise<void> {
    const presIds = this._productPresentationList
      .map((productPresentation) => productPresentation.presentation);
    
    return this.presentationsCollection.fetchAllWhereIn(PresentationScheme.fields.id.sfdc, presIds)
      .then(response => this.presentationsCollection.getAllEntitiesFromResponse(response))
      .then(response => {
        const sortField = this.presentationsCollection.scheme.fields.name.sfdc;
        
        this._presentationList = _.sortBy(response, sortField);
      });
  }
  
  initPresentationList(): Promise<void> {
    return this.presentationsCollection.fetchAll()
      .then(response => this.presentationsCollection.getAllEntitiesFromResponse(response))
      .then(response => {
        const sortField = this.presentationsCollection.scheme.fields.name.sfdc;
        this._presentationList = _.sortBy(response, sortField);
      });
  }


  initAllProductPresentationList(): Promise<void> {
    const fetchFn = () => {
      return this.productPresentationsCollection.fetchAll();
    };

    return this.getProductPresentationList(fetchFn)
      .then(this.setProductPresentationList.bind(this))
  }


  initFocusedBrandsProductPresentationList(brandPlannings: BrandPlanning[]) {
    const fetchFn = () => {
      const field = this.productPresentationsCollection.scheme.fields.product.sfdc;
      const focusedBrandIds = this.getFocusedBrandIds(brandPlannings);
      return this.productPresentationsCollection.fetchAllWhereIn(field, focusedBrandIds);
    };

    return this.getProductPresentationList(fetchFn)
      .then(this.setProductPresentationList.bind(this))
  }


  getProductPresentationList(fetchFn) {
    return fetchFn()
      .then(response => this.productPresentationsCollection.getAllEntitiesFromResponse(response))
  }


  setProductPresentationList(response: ProductPresentation[]) {
    this._productPresentationList = _.sortBy(response, 'productName');
    this._uniqProductPresentations = _.uniqBy(this._productPresentationList, 'product');
  }


  get presentationList(): Presentation[] {
    return this._presentationList;
  }


  get productPresentationList(): ProductPresentation[] {
    return this._uniqProductPresentations;
  }


  filterPresentationsByProduct(productId: string = '') {
    const productPresentationIds = this._productPresentationList
      .filter(pp => {
        // filter by product if selected
        if (productId) {
          return pp.product === productId;
        }
        return true;
      })

      .map(pp => pp.presentation);
  
    
    this.setActiveProduct(productId);

    
    return this.presentationList
      .filter(pr => {
        if (productId) {
          return productPresentationIds.indexOf(pr.id) !== -1;
        }
        return true;
      });
  }


  getFocusedBrandIds(brandPlannings: BrandPlanning[] = []): string[] {
    return brandPlannings
      .map((brandPlanning: BrandPlanning) => brandPlanning.focusedBrandIds)
      .reduce((prev, curr) => prev.concat(curr), []);
  }


  openPresentation(presentationId: string) {
    this.presentationFileManager.presentationExist(presentationId)
      .then(() => {
        return this.checkProducts(presentationId)
      })
      .then((productId) => {
        return this.presentationViewer.open(presentationId)
          .then(() => this.presentationViewer.getKPI())
          .then((kpi: string) => {
            this.collectKPI(presentationId, productId, kpi);
            this.presentationViewer.close();
          })
          .catch(err => console.error('openPresentation err', err));
      });
  }


  downloadPresentation(presentation: Presentation): PresentationLoader {
    return this.presentationsLoadManager.queueInvoke(presentation.id, presentation.url);
  }


  cancelDownloadPresentation(presentationId: string) {
    return this.presentationsLoadManager.dequeueInvoke(presentationId);
  }


  getPresentationIconPath(presentationId: string): Promise<string> {
    return this.presentationFileManager.getPresentationIconPath(presentationId);
  }


  updatePresentationEntity(presentation: Presentation) {
    return this.presentationsCollection.updateEntity(presentation);
  }
  
  setActiveFocusBrand(focusBrandId: string): void {
    this.activeFocusBrandId = focusBrandId;
  }
  
  setActiveProduct(productId: string): void {
    this.activeProductId = productId;
  }


  async showNoBrandPlanningsPopup() {
    const title = '';
    const message: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.NoBrandPlanningsPopup.Message');
    return this._showAlert(title, message);
  }


  async showNoFocusedBrandsPopup() {
    const title = '';
    const message: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.NoFocusedBrandsPopup.Message');
    return this._showAlert(title, message);
  }


  async showPresentationNotLoadedPopup() {
    const title: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.PresentationNotLoadedPopup.Caption');
    const message: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.PresentationNotLoadedPopup.Message');
    return this._showAlert(title, message);
  }


  async showConnectionErrorPopup(): Promise<Alert> {
    const title: string = await this.localizationManager.promiseLocale('home.AlertPopup.Caption');
    const message: string = await this.localizationManager.promiseLocale('home.AlertPopup.Message');
    return this._showAlert(title, message);
  }


  async showDownloadErrorPopup(): Promise<Alert> {
    const title: string = await this.localizationManager.promiseLocale('Media.DownloadPresentationErrorCaption');
    const message: string = await this.localizationManager.promiseLocale('Media.DownloadPresentationErrorMessage');
    return this._showAlert(title, message);
  }


  async showNoPresentationsFound(): Promise<Alert> {
    const message: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.PresentationsNotFoundPopup.Message');
    return this._showAlert('', message);
  }


  async showConfirmPopupBeforeStartCall(): Promise<Alert> {
    const title: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.BeforeStartCallConfirmPopup.Title');
    const message: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.BeforeStartCallConfirmPopup.Message');
    return this._showConfirm(title, message);
  }


  async showConfirmBeforeGoBack(): Promise<Alert> {
    const title: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.BeforeGoBackConfirmPopup.Title');
    const message: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.BeforeGoBackConfirmPopup.Message');
    return this._showConfirm(title, message);
  }
  
  async showSelectProductListPopup(listOfProducts: Array<ProductPresentation>): Promise<Alert> {
    const title: string = await this.localizationManager.promiseLocale('BRAND_MATRIX.OneProductSeveralPresentationPopup.SelectProduct');
    
    const list = this.prepareProductsToPopupList(listOfProducts);
    
    const listPopup = new ListPopup(this.alertCtrl, list, title);
    
    return listPopup.showPopup();
  }


  private async _showAlert(title: string = '', message: string = '', _okLabel?: string): Promise<Alert> {
    let okLabel = _okLabel;

    let cssClass = 'popup alert single-button';

    if (!_okLabel) {
      okLabel = await this.localizationManager.promiseLocale('common.buttons.OkBtn');
    }

    if (!title) {
      cssClass += ' without-header'
    }

    if (!message) {
      cssClass += ' without-message'
    }

    const options: AlertOptions = {
      title: title,
      message: message,
      cssClass: cssClass,
      buttons: [
        {
          text: okLabel,
          role: 'cancel'
        }
      ]
    };

    const alert = await this.alertCtrl.create(options);

    return alert.present();
  }


  private async _showConfirm(title: string = '', message: string = ''): Promise<any> {
    let cssClass = 'popup alert confirm';

    const yesLabel = await this.localizationManager.promiseLocale('common.buttons.YesBtn');
    const noLabel = await this.localizationManager.promiseLocale('common.buttons.NoBtn');

    if (!title) {
      cssClass += ' without-header'
    }

    if (!message) {
      cssClass += ' without-message'
    }

    return new Promise((resolve, reject) => {
      const options: AlertOptions = {
        title: title,
        message: message,
        cssClass: cssClass,
        buttons: [
          {
            text: yesLabel,
            role: 'cancel',
            handler: () => {
              resolve();
            }
          },
          {
            text: noLabel,
            role: 'cancel',
            cssClass: 'no',
            handler: () => {
              reject();
            }
          }
        ]
      };

      const alert = this.alertCtrl.create(options);

      return alert.present();
    })
  }
  
  private checkProducts(presentationId: string): Promise<any> {
    if(this.activeProductId) {
      return Promise.resolve(this.activeProductId);
    }
    
    const products = this.getPresentationProducts(presentationId);
    const uniqProducts = _.uniqBy(products, 'product');
    
    
    if(uniqProducts.length > 1) {
      return this.showSelectProductListPopup(uniqProducts)
        .then((presProduct) => {
          return presProduct.id;
        });
    }
  
    if(uniqProducts.length == 1) {
      return Promise.resolve(_.first(uniqProducts).product);
    }
  
    return Promise.resolve();
  }

  private isPresentationInMoreThenOneProducts(presentationId: string): boolean {
    return !!this.getPresentationProducts(presentationId).length;
  }
  
  private getPresentationProducts(presentationId: string): Array<ProductPresentation> {
    return this._productPresentationList.filter((productPresentation) => {
      return productPresentation.presentation == presentationId;
    })
  }
  
  private prepareProductsToPopupList(productsList: Array<ProductPresentation>): Array<{ id: string, description: string }> {
    return productsList.map((product) => ({
      id: product.product,
      description: product.productName
    }))
  }
}
