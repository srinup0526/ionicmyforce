import {Component, ViewChild} from '@angular/core';
import {App, IonicPage, Navbar, NavParams, Platform} from 'ionic-angular';
import {BrandMatrixService} from "./brand-matrix.service";
import {Presentation} from "../../models/Presentation";
import {ProductPresentation} from "../../models/ProductPresentation";
import {Reference} from "../../models/Reference";
import {CallReportPage} from "../call-report/call-report";
import {BrandPlanning} from "../../models/BrandPlanning";

declare let _;

@IonicPage()
@Component({
  selector: 'page-brand-matrix',
  templateUrl: 'brand-matrix.html',
})
export class BrandMatrixPage {
  @ViewChild('navbar') navBar: Navbar;
  private unsubscribeBackEvent: any;

  isFocusedBrands: boolean;
  reference: Reference;
  brandPlannings: BrandPlanning[];

  presentationList: Presentation[];

  productPresentationList: ProductPresentation[];
  selectedProductId: string;


  constructor(public app: App,
              public navParams: NavParams,
              public platform: Platform,
              private brandMatrixService: BrandMatrixService) {
    this.isFocusedBrands = this.navParams.get('isFocusedBrands') || false;
    this.reference = this.navParams.get('reference');
    this.brandPlannings = this.navParams.get('brandPlannings') || [];
    this.presentationList = [];
    this.productPresentationList = [];
    this.selectedProductId = '';
  }


  ionViewDidLoad() {
    this.initializeBackButtonCustomHandler();
    this.initData();
  }


  ionViewDidLeave() {
    this.unsubscribeBackEvent && this.unsubscribeBackEvent();
  }


  applyProductPresentationFilter(productId: string = '') {
    this.selectedProductId = productId;
    this.presentationList = this.brandMatrixService.filterPresentationsByProduct(productId);
    this.showNoPresentationsPopupIfEmpty();
  }


  onClickStartCall() {
    return this.brandMatrixService.showConfirmPopupBeforeStartCall()
      .then(() => this.navCtrl.pop())
      .then(() => this.openCallReport())
      .catch((err) => {
        if (err) {
          console.log(err);
        }
      });
  }


  private initializeBackButtonCustomHandler(): void {
    this.navBar.backButtonClick = this.backButtonClick.bind(this);
    this.unsubscribeBackEvent = this.platform.registerBackButtonAction(
      this.backButtonClick.bind(this),
      101
    );
  }


  private backButtonClick(event) {
    if (this.brandMatrixService.isKPIExists) {
      return this.brandMatrixService.showConfirmBeforeGoBack()
        .then(this.goBack.bind(this))
        .catch((err) => {
          if (err) {
            console.log(err);
          }
        });
    }

    return this.goBack();
  }


  private goBack() {
    return this.navCtrl.pop();
  }


  private initData() {
    this.brandMatrixService.resetKPI();
    if (this.isFocusedBrands) {
      return this.initFocusedBrands();
    }

    return this.initEDetailing();
  }


  private initFocusedBrands() {
    return  this.brandMatrixService.initFocusedBrandsProductPresentationList(this.brandPlannings)
      .then(() => this.brandMatrixService.initFocusBrandPresentationList())
      .then(() => {
        this.productPresentationList = this.getSortedByFocusedBrandsList();
        this.applyProductPresentationFilter(this.brandPlannings[0] && this.brandPlannings[0].focusBrand1 || '')
      });
  }


  private initEDetailing() {
    return this.brandMatrixService.initPresentationList()
      .then(() => this.brandMatrixService.initAllProductPresentationList())
      .then(() => {
        this.productPresentationList = this.brandMatrixService.productPresentationList;
        this.applyProductPresentationFilter('')
      });
  }


  private get navCtrl() {
    return this.app.getRootNav();
  }


  private showNoPresentationsPopupIfEmpty() {
    if (!this.presentationList.length) {
      this.brandMatrixService.showNoPresentationsFound();
    }
  }


  private openCallReport() {
    const params = { row: this.reference, kpi: this.brandMatrixService.kpi };
    
    return this.navCtrl.push(CallReportPage, params);
  }


  private getSortedByFocusedBrandsList() {
    // todo refactor
    const focusedBrandsMap = { fb1: [], fb2: [], fb3: [], fb4: [] };

    this.brandPlannings.forEach((brandPlanning: BrandPlanning) => {
      focusedBrandsMap.fb1.push(brandPlanning.focusBrand1);
      focusedBrandsMap.fb2.push(brandPlanning.focusBrand2);
      focusedBrandsMap.fb3.push(brandPlanning.focusBrand3);
      focusedBrandsMap.fb4.push(brandPlanning.focusBrand4);
    });

    const focusedBrands = Object.keys(focusedBrandsMap)
      .map(key => focusedBrandsMap[key])
      .reduce((prev, curr) => prev.concat(curr), [])
      .filter(v => v);

    const uniqFocusedBrands = _.uniq(focusedBrands);

    const productPresentations = uniqFocusedBrands
      .map(focusedBrand => this.brandMatrixService.productPresentationList.find(pp => pp.product === focusedBrand))
      .filter(v => v);

    return _.uniqBy(productPresentations, 'product');
  }

}
