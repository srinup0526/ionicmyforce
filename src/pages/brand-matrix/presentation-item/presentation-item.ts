import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Presentation} from "../../../models/Presentation";
import {PresentationLoader, PresentationLoaderState} from "../../media/PresentationLoader";
import {BrandMatrixService} from "../brand-matrix.service";
import {Utils} from "../../../utils/Utils";
import {Subscription} from "rxjs";
import {Platform} from "ionic-angular";

declare let window;

@Component({
  selector: 'presentation-item',
  templateUrl: 'presentation-item.html'
})
export class PresentationItemComponent implements OnInit, OnDestroy {
  @Input() presentation: Presentation;

  isUpdateAvailable: boolean;
  isDownloadAvailable: boolean;

  progress: number;
  icon: string;
  loader: PresentationLoader | null;

  private stateSubscription: Subscription;
  private progressSubscription: Subscription;

  constructor(private brandMatrixService: BrandMatrixService,
              private platform: Platform) {

  }


  ngOnInit() {
    this._setPresentationIcon();
    this._updateButtonsState();
  }


  ngOnDestroy() {
    this._loaderUnSubscribe();
  }


  onClickOpen(event: Event) {
    event.stopPropagation();

    if (this.loader) {
      return null;
    }

    if (!this.presentation.wasDownloaded()) {
      return this.brandMatrixService.showPresentationNotLoadedPopup();
    }

    this.brandMatrixService.openPresentation(this.presentation.id);

  }


  onClickDownload(event: Event) {
    event.stopPropagation();
    if (this.loader) {
      return null;
    }

    if (!Utils.deviceIsOnline()) {
      return this.brandMatrixService.showConnectionErrorPopup();
    }

    this.loader = this.brandMatrixService.downloadPresentation(this.presentation);
    this._loaderSubscribe();
  }


  private _setPresentationIcon() {
    if (this.presentation.wasDownloaded()) {
      const timestamp = new Date().getTime();
      const filePath = `${this.presentation.iconPath}?${timestamp}`;
      const isIOS = this.platform.is('ios');
      const iOSFilePath = `${window.Ionic.WebView.convertFileSrc(filePath)}`;
      this.icon = isIOS ? iOSFilePath : filePath;
    } else {
      this.icon = '';
    }
  }


  private _updateButtonsState() {
    this.isUpdateAvailable = this.presentation.wasDownloaded() && this.presentation.hasUpdate();
    this.isDownloadAvailable = !this.presentation.wasDownloaded();
  }


  private _loaderSubscribe() {
    if (this.loader) {
      this.stateSubscription = this.loader.state.subscribe(value => this._onLoaderStateChange(value));
      this.progressSubscription = this.loader.progress.subscribe(value => this._onLoaderProgressChange(value));
    }
  }


  private _loaderUnSubscribe() {
    if (this.stateSubscription) {
      this.stateSubscription.unsubscribe();
    }
    if (this.progressSubscription) {
      this.progressSubscription.unsubscribe();
    }
  }


  private _onLoaderStateChange(state: PresentationLoaderState) {
    if (state === PresentationLoaderState.FINISHED) {
      this.onSuccess();
    }

    if (state === PresentationLoaderState.FAILED) {
      this.onFail();
    }
  }


  private _onLoaderProgressChange(value) {
    this.progress = value;
  }

  private onSuccess() {
    this._updatePresentationIcon()
      .then(() => {
        this.presentation.currentVersion = this.presentation.availableVersion;
        return this._updatePresentationEntity();
      })
      .then(() => {
        this._resetDownloadValues();
        this._updateButtonsState();
        this._setPresentationIcon();
        this.brandMatrixService.cancelDownloadPresentation(this.presentation.id);
      });
  }


  private onFail() {
    // const error = this.loader && this.loader.error;
    this.brandMatrixService.cancelDownloadPresentation(this.presentation.id);
    this._loaderUnSubscribe();
    this._resetDownloadValues();
    this.brandMatrixService.showDownloadErrorPopup();
  }


  private _updatePresentationIcon(): Promise<void> {
    return this.brandMatrixService.getPresentationIconPath(this.presentation.id)
      .then((nativeUrl: string) => {
        this.presentation.iconPath = nativeUrl;
      })
  }


  private _updatePresentationEntity() {
    return this.brandMatrixService.updatePresentationEntity(this.presentation);
  }


  private _resetDownloadValues() {
    this.loader = null;
    this.progress = 0;
  }

}
