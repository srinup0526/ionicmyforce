import {NgModule} from '@angular/core';
import {IonicModule, IonicPageModule, Platform} from 'ionic-angular';
import {BrandMatrixPage} from './brand-matrix';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {CustomLoader} from "../../services/common/localizations/CustomLoader";
import {HttpClient} from "@angular/common/http";
import {FileService} from "../../services/common/FileService";
import {PresentationItemComponent} from "./presentation-item/presentation-item";

@NgModule({
  declarations: [
    BrandMatrixPage,
    PresentationItemComponent
  ],
  imports: [
    IonicPageModule.forChild(BrandMatrixPage),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CustomLoader,
        deps: [HttpClient, Platform, FileService]
      }
    }),
    IonicModule
  ],
  exports: [
    PresentationItemComponent
  ]
})
export class BrandMatrixPageModule {
}
