import {AlertController, App, LoadingController} from "ionic-angular";
import {Injectable} from "@angular/core";
import {LocalizationManager} from "../../../services/common/localizations/LocalizationManager";
import {BrandMatrixPage} from "../brand-matrix";
import {Reference} from "../../../models/Reference";
import {CallReportPage} from "../../call-report/call-report";
import {BrandPlanningsCollection} from "../../../collections/BrandPlanningsCollection";
import {BrandPlanning} from "../../../models/BrandPlanning";
import {BrandMatrixService} from "../brand-matrix.service";

@Injectable({
  providedIn: 'root'
})
export class StartCallPopup {
  private options: any;
  private static optionsValue = {
    FocusedBrands: 'FocusedBrands',
    eDetailing: 'eDetailing',
    SkipEDetailing: 'SkipEDetailing',
  };

  private reference: Reference;

  constructor(public app: App,
              public alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
              public localizationManager: LocalizationManager,
              private brandPlanningsCollection: BrandPlanningsCollection,
              private brandMatrixService: BrandMatrixService) {
    this.options = [
      {
        id: StartCallPopup.optionsValue.FocusedBrands,
        loc: 'BRAND_MATRIX.StartCallPopup.FocusedBrands'
      },
      {
        id: StartCallPopup.optionsValue.eDetailing,
        loc: 'BRAND_MATRIX.StartCallPopup.eDetailing'
      },
      {
        id: StartCallPopup.optionsValue.SkipEDetailing,
        loc: 'BRAND_MATRIX.StartCallPopup.SkipEDetailing'
      }
    ];
  }
  
  public showPopup(selectedReference: Reference): Promise<any> {
    this.reference = selectedReference;

    const alert = this.alertCtrl.create();
    alert.setCssClass('popup alert list start-call-popup');
    
    return this.getOptionsLocales()
      .then(() => {
        this.options.forEach(option => {
          alert.addInput({
            type: 'radio',
            label: option.name,
            value: option.id,
            checked: false,
            handler: (selected) => {
              alert.dismiss();
              this.handleSelectedOption(selected.value);
            }
          });
        });
            
        return alert.present();
      });
  }


  private getOptionsLocales() {
    const promises = this.options.map(option => {
      return this.localizationManager.promiseLocale(option.loc)
        .then(loc => {
          option.name = loc;
        })
    });
    return Promise.all(promises);
  }


  private handleSelectedOption(selectedOption: string) {
    if (selectedOption === StartCallPopup.optionsValue.FocusedBrands) {
      this.handleFocusedBrands();
    }
    if (selectedOption === StartCallPopup.optionsValue.eDetailing) {
      this.handleEDetailing();
    }
    if (selectedOption === StartCallPopup.optionsValue.SkipEDetailing) {
      this.handleSkipEDetailing();
    }
  }


  private handleFocusedBrands() {
    const loader = this.getLoader();
    return loader.present()
      .then(() => this.fetchBrandPlannings())
      .then((brandPlannings: BrandPlanning[]) => {
        const isBrandPlanningsExists = brandPlannings.length > 0;
        const isFocusedBrandsExists = this.brandMatrixService.getFocusedBrandIds(brandPlannings).length > 0;

        if (!isBrandPlanningsExists) {
          return this.brandMatrixService.showNoBrandPlanningsPopup();
        }

        if (!isFocusedBrandsExists) {
          return this.brandMatrixService.showNoFocusedBrandsPopup();
        }

        return this.navCtrl.push(BrandMatrixPage, {isFocusedBrands: true, reference: this.reference, brandPlannings: brandPlannings})
      });
  }


  private handleEDetailing() {
    return this.navCtrl.push(BrandMatrixPage, {isFocusedBrands: false, reference: this.reference});
  }


  private handleSkipEDetailing() {
    this.openCallReport();
  }


  private get navCtrl() {
    return this.app.getRootNav();
  }


  private openCallReport() {
    return this.navCtrl.push(CallReportPage, { row: this.reference });
  }


  private fetchBrandPlannings() {
    return this.brandPlanningsCollection.fetchCurrentForAccount(this.reference.contactSfId)
  }


  private getLoader() {
    return this.loadingCtrl.create({
      content: 'Please wait',
      duration: 100
    })
  }

}
