import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentDetailsPage } from './appointment-details';

@NgModule({
  declarations: [
    AppointmentDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentDetailsPage),
  ],
})
export class AppointmentDetailsPageModule {}
