import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { ConvertCallReportPage } from '../convert-call-report/convert-call-report';
import { ConfirmationDialogService } from '../calendar/confirmation-dialog/confirmation-dialog.service';
import { EditAppointmentPage } from '../edit-appointment/edit-appointment'
import { Storage } from '@ionic/storage';
import * as Constants from '../../services/constants';
import moment from 'moment';
import { OrganizationdetailsPage } from '../organizationdetails/organizationdetails';
import { ContactdetailsPage } from '../contactdetails/contactdetails';
import { CallReportDetailsPage } from '../call-report-details/call-report-details';
import { CallReport } from '../../models/CallReport';
import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { UsersCollection } from '../../collections/UsersCollection';
import { CallReportsCollection } from '../../collections/CallReportsCollection/CallReportsCollection';
import { CallReportStatusPickListDatasource } from '../../services/db/picklist-managers/datasource/CallReportStatusPickListDatasource';
import { CallReportTypePickListDatasource } from '../../services/db/picklist-managers/datasource/CallReportTypePickListDatasource';
import { CallReportCoachingVisitPickListDatasource } from '../../services/db/picklist-managers/datasource/CallReportCoachingVisitPickListDatasource';
import { CallReportTypeOfVisitPickListDatasource } from '../../services/db/picklist-managers/datasource/CallReportTypeOfVisitPickListDatasource';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import { Utils } from '../../utils/Utils';
//import {UserListSingleSelectPage} from "../user-list-single-select/user-list-single-select";
import { SingleUserListPage } from "../single-user-list/single-user-list";
import {UserListMultiSelectPage} from "../user-list-multi-select/user-list-multi-select";
import {Loader} from "../../services/common/Loader";

declare var cordova:any;

/**
 * Generated class for the AppointmentDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-appointment-details',
  templateUrl: 'appointment-details.html',
})
export class AppointmentDetailsPage {
	AppointmentId:any;
  EventDetails:any = {};
  coachingVisitUsr:any;
  CoachingVisitUsers:any = [];
  message:string = "";

  contact: any;
  Reference:any;
  Organisation:any;
  Customer:any;
  row:any;
  coachingVisit:any="";
  dateTimePlanned:any;
  Customer_Name__c:any;
  Org_Name__c:any;
  ActivitesName:'Activites';
  JointVisitParticipants:any;
  CoachingVisitUser:any;
  saveAppointmentData: {};
  contactInformation:any;
  loggedInUserDetails:any;
  Appointments: Array<{ Name: any, Date_Time_Planned__c: string, Contact1__r: any, User__r:any, Organisation__r:any, Type__c:any }>;
  appointmentData: {};
  isDataValid:boolean = true;
  Contact1__c: string = "";
  Date_Time_Planned__c:any;
  Organisation__c: string = "";
  User__c : string = "";
  Coaching_Visit__c: string = "";
  Coaching_Visit_User__c: string = "";
  Duration__c:string = "";
  Joint_Visit_Participants__c: string = "";
  Call_Objective__c:string = "";
  Created_Offline__c:boolean = true;
  Type__c:string = "Appointment";
  RecordTypeId:string = "";
  Call_Report_Status__c:string = "";
  rowId:string="";
  soupEntryId:number = 0;
  rowInfo:any = {};
  JointVisitParticipantUsers:any=[];
  jointVisitParticipants:any;
  config:any;
  coachingPicklistOptions:any=[];
  durationPickListOptions:any = [];
  orgData:any;
  customerData:any;
  userData:any;
  callReport:CallReport;

  public selectedUserSingleStringList : string = "";
  private selectedUserSingleIds: any;

  public selectedUserMultiStringList : string = "";
  private selectedUserMultiIds: Array<string> = [];

  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }

  constructor(public storage:Storage,
    public navCtrl: NavController,
    private confirmationDialogService: ConfirmationDialogService,
    public navParams: NavParams,
    public service:SmartstoreServiceProvider,
    public toastCtrl: ToastController,
    public usersCollection:UsersCollection,
    public callReportsCollection:CallReportsCollection,
    private callReportStatusPickListDatasource: CallReportStatusPickListDatasource,
    private callReportTypePickListDatasource: CallReportTypePickListDatasource,
    private callReportCoachingVisitPickListDatasource: CallReportCoachingVisitPickListDatasource,
    private callReportTypeOfVisitPickListDatasource: CallReportTypeOfVisitPickListDatasource,
    private loader : Loader) {
    this.callReport = new CallReport({});
    SforceDataContext.getActiveUser().then(data => { this.loggedInUserDetails = data; });
    ConfigurationManager.getConfig().then(config => { this.config = config;
      this.durationPickListOptions =config.tourPlanningSettings?config.tourPlanningSettings.duration.split(","):"";
    });
    this.callReportCoachingVisitPickListDatasource.getItems().then(coachingVisitDataSource=> { this.coachingPicklistOptions = coachingVisitDataSource; });
    this.callReport = this.navParams.data['row'];
    console.log("this.callReport",this.callReport);
    this.selectedUserMultiStringList = this.callReport.jointVisitParticipants;
    this.usersCollection.fetchEntityById(this.callReport.coachingVisitUserSfid) .then((callReport) => {
      console.log("callReport",callReport);
      this.selectedUserSingleIds = this.callReport.coachingVisitUserSfid;
      this.selectedUserSingleStringList = callReport.name;
    })
    //this.selectedUserSingleStringList = this.callReport.contactFirstName +" "+ this.callReport.contactLastName;
    this.callReport.dateTimePlanned = new Date(this.callReport.dateTimePlanned.split('.')[0]).toISOString();
    this.loadAllUsers();
  }

  ionViewDidLoad() {
    // this.loadAllUsers();
  }

  setJointVisitParticipants(participants:string)
  {
    if(participants!=null)
    {
      let participantArray = participants.split(';');
      console.log("participantArray",participantArray);
      let array = [];
      console.log("this.JointVisitParticipantUsers",this.JointVisitParticipantUsers)
      array = this.JointVisitParticipantUsers.filter((user) => {
        console.log("user",user);
        console.log("participantArray.indexOf(user.Name)",participantArray.indexOf(user.Name));
        console.log("user.Name in participantArray",user.Name in participantArray);
        return (participantArray.indexOf(user.Name)>-1);
      });
      let finalParticipants = [];
      array.forEach(val=>{
        finalParticipants.push(val);
      })
      console.log("array",array);
      console.log("finalParticipants",finalParticipants);
      this.jointVisitParticipants = array;

    }
  }

  setJointVisitFieldInfo(participants:any)
  {
    if(participants!=null && participants!='')
    {
      let participantArray = participants.split(',');
      let targetArray = [];
      this.JointVisitParticipantUsers.forEach((user) => {
        if((participantArray.indexOf(user.Id)>-1))
        {
          targetArray.push(user.Name);
        }
      })
      return targetArray.join(';');
    }
    else{
      return '';
    }
  }

  public onTapUserHandler() {		
    this.gotoUserPage();		
  }

  public onTapMultiUserHandler() {		
    this.gotoMultiUserPage();		
  }

  private gotoUserPage() {
    this.navCtrl.push(SingleUserListPage, {
      "coaching-user-id": this.selectedUserSingleIds,
      callback: this.setUser.bind(this)
    })
  }

  private gotoMultiUserPage() {
    this.navCtrl.push(UserListMultiSelectPage, {
      ids: this.selectedUserMultiIds,
      callback: this.setMultiUser.bind(this)
    })
  }

  private setMultiUser(userIds) {
    this.selectedUserMultiIds = userIds;
    this.selectedUserMultiStringList = this.selectedUserMultiIds.join('; ');
    
    this.setUserMultiListToView();
  }

  private setUserMultiListToView() {
    if(this.selectedUserMultiIds.length) {
      return this.loader.run(() => {
        return this.usersCollection.fetchForUserIds(this.selectedUserMultiIds)
          .then((users) => {
            this.selectedUserMultiStringList = users
              .map((user) => user.fullName())
              .join('; ');
          })
      })
    }
  
    this.selectedUserMultiStringList = '';
  }

  private setUser(userIds) {
    console.log("userIds",userIds);
    this.selectedUserSingleIds = userIds;
    this.selectedUserSingleStringList = userIds.fullName();
    //this.selectedUserSingleStringList = this.selectedUserSingleIds.join(', ');
    
    //this.setUserListToView();
  }

  private setUserListToView() {
    if(this.selectedUserSingleIds.length) {
      return this.loader.run(() => {
        return this.usersCollection.fetchForUserIds(this.selectedUserSingleIds)
          .then((users) => {
            this.selectedUserSingleStringList = users
              .map((user) => user.fullName())
              .join('; ');
          })
      })
    }
  
    this.selectedUserSingleStringList = '';
  }

  startCall()
  {
    console.log("this.rowInfo.Date_Time_Planned__c",this.rowInfo.Date_Time_Planned__c);
    if(moment()>=moment(this.callReport.dateTimePlanned))
    {
      this.navCtrl.push(ConvertCallReportPage,{callReport:this.callReport});
    }
    else
    {
      const toast = this.toastCtrl.create({
        message: "Not able to convert the future calls",
        showCloseButton:true,
        cssClass:"toastClass",
        duration:3000,
      });
      toast.present();
    }
   // this.navCtrl.push(CallReportDetailsPage,{row:this.rowInfo})
  }

  deleteAppointment(event:any)
  {
    this.confirmationDialogService.confirm('Please confirm..', 'Are you sure, want to delete this appointment ?')
    .then((confirmed) => {
      console.log('User confirmed:', confirmed);
      this.callReportsCollection.removeEntity(this.callReport).then(result=>{
        const toast = this.toastCtrl.create({
          message: "Appointment Deleted Successfully",
          showCloseButton:true,
          cssClass:"toastClass",
          duration:2000,
        });
        toast.present();
        this.navCtrl.pop();
      })
    })
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }
  editAppointment()
  {
    this.navCtrl.push(EditAppointmentPage,{row:this.callReport});
  }
 validateInputData()
  {
    console.log("message",this.message);
    console.log("this.validateInput",this.validateInput());
    if(this.validateInput())
    {
      this.saveAppointment();
    }
    else
    {
      const toast = this.toastCtrl.create({
        message: this.message,
        showCloseButton:true,
        cssClass:"toastClass",
        duration:3000,
      });
      toast.present();
    }
    
  }


  getCoachingBasedUsers(coachingVisist : string)
  {
    if(this.callReport.coachingVisit!='')
    {
      this.CoachingVisitUsers = this.JointVisitParticipantUsers.filter((user) => {
        return (user.currency === this.callReport.coachingVisit.toUpperCase() && user.currency === this.loggedInUserDetails.currency);
      })
    }
  }

  validateInput()
  {
    var isValid = true;
    this.message = "";
    if(!this.callReport.coachingVisit)
    {
      isValid = false;
      this.message+="\n Coaching Visit User Mandatory";
    }
    if(!this.callReport.dateTimePlanned)
    {
      isValid = false;
      this.message+="\n Date Time Planned Can not be empty";
    }
    if(moment(this.callReport.dateTimePlanned) <= moment())
    {
      isValid = false;
      this.message+="\n Date Time Planned Value cannot be less than the current Date ";
    }
    return isValid;  
  }
  saveAppointment(){
    console.log("updated call reort info",this.selectedUserMultiStringList , this.selectedUserSingleIds);
    if(this.selectedUserSingleIds) {
      if(this.selectedUserSingleIds.id)
        this.callReport.coachingVisitUserSfid = this.selectedUserSingleIds.id;
      else
        this.callReport.coachingVisitUserSfid = this.selectedUserSingleIds;
    }
    
    this.callReport.jointVisitParticipants = this.selectedUserMultiStringList;

    console.log('this.callReport.coachingVisitUserSfid',this.callReport.coachingVisitUserSfid);
    console.log('this.callReport.jointVisitParticipants',this.callReport.jointVisitParticipants);
    this.callReportsCollection.updateEntity(this.callReport)
    .then(res=>{
      const toast = this.toastCtrl.create({
        message: "Appointment Updated Successfully!",
        showCloseButton:true,
        cssClass:"toastClass",
        duration:3000,
      });
      toast.present();
      this.navCtrl.pop();
    })
  }

  loadAllUsers():Promise<void> {
    return this.usersCollection.fetchAll()
      .then(res => { return this.usersCollection.getAllEntitiesFromResponse(res);})
      .then(results => {
        this.CoachingVisitUsers = results;
        this.JointVisitParticipantUsers = results;
      })
  }

  navigateToOrganisationPage(){
      this.navCtrl.push(OrganizationdetailsPage, { "row": this.Organisation });
    }

    navigateToContactPage(){
      let row = {};
      this.navCtrl.push(ContactdetailsPage, { "contactSfId": this.callReport.contactSfId });
    }

    public maxDate() {
      const afterDays = 90;

      return moment().add(afterDays, 'days').format("YYYY-MM-DD");
    }

    public minDate() {
      const beforeDays = 10;

      return moment().subtract(beforeDays, 'days').format("YYYY-MM-DD");
    }

}