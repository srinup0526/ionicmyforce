import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TourPlanPage } from './tour-plan';

@NgModule({
  declarations: [
    TourPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(TourPlanPage),
  ],
})
export class TourPlanPageModule {}
