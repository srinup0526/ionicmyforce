import {PresentationFileManager} from "../media/PresentationFileManager";
import {FileProcessor} from "./FileProcessor";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class PresentationStructureGenerator {
  constructor(private presentationFileManager: PresentationFileManager) {

  }

  generate(scenario) {
    const slidesArray = JSON.parse(scenario.structure);
    const chapterName = scenario.name || 'preview';
    const jsonStructure = {
      slides: {},
      chapters: {},
      storyboard: [`${chapterName}`]
    };
    jsonStructure.chapters[`${chapterName}`] = {
      name: `${chapterName}`,
      content: []
    };

    slidesArray.forEach(slideObject => {
      const slideId = `P${slideObject.presentationId}_${slideObject.id}`;
      jsonStructure.slides[slideId] = {
        name: slideObject.name,
        template: `${this.presentationFileManager.getPathToPresentation(slideObject.presentationId)}/${slideObject.path}`
      };
      jsonStructure.chapters[`${chapterName}`].content.push(slideId);
    });

    const fileProcessor = new FileProcessor();
    return fileProcessor.write('structure.json', JSON.stringify(jsonStructure))
      .then(() => {
        return fileProcessor.getFullPath('structure.json')
      })
  }


}


