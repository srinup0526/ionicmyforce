import {Injectable} from '@angular/core';
import {Scenario} from "../../models/Scenario";
import {Presentation} from "../../models/Presentation";
import {PresentationsCollection} from "../../collections/PresentationsCollection";
import {PresentationFileManager} from "../media/PresentationFileManager";
import {PresentationStructureManager} from "./PresentationStructureManager";
import {PresentationScenarioViewer} from "./PresentationScenarioViewer";
import {AlertController, AlertOptions, Keyboard, Platform, ToastController} from "ionic-angular";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {ScenariosCollection} from "../../collections/ScenariosCollection";

declare let window;

@Injectable({
  providedIn: 'root'
})
export class ScenarioDetailsService {
  private _scenario: Scenario;
  private _presentations: Presentation[];
  private _selectedPresentation: Presentation;

  public isChanged: boolean;
  public isEnabled: boolean;
  public presentationStructure: any[];
  public selectedSlides: any[];

  constructor(private presentationsCollection: PresentationsCollection,
              private presentationFileManager: PresentationFileManager,
              private presentationStructureManager: PresentationStructureManager,
              private presentationScenarioViewer: PresentationScenarioViewer,
              private toastCtrl: ToastController,
              private localizationManager: LocalizationManager,
              private scenariosCollection: ScenariosCollection,
              private keyboard: Keyboard,
              private alertCtrl: AlertController,
              private platform: Platform) {
    this._scenario = new Scenario({});
  }


  get scenario(): Scenario {
    return this._scenario;
  }


  get availablePresentations(): Presentation[] {
    return this._presentations;
  }


  init(scenario?: Scenario): Promise<void> {
    this._scenario = scenario || new Scenario({});
    this.selectedSlides = this.scenario.structure && JSON.parse(this.scenario.structure) || [];
    this._selectedPresentation = null;
    this.isChanged = false;
    this.isEnabled = true;
    this._presentations = [];
    this.presentationStructure = [];
    return this.loadAvailablePresentations()
  }


  destroy() {
    this._scenario = new Scenario({});
    this._selectedPresentation = null;
    this.isChanged = false;
    this.isEnabled = true;
    this._presentations = [];
    this.presentationStructure = [];
    this.selectedSlides = [];
  }


  updateName(event) {
    this.isChanged = true;
    this._scenario.name = event;
  }


  selectSlide(slide: any) {
    const selectedSlide = {...slide, presentationName: this._selectedPresentation.name};
    this.selectedSlides.push(selectedSlide);
    this.isChanged = true;
    this.updateSlideVisibility();
  }


  unSelectSlide(slide: any) {
    this.selectedSlides = this.selectedSlides.filter((sequenceSlide) => {
      return !(sequenceSlide.id === slide.id && sequenceSlide.presentationId === slide.presentationId);
    });
    this.isChanged = true;
    this.updateSlideVisibility();
  }


  onPresentationSelect(presentationId: string) {
    this._selectedPresentation = this.getPresentationById(presentationId);
    return this._getTypeOfPresentation(presentationId)
      .then((typeOfVersion) => {
        this.isEnabled = this._checkTypeOfVersionCompatibility(typeOfVersion);
        this.updateSlideList(presentationId);
      })
  }


  buildTSlideThumbnailPath(slide: any) {
    const filePath = this.getPresentationPath(slide.presentationId) + slide.thumbnail;
    const isIOS = this.platform.is('ios');
    const iOSFilePath = `${window.Ionic.WebView.convertFileSrc(filePath)}`;
    return isIOS ? iOSFilePath : filePath;
  }


  previewScenario() {
    if (!this.selectedSlides.length) {
      return this._showNoSlidesForPreviewToast();
    }
    this.openPreviewScenario();
  }


  saveScenario() {
    if (!this.scenario.name) {
      this._showNoNameToast();
      return Promise.reject();
    }
    if (!this.selectedSlides.length) {
      this._showNoSlidesForSaveToast();
      return Promise.reject();
    }
    return this.doSaveScenario();
  }


  loadAvailablePresentations() {
    return this.presentationsCollection.fetchAllLoaded()
      .then((response: Presentation[]) => {
        this._presentations = response;
      })
  }


  showSavePopup(): Promise<void> {
    return new Promise((resolve, reject) => {
      const yesLabelKey = 'common.buttons.YesBtn';
      const noLabelKey = 'common.buttons.NoBtn';
      const titleKey = 'card.ConfirmationPopup.SaveChanges.Caption';
      this.localizationManager.getSeveralLocales([yesLabelKey, noLabelKey, titleKey])
        .then((locales) => {
          const options: AlertOptions = {
            title: locales[titleKey],
            message: '',
            cssClass: 'popup alert confirm without-msg',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: locales[yesLabelKey],
                role: 'cancel',
                handler: resolve
              },
              {
                text: locales[noLabelKey],
                role: 'cancel',
                cssClass: 'no',
                handler: reject
              }
            ]
          };

          this.alertCtrl.create(options).present();
        });
    });

  }


  private updateSlideList(presentationId: string) {
    return this.presentationStructureManager.getParsedStructure(presentationId)
      .then((presentationStructure) => {
        this.presentationStructure = presentationStructure;
        this.updateSlideVisibility();
      });
  }


  private updateSlideVisibility() {
    this.presentationStructure = this.presentationStructure.map((chapter) => {
      chapter.content = chapter.content.map(slide => {
        slide.removed = this._isInSequence(slide);
        return slide;
      });

      chapter.removed = chapter.content.every(slide => slide.removed);

      return chapter;
    });
  }


  private _isInSequence(slide) {
    return this.selectedSlides.filter(sequenceSlide => {
      return (slide.presentationId === sequenceSlide.presentationId) && (slide.id === sequenceSlide.id);
    }).length > 0
  }


  private getPresentationById(id: string): Presentation|null {
    return this._presentations.find((presentation: Presentation) => presentation.id === id)
  }


  private _getTypeOfPresentation(presentationId: string) {
    return this._isCoreTypeOfPresentation(presentationId)
      .then(isCorePresentation => {
        if (isCorePresentation) {
          return Scenario.TYPE.CORE_VERSION;
        } else {
          return Scenario.TYPE.OTHER_VERSION;
        }
      })
  }


  private _isCoreTypeOfPresentation(presentationId) {
    const CORE_FILE = "core.js";

    return this.presentationFileManager.isPresentationFileExist(presentationId, CORE_FILE);
  }


  private _checkTypeOfVersionCompatibility(typeOfVersion) {
    const curTypeOfVersion = this.scenario.typeOfVersion;

    if (!curTypeOfVersion && !this.selectedSlides.length) {
      this.scenario.typeOfVersion = typeOfVersion;
      // TODO check
      return true;
    } else {
      return false;
    }
  }


  private getPresentationPath(presentationId: string) {
    return this.presentationFileManager.getPathToPresentation(presentationId);
  }


  openScenario(scenario: Scenario) {
    const _scenario = {
      name: scenario.name,
      structure: scenario.structure,
      typeOfVersion: scenario.typeOfVersion,
      cobaltVersion: scenario.cobaltVersion || ''
    };

    this.presentationScenarioViewer.open(_scenario)
      .then(() => this.presentationScenarioViewer.close())
      .catch(() => this.presentationScenarioViewer.close());
  }


  private openPreviewScenario() {
    const scenario = {
      name: this.scenario.name,
      structure: JSON.stringify(this.selectedSlides),
      typeOfVersion: this.scenario.typeOfVersion,
      cobaltVersion: this.scenario.cobaltVersion
    };

    this.presentationScenarioViewer.open(scenario)
      .then(() => this.presentationScenarioViewer.close())
      .catch(() => this.presentationScenarioViewer.close());
  }


  private doSaveScenario() {
    this._scenario.structure = JSON.stringify(this.selectedSlides);
    this.keyboard.close();

    if (this.scenario.id) {
      return this.doUpdate();
    } else {
      return this.doCreate();
    }
  }


  private doUpdate() {
    return this.scenariosCollection.updateEntity(this.scenario);
  }


  private doCreate() {
    return this.scenariosCollection.createEntity(this.scenario);
  }


  private _showNoNameToast() {
    const requiredLabelKey = 'card.ToastMessage.RequiredFieldsHeader';
    const nameKey = 'common.names.Name';
    this.localizationManager.getSeveralLocales([requiredLabelKey, nameKey])
      .then((locales) => {
        const message = `${locales[requiredLabelKey]}:\n${locales[nameKey]}`;
        this.showToast(message);
      })
  }


  private _showNoSlidesForPreviewToast() {
    this.localizationManager.promiseLocale('scenarioDetails.NoSlidesForPreview')
      .then(message => {
        this.showToast(message);
      });
  }


  private _showNoSlidesForSaveToast() {
    this.localizationManager.promiseLocale('scenarioDetails.NoSlidesForSave')
      .then(message => {
        this.showToast(message);
      });
  }


  private showToast(message: string) {
    const toast = this.toastCtrl.create({
      message: message,
      showCloseButton: false,
      cssClass: 'toastClass',
      position: 'bottom',
      duration: 3000,
    });
    toast.present();
  }

}
