import { NgModule } from '@angular/core';
import {IonicModule, IonicPageModule, Platform} from 'ionic-angular';
import { ScenarioDetailsPage } from './scenario-details';
import {ScenariosSideViewComponent} from "./scenarios-side-view/scenarios-side-view";
import {ScenariosGridViewComponent} from "./scenarios-grid-view/scenarios-grid-view";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {CustomLoader} from "../../services/common/localizations/CustomLoader";
import {HttpClient} from "@angular/common/http";
import {FileService} from "../../services/common/FileService";
import { DragDropModule } from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [
    ScenarioDetailsPage,
    ScenariosSideViewComponent,
    ScenariosGridViewComponent
  ],
  imports: [
    IonicPageModule.forChild(ScenarioDetailsPage),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CustomLoader,
        deps: [HttpClient, Platform, FileService]
      }
    }),
    IonicModule,
    DragDropModule
  ],
  exports: [
    ScenariosSideViewComponent,
    ScenariosGridViewComponent
  ]
})
export class ScenarioDetailsPageModule {}
