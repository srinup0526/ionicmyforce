import {Component, ViewChild} from '@angular/core';
import {Events, IonicPage, Navbar, NavController, NavParams, Platform} from 'ionic-angular';
import {Scenario} from "../../models/Scenario";
import {ScenarioDetailsService} from "./scenario-details.service";


@IonicPage()
@Component({
  selector: 'page-scenario-details',
  templateUrl: 'scenario-details.html',
})
export class ScenarioDetailsPage {
  @ViewChild('navbar') navBar: Navbar;
  private unsubscribeBackEvent: any;

  scenario: Scenario;
  isEditMode: boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public platform: Platform,
              private scenarioDetailsService: ScenarioDetailsService,
              public events: Events) {
  }


  ionViewDidLoad() {
    this.isEditMode = !!this.scenarioDetailsService.scenario.id;
    this.initializeBackButtonCustomHandler();
  }
  ionViewDidLeave() {
    this.scenarioDetailsService.destroy();
    this.unsubscribeBackEvent && this.unsubscribeBackEvent();
  }


  onClickPreview() {
    this.scenarioDetailsService.previewScenario();
  }


  onClickSave() {
    this.scenarioDetailsService.saveScenario()
      .then(() => {
        this.publishEvent();
        this.goBack();
      })
      .catch(() => {});
  }

  private initializeBackButtonCustomHandler(): void {
    this.navBar.backButtonClick = this.backButtonClick.bind(this);
    this.unsubscribeBackEvent = this.platform.registerBackButtonAction(
      this.backButtonClick.bind(this),
      101
    );
    /* here priority 101 will be greater then 100
    if we have registerBackButtonAction in app.component.ts */
  }

  private backButtonClick(event) {
    if (this.scenarioDetailsService.isChanged) {
      this.scenarioDetailsService.showSavePopup()
        .then(() => {
          this.scenarioDetailsService.saveScenario()
            .then(() => {
              this.publishEvent();
              this.goBack();
            })
            .catch(() => {});
        })
        .catch(() => this.goBack())
    } else {
      this.goBack();
    }
  }


  private goBack() {
    return this.navCtrl.pop();
  }


  private publishEvent() {
    this.events.publish('reloadScenarios');
  }

}
