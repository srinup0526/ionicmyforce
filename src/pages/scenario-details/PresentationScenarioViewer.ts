import { Injectable } from '@angular/core';
import {PresentationFileManager} from "../media/PresentationFileManager";
import {Scenario} from "../../models/Scenario";
import {PresentationStructureGenerator} from "./PresentationStructureGenerator";
import {Utils} from "../../utils/Utils";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";

declare let cordova: any;

@Injectable({
  providedIn: 'root'
})
export class PresentationScenarioViewer {
  private static PLUGIN_NAME = 'PresentationViewer';
  private static ENTRY_FILENAME = 'index.html';
  private static ANDROID_ENTRY_FILENAME = 'file:///android_asset/www/assets/engine/index.html';
  private static IOS_ENTRY_FILENAME = 'engine/index.html';

  private static EVENT_DID_LOAD = 'DID_LOAD';
  private static EVENT_ON_COMPLETE = 'COMPLETE';

  private static ANDROID_ENTRY_DIR = 'file:///android_asset/www/assets/engine/';
  private static IOS_ENTRY_DIR = 'assets/engine/';

  private static OPEN_COMMAND = 'openPresentation';
  private static CLOSE_COMMAND = 'closePresentation';
  private static GET_KPI_COMMAND = 'getKPI';

  private ENTRY_FILE: any;
  private _indexFile: string;

  constructor(private presentationFileManager: PresentationFileManager,
              private presentationStructureGenerator: PresentationStructureGenerator,
              private localizationManager: LocalizationManager) {}


  open(scenario: any, didLoadCb = () => {}): Promise<void> {
    this.ENTRY_FILE = {};
    this.ENTRY_FILE[Scenario.TYPE.OTHER_VERSION] = 'index.html';
    this.ENTRY_FILE[Scenario.TYPE.CORE_VERSION] = 'index-core.html';

    this._setEntryFile(scenario.typeOfVersion);

    return new Promise((resolve, reject) => {
      this.presentationStructureGenerator.generate(scenario)
        .then((structurePath) => {
          const completeKey = 'PresentationViewer.Complete';
          const pauseKey = 'PresentationViewer.Pause';
          const resumeKey = 'PresentationViewer.Resume';
          const pausedKey = 'PresentationViewer.SuspendedLabel';

          this.localizationManager.getSeveralLocales([completeKey, pauseKey, resumeKey, pausedKey])
            .then((locales) => {
              const translations = {
                Complete: locales[completeKey],
                Pause: locales[pauseKey],
                Resume: locales[resumeKey],
                SuspendedLabel: locales[pausedKey]
              };

              const structure = Utils.isIOS() ? structurePath : structurePath.replace('file://', '');

              const index = this._getEntryPath();

              const params = {index, translations, structure};

              const onViewingEvent = (eventMessage) => {
                if (eventMessage === PresentationScenarioViewer.EVENT_DID_LOAD) {
                  didLoadCb();
                }

                if (eventMessage === PresentationScenarioViewer.EVENT_ON_COMPLETE) {
                  resolve();
                }
              };

              const onViewingError = (errorMessage) => {
                reject(errorMessage);
              };

              cordova.exec(onViewingEvent, onViewingError, PresentationScenarioViewer.PLUGIN_NAME, PresentationScenarioViewer.OPEN_COMMAND, [params]);
            });
        });
    });
  }

  close() {
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, PresentationScenarioViewer.PLUGIN_NAME, PresentationScenarioViewer.CLOSE_COMMAND, []);
    });
  }

  getKPI() {
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, PresentationScenarioViewer.PLUGIN_NAME, PresentationScenarioViewer.GET_KPI_COMMAND, []);
    });
  }


  private _setEntryFile(typeOfVersion = Scenario.TYPE.OTHER_VERSION) {
    this._indexFile = this.ENTRY_FILE[typeOfVersion];
    // There are differences in the versions of the cobalt engine
    // There are two critically different approaches in which the spa presentation file does not work in another version.
    // Such transitions can be divided into two types: those in which the core appeared, and versions without the core
  }


  private _getEntryPath() {
    return this._getPresentationIndexDirectory() + this._getIndexFile();
  }


  private _getPresentationIndexDirectory() {
    return Utils.isIOS() ? PresentationScenarioViewer.IOS_ENTRY_DIR : PresentationScenarioViewer.ANDROID_ENTRY_DIR;
  }


  private _getPresentationIndex() {
    return Utils.isIOS() ? PresentationScenarioViewer.IOS_ENTRY_FILENAME : PresentationScenarioViewer.ANDROID_ENTRY_FILENAME;
  }


  private _getIndexFile() {
    return this._indexFile;
  }

}
