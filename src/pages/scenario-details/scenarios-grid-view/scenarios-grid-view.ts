import { Component, ViewChild } from '@angular/core';
import {ScenarioDetailsService} from "../scenario-details.service";
import {CdkDragDrop, CdkDrag, CdkDropListGroup, CdkDropList, CdkDropListContainer, moveItemInArray} from "@angular/cdk/drag-drop";

@Component({
  selector: 'scenarios-grid-view',
  templateUrl: 'scenarios-grid-view.html'
})
export class ScenariosGridViewComponent {
  
  @ViewChild(CdkDropListGroup) listGroup: CdkDropListGroup<CdkDropList>;
  @ViewChild(CdkDropList) placeholder: CdkDropList;
  
  public target: CdkDropList;
  public targetIndex: number;
  public source: CdkDropListContainer;
  public sourceIndex: number;
  public dropListEnterPredicateHandler;

  constructor(private scenarioDetailsService: ScenarioDetailsService) {
    this.dropListEnterPredicateHandler = this.enter.bind(this);
  }


  onClickRemove(slide) {
    this.scenarioDetailsService.unSelectSlide(slide);
  }
  
  ngAfterViewInit() {
    let phElement = this.placeholder.element.nativeElement;
    
    phElement.style.display = 'none';
    phElement.parentNode.removeChild(phElement);
  }
  
  drop() {
    if (!this.target)
      return;
    
    let phElement = this.placeholder.element.nativeElement;
    let parent = phElement.parentNode;
    
    phElement.style.display = 'none';
    
    parent.removeChild(phElement);
    parent.appendChild(phElement);
    parent.insertBefore(this.source.element.nativeElement, parent.children[this.sourceIndex]);
    
    this.target = null;
    this.source = null;
    
    if (this.sourceIndex != this.targetIndex)
      moveItemInArray(this.scenarioDetailsService.selectedSlides, this.sourceIndex, this.targetIndex);
  }
  
  enter(drag: CdkDrag, drop: CdkDropList) {
    if (drop == this.placeholder)
      return true;
    
    let phElement = this.placeholder.element.nativeElement;
    let dropElement = drop.element.nativeElement;
    
    let dragIndex = __indexOf(dropElement.parentNode.children, drag.dropContainer.element.nativeElement);
    let dropIndex = __indexOf(dropElement.parentNode.children, dropElement);
    
    if (!this.source) {
      this.sourceIndex = dragIndex;
      this.source = drag.dropContainer;
      
      let sourceElement = this.source.element.nativeElement;
      phElement.style.width = sourceElement.clientWidth + 'px';
      phElement.style.height = sourceElement.clientHeight + 'px';
      
      sourceElement.parentNode.removeChild(sourceElement);
    }
    
    this.targetIndex = dropIndex;
    this.target = drop;
    
    phElement.style.display = '';
    dropElement.parentNode.insertBefore(phElement, (dragIndex < dropIndex)
      ? dropElement.nextSibling : dropElement);
    
    this.source.start();
    this.placeholder.enter(drag, drag.element.nativeElement.offsetLeft, drag.element.nativeElement.offsetTop);
    
    return false;
  }

}

function __indexOf(collection, node) {
  return Array.prototype.indexOf.call(collection, node);
}
