declare let window;
declare let LocalFileSystem;
declare let FileReader;

export class FileProcessor {
  private _rootPath: string;
  private fileName: string;
  private dataString: string;


  write(fileName, dataString) {
    this.fileName = fileName;
    this.dataString = dataString;
    return this._requestFileSystem()
      .then(this._gotFS.bind(this))
      .then(this._gotFileEntry.bind(this))
      .then(this._gotFileWriter.bind(this))
  }

  read(fileName) {
    this.fileName = fileName;
    return this._requestFileSystem()
      .then(this._gotFS.bind(this))
      .then(this._gotFile.bind(this))
      .then(this._readAsText.bind(this))
  }


  getFullPath(fileName) {
    this.fileName = fileName;
    return this._requestFileSystem()
      .then(() => {
        return `${this._rootPath}/${this.fileName}`
      });
  }


  getFileDirectory(fileName) {
    this.fileName = fileName;
    return this._requestFileSystem()
      .then(() => {
        return `${this._rootPath}`
      });
  }



  private _requestFileSystem() {
    return new Promise((resolve, reject) => {
      window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, (fileSystem) => {
        this._rootPath = fileSystem.root.toURL().replace('file://localhost', '');
        resolve(fileSystem);
      }, reject);
    });
  }


  private _gotFS(fileSystem) {
    return new Promise((resolve, reject) => {
      fileSystem.root.getFile(this.fileName, {create: true, exclusive: false}, resolve, reject);
    });
  }


  private _gotFileEntry(fileEntry) {
    return new Promise((resolve, reject) => {
      fileEntry.createWriter(resolve, reject);
    });
  }


  private _gotFile(fileEntry) {
    return new Promise((resolve, reject) => {
      fileEntry.file(resolve, reject);
    });
  }


  private _readAsText(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onloadend = (event) => {
        resolve(event.target.result);
      };
      reader.readAsText(file);
    });
  }


  private _gotFileWriter(writer) {
    return new Promise((resolve, reject) => {
      writer.onwriteend = () => {
        resolve(writer);
      };
      writer.write(this.dataString)
    });
  }

}
