import {Component, ViewChild} from '@angular/core';
import {AlertOptions, Select} from "ionic-angular";
import {Presentation} from "../../../models/Presentation";
import {ScenarioDetailsService} from "../scenario-details.service";


@Component({
  selector: 'scenarios-side-view',
  templateUrl: 'scenarios-side-view.html'
})
export class ScenariosSideViewComponent {

  nameModel: string;

  @ViewChild('presentationSelect')
  presentationSelect: Select;
  presentationsSelectOptions: AlertOptions;
  selectedPresentationId: string;
  private selectedPresentation: Presentation;


  constructor(public scenarioDetailsService: ScenarioDetailsService) {
    this.presentationsSelectOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };

    this.selectedPresentationId = '';
    this.selectedPresentation = null;
  }

  ngOnInit() {
    this.nameModel = this.scenarioDetailsService.scenario.name;
  }


  onPresentationSelectHandler(presentationId: string = '') {
    this.selectedPresentationId = presentationId;
    this.presentationSelect.close();
    this.scenarioDetailsService.onPresentationSelect(presentationId);
  }


  onSlideClickHandler(slide: any) {
    this.scenarioDetailsService.selectSlide(slide);
  }

}
