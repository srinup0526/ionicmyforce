import {Injectable} from "@angular/core";
import {PresentationFileManager} from "../media/PresentationFileManager";

@Injectable({
  providedIn: 'root'
})
export class PresentationStructureManager {
  private static _relativePathToStructure = 'structure.json';

  constructor(private presentationFileManager: PresentationFileManager) {

  }


  getParsedStructure(presentationId: string) {
    const structurePath = this._mapPathToStructure(presentationId);
    return this.presentationFileManager.readPresentationFile(structurePath)
      .then(structureContent => {
        return this._parseStructure(structureContent, presentationId);
      })

  }


  private _parseStructure(structureContent, presentationId) {
    const structure = JSON.parse(structureContent);
    return Object.keys(structure['chapters'])
      .filter(chapterId => {
        return structure['chapters'][chapterId].content.length > 0;
      })
      .map(chapterId => {
        const chapter = structure['chapters'][chapterId];
        chapter['id'] = chapterId;
        chapter['content'] = chapter['content']
          .map(slideId => {
            const slide = structure['slides'][slideId];
            slide['id'] = slideId;
            slide['path'] = slide['template'];
            slide['thumbnail'] = this._mapThumbnailPath(slideId);
            slide['chapterId'] = chapterId;
            slide['presentationId'] = presentationId;
            delete slide['template'];
            return slide
          });
        return chapter;
      })
  }


  private _mapPathToStructure(presentationId: string) {
    return `${presentationId}/${PresentationStructureManager._relativePathToStructure}`
  }


  private _mapThumbnailPath(slideId: string) {
    return `/media/images/common/thumbs/${slideId}.jpg`
  }


}
