import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganizationEditCardPage } from './organization-edit-card';

@NgModule({
  declarations: [
    OrganizationEditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganizationEditCardPage),
  ],
})
export class OrganizationEditCardPageModule {}
