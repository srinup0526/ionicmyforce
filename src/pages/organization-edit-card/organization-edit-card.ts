import {Component, ViewChild} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertOptions,
  Select,
  ToastController,
  AlertController,
  Platform
} from 'ionic-angular';
import {Organization} from "../../models/Organization";
import {OrganizationJuridicGroupPicklistDatasource} from "../../services/db/picklist-managers/datasource/OrganizationJuridicGroupPicklistDatasource";
import {OrderStatusPickListDatasource} from "../../services/db/picklist-managers/datasource/OrderStatusPicklistDatasource";
import {OrganizationStatusPicklistDatasource} from "../../services/db/picklist-managers/datasource/OrganizationStatusPicklistDatasource";
import {OrganizationRecordTypePicklistDatasource} from "../../services/db/picklist-managers/datasource/OrganizationRecordTypePicklistDatasource";
import {DataChangeRequestHelper} from "../data-change-requests/DataChangeRequestHelper";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {BackHandlers} from "../../utils/BackHandlers";
import {Loader} from "../../services/common/Loader";
import {OrganizationScheme} from "../../models/scheme/OrganizationScheme";


@IonicPage()
@Component({
  selector: 'page-organization-edit-card',
  templateUrl: 'organization-edit-card.html',
})
export class OrganizationEditCardPage extends BackHandlers {
  
  public filterOptions: AlertOptions;
  public organization: Organization;
  
  public organizationStartBackup: Organization;
  
  public organizationRecordTypes;
  public organizationJuridicGroups;
  public organizationStatuses;
  
  @ViewChild('recordTypeSelect') recordTypeSelect: Select;
  @ViewChild('juridicGroupsSelect') juridicGroupsSelect: Select;
  @ViewChild('statusesSelect') statusesSelect: Select;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private dataChangeRequestHelper: DataChangeRequestHelper,
              private orgJuridicGroupPicklistDatasource: OrganizationJuridicGroupPicklistDatasource,
              private orgStatusPicklistDatasource: OrganizationStatusPicklistDatasource,
              private orgRecordTypePicklistDatasource: OrganizationRecordTypePicklistDatasource,
              localizationManager: LocalizationManager,
              loader: Loader,
              alertCtrl: AlertController,
              platform: Platform) {
    
    super(navCtrl, localizationManager, loader, alertCtrl, platform);
    
    this.filterOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };
    
    this.organization = new Organization({});
    this.organizationStartBackup = new Organization({});
  }
  
  public ionViewDidLoad() {
    this.organization = this.navParams.data['row'];
    
    this.loader.run(this.initPicklists.bind(this))
      .then(() => {
        return this.setStartBackup();
      });
    
    super.ionViewDidLoad();
  }
  
  public onTapSaveBtnHandler() {
    this.save();
  }
  
  public onChangeJuridicGroupHandler(jGroup): void {
    this.organization.juridicGroup = jGroup.id;
    
    this.juridicGroupsSelect.close();
  }
  
  public onChangeStatusHandler(status): void {
    this.organization.status = status.id;
    
    this.statusesSelect.close();
  }
  
  public onChangeRecordTypeHandler(type): void {
    this.organization.recordTypeId = type.id;
    this.organization.recordType = type.id;

    this.recordTypeSelect.close();
  }
  
  protected validate(): Promise<any> {
    return this.localizationManager.getSeveralLocales([
      'common.alerts.NoRequiredFields',
      'common.names.Name',
      'common.names.Type',
      'common.names.Status'
    ])
      .then((localization) => {
        return new Promise((resolve, reject) => {
          const toastHeader = `${localization['common.alerts.NoRequiredFields']} \n`;
          let toastBody = '';
          
          if (!this.organization.name) {
            toastBody += ` - ${localization['common.names.Name']}\n`;
          }
          
          if (!this.organization.recordTypeId) {
            toastBody += ` - ${localization['common.names.Type']}\n`;
          }
          
          if (!this.organization.status) {
            toastBody += ` - ${localization['common.names.Status']}\n`;
          }
          
          const isDataValid = toastBody.length == 0;
          
          if (isDataValid) {
            return resolve();
          }
          
          return reject(new Error(toastHeader + toastBody));
        });
      });
  }
  
  protected initPicklists(): Promise<any> {
    return Promise.all([
      this.orgRecordTypePicklistDatasource.getItems()
        .then((items) => {
          this.organizationRecordTypes = items;
          
          if(!this.organization.recordType) {
            this.organization.recordType = items[0].id;
          }
        }),
      this.orgJuridicGroupPicklistDatasource.getItems()
        .then((items) => {
          this.organizationJuridicGroups = items;
          
          if(!this.organization.juridicGroup) {
            this.organization.juridicGroup = items[0].id;
          }
        }),
      this.orgStatusPicklistDatasource.getItems()
        .then((items) => {
          this.organizationStatuses = items;
          
          if(!this.organization.status){
            this.organization.status = items[0].id;
          }
        })
    ]);
  }
  
  protected validateAndSaveOrder(): Promise<any> {
    return this.save();
  }
  
  protected hasChanges() {
    return this.isSameFillFields(this.organization, this.organizationStartBackup, OrganizationScheme.fields);
  }
  
  private save(): Promise<any> {
    return this.validate()
      .then(() => this.doSave())
      .then(() => this.goBack())
      .catch((error: Error) => {
        return this.showValidateToast(error.message);
      })
  }
  
  private doSave() {
    return this.dataChangeRequestHelper.createOrganization(this.organization);
  }
  
  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton: true,
      cssClass: "toastClass",
      duration: 5000,
    });
    
    return toast.present();
  }
  
  private setStartBackup() {
    this.organizationStartBackup = new Organization(this.organization);
  }
  
}
