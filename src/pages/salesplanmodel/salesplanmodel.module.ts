import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesplanmodelPage } from './salesplanmodel';

@NgModule({
  declarations: [
    SalesplanmodelPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesplanmodelPage),
  ],
})
export class SalesplanmodelPageModule {}
