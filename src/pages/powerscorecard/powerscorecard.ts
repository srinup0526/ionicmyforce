import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { CoachingPlanCollection } from "../../collections/CoachingPlanCollection";
import { CoachingPlanScheme } from "../../models/scheme/CoachingPlanScheme";
import { PoweScorecardYearPicklisttDatasource } from './../../services/db/picklist-managers/datasource/PoweScorecardYearPicklisttDatasource';
import { PoweScorecardMonthPicklisttDatasource } from './../../services/db/picklist-managers/datasource/PoweScorecardMonthPicklisttDatasource';
import { PoweScorecardDesigPicklisttDatasource } from './../../services/db/picklist-managers/datasource/PoweScorecardDesigPicklisttDatasource';

import {FilterPanelComponent} from "../../components/filter/filter-panel/filter-panel";
import { Query } from "../../services/common/Query";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { PowerscorecardDetailsPage } from '../powerscorecard-details/powerscorecard-details';


/**
 * Generated class for the PowerscorecardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-powerscorecard',
  templateUrl: 'powerscorecard.html',
})
export class PowerscorecardPage {
  public tableOptions: LazyTableOption;
  public collection: CoachingPlanCollection;
  public searchString : string = "";
  public powerScoreCardCount:any = 0;
  public settings: SettingsImpl;

  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('mainContent') mainContent: ElementRef;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public poweScorecardYearPicklisttDatasource: PoweScorecardYearPicklisttDatasource,
    public poweScorecardMonthPicklisttDatasource: PoweScorecardMonthPicklisttDatasource,
    public poweScorecardDesigPicklisttDatasource: PoweScorecardDesigPicklisttDatasource,
    public coachingPlanCollection : CoachingPlanCollection) {
    this.coachingPlanCollection.preparePicklists();
    this.settings = Settings.getInstance();
    const headerItems = [
      {
        title: 'common.names.Name',
        fields: [CoachingPlanScheme.fields.name],
        isSortable: true,
        isAsc: true,
        isActive: false,
        picklistValues: {}
      },
      {
        title: 'common.names.Month',
        fields: [CoachingPlanScheme.fields.month],
        isSortable: true,
        isAsc: true,
        isActive: false,
        picklistValues: {}
      },
      {
        title: 'common.names.Year',
        fields: [CoachingPlanScheme.fields.year],
        isSortable: true,
        isAsc: true,
        isActive: false
      },
      {
        title: 'common.names.EmployeeName',
        fields: [CoachingPlanScheme.fields.employeeName],
        isSortable: true,
        isAsc: true,
        isActive: false
      },
      {
        title: 'common.names.Designation',
        fields: [CoachingPlanScheme.fields.designation],
        isSortable: true,
        isAsc: true,
        isActive: false
      },
      {
        title: 'common.names.DateOfJoining',
        fields: [CoachingPlanScheme.fields.dateofjoining],
        isSortable: true,
        isAsc: true,
        isActive: false,
        modelFunction:'getJoinDateFormat'
      }
      ];
  
      this.tableOptions = {
        headerItems: headerItems,
        rowHandler: (event, reference) => {
          this.itemTapped(reference);
        },
        batchSize: 100
      };
  
      this.collection = this.coachingPlanCollection;
  }

  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable();
  }

  private setPowerScoreCount(): number {
    const countQuery = this.collection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);

    return this.collection.runCountQuery(query.query)
    .then((count) => {
      return this.powerScoreCardCount = count || 0;
    })
  }

  public onFilterChangeHandler(filterData): void {
    console.log("filterData",filterData);
    this.lazyTable.reloadTableByFilterData(filterData);
  }

  public onChangeQueryHandler(query: Query): void {
    this.setPowerScoreCount();
  }

itemTapped(row: string) {
    console.log("detail page show",row)
    this.navCtrl.push(PowerscorecardDetailsPage, { "row": row });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PowerscorecardPage');
  }

}
