import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PowerscorecardPage } from './powerscorecard';

@NgModule({
  declarations: [
    PowerscorecardPage,
  ],
  imports: [
    IonicPageModule.forChild(PowerscorecardPage),
  ],
})
export class PowerscorecardPageModule {}
