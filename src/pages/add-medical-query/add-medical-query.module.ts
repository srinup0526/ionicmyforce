import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddMedicalQueryPage } from './add-medical-query';

@NgModule({
  declarations: [
    AddMedicalQueryPage,
  ],
  imports: [
    IonicPageModule.forChild(AddMedicalQueryPage),
  ],
})
export class AddMedicalQueryPageModule {}
