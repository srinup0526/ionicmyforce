import { Component } from '@angular/core';
import { IonicPage, NavController,AlertController, NavParams,ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { ReferenceCollection } from '../../collections/references/ReferenceCollection';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { MQPickListDatasource } from '../../services/db/picklist-managers/datasource/MQPickListDatasource';
import { UsersCollection } from '../../collections/UsersCollection';
import { ProductsCollection } from '../../collections/ProductsCollection';
import { OrganizationsCollection } from '../../collections/OrganizationsCollection';
import {MedicalQuery} from "../../models/MedicalQuery";
import { MedicalQueryScheme } from "../../models/scheme/MedicalQueryScheme";
import { Utils} from '../../utils/Utils';
import {MedicalQueriesCollection} from "../../collections/MedicalQueriesCollection/MedicalQueriesCollection";
/**
 * Generated class for the AddMedicalQueryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-add-medical-query',
  templateUrl: 'add-medical-query.html',
})
export class AddMedicalQueryPage {
  loggedInUserDetails: any;
  medicalquerylistvalues:any;
  Allmedquery:any;
  medquery:any;
  text: string;
  _customer:any;
  customerOrg:any;
  productlist:any;
  public activeUser:any;
  organizationlist:any;
  Pharma_Product__r:any;
  Mylan_Medical_Contact__r:any;
  Medicalquery:any= {};
  medicalcontactperson:any;
  customerName:any;
  noncustomer:any = "";
  organization:any = "";
  email:any;
  mobileNumber:any;
  CreatedDate:any;
  customerproduct:any;
  medicalContact:any;
  typeOfQuery:any;
  selectedProducts:any;
  Description__c:any;
  description:any;
  message = "";
  status = MedicalQueryScheme.APPROVAL_STATUS_DRAFT;
  createdDate:any = new Date().toISOString();
  product:any;
  speciality:any;
  mediaquery:any;
  config:any;
  MedicalContactId:string='';
  MedicalContactPersons:any = [];
  MedicalContactPersonsBkup:any = [];
  public configData:any;
  public customers:any;
  today: any;
  pickListValues: any;
  custName:any;
  medicalContactName:any;
  medicalQuery:MedicalQuery;
  //Medicalquery:any;
  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }
  Referenceteam:any;
  rows:any;
  contactsBackup:any;
  constructor(public navCtrl: NavController,
              public toastCtrl: ToastController,
              public storage:Storage,
              public alertCtrl: AlertController,
              public navParams: NavParams,
              private service: SmartstoreServiceProvider,
              public referenceCollection:ReferenceCollection,
              public mqPickListDatasource:MQPickListDatasource,
              public usersCollection:UsersCollection,
              public productsCollection:ProductsCollection,
              public organizationsCollection : OrganizationsCollection,
              private medicalQueriesCollection : MedicalQueriesCollection) {
                this.today = Date.now();
    ConfigurationManager.getConfig()
    .then(config=>{
      console.log("config",config);
      this.configData = config;
    })
    this.referenceCollection.fetchAll()
    .then(records=>{ return this.referenceCollection.getAllEntitiesFromResponse(records);})
    .then(res=>{
      console.log("res",res);
      this.customers = res;
      console.log("Customer list",this.customers);
    })
    SforceDataContext.getActiveUser()
    .then(activeUser=>{
      console.log("activeUser",activeUser);
      this.loggedInUserDetails = activeUser;
    })
    this.mqPickListDatasource.getItems()
    .then(res=>{
      console.log("type of query details",res);
      this.pickListValues = res;
    })
    this.usersCollection.fetchAll()
    .then(records=>{ return this.usersCollection.getAllEntitiesFromResponse(records);})
    .then(res=>{
      console.log("res",res);
      this.MedicalContactPersons = res.filter(user=> { return user.userRoleId==this.configData.sampleManagementSettings.MedicalContactId});
      this.MedicalContactPersonsBkup = this.MedicalContactPersons;
      console.log("this.MedicalContactPersons",this.MedicalContactPersons);
    })
    this.productsCollection.fetchAll()
    .then(records=>{ return this.productsCollection.getAllEntitiesFromResponse(records);})
    .then(res=>{
      console.log("products res",res);
      this.productlist = res;
    })
    this.organizationsCollection.fetchAll()
    .then(records=>{ return this.organizationsCollection.getAllEntitiesFromResponse(records);})
    .then(res=>{
      console.log("organization res",res);
      this.organizationlist = res;
    })
  }

  ionViewDidLoad() {
    // this.storage.get("currentUserDetails").then(data => { this.loggedInUserDetails = data; console.log("UserData",this.loggedInUserDetails); });
    // this.storage.get("medicalquerypicklist").then(data=>{
    //   this.medicalquerylistvalues =data[0].picklistOptions;
    //   console.log("medicalquerylistvalues",data);
    // })
    // console.log('ionViewDidLoad AddMedicalQueryPage');
    // this.Medicalquery = {};
    // this.loadReference();
    // this.loadproducts();
    //this.loadAllmedicalcontactperson();
    //this.loadmedmedicalquery();
   
  }
  loadReference() {

    return this.service.loadReference()
      .then(results => {
        console.log(results);
        this.Referenceteam = results.records;
        this.contactsBackup = results.records;

      console.log('Referenceteam:',this.Referenceteam);
      })
  }
  setCustomer(customer: any){
    this.custName = customer.name;
  }
  setMedicalContact(medicalContact:any) {
    this.medicalContactName = medicalContact.name;
  }
  loadproducts() {

    return this.service.loadproducts()
      .then(results => {
        console.log(results);
        this.productlist = results.records;

      console.log('productlist:',this.productlist);
      })
  }

  filterMedicalContactPersons(event:string)
  {
    console.log("typeOfQuery",event);
    if(event!='')
    {
      this.MedicalContactPersons = this.MedicalContactPersonsBkup;
      this.MedicalContactPersons = this.MedicalContactPersons.filter(user=>{
        console.log("user filter", user);
        console.log("user.typeofquery == event",user.typeofquery == event);
        return user.typeofquery == event;
      }) 
      console.log("this.MedicalContactPersons",this.MedicalContactPersons);
    }
    else
    {
      this.MedicalContactPersons = this.MedicalContactPersonsBkup;
    } 
    console.log("filterMedicalContactPersons",this.MedicalContactPersons);
  }

  // loadAllmedicalcontactperson(){
  //   return this.service.loadAllmedicalcontactperson()
  //   .then(results => {
  //     console.log(results);
  //     this.medicalcontactperson = results.records;

  //   console.log('medicalcontactperson:',this.medicalcontactperson);
  //   })
  // }
  loadmedmedicalquery() {

    return this.service.loadmedmedicalquery()
      .then(results => {
        console.log('medical-query',results);
        results.records.forEach(element => {
          console.log("Element",element);
          console.log("typeOfEachRecord",element.Pharma_Product__r);
          if(element.Pharma_Product__r==null)
          {
            element.Pharma_Product__r = {};
            element.Pharma_Product__r.Name = null;
          }
          if(element.Customer__r==null)
          {
            element.Customer__r = {};
            element.Customer__r.Name = null;
          }
          if(element.Mylan_Medical_Contact__r==null)
          {
            element.Mylan_Medical_Contact__r = {};
            element.Mylan_Medical_Contact__r.Name = null;
          }
          
        });
        console.log("result.records",results.records)
        this.Medicalquery = results.records[0];
        console.log('Medmedquery',this.Medicalquery);
      })
  }
  // saveMedQuery(id: string,_soupEntryId:string){
  //   let confirmUpdate = this.alertCtrl.create({
  //     title: `Update PharmaEvent?`,
  //     message: `Are you sure you want to Update`,
  //     buttons: [
  //       {
  //         text: 'No',
  //         handler: () => {
  
  //           confirmUpdate.dismiss();
  //         }
  //       },
  //       {
  //         text: 'Yes',
  //         handler: () => {
  //           // let editPharmaEvent = {
  //           // };
  //           this.Medmedquery.Customer_Name__c=this.customerName;
  //           this.Medmedquery.End_Date__c=this.customerOrg;
  //           this.Medmedquery.Stage__c=this.email;
  //           this.Medmedquery.Start_Date__c=this.MobileNumber;
  //           this.Medmedquery.Name=this.CreatedDate;
  //           this.Medmedquery.Location__c=this.customerproduct;
  //           this.Medmedquery.Location__c=this.medicalContact; 
  //           this.Medmedquery.Location__c=this.typeOfQuery;
          
  
  //          let success = (items) => {console.log("items",items);
  //           this.navParams.get("parentPage").reloadPE();
            
  //           //this.navCtrl.pop();
  //         }
  //           let failure = (error) => console.error(`Soup Upsert Error: ${error}`);
  //           console.log("this.PharmaEvent",this.Allmedquery);
  //           //this.service.updatePharmaEventIdandSoupEntryId(this.PharmaEvent.Id,this.PharmaEvent._soupEntryId);
  //           this.service.smartStore().upsertSoupEntries("PharmaEvent", [this.Allmedquery], success, failure);
  //         } 
  //       }
  //     ]
  //   });
  
  //   confirmUpdate.present();
  // }
  saveMedQuery(){
    console.log("save media query");

    let MQ = new MedicalQuery({
      createdDateTime: this.createdDate,
      customer: this.customerName,
      customerName: this.custName,
      noncustomer: this.noncustomer,
      specialty: this.speciality,
      organization: this.organization,
      email: this.email,
      mobile: this.mobileNumber,
      medicalContact: this.medicalContact,
      medicalContactName: this.medicalContactName,
      pharmaProduct: this.product,
      queryDescription: this.mediaquery,
      status: MedicalQueryScheme.APPROVAL_STATUS_DRAFT,
      typeofquery : this.typeOfQuery,
      user: this.loggedInUserDetails.id
   });

   this.medicalQueriesCollection.createEntity(MQ)
   .then((record) => this.medicalQueriesCollection.parseModel(record))
   .then(mqQuery=>{
    console.log("medical query saved entry",mqQuery);
   // this.navParams.get("parentPage").reloadMedQuery();
    this.navCtrl.pop();
    const toast = this.toastCtrl.create({
      message: "Medical Query Created Successfully!",
      showCloseButton:true,
      cssClass:"toastClass",
      position:'middle',
      duration:3000,
    });
    toast.present();
  })
                                 
                                  // let mqId = mqQuery._soupEntryId;
                                  // let MQComment = new MedicalComment({
                                  //   commentDescription:" Good product",
                                  //   commentCreatedDateTime:"Friday 12 July 2019",
                                  //   commentFeedback:"Vey good",
                                  //   mqId:mqId
                                  //    })
                                  //  this.medicalCommentCollection.createEntity(MQComment).then(mqcomments => {
                                  //    console.log(mqcomments);
                                  //  })
                              
                                
    // this.Medicalquery.Pharma_Product__r = JSON.parse(this.Medicalquery.Pharma_Product__r);
    // this.customerName = JSON.parse(this.customerName);
    // this.customerName.Name= this.customerName.Customer__r.Name;
    // this.customerOrg = this.customerName.Organisation__r;

  // this.customerOrg = JSON.parse(this.customerOrg);
  // this.customerOrg= this.customerOrg.Organization__r;
 //   this.Medicalquery.Mylan_Medical_Contact__r = JSON.parse(this.Medicalquery.Mylan_Medical_Contact__r);
    // console.log("MedicalQuery",this.Medicalquery)
    //    let saveMedQuery = {
    //    };
    //   //  console.log(" this.customerproduct;", this.customerproduct);
    //   //  let productData = JSON.stringify(this.customerproduct);
    //   //  console.log("productData",productData);

    //    saveMedQuery["Customer__r"]=this.customerName;
    //    saveMedQuery["Organization__r"] = this.customerOrg;
    //    saveMedQuery["Email1__c"]=this.email;
    //    saveMedQuery["Mobile_Phone__c"]=this.mobileNumber;
    //    saveMedQuery["Creation_Date_Time__c"]=this.Medicalquery.CreatedDate;
    //    saveMedQuery["Pharma_Product__r"] = this.Medicalquery.Pharma_Product__r;
    //    saveMedQuery["Mylan_Medical_Contact__r"] = this.Medicalquery.Mylan_Medical_Contact__r;
    //    saveMedQuery["Owner"] = this.loggedInUserDetails;
    //    saveMedQuery["Status__c"] = "Draft";
    //    saveMedQuery["Query_description__c"] = this.Medicalquery.description;
    //    saveMedQuery["Non_Customer__c"] = this.Medicalquery.noncustomer;
    //    saveMedQuery["MQ_Speciality__c"] = this.speciality;
    //    saveMedQuery["Pharma_Product__c"] = this.Medicalquery.Pharma_Product__r.Id;
    //    saveMedQuery["Mylan_Medical_Contact__c"] = this.Medicalquery.Mylan_Medical_Contact__r.Id;
       
    //    //saveMedQuery["_soupEntryId"]= this.Medmedquery._soupEntryId?this.Medmedquery._soupEntryId:null;
    //    saveMedQuery["Type_of_Medical_Query__c"] = this.Medicalquery.typeOfQuery;
    //    console.log("saveMedQuery",saveMedQuery);
    //    let success = (data) => {
    //      console.log("Data Upserted successfully and created data is",data);
    //      this.navParams.get("parentPage").reloadMedQuery();
    //      this.navCtrl.pop();
   
    //    }
    //    let failure = (error) => {
    //      console.log("Data Upsertion having an issue",error);
    //    }
   
    //    this.smartStore().upsertSoupEntries("MedicalQuery", [saveMedQuery], success, failure); 
   }
   validateInputData()
   {
     console.log("message",this.message);
    // console.log("this.validateInput",this.validateInput());
     if(this.validateInput())
     {
       console.log("saveMedQuery");
       this.saveMedQuery();
     }
     else
     {
      this.message = this.message.startsWith('\n')?this.message.substring(2):this.message;
      const toast = this.toastCtrl.create({
         message: this.message,
         showCloseButton:true,
         cssClass:"toastClass",
         duration:7000,
       });
       toast.present();
     }
     
   }
   validateInput()
   {
     var isValid = true;
     this.message = "";
     
     var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if(!re.test(this.email)) {
        isValid = false;
       this.message+="\n Enter Valid Email";
      }

     if(this.createdDate =='' || this.createdDate==null)
     {
       isValid = false;
       this.message+="\n Creation Date-Time Mandatory";
     }
     if(this.product=='' || this.product==null)
     {
       isValid = false;
       this.message+="\n Product Name Mandatory";
     }
   
     if((this.medicalContact=='' || this.medicalContact==null) && (this.noncustomer=='' ||  this.noncustomer==null))
     {
       isValid = false;
       this.message+="\n  Customer Name Mandatory ";
     }
    //  if((typeof(this.Medicalquery.noncustomer)!=undefined && (this.Medicalquery.noncustomer!='' ||  this.Medicalquery.noncustomer!=null)) && (typeof(this.customerName)!=undefined && ( this.customerName!='' || this.customerName!=null)) )
    //  {
    //    isValid = false;
    //    this.message+="\n please enter only Customer and Non Customer ";
    //  }
    console.log("dates",new Date(this.createdDate), new Date());
     if(new Date(this.createdDate) < new Date())
     {
       isValid = false;
       this.message+="\n  Created date Value cannot be less than the current Date";
     }
    
     if((this.medicalContact=='' || this.medicalContact==null))
     {
       isValid = false;
       this.message+="\n Mylan medical contact value is Mandatory";
     }

     if((this.mediaquery=='' || this.mediaquery==null))
     {
       isValid = false;
       this.message+="\n Media query is Mandatory";
     }
     return isValid;  
   }
}
