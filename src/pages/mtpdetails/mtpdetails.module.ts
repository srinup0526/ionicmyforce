import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MtpdetailsPage } from './mtpdetails';

@NgModule({
  declarations: [
    MtpdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(MtpdetailsPage),
  ],
})
export class MtpdetailsPageModule {}
