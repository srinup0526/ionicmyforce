import { Component } from '@angular/core';
import {IonicPage, NavController, PopoverController, NavParams, AlertController, NavOptions} from 'ionic-angular';
import { AppointmentDetailsPage } from '../appointment-details/appointment-details';
import { CallReportDetailsPage } from '../call-report-details/call-report-details';
import { MTPCollection } from '../../collections/MTPCollection';
import { PopoverComponent } from '../../components/popover/popover';
import { MTPScheme } from '../../models/scheme/MTPScheme';
import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import { CallReportsCollection } from '../../collections/CallReportsCollection/CallReportsCollection';
import { CallReport } from '../../models/CallReport';
/**
 * Generated class for the MtpdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mtpdetails',
  templateUrl: 'mtpdetails.html',
})
export class MtpdetailsPage {
  MTP:any;
  row:any;
  rows:any;
  MTPEventId:any;
  loggedInUserDetails:any;
  callsData:Array<CallReport> = [];
  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public mtpCollection:MTPCollection,
    public popoverController:PopoverController,
    public callReportCollection: CallReportsCollection ) {
    this.row = this.navParams.data['row'];
    this.reloadEntity(this.row);
    this.getCallReportData(this.row);
  }

  getCallReportData(mtp) {
    const fieldValues = {};
    fieldValues[CallReportScheme.fields.mtp.sfdc] = mtp.id;
    this.callReportCollection.fetchAllWhere(fieldValues)
    .then(calls=>{ return this.callReportCollection.getAllEntitiesFromResponse(calls);})
    .then(relatedCalls=>{
      console.log("callsData",relatedCalls);
      this.callsData = relatedCalls;
    })
  }
  
  public onTapEditBtnHandler(): void {
    this.editMTPDetails();
  }
  
  public onDismissEditPopoverHandler(unparsedEntity): void {
    this.reloadEntity(unparsedEntity);
  }

  private ionViewDidLoad():void {
  }

  private editMTPDetails(): Promise<any> {
    console.log("edit popover comp",this.MTP);
    let popover= this.popoverController.create(PopoverComponent,{ "row": this.MTP});
    let options: NavOptions = {};
  
    popover.onDidDismiss(this.onDismissEditPopoverHandler.bind(this));
    
    return popover.present(options);
  }
  
  private reloadEntity(unparsedEntity): Promise<any> {
    if(!unparsedEntity || !unparsedEntity._soupEntryId) {
      return Promise.resolve();
    }
    
    return this.mtpCollection.fetchEntityById(unparsedEntity._soupEntryId)
      .then((mtp) => {
        this.MTP = mtp;
      })
  }

  submitApproval(){
  let confirm = this.alertCtrl.create({
    title: `Please confirm..?`,
    message: `Do you want Submit it for approval?`,
    buttons: [
      {
        text: 'No',
        handler: () => {

          confirm.dismiss();
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.MTP.status = MTPScheme.APPROVAL_STATUS_SUBMITTED_OFFLINE;
          let success = (items) => {console.log("items",items);
        }
       
        let failure = (error) => console.error(`Soup Upsert Error: ${error}`);
        console.log("this.MTP",this.MTP);
        this.mtpCollection.updateEntity(this.MTP)
         
      }
      }
    ]
  });

  confirm.present();
   }

   recallApproval(){
    let confirm = this.alertCtrl.create({
      title: `Please confirm..?`,
      message: `Do you want recall it from approval?`,
      buttons: [
        {
          text: 'No',
          handler: () => {
  
            confirm.dismiss();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.MTP.status = MTPScheme.APPROVAL_STATUS_DRAFT;
            this.mtpCollection.updateEntity(this.MTP);           
          }
        }
      ]
    });
  
    confirm.present();
     }
 
   
   itemTappedCallReport(row: any) {
    console.log("customerRow",row);
    this.navCtrl.push(CallReportDetailsPage,{ "row": row });
  }

  deleteMTPdetails(id: string,_soupEntryId:string) {
    let confirmDelete = this.alertCtrl.create({
      title: `Delete MTP?`,
      message: `Are you sure you want to delete`,
      buttons: [
        {
          text: 'No',
          handler: () => {
            confirmDelete.dismiss();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log("delete MTP",this.MTP);
            this.mtpCollection.removeEntity(this.MTP)
              .then((result) => {
                console.log("delete MTP",result);
                 this.navCtrl.pop();
              });
          }
        }
      ]
    });

    confirmDelete.present();
  }

  itemTappedType(row:any){
    console.log("customerRow",row[0]);

    if((this.MTPEventId.Type__c)=='Appointment')
    {
      this.navCtrl.push(AppointmentDetailsPage,{'row':row});
    }
    else
    {
      this.navCtrl.push(CallReportDetailsPage,{'row':row});
    }
  }
}
