import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatchPage } from './patch';

@NgModule({
  declarations: [
    PatchPage,
  ],
  imports: [
    IonicPageModule.forChild(PatchPage),
  ],
})
export class PatchPageModule {}
