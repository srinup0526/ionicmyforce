import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertOptions,
  Select} from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { MenuController } from 'ionic-angular';
import { PatchdetailsPage } from '../patchdetails/patchdetails';
import * as _ from 'lodash';
import { IndiaAppointmentPage } from '../india-appointment/india-appointment';
import { TableHeaderItemOption } from './../../components/table/table-header/TableHeaderItemOption';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { PatchCustomerScheme } from './../../models/scheme/PatchCustomerScheme';
import { PatchCustomerCollection } from '../../collections/PatchCustomerCollection'
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { FilterPanelComponent } from "../../components/filter/filter-panel/filter-panel";
import { Query } from "../../services/common/Query";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { PatchRecordTypePicklistDatasource } from '../../services/db/picklist-managers/datasource/PatchRecordTypePicklistDatasource';
import { PatchPicklistDatasource } from '../../services/db/picklist-managers/datasource/PatchPicklistDatasource';
import { RecordTypeCollection } from '../../collections/RecordTypeCollection';


/**
 * Generated class for the PatchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patch',
  templateUrl: 'patch.html',
})
export class PatchPage {

  public isSearchbarOpened = false;
  public patchCount:any = 0;

  targetNontarget:string = "tc";
  
  references: Array<any> = [];
  
  public tableOptions: LazyTableOption;
  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: PatchCustomerCollection;
  public filterOptions: AlertOptions;
  public settings: SettingsImpl;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('filterType') filterTypeComponent: Select;
  @ViewChild('mainContent') mainContent: ElementRef;
  PatchCustomer:any;
  PatchCustomerBackup:any;
  rows:any;
  activeMenu: string;
  selectedPatch:any = "";
  patchSet: any = [];
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private menuCtrl: MenuController,
              private service: SmartstoreServiceProvider,
              public loadingCtrl:LoadingController,
              public patchCustomerCollection:PatchCustomerCollection,
              public patchRecordTypePicklistDatasource:PatchRecordTypePicklistDatasource,
              public patchPicklistDatasource:PatchPicklistDatasource,
              public recordTypeCollection:RecordTypeCollection) {
        this.settings = Settings.getInstance();
        
        const headerItems = [
        {
          title: 'common.names.Customer',
          fields: [PatchCustomerScheme.fields.contactName],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Frequency',
          fields: [PatchCustomerScheme.fields.frequency],
          isSortable: true,
          isAsc: true,
          isActive: false,
          picklistValues: {}
        },
        {
          title: 'common.names.Class',
          fields: [PatchCustomerScheme.fields.class],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Patch',
          fields: [PatchCustomerScheme.fields.patchName],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.RecordType',
          fields: [PatchCustomerScheme.fields.recordType],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        
        {
          title: 'common.names.Appt',
          fields: [],
          cssClass: ['appt'],
          handler: (event, reference) => {
            this.gotoCreateAppointment(reference);
            event.stopPropagation();
          }
        },
        
        ];
    
    this.tableOptions = {
      headerItems: headerItems,
      rowHandler: (event, patchCustomer) => {
        this.itemTapped(patchCustomer);
      },
      batchSize: 100
    };
    
    this.searchString = '';

    this.collection = this.patchCustomerCollection;
    
     this.filterOptions = {
       cssClass: 'popup alert list without-header',
       buttons: []
     };

     
  }


  public onChangeQueryHandler(query: Query): void {
    this.setPatchCustomerCount();
  }

  private setPatchCustomerCount(): number {
    const countQuery = this.collection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);

    return this.collection.runCountQuery(query.query)
      .then((count) => {
        return this.patchCount = count || 0;
      })
  }
  
  public onSwipeHandler(event){
    console.log("event",event)
    switch (event.offsetDirection) {
      case 2: this.filterPanel.hidePanel(); break;
      case 4: this.filterPanel.showPanel(); break;
    }
  }

  public onFilterChangeHandler(filterData): void {
    this.lazyTable.reloadTableByFilterData(filterData);
  }

  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
  
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public ionViewDidLoad() : void {}

  public ionViewDidEnter(): void {}

  public ionViewWillEnter(): void {
  }
  
  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable();
  }
  loadPatchCustomer() {

    return this.service.loadPatchCustomer()
      .then(results => {
        console.log('Patch',results);
       
         this.PatchCustomer = results.records;
         this.PatchCustomerBackup = results.records;
      })
  }
  toggleMenu() {
    this.menuCtrl.toggle('filters-3');
  }

  itemTapped(row) {
    console.log("rowpatch",row)
     this.navCtrl.push(PatchdetailsPage, { "row": row, parentPage:this });
 
   }

  gotoCreateAppointment(row:any)
  {
    this.navCtrl.push(IndiaAppointmentPage,{ "row": row });
  }
}
