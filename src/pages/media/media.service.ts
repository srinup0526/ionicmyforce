import {EventEmitter, Injectable} from '@angular/core';
import {PresentationsCollection} from '../../collections/PresentationsCollection';
import {Presentation} from '../../models/Presentation';
import {PresentationsLoadManager} from './PresentationLoaderManager';
import {PresentationFileManager} from './PresentationFileManager';
import {PresentationViewer} from './PresentationViewer';
import {PresentationLoader} from './PresentationLoader';
import {Alert, AlertController, AlertOptions} from 'ionic-angular';
import {LocalizationManager} from '../../services/common/localizations/LocalizationManager';
import {Utils} from '../../utils/Utils';

@Injectable({
  providedIn: 'root'
})
export class MediaService {
  isDownloadingAll: boolean = false;
  downloadInvoke: EventEmitter<string> = new EventEmitter();

  private list: Presentation[] = [];

  constructor(public presentationsLoadManager: PresentationsLoadManager,
              private presentationsCollection: PresentationsCollection,
              private presentationFileManager: PresentationFileManager,
              private presentationViewer: PresentationViewer,
              private localizationManager: LocalizationManager,
              private alertCtrl: AlertController) {
  }


  getAll(): Promise<Presentation[]> {
    return this.presentationsCollection.fetchAll()
      .then((response) => this.presentationsCollection.getAllEntitiesFromResponse(response))
      .then((list: Presentation[]) => this.list = list);
  }


  download(presentation: Presentation): PresentationLoader {
    return this.presentationsLoadManager.queueInvoke(presentation.id, presentation.url);
  }


  cancelDownload(presentationId: string) {
    return this.presentationsLoadManager.dequeueInvoke(presentationId);
  }


  downloadAll(): void {
    if (Utils.deviceIsOnline()) {
      this.isDownloadingAll = true;
      this.list.forEach(this.initLoader.bind(this));
    } else {
      this.showConnectionErrorPopup();
    }
  }


  open(presentationId: string): Promise<any> {
    return this.presentationFileManager.presentationExist(presentationId)
      .then(() => {
        return this.presentationViewer.open(presentationId)
          .then(() => {
            return this.presentationViewer.getKPI()
              .then((presentationKPI) => {
                // callback for close method not working on android
                this.presentationViewer.close();
                return presentationKPI;
              });
          });
      });
  }


  private initLoader(presentation: Presentation) {
    if (presentation.hasUpdate() && !this.presentationsLoadManager.getLoaderForId(presentation.id)) {
      this.downloadInvoke.emit(presentation.id);
    }
  }


  async showDownloadErrorPopup(): Promise<Alert> {
    const title: string = await this.localizationManager.promiseLocale('Media.DownloadPresentationErrorCaption');
    const message: string = await this.localizationManager.promiseLocale('Media.DownloadPresentationErrorMessage');
    return this._showAlert(title, message);
  }


  async showConnectionErrorPopup(): Promise<Alert> {
    const title: string = await this.localizationManager.promiseLocale('home.AlertPopup.Caption');
    const message: string = await this.localizationManager.promiseLocale('home.AlertPopup.Message');
    return this._showAlert(title, message);
  }


  private async _showAlert(title: string, message: string, _okLabel?: string): Promise<Alert> {
    let okLabel = _okLabel;

    if (!_okLabel) {
      okLabel = await this.localizationManager.promiseLocale('common.buttons.OkBtn');
    }

    const options: AlertOptions = {
      title: title,
      message: message,
      cssClass: 'popup alert single-button',
      buttons: [
        {
          text: okLabel,
          role: 'cancel'
        }
      ]
    };

    const alert = await this.alertCtrl.create(options);

    return alert.present();
  }


}
