import {PresentationFileManager} from './PresentationFileManager';
import {FileTransferObject} from '@ionic-native/file-transfer';
import {SforceDataContext} from '../../collections/SforceDataContext';
import {ZipManager} from './ZipManager';
import {BehaviorSubject} from "rxjs";

export enum PresentationLoaderState {
  INITED,
  DOWNLOAD,
  UNZIP,
  REPLACING,
  FINISHED,
  FAILED
}

export interface IPresentationLoaderState {
  total: number,
  loaded: number,
  state: number,
}

export class PresentationLoader {
  loaderState: BehaviorSubject<IPresentationLoaderState>;
  state: BehaviorSubject<PresentationLoaderState>;
  fileSize: BehaviorSubject<number>;
  loadedSize: BehaviorSubject<number>;
  progress: BehaviorSubject<number>;
  error: any;

  private presentationId: string;
  private temporaryDirectory: string;
  private temporaryFile: string;
  private baseUrl: string;
  private isTokenRefreshed: boolean;
  private fileTransfer: FileTransferObject;

  static presentationFileManager: PresentationFileManager;

  static init(presentationFileManager: PresentationFileManager) {
    PresentationLoader.presentationFileManager = presentationFileManager;
  }


  constructor(presentationId: string, baseUrl: string) {
    const initialState: IPresentationLoaderState = {
      state: PresentationLoaderState.INITED,
      total: 0,
      loaded: 0
    };
    this.loaderState = new BehaviorSubject<IPresentationLoaderState>(initialState);
    this.state = new BehaviorSubject<PresentationLoaderState>(PresentationLoaderState.INITED);
    this.fileSize = new BehaviorSubject<number>(0);
    this.loadedSize = new BehaviorSubject<number>(0);
    this.progress = new BehaviorSubject<number>(0);
    this.error = null;

    this.presentationId = presentationId;
    this.baseUrl = baseUrl;

    this.temporaryDirectory = PresentationLoader.presentationFileManager.getPathToTemporaryDir();
    this.temporaryFile = `${this.temporaryDirectory}/${this.presentationId}.zip`;
  }

  download() {
    this.state.next(PresentationLoaderState.DOWNLOAD);
    this.isTokenRefreshed = false;
    this._initFileTransfer();
  }

  abort() {
    if (this.canStop()) {
      this.fileTransfer.abort();
      this.state.next(PresentationLoaderState.INITED);
    }
  }

  canStop() {
    return this.state.value === PresentationLoaderState.DOWNLOAD
      || this.state.value === PresentationLoaderState.FINISHED
      || this.state.value === PresentationLoaderState.FAILED;
  }

  errorHandler(error) {
    console.log(`File transfer error handler called: ${JSON.stringify(error)}`);
    this.error = error;
    this.state.next(PresentationLoaderState.FAILED);
    this._cleanup();
  }

  _initFileTransfer() {
    return this._buildUrl()
      .then((url: string) => {
        if (this.isTokenRefreshed) {
          setTimeout(() => {
            this._loadPresentation(url);
          }, 5000);
        } else {
          this._loadPresentation(url);
        }
      });
  }

  _loadPresentation(url: string) {
    this.fileTransfer = new FileTransferObject();
    this.fileTransfer.onProgress((progress: ProgressEvent) => this._onProgressHandler(progress));
    return this.fileTransfer.download(url, this.temporaryFile, true)
      .then((entry) => this._onDownloadSuccess(entry))
      .catch((error) => this._onDownloadError(error));
  }

  _buildUrl(): Promise<string> {
    return SforceDataContext.getAuthCredentials()
      .then(credentials => {
        return `${this.baseUrl}&accessToken=${credentials.accessToken}&instanceUrl=${credentials.instanceUrl}`;
      });
  }

  _cleanup() {
    PresentationLoader.presentationFileManager.removeArchive(this.presentationId);
    PresentationLoader.presentationFileManager.removePresentationById(this.presentationId);
  }

  _onDownloadSuccess(entry) {
    this._unzip(entry.nativeURL);
  }

  _onDownloadError(error) {
    if (error && error.http_status === 404 && !this.isTokenRefreshed) {
      this.isTokenRefreshed = true;
      this.abort();
      return SforceDataContext.refreshToken()
        .then(() => this._initFileTransfer());
    } else {
      return this.errorHandler(error);
    }
  }

  _onUnzipSuccess() {
    this.state.next(PresentationLoaderState.REPLACING);
    return PresentationLoader.presentationFileManager.replacePresentation(this.presentationId)
      .then(() => PresentationLoader.presentationFileManager.removeArchive(this.presentationId))
      .then(() => this.state.next(PresentationLoaderState.FINISHED))
      .catch((error) => this.errorHandler(error));
  }

  _unzip(source: string) {
    this.state.next(PresentationLoaderState.UNZIP);
    const destination = `${this.temporaryDirectory}/${this.presentationId}`;
    const zipManager = new ZipManager(source, destination);
    return zipManager.unzip(this._onUnzipSuccess.bind(this), this.errorHandler.bind(this));
  }

  private _onProgressHandler(progress: ProgressEvent) {
    const fileSizeMb = Math.round(progress.total / 1024 / 1024 * 100) / 100;
    const loadedSizeMb = Math.round(progress.loaded / 1024 / 1024 * 100) / 100;
    const progressValue = loadedSizeMb / fileSizeMb * 100;
    this.fileSize.next(fileSizeMb);
    this.loadedSize.next(loadedSizeMb);
    this.progress.next(progressValue);
  }

}
