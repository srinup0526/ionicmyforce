import { Injectable } from '@angular/core';
import {PresentationFileManager} from './PresentationFileManager';
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";

declare let cordova: any;

@Injectable({
  providedIn: 'root'
})
export class PresentationViewer {
  private static ENTRY_FILENAME = 'index.html';
  private static PLUGIN_NAME = 'PresentationViewer';

  private static EVENT_DID_LOAD = 'DID_LOAD';
  private static EVENT_ON_COMPLETE = 'COMPLETE';

  private static OPEN_COMMAND = 'openPresentation';
  private static CLOSE_COMMAND = 'closePresentation';
  private static GET_KPI_COMMAND = 'getKPI';

  constructor(private presentationFileManager: PresentationFileManager,
              private localizationManager: LocalizationManager) {}


  open(presentationId: string, didLoadCb = () => {}): Promise<void> {
    return new Promise((resolve, reject) => {
      const index = this._getPathToEntryComponent(presentationId);

      const completeKey = 'PresentationViewer.Complete';
      const pauseKey = 'PresentationViewer.Pause';
      const resumeKey = 'PresentationViewer.Resume';
      const pausedKey = 'PresentationViewer.SuspendedLabel';

      this.localizationManager.getSeveralLocales([completeKey, pauseKey, resumeKey, pausedKey])
        .then((locales) => {
          const translations = {
            Complete: locales[completeKey],
            Pause: locales[pauseKey],
            Resume: locales[resumeKey],
            SuspendedLabel: locales[pausedKey]
          };

          const params = {index, translations};

          const onViewingEvent = (eventMessage) => {
            if (eventMessage === PresentationViewer.EVENT_DID_LOAD) {
              didLoadCb();
            }

            if (eventMessage === PresentationViewer.EVENT_ON_COMPLETE) {
              resolve();
            }
          };

          const onViewingError = (errorMessage) => {
            reject(errorMessage);
          };

          cordova.exec(onViewingEvent, onViewingError, PresentationViewer.PLUGIN_NAME, PresentationViewer.OPEN_COMMAND, [params]);
        });
    });
  }

  close() {
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, PresentationViewer.PLUGIN_NAME, PresentationViewer.CLOSE_COMMAND, []);
    });
  }

  getKPI(): Promise<string> {
    return new Promise((resolve, reject) => {
      cordova.exec(resolve, reject, PresentationViewer.PLUGIN_NAME, PresentationViewer.GET_KPI_COMMAND, []);
    });
  }


  private _getPathToEntryComponent(presentationId: string): string {
    return `${this.presentationFileManager.getPathToPresentation(presentationId)}/${PresentationViewer.ENTRY_FILENAME}`;
  }

}
