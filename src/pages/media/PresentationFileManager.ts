import { Injectable } from '@angular/core';
import {DirectoryEntry, File, FileEntry, FileError} from '@ionic-native/file';
import {Presentation} from "../../models/Presentation";

declare let window: any;

@Injectable({
  providedIn: 'root'
})
export class PresentationFileManager {
  private rootEntry: DirectoryEntry;
  private rootPath: string;
  private readonly presentationsRoot: string;
  private readonly temporaryRoot: string;

  constructor() {
    this.presentationsRoot = 'presentations';
    this.temporaryRoot = 'temporary';
  }

  initFileSystem() {
    return new Promise((resolve, reject) => {
      if (window.requestFileSystem) {
        const fs = window.LocalFileSystem && window.LocalFileSystem.PERSISTENT;
        window.requestFileSystem(fs, 0, (fileSystem) => {
          this.rootPath = fileSystem.root.toURL().replace('file://localhost', '');
          this.rootEntry = fileSystem.root;
          resolve();
        }, reject);
      } else {
        console.log('File system unavailable');
        reject(new Error('File system unavailable'));
      }
    });
  }


  getPathToTemporaryDir(): string {
    return `${this.rootPath}/${this.temporaryRoot}`;
  }

  getPathToPresentationsDir(): string {
    return `${this.rootPath}/${this.presentationsRoot}`;
  }

  getPathToTemporaryPresentation(presentationId: string): string {
    return `${this.getPathToTemporaryDir()}/${presentationId}`;
  }

  getPathToPresentation(presentationId: string): string {
    return `${this.getPathToPresentationsDir()}/${presentationId}`;
  }

  presentationExist(presentationId: string) {
    return this._getPresentationsDir()
      .then(presentationDir => {
        return this._getDirectory(presentationDir, presentationId, false);
      });
  }

  getPresentationIconPath(presentationId: string): Promise<string> {
    const emptyPath = '';
    const retinaIconPath = `${this.presentationsRoot}/${presentationId}/${Presentation.RETINA_ICON}`;
    const iconPath = `${this.presentationsRoot}/${presentationId}/${Presentation.ICON}`;
    return this._getFile(this.rootEntry, retinaIconPath)
      .then((fileEntry: FileEntry) => fileEntry.nativeURL)
      .catch((error: FileError) => {
        return this._getFile(this.rootEntry, iconPath)
          .then((fileEntry: FileEntry) => fileEntry.nativeURL)
          .catch((error: FileError) => {
            return emptyPath;
          });
      });
  }

  readPresentationFile(pathToFile: string) {
    return new Promise((resolve, reject) => {
      const filePath = `${this.presentationsRoot}/${pathToFile}`;
      const onReadError = (error) => {
        reject(error);
      };
      const onReadSuccess = (file) => {
        const fileReader = new FileReader();
        fileReader.onloadend = (event) => {
          resolve(fileReader.result);
        };
        fileReader.onerror = onReadError;
        fileReader.readAsText(file);
      };

      this.rootEntry.getFile(
        filePath,
        {create: false, exclusive: false},
          (fileEntry) => {
            fileEntry.file(onReadSuccess, onReadError);
        },
        onReadError
      );
    });
  }

  presentationFileExist(presentationId: string, filePath: string) {
    return this._getPresentationsDir()
      .then(presentationDir => {
        return this._getDirectory(presentationDir, presentationId);
      })
      .then((presentationDir: DirectoryEntry) => {
        return this._getFile(presentationDir, filePath, false);
      });
  }

  isPresentationFileExist(presentationId: string, filePath: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.presentationFileExist(presentationId, filePath)
        .then(() => resolve(true))
        .catch(() => resolve(false));
    });
  }

  replacePresentation(presentationId: string) {
    return this.removePresentationById(presentationId)
      .then(() => this._getTempPresentationsDir())
      .then((tempDir: DirectoryEntry) => {
        return this._getDirectory(tempDir, presentationId);
      })
      .then((tempPresentationDir: DirectoryEntry) => {
        return this._getPresentationsDir()
          .then((presentationDir: DirectoryEntry) => {
            return this.moveEntry(tempPresentationDir, presentationDir);
          });
      });
  }

  wipePresentationsStore(): Promise<any> {
    return this._getPresentationsDir()
      .then((presentationsDir: DirectoryEntry) => this._removeDirectory(presentationsDir));
  }

  removeArchive(presentationId: string) {
    return this._getTempPresentationsDir()
      .then((presentationsTemporaryDir: DirectoryEntry) => {
        const path = `${presentationId}.zip`;
        return this._getFile(presentationsTemporaryDir, path);
      })
      .then(this._removeFile.bind(this));
  }

  removePresentationById(presentationId: string) {
    return this._getPresentationsDir()
      .then((presentationDir: DirectoryEntry) => this._getDirectory(presentationDir, presentationId))
      .then((targetDir: DirectoryEntry) => this._removeDirectory(targetDir));
  }

  private _removeDirectory(dirEntry: DirectoryEntry): Promise<any> {
    return new Promise((resolve, reject) => {
      dirEntry.removeRecursively(resolve, (fileError) => {
        if (fileError.code === FileError.PATH_EXISTS_ERR || fileError.code === FileError.NOT_FOUND_ERR) {
          resolve(null);
        } else {
          reject(fileError);
        }
      });
    });
  }

  private moveEntry(fsEntry, newParent) {
    return this._transferEntry(fsEntry, newParent);
  }

  private _transferEntry(entry, toDirectory, action = 'move', name = entry.name) {
    return new Promise((resolve, reject) => {
      entry[`${action}To`](toDirectory, name, resolve, reject);
    });
  }

  private _getFile(dirEntry: DirectoryEntry, fileName: string, create: boolean = true): Promise<FileEntry> {
    return new Promise((resolve, reject) => {
      dirEntry.getFile(fileName, {create}, resolve, reject);
    });
  }

  private _removeFile(fileEntry: FileEntry) {
    return new Promise((resolve, reject) => {
      fileEntry.remove(resolve, (fileError) => {
        const errorCode = fileError.code;
        if (errorCode === FileError.NOT_FOUND_ERR || errorCode === FileError.NO_MODIFICATION_ALLOWED_ERR) {
          resolve(fileEntry);
        } else {
          reject(fileError);
        }
      });
    });
  }

  private _getDirectory(dirEntry: DirectoryEntry, dirPath: string, create: boolean = true): Promise<DirectoryEntry> {
    return new Promise((resolve, reject) => {
      dirEntry.getDirectory(dirPath, {create, exclusive: false}, resolve, reject);
    });
  }

  private _getTempPresentationsDir(): Promise<DirectoryEntry> {
    return this._getDirectory(this.rootEntry, this.temporaryRoot);
  }

  private _getPresentationsDir(): Promise<DirectoryEntry> {
    return this._getDirectory(this.rootEntry, this.presentationsRoot);
  }


}
