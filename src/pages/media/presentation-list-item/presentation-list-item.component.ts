import {ChangeDetectorRef, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AlertController, ModalController, ModalOptions, NavController, Platform} from 'ionic-angular';
import {Presentation} from '../../../models/Presentation';
import {PresentationFileManager} from '../PresentationFileManager';
import {PresentationsCollection} from '../../../collections/PresentationsCollection';
import {ProductPresentationsCollection} from '../../../collections/ProductPresentationsCollection';
import {MediaService} from '../media.service';
import {PresentationLoader, PresentationLoaderState} from '../PresentationLoader';
import {ConfirmPopup} from "../../../components/popups/ConfirmPopup";
import {ListPopup} from "../../../components/popups/ListPopup";
import {ReferenceSelectPopupComponent} from "../../../components/reference-select-popup/reference-select-popup";
import {LocalizationManager} from '../../../services/common/localizations/LocalizationManager';
import {Subscription} from 'rxjs';
import {Utils} from '../../../utils/Utils';
import { Settings } from '../../../services/db/Settings';
import {NumberValidator} from "../../../utils/NumberValidator";
import {Validators} from "@angular/forms";
import { CallReportPage } from '../../call-report/call-report';
import {Reference} from "../../../models/Reference";

declare let window;

@Component({
  selector: 'app-presentation-list-item',
  templateUrl: './presentation-list-item.component.html',
})
export class PresentationListItemComponent implements OnInit, OnDestroy {
  @Input()
  presentation: Presentation;
  currentSize: number;
  totalSize: number;
  progress: number;
  icon: string;
  isUpdateAvailable: boolean;
  isDownloadAvailable: boolean;
  loader: PresentationLoader|null;

  private loaderSubscription: Subscription;
  private stateSubscription: Subscription;
  private progressSubscription: Subscription;
  private isDownloadingErrorHandling: boolean;


  constructor(public navCtrl: NavController,
              private presentationFileManager: PresentationFileManager,
              private presentationsCollection: PresentationsCollection,
              private productPresentationsCollection: ProductPresentationsCollection,
              private localizationManager: LocalizationManager,
              private alertCtrl: AlertController,
              private modalCtrl: ModalController,
              public mediaService: MediaService,
              private platform: Platform,
              private changeDetectorRef: ChangeDetectorRef) {
    this.isDownloadingErrorHandling = true;
  }


  ngOnInit() {
    this._resetDownloadValues();
    this._updateButtonsState();
    this._setPresentationIcon();
    this._subscribeDownloadAllEvent()
  }


  ngOnDestroy() {
    this._unSubscribeDownloadAllEvent();
  }


  onClickDownload(event: Event): void {
    event.stopPropagation();
    this._doDownload();
  }


  onClickUpdate(event: Event): void {
    event.stopPropagation();
    this._doDownload();
  }


  onClickCancel(event: Event): void {
    event.stopPropagation();
    this._doCancelDownload();
  }


  onClickOpen(event: Event): void {
    event.stopPropagation();
    this._doOpen();
  }


  private _resetDownloadValues() {
    this.isDownloadingErrorHandling = true;
    this.loader = null;
    this.progress = 0;
  }


  private _updateButtonsState() {
    this.isUpdateAvailable = this.presentation.wasDownloaded() && this.presentation.hasUpdate();
    this.isDownloadAvailable = !this.presentation.wasDownloaded();
  }


  private _updatePresentationEntity() {
    return this.presentationsCollection.updateEntity(this.presentation);
  }


  private _updatePresentationIcon(): Promise<void> {
    return this.presentationFileManager.getPresentationIconPath(this.presentation.id)
      .then((nativeUrl: string) => {
        this.presentation.iconPath = nativeUrl;
      })
  }


  private _setPresentationIcon() {
    if (this.presentation.wasDownloaded()) {
      const timestamp = new Date().getTime();
      const filePath = `${this.presentation.iconPath}?${timestamp}`;
      const isIOS = this.platform.is('ios');
      const iOSFilePath = `${window.Ionic.WebView.convertFileSrc(filePath)}`;
      this.icon = isIOS ? iOSFilePath : filePath;
    } else {
      this.icon = '';
    }
  }


  private _doDownload() {
    if (Utils.deviceIsOnline()) {
      this.loader = this.mediaService.download(this.presentation);
      this._loaderSubscribe();
    } else {
      this.mediaService.showConnectionErrorPopup();
    }
  }


  private _doCancelDownload() {
    if (this.loader.canStop()) {
      this.mediaService.cancelDownload(this.presentation.id);
      this._loaderUnSubscribe();
      this.loader = null;
    }

  }


  private _doOpen() {
    if (this.loader) {
      return;
    }

    this.mediaService.open(this.presentation.id)
      .then((presentationKPI) => {
        this.showCallReportFromMediaConfirm(presentationKPI);
      });
  }


  private showCallReportFromMediaConfirm(presentationKPI){
    if(Settings.getInstance().isCallReportFromMediaEnabled()) {
      const confirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);

      return confirmPopup.showPopup({
        title: '',
        message: 'Media.CreateCallReportConfirmPopup.Message',
        yesLabel: 'common.buttons.YesBtn',
        noLabel: 'common.buttons.NoBtn'
      })
        .then(async (action) => {

          if(action.doAction){

            return this.showSelectProductPopup(presentationKPI);

          }
        });
    }
  }


  private showSelectProductPopup(presentationKPI){
    return this.productPresentationsCollection.getProductsByPresentation(this.presentation.id)
      .then(async (products) => {

        if (products.length === 1) {
          return this.showReferencePopup(products[0].product, presentationKPI);
        }

        const productList = products.map((item) => {
          return {
            id:item.product,
            description: item.productName
          }
        });

        const title = await this.localizationManager.promiseLocale('Media.CreateCallReportSelectProductPopup.Message');
        const listPopup = new ListPopup(this.alertCtrl, productList, title);

        return listPopup.showPopup()
          .then((item) => {
            const productId = item.id;
            this.showReferencePopup(productId, presentationKPI);
          });
      });
  }


  private showReferencePopup(productId: string, presentationKPI: any){
    const componentOptions = {};

    const popupOptions: ModalOptions = {
      cssClass: 'reference-list-modal-popup',
      enterAnimation: 'modal-fade-in',
      leaveAnimation: 'modal-fade-out '
    };

    const profileModal = this.modalCtrl.create(ReferenceSelectPopupComponent, componentOptions, popupOptions);

    profileModal.onDidDismiss((options: { reference: Reference }) => {
      if(options && options.reference) {

        const kpiData = [{
          productId: productId,
          presentations: [{
            presentationId: this.presentation.id,
            kpis: [presentationKPI]
          }]
        }];

        this.navCtrl.push(CallReportPage,{ row: options.reference, kpi: kpiData});

        console.log('@@ create call report', options.reference, productId, presentationKPI);
      }
    });

    return profileModal.present();
  }


  private _loaderSubscribe() {
    if (this.loader) {
      this.stateSubscription = this.loader.state.subscribe(value => this._onLoaderStateChange(value));
      this.progressSubscription = this.loader.progress.subscribe(value => this._onLoaderProgressChange(value));
    }
  }


  private _loaderUnSubscribe() {
    if (this.stateSubscription) {
      this.stateSubscription.unsubscribe();
      this.progressSubscription.unsubscribe();
    }
  }


  private _onLoaderStateChange(state: PresentationLoaderState) {
    if (state === PresentationLoaderState.FINISHED) {
      this.onSuccess();
    }

    if (state === PresentationLoaderState.FAILED) {
      this.onFail();
    }
  }


  private _onLoaderProgressChange(value) {
    this.progress = value;
  }


  private onSuccess() {
    this._updatePresentationIcon()
      .then(() => {
        this.presentation.currentVersion = this.presentation.availableVersion;
        return this._updatePresentationEntity();
      })
      .then(() => {
        this._resetDownloadValues();
        this._updateButtonsState();
        this._setPresentationIcon();
        this.mediaService.cancelDownload(this.presentation.id);
        this.changeDetectorRef.detectChanges();
      });
  }


  private onFail() {
    this.mediaService.cancelDownload(this.presentation.id);
    this._loaderUnSubscribe();
    if (this.isDownloadingErrorHandling) {
      this.mediaService.showDownloadErrorPopup();
    }
    this._resetDownloadValues();
    this.changeDetectorRef.detectChanges();
  }


  private _subscribeDownloadAllEvent() {
    this.loaderSubscription = this.mediaService.downloadInvoke.subscribe(this._onPresentationStartDownload.bind(this))
  }


  private _unSubscribeDownloadAllEvent() {
    if (this.loaderSubscription) {
      this.loaderSubscription.unsubscribe();
    }
  }


  private _onPresentationStartDownload(presentationId: string) {
    if (presentationId === this.presentation.id && !this.loader) {
      this.isDownloadingErrorHandling = false;
      this._doDownload();
    }
  }

}
