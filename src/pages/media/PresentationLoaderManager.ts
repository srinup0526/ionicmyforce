import {PresentationLoader} from './PresentationLoader';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PresentationsLoadManager {
  queue: any;


  constructor() {
    this.queue = {};
  }


  getLoaderForId(presentationId) {
    return this.queue[presentationId] || null;
  }


  getQueuedPresentations() {
    return Object.keys(this.queue);
  }


  setQueue(presentationId: string, remotePath: string): PresentationLoader  {

    if (this.queue[presentationId]) {
      console.log(`Presentation ${presentationId} already queued!`);
      return null;
    }

    this.queue[presentationId] = new PresentationLoader(presentationId, remotePath);

    return this.queue[presentationId];

  }


  queueInvoke(presentationId: string, remotePath: string): PresentationLoader {
    const loader = this.setQueue(presentationId, remotePath);
    if (loader) {
      loader.download();
    }
    return loader;
  }


  dequeue(presentationId: string): void {
    const loader = this.getLoaderForId(presentationId);
    if (loader.canStop()) {
      delete this.queue[presentationId];
      loader.abort();
    }
  }


  dequeueInvoke(presentationId): void {
    this.dequeue(presentationId);
  }

}
