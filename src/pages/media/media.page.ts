import { Component, OnInit } from '@angular/core';
import {MediaService} from './media.service';
import {Presentation} from '../../models/Presentation';

@Component({
  selector: 'app-media',
  templateUrl: './media.page.html'
})
export class MediaPage implements OnInit {
  public list: Presentation[];


  constructor(public mediaService: MediaService) {
    this.list = [];
  }


  ngOnInit() {}


  ionViewDidEnter() {
    this.getList();
  }


  onClickDownloadAll(): void {
    this.mediaService.downloadAll();
  }


  private getList(): Promise<void> {
    return this.mediaService.getAll()
      .then((response: Presentation[]) => {
        console.log("response", response);
        this.list = response;
      });
  }

}
