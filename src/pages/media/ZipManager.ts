declare let cordova;

export class ZipManager {
  private zip: any;
  private sourceFile: any;
  private destinationFolder: any;

  constructor(sourceFile, destinationFolder) {
    this.sourceFile = sourceFile;
    this.destinationFolder = destinationFolder;
    this.zip = cordova.require('com.qapint.cordova.zip.Zip');
  }

  unzip(onSuccess, onError) {
    this.zip.unzip(this.sourceFile, this.destinationFolder, onSuccess, onError);
  }
}
