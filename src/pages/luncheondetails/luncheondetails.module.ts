import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LuncheondetailsPage } from './luncheondetails';

@NgModule({
  declarations: [
    LuncheondetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(LuncheondetailsPage),
  ],
})
export class LuncheondetailsPageModule {}
