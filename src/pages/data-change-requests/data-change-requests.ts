import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { Query } from "../../services/common/Query";
import { FilterPanelComponent } from "../../components/filter/filter-panel/filter-panel";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { DataChangeRequestsCollection } from "../../collections/DataChangeRequestsCollection";
import { DataChangeRequestScheme } from './../../models/scheme/DataChangeRequestScheme';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { DataChangeRequestStatusPicklistDatasource } from './../../services/db/picklist-managers/datasource/DataChangeRequestStatusPicklistDatasource';
import { DataChangeRequestTypePicklistDatasource } from './../../services/db/picklist-managers/datasource/DataChangeRequestTypePicklistDatasource';
import { PicklistPopup } from './../../components/popups/PicklistPopup';
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {ContactEditCardPage} from "../contact-edit-card/contact-edit-card";
import {Contact} from "../../models/Contact";
import {DataChangeRequest} from "../../models/DataChangeRequest";
import {DataChangeRequestsContactCardPage} from "../data-change-requests-contact-card/data-change-requests-contact-card";
import {OrganizationEditCardPage} from "../organization-edit-card/organization-edit-card";
import {Organization} from "../../models/Organization";
import {DataChangeRequestsOrganizationCardPage} from "../data-change-requests-organization-card/data-change-requests-organization-card";
import {ReferenceEditCardPage} from "../reference-edit-card/reference-edit-card";
import {Reference} from "../../models/Reference";
import {DataChangeRequestsReferenceCardPage} from "../data-change-requests-reference-card/data-change-requests-reference-card";
import {ContactChinaEditCardPage} from "../contact-china-edit-card/contact-china-edit-card";
import {OrganizationChinaEditCardPage} from "../organization-china-edit-card/organization-china-edit-card";
import {DataChangeRequestsSegmentationListPage} from "../data-change-requests-segmentation-list/data-change-requests-segmentation-list";
import {DataChangeRequestsSegmentationCardPage} from "../data-change-requests-segmentation-card/data-change-requests-segmentation-card";


@IonicPage()
@Component({
  selector: 'page-data-change-requests',
  templateUrl: 'data-change-requests.html',
})
export class DataChangeRequestsPage {
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: DataChangeRequestsCollection;
  public settings: SettingsImpl;
  public Query;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  
  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private alertCtrl: AlertController,
              private localizationManager: LocalizationManager,
              private DataChangeRequestsCollection: DataChangeRequestsCollection,
              public dcrStatusPicklistDatasource: DataChangeRequestStatusPicklistDatasource,
              public dcrTypePicklistDatasource: DataChangeRequestTypePicklistDatasource
              ) {
    
    this.settings = Settings.getInstance();
    this.Query = Query;
    
    this.collection = this.DataChangeRequestsCollection;
    
    this.searchString = '';
  }
  
  public ngAfterContentInit() {
    this.preparePicklists()
      .then(this.setTableOptions.bind(this))
      .then(() => {
        this.lazyTable.setTableOptions(this.tableOptions);
        this.lazyTable.setSearchString(this.searchString);
        this.lazyTable.setCollection(this.collection);
  
        this.lazyTable.reloadTable();
      });
  }
  
  
  public onSwipeHandler(event){
    switch (event.offsetDirection) {
      case 2: this.filterPanel.hidePanel(); break;
      case 4: this.filterPanel.showPanel(); break;
    }
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onFilterChangeHandler(filterData): void {
    this.lazyTable.reloadTableByFilterData(filterData);
  }
  
  public onTapRowHandler(event, record) {
    this.gotoDCRDetail(record);
    event.stopPropagation();
  }
  
  public onTapCreateBtnHandler(): void {
    this.showTypeCreatePopup();
  }
  
  public ionViewDidEnter() {}
  
  public ionViewWillEnter(): void {
    this.lazyTable.reloadTable();
  }
  
  public ionViewDidLoad() {}
  
  
  private gotoDCRDetail(dcr: DataChangeRequest) {
    if (dcr.isReferenceChangeRequest()){
      return this.navCtrl.push(DataChangeRequestsReferenceCardPage, { "row": dcr });
    }
    
    if (dcr.isContactWithReferenceChangeRequest()){
      return this.navCtrl.push(DataChangeRequestsContactCardPage, { "row": dcr });
    }
  
    if (dcr.isOrganizationChangeRequest()){
      return this.navCtrl.push(DataChangeRequestsOrganizationCardPage, { "row": dcr });
    }
    
    if (dcr.isProductSegmentationChangeRequest()) {
      return this.navCtrl.push(DataChangeRequestsSegmentationCardPage, { "row": dcr });
    }
    
    else
      throw new Error('Unknown DCR type ' + dcr.dcrType);
  }
  
  private setTableOptions([statusPicklist, typePicklist]) {
    this.tableOptions = {
      headerItems: [
        {
          title: 'common.names.Date',
          fields: [DataChangeRequestScheme.fields.dcrDateOfRequest],
          isSortable: true,
          isAsc: true,
          isActive: true,
          modelFunction: "dateOfRequestStr"
        },
        {
          title: 'common.names.Type',
          fields: [DataChangeRequestScheme.fields.dcrType],
          isSortable: true,
          isAsc: true,
          isActive: false,
          picklistValues: typePicklist
        },
        {
          title: 'common.names.Status',
          fields: [DataChangeRequestScheme.fields.dcrStatus],
          isSortable: true,
          isAsc: true,
          isActive: false,
          picklistValues: statusPicklist
        },
        {
          title: 'common.names.Contact',
          fields: [DataChangeRequestScheme.fields.dcrContactLastName, DataChangeRequestScheme.fields.dcrContactFirstName],
          isSortable: true,
          isAsc: true,
          isActive: false,
          modelFunction: 'contactName'
        },
        {
          title: 'common.names.Specialty',
          fields: [DataChangeRequestScheme.fields.dcrContactSpecialty],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Organization',
          fields: [DataChangeRequestScheme.fields.dcrOrganizationName],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.BillingAddress',
          fields: [DataChangeRequestScheme.fields.dcrOrganizationBillingStreet]
        }
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
  }
  
  private preparePicklists(): Promise<[{ [key: string]: string; }, { [key: string]: string; }]> {
    return Promise.all([
      this.getStatusPicklist(),
      this.getTypePicklist()
    ])
  }
  
  private getStatusPicklist(): Promise<{[key: string]: string}> {
    return this.getAndPreparePicklistItemsForTable(this.dcrStatusPicklistDatasource);
  }
  
  private getTypePicklist(): Promise<{[key: string]: string}> {
    return this.getAndPreparePicklistItemsForTable(this.dcrTypePicklistDatasource);
  }
  
  private getAndPreparePicklistItemsForTable(datasource): Promise<{[key: string]: string}> {
    return datasource.getItems()
      .then((items) => {
        return this.filterEmptyPicklistTypes(items)
          .reduce((all, item) => {
          
            all[item.id] = item.description;
          
            return all;
          }, {});
      })
  }
  
  private showTypeCreatePopup(): Promise<any> {
    return this.localizationManager.promiseLocale('DCR.CreatePopupTitle')
      .then((titleLabel) => {
        const isHeaderEnabled = true;
      
        const picklistPopup = new PicklistPopup(
          this.alertCtrl,
          this.dcrTypePicklistDatasource,
          titleLabel,
          isHeaderEnabled
        );
  
        return picklistPopup.showPopup(null, this.filterEmptyPicklistTypes.bind(this))
          .then((item) => {
            return this.gotoCreatePage(item);
          })
      });
  }
  
  private filterEmptyPicklistTypes(items): Array<any>{
    return items.filter((item) => {
      return !!item.id;
    })
  }
  
  private gotoCreatePage(item): Promise<any> {
    if(this.settings.isChinaUser()) {
      return this.gotoChinaCreatePage(item.id);
    }
    
    return this.gotoGeneralCreatePage(item.id);
  }
  
  private gotoGeneralCreatePage(type: string) {
    switch(type) {
      case DataChangeRequestScheme.TYPES.CONTACT: return this.navCtrl.push(ContactEditCardPage, { row: new Contact({}) });
      case DataChangeRequestScheme.TYPES.ORGANIZATION: return this.navCtrl.push(OrganizationEditCardPage, { row: new Organization({}) });
      case DataChangeRequestScheme.TYPES.REFERENCE: return this.navCtrl.push(ReferenceEditCardPage, { row: new Reference({}) });
      case DataChangeRequestScheme.TYPES.SEGMENTATION: return this.navCtrl.push(DataChangeRequestsSegmentationListPage, {});
    }
  }
  
  private gotoChinaCreatePage(type) {
    switch(type) {
      case DataChangeRequestScheme.TYPES.CONTACT: return this.navCtrl.push(ContactChinaEditCardPage, { row: new Contact({}) });
      case DataChangeRequestScheme.TYPES.ORGANIZATION: return this.navCtrl.push(OrganizationChinaEditCardPage, { row: new Organization({}) });
      case DataChangeRequestScheme.TYPES.REFERENCE: return this.navCtrl.push(ReferenceEditCardPage, { row: new Reference({}) });
      case DataChangeRequestScheme.TYPES.SEGMENTATION: return this.navCtrl.push(DataChangeRequestsSegmentationListPage, {});
    }
  }

}
