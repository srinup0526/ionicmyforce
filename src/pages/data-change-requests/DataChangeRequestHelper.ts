import {ContactsCollection} from "../../collections/ContactsCollection";
import {OrganizationsCollection} from "../../collections/OrganizationsCollection";
import {DataChangeRequest} from "../../models/DataChangeRequest";
import {DataChangeRequestsCollection} from "../../collections/DataChangeRequestsCollection";
import {DataChangeRequestScheme} from "../../models/scheme/DataChangeRequestScheme";
import {Utils} from "../../utils/Utils";
import {Injectable} from "@angular/core";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {AlertController, Alert, AlertOptions} from "ionic-angular";
import {Loader} from "../../services/common/Loader";
import {ReferenceCollection} from "../../collections/references/ReferenceCollection";
import {ProductSegmentationsCollection} from "../../collections/ProductSegmentationsCollection";


@Injectable({
  providedIn: 'root'
})
export class DataChangeRequestHelper {
  
  constructor(private contactsCollection: ContactsCollection,
              private organizationsCollection: OrganizationsCollection,
              private referencesCollection: ReferenceCollection,
              private dataChangeRequestsCollection: DataChangeRequestsCollection,
              private alertCtrl: AlertController,
              private productSegmentationsCollection: ProductSegmentationsCollection,
              private loader: Loader,
              private localizationManager: LocalizationManager) {
  }
  
  public createOrganization(organization) {
    return this.loader.run(() => this.doCreateOrganization(organization))
      .then(this.showSaveDCRPopup.bind(this));
  }

  public createContact(contact, organization) {
    return this.loader.run(() => this.doCreateContact(contact, organization))
      .then(this.showSaveDCRPopup.bind(this));
  }

  public createReference(reference) {
    return this.loader.run(() => this.doCreateReference(reference))
      .then(this.showSaveDCRPopup.bind(this));
  }
  
  public editProductSegmentation(productSegmentation, reference) {
    return this.loader.run(() => this.doCreateProductSegmentation(productSegmentation, reference))
      .then(this.showSaveDCRPopup.bind(this));
  }
  
  public createProductSegmentation(productSegmentation, reference) {
    return this.loader.run(() => this.doEditProductSegmentation(productSegmentation, reference))
      .then(this.showSaveDCRPopup.bind(this));
  }

  private doCreateOrganization(organization) {
    const parsedOrganization = this.organizationsCollection.parseEntity(organization);

    let dcr = new DataChangeRequest({});

    dcr = this.setOrganizationFields(dcr, parsedOrganization);

    return this.dataChangeRequestsCollection.createEntity(dcr);
  }

  private doCreateReference(reference) {
    const parsedReferences = this.referencesCollection.parseEntity(reference);

    let dcr = new DataChangeRequest({});

    dcr = this.setReferenceFields(dcr, parsedReferences);

    return this.dataChangeRequestsCollection.createEntity(dcr);
  }
  
  private doCreateContact(contact, organization) {
    const parsedContact = this.contactsCollection.parseEntity(contact);
    const parsedOrganization = this.organizationsCollection.parseEntity(organization);
    
    let dcr = new DataChangeRequest({});
    
    dcr = this.setContactFields(dcr, parsedContact, parsedOrganization);
    
    return this.dataChangeRequestsCollection.createEntity(dcr);
  }
  
  private doCreateProductSegmentation(productSegmentation, reference) {
    const parsedProductSegmentation = this.productSegmentationsCollection.parseEntity(productSegmentation);
    const parsedReference = this.referencesCollection.parseEntity(reference);
  
    let dcr = new DataChangeRequest({});
  
    dcr = this.setProductSegmentationFields(dcr, parsedProductSegmentation);
  
    dcr.dcrContactFirstName = parsedReference.contactFirstName;
    dcr.dcrContactLastName = parsedReference.contactLastName;
    dcr.dcrContactSpecialty = parsedReference.specialty;
    dcr.dcrContactId = parsedReference.contactAccountId;
    dcr.dcrProductSegmentationAccount = parsedReference.contactAccountId;
    
    return this.dataChangeRequestsCollection.createEntity(dcr);
  }
  
  private doEditProductSegmentation(productSegmentation, reference) {
    const parsedProductSegmentation = this.productSegmentationsCollection.parseEntity(productSegmentation);
    const parsedReference = this.referencesCollection.parseEntity(reference);
  
    let dcr = new DataChangeRequest({});
  
    dcr = this.setProductSegmentationFields(dcr, parsedProductSegmentation);
  
    dcr.dcrContactFirstName = parsedReference.contactFirstName;
    dcr.dcrContactLastName = parsedReference.contactLastName;
    dcr.dcrContactSpecialty = parsedReference.specialty;
    dcr.dcrContactId = parsedReference.contactAccountId;
    dcr.dcrProductSegmentationId = parsedProductSegmentation.id;
    dcr.dcrProductSegmentationAccount = parsedReference.contactAccountId;
  
    return this.dataChangeRequestsCollection.createEntity(dcr);
  }
  
  private setProductSegmentationFields(dcr, parsedProductSegmentation) {
    dcr.dcrProductSegmentationSegmentation1 = parsedProductSegmentation.segmentation1;
    dcr.dcrProductSegmentationSegmentation2 = parsedProductSegmentation.segmentation2;
    dcr.dcrProductSegmentationPhProduct = parsedProductSegmentation.phProductId;
  
    dcr.dcrStatus = DataChangeRequestScheme.STATUSES.DRAFT;
    dcr.dcrType = DataChangeRequestScheme.TYPES.SEGMENTATION;
    dcr.dcrDateOfRequest = Utils.currentDateToSalesForceDateTimeFormat();
  
    return dcr;
  }

  private setContactFields(dcr, parsedContact, parsedOrganization) {
    dcr.dcrContactCNAcademicTitle = parsedContact.academicTitle;
    dcr.dcrContactCNAdministrative = parsedContact.administrative;
    dcr.dcrContactCNDescriptionIntroduction = parsedContact.hcpDescriptionIntroduction;
    dcr.dcrContactCNHcpStatus = parsedContact.hcpStatus;
    dcr.dcrContactCNHcpType = parsedContact.hcpType;
    dcr.dcrContactCNKOL = parsedContact.cnKol;
    dcr.dcrContactCNKolProduct1 = parsedContact.kolProduct1;
    dcr.dcrContactCNKolProduct2 = parsedContact.kolProduct2;
    dcr.dcrContactCNNamedDepartment = parsedContact.namedDepartment;
    dcr.dcrContactCNPhysicianId = parsedContact.physicianId;
    dcr.dcrContactCNProfessionalTitle = parsedContact.professionalTitle;
    dcr.dcrContactCNSocialOrgAndTitle = parsedContact.socialOrganizationAndTitle;
    dcr.dcrContactCNSpeaker = parsedContact.speaker;
    dcr.dcrContactCNSpeakerProduct1 = parsedContact.speakerProduct1;
    dcr.dcrContactCNSpeakerProduct2 = parsedContact.speakerProduct2;
    dcr.dcrContactCNSpecialtyDescription = parsedContact.specialtyDescription;
    dcr.dcrContactCNStandardDepartment = parsedContact.standardDepartment;
    dcr.dcrContactCNStatusDescription = parsedContact.statusDescription;
    dcr.dcrContactCNWorkingTime = parsedContact.workingTime;
  
    dcr.dcrContactDescription = parsedContact.description;
    dcr.dcrContactEmail = parsedContact.email;
    dcr.dcrContactFirstName = parsedContact.firstName;
    dcr.dcrContactGender = parsedContact.gender;
    dcr.dcrContactHomePhone = parsedContact.homePhone;
    dcr.dcrContactCJobTitle = parsedContact.jobTitle;
    // #dcr.dcrContactInclinationToMylan;
    dcr.dcrContactKOL = parsedContact.kol;
    dcr.dcrContactLastName = parsedContact.lastName;
    dcr.dcrContactLegalMedicalLicence = parsedContact.legalMedicalLicence;
    dcr.dcrContactMobilePhone = parsedContact.mobilePhone;
    dcr.dcrContactName = parsedContact.name;
    // #dcr.dcrContactNumberOfPatientsPerDay;
    dcr.dcrContactPersonType = parsedContact.subtype;
    dcr.dcrContactSpecialty = parsedContact.specialty;
    dcr.dcrContactYearOfGraduation = parsedContact.yearOfGraduation;
  
    if(parsedOrganization) {
      dcr.dcrOrganizationId = parsedOrganization.id;
      dcr.dcrOrganizationName = parsedOrganization.name;
      dcr.dcrOrganizationBillingCity = parsedOrganization.city;
      dcr.dcrOrganizationBillingCountry = parsedOrganization.country;
      dcr.dcrOrganizationBillingStreet = parsedOrganization.address;
    }
  
  
    dcr.dcrStatus = DataChangeRequestScheme.STATUSES.DRAFT;
    dcr.dcrType = DataChangeRequestScheme.TYPES.CONTACT;
    dcr.dcrDateOfRequest = Utils.currentDateToSalesForceDateTimeFormat();
  
    return dcr;
  }

  private setOrganizationFields(dcr, parsedOrganization) {
    dcr.dcrOrganizationCNAlias = parsedOrganization.alias;
    dcr.dcrOrganizationCNHospitalGrade = parsedOrganization.hospitalGrade;
    dcr.dcrOrganizationCNHospitalLevel = parsedOrganization.hospitalLevel;
    dcr.dcrOrganizationCNId = parsedOrganization.organizationId;
    dcr.dcrOrganizationCNProperty = parsedOrganization.organizationProperty;
    dcr.dcrOrganizationCNStatusDescription = parsedOrganization.statusDescription;
    dcr.dcrOrganizationCNTargetHospital = parsedOrganization.targetHospital;
    dcr.dcrOrganizationCNType = parsedOrganization.organizationType;

    dcr.dcrOrganizationBillingCity = parsedOrganization.city;
    dcr.dcrOrganizationBillingCountry = parsedOrganization.country;
    dcr.dcrOrganizationBillingPostalCode = parsedOrganization.postalCode;
    dcr.dcrOrganizationBillingState = parsedOrganization.state;
    dcr.dcrOrganizationBillingStreet = parsedOrganization.address;
    dcr.dcrOrganizationBrick = parsedOrganization.brickSfId;
    dcr.dcrOrganizationCJuridicGroup = parsedOrganization.juridicGroup;
  // #dcr.dcrOrganizationCSpecialty1 = parsedOrganization.specialty1;
  // #dcr.dcrOrganizationCSpecialty2 = parsedOrganization.specialty2;
  // #dcr.dcrOrganizationExternal_Id = parsedOrganization.externalId;
    dcr.dcrOrganizationName = parsedOrganization.name;
    dcr.dcrOrganizationPhone = parsedOrganization.phone;
    dcr.dcrOrganizationPriority = parsedOrganization.globalPriority;
    dcr.dcrOrganizationRecordType = parsedOrganization.recordTypeId;
    dcr.dcrOrganizationStatus = parsedOrganization.status;
    dcr.dcrOrganizationSubtype = parsedOrganization.subtype;
  // #dcr.dcrOrganizationNumberOfPatientsPerDay;

    dcr.dcrStatus = DataChangeRequestScheme.STATUSES.DRAFT;
    dcr.dcrType = DataChangeRequestScheme.TYPES.ORGANIZATION;
    dcr.dcrDateOfRequest = Utils.currentDateToSalesForceDateTimeFormat();

    return dcr;
  }

  private setReferenceFields(dcr, parsedReference) {
    dcr.dcrContactId = parsedReference.contactAccountId;
    dcr.dcrOrganizationId = parsedReference.organizationSfId;
    dcr.dcrReferenceCStatus = parsedReference.status;
    dcr.dcrReferencePrimary = parsedReference.isPrimary;

    dcr.dcrContactFirstName = parsedReference.contactFirstName;
    dcr.dcrContactLastName = parsedReference.contactLastName;
    dcr.dcrContactSpecialty = parsedReference.specialty;

    dcr.dcrOrganizationName = parsedReference.organizationName;
    dcr.dcrOrganizationBillingCity = parsedReference.organizationCity;
    dcr.dcrOrganizationBillingCountry = parsedReference.organizationCountry;
    dcr.dcrOrganizationBillingStreet = parsedReference.organizationAddress;

    dcr.dcrStatus = DataChangeRequestScheme.STATUSES.DRAFT;
    dcr.dcrType = DataChangeRequestScheme.TYPES.REFERENCE;
    dcr.dcrDateOfRequest = Utils.currentDateToSalesForceDateTimeFormat();

    return dcr;
  }

  
  private showSaveDCRPopup() {
    return this.showAlert();
  }
  
  private async showAlert(): Promise<any> {
    const title: string = await this.localizationManager.promiseLocale('DCR.ListHeaderTitle');
    const message: string = await this.localizationManager.promiseLocale('DCR.DCRCreatedSuccessfully');
    const okLabel: string = await this.localizationManager.promiseLocale('common.buttons.OkBtn');
    
    return new Promise(async (resolve) => {
      
      const options: AlertOptions = {
        title: title,
        message: message,
        cssClass: 'popup alert single-button pin-alert',
        buttons: [
          {
            text: okLabel,
            role: 'cancel',
            handler: () => {
              resolve();
            }
          }
        ]
      };
      
      const alert: Alert = await this.alertCtrl.create(options);
      
      return alert.present();
    });
  }
  
}
