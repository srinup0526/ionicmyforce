import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataChangeRequestsPage } from './data-change-requests';

@NgModule({
  declarations: [
    DataChangeRequestsPage,
  ],
  imports: [
    IonicPageModule.forChild(DataChangeRequestsPage),
  ],
})
export class DataChangeRequestsPageModule {}
