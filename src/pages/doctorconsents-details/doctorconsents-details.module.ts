import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorconsentsDetailsPage } from './doctorconsents-details';

@NgModule({
  declarations: [
    DoctorconsentsDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorconsentsDetailsPage),
  ],
})
export class DoctorconsentsDetailsPageModule {}
