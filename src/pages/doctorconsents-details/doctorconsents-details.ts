import { Component, ViewChild, ElementRef} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DoctorsConsentScheme } from "../../models/scheme/DoctorsConsentScheme";
import { DoctorsConsent } from '../../models/DoctorsConsent';
import { DoctorConsentCollection } from "../../collections/DoctorConsentCollection";
import { DoctorconsentsPage } from '../doctorconsents/doctorconsents';

import { Query } from "../../services/common/Query";
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';

/**
 * Generated class for the DoctorconsentsDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-doctorconsents-details',
  templateUrl: 'doctorconsents-details.html',
})
export class DoctorconsentsDetailsPage {

  public doctorsConsentsCount:any = 0;
  public tableOptions: LazyTableOption;
  public searchString : string = "";


  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild('mainContent') mainContent: ElementRef;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public doctorConsentCollection : DoctorConsentCollection,) {
      const headerItems = [
        {
          title: 'common.names.CustomerName',
          fields: [DoctorsConsentScheme.fields.firstname, DoctorsConsentScheme.fields.lastname ],
          isSortable: true,
          isAsc: true,
          isActive: false,
          modelFunction: 'contactFullNameWithType'
        },
        {
          title: 'DoctorConsent.OrganizationName',
          fields: [DoctorsConsentScheme.fields.organization],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'DoctorConsent.NameofOrganization',
          fields: [DoctorsConsentScheme.fields.organization],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Specialty',
          fields: [DoctorsConsentScheme.fields.speciality],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'DoctorConsent.CellPhoneNumber',
          fields: [DoctorsConsentScheme.fields.cellphonenumber],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'DoctorConsent.EmailAddress',
          fields: [DoctorsConsentScheme.fields.email],
          isSortable: true,
          isAsc: true,
          isActive: false
        }
        ];
        this.tableOptions = {
          headerItems: headerItems,
          batchSize: 100
        };
        this.doctorConsentCollection = this.doctorConsentCollection;
  }
  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setCollection(this.doctorConsentCollection);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DoctorconsentsDetailsPage');
  }

  ionViewWillEnter():void {
    this.lazyTable.reloadTable();
  }
  private setDoctosConsentCount(): number {
    const countQuery = this.doctorConsentCollection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);

    return this.doctorConsentCollection.runCountQuery(query.query)
    .then((count) => {
      return this.doctorsConsentsCount = count || 0;
    })
  }

  public onChangeQueryHandler(query: Query): void {
    this.setDoctosConsentCount();
  }
  openModal() {
    this.navCtrl.push(DoctorconsentsPage,{"parentPage": this});
  }
}
