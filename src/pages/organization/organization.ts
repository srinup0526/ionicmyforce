import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertOptions } from 'ionic-angular';
import { OrganizationdetailsPage } from '../organizationdetails/organizationdetails';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { OrganizationsCollection } from "../../collections/OrganizationsCollection";
import { OrganizationScheme } from './../../models/scheme/OrganizationScheme';
import { OrganizationRecordTypePicklistDatasource } from './../../services/db/picklist-managers/datasource/OrganizationRecordTypePicklistDatasource';
import { OrganizationSubtypePicklistDatasource } from './../../services/db/picklist-managers/datasource/OrganizationSubtypePicklistDatasource';
import { OrganizationPriorityPicklistDatasource } from './../../services/db/picklist-managers/datasource/OrganizationPriorityPicklistDatasource';
import { Query } from "../../services/common/Query";
import { FilterPanelComponent } from "../../components/filter/filter-panel/filter-panel";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { PatchOrganizationCollection } from '../../collections/PatchOrganizationCollection';
import { PatchCustomerScheme } from '../../models/scheme/PatchCustomerScheme';
import { PatchdetailsPage } from '../patchdetails/patchdetails'






@IonicPage()
@Component({
  selector: 'page-organization',
  templateUrl: 'organization.html',
})
export class OrganizationPage {
  public orgCount:number = 0;
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: any;
  public settings: SettingsImpl;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  
  constructor(private navCtrl: NavController,
              private navParams: NavParams,
              private organizationsCollection: OrganizationsCollection,
              public organizationRecordTypePicklistDatasource: OrganizationRecordTypePicklistDatasource,
              public organizationSubtypePicklistDatasource: OrganizationSubtypePicklistDatasource,
              public organizationPriorityPicklistDatasource: OrganizationPriorityPicklistDatasource,
              private patchOrganizationCollection:PatchOrganizationCollection) {

    this.settings = Settings.getInstance();
    console.log("this.settings",this.settings);
    const nonIndiaTableHeaderItems = [
    {
      title: 'common.names.AccountName',
      fields: [OrganizationScheme.fields.name],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.AccountRecordType',
      fields: [OrganizationScheme.fields.recordType],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.Subtype',
      fields: [OrganizationScheme.fields.subtype],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.GlobalPriority',
      fields: [OrganizationScheme.fields.globalPriority],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.StateProvince',
      fields: [OrganizationScheme.fields.state],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.City',
      fields: [OrganizationScheme.fields.city],
      isAsc: true,
      isSortable: true,
      isActive: false
    },
    {
      title: 'common.names.BillingAddress',
      fields: [OrganizationScheme.fields.address],
      cssClass: ['appt'],
      isAsc: true,
      isSortable: true,
      isActive: false
    },
    {
      title: 'common.names.Phone',
      fields: [OrganizationScheme.fields.phone],
      cssClass: ['call-report']
    }
    ];
    const indiaTableHeaderItems = [
    {
      title: 'common.names.Customer',
      fields: [PatchCustomerScheme.fields.accountName],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.Frequency',
      fields: [PatchCustomerScheme.fields.frequency],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {}
    },
    {
      title: 'common.names.Class',
      fields: [PatchCustomerScheme.fields.class],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.Patch',
      fields: [PatchCustomerScheme.fields.patchName],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.RecordType',
      fields: [PatchCustomerScheme.fields.recordType],
      isSortable: true,
      isAsc: true,
      isActive: false
    },

    {
      title: 'common.names.Appt',
      fields: [],
      cssClass: ['appt'],
      handler: (event, patchCustomer) => {
        //this.gotoCreateAppointment(reference);
        event.stopPropagation();
      }
    },
    ];
    console.log("this.settings.isIndiaUser()",this.settings.isIndiaUser());
    if(this.settings.isIndiaUser())
    {
      this.tableOptions = {
        headerItems:indiaTableHeaderItems,
        rowHandler: this.onTapRowHandlerForIndia.bind(this),
        batchSize: 100
      }
      this.collection = this.patchOrganizationCollection;
    }
    else{
      this.tableOptions = {
        headerItems:nonIndiaTableHeaderItems,
        rowHandler: this.onTapRowHandler.bind(this),
        batchSize: 100
      }
      this.collection = this.organizationsCollection;
    }

    this.searchString = '';
  }
  
  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
    
    this.lazyTable.reloadTable();
  }
  
  
  public onSwipeHandler(event){
    switch (event.offsetDirection) {
      case 2: this.filterPanel.hidePanel(); break;
      case 4: this.filterPanel.showPanel(); break;
    }
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
  
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onFilterChangeHandler(filterData): void {
    this.lazyTable.reloadTableByFilterData(filterData);
  }
  
  public onChangeQueryHandler(query: Query): void {
    this.setOrganizationCount();
  }
  
  public onTapRowHandler(event, record) {
    this.gotoOrganizationDetail(record.id);
    event.stopPropagation();
  }

  public onTapRowHandlerForIndia(event, record) {
    this.gotoPatchDetails(record);
    event.stopPropagation();
  }

  private gotoPatchDetails(record)
  {
    this.navCtrl.push(PatchdetailsPage, { "row": record });
  }
  
  public ionViewDidEnter() {}
  
  public ionViewWillEnter(): void {
    this.lazyTable.reloadTable();
  }
  
  public ionViewDidLoad() {}
  
  private gotoOrganizationDetail(id: string) {
    this.navCtrl.push(OrganizationdetailsPage, { "organizationSfId": id });
  }
  
  private setOrganizationCount(): number {
    const countQuery = this.collection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);
    
    return this.collection.runCountQuery(query.query)
      .then((count) => {
        return this.orgCount = count || 0;
      })
  }
}
