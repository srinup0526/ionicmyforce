import { Component, ViewChild,Renderer,TemplateRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController,PopoverController, Loading, ModalController, ModalOptions, ToastController,
  AlertController, Navbar, Platform, AlertOptions, Select } from 'ionic-angular';
declare var cordova: any;
import { OAuth, DataService } from 'forcejs';
import { Storage } from '@ionic/storage';
import * as Constants from '../../services/constants';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { OrganizationdetailsPage } from '../organizationdetails/organizationdetails';
import { ContactdetailsPage } from '../contactdetails/contactdetails';
import { AddRedFlagPage } from '../add-red-flag/add-red-flag';
import { CallReport } from '../../models/CallReport';
import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import moment from 'moment';
import { LocalizationManager } from "../../services/common/localizations/LocalizationManager";
import { CallReportStatusPickListDatasource } from '../../services/db/picklist-managers/datasource/CallReportStatusPickListDatasource';
import { CallReportTypePickListDatasource } from '../../services/db/picklist-managers/datasource/CallReportTypePickListDatasource';
import { CallReportCoachingVisitPickListDatasource } from '../../services/db/picklist-managers/datasource/CallReportCoachingVisitPickListDatasource';
import { CallReportTypeOfVisitPickListDatasource } from '../../services/db/picklist-managers/datasource/CallReportTypeOfVisitPickListDatasource';
import { SingleUserListPage } from "../single-user-list/single-user-list";
import {UserListMultiSelectPage} from "../user-list-multi-select/user-list-multi-select";
import {Loader} from "../../services/common/Loader";
import { UsersCollection } from '../../collections/UsersCollection';
import { ProductsCollection } from '../../collections/ProductsCollection';
import { MarketingMessagesCollection } from '../../collections/MarketingMessagesCollection';
import { CallReportsCollection } from '../../collections/CallReportsCollection/CallReportsCollection';
import { Utils } from "../../utils/Utils";
import {ConfirmPopup} from "../../components/popups/ConfirmPopup";

/**
 * Generated class for the CallReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-call-report',
  templateUrl: 'call-report.html',
})
export class CallReportPage {

  @ViewChild('modalContent') modalContent: TemplateRef<any>;
  @ViewChild('filterType') filterTypeComponent: Select;
  callReport:CallReport;
  public filterOptions: AlertOptions;
  Media : any;
  message:string = "";


  tmIsAllFieldsFilled: any = null;
  tmSaveIqviaTaskAdjustments: any = null;


  redFlagRequired:boolean = false;

  marketingMessages:any;
  marketingMessages1:any;
  marketingMessages2:any;
  marketingMessages3:any;
  marketingMessages4:any;
  marketingMessages5:any;
  errData:string = '';
  popUpContent:string = '';
  modalTitle:string = 'Error';
  shownGroup = null;
  numberOfPromotedProducts : any;
  marketingMessage : any;
  product:any;
  
  PharmaMessage:any;
  callreportBackup:any;
  contact: any;
  Reference:any;
  Organisation:any;
  pharmaProducts:any;
  excludedProducts:any = [];
  pharmaProductsId:any;
  row:any;
  coachingVisit:any;
  dateTimePlanned:any;
  Customer_Name__c:any;
  Org_Name__c:any;
  ActivitesName:'Activites';
  activeUser:any;
  JointVisitParticipants:any = [];
  CoachingVisitUser:any;
  saveAppointmentData: {};
  contactInformation:any;
 
  duration:any;
  @ViewChild("card1") CardContent:any;
  @ViewChild("card2") CardContent2:any;
  @ViewChild("card3") CardContent3:any;
  @ViewChild("card4") CardContent4:any;
  @ViewChild("card5") CardContent5:any;
  @ViewChild("card6") CardContent6:any;
  typeofvisit: any;
  PharmaMessageBackup: any;
  loggedInUserDetails:any;
  public serviceOnline:any;
  callObjectives:any="";
  callComments:any;
  nextCallObjective:any;
  users:any;
  coachingPicklistOptions:any = [];
  typeOfVisitPicklistOptions:any = [];
  durationPickListOptions:any = [];
  coachingVisitPickListDataSource:any;
  config:any;
  productlist: any;
  isProductSelect: boolean = false;
  marketingMessageList:any = [];
  keyMessageSource: any = [];
  @ViewChild('navbar') navBar: Navbar;

  public selectedUserSingleStringList : string = "";
  private selectedUserSingleIds: any;

  public selectedUserMultiStringList : string = "";
  private selectedUserMultiIds: Array<string> = [];

  constructor(private modal: NgbModal,
    public storage:Storage,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public renderer:Renderer,
    private popoverController: PopoverController,
    public navParams: NavParams,
    public localizationManager: LocalizationManager,
    private callReportStatusPickListDatasource: CallReportStatusPickListDatasource,
    private callReportTypePickListDatasource: CallReportTypePickListDatasource,
    private callReportCoachingVisitPickListDatasource: CallReportCoachingVisitPickListDatasource,
    private callReportTypeOfVisitPickListDatasource: CallReportTypeOfVisitPickListDatasource,
    public usersCollection:UsersCollection,
    private loader : Loader,
    private productsCollection : ProductsCollection,
    private marketingMessagesCollection : MarketingMessagesCollection,
    private callReportsCollection : CallReportsCollection,
    private alertCtrl: AlertController)
  {
    this.callReport = new CallReport({});
    this.callReport.dateTimePlanned = new Date().toISOString();
    this.callReport.dateTimeVisitStart = new Date().toISOString();
    this.marketingMessagesCollection.fetchAll()
    .then(records=>{ return this.marketingMessagesCollection.getAllEntitiesFromResponse(records);})
    .then(res=>{
      this.marketingMessageList = res;
      console.log("this.marketingMessageList",this.marketingMessageList);
      this.loadDependentData();
      this.loadConfigData();
      //SettingsManager.getTourPlanningSettings().then(settings=>{ console.log("settings",settings)});
    });
    this.productsCollection.fetchAll()
    .then(records=>{ return this.productsCollection.getAllEntitiesFromResponse(records);})
    .then(res=>{
      console.log("products res",res);
      this.productlist = res;
    })

    
    this.Reference = this.navParams.data['row'];
    console.log("row",this.navParams.data['row']);
    

    this.callReportStatusPickListDatasource.getItems()
      .then((picklistItems) => {
        console.log('------ PicklistItems.length ------');
        console.log(picklistItems.length);
        console.log("callReportStatusPickListDatasource",picklistItems);
      });
    this.callReportTypePickListDatasource.getItems()
      .then((picklistItems) => {
        console.log('------ PicklistItems.length ------');
        console.log(picklistItems.length);
        console.log("callReportTypePickListDatasource",picklistItems);
      });
    this.callReportCoachingVisitPickListDatasource.getItems()
      .then((picklistItems) => {
        console.log('------ PicklistItems.length ------');
        console.log(picklistItems.length);
        console.log("callReportCoachingVisitPickListDatasource",picklistItems);
        this.coachingVisitPickListDataSource = picklistItems;
      });
    this.callReportTypeOfVisitPickListDatasource.getItems()
      .then((picklistItems) => {
        console.log('------ PicklistItems.length ------');
        console.log(picklistItems.length);
        console.log("callReportTypeOfVisitPickListDatasource",picklistItems);
        this.typeOfVisitPicklistOptions = picklistItems;
      });
    this.filterOptions = {
       cssClass: 'popup alert list without-header',
       buttons: []
     };
  }

  loadDependentData()
  {
    return SforceDataContext.getActiveUser()
    .then(activeUser=>{
      console.log("activeUser",activeUser);
      this.activeUser = activeUser;
    })
  }
  loadConfigData()
  {
    return ConfigurationManager.getConfig()
    .then(config=>{
      console.log("config",config);
      this.config = config;
      this.numberOfPromotedProducts = [];
      if(this.config.callReportProductsSettings)
      {
        for(var i=1; i <= this.config.callReportProductsSettings.numberOfPromotedProducts;i++) {
          this.numberOfPromotedProducts.push({productKey:"prio"+i+"ProductSfid", producttextInput: "", promotionalItem:"promotionalItemsPrio"+i,  marketingMessage1Key:"prio"+i+"MarketingMessage1",marketingMessage2Key:"prio"+i+"MarketingMessage2" ,marketingMessage3Key:"prio"+i+"MarketingMessage3" ,marketingMessage4Key:"prio"+i+"MarketingMessage4" ,marketingMessage5Key:"prio"+i+"MarketingMessage5", marketingMessageSource:this.marketingMessageList});
        }
      }
      console.log("numberOfPromotedProducts",this.numberOfPromotedProducts);
      this.redFlagRequired = config.sampleManagementSettings.isItRedFlagRequired;
      this.durationPickListOptions = config.tourPlanningSettings?config.tourPlanningSettings.duration.split(","):"";
    })
  }

  ionViewDidLoad() {
    console.log("information loaded")
  }

  onTapTypeOfVisitBtnHandler() {

  }

  public onTapUserHandler() {		
    this.gotoUserPage();		
  }

  public onTapMultiUserHandler() {		
    this.gotoMultiUserPage();		
  }

  private gotoUserPage() {
    this.navCtrl.push(SingleUserListPage, {
      "coaching-user-id": this.selectedUserSingleIds,
      callback: this.setUser.bind(this)
    })
  }

  private gotoMultiUserPage() {
    this.navCtrl.push(UserListMultiSelectPage, {
      ids: this.selectedUserMultiIds,
      callback: this.setMultiUser.bind(this)
    })
  }

  private setMultiUser(userIds) {
    this.selectedUserMultiIds = userIds;
    this.selectedUserMultiStringList = this.selectedUserMultiIds.join('; ');
    
    this.setUserMultiListToView();
  }

  private setUserMultiListToView() {
    if(this.selectedUserMultiIds.length) {
      return this.loader.run(() => {
        return this.usersCollection.fetchForUserIds(this.selectedUserMultiIds)
          .then((users) => {
            this.selectedUserMultiStringList = users
              .map((user) => user.fullName())
              .join('; ');
          })
      })
    }
  
    this.selectedUserMultiStringList = '';
  }

  private setUser(userIds) {
    console.log("userIds",userIds);
    this.selectedUserSingleIds = userIds;
    this.selectedUserSingleStringList = userIds.fullName();
    //this.selectedUserSingleStringList = this.selectedUserSingleIds.join(', ');
    
    //this.setUserListToView();
  }

  private setUserListToView() {
    if(this.selectedUserSingleIds.length) {
      return this.loader.run(() => {
        return this.usersCollection.fetchForUserIds(this.selectedUserSingleIds)
          .then((users) => {
            this.selectedUserSingleStringList = users
              .map((user) => user.fullName())
              .join('; ');
          })
      })
    }
  
    this.selectedUserSingleStringList = '';
  }


  saveCallReport()
  {
    this.callReport["createdOffline"] = true;
    this.callReport["createdFromMobile"] = true;
    this.callReport["isSandbox"] = false;
    this.callReport["realCallDuration"] = Utils.getDuration(new Date(this.callReport.dateTimePlanned).getTime(),(new Date).getTime());
    this.callReport["callWithIPad"] = true;
    this.callReport["dateTimePlanned"] = Utils.originalDateTime(new Date(this.callReport.dateTimePlanned));
    this.callReport["dateTimeVisitStart"] = Utils.originalDateTime(new Date(this.callReport.dateTimeVisitStart));
    this.callReport["dateTimeVisitEnd"] = Utils.originalDateTime(new Date());
    this.callReport["userSfid"] = this.activeUser.id;
    this.callReport["organizationSfId"] = this.Reference.organizationSfId;
    if(this.selectedUserSingleIds)
      this.callReport["coachingVisitUserSfid"] =  this.selectedUserSingleIds.id;
    this.callReport["jointVisitParticipants"] = this.selectedUserMultiStringList;
    this.callReport["generalComments"] = this.callComments;
    this.callReport["type"] = CallReportScheme.TYPE_ONE_TO_ONE;
    this.callReport["recordTypeId"] = this.config.callReportRecordTypeId;
    this.callReport["remoteOrganizationName"] = this.Reference.organizationName;
    this.callReport["organizationName"] = this.Reference.organizationName;
    this.callReport["organizationCity"] = this.Reference.organizationCity;
    this.callReport["organizationAddress"] = this.Reference.organizationAddress;
    this.callReport["contactSfId"] = this.Reference.contactSfId;
    this.callReport["remoteContactFirstName"] = this.Reference.contactFirstName;
    this.callReport["remoteContactLastName"] = this.Reference.contactLastName;
    this.callReport["contactFirstName"] = this.Reference.contactFirstName;
    this.callReport["contactLastName"] = this.Reference.contactLastName;
    this.callReport["contactRecordType"] = this.Reference.contactRecordType;
    this.callReport["userFirstName"] = this.activeUser.firstName;
    this.callReport["userLastName"] = this.activeUser.lastName;
    this.callReport["contactRecordType"] = this.Reference.contactRecordType;
    this.callReport["specialty"] = this.Reference.specialty;
    this.callReport["coachingVisit"] = this.coachingVisit;
    this.callReport["status"] = CallReportScheme.STATUS_SAVED;

    console.log("saveCallReport",this.numberOfPromotedProducts);
    
    for(var i=0;i< this.numberOfPromotedProducts.length; i++) {
      if(i==0) {
        console.log("saveCallReport i=1",this.numberOfPromotedProducts[i].productKey);
        this.numberOfPromotedProducts[i].productKey == "prio1ProductSfid" ? "" : this.callReport.prio1ProductSfid = this.numberOfPromotedProducts[i].productKey;
        this.numberOfPromotedProducts[i].producttextInput == "noteForPrio1" ? "" : this.callReport.noteForPrio1 = this.numberOfPromotedProducts[i].producttextInput;
        this.numberOfPromotedProducts[i].promotionalItem == "promotionalItemsPrio1" ? "" :this.callReport.promotionalItemsPrio1 = this.numberOfPromotedProducts[i].promotionalItem;
        this.numberOfPromotedProducts[i].marketingMessage1Key == "prio1MarketingMessage1" ? "" :this.callReport.prio1MarketingMessage1 = this.numberOfPromotedProducts[i].marketingMessage1Key;
        this.numberOfPromotedProducts[i].marketingMessage2Key == "prio1MarketingMessage2" ? "" : this.callReport.prio1MarketingMessage2 = this.numberOfPromotedProducts[i].marketingMessage2Key;
        this.numberOfPromotedProducts[i].marketingMessage3Key == "prio1MarketingMessage3" ? "" : this.callReport.prio1MarketingMessage3 = this.numberOfPromotedProducts[i].marketingMessage3Key;
        this.numberOfPromotedProducts[i].marketingMessage4Key == "prio1MarketingMessage4" ? "" : this.callReport.prio1MarketingMessage4 = this.numberOfPromotedProducts[i].marketingMessage4Key;
        this.numberOfPromotedProducts[i].marketingMessage5Key == "prio1MarketingMessage5" ? "" : this.callReport.prio1MarketingMessage5 = this.numberOfPromotedProducts[i].marketingMessage5Key;
      }
      if(i==1) {
        console.log("saveCallReport i=2",this.numberOfPromotedProducts[i].productKey);
        this.numberOfPromotedProducts[i].productKey == "prio2ProductSfid" ? "" : this.callReport.prio2ProductSfid = this.numberOfPromotedProducts[i].productKey;
        this.numberOfPromotedProducts[i].producttextInput == "noteForPrio2" ? "" :this.callReport.noteForPrio2 = this.numberOfPromotedProducts[i].producttextInput;
        this.numberOfPromotedProducts[i].promotionalItem == "promotionalItemsPrio2" ? "" :this.callReport.promotionalItemsPrio2 = this.numberOfPromotedProducts[i].promotionalItem;
        this.numberOfPromotedProducts[i].marketingMessage1Key == "prio2MarketingMessage1" ? "" :this.callReport.prio2MarketingMessage1 = this.numberOfPromotedProducts[i].marketingMessage1Key;
        this.numberOfPromotedProducts[i].marketingMessage2Key == "prio2MarketingMessage2" ? "" : this.callReport.prio2MarketingMessage2 = this.numberOfPromotedProducts[i].marketingMessage2Key;
        this.numberOfPromotedProducts[i].marketingMessage3Key == "prio2MarketingMessage3" ? "" : this.callReport.prio2MarketingMessage3 = this.numberOfPromotedProducts[i].marketingMessage3Key;
        this.numberOfPromotedProducts[i].marketingMessage4Key == "prio2MarketingMessage4" ? "" : this.callReport.prio2MarketingMessage4 = this.numberOfPromotedProducts[i].marketingMessage4Key;
        this.numberOfPromotedProducts[i].marketingMessage5Key == "prio2MarketingMessage5" ? "" :this.callReport.prio2MarketingMessage5 = this.numberOfPromotedProducts[i].marketingMessage5Key;
      }
      if(i==2) {
        this.numberOfPromotedProducts[i].productKey == "prio3ProductSfid" ? "" : this.callReport.prio3ProductSfid = this.numberOfPromotedProducts[i].productKey;
        this.numberOfPromotedProducts[i].producttextInput == "noteForPrio3" ? "" : this.callReport.noteForPrio3 = this.numberOfPromotedProducts[i].producttextInput;
        this.numberOfPromotedProducts[i].promotionalItem == "promotionalItemsPrio3" ? "" : this.callReport.promotionalItemsPrio3 = this.numberOfPromotedProducts[i].promotionalItem;
        this.numberOfPromotedProducts[i].marketingMessage1Key == "prio3MarketingMessage1" ? "" : this.callReport.prio3MarketingMessage1 = this.numberOfPromotedProducts[i].marketingMessage1Key;
        this.numberOfPromotedProducts[i].marketingMessage2Key == "prio3MarketingMessage2" ? "" : this.callReport.prio3MarketingMessage2 = this.numberOfPromotedProducts[i].marketingMessage2Key;
        this.numberOfPromotedProducts[i].marketingMessage3Key == "prio3MarketingMessage3" ? "" : this.callReport.prio3MarketingMessage3 = this.numberOfPromotedProducts[i].marketingMessage3Key;
        this.numberOfPromotedProducts[i].marketingMessage4Key == "prio3MarketingMessage4" ? "" : this.callReport.prio3MarketingMessage4 = this.numberOfPromotedProducts[i].marketingMessage4Key;
        this.numberOfPromotedProducts[i].marketingMessage5Key == "prio3MarketingMessage5" ? "" : this.callReport.prio3MarketingMessage5 = this.numberOfPromotedProducts[i].marketingMessage5Key;
      }
      if(i==3) {
        this.numberOfPromotedProducts[i].productKey == "prio4ProductSfid" ? "" : this.callReport.prio4ProductSfid = this.numberOfPromotedProducts[i].productKey;
        this.numberOfPromotedProducts[i].producttextInput == "noteForPrio4" ? "" : this.callReport.noteForPrio4 = this.numberOfPromotedProducts[i].producttextInput;
        this.numberOfPromotedProducts[i].promotionalItem == "promotionalItemsPrio4" ? "" : this.callReport.promotionalItemsPrio4 = this.numberOfPromotedProducts[i].promotionalItem;
        this.numberOfPromotedProducts[i].marketingMessage1Key == "prio4MarketingMessage1" ? "" : this.callReport.prio4MarketingMessage1 = this.numberOfPromotedProducts[i].marketingMessage1Key;
        this.numberOfPromotedProducts[i].marketingMessage2Key == "prio4MarketingMessage2" ? "" :this.callReport.prio4MarketingMessage2 = this.numberOfPromotedProducts[i].marketingMessage2Key;
        this.numberOfPromotedProducts[i].marketingMessage3Key == "prio4MarketingMessage3" ? "" : this.callReport.prio4MarketingMessage3 = this.numberOfPromotedProducts[i].marketingMessage3Key;
        this.numberOfPromotedProducts[i].marketingMessage4Key == "prio4MarketingMessage4" ? "" : this.callReport.prio4MarketingMessage4 = this.numberOfPromotedProducts[i].marketingMessage4Key;
        this.numberOfPromotedProducts[i].marketingMessage5Key == "prio4MarketingMessage5" ? "" : this.callReport.prio4MarketingMessage5 = this.numberOfPromotedProducts[i].marketingMessage5Key;
      }
      if(i==4) {
        this.numberOfPromotedProducts[i].productKey == "prio5ProductSfid" ? "" : this.callReport.prio5ProductSfid = this.numberOfPromotedProducts[i].productKey;
        this.numberOfPromotedProducts[i].producttextInput == "noteForPrio5" ? "" :this.callReport.noteForPrio5 = this.numberOfPromotedProducts[i].producttextInput;
        this.numberOfPromotedProducts[i].promotionalItem == "promotionalItemsPrio5" ? "" : this.callReport.promotionalItemsPrio5 = this.numberOfPromotedProducts[i].promotionalItem;
        this.numberOfPromotedProducts[i].marketingMessage1Key == "prio5MarketingMessage1" ? "" : this.callReport.prio5MarketingMessage1 = this.numberOfPromotedProducts[i].marketingMessage1Key;
        this.numberOfPromotedProducts[i].marketingMessage2Key == "prio5MarketingMessage2" ? "" : this.callReport.prio5MarketingMessage2 = this.numberOfPromotedProducts[i].marketingMessage2Key;
        this.numberOfPromotedProducts[i].marketingMessage3Key == "prio5MarketingMessage3" ? "" : this.callReport.prio5MarketingMessage3 = this.numberOfPromotedProducts[i].marketingMessage3Key;
        this.numberOfPromotedProducts[i].marketingMessage4Key == "prio5MarketingMessage4" ? "" : this.callReport.prio5MarketingMessage4 = this.numberOfPromotedProducts[i].marketingMessage4Key;
        this.numberOfPromotedProducts[i].marketingMessage5Key == "prio5MarketingMessage5" ? "" : this.callReport.prio5MarketingMessage5 = this.numberOfPromotedProducts[i].marketingMessage5Key;
      }
      if(i==5) {
        this.numberOfPromotedProducts[i].productKey == "prio6ProductSfid" ? "" : this.callReport.prio6ProductSfid = this.numberOfPromotedProducts[i].productKey;
        //this.callReport.prio6ProductSfid = this.numberOfPromotedProducts[i].productKey;
        this.numberOfPromotedProducts[i].producttextInput == "noteForPrio6" ? "" : this.callReport.noteForPrio6 = this.numberOfPromotedProducts[i].producttextInput;
        this.numberOfPromotedProducts[i].producttextInput == "noteForPrio6" ? "" : this.callReport.noteForPrio6 = this.numberOfPromotedProducts[i].producttextInput;
        this.numberOfPromotedProducts[i].promotionalItem == "promotionalItemsPrio6" ? "" : this.callReport.promotionalItemsPrio6 = this.numberOfPromotedProducts[i].promotionalItem;
        this.numberOfPromotedProducts[i].marketingMessage1Key == "prio6MarketingMessage1" ? "" : this.callReport.prio6MarketingMessage1 = this.numberOfPromotedProducts[i].marketingMessage1Key;
        this.numberOfPromotedProducts[i].marketingMessage2Key == "prio6MarketingMessage2" ? "" : this.callReport.prio6MarketingMessage2 = this.numberOfPromotedProducts[i].marketingMessage2Key;
        this.numberOfPromotedProducts[i].marketingMessage3Key == "prio6MarketingMessage3" ? "" : this.callReport.prio6MarketingMessage3 = this.numberOfPromotedProducts[i].marketingMessage3Key;
        this.numberOfPromotedProducts[i].marketingMessage4Key == "prio6MarketingMessage4" ? "" : this.callReport.prio6MarketingMessage4 = this.numberOfPromotedProducts[i].marketingMessage4Key;
        this.numberOfPromotedProducts[i].marketingMessage5Key == "prio6MarketingMessage5" ? "" : this.callReport.prio6MarketingMessage5 = this.numberOfPromotedProducts[i].marketingMessage5Key;
      }
    }
    console.log("this.callReport",this.callReport);

    this.callReportsCollection.createEntity(this.callReport)
        .then(createdData=>{
          console.log("createdData",createdData);
          let success = (data) => {
            console.log("Data Upserted successfully and created data is",data);
            //this.navParams.get("parentPage").reloadTOT();
            this.navCtrl.pop();
            const toast = this.toastCtrl.create({
              message: "Call Report Created Successfully!",
              showCloseButton:true,
              cssClass:"toastClass",
              position:'middle',
              duration:3000,
            });
            toast.present();
          }

          if(this.tmSaveIqviaTaskAdjustments){
            return this.tmSaveIqviaTaskAdjustments(createdData)
              .then(() => {
                success(createdData);
              });
          }else{
            success(createdData);
          }
        });
    
  }

  saveCallOnline()
  {
  } 

    openPresentations()
    {
      alert("This functionality work in progress for now")
    }

    onChangeProduct(product) {
      console.log('onchangeProduct',product);
      console.log("this.numberOfPromotedProducts",this.numberOfPromotedProducts);
      this.isProductSelect = true;
      this.numberOfPromotedProducts.forEach((keySource,i)=>{
        if(keySource.productKey == product.productKey)
        {
          //this.numberOfPromotedProducts[i].marketingMessageSource = [];
          this.keyMessageSource = []
          this.marketingMessageList.map(marketingMessage=>{
            //console.log("product.productKey == marketingMessage.produtSfId",product.productKey == marketingMessage.produtSfId);
            if(product.productKey == marketingMessage.produtSfId)
            {
              this.keyMessageSource.push(marketingMessage)
            }
            this.numberOfPromotedProducts[i].marketingMessageSource =  this.keyMessageSource || [];
          });
          console.log("this.numberOfPromotedProducts[i].marketingMessageSource",this.numberOfPromotedProducts[i].marketingMessageSource);
           
        }
      })
    }
    validateCallReport()
    {
      if(this.validateCallInputData())
      {

        if(this.tmIsAllFieldsFilled){
          this.validateTradeModuleData(this.saveCallReport.bind(this));
        }else{
          this.saveCallReport();
        }

      }
      else
      {
        const toast = this.toastCtrl.create({
          message: this.message,
          showCloseButton:true,
          cssClass:"toastClass",
          duration:7000,
        });
        toast.present();
      }
    }

    validateCallInputData()
    {
      let valid:boolean = true;
      this.message = "";
      // if(this.productName1=='' || this.productName1==null)
      // {
      //   valid = false;
      //   this.message+="First Product Mandatory";
      // }
      // if(this.productMessage1=='' || this.productMessage1==null)
      // {
      //   valid = false;
      //   this.message+="First Product Marketing Key Message Mandatory";
      // }
      // // if((this.dateTimePlanned=='' || this.dateTimePlanned==null))
      // // {
      // //   valid = false;
      // //   this.message+="Date Time Planned Mandatory";
      // // }
      // if((this.DateTimeVisitStart =='' || this.DateTimeVisitStart==null))
      // {
      //   valid = false;
      //   this.message+="Date Time Visit Can not be Empty";
      // }
      if(!this.isProductSelect) {
        valid = false;
        this.message+=" Please select atleast one product ";
      }

      if(new Date(this.callReport.dateTimePlanned)>=new Date(this.callReport.dateTimeVisitStart))
      {
        valid = false;
        this.message+=" \n Planned Date Can not be grater than the Visit Start Date";
      }
      if(this.coachingVisit!=null && (this.selectedUserSingleStringList=='' || this.selectedUserSingleStringList==null))
      {
        valid = false;
        this.message+=" \n Coaching Visit User Mandatory to select when coaching visit selected ";
      }

      return valid;
    }

    navigateToOrganisationPage(reference:any){
      console.log("reference",reference);
      let row = {};
      row["Id"] = reference.Organisation__c;
      console.log("row",row);
      this.navCtrl.push(OrganizationdetailsPage, { "row": row });
    }

    navigateToContactPage(reference:any){
      console.log("reference",reference);
      this.navCtrl.push(ContactdetailsPage, { "row": reference });
    }

    public maxDate() {
      const afterDays = 90;

      return moment().add(afterDays, 'days').format("YYYY-MM-DD");
    }

    public minDate() {
      const beforeDays = 10;

      return moment().subtract(beforeDays, 'days').format("YYYY-MM-DD");
    }

    toggleGroup(group) {
      if (this.isGroupShown(group)) {
        this.shownGroup = null;
      } else {
        this.shownGroup = group;
      }
    };
    isGroupShown(group) {
      return this.shownGroup === group;
    };



  private tradeModuleChange(event){
    // TODO: need add isChanged flag when it will be on call report

    if(event){
      if(!this.tmIsAllFieldsFilled && event.isAllFieldsFilled){
        this.tmIsAllFieldsFilled = event.isAllFieldsFilled;
      }

      if(!this.tmSaveIqviaTaskAdjustments && event.saveIqviaTaskAdjustments){
        this.tmSaveIqviaTaskAdjustments = event.saveIqviaTaskAdjustments;
      }
    }

  }


  private validateTradeModuleData(callback){
    if(this.tmIsAllFieldsFilled()){
      callback();
    }else{
      const confirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);
      return confirmPopup.showPopup({
        title: 'tradeModule.Title',
        message: 'tradeModule.AreYouShureSaveAndExit',
        yesLabel: 'common.buttons.YesBtn',
        noLabel: 'common.buttons.NoBtn'
      })
        .then(async (action) => {
          if(action.doAction){
            callback();
          }
        });
    }
  }


    
}
