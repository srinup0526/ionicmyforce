import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CallReportPage } from './call-report';

@NgModule({
  declarations: [
    CallReportPage,
  ],
  imports: [
    IonicPageModule.forChild(CallReportPage),
  ],
})
export class CallReportPageModule {}
