import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddpharmaeventPage } from './addpharmaevent';

@NgModule({
  declarations: [
    AddpharmaeventPage,
  ],
  imports: [
    IonicPageModule.forChild(AddpharmaeventPage),
  ],
})
export class AddpharmaeventPageModule {}
