import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { Storage } from '@ionic/storage';
import * as Constants from '../../services/constants';
import moment from 'moment';
import { PETypeEventPickListDatasource } from '../../services/db/picklist-managers/datasource/PETypeEventPickListDatasource';
import { PEStagePickListDatasource } from '../../services/db/picklist-managers/datasource/PEStagePickListDatasource'
import { ReferenceCollection } from './../../collections/references/ReferenceCollection';
import { ProductsCollection } from './../../collections/ProductsCollection';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { PharmaEvent } from '../../models/PharmaEvent';
import { PEAttendee } from '../../models/PEAttendee';
import { PharmaEventsCollection } from '../../collections/PharmaEventsCollection';
import { PEAttendeesCollection } from '../../collections/PEAttendeesCollection';
import { PharmaEventScheme } from '../../models/scheme/PharmaEventScheme';
import { ConfigurationManager } from '../../services/db/ConfigurationManager'
import {AttendeesListSelectPage} from "../attendees-list-select/attendees-list-select";
import {ProductListSelectPage} from "../product-list-select/product-list-select";
import {ContactsCollection} from "../../collections/ContactsCollection";
import {Loader} from "../../services/common/Loader";

//import { IonicSelectableComponent } from 'ionic-selectable';
/**
 * Generated class for the AddpharmaeventPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-addpharmaevent',
  templateUrl: 'addpharmaevent.html',
})
export class AddpharmaeventPage {
  PharmaEventid:any;
  PharmaEvent:any;
  productsData:any;
  speakersData:any;
  attendessData:any = [];
  selectedPharmaProducts:any = [];
  selectedProductsData:any = [];
  selectedAttendeesData:any = [];
  selectedSpeakersData:any = [];
  loggedInUserDetails:any;
  pharameventvalidation:any;
  message = "";
  stagePicklistOptions:any = [];
  typeOfEventPicklistOptions:any = [];
  viewEnteredDateTime:any;
  attendeesList:any;
  productList:any;
  dropdownList = [];
  selectedItems = [];
  eventName : any = "";
  dropdownSettings = {};
  startDate:any = new Date().toISOString();
  endDate: any = new Date().toISOString();
  evaluation : any = "";
  eventType : any = "";
  speakers : any = "";
  objectives : any = "";
  stage : any = "";
  location : any = "";
  agenda : any = "";
  public activeUser:any;
  public picklistDataSource:any;
  public picklistStageSource:any;
  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }
  
  public selectedAttendeesStringList : string = "";
  private selectedAttendeesIds: Array<string> = [];

  public selectedProductStringList : string = "";
  private selectedProductIds: Array<string> = [];
  
  
  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    private service: SmartstoreServiceProvider,
    public storage: Storage,
    private peTypeEventPickListDatasource : PETypeEventPickListDatasource,
    private peStagePickListDatasource : PEStagePickListDatasource,
    private pharmaEventsCollection : PharmaEventsCollection,
    private peAttendeesCollection : PEAttendeesCollection,
    private contactsCollection : ContactsCollection,
    private productsCollection : ProductsCollection,
    private loader : Loader
    ) {
    this.PharmaEvent = {};
   
    SforceDataContext.getActiveUser()
      .then((currentUser) => {
        console.log("currentUser",currentUser);
        this.activeUser = currentUser;
      });
    
    this.viewEnteredDateTime = moment();

    this.peTypeEventPickListDatasource.getItems()
    .then(items=>{
      console.log("peTypeEventPickListDatasource",items);
      this.picklistDataSource = items;
    })

    this.peStagePickListDatasource.getItems()
    .then(items=>{
      console.log("peStagePickListDatasource",items);
      this.picklistStageSource = items;
    })

    const attendeesCollection: ReferenceCollection = new ReferenceCollection();
    const productItemsCollection: ProductsCollection = new ProductsCollection();

    attendeesCollection
      .fetchAll()
      .then((records) => {
        return attendeesCollection.getAllEntitiesFromResponse(records);
      })
      .then((users) => {
        console.log(users);
        this.attendeesList = users;
        
      });

      productItemsCollection
      .fetchAll()
      .then((records) => {
        return productItemsCollection.getAllEntitiesFromResponse(records);
      })
      .then((users) => {
        console.log(users);
        this.productList = users;
        
      });

       this.dropdownList = [
                              {"id":1,"itemName":"India"},
                              {"id":2,"itemName":"Singapore"},
                              {"id":3,"itemName":"Australia"},
                              {"id":4,"itemName":"Canada"},
                              {"id":5,"itemName":"South Korea"},
                              {"id":6,"itemName":"Germany"},
                              {"id":7,"itemName":"France"},
                              {"id":8,"itemName":"Russia"},
                              {"id":9,"itemName":"Italy"},
                              {"id":10,"itemName":"Sweden"}
                            ];
      
        this.dropdownSettings = {
                                  singleSelection: true,
                                  text:"Select Countries",
                                  selectAllText:'Select All',
                                  unSelectAllText:'UnSelect All',
                                  enableSearchFilter: true,
                                  classes:"myclass custom-class"
                                };

    ConfigurationManager.getConfig()
    .then(config=>{
      console.log("config",config);
      this.pharameventvalidation = config.pharmaEventSettings;
    })
    // this.storage.get("configurationData").then(data=>{
    //   this.pharameventvalidation=data.pharmaEventSettings;
    //   console.log("pharameventvalidation",this.pharameventvalidation);
    //   this.storage.get("currentUserDetails").then(data => { this.loggedInUserDetails = data; console.log("UserData",this.loggedInUserDetails); });
    //   this.service.loadproducts()
    //   .then(data=>{
    //     this.productsData = data.records;
    //   })
    //   .catch(err => alert(JSON.stringify(err)));
    //   this.loadReferenceCollection();
    // })

    // this.storage.get(Constants.PharmaEvent+'PicklistValues').then(picklistValues=>{
    //   console.log("picklistValues",picklistValues);
    //   picklistValues.picklists.forEach(picklistEntries=>{
    //     console.log("picklistEntries",picklistEntries);
    //     if(picklistEntries.fieldName==='Stage__c')
    //     {
    //       this.stagePicklistOptions = picklistEntries.picklistOptions;
    //     }
    //     if(picklistEntries.fieldName==='Type_of_Event__c')
    //     {
    //       this.typeOfEventPicklistOptions = picklistEntries.picklistOptions;
    //     }
    //   })
    // })

  }
  
  public onTapAttendeeHandler() {
    this.gotoAttendeePage();
  }

  public onTapProductHandler() {
    this.gotoProductPage();
  }

  onItemSelect(item:any){
    console.log(item);
    console.log(this.selectedItems);
  }
  OnItemDeSelect(item:any){
      console.log(item);
      console.log(this.selectedItems);
  }
  onSelectAll(items: any){
      console.log(items);
  }
  onDeSelectAll(items: any){
      console.log(items);
  }

  ionViewDidLoad() {
  
  }
  loadPharmaEvent() {
    return this.service.loadPharmaEvent()
      .then(results => {
        console.log('PharmaEvent',results);
        this.PharmaEvent = results.records;
      })
  }
  loadReferenceCollection() {

    return this.service.loadReference()
      .then(results => {
        console.log('references',results);
        this.attendessData = results.records;
      })
  }


  savePharmaEvent(){
  
    let pharmaEvent = new PharmaEvent({
      ownerSfid : this.activeUser.id,
      createdOffline : true,
      ownerFirstName:  this.activeUser.firstName,
      ownerLastName:  this.activeUser.lastName,
      eventType : this.eventType,
      stage : this.stage,
      startDate : this.startDate,
      endDate : this.endDate,
      eventName : this.eventName,
      location : this.location,
      status : PharmaEventScheme.APPROVAL_STATUS_DRAFT,
      speakers : this.speakers,
      objectives : this.objectives,
      agenda : this.agenda,
      evaluation : this.evaluation,
      productPrio1SfId : undefined,
      productPrio2SfId : undefined,
      productPrio3SfId : undefined,
      productPrio4SfId : undefined
    });
    console.log("this.selectedProductsData",this.selectedProductIds, this.selectedProductIds.length);
    for(var i=1;i<= this.selectedProductIds.length; i++) {
      i==1?pharmaEvent.productPrio1SfId = this.selectedProductIds[i] : undefined;
      i==2?pharmaEvent.productPrio2SfId = this.selectedProductIds[i] : undefined;
      i==3?pharmaEvent.productPrio3SfId = this.selectedProductIds[i] : undefined;
      i==4?pharmaEvent.productPrio4SfId = this.selectedProductIds[i] : undefined;
    }

    console.log("pharmaEvent",pharmaEvent);
    this.pharmaEventsCollection.createEntity(pharmaEvent)
    .then(createdData=>{
      console.log("createdData Pharma Event",createdData);
      let success = (data) => {
        console.log("Data Upserted successfully and created data is",createdData._soupEntryId, data);
        //this.navParams.get("parentPage").reloadTOT();

        console.log("attandee list",this.selectedAttendeesStringList , this.selectedAttendeesIds);
        for(var i=1; i<= this.selectedAttendeesIds.length; i++) {
          let saveAttendee = new PEAttendee({
            pharmaEventSfId : createdData._soupEntryId,
            attendeeSfId : this.selectedAttendeesIds[i]
          });

          this.peAttendeesCollection.createEntity(saveAttendee)
          .then(createdAttendee=>{
            console.log("createdAttendee",createdAttendee);
          })
        }

        this.navCtrl.pop();
        const toast = this.toastCtrl.create({
          message: "Add pharma event Created Successfully!",
          showCloseButton:true,
          cssClass:"toastClass",
          position:'middle',
          duration:3000,
        });
        toast.present();
        
        
      }
      success(createdData);
    })
 
    // let success = (data) => {
    //   console.log("Data Upserted successfully and created data is",data);
    //   let speakers = this.selectedAttendeesData;
    //   console.log("speakers",speakers);
    //   console.log("speakers.length",speakers.length);
    //   if(speakers.length>0)
    //   {
    //     for(let i=0;i<speakers.length;i++)
    //     {
    //       let savePeAttendee = {};
    //       savePeAttendee["Attendee__c"] = speakers[i];
    //       savePeAttendee["Pharma_Event__c"] = data[0]._soupEntryId;
    //       console.log("savePeAttendee",savePeAttendee);
    
    //       this.smartStore().upsertSoupEntries(Constants.PEAttendee, [savePeAttendee], (success)=>{ console.log("created attendee successfully",success);}, (failure)=>{ console.log("failed attendee creation",failure);});
    //     }
    //   }
    //   //this.navParams.get("parentPage").reloadPE();
    //   this.navCtrl.pop();

    // }
    // let failure = (error) => {
    //   console.log("Data Upsertion having an issue",error);
    // }

    // this.smartStore().upsertSoupEntries(Constants.PharmaEvent, [savePharmaEvent], success, failure);
}



selectedSpeakers(selectedItems:any)
{
  console.log("selected products",selectedItems);
  this.selectedSpeakersData = selectedItems;
}
validateInputData()
{
  console.log("message",this.message);
  console.log("this.validateInput",this.validateInput());
  if(this.validateInput())
  {
    this.savePharmaEvent();
  }
  else
  {
    const toast = this.toastCtrl.create({
      message: this.message,
      showCloseButton:true,
      cssClass:"toastClass",
      duration:3000,
      position:'middle'
    });
    toast.present();
  }
  
}


validateInput()
  {
    var isValid = true;
    this.message = "";
    console.log("this.startDate",this.pharameventvalidation);
    if((this.startDate=='' || this.startDate==null))
    {
      isValid = false;
      this.message+=" \n Start Date Mandatory";
    }
    if((this.endDate=='' || this.endDate==null))
    {
      isValid = false;
      this.message+=" \n End Date Mandatory";
    }
    if(this.pharameventvalidation.EventName && (this.eventName=='' || this.eventName==null) )
    {
      isValid = false;
      this.message+=" \n Event Name Mandatory ";
    }
    if(moment(this.startDate) < this.viewEnteredDateTime)
    {
      isValid = false;
      this.message+=" \n Start date Value cannot be less than the current Date ";
    }
    if(moment(this.endDate) <= this.viewEnteredDateTime)
    {
      isValid = false;
      this.message+=" \n End date Value cannot be less than the current Date ";
    }
    if(this.pharameventvalidation.EventName && (this.evaluation=='' || this.evaluation==null ))
    {
      isValid = false;
      this.message+=" \n Evaluation Mandatory";
    }
    if(this.pharameventvalidation.TypeOfEvent && (this.eventType=='' || this.eventType==null ))
    {
      isValid = false;
      this.message+=" \n TypeOfEvent Mandatory";
    }
    if(this.pharameventvalidation.Products && (this.selectedProductIds && (this.selectedProductIds.length<=0)))
    {
      isValid = false;
      this.message+=" \n Products Mandatory";
    }
    if(this.selectedProductIds.length > 4)
    {
      isValid = false;
      this.message+=" \n Can not choose more than 4 products";
    }
    if(this.pharameventvalidation.Speakers && (this.speakers=='' || this.speakers==null))
    {
      isValid = false;
      this.message+=" \n Speakers Mandatory";
    }
    if(this.pharameventvalidation.Objectives && (this.objectives=='' || this.objectives==null))
    {
      isValid = false;
      this.message+=" \n Objectives Mandatory";
    }
    return isValid;
  }
  
  
  private gotoAttendeePage() {
    this.navCtrl.push(AttendeesListSelectPage, {
      ids: this.selectedAttendeesIds,
      callback: this.setAttendees.bind(this)
    })
  }
  private gotoProductPage() {
    this.navCtrl.push(ProductListSelectPage, {
      ids: this.selectedProductIds,
      callback: this.setProduct.bind(this)
    })
  }
  
  private setAttendees(attendeesIds) {
    this.selectedAttendeesIds = attendeesIds;
    this.selectedAttendeesStringList = this.selectedAttendeesIds.join(', ');
    
    this.setAttandeesListToView();
  }

  private setProduct(productIds) {
    this.selectedProductIds = productIds;
    this.selectedProductStringList = this.selectedProductIds.join(', ');
    
    this.setProductListToView();
  }
  
  private setAttandeesListToView() {
    if(this.selectedAttendeesIds.length) {
      return this.loader.run(() => {
        return this.contactsCollection.fetchForContactIds(this.selectedAttendeesIds)
          .then((attendees) => {
            this.selectedAttendeesStringList = attendees
              .map((attendee) => attendee.fullName())
              .join(', ');
          })
      })
    }
  
    this.selectedAttendeesStringList = '';
  }

  setProductListToView() {
    if(this.selectedProductIds.length) {
      return this.loader.run(() => {
        return this.productsCollection.fetchForProductIds(this.selectedProductIds)
          .then((product) => {
            this.selectedProductStringList = product
              .map((products) => products.getName())
              .join(', ');
          })
      })
    }
  
    this.selectedProductStringList = '';
  }
  
}
