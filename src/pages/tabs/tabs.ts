import { Component } from '@angular/core';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { ActivitiesPage } from '../activities/activities';
import { OrganizationPage } from '../organization/organization';
// SalesplanningPage
import { SalesplanningPage } from '../salesplanning/salesplanning';
import{ MtpPage } from '../mtp/mtp';
import { TotPage } from '../tot/tot';
import { PowerscorecardPage } from '../powerscorecard/powerscorecard';
//import { DoctorconsentsPage } from '../doctorconsents/doctorconsents';
import { DoctorconsentsDetailsPage } from '../doctorconsents-details/doctorconsents-details';
import { MedicalQueryPage } from '../medical-query/medical-query';
import { CalendarPage } from '../calendar/calendar';
import { SurveyPage } from '../survey/survey';
import { Storage } from '@ionic/storage'
import { MediaPage } from '../media/media.page';
import { PatchPage } from '../patch/patch';
import { OAuth, DataService } from 'forcejs';
import {OrderListPage} from "../order-management/order-list/order-list";
import {DataChangeRequestsPage} from "../data-change-requests/data-change-requests";
import SettingsManager from '../../services/db/SettingsManager';
import {Events} from "ionic-angular";
import { LuncheonmeetingsPage } from '../luncheonmeetings/luncheonmeetings';
import { TourPlanPage } from '../tour-plan/tour-plan';
import { IndiaActivitiesPage } from '../india-activities/india-activities';


@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ActivitiesPage;
  tab3Root = ContactPage;
  tab4Root = OrganizationPage;
  tab5Root = MtpPage;
  tab6Root = CalendarPage;
  tabOrderRoot = OrderListPage;
  tab8Root = TotPage;
  tabMediaRoot = MediaPage;
  tab10Root = MedicalQueryPage;
  tab11Root = SurveyPage;
  tab12Root = PatchPage;
  tab13Root = SalesplanningPage;
  tab14Root = PowerscorecardPage;
  tab15Root = DoctorconsentsDetailsPage;
  tabDCRRoot = DataChangeRequestsPage;
  tabLOMRoot = LuncheonmeetingsPage;
  tab16Root = TourPlanPage;
  tab17Root = IndiaActivitiesPage;
  isMedicalModuleEnabled:boolean = false;
  isSurveyModuleEnabled:boolean = false;
  isOrderManagementEnabled:boolean = false;
  isLuncheonModuleEnabled:boolean = false;
  isSalesplanModuleEnabled: boolean = false;
  isNewPEModuleEnabled: boolean = false;
  isConsentsEnable:boolean= false;
  isDCREnabled:boolean = false;
  isMtpEnabled:boolean = false;
  isIndiEnabled:boolean = false;
  serviceOnline:any;
  currentUserDetails:any;
  config:any;
  isIndiamodule:boolean= false;
  loggeInUserData:any;
  _restPath:string = '/services/apexrest/clmconfiguration';
  seeTabs:boolean = false;
  settings:any;
  constructor(private events: Events) {
    this.setSettingsThenDBLoaded();
    this.setSettingsThenDBSynchronized();
  }
  

  private setSettingsThenDBLoaded() {
    this.events.subscribe('common:dataBaseReady', (time) => {
      console.log(` The database is initialized: ${ new Date(time).toString() } `);
      this.setSettings();
    });
  }
  
  private setSettingsThenDBSynchronized() {
    this.events.subscribe('synchronization:sync-end', () => {
      this.setSettings();
    });
  }

  private setSettings():Promise<void>{
    return SettingsManager.getSettings()
    .then(settings=>{
      console.log("settings",settings);
      this.settings = settings;
      this.isDCREnabled = settings.isDCREnabled;
      this.isMtpEnabled = settings.isMtpModuleEnabled;
      this.isMedicalModuleEnabled = settings.isMedicalModuleEnabled;
      this.isSurveyModuleEnabled = settings.isSurveyModuleEnabled;
      this.isOrderManagementEnabled = settings.isOrderManagementEnabled;
      this.isConsentsEnable = settings.isConsents;
      // this.isSalesplanModuleEnabled = settings.isSalesplanModuleEnabled;
      this.isIndiamodule = settings.isIndiaUser;
     this.isNewPEModuleEnabled  = settings.pharmaEventSettings.isNewPharmaModuleEnable;
    })
  }
}
