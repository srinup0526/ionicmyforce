import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as Constants from '../../services/constants';
import moment from 'moment';
import { TotsCollection } from '../../collections/tots-collection/tots-collection';
import { TotScheme } from '../../models/scheme/TotScheme';
import { TOTPickListDatasource } from '../../services/db/picklist-managers/datasource/TOTPickListDatasource';
import { TOTEventPickListDatasource } from '../../services/db/picklist-managers/datasource/TOTEventPickListDatasource';
import { Utils} from '../../utils/Utils';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import { Tot} from '../../models/tot';

declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-addtot',
  templateUrl: 'addtot.html',
})
export class AddtotPage {
  TOT:any = {}; //All_Day__c:Boolean,Created_Offline__c:Boolean,End_Date__c:String,Start_Date__c:String,Type_First_Quarter__c:String,Type_Fourth_Quarter__c:String, Type_Second_Quarter__c:String, Type_Third_Quarter__c:String, Type__c:String, _soupEntryId:Number
  totPickListSource:any = [];
  loggedInUserDetails: any;
  message = "";
  serviceOnline:any;
  public activeUser:any;
  public totInfo:{};
  allDay:boolean = false;
  fullDay: string = "";
  halfDay: string = "";
  thirdPart: string = "";
  fourthPart: string = "";
  startDate:String = new Date().toISOString();
  endDate: String = new Date().toISOString();
  totDesc: any;
  disabledAllValues:boolean = false;
  public picklistDataSource:any;
  tot:any;
  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }
  constructor(public navCtrl: NavController,
    public storage:Storage,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    private totPicklistDataSource: TOTPickListDatasource,
    private totEventPickListDatasource:TOTEventPickListDatasource,
    private totsCollection:TotsCollection) {
    this.TOT.Start_Date__c = moment().toISOString();
    this.TOT.End_Date__c = moment().toISOString();
    this.TOT.All_Day__c = false;
    SforceDataContext.getActiveUser()
      .then((currentUser) => {
        console.log("currentUser",currentUser);
        this.activeUser = currentUser;
      });
    console.log("totPicklistDataSource",this.totPicklistDataSource);
    this.totPicklistDataSource.getItems()
    .then(items=>{
      console.log("items",items);
      
    })
    this.totEventPickListDatasource.getItems()
    .then(items=>{
      console.log("totEventPicklistDataSource",items);
      this.picklistDataSource = items;
    })

    console.log('ionViewDidLoad addtot page', this.navParams.data['row']);

    if(this.navParams.data['row']) {
      let row= this.navParams.data['row'];
      this.totsCollection.fetchEntityById(this.navParams.data['row'].id).then(totRes=>{
        this.fullDay = totRes.firstQuarterEvent;
        this.halfDay = totRes.secondQuarterEvent;
        this.thirdPart = totRes.thirdQuarterEvent;
        this.fourthPart = totRes.fourthQuarterEvent;
        this.startDate = totRes.startDate;
        this.endDate = totRes.endDate;
        this.totDesc = totRes.description;
        this.allDay = totRes.allDay;
        if(this.allDay) {
          this.blockAllOtherFields(this.allDay);
        }
      })
    }
  }

  saveTOT(){
       
      if(this.navParams.data['row']) {
        this.tot = this.navParams.data['row'];
        this.tot.allDay = this.allDay;
        this.tot.startDate =  this.startDate;
        this.tot.endDate=  this.endDate;
        this.tot.firstQuarterEvent= this.fullDay;
        this.tot.secondQuarterEvent = this.halfDay;
        this.tot.thirdQuarterEvent =  this.thirdPart;
        this.tot.fourthQuarterEvent =  this.fourthPart;
        this.tot.type =  TotScheme.TYPE_OPEN;
        this.tot.description =  this.totDesc;
        this.totsCollection.updateEntity(this.tot)
        .then(createdData=>{
          console.log("createdData",createdData);
          let success = (data) => {
            console.log("Data Upserted successfully and created data is",data);
            //this.navParams.get("parentPage").reloadTOT();
            this.navCtrl.pop();
            const toast = this.toastCtrl.create({
              message: "Time Off Territory Updated Successfully!",
              showCloseButton:true,
              cssClass:"toastClass",
              position:'middle',
              duration:3000,
            });
            toast.present();
          }
          success(createdData);
        }) 
      } else {
        let tot = new Tot({
          createdOffline: true,
          userSfId: this.activeUser.id,
          userLastName:  this.activeUser.firstName,
          userFirstName:  this.activeUser.lastName,
          allDay: this.allDay,
          startDate: this.startDate,
          endDate: this.endDate,
          firstQuarterEvent: this.fullDay,
          secondQuarterEvent: this.halfDay,
          thirdQuarterEvent: this.thirdPart,
          fourthQuarterEvent: this.fourthPart,
          type: TotScheme.TYPE_OPEN,
          description: this.totDesc,
          isSubmittedForApproval: false
        });
        this.totsCollection.createEntity(tot)
        .then(createdData=>{
          console.log("createdData",createdData);
          let success = (data) => {
            console.log("Data Upserted successfully and created data is",data);
            //this.navParams.get("parentPage").reloadTOT();
            this.navCtrl.pop();
            const toast = this.toastCtrl.create({
              message: "Time Off Territory Created Successfully!",
              showCloseButton:true,
              cssClass:"toastClass",
              position:'middle',
              duration:3000,
            });
            toast.present();
          }
          success(createdData);
        }) 
      }
     
   }


  blockAllOtherFields(allDay:boolean)
  {
    console.log("allDay",allDay);
    if(allDay)
    {
      this.TOT.All_Day__c = true;
      this.allDay = true;
      this.disabledAllValues = true;
    }
    else
    {
      this.disabledAllValues = false;
    }
  }

  validateInputData()
{
  console.log("message",this.message);
  //console.log("this.validateInput",this.validateInput());

 // var isValid = true;
  //  this.message = "";
    
  if(this.validateInput())
  {
    
   
    this.saveTOT();
  }
  else
  {
    this.message = this.message.startsWith('\n')?this.message.substr(2):this.message;
    console.log("this.message",this.message);
    const toast = this.toastCtrl.create({
      message: this.message,
      showCloseButton:true,
      cssClass:"toastClass",
      duration:7000,
      position:'middle',
    });
    toast.present();
  }
  
}

getPickInfo(fullday:any)
{
  console.log("getPickInfo",fullday);
}


validateInput()
  {
    var isValid = true;
    this.message = "";
    console.log("check validation",this.fullDay , this.halfDay , this.thirdPart,  this.fourthPart);
    if(this.allDay) {
       if(!this.fullDay) {
         isValid = false;
         this.message+="Full Day Mandatory";
       }
     } else {
      if((this.fullDay == "None" || this.fullDay == "") && (this.halfDay == "None" || this.halfDay == "") && (this.thirdPart == "None" || this.thirdPart == "") && (this.fourthPart == "None" || this.fourthPart == "")) {
        isValid = false;
        this.message+="Full day, Half day, 6 hours or 2 hours Mandatory";
      }
     }
     if(this.startDate == "" || this.startDate == null) {
      isValid = false;
      this.message+="Start Date Mandatory";
     }
     if(this.endDate == "" || this.endDate == null) {
      isValid = false;
      this.message+="End Date Mandatory";
     }
     if(this.startDate > this.endDate) {
      isValid = false;
      this.message+="End Date should be greater than start date";
     }
     //this.startDate = this.startDate.split("T")[0];
    // this.endDate = this.endDate.split("T")[0];
     console.log("Date check" , this.startDate , this.endDate)
    return isValid;  
  }
}
