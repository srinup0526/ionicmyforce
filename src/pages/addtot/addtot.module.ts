import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddtotPage } from './addtot';

@NgModule({
  declarations: [
    AddtotPage,
  ],
  imports: [
    IonicPageModule.forChild(AddtotPage),
  ],
})
export class AddtotPageModule {}
