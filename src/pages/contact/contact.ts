import {Component, ElementRef, ViewChild} from '@angular/core';
import {
  IonicPage, NavController, NavParams, LoadingController, ModalController, AlertOptions,
  Select
} from 'ionic-angular';
import { ContactdetailsPage } from '../contactdetails/contactdetails';
import { OrganizationPage } from '../organization/organization'
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CallReportPage } from '../call-report/call-report';
import { AppointmentPage } from '../appointment/appointment';
import { OrganizationdetailsPage } from '../organizationdetails/organizationdetails';
import {ReferenceCollection} from "../../collections/references/ReferenceCollection";
import {TargetReferencesCollection} from "../../collections/references/TargetReferencesCollection";
import {NonTargetReferencesCollection} from "../../collections/references/NonTargetReferencesCollection";
import { TableHeaderItemOption } from './../../components/table/table-header/TableHeaderItemOption';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { ReferenceScheme } from './../../models/scheme/ReferenceScheme';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import {OrderStatusPickListDatasource} from "../../services/db/picklist-managers/datasource/OrderStatusPicklistDatasource";
import {BrickPickListDatasource} from "../../services/db/picklist-managers/datasource/BrickPicklistDatasource";
import {PriorityPickListDatasource} from "../../services/db/picklist-managers/datasource/PriorityPicklistDatasource";
import {SpecialtyPickListDatasource} from "../../services/db/picklist-managers/datasource/SpecialtyPicklistDatasource";
import {SubtypePickListDatasource} from "../../services/db/picklist-managers/datasource/SubtypePicklistDatasource";
import {FilterPanelComponent} from "../../components/filter/filter-panel/filter-panel";
import {Query} from "../../services/common/Query";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import {StartCallPopup} from "../brand-matrix/start-call-popup/start-call-popup";
import {Reference} from "../../models/Reference";


@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})

export class ContactPage {

  public isSearchbarOpened = false;
  public contactsCount:any = 0;

  targetNontarget:string = "tc";
  
  references: Array<any> = [];
  
  public tableOptions: LazyTableOption;
  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: ReferenceCollection;
  public filterOptions: AlertOptions;
  public settings: SettingsImpl;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('filterType') filterTypeComponent: Select;
  @ViewChild('mainContent') mainContent: ElementRef;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private service: SmartstoreServiceProvider,
              public modalCtrl: ModalController,
              public ngxDatatablemodule: NgxDatatableModule,
              public loadingCtrl: LoadingController,
              private startCallPopup: StartCallPopup,
              private referenceCollection: ReferenceCollection,
              private targetReferencesCollection: TargetReferencesCollection,
              private nonTargetReferencesCollection: NonTargetReferencesCollection,
              private orderStatusPickListDatasource: OrderStatusPickListDatasource,
              private brickPickListDatasource: BrickPickListDatasource,
              private priorityPickListDatasource: PriorityPickListDatasource,
              private specialtyPickListDatasource: SpecialtyPickListDatasource,
              private subtypePickListDatasource: SubtypePickListDatasource) {
  
    this.settings = Settings.getInstance();
    
    const headerItems = [
        {
          title: 'common.names.Contact',
          fields: [ReferenceScheme.fields.contactLastName, ReferenceScheme.fields.contactFirstName],
          isSortable: true,
          isAsc: true,
          isActive: false,
          picklistValues: {},
          modelFunction: 'contactFullNameWithType'
        },
        {
          title: 'common.names.Priority',
          fields: [ReferenceScheme.fields.priority],
          isSortable: true,
          isAsc: true,
          isActive: false,
          picklistValues: {}
        },
        {
          title: 'common.names.Specialty',
          fields: [ReferenceScheme.fields.specialty],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.AtCalls',
          fields: [ReferenceScheme.fields.atCalls],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.LastCall',
          fields: [ReferenceScheme.fields.lastCall],
          isSortable: true,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Organization',
          fields: [ReferenceScheme.fields.organizationName],
          isAsc: true,
          isSortable: true,
          isActive: false,
          modelFunction: 'organizationNameAndAddress',
          cssClass: ['hyperlink'],
          handler: (event, reference) => {
            this.gotoOrganization(reference);
            event.stopPropagation();
          }
        },
        {
          title: 'common.names.Appt',
          fields: [],
          cssClass: ['appt'],
          handler: (event, reference) => {
            this.gotoCreateAppointment(reference);
            event.stopPropagation();
          }
        },
        {
          title: 'common.names.Call',
          fields: [],
          cssClass: ['call-report'],
          handler: (event, reference) => {
            console.log("reference",reference);
            event.stopPropagation();
            this.gotoCreateCallReport(reference);
          }
        }
        ];

    if(this.settings.isChinaUser()) {
      headerItems.splice(3,1);
    }
    
    this.tableOptions = {
      headerItems: headerItems,
      rowHandler: (event, reference) => {
        this.gotoContactDetail(reference);
      },
      batchSize: 100
    };
    
    this.searchString = '';

    this.collection = this.targetReferencesCollection;
    
     this.filterOptions = {
       cssClass: 'popup alert list without-header',
       buttons: []
     };
  }

  public onChangeQueryHandler(query: Query): void {
    this.setReferenceCount();
  }
  
  public onChangeContactTypeHandler(type) {
    if(type == 'tc') {
      this.collection = this.targetReferencesCollection;
    }else{
      this.collection = this.nonTargetReferencesCollection;
    }
    
    this.targetNontarget = type;
    
    this.filterTypeComponent.close();
    
    this.lazyTable.setCollection(this.collection);
    this.lazyTable.reloadTable();
  }
  
  public onSwipeHandler(event){
    switch (event.offsetDirection) {
      case 2: this.filterPanel.hidePanel(); break;
      case 4: this.filterPanel.showPanel(); break;
    }
  }

  public onFilterChangeHandler(filterData): void {
    this.lazyTable.reloadTableByFilterData(filterData);
  }

  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
  
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public ionViewDidLoad() : void {}

  public ionViewDidEnter(): void {}

  public ionViewWillEnter(): void {}
  
  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable();

    this.setCssClassForChina();
  }
  
  private setCssClassForChina() {
    if(this.settings.isChinaUser()) {
      this.mainContent.nativeElement.classList.add('china');
    }
  }

  private setReferenceCount(): number {
    const countQuery = this.collection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);

    return this.collection.runCountQuery(query.query)
      .then((count) => {
        return this.contactsCount = count || 0;
      })
  }

  private gotoContactDetail(reference): void {
    this.navCtrl.push(ContactdetailsPage, { "contactSfId": reference.contactSfId });
  }

  private gotoCreateCallReport(reference: Reference): Promise<any> {
    if (Settings.getInstance().isBrandMatrixModuleEnabled()) {
      return this.startCallPopup.showPopup(reference);
    }
    // if any changes on open call report here please apply to
    // start-call-popup.ts openCallReport
    // and
    // brand-matrix.ts openCallReport
    return this.navCtrl.push(CallReportPage,{ "row": reference, kpi: {} });
  }

  private gotoCreateAppointment(row: any): void {
    //alert('Create Appointment');
    this.navCtrl.push(AppointmentPage,{ "row": row });
  }

  private gotoOrganization(reference:any): void {
      this.navCtrl.push(OrganizationdetailsPage, { "organizationSfId": reference.organizationSfId });
  }
}
