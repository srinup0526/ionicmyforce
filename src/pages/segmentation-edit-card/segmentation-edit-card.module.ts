import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SegmentationEditCardPage } from './segmentation-edit-card';

@NgModule({
  declarations: [
    SegmentationEditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(SegmentationEditCardPage),
  ],
})
export class SegmentationEditCardPageModule {}
