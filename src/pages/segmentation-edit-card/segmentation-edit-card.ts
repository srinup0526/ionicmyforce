import { Component } from '@angular/core';
import {
  IonicPage, NavController, NavParams, AlertOptions, Platform, AlertController,
  ToastController
} from 'ionic-angular';
import {BackHandlers} from "../../utils/BackHandlers";
import {Loader} from "../../services/common/Loader";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {Reference} from "../../models/Reference";
import {ProductSegmentation} from "../../models/ProductSegmentation";
import {DataChangeRequestHelper} from "../data-change-requests/DataChangeRequestHelper";
import {ProductSegmentationScheme} from "../../models/scheme/ProductSegmentationScheme";
import {ProductsCollection} from "../../collections/ProductsCollection";
import {ProductSegmentationPickListManager} from "../../services/db/picklist-managers/ProductSegmentationPickListManager";
import {PickListDatasource} from "../../services/db/picklist-managers/base/PicklistDatasource";


@IonicPage()
@Component({
  selector: 'page-segmentation-edit-card',
  templateUrl: 'segmentation-edit-card.html',
})
export class SegmentationEditCardPage extends BackHandlers{
  
  public filterOptions: AlertOptions;
  public segmentationFirstValues: Array<{id: string, description: string}>;
  public segmentationSecondValues: Array<{id: string, description: string}>;
  public productPicklistValues: Array<{id: string, description: string}>;
  
  public reference: Reference;
  public productSegmentation: ProductSegmentation;
  public productSegmentationStartBackup: ProductSegmentation;
  public ProductSegmentationScheme;
  
  private isEditMode: boolean;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private dataChangeRequestHelper: DataChangeRequestHelper,
              private toastCtrl: ToastController,
              private productsCollection: ProductsCollection,
              private productSegmentationPickListManager: ProductSegmentationPickListManager,
              localizationManager: LocalizationManager,
              loader: Loader,
              alertCtrl: AlertController,
              platform: Platform) {
    super(navCtrl, localizationManager, loader, alertCtrl, platform);
    
    this.filterOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };
  
    this.reference = new Reference({});
    this.productSegmentation = new ProductSegmentation({});
    
    this.segmentationFirstValues = [];
    this.segmentationSecondValues = [];
    this.productPicklistValues = [];
    this.isEditMode = false;
    
    this.ProductSegmentationScheme = ProductSegmentationScheme;
  }
  
  public onTapSaveBtnHandler() {
    this.save();
  }
  
  public ionViewDidLoad() {
    this.reference = this.navParams.data['reference'] || this.reference;
    this.productSegmentation = this.navParams.data['row'] || this.productSegmentation;
    
    this.isEditMode = !!this.productSegmentation.phProductId;
    
    this.initData()
      .then(() => {
        return this.setStartBackup();
      });
    
    super.ionViewDidLoad();
  }
  
  public setDataAndCloseSelect(localField, value, ref) {
    this.productSegmentation[localField] = value;
    ref.close();
  }
  
  protected validateAndSaveOrder(): Promise<any> {
    return this.save();
  }
  
  private initData() {
    return Promise.all([
      this.fetchAndSetProducts(),
      this.fetchPicklists()
    ]);
  }
  
  private fetchAndSetProducts(): Promise<any> {
    return this.productsCollection.fetchAll()
      .then((response) => this.productsCollection.getAllEntitiesFromResponse(response))
      .then((products) => products.map((product) => ({
        id: product.id,
        description: product.name
      })))
      .then((picklist) => {
        this.productPicklistValues = picklist;
      })
  }
  
  private setStartBackup() {
    this.productSegmentationStartBackup = new ProductSegmentation(this.productSegmentation);
  }
  
  protected validate(): Promise<any> {
    const validationFields = this.getLocalizationKeysForRequiredFields();
    const localizationKeys = this.getLocalizationHeaderKey();
    const localizationFieldsKeys = Object
      .keys(validationFields)
      .map((fieldLocalId) => {
        return validationFields[fieldLocalId];
      });
    
    
    return this.localizationManager.getSeveralLocales(localizationKeys.concat(localizationFieldsKeys))
      .then((localization) => {
        return new Promise((resolve, reject) => {
          const toastHeader = `${localization['common.alerts.NoRequiredFields']} \n`;
          
          const toastBody = Object
            .keys(validationFields)
            .filter((fieldLocalId) => !this.productSegmentation[fieldLocalId])
            .map((fieldLocalId) => localization[validationFields[fieldLocalId]])
            .join('\n - ');
          
          
          const isDataValid = toastBody.length == 0;
          
          if (isDataValid) {
            return resolve();
          }
          
          return reject(new Error(toastHeader + '- ' + toastBody));
        });
      });
  }
  
  protected hasChanges() {
    return this.isSameFillFields(this.productSegmentation, this.productSegmentationStartBackup, ProductSegmentationScheme.fields);
  }
  
  private getLocalizationHeaderKey(): Array<string> {
    return [
      'common.alerts.NoRequiredFields'
    ];
  }
  
  private getLocalizationKeysForRequiredFields(): { [key: string]: string } {
    let validationFields = {};
    
    validationFields[ProductSegmentationScheme.fields.phProductId.local] = 'common.names.PharmaProduct';
    validationFields[ProductSegmentationScheme.fields.segmentation1.local] = 'common.names.Segmentation1';
    validationFields[ProductSegmentationScheme.fields.segmentation2.local] = 'common.names.Segmentation2';
    
    return validationFields;
  }
  
  private save(): Promise<any> {
    return this.validate()
      .then(() => this.doSave())
      .then(() => this.goBack())
      .catch((error: Error) => {
        return this.showValidateToast(error.message);
      })
  }
  
  private doSave() {
    if (this.isEditMode) {
      return this.dataChangeRequestHelper.editProductSegmentation(this.productSegmentation, this.reference);
    }
    
    return this.dataChangeRequestHelper.createProductSegmentation(this.productSegmentation, this.reference);
  }
  
  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton: true,
      cssClass: "toastClass",
      duration: 5000,
    });
    
    return toast.present();
  }
  
  private fetchPicklists(): Promise<any> {
    return this.productSegmentationPickListManager.getPickLists()
      .then((picklist) => {
        
        const pickListDatasource = new PickListDatasource(this.localizationManager);
        
        return Promise.all([
          pickListDatasource.preparePickListItems(picklist[ProductSegmentationScheme.fields.segmentation1.sfdc] || []),
          pickListDatasource.preparePickListItems(picklist[ProductSegmentationScheme.fields.segmentation2.sfdc] || [])
        ])
          .then(([segmentationFirstValues, segmentationSecondValues]) => {
            this.segmentationFirstValues = segmentationFirstValues;
            this.segmentationSecondValues = segmentationSecondValues;
          });
      })
  }
}
