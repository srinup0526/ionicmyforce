import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactChinaEditCardPage } from './contact-china-edit-card';

@NgModule({
  declarations: [
    ContactChinaEditCardPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactChinaEditCardPage),
  ],
})
export class ContactChinaEditCardPageModule {}
