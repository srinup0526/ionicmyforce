import {Component, ViewChild, ElementRef} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertOptions, Select, ToastController, AlertController, Platform} from 'ionic-angular';
import {Contact} from "../../models/Contact";
import {Settings, SettingsImpl} from './../../services/db/Settings';
import {OrganizationListSelectPage} from "../organization-list-select/organization-list-select";
import {Organization} from "../../models/Organization";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {DataChangeRequestHelper} from "../data-change-requests/DataChangeRequestHelper";
import {ContactScheme} from "../../models/scheme/ContactScheme";
import {ProductsCollection} from "../../collections/ProductsCollection";
import {ContactChinaPicklistManager} from "../../services/db/picklist-managers/ContactChinaPicklistManager";
import {BackHandlers} from "../../utils/BackHandlers";
import {Loader} from "../../services/common/Loader";
import {OrganizationsCollection} from "../../collections/OrganizationsCollection";
import {PickListDatasource} from "../../services/db/picklist-managers/base/PicklistDatasource";


@IonicPage()
@Component({
  selector: 'page-contact-china-edit-card',
  templateUrl: 'contact-china-edit-card.html',
})
export class ContactChinaEditCardPage extends BackHandlers{
  public settings: SettingsImpl;
  public contact: Contact;
  public contactStartBackup: Contact;
  public organization: Organization;
  public kols: Array<any>;
  public filterOptions: AlertOptions;
  public ContactScheme;
  
  public hcpStatusPicklistValues: Array<{id: string, description: string}>;
  public statusDescriptionPicklistValues: Array<{id: string, description: string}>;
  public genderPicklistValues: Array<{id: string, description: string}>;
  public productPicklistValues: Array<{id: string, description: string}>;
  
  
  @ViewChild('description') descriptionArea;
  @ViewChild('organizationAddress') organizationAddressArea;
  @ViewChild('organizationName') organizationNameArea;
  
  @ViewChild('kolSelect') kolSelect: Select;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private productsCollection: ProductsCollection,
              private contactChinaPicklistManager: ContactChinaPicklistManager,
              private dataChangeRequestHelper: DataChangeRequestHelper,
              private organizationsCollection: OrganizationsCollection,
              localizationManager: LocalizationManager,
              loader: Loader,
              alertCtrl: AlertController,
              platform: Platform) {
  
    super(navCtrl, localizationManager, loader, alertCtrl, platform);
    
    this.contact = new Contact({});
    this.contactStartBackup = new Contact({});
    
    this.organization = new Organization({});
    this.settings = Settings.getInstance();
    this.ContactScheme = ContactScheme;
    
    this.filterOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };
  
    this.productPicklistValues = [];
    this.genderPicklistValues = [];
    this.statusDescriptionPicklistValues = [];
    this.hcpStatusPicklistValues = [];
  }
  
  public onTapSaveBtnHandler() {
    this.save();
  }
  
  public setDataAndCloseSelect(localField, value, ref) {
    this.contact[localField] = value;
    ref.close();
  }
  
  public onTapAccountBtnHandler() {
    this.gotoAccountPage();
  }
  
  public ionViewDidLoad() {
    this.contact = this.navParams.data['row'];
    
    this.initData()
      .then(() => {
        return this.setStartBackup();
      });
  
    super.ionViewDidLoad();
  }
  
  public isShowContactDescriptionEnable(): boolean {
    return !!this.settings.showContactDescription || !!this.contact.description;
  }
  
  protected validateAndSaveOrder(): Promise<any> {
    return this.save();
  }
  
  protected hasChanges() {
    return this.isSameFillFields(this.contact, this.contactStartBackup, ContactScheme.fields);
  }
  
  private gotoAccountPage() {
    this.navCtrl.push(OrganizationListSelectPage, {
      organizationId: this.contact.organizationSfId,
      callback: this.setAccount.bind(this)
    })
  }
  
  private setAccount(organization) {
    this.organization = organization;
    this.contact.organizationSfId = this.organization.id;
  }
  
  protected validate(): Promise<any> {
    const validationFields = this.getLocalizationKeysForRequiredFields();
    const localizationKeys = this.getLocalizationHeaderKey();
    const localizationFieldsKeys = Object
      .keys(validationFields)
      .map((fieldLocalId) => {
        return validationFields[fieldLocalId];
      });
    
    
    return this.localizationManager.getSeveralLocales(localizationKeys.concat(localizationFieldsKeys))
      .then((localization) => {
        return new Promise((resolve, reject) => {
          const toastHeader = `${localization['common.alerts.NoRequiredFields']} \n`;
          
          const toastBody = Object
            .keys(validationFields)
            .filter((fieldLocalId) => !this.contact[fieldLocalId])
            .map((fieldLocalId) => localization[validationFields[fieldLocalId]])
            .join('\n - ');
          
          
          const isDataValid = toastBody.length == 0;
          
          if (isDataValid) {
            return resolve();
          }
          
          return reject(new Error(toastHeader + '- ' + toastBody));
        });
      });
  }
  
  private getLocalizationHeaderKey(): Array<string> {
    return [
      'common.alerts.NoRequiredFields'
    ];
  }
  
  private getLocalizationKeysForRequiredFields(): { [key: string]: string } {
    let validationFields = {};
    
    validationFields[ContactScheme.fields.organizationSfId.local] = 'common.names.Organization';
    validationFields[ContactScheme.fields.hcpStatus.local] = 'common.names.hcpStatus';
    validationFields[ContactScheme.fields.statusDescription.local] = 'common.names.StatusDescription';
    validationFields[ContactScheme.fields.firstName.local] = 'common.names.PhysicianFirstName';
    validationFields[ContactScheme.fields.lastName.local] = 'common.names.PhysicianLastName';
    validationFields[ContactScheme.fields.gender.local] = 'common.names.Gender';
    validationFields[ContactScheme.fields.namedDepartment.local] = 'common.names.NamedDepartment';
    validationFields[ContactScheme.fields.standardDepartment.local] = 'common.names.StandardDepartment';
    validationFields[ContactScheme.fields.administrative.local] = 'common.names.Administrative';
    validationFields[ContactScheme.fields.professionalTitle.local] = 'common.names.ProfessionalTitle';
    validationFields[ContactScheme.fields.academicTitle.local] = 'common.names.AcademicTitle';
    validationFields[ContactScheme.fields.hcpType.local] = 'common.names.hcpType';
    validationFields[ContactScheme.fields.territoryCode.local] = 'common.names.TerritoryCode';
    
    return validationFields;
  }
  
  private save(): Promise<any> {
    return this.validate()
      .then(() => this.doSave())
      .then(() => this.goBack())
      .catch((error: Error) => {
        return this.showValidateToast(error.message);
      })
  }
  
  private doSave() {
    return this.dataChangeRequestHelper.createContact(this.contact, this.organization);
  }
  
  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton: true,
      cssClass: "toastClass",
      duration: 5000,
    });
    
    return toast.present();
  }
  
  private fetchAndSetProducts(): Promise<any> {
    return this.productsCollection.fetchAll()
      .then((response) => this.productsCollection.getAllEntitiesFromResponse(response))
      .then((products) => products.map((product) => ({
        id: product.id,
        description: product.name
      })))
      .then((picklist) => {
        this.productPicklistValues = picklist;
      })
  }
  
  private initData() {
    return Promise.all([
      this.fetchAndSetProducts(),
      this.fetchPicklists(),
      this.fetchOrganization()
    ])
  }
  
  private fetchOrganization(): Promise<any> {
    if(!this.contact.id){
      return Promise.resolve();
    }
    return this.contact.getOrganizationByReference()
      .then((organization) => {
        if(organization) {
          this.organization = organization;
        }
      })
  }
  
  private fetchPicklists(): Promise<any> {
    return this.contactChinaPicklistManager.getPickLists()
      .then((picklist) => {
  
        const pickListDatasource = new PickListDatasource(this.localizationManager);
      
        return Promise.all([
          pickListDatasource.preparePickListItems(picklist[ContactScheme.fields.gender.sfdc] || []),
          pickListDatasource.preparePickListItems(picklist[ContactScheme.fields.statusDescription.sfdc] || []),
          pickListDatasource.preparePickListItems(picklist[ContactScheme.fields.hcpStatus.sfdc] || [])
        ])
          .then(([genderPicklistValues, statusDescriptionPicklistValues, hcpStatusPicklistValues]) => {
            this.genderPicklistValues = genderPicklistValues;
            this.statusDescriptionPicklistValues = statusDescriptionPicklistValues;
            this.hcpStatusPicklistValues = hcpStatusPicklistValues;
          });
      })
  }
  
  private setStartBackup() {
    this.contactStartBackup = new Contact(this.contact);
  }
}
