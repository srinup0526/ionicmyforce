import { Component } from '@angular/core';
import { IonicPage, LoadingController, ToastController, NavParams, NavController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ConfirmationDialogService } from '../calendar/confirmation-dialog/confirmation-dialog.service';
import { RedFlagCollection } from '../../collections/RedFlagCollection';
import { RedFlag } from '../../models/RedFlag';
import { RedFlagScheme } from '../../models/scheme/RedFlagScheme';



/**
 * Generated class for the AddRedFlagPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-red-flag',
  templateUrl: 'add-red-flag.html',
})
export class AddRedFlagPage {

	redFlagInfo:any;
	row:any;
	contactName:string='';
	redFlagType:string='';
	contactNumber:string='';
	contactEmail:string='';
	description:string='';
	photo:string = '';
	imageURI:any;
	imageFileName:any;
	errMsg:string = '';
	soupEntryId:string = '';
	serviceOnline:any;
	deleteButton:boolean = false;
	base64Image:string = '';


  constructor(
              private transfer: FileTransfer,
              private confirmationDialogService: ConfirmationDialogService,
              private camera: Camera,
              public loadingCtrl: LoadingController,
              public toastCtrl: ToastController,
              public navCtrl: NavController,
              public navParams: NavParams,
              public redFlagCollection:RedFlagCollection
            ) {
  	console.log("row",this.navParams.data['row'])
  	this.row = this.navParams.data['row'];
    this.contactName = this.row.name;
  	this.redFlagCollection.fetchEntityBycontactSfId(this.row.contactSfId).then(res=>{
  		console.log("res",res);
  		if(res.length>0)
  		{
  			this.redFlagInfo = res[0];
  			console.log("this.redFlagInfo",res[0]);
  			this.redFlagType = this.redFlagInfo.redFlagType;
  			this.contactNumber = this.redFlagInfo.contactNumber;
  			this.contactEmail = this.redFlagInfo.email;
  			this.description = this.redFlagInfo.description;
  			this.deleteButton = true;
  			this.imageURI = this.redFlagInfo.photo;
  		}
  		else
  		{
  			this.redFlagInfo = new RedFlag({});
  		}
  		console.log("this.redFlagInfo",this.redFlagInfo);
  	})
  }

  getImage() {
  	const options: CameraOptions = {
  		quality: 100,
  		destinationType: this.camera.DestinationType.FILE_URI,
  		sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
  	}

  	this.camera.getPicture(options).then((imageData) => {
  		this.imageURI = imageData;
  		this.base64Image = 'data:image/jpeg;base64,' + imageData;
  	}, (err) => {
  		console.log(err);
  		this.presentToast(err);
  	});
  }

  ionViewWillEnter():void {}

  presentToast(msg) {
  	let toast = this.toastCtrl.create({
  		message: msg,
  		duration: 3000,
  		cssClass:"toastClass",
  		showCloseButton:true,
  		position:'middle',
  	});

  	toast.onDidDismiss(() => {
  		console.log('Dismissed toast');
  	});

  	toast.present();
  }

  ionViewDidLoad():void {
    console.log('ionViewDidLoad AddRedFlagPage');
  }

  validateInputData()
  {
  	if(this.validate())
  	{
  		this.saveRedFlag();
  	}
  	else
  	{
  		this.errMsg = this.errMsg.startsWith('\n')?this.errMsg.substr(2):this.errMsg;
  		console.log("this.message",this.errMsg);
  		this.presentToast(this.errMsg);
  	}
  }

  validate()
  {
  	let isValid = true;
  	this.errMsg = 'Please fill all required fields';
  	if(!this.contactName)
  	{
  		isValid = false;
  		this.errMsg+='\n Contact Name';
  	}
  	if(!this.redFlagType)
  	{
  		isValid = false;
  		this.errMsg+='\n Type';
  	}
  	if(!this.contactNumber)
  	{
  		isValid = false;
  		this.errMsg+='\n Contact Number';
  	}
  	if(!this.contactEmail)
  	{
  		isValid = false;
  		this.errMsg+='\n Contact Email';
  	}
  	if(!this.description)
  	{
  		isValid = false;
  		this.errMsg+='\n Description';
  	}

  	return isValid;

  }

  saveRedFlag()
  {
  	  if(this.deleteButton)
      {
        this.redFlagInfo.redFlagType = this.redFlagType;
        this.redFlagInfo.contactNumber = this.contactNumber;
        this.redFlagInfo.email = this.contactEmail;
        this.redFlagInfo.description = this.description;
        this.redFlagInfo.photo = this.imageURI;
        this.redFlagCollection.updateEntity(this.redFlagInfo).then(res=>{
          this.navCtrl.pop();
          this.presentToast("Red Flag Updated Successfully!");
        })
      }
      else
      {
        const redFlagInfo = new RedFlag({
          "customerName":this.row.contactSfId,
          "redFlagType":this.redFlagType,
          "contactNumber": this.contactNumber,
          "email":this.contactEmail,
          "description":this.description,
          "photo":this.imageURI
        })
        console.log("redFlagInfo",redFlagInfo);
        
        this.redFlagCollection.createEntity(redFlagInfo).then(res=>{
          console.log("res",res);
          this.navCtrl.pop();
          this.presentToast("Red Flag Created Successfully!");

        })
      }
  }

  deleteRedFlag(event:any)
  {
    this.confirmationDialogService.confirm('Please confirm..', 'Are you sure, want to delete this Red Flag ?')
    .then((confirmed) => {
      console.log('User confirmed:', confirmed);
      this.redFlagCollection.removeEntity(this.redFlagInfo).then(result=>{
        this.presentToast("Red Flag Deleted Successfully");
        this.navCtrl.pop();
      })
    })
    .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }
}
