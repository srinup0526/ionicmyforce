import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddRedFlagPage } from './add-red-flag';

@NgModule({
  declarations: [
    AddRedFlagPage,
  ],
  imports: [
    IonicPageModule.forChild(AddRedFlagPage),
  ],
})
export class AddRedFlagPageModule {}
