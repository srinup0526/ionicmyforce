import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataChangeRequestsOrganizationCardPage } from './data-change-requests-organization-card';

@NgModule({
  declarations: [
    DataChangeRequestsOrganizationCardPage,
  ],
  imports: [
    IonicPageModule.forChild(DataChangeRequestsOrganizationCardPage),
  ],
})
export class DataChangeRequestsOrganizationCardPageModule {}
