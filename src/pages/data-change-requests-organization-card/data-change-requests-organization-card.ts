import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Settings, SettingsImpl } from '../../services/db/Settings';
import {DataChangeRequest} from "../../models/DataChangeRequest";
import {Loader} from "../../services/common/Loader";
import {ConfirmPopup} from "../../components/popups/ConfirmPopup";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import {DataChangeRequestsCollection} from "../../collections/DataChangeRequestsCollection";
import {BrickPickListDatasource} from "../../services/db/picklist-managers/datasource/BrickPicklistDatasource";
import {DataChangeRequestPickListManager} from "../../services/db/picklist-managers/DataChangeRequestPickListManager";
import {BricksCollection} from "../../collections/BricksCollection";
import {RecordTypeCollection} from "../../collections/RecordTypeCollection";


@IonicPage()
@Component({
  selector: 'page-data-change-requests-organization-card',
  templateUrl: 'data-change-requests-organization-card.html',
})
export class DataChangeRequestsOrganizationCardPage {

  public isChinaUser: boolean;
  public isDeleteBtnEnable: boolean;
  public dcr: DataChangeRequest;
  public picklists;
  public brickPicklist;
  public recordTypePicklist;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public dataChangeRequestsCollection: DataChangeRequestsCollection,
              private loader: Loader,
              private alertCtrl: AlertController,
              private localizationManager: LocalizationManager,
              private brickPicklistDatasource: BrickPickListDatasource,
              private dataChangeRequestPickListManager: DataChangeRequestPickListManager,
              private bricksCollection: BricksCollection,
              private recordTypeCollection: RecordTypeCollection) {

    const settings = Settings.getInstance();

    this.isDeleteBtnEnable = false;
    this.isChinaUser = settings.isChinaUser();
    this.dcr = new DataChangeRequest({});
    this.picklists = {};
    this.brickPicklist = [];
    this.recordTypePicklist = [];
  }

  public onTapDeleteBtnHandler() {
    this.showDeleteConfirmPopup()
      .then((result) => {
        if (result.doAction) {
          return this.deleteDcr()
            .then(() => this.goBack());
        }
      })
  }

  public ionViewDidLoad() {
    this.initParams();
  }

  public initParams() {
    this.dcr = this.navParams.data['row'];

    return this.loader.run(this.init.bind(this));
  }

  public getBrickPicklistValues(value) {
    return this.getPicklistLabel(this.brickPicklist, value);
  }

  public getRecordTypePicklistValues(value) {
    return this.getPicklistLabel(this.recordTypePicklist, value);
  }

  public getPicklistValues(sfdcField, value) {
    if(!this.picklists[sfdcField]) {
      return value;
    }

    return this.getPicklistLabel(this.picklists[sfdcField], value);
  }

  private getPicklistLabel(picklist, value){
    const pickListItem = picklist.filter((item) => item.value == value)[0];

    return pickListItem ? pickListItem.label : value;
  }

  private init() {
    this.initDeleteBtn();
    return this.fetchPicklists()
      .then(this.setPicklist.bind(this))
      .then(this.fetchBrickPicklists.bind(this))
      .then(this.setBrickPicklist.bind(this))
      .then(this.fetchRecordTypePicklists.bind(this))
      .then(this.setRecordTypePicklist.bind(this))
  }

  private initDeleteBtn() {
    this.isDeleteBtnEnable = this.dcr.isDraftStatus();
  }

  private fetchRecordTypePicklists() {
    return this.fetchCollectionAsPicklist(this.recordTypeCollection);
  }

  private fetchBrickPicklists() {
    return this.fetchCollectionAsPicklist(this.bricksCollection);
  }

  private fetchPicklists() {
    return this.dataChangeRequestPickListManager.getPickLists()
  }

  private setBrickPicklist(picklists) {
    this.brickPicklist = picklists;
  }

  private setPicklist(picklists) {
    this.picklists = picklists;
  }

  private setRecordTypePicklist(picklists) {
    this.recordTypePicklist = picklists;
  }

  private showDeleteConfirmPopup(): Promise<{ doAction: boolean }> {
    const confirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);

    return confirmPopup.showPopup({
      title: 'card.DCR.ConfirmationPopup.DeleteItem.Caption',
      message: 'card.DCR.ConfirmationPopup.DeleteItem.Message',
      yesLabel: 'common.buttons.YesBtn',
      noLabel: 'common.buttons.NoBtn'
    });
  }

  private deleteDcr(): Promise<any> {
    return this.dataChangeRequestsCollection.removeEntity(this.dcr);
  }

  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }

  private fetchCollectionAsPicklist(collection) {
    return collection.fetchAll()
      .then((response) => {
        return collection.getAllEntitiesFromResponse(response);
      })
      .then((records) => {
        return records.map((record) => ({
          value: record.id,
          label: record.name
        }))
      });
  }

}
