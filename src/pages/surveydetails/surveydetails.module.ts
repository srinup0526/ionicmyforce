import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SurveydetailsPage } from './surveydetails';

@NgModule({
  declarations: [
    SurveydetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(SurveydetailsPage),
  ],
})
export class SurveydetailsPageModule {}
