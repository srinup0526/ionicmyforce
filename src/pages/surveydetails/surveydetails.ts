import { Component,ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController,AlertOptions, NavParams,Select, LoadingController, Loading, ModalController, ModalOptions, ToastController,
  AlertController, Navbar, Platform } from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import { ReferenceCollection} from '../../collections/references/ReferenceCollection';
import { Reference} from '../../models/Reference';
import { SurveyCollection } from '../../collections/SurveyCollection';
import { SurveyQuestionnaireCollection } from '../../collections/SurveyQuestionnaireCollection';
import { SurveyQuestionnaireDetailCollection } from '../../collections/SurveyQuestionnaireDetailCollection';
import { SurveyQuestionnaire } from "../../models/SurveyQuestionnaire";
import { SurveyQuestionnaireDetail} from '../../models/SurveyQuestionnaireDetail';
import { SurveyQuestionnaireDetailScheme } from '../../models/scheme/SurveyQuestionnaireDetailScheme';
import { LocalizationManager } from '../../services/common/localizations/LocalizationManager';
import { ContactsCollection } from '../../collections/ContactsCollection';

/**
* Generated class for the SurveydetailsPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-surveydetails',
  templateUrl: 'surveydetails.html',
})
export class SurveydetailsPage {
  surveyid:any;
  public survey:any;
  showItem: boolean;
  Referenceteam:any;
  contactsBackup:any;
  customer:string;
  nonCustomer:string;
  surveyQuestinaire:any;
  customerList: any;
  surveyDetails: any;
  customerName:any;
  msg:string='';
  selectedCustomer:Reference;
  surveyQuestionandAnswers: any;
  surveyResponseData:Array<any> = [];
  public references: Reference[];
  number: any;
  public surveyQuestionaire:SurveyQuestionnaire[] = [];

  //values

  textValue: any;
  numericValue: any;
  radioValue: any;
  checkboxValue: any;
  multicheckboxValue: any;
  dynamicSurveyBinder:any=[];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private service: SmartstoreServiceProvider,
    private referenceCollection: ReferenceCollection,
    private surveyCollection: SurveyCollection,
    private surveyQuestinaireCollection: SurveyQuestionnaireCollection,
    private surveyQuestionarieDetailsCollection: SurveyQuestionnaireDetailCollection,
    private localization:LocalizationManager,
    public toastCtrl: ToastController,
    public contactsCollection:ContactsCollection
    ) {
    this.fetchAllQuery()
    .then(res=>{
      this.customerList = res;
    })
    this.surveyDetails = this.navParams.get('row');
    console.log("survey information in detail page",this.surveyDetails.id);
    this.surveyQuestinaireCollection.fetchBySurveyId(this.surveyDetails.id)
    .then(res=>{
      this.surveyQuestionaire = res;
      this.prepareDynamicSurveyItems(res);
    })
    this.getAllSurveyResponse();
  }


  public getAllSurveyResponse():Promise<void>
  {
    return this.surveyQuestionarieDetailsCollection.fetchBySurveyId(this.surveyDetails.id).then(surveyData=>{
      console.log("surveyData",surveyData);
        this.surveyResponseData = surveyData;
      console.log("surveyResponseData",this.surveyResponseData);
    })
  }


  public getCustomer(customerId)
  {
    console.log("customerId",customerId)
    if(customerId)
    {
      return this.contactsCollection.fetchEntityById(customerId).then(contact=>{
        console.log("contact",contact);
        return `${contact.name || ''}`;
      })
    }
    else
    {
      return '';
    }
  }

  prepareDynamicSurveyItems(surveyQuesionaire):Promise<void>{
    return surveyQuesionaire.map(surveyQuesionaire=>{
      let surveyQuesinaireWithDetail:any = surveyQuesionaire;
      surveyQuesinaireWithDetail["surveyDetail"] = new SurveyQuestionnaireDetail({});
      this.dynamicSurveyBinder.push(surveyQuesinaireWithDetail);
    })
  }


  private fetchAllQuery():Promise<Reference[]> {
    return this.referenceCollection.fetchAll()
    .then((response) => this.referenceCollection.getAllEntitiesFromResponse(response));
  }

  reformat(str: string) {
    if (str) {
      return str.split(';');
    }
    return [];
  }

  ionViewDidLoad():void {
  }

  onTapSave(){
    console.log("dynamicSurveyBinder",this.dynamicSurveyBinder);
    let surveyResponseCapturing = Array<SurveyQuestionnaireDetail>();
    this.dynamicSurveyBinder.map(surveyResponse=>{
      let surveyResponseDetail = new SurveyQuestionnaireDetail({});
      surveyResponseDetail.question = surveyResponse.question;
      surveyResponseDetail.surveyid = surveyResponse.surveyid;
      surveyResponseDetail.surveyquestionnaireid = surveyResponse.id;
      if(this.surveyDetails.surveytype=='External')
      {
        if(this.selectedCustomer) {
          // surveyResponseDetail.accountid = this.selectedCustomer.organizationSfId; // for temporary fix before you will update the salesforce filter
          surveyResponseDetail.customerId = this.customerName;
          surveyResponseDetail.remoteCustomerName = this.selectedCustomer.name;
        }
        if(!this.nonCustomer)
        {
          surveyResponseDetail.noncustomervalue = this.nonCustomer;
        }
      }
      if(surveyResponse.type.toLowerCase()=='text')
      {
       surveyResponseDetail.stringrealvalue = surveyResponse.surveyDetail.stringrealvalue;
      }
      else if(surveyResponse.type.toLowerCase()=='numeric')
      {
        if(surveyResponse.surveyDetail.numberrealvalue)
          surveyResponseDetail.numberrealvalue = parseInt(surveyResponse.surveyDetail.numberrealvalue);
      }
      else if(surveyResponse.type.toLowerCase()=='picklist' || surveyResponse.type.toLowerCase()=='multipicklist' || surveyResponse.type.toLowerCase()=='radio')
      {
       surveyResponseDetail.realvalue = surveyResponse.surveyDetail.realvalue;
      }

      if(typeof(surveyResponseDetail.stringrealvalue) == "undefined" && typeof(surveyResponseDetail.numberrealvalue) == "undefined" && typeof(surveyResponseDetail.realvalue) == "undefined")
      {
        console.log("answer is empty")
      }
      else
      {
        surveyResponseCapturing.push(surveyResponseDetail);

        this.surveyQuestionarieDetailsCollection.createEntity(surveyResponseDetail).then(res=>{
          console.log("surveyResponseDetail after save",res);
        })
      }
    })
      this.navCtrl.pop();
  }

  validateInput()
  {
    if(this.validInput())
    {
      this.onTapSave();
    }
    else{
      this.showValidateToast(this.msg);
    }
  }

  validInput()
  {
    let isValid = true;
    this.msg = '';
    if(this.surveyDetails.surveytype=='External')
    {
      if(!this.customerName && !this.nonCustomer)
      {
        this.msg+=` Please selct customer or provide non customer details \n`;
        isValid = false;
      }
      else if(this.customerName && this.nonCustomer)
      {
        this.msg = `\n Please select only one option i.e. either customer nor noncustomer\n`;
        isValid = false;
      }
    }
    this.dynamicSurveyBinder.map(surveyValues=>{
      if(surveyValues.isthisquestionrequire)
      {
        if(surveyValues.type=='Text')
        {
          if(!surveyValues.surveyDetail.stringrealvalue)
          {
            isValid = false;
            this.msg+=' Please Enter '+surveyValues.question+'\n' ;
          }
        }
       else if(surveyValues.type=='Numeric')
        {
          if(!surveyValues.surveyDetail.numberrealvalue)
          {
            isValid = false;
            this.msg+=' Please Enter '+surveyValues.question+'\n';
          }
        }
       else
        {
          if(!surveyValues.surveyDetail.realvalue)
          {
            isValid = false;
            this.msg+=' Please Select '+surveyValues.question+'\n';
          }
        }
      }
    })
   return isValid;
  }

  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton:true,
      cssClass:"toastClass",
      duration: 5000,
    });
    return toast.present();
  }

  onChangeContactTypeHandler(customer)
  {
    console.log("onChangeContactTypeHandler",customer);
    this.selectedCustomer =  customer;
    const fieldValues = {};
    fieldValues[SurveyQuestionnaireDetailScheme.fields.customerId.sfdc] = customer.contactSfId;
    fieldValues[SurveyQuestionnaireDetailScheme.fields.surveyid.sfdc] = this.surveyDetails.id;
    this.surveyQuestionarieDetailsCollection.fetchAllWhere(fieldValues).then(res=>{
      return this.surveyQuestionarieDetailsCollection.getAllEntitiesFromResponse(res);
    })
    .then(customerAvailableData=>{
      console.log("customerAvailableData",customerAvailableData);
      if(customerAvailableData.length>0)
      {
        this.showValidateToast(`${customer.name} already submitted the Survey. Please choose another customer`);
        this.customerName = '';
      }
    })
  }
}
