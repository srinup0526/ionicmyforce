import { Component, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import { NavController, AlertController, Events, ModalController, Modal } from 'ionic-angular';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
import {  AppointmentDetailsPage } from '../appointment-details/appointment-details';
import { ModalOptions } from '@ionic/core';
import { SyncManager } from './../../services/synchronisation/SyncManager';
import { SyncPopupComponent } from '../../components/sync-popup/sync-popup.component';
import SettingsManager from '../../services/db/SettingsManager';
import { Utils } from '../../utils/Utils';
import { HelpdeskPage } from '../helpdesk/helpdesk';
import { LogManager } from './../../services/common/LogManager';
import { EmailManager } from './../../services/common/EmailManager';
import { ContactsCollection } from "../../collections/ContactsCollection";
import { OrderListPage } from "../order-management/order-list/order-list";
import { AppointmentsCollection } from '../../collections/CallReportsCollection/AppointmentsCollection';
import { CoachingUsersCollection } from '../../collections/CoachingUsersCollection';

import { AppointmentsTodayCollection } from '../../collections/CallReportsCollection/AppointmentsTodayCollection';
import {EmailSender} from "../helpdesk/component/email-sender/email-sender";
import {SelectLanguagePopup} from "../../components/popups/SelectLanguagePopup";
import {DatabaseCleaner} from "../helpdesk/component/database-cleaner/database-cleaner";
import {DatabaseManager} from "../../services/db/DatabaseManager";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";
import { SforceDataContext } from '../../collections/SforceDataContext';

declare var window:any;
declare var cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loadProgress: number = 0;
  progressActivate:boolean = false;
  closestVisit1:any = {};
  closestVisit2:any = {};
  closestVisitsAvailable:boolean = false;
  callAverage:string = '';
  callAchievement:string = '';
  errLog:string='';
  syncDone:boolean = false;
  settings:any;

  public lastSyncDate: string;
  public activeUserFullName: string;

  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }

  constructor(private service: SmartstoreServiceProvider,
    private modal: NgbModal,
    public navCtrl: NavController,
    public alertController: AlertController,
    private modalController: ModalController,
    private syncManager: SyncManager,
    private emailManager: EmailManager,
    private events: Events,
    private logManager: LogManager,
    private appointmentsCollection:AppointmentsCollection,
    private coachingUsersCollection:CoachingUsersCollection,
    private appointmentsTodayCollection:AppointmentsTodayCollection,
    private selectLanguagePopup: SelectLanguagePopup,
    private emailSender: EmailSender,
    private databaseManager: DatabaseManager,
    private localizationManager: LocalizationManager,
    private alertCtrl: AlertController) {

    this.initHomePageDataThenDBLoaded();
  }

  ionViewDidLoad():void{}

  public onTapSynchronizeBtnHandler(): void {
    this.synchronise();
  }
  
  public onTapLanguageBtnHandler(): void {
    this.openLanguagePopup();
  }
  
  public onTapViewLogBtnHandler(): void {
    this.gotoViewLastLogPage();
  }
  
  public onTapSendLogBtnHandler(): void {
    this.sendLogData();
  }
  
  public onTapClearDatabaseBtnHandler(): void {
    this.clearDatabase();
  }

  public setSettings():Promise<void>{
    return SettingsManager.getSettings()
    .then(settings=>{
      console.log("settings",settings);
      this.settings = settings;
    })
  }

  public onTapShowBottomMenuBtnHandler(event): void {
    this.showBottomMenu();
    event.stopPropagation();
  }

  private listenSyncChanges() {
    this.events.subscribe('synchronization:sync-end', (result) => {
      this.onSyncEndHandler(result);
    });
  }

  public onSyncEndHandler(result): void {
    this.setLastSyncData();
    this.checkSyncResult(result);
  }

  private setLastSyncData() {
    this.setLastSyncDate();
    this.setActiveUser();
    this.setSettings();
    this.getClosestVisits();
    this.setCallAverage();
    //this.getCoachingData();

  }

  // public getCoachingData()
  // {
  //   this.coachingUsersCollection.modifyResponse(this.use)
  //   .then(res=>{
  //     console.log("res",res);
  //   })
  // }

  private synchronise(): Promise<Modal> {
    return this.showSyncModalPopup();
  }

  private async showSyncModalPopup(): Promise<Modal> {
    const modalOptions: ModalOptions<any> = {
      component: SyncPopupComponent,
      componentProps: {
        message: '',
        statusProgress: 0,
        syncManager: this.syncManager
      },
      backdropDismiss: false
    };

    const modal: Modal = await this.modalController.create(
      modalOptions.component,
      modalOptions.componentProps,
      {
        enableBackdropDismiss: modalOptions.backdropDismiss,
        cssClass: 'sync'
      }
      );

    await modal.present();

    return modal;
  }

  private setLastSyncDate(): Promise<void> {
    return SettingsManager.getLastSucceededSyncDateTime()
    .then((date) => {
      if (date) {
        this.lastSyncDate = `Last synchronized: ${Utils.formatDateTime(date)}`;
      }
    });
  }

  private setActiveUser(): Promise<void> {
    return SettingsManager.getActiveUserFullName()
    .then((fullName) => {
      console.log("fullName",fullName);
      this.activeUserFullName = fullName;
    });
  }

  private setCallAverage(): Promise<void>{
    return SforceDataContext.getActiveUser()
    .then(activeUser=>{
      console.log("activeUser",activeUser);
      if(activeUser.targetPercentWithAvg) {
        this.callAchievement = activeUser.targetPercentWithAvg.split(" ")[0];
        this.callAverage = activeUser.targetPercentWithAvg.split(" ")[1];
      } else {
        this.callAchievement = "0";
        this.callAverage = "0";
      }
      
    })
  }

  presentAlert(header:string,subHeader:string,message:string) {
    const alert = this.alertController.create({
      title: header,
      subTitle: subHeader,
      message: message,
      buttons: ['OK']
    });

    alert.present();
  }


  logout(){
    let  OAuth = cordova.require("com.salesforce.plugin.oauth")
    OAuth.logout();

  }

  getClosestVisits()
  {
    this.appointmentsCollection.fetchClosest().then(data=>{
      console.log("response",data);
      if(data.records.length>0)
      {
        this.closestVisitsAvailable = true;
        this.closestVisit1 = data.records[0];
        this.closestVisit2 = data.records.length>=2?data.records[1]:{};
      }
    });
  }
  appointmentDetails(row:any)
  {
    console.log("row",row);
    this.navCtrl.push(AppointmentDetailsPage,{row:row});
  }

  private checkSyncResult(result) {
    if (result.isError && result.showLog) {
      this.showLogData();
    }

  }

  private showLogData(): void {
    this.navCtrl.push(HelpdeskPage);
  }

  private showBottomMenu(): void {
    this.events.publish('bottomMenu:toggle');
  }
  
  private initHomePageDataThenDBLoaded() {
    this.events.subscribe('common:dataBaseReady', () => {
      this.listenSyncChanges();
      this.setLastSyncData();
    });
  }
  
  private openLanguagePopup(): void {
    this.selectLanguagePopup.showPopup();
  }
  
  private sendLogData(): void {
    this.emailSender.sendLogData();
  }
  
  private clearDatabase(): void {
    const databaseCleaner: DatabaseCleaner = new DatabaseCleaner(this.alertCtrl, this.databaseManager, this.localizationManager);
    
    databaseCleaner.clear();
  }
  
  private gotoViewLastLogPage(): void {
    this.navCtrl.push(HelpdeskPage, { activeTab: HelpdeskPage.ACTIVE_TABS.LAST_LOG });
  }
}
