import {Page} from "./page";

export class PagedData {
  data = new Array<any>();
  page = new Page();
}
