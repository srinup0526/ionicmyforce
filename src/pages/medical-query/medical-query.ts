import {Component, ElementRef, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertOptions, Select } from 'ionic-angular';
import { MedicalQueryDetailsPage } from '../medical-query-details/medical-query-details';
import { AddMedicalQueryPage } from '../add-medical-query/add-medical-query';
import { ClosedQueries } from "../../collections/MedicalQueriesCollection/ClosedQueries";
import { DraftQueries } from "../../collections/MedicalQueriesCollection/DraftQueries";
import { MedicalQueriesCollection } from "../../collections/MedicalQueriesCollection/MedicalQueriesCollection";
import { QueriesSubmittedToMedical } from "../../collections/MedicalQueriesCollection/QueriesSubmittedToMedical";
import { QueriesSubmittedToRep } from "../../collections/MedicalQueriesCollection/QueriesSubmittedToRep";
import { TableHeaderItemOption } from './../../components/table/table-header/TableHeaderItemOption';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import {FilterPanelComponent} from "../../components/filter/filter-panel/filter-panel";
import { MedicalQueryScheme } from '../../models/scheme/MedicalQueryScheme';
import {Query} from "../../services/common/Query";
import { MedicalCommentsCollection} from '../../collections/MedicalCommentsCollection';
import { MedicalQuery } from '../../models/MedicalQuery';
import { MedicalComment } from '../../models/MedicalComment';
import { Utils } from '../../utils/Utils';

/**
* Generated class for the MedicalQueryPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-medical-query',
  templateUrl: 'medical-query.html',
})
export class MedicalQueryPage {

  //
  public isSearchbarOpened = false;
  public contactsCount:any = 0;

  medicalString:string = "all";

  references: Array<any> = [];

  public tableOptions: LazyTableOption;
  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: MedicalQueriesCollection;
  public filterOptions: AlertOptions;
  public settings: SettingsImpl;

  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('filterType') filterTypeComponent: Select;
  @ViewChild('mainContent') mainContent: ElementRef;

  showNewTable: boolean;
  medquery:any;
  constructor(public navParams: NavParams,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public closedQueries: ClosedQueries,
    public draftQueries: DraftQueries,
    public medicalQueries: MedicalQueriesCollection,
    public medicalCommentCollection:MedicalCommentsCollection,
    public queriesSubmittedToMedical: QueriesSubmittedToMedical,
    public queriesSubmittedtoRep: QueriesSubmittedToRep) {

    this.settings = Settings.getInstance();


    const headerItems = [
    {
      title: 'common.names.CreatedDateTime',
      fields: [MedicalQueryScheme.fields.createdDateTime],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {},
      modelFunction:'getCreatedDateTimeInFormat'
    },
    {
      title: 'common.names.Status',
      fields: [MedicalQueryScheme.fields.status],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {}
    },
    {
      title: 'common.names.PharmaProduct',
      fields: [MedicalQueryScheme.fields.pharmaProductName],
      isSortable: true,
      isAsc: true,
      isActive: false,
      //modelFunction:'getProductName'
    },
    {
      title: 'common.names.Customer',
      fields: [MedicalQueryScheme.fields.customerName],
      isSortable: true,
      isAsc: true,
      isActive: false,
     // modelFunction:'getCustomerName'
    },
    {
      title: 'common.names.NonContactName',
      fields: [MedicalQueryScheme.fields.noncustomer],
      isSortable: true,
      isAsc: true,
      isActive: false
    },
    {
      title: 'common.names.MylanMedicalContact',
      fields: [MedicalQueryScheme.fields.medicalContactName],
      isSortable: true,
      isAsc: true,
      isActive: false,
     // modelFunction:'getMedicalContactName'
    }
    ];

    this.tableOptions = {
      headerItems: headerItems,
      rowHandler: (event, reference) => {
        this.itemTapped(reference);
      },
      batchSize: 100
    };

    this.searchString = '';

    this.collection = this.medicalQueries;
    this.medicalQueries
    .fetchAll()
    .then((records) => {
      return this.medicalQueries.getAllEntitiesFromResponse(records);
    })
    .then((medQueries) => {
      console.log('------ Users.length ------');
      console.log(medQueries.length);
      console.log("medQueries",medQueries);
    });
    console.log("medicalQueries", this.collection);
    this.filterOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };
  }

  public onChangeQueryHandler(query: Query): void {
    this.setReferenceCount();
  }

  public onChangeContactTypeHandler(type) {
    if(type == 'all') {
      this.collection = this.medicalQueries;
    }else if(type == 'smc'){
      this.collection = this.queriesSubmittedToMedical;
    }else if(type == 'smr'){
      this.collection = this.queriesSubmittedtoRep;
    }else if(type == 'cl'){
      this.collection = this.closedQueries;
    }else if(type == 'dr'){
      this.collection = this.draftQueries;
    }

    this.medicalString = type;

    this.filterTypeComponent.close();

    this.lazyTable.setCollection(this.collection);
    this.lazyTable.reloadTable();
  }

  public onSwipeHandler(event){
    switch (event.offsetDirection) {
      case 2: this.filterPanel.hidePanel(); break;
      case 4: this.filterPanel.showPanel(); break;
    }
  }

  public onFilterChangeHandler(filterData): void {
    this.lazyTable.reloadTableByFilterData(filterData);
  }

  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;

    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }




  public ionViewDidLoad() : void { 
  }

  public ionViewDidEnter(): void {
  }

  public ionViewWillEnter(): void {
    this.lazyTable.reloadTable();
  }


  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable();


  }
  private setReferenceCount(): number {
    const countQuery = this.collection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);

    return this.collection.runCountQuery(query.query)
    .then((count) => {
      return this.contactsCount = count || 0;
    })
  }

  itemTapped(row: any) {
    console.log("row",row)
    this.navCtrl.push(MedicalQueryDetailsPage, { "row": row });

  }

  openModal() {
    this.navCtrl.push(AddMedicalQueryPage,{"parentPage": this});
  }

  gotoAddMedicalQueryPage(){
    this.navCtrl.push(AddMedicalQueryPage);
  }
}
