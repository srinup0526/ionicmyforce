import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedicalQueryPage } from './medical-query';

@NgModule({
  declarations: [
    MedicalQueryPage,
  ],
  imports: [
    IonicPageModule.forChild(MedicalQueryPage),
  ],
})
export class MedicalQueryPageModule {}
