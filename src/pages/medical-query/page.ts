export class Page {
    size: number = 100;
    totalElements: number = 0;
    totalPages: number = 0;
    pageNumber: number = 0;
  }