import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrganizationdetailsPage } from './organizationdetails';

@NgModule({
  declarations: [
    OrganizationdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(OrganizationdetailsPage),
  ],
})
export class OrganizationdetailsPageModule {}
