import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
//import { ContactsServiceProvider } from '../../providers/contacts-service/contacts-service';
import { AppointmentPage } from '../appointment/appointment';
import { CallReportPage } from '../call-report/call-report';
import { AppointmentDetailsPage } from '../appointment-details/appointment-details';
import { ContactdetailsPage } from '../contactdetails/contactdetails';
import { CallReportDetailsPage } from '../call-report-details/call-report-details';
import { OrganizationsCollection } from  '../../collections/OrganizationsCollection';
import { ReferenceCollection } from '../../collections/references/ReferenceCollection';
import { CallReportsCollection } from '../../collections/CallReportsCollection/CallReportsCollection';
import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import {OrganizationEditCardPage} from "../organization-edit-card/organization-edit-card";
import {Settings} from "../../services/db/Settings";
import {Organization} from "../../models/Organization";



/**
* Generated class for the OrganizationdetailsPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-organizationdetails',
  templateUrl: 'organizationdetails.html',
})
export class OrganizationdetailsPage {
  account: any;
  Reference:any;
  row:any;
  infieldActivitiesOrg:any;
  references:any;
  
  isEditBtnEnabled: boolean;
  
  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public referenceCollection:ReferenceCollection,
    public organizationsCollection:OrganizationsCollection,
    public callReportsCollection:CallReportsCollection) {
    this.row = this.navParams.data['row'];
    this.organizationsCollection.fetchEntityById(this.navParams.data['organizationSfId'])
    .then(orgData=>{
      console.log("orgData",orgData);
      this.account = orgData;
    })

    this.referenceCollection.getReferenceByOrganization(this.navParams.data['organizationSfId'])
    .then(references=>{
      console.log("references",references);
      this.references = references;
    })

   const whereFields = {};
    whereFields[CallReportScheme.fields.organizationSfId.sfdc] = this.navParams.data['organizationSfId'];
    this.callReportsCollection.fetchAllWhere(whereFields)
    .then(infieldActivities=>{
      console.log("infieldActivities",infieldActivities);
      this.infieldActivitiesOrg = infieldActivities.records;
    })

  }

  ionViewDidLoad():void {}
  
  ionViewWillEnter(): void {
    this.initButtons();
  }
  
  public onTapEditBtnHandler() {
    this.gotoEditPage();
  }
  
  itemTappedCallReport(row: any) {

    console.log("customerRow",row);

    this.navCtrl.push(CallReportPage,{ "row": row });
  }

  itemTappedAppointment(row: any) {
    console.log("customerRow",row);
    this.navCtrl.push(AppointmentPage,{ "row": row });
  }
  itemTappedType(row:any){
    console.log("customerRow",row);

    if(row.type.toLowerCase()=='appointment')
    {
      this.navCtrl.push(AppointmentDetailsPage,{"row":row});
    }
    else
    {
      this.navCtrl.push(CallReportDetailsPage,{"row":row});
    }
  }

  gotoContactDetailsPage(contactSfId)
  {
    this.navCtrl.push(ContactdetailsPage,{"contactSfId":contactSfId});
  }
  
  private gotoEditPage() {
    this.navCtrl.push(OrganizationEditCardPage, { "row": new Organization(this.account) });
  }
  
  private initButtons(): void {
    let settings = Settings.getInstance();
    
    this.isEditBtnEnabled = settings.isDataChangeRequestsModuleEnabled();
  }
}
