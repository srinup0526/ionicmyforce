import { Component, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { ProductScheme } from './../../models/scheme/ProductScheme';
import { ProductsCollection } from './../../collections/ProductsCollection';
import {ContactdetailsPage} from "../contactdetails/contactdetails";
import {Contact} from "../../models/Contact";


@IonicPage()
@Component({
  selector: 'page-product-list-select',
  templateUrl: 'product-list-select.html',
})
export class ProductListSelectPage {
  static readonly CSS_ACTIVE_CLASS = 'active';
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: ProductsCollection;
  private callback: any;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private productCollection: ProductsCollection) {
    
    this.setTableOptions();
    this.setTableCollection();
    
    this.searchString = '';
    this.callback = navParams.data['callback'];
    
    ProductScheme.activeRows = navParams.data['ids'] || [];
  }
  
  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
    
    this.lazyTable.reloadTable()
      .then(() => {
        this.updateSelectedElement(ProductScheme.activeRows);
      })
  }
  
  public onChangeQueryHandler(searchBar: any) {
    this.updateSelectedElement(ProductScheme.activeRows);
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onTapRowHandler(event, record): void {
    const selectedIndexOnActiveRows = ProductScheme.activeRows.indexOf(record.id);
    
    if(~selectedIndexOnActiveRows) {
      ProductScheme.activeRows.splice(selectedIndexOnActiveRows, 1);
    } else {
      ProductScheme.activeRows.push(record.id);
    }
    
    this.updateSelectedElement(ProductScheme.activeRows);
    
    Promise.resolve(this.runCallback(ProductScheme.activeRows))
  }
  
  private setTableOptions() {
    this.tableOptions = {
      headerItems: [
        {
          title: '',
          fields: [ProductScheme.fields.id],
          modelFunction: 'getIdForTableColumn'
        },
        {
          title: 'common.names.Name',
          fields: [ProductScheme.fields.name],
          isSortable: false,
          isAsc: true,
          isActive: false
        },
        // {
        //   title: 'common.names.Contact',
        //   fields: [ProductScheme.fields.atcClass],
        //   isSortable: false,
        //   isAsc: true,
        //   modelFunction: 'fullName',
        //   isActive: false,
        //   handler: (event, contact) => {
        //     this.gotoContact(contact);
            
        //     event.stopPropagation();
        //   }
        // },
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
  }
  
  private setTableCollection() {
    return this.collection = this.productCollection;
  }
  
  private runCallback(record) {
    return this.callback(record);
  }
  
  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }
  
  private updateSelectedElement(activeIds) {
    const elements = document.querySelectorAll('.product-table table-row .cell:first-of-type .contact-id');
    
    [].slice.call(elements)
      .filter((item) => {
        this.resetActiveElement(item.parentElement);
        
        return ~activeIds.indexOf(item.innerText);
      })
      .map((item) => this.setActiveElement(item.parentElement));
  }
  
  private resetActiveElement(element): void {
    if (element && element.classList.contains(ProductListSelectPage.CSS_ACTIVE_CLASS)) {
      element.classList.remove(ProductListSelectPage.CSS_ACTIVE_CLASS);
    }
  }
  
  private setActiveElement(element): void {
    if (element) {
      element.classList.add(ProductListSelectPage.CSS_ACTIVE_CLASS);
    }
  }
 
}
