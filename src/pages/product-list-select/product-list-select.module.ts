import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductListSelectPage } from './product-list-select';

@NgModule({
  declarations: [
    ProductListSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductListSelectPage),
  ],
})
export class ProductListSelectPageModule {}
