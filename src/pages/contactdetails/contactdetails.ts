import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,} from 'ionic-angular';
import { AppointmentPage } from '../appointment/appointment';
import { CallReportPage } from '../call-report/call-report';
import { AppointmentDetailsPage } from '../appointment-details/appointment-details';
import { CallReportDetailsPage } from '../call-report-details/call-report-details';
import { OrganizationdetailsPage } from '../organizationdetails/organizationdetails';
import { ContactsCollection } from '../../collections/ContactsCollection';
import { ReferenceCollection } from '../../collections/references/ReferenceCollection';
import { CallReportsCollection } from '../../collections/CallReportsCollection/CallReportsCollection';
import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import {Settings} from "../../services/db/Settings";
import {ContactEditCardPage} from "../contact-edit-card/contact-edit-card";
import {Contact} from "../../models/Contact";
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import { AddRedFlagPage } from '../../pages/add-red-flag/add-red-flag';





//import { ContactsServiceProvider } from '../../providers/contacts-service/contacts-service';
/**
* Generated class for the ContactdetailsPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-contactdetails',
  templateUrl: 'contactdetails.html',
})
export class ContactdetailsPage {
  contact: any;
  Reference:any;
  references:any;
  row:any;
  infieldActivities:any=[];

  isEditBtnEnabled: boolean;

  redFlagRequired:boolean = false;

  constructor(
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public contactsCollection:ContactsCollection,
    public referenceCollection:ReferenceCollection,
    public callReportsCollection:CallReportsCollection) {
    this.contactsCollection.fetchEntityById(this.navParams.data['contactSfId'])
    .then(contact=>{
      console.log("contact",contact);
      this.contact = contact;
    })
    this.referenceCollection.getReferenceByContact(this.navParams.data['contactSfId'])
    .then(references=>{
      console.log("references",references);
      this.references = references;
    })
    const whereFields = {};
    whereFields[CallReportScheme.fields.contactSfId.sfdc] = this.navParams.data['contactSfId'];
    this.callReportsCollection.fetchAllWhere(whereFields)
    .then(infieldActivities=>{
      console.log("infieldActivities",infieldActivities);
      this.infieldActivities = infieldActivities.records;
    })
   ConfigurationManager.getConfig()
   .then(config=>{
     this.redFlagRequired = config.sampleManagementSettings.isItRedFlagRequired;

   })
  }

  ionViewDidLoad():void {}

  ionViewWillEnter(): void {
    this.initButtons();
  }

  itemTappedCallReport(row: any) {
    console.log("customerRow",row);
    this.navCtrl.push(CallReportPage,{ "row": row });
  }

  public onTapEditBtnHandler() {
    this.gotoEditPage();
  }

  itemTappedAppointment(row: any) {
    console.log("customerRow",row);
    this.navCtrl.push(AppointmentPage,{ "row": row });
  }
  itemTappedType(row:any){
    console.log("customerRow",row);
    if(row.type==CallReportScheme.TYPE_APPOINTMENT)
    {
      this.navCtrl.push(AppointmentDetailsPage,{row:row});
    }
    else
    {
      this.navCtrl.push(CallReportDetailsPage,{row:row});
    }
  }

  navigateToOrganisationPage(reference:any){
    console.log("reference",reference);
    let row = {};
    row["Id"] = reference.organizationSfId;
    console.log("row",row)
    this.navCtrl.push(OrganizationdetailsPage, { "row": row });
  }

  tappedRedFlag(reference:any)
  {
    console.log("Reference",reference);
    this.navCtrl.push(AddRedFlagPage,{'row':reference});
  }

  private gotoEditPage() {
    this.navCtrl.push(ContactEditCardPage, { "row": new Contact(this.contact) });
  }

  private initButtons(): void {
    let settings = Settings.getInstance();

    this.isEditBtnEnabled = settings.isDataChangeRequestsModuleEnabled();
  }
}
