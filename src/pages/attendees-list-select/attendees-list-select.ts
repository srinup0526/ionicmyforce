import { Component, ViewChild, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { ContactScheme } from './../../models/scheme/ContactScheme';
import {ContactsCollection} from "../../collections/ContactsCollection";
import {ContactdetailsPage} from "../contactdetails/contactdetails";
import {Contact} from "../../models/Contact";


@IonicPage()
@Component({
  selector: 'page-attendees-list-select',
  templateUrl: 'attendees-list-select.html',
})
export class AttendeesListSelectPage {
  static readonly CSS_ACTIVE_CLASS = 'active';
  public searchString: string;
  public search: string;
  public tableOptions: LazyTableOption;
  public collection: ContactsCollection;
  private callback: any;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private contactsCollection: ContactsCollection) {
    
    this.setTableOptions();
    this.setTableCollection();
    
    this.searchString = '';
    this.callback = navParams.data['callback'];
    
    ContactScheme.activeRows = navParams.data['ids'] || [];
  }
  
  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
    
    this.lazyTable.reloadTable()
      .then(() => {
        this.updateSelectedElement(ContactScheme.activeRows);
      })
  }
  
  public onChangeQueryHandler(searchBar: any) {
    this.updateSelectedElement(ContactScheme.activeRows);
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onTapRowHandler(event, record): void {
    const selectedIndexOnActiveRows = ContactScheme.activeRows.indexOf(record.id);
    
    if(~selectedIndexOnActiveRows) {
      ContactScheme.activeRows.splice(selectedIndexOnActiveRows, 1);
    } else {
      ContactScheme.activeRows.push(record.id);
    }
    
    this.updateSelectedElement(ContactScheme.activeRows);
    
    Promise.resolve(this.runCallback(ContactScheme.activeRows))
  }
  
  private setTableOptions() {
    this.tableOptions = {
      headerItems: [
        {
          title: '',
          fields: [ContactScheme.fields.id],
          modelFunction: 'getIdsForTableColumn'
        },
        {
          title: 'common.names.BUSpecialty',
          fields: [ContactScheme.fields.buSpecialty],
          isSortable: false,
          isAsc: true,
          isActive: false
        },
        {
          title: 'common.names.Contact',
          fields: [ContactScheme.fields.lastName, ContactScheme.fields.firstName],
          isSortable: false,
          isAsc: true,
          modelFunction: 'fullName',
          isActive: false,
          handler: (event, contact) => {
            this.gotoContact(contact);
            
            event.stopPropagation();
          }
        },
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
  }
  
  private setTableCollection() {
    return this.collection = this.contactsCollection;
  }
  
  private runCallback(record) {
    return this.callback(record);
  }
  
  private goBack(): Promise<any> {
    return this.navCtrl.pop();
  }
  
  private updateSelectedElement(activeIds) {
    const elements = document.querySelectorAll('.attendee-table table-row .cell:first-of-type .contact-id');
    
    [].slice.call(elements)
      .filter((item) => {
        this.resetActiveElement(item.parentElement);
        
        return ~activeIds.indexOf(item.innerText);
      })
      .map((item) => this.setActiveElement(item.parentElement));
  }
  
  private resetActiveElement(element): void {
    if (element && element.classList.contains(AttendeesListSelectPage.CSS_ACTIVE_CLASS)) {
      element.classList.remove(AttendeesListSelectPage.CSS_ACTIVE_CLASS);
    }
  }
  
  private setActiveElement(element): void {
    if (element) {
      element.classList.add(AttendeesListSelectPage.CSS_ACTIVE_CLASS);
    }
  }
  
  private gotoContact(contact: Contact): Promise<any> {
    return this.navCtrl.push(ContactdetailsPage, { "row": contact});
  }
}
