import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AttendeesListSelectPage } from './attendees-list-select';

@NgModule({
  declarations: [
    AttendeesListSelectPage,
  ],
  imports: [
    IonicPageModule.forChild(AttendeesListSelectPage),
  ],
})
export class AttendeesListSelectPageModule {}
