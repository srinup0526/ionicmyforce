import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndiaConvertCallReportPage } from './india-convert-call-report';

@NgModule({
  declarations: [
    IndiaConvertCallReportPage,
  ],
  imports: [
    IonicPageModule.forChild(IndiaConvertCallReportPage),
  ],
})
export class CallReportPageModule {}
