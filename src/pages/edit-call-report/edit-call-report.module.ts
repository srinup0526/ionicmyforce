import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditCallReportPage } from './edit-call-report';

@NgModule({
  declarations: [
    EditCallReportPage,
  ],
  imports: [
    IonicPageModule.forChild(EditCallReportPage),
  ],
})
export class EditCallReportPageModule {}
