import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController,PopoverController, NavParams, LoadingController, AlertOptions, Select } from 'ionic-angular';
import { TotdetailsPage } from '../totdetails/totdetails';
import { AddtotPage } from '../addtot/addtot';
import { TableHeaderItemOption } from './../../components/table/table-header/TableHeaderItemOption';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { TotScheme } from './../../models/scheme/TotScheme';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import { TOTPickListDatasource } from "../../services/db/picklist-managers/datasource/TOTPickListDatasource";
import { FilterPanelComponent } from "../../components/filter/filter-panel/filter-panel";
import { Query } from "../../services/common/Query";
import { Settings, SettingsImpl } from '../../services/db/Settings';
import { TotsCollection } from '../../collections/tots-collection/tots-collection';
import { TotsOpenCollection } from '../../collections/tots-collection/tots-open-collection';
import { TotsClosedCollection } from '../../collections/tots-collection/tots-closed-collection';
import { TotsSubmitCollection } from '../../collections/tots-collection/tots-submit-collection';
import { Utils } from '../../utils/Utils';
import { Tot } from "../../models/tot";
//import moment from 'moment';
/**
* Generated class for the TotPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-tot',
  templateUrl: 'tot.html',
})
export class TotPage {
  public totsCount:any = 0;
  public tableOptions: LazyTableOption;
  public customCondition: string;
  public filterWhereCondition: any;
  public filterPanelFields: any;
  public searchString: string;
  public search: string;
  public collection: TotsCollection;
  public filterOptions: AlertOptions;
  public settings: SettingsImpl;
  public typeFilterOptions:string = 'all';

  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;
  @ViewChild(FilterPanelComponent) filterPanel: FilterPanelComponent;
  @ViewChild('filterType') filterTypeComponent: Select;
  @ViewChild('mainContent') mainContent: ElementRef;
  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public popoverController:PopoverController,
    public navParams: NavParams,
    public totCollection:TotsCollection,
    public totOpenCollection:TotsOpenCollection,
    public totClosedCollection:TotsClosedCollection,
    public totSubmitCollection:TotsSubmitCollection,) {
    this.settings = Settings.getInstance();
    const headerItems = [
    {
      title: 'common.names.MedRep',
      fields: [TotScheme.fields.userFirstName, TotScheme.fields.userLastName],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {},
      modelFunction: 'userFullName',
    },
    {
      title: 'common.names.Type',
      fields: [TotScheme.fields.type],
      isSortable: true,
      isAsc: true,
      isActive: false,
      picklistValues: {}
    },
    {
      title: 'common.names.AllDay',
      fields: [TotScheme.fields.allDay],
      isSortable: true,
      isAsc: true,
      isActive: false,
      modelFunction:'allDayTrueOrNot'
    },
    {
      title: 'common.names.StartDate',
      fields: [TotScheme.fields.startDate],
      isSortable: true,
      isAsc: true,
      isActive: false,
      modelFunction:'getStartDateFormat'
    },
    {
      title: 'common.names.EndDate',
      fields: [TotScheme.fields.endDate],
      isSortable: true,
      isAsc: true,
      isActive: false,
      modelFunction:'getEndDateFormat'
    }
    ];

    this.tableOptions = {
      headerItems: headerItems,
      rowHandler: (event, reference) => {
        this.itemTapped(reference);
      },
      batchSize: 100
    };

    this.searchString = '';

    this.collection = this.totCollection;

    this.filterOptions = {
      cssClass: 'popup alert list without-header',
      buttons: []
    };

  }

  public onFilterChangeHandler(filterData): void {
    this.lazyTable.reloadTableByFilterData(filterData);
  }

  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;

    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }

  public ionViewDidLoad() : void {}

  public ionViewDidEnter(): void {}

  public ionViewWillEnter(): void {
    // this.lazyTable.isTableInited
    this.lazyTable.reloadTable();
  }

  public ngAfterContentInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);

    this.lazyTable.reloadTable();
  }

  private setReferenceCount(): number {
    const countQuery = this.collection.getCountQuery();
    const query = this.lazyTable.getTableQueryWithConditions(countQuery);

    return this.collection.runCountQuery(query.query)
    .then((count) => {
      return this.totsCount = count || 0;
    })
  }


  public onChangeStatusTypeHandler(type) {
    console.log("type",type);
    console.log("this.totOpenCollection",this.totOpenCollection);
    console.log("this.totClosedCollection",this.totClosedCollection);
    console.log("this.totSubmitCollection",this.totSubmitCollection);
    console.log("this.totCollection",this.totCollection);
    if(type == 'open') {
      this.collection = this.totOpenCollection //.filter(data=> { console.log("data",data); return data.status='open';});
    }
    else if(type == 'closed') {
      this.collection = this.totClosedCollection;
    }
    else if(type == 'submit') {
      this.collection = this.totSubmitCollection;
    }else{
      this.collection = this.totCollection;
    }

    this.typeFilterOptions = type;

    this.filterTypeComponent.close();

    this.lazyTable.setCollection(this.collection);
    this.lazyTable.reloadTable();
  }

  public onChangeQueryHandler(query: Query): void {
    this.setReferenceCount();
  }

  openModal() {
    this.navCtrl.push(AddtotPage,{"parentPage": this});
  }
  itemTapped(row: string) {
    console.log("totdetailspagerow",row)
    this.navCtrl.push(TotdetailsPage, { "row": row });

  }

}
