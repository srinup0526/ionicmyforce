import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TotPage } from './tot';

@NgModule({
  declarations: [
    TotPage,
  ],
  imports: [
    IonicPageModule.forChild(TotPage),
  ],
})
export class TotPageModule {}
