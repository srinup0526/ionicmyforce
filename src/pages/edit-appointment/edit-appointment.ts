import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
// import { CallReportPage } from '../call-report/call-report';
// import { ConfirmationDialogService } from '../calendar/confirmation-dialog/confirmation-dialog.service';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the EditAppointmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var cordova:any;

@IonicPage()
@Component({
  selector: 'page-edit-appointment',
  templateUrl: 'edit-appointment.html',
})
export class EditAppointmentPage {

  contact: any;
  Reference:any;
  Organisation:any;
  row:any;
  coachingVisit:any;
  dateTimePlanned:any;
  Customer_Name__c:any;
  Org_Name__c:any;
  ActivitesName:'Activites';
  JointVisitParticipants:any;
  CoachingVisitUser:any;
  saveAppointmentData: {};
  contactInformation:any;
  CoachingVisitUsers:any;
  loggedInUserDetails:any;
  Appointments: Array<{ Name: any, Date_Time_Planned__c: string, Contact1__r: any, User__r:any, Organisation__r:any, Type__c:any }>;
  appointmentData: {};
  isDataValid:boolean = true;
  Contact1__c: string = "";
  Date_Time_Planned__c:Date = new Date();
  Organisation__c: string = "";
  User__c : string = "";
  Coaching_Visit__c: string = "";
  Coaching_Visit_User__c: string = "";
  Duration__c:string = "";
  Joint_Visit_Participants__c: string = "";
  Call_Objective__c:string = "";
  Created_Offline__c:boolean = true;
  Type__c:string = "Appointment";
  RecordTypeId:string = "01230000000IgPO";
  Call_Report_Status__c:string = "";
  message = "";

  public CallReportName='callReport'

  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }
  public Callreport='callreport'
  constructor(public navCtrl: NavController, 
    public viewCtrl: ViewController,public navParams: NavParams,public service:SmartstoreServiceProvider,public storage: Storage, public toastCtrl: ToastController) {
    this.row = this.navParams.data["row"];
    this.coachingVisit=""
    console.log("this.row",this.row)

  }
  ionViewDidLoad() {
    this.storage.get("currentUserDetails").then(data => { this.loggedInUserDetails = data; console.log("UserData",this.loggedInUserDetails); });
    this.row = this.navParams.data["row"];
    this.loadAllUsers();
    this.dateTimePlanned = new Date();
    this.getContactReference(this.row.Customer__c);
    this.getContact(this.row.Customer__c);
    this.getOrganization(this.row.Organisation__c);
  }
  getContact(id: string) {
    this.service.getContact(id)
    .then(results => {

      this.contact = results.records[0];
    })
  }
  loadAllUsers() {
    return this.service.loadAllUsers()
      .then(results => {
        console.log('Users',results);
        this.CoachingVisitUsers = results.records;
      })
  }
  getContactReference(id: string) {
    this.service.getContactReference(id)
    .then(results => {

      this.Reference = results.records[0];
      console.log("refContactdetails",this.Reference);
    })
  }
  getOrganization(id: string) {
    this.service.getOrganization(id)
    .then(results => {
      this.Organisation = results.records[0];
      console.log("refContactdetails",this.Reference);
    })
  }
  change(coachingVisit: string)
  {
    console.log("coachingVisit",coachingVisit);
  }
  validateInputData()
  {
    console.log("message",this.message);
    console.log("this.validateInput",this.validateInput());
    if(this.validateInput())
    {
      this.saveAppointment();
    }
    else
    {
      const toast = this.toastCtrl.create({
        message: this.message,
        showCloseButton:true,
        cssClass:"toastClass",
        duration:3000,
      });
      toast.present();
    }
    
  }

  validateInput()
  {
    var isValid = true;
    this.message = "";
    console.log("this.Date_Time_Planned__c",this.Date_Time_Planned__c);
    if(this.Coaching_Visit__c!='' && this.Coaching_Visit__c!=null && (this.Coaching_Visit_User__c=='' || this.Coaching_Visit_User__c==null))
    {
      isValid = false;
      this.message+="Coaching Visit User Mandatory";
    }
    if(this.Date_Time_Planned__c==null)
    {
      isValid = false;
      this.message+=" Date Time Planned Can not be empty ";
    }
    if(new Date(this.Date_Time_Planned__c) <= new Date())
    {
      isValid = false;
      this.message+=" Date Time Planned Value cannot be less than the current Date ";
    }
    return isValid;  
  }
  saveAppointment(){
    let appointment={}

    appointment["Created_Offline__c"] = this.Created_Offline__c;
    appointment["Date_Time_Planned__c"] = this.Date_Time_Planned__c;
    appointment["Organisation__c"] = this.row?this.row.Organisation__c:'';
    appointment["Contact1__c"] = this.row?this.row.Customer__c:'';
    appointment["Contact1__r"] = this.contact;
    appointment["Organisation__r"] = this.Organisation;
    appointment["User__c"] = this.loggedInUserDetails.Id;
    appointment["User__r"] = this.loggedInUserDetails;
    appointment["Duration__c"] = this.Duration__c?this.Duration__c:"30";
    appointment["Type__c"] = this.Type__c;
    appointment["RecordTypeId"] = this.RecordTypeId
    appointment["Coaching_Visit__c"] = this.Coaching_Visit__c
    appointment["Coaching_Visit_User__c"] = this.Coaching_Visit_User__c
    appointment["Joint_Visit_Participants__c"] = this.Joint_Visit_Participants__c
    appointment["Call_Objective__c"] = this.Call_Objective__c
    appointment['attributes'] = {type: "callReport"}
    appointment["Call_Report_Status__c"] = this.Call_Report_Status__c;
    appointment["__locally_created__"] = true;
    appointment["__locally_updated__"] = false;
    appointment["__locally_deleted__"] = false;
    appointment["__local__"] = true;
    console.log("appointment",appointment);
    let success = (data) => {
      console.log("Data Upserted successfully and created data is",data);
      const toast = this.toastCtrl.create({
        message: "Appointment Saved Successfully!",
        showCloseButton:true,
        cssClass:"toastClass",
        duration:3000,
      });
      toast.present();
      this.navCtrl.pop();
    }
    let failure = (error) => {
      console.log("Data Upsertion having an issue",error);
    }
    this.smartStore().upsertSoupEntries("callReport", [appointment], success, failure);
  }

}
