import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DynamicAgendaPage } from './dynamic-agenda';

@NgModule({
  declarations: [
    DynamicAgendaPage,
  ],
  imports: [
    IonicPageModule.forChild(DynamicAgendaPage),
  ],
})
export class DynamicAgendaPageModule {}
