import {Component} from '@angular/core';
import {AlertController, AlertOptions, Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {ScenarioDetailsPage} from "../scenario-details/scenario-details";
import {ScenariosCollection} from "../../collections/ScenariosCollection";
import {ScenarioDetailsService} from "../scenario-details/scenario-details.service";
import {Scenario} from "../../models/Scenario";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";


@IonicPage()
@Component({
  selector: 'page-dynamic-agenda',
  templateUrl: 'dynamic-agenda.html',
})
export class DynamicAgendaPage {
  reloadEventSubscriber: any;
  scenarios: Scenario[];

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private scenariosCollection: ScenariosCollection,
                private scenarioDetailsService: ScenarioDetailsService,
                public events: Events,
                public alertCtrl: AlertController,
                private localizationManager: LocalizationManager) {
      this.scenarios = [];
  }

  ionViewDidLoad() {
    this.subscribeReloadEvent();
    this.getScenarios();
  }

  ionViewWillUnload() {
    this.reloadEventSubscriber && this.reloadEventSubscriber.unsubscribe();
  }


  onClickCreate(event) {
    event.stopPropagation();
    this.openScenarioDetailsPage();
  }


  onClickEdit(event, scenario: Scenario) {
    event.stopPropagation();
    this.openScenarioDetailsPage(scenario);
  }


  onClickDelete(event, scenario: Scenario) {
    event.stopPropagation();
    this.deleteScenario(scenario);
  }


  onClickOpen(event, scenario: Scenario) {
    event.stopPropagation();
    this.openScenario(scenario);
  }


  private getScenarios() {
      return this.scenariosCollection.fetchAll()
        .then((response) => this.scenariosCollection.getAllEntitiesFromResponse(response))
        .then((enitites: Scenario[]) => {
          this.scenarios = enitites;
        })
  }


  private openScenarioDetailsPage(scenario: Scenario = null) {
    this.scenarioDetailsService.init(scenario);
    return this.navCtrl.push(ScenarioDetailsPage, {scenario: scenario});
  }


  private openScenario(scenario: Scenario) {
      this.scenarioDetailsService.openScenario(scenario)
  }


  private subscribeReloadEvent() {
      this.reloadEventSubscriber = this.events.subscribe('reloadScenarios', () => {
        this.getScenarios();
      })
  }

  private deleteScenario(scenario: Scenario) {
      return this.showConfirmToDeletePopup()
        .then(() => this.scenariosCollection.removeEntity(scenario))
        .then(() => this.getScenarios())
        .catch(() => {})
  }


  showConfirmToDeletePopup(): Promise<void> {
    return new Promise((resolve, reject) => {
      const yesLabelKey = 'common.buttons.YesBtn';
      const noLabelKey = 'common.buttons.NoBtn';
      const titleKey = 'scenarios.ConfirmationPopup.DeleteItem.Caption';
      const messageKey = 'card.ConfirmationPopup.DeleteItem.Question';
      this.localizationManager.getSeveralLocales([yesLabelKey, noLabelKey, titleKey, messageKey])
        .then((locales) => {
          const options: AlertOptions = {
            title: locales[titleKey],
            message: locales[messageKey],
            cssClass: 'popup alert confirm',
            enableBackdropDismiss: false,
            buttons: [
              {
                text: locales[yesLabelKey],
                role: 'cancel',
                handler: resolve
              },
              {
                text: locales[noLabelKey],
                role: 'cancel',
                cssClass: 'no',
                handler: reject
              }
            ]
          };

          this.alertCtrl.create(options).present();
        });
    });

  }
}
