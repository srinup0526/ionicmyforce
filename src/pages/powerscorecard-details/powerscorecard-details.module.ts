import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PowerscorecardDetailsPage } from './powerscorecard-details';

@NgModule({
  declarations: [
    PowerscorecardDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(PowerscorecardDetailsPage),
  ],
})
export class PowerscorecardDetailsPageModule {}
