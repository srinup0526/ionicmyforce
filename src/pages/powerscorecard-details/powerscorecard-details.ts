import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CoachingPlanCollection} from '../../collections/CoachingPlanCollection';
import { JointVisitPlanCollection } from '../../collections/JointVisitPlanCollection';
import { CoachingPlan } from '../../models/CoachingPlan';
import { SforceDataContext } from '../../collections/SforceDataContext';


/**
 * Generated class for the PowerscorecardDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-powerscorecard-details',
  templateUrl: 'powerscorecard-details.html',
})
export class PowerscorecardDetailsPage {
  CoachingPlanRecord: any;
  CoachingPlanDesc: any;
  account: any;
  JointVisitPlan:any;
  jointVisitPlans:any;
  jointvisitplanRecords:any = [];
  row:any;
  jointvisitplanRecord: any;
  public activeUser:any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private coachingPlanCollection: CoachingPlanCollection,
    public jointVisitPlanCollection: JointVisitPlanCollection
    ) {
    this.row = this.navParams.data['row'];
    this.CoachingPlanRecord = this.navParams.data['row'];
      // Fetch joint visit plan records

      this.jointVisitPlanCollection.fetchEntityByCoachingPlan(this.navParams.data['row'].id)
      .then(res=>{
        console.log("res",res);
        this.jointvisitplanRecords = res;
      })

    SforceDataContext.getActiveUser()
    .then((currentUser) => {
      console.log("currentUser",currentUser);
      this.activeUser = currentUser;
    });
  }

  ionViewDidLoad():void {
  }
}
