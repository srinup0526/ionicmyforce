import SettingsManager from './../services/db/SettingsManager';

export class DurationFilter
  {
    mapFilterItems(settings){
      const minutes = settings.duration
      console.log("settings",settings);
      let indexOfDefault = minutes.indexOf(settings.callDuration.minutes())
      indexOfDefault = indexOfDefault==-1?0:indexOfDefault;
      const durations = minutes.map((minute, index)=>{
        {
          id: index
          value: minute
          description: "${minute} minutes" 
        }
        durations.defaultValue = durations[indexOfDefault]
        return durations;
      });
    }
    public resources()
    {
      return SettingsManager.getTourPlanningSettings().then(settings=>{ return this.mapFilterItems(settings);});
    }
  }