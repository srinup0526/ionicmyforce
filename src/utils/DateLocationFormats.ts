const DATETIME_FORMATS = {
  zh: {
    'DD.MM.YYYY HH:mm': 'YYYY/MM/DD, HH:mm',
    'DD.MM.YYYY': 'YYYY/MM/DD',
    'DD/MM/YYYY': 'YYYY/MM/DD',
    'DD/MM/YYYY, HH:mm': 'YYYY/MM/DD, HH:mm',
    'HH:mm': 'HH:mm',
    'YYYY-MM-DD': 'YYYY-MM-DD',
    'YYYY-MM-DDTHH:mm:ss.SSSZZ': 'YYYY-MM-DDTHH:mm:ss.SSSZZ',
    'YYYY-MM-DDTHH:mm:ss.SSS': 'YYYY-MM-DDTHH:mm:ss.SSS',
    '-MMM-': '-MMM-'
  },

  default: {
    'DD.MM.YYYY HH:mm': 'DD.MM.YYYY HH:mm',
    'DD.MM.YYYY': 'DD.MM.YYYY',
    'DD/MM/YYYY': 'DD/MM/YYYY',
    'DD/MM/YYYY, HH:mm': 'DD/MM/YYYY, HH:mm',
    'HH:mm': 'HH:mm',
    'YYYY-MM-DD': 'YYYY-MM-DD',
    'YYYY-MM-DDTHH:mm:ss.SSSZZ': 'YYYY-MM-DDTHH:mm:ss.SSSZZ',
    'YYYY-MM-DDTHH:mm:ss.SSS': 'YYYY-MM-DDTHH:mm:ss.SSS',
    '-MMM-': '-MMM-'
  }
};


export class DateLocationFormats {
  static getFormat(timeString) {
    const DEFAULT_LANG = 'default';
    let currentLng = 'en'; // i18n.lng();

    if (DATETIME_FORMATS[currentLng]) {
      return DATETIME_FORMATS[currentLng][timeString];
    }

    return DATETIME_FORMATS[DEFAULT_LANG][timeString];
  }
}