import {FormControl, ValidatorFn} from "@angular/forms";

export class NumberValidator {
  
  static max(max: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      
      let val: number = control.value;
      
      if (val <= max) {
        return null;
      }
      return { 'max': true };
    }
  }
  
  static min(min: number): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } | null => {
      
      let val: number = control.value;
      
      if (val >= min) {
        return null;
      }
      
      return { 'min': true };
    }
  }
}
