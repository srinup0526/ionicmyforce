import * as moment from 'moment';
import {DateLocationFormats} from './DateLocationFormats';

declare var cordova;
declare var _;
var months = [
  'January', 'February', 'March', 'April', 'May',
  'June', 'July', 'August', 'September',
  'October', 'November', 'December'
  ];
 

export class Utils {

  static runSequentially(functionsList, results = []) {
    return new Promise((resolve, reject) => {
      let func;

      if (functionsList) {
        func = functionsList.shift();
      }

      if (func) {
        return Promise.resolve(func())
          .then((result) => {
            results.push(result);
            return Utils.runSequentially(functionsList, results);
          })
          .then(resolve, resolve);
      } else {
        return resolve(results);
      }
    });
  }

  static currentDate(dateStr?) {
    const today = dateStr ? new Date(dateStr) : new Date();

    return moment(today).format('YYYY-MM-DD');
  }

  static getYear(dateStr?) {
    const date = dateStr ? new Date(dateStr) : new Date();

    return moment(date).year();
  }


  static getQuarter(dateStr?) {
    const date = dateStr ? new Date(dateStr) : new Date();
    return moment(date).quarter();
  }

  static isMobileDevice() {
    return Utils.matchMedia('(max-height: 400px)');
  }

  static isIOS() {
   return /iphone|ipod|ipad/.test(window.navigator.userAgent.toLowerCase());
  }

  static isIPAD() {
    return /ipad/.test(window.navigator.userAgent.toLowerCase())
  }

  static getDuration(startTime, endTime) {
    return Math.round((endTime - startTime) / 1000)
  }

  static matchMedia(mediaQuery) {
    return window.matchMedia(mediaQuery).matches;
  }

  static extend( defaults, options ) {
    let extended = {};
    let prop;

    for (prop in defaults) {
      if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
        extended[prop] = defaults[prop];
      }
    }

    for (prop in options) {
      if (Object.prototype.hasOwnProperty.call(options, prop)) {
        extended[prop] = options[prop];
      }
    }

    return extended;
  }
  
  static localToUtc(time): moment.Moment {
    return moment(time).utc();
  }

  static toSalesForceDateTimeFormat(date) {
    return moment(date).format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';
  }

  static toSalesForceDateFormat(date) {
    return moment(date).format('YYYY-MM-DD');
  }

  static currentDateToSalesForceDateTimeFormat() {
    return moment().utc().format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';
  }

  static dateToSalesForceDateTimeFormat(date) {
    return moment(date).utc().format('YYYY-MM-DDTHH:mm:ss.SSS') + 'Z';
  }

  static originalDate(dateTime) {
    const dateFormat = Utils._getLocationFormat('YYYY-MM-DD');

    return  moment(dateTime).utc().format(dateFormat);
  }

  static originalDateTime(date) {
    const dateString = Utils.currentDate(date);
    let hh = date.getHours();
    let mm = date.getMinutes();

    if (hh < 10) {
      hh = '0' + hh;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }

    return moment(`${dateString}T${hh}:${mm}:00`).utc().format('YYYY-MM-DDTHH:mm:ss.SSSZZ');
  }

  static originalStartOfDate(date){
    const dateFormat = this._getLocationFormat("YYYY-MM-DDTHH:mm:ss.SSSZZ");

    const startOfDay = moment(date)
    startOfDay.hours(0)
    startOfDay.minutes(0)
    startOfDay.seconds(0)
    startOfDay.milliseconds(0)
    return startOfDay.utc().format(dateFormat)
  }

  static originalEndOfDate(date){
    const dateFormat = this._getLocationFormat("YYYY-MM-DDTHH:mm:ss.SSSZZ");

    const endOfDay = moment(date)
    endOfDay.hours(23)
    endOfDay.minutes(59)
    endOfDay.seconds(59)
    endOfDay.milliseconds(999)
    return endOfDay.utc().format(dateFormat)
  }
  
  static isNegativeCondition(field) {
    return ~field.indexOf('!');
  }
  
  static getOrCondition(settings, condition) {
    return condition.split('|').reduce(((result, field) => {
      if(Utils.isNegativeCondition(field)) {
        return result || !settings[field.substr(1, field.length)];
      }
      
      return result || settings[field];
    }), false);
  }
  
  static getAndCondition(settings, condition) {
    return condition.split('&').reduce(((result, field) => {
      
      if(Utils.isNegativeCondition(field)) {
        return result && !settings[field.substr(1, field.length)];
      }
      
      return result && settings[field];
    }), true);
  }
  
  static isPositiveConditionBySettings(settings, condition) {
    if (~condition.indexOf('&')) {
      return Utils.getAndCondition(settings, condition);
    } else if (~condition.indexOf('|')) {
      return Utils.getOrCondition(settings, condition);
    } else {
      if(Utils.isNegativeCondition(condition)) {
        return !settings[condition.substr(1, condition.length)];
      }
      
      return settings[condition];
    }
  }

  static isBetweenDate(date, startDate, endDate){
    const momentDate = this.clearSmallTimePart(moment(date));
    const momentEndDate = this.clearSmallTimePart(moment(endDate));
    const momentStartDate = this.clearSmallTimePart(moment(startDate));

    return (momentDate.isBefore(momentEndDate) && momentDate.isAfter(momentStartDate)) || momentDate.isSame(momentEndDate) || momentDate.isSame(momentStartDate);
  }

  static clearSmallTimePart (date){
    const momentDate = date.clone();
    momentDate.seconds(0)
    momentDate.milliseconds(0)
    return momentDate;
  }

  static getDateByStr(dateStr) {
    return moment(dateStr).toDate()
  }

  static dateWithAddingOffsetMin(date, offsetMin) {
    if (!Number.isInteger(offsetMin))
    {
      offsetMin = 0;
    }
    return moment(date).add(offsetMin, 'minutes').toDate()
  }

  static formatDateVisit(date){
    const dateFormat = this._getLocationFormat("YYYY-MM-DD");
    return moment(date).format(dateFormat);
  }

  static timeFromString(timeString)
  {
    const timeParts = timeString.split(':');
    return moment({hours: parseInt(timeParts[0]), minutes: parseInt(timeParts[1])})
  }

  static originalStartOfToday()
  {
    return this.originalStartOfDate(new Date)
  }

  static originalEndOfToday()
  {
    return this.originalEndOfDate(new Date)
  }
  
  static originalStartOfTomorrow()
  {
    const date = new Date
    date.setDate(date.getDate() + 1)
    return this.originalStartOfDate(date);
  }

  static originalEndOfTomorrow()
  {
    const date = new Date
    date.setDate(date.getDate() + 1)
    return this.originalEndOfDate(date);
  }
  

  static chunkArray(array = [], chunkSize = 1) {
    const result = [];
    let i = 0;

    if (chunkSize < 1) {
      return array;
    }

    while (i < array.length) {
      result.push(array.slice(i, i + chunkSize));
      i += chunkSize;
    }

    return result;
  }

  static _getLocationFormat(dateTime) {
    return DateLocationFormats.getFormat(dateTime);
  }

  static formatDateTime(timeString) {
    const dateFormat = Utils._getLocationFormat('DD.MM.YYYY HH:mm');

    return moment(moment.utc(timeString).toDate()).format(dateFormat);
  }

  static chunk(array, n) {
    const lists = _.chain(array)
      .groupBy((element, index) => {
        return Math.floor(index / n);
      })
      .toArray()
      .value();

    return lists;
  }

  static deviceIsOnline(): boolean {
    const deviceConnection = cordova.require('com.salesforce.util.bootstrap');

    return deviceConnection.deviceIsOnline();
  }

  static dotFormatDate(timeString: string): string {
    return moment(timeString).format("DD.MM.YYYY");
  }
  
  static delay(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }
  
  static currentDateTime(date?: string): string {
    return Utils.formatDateTime(date || new Date());
  }

  static formatDateTimeWithBreak(timeString: string): string {
    return Utils.formatDateTime(timeString).replace(' ', '<br/>');
  }

  static firstDateCurrentMonth(month,year) {
    var monthnum = months.indexOf(month);
    var startdate = new Date(year,monthnum,1);
    return this.formatDateVisit(startdate);
  }

  static endDateCurrentMonth(month,year) {
    var monthnum = months.indexOf(month);
    var enddate = new Date(year,monthnum+1,0);
    return this.formatDateVisit(enddate);
  }

  static perviousMonthCheck(month,year) {
    var monthnum = months.indexOf(month);
    var selectedDate = new Date(year,monthnum,1);
    console.log("check date value", this.formatDateVisit(selectedDate) > this.formatDateVisit(new Date()));
    if(this.formatDateVisit(selectedDate) > this.formatDateVisit(new Date())) {
      return true;
    } else {
      return false;
    }
  }
  
}
