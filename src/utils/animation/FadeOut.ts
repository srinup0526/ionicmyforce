import { Animation, PageTransition } from 'ionic-angular';

export class FadeOut extends PageTransition {

	public init() {
		const element = this.leavingView.pageRef().nativeElement;
		const wrapper = new Animation(this.plt, element.querySelector('.modal-wrapper'));
		const contentWrapper = new Animation(this.plt, element.querySelector('.wrapper'));
		const backdropAnimation = new Animation(this.plt, element.querySelector('ion-backdrop'));

		wrapper.beforeStyles({ 'transform': 'translate3d(0, 0, 0)', 'opacity': 0 });
		wrapper.fromTo('transform', 'translate3d(0, 0, 0)', 'translate3d(0, 0, 0)');
		wrapper.fromTo('opacity', 0, 1);
		contentWrapper.fromTo('opacity', 1, 0);
		backdropAnimation.fromTo('opacity', 0.4, 0);

		this
			.element(this.leavingView.pageRef())
			.duration(500)
			.easing('cubic-bezier(.1, .7, .1, 1)')
			.add(contentWrapper)
			.add(backdropAnimation)
			.add(wrapper);

	}
}