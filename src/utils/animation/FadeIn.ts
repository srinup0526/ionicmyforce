import { Animation, PageTransition } from 'ionic-angular';

export class FadeIn extends PageTransition {

	public init() {
		const element = this.enteringView.pageRef().nativeElement;
		const wrapperAnimation = new Animation(this.plt, element.querySelector('.modal-wrapper'));
		const backdropAnimation = new Animation(this.plt, element.querySelector('ion-backdrop'));

		wrapperAnimation.beforeStyles({ 'transform': 'translate3d(0, 0, 0)', 'opacity': 0 });
		wrapperAnimation.fromTo('transform', 'translate3d(0, 0, 0)', 'translate3d(0, 0, 0)');
		wrapperAnimation.fromTo('opacity', '0', '1');
		backdropAnimation.fromTo('opacity', 0.01, 0.4);

		this
			.element(this.enteringView.pageRef())
			.duration(500)
			.beforeAddClass('show-page')
			.easing('cubic-bezier(.1, .7, .1, 1)')
			.add(backdropAnimation)
			.add(wrapperAnimation);

	}
}