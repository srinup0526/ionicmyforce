import {NavController, Navbar, Platform, AlertController} from "ionic-angular";
import {ViewChild} from "@angular/core";
import {ConfirmPopup} from "./../components/popups/ConfirmPopup";
import {Loader} from "../services/common/Loader";
import {LocalizationManager} from "./../services/common/localizations/LocalizationManager";


export abstract class BackHandlers {
  
  public unregisterBackButtonAction: any;
  
  protected isChanged: boolean;
  
  @ViewChild('navbar') navBar: Navbar;
  
  constructor(protected navCtrl: NavController,
              protected localizationManager: LocalizationManager,
              protected loader: Loader,
              protected alertCtrl: AlertController,
              protected platform: Platform){
    this.isChanged = false;
  }
  
  public ionViewWillLeave() {
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }
  
  public ionViewDidLoad() {
    this.initializeBackButtonCustomHandler();
  }
  
  public initializeBackButtonCustomHandler(): void {
    this.navBar.backButtonClick = this.backButtonClick.bind(this);
    
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(
      this.backButtonClick.bind(this),
      101
    );
  }
  
  protected backButtonClick(event) {
    if(this.hasChanges()) {
      return this.backConfirmPopup()
        .then((result) => {
          if(result.doAction) {
            return this.loader.run(this.validateAndSaveOrder.bind(this))
          }
          
          return this.goBack();
        })
    }
    
    return this.goBack();
  }
  
  protected goBack() {
    return this.navCtrl.pop();
  }
  
  protected hasChanges() {
    return this.isChanged;
  }
  
  protected dataChanged() {
    this.isChanged = true;
  }
  
  protected resetChanges() {
    this.isChanged = false;
  }
  
  protected backConfirmPopup() {
    const popup: ConfirmPopup = new ConfirmPopup(this.alertCtrl, this.localizationManager);
    
    return popup.showPopup({
      title: 'card.ConfirmationPopup.SaveChanges.Caption',
      yesLabel: 'common.buttons.YesBtn',
      noLabel: 'common.buttons.NoBtn'
    });
  }
  
  protected isSameFillFields(current, startBackup, schemeFields) {
    return Object.keys(schemeFields)
      .some((localKey) => {
        if(typeof current[localKey] == "number" || typeof startBackup[localKey] == "number"){
          return parseFloat(current[localKey]) != parseFloat(startBackup[localKey]);
        }
        
        if(current[localKey] || startBackup[localKey]){
          return current[localKey] != startBackup[localKey];
        }
        
        return !!current[localKey] != !!startBackup[localKey];
      })
  }
  
  protected abstract validateAndSaveOrder();
}

