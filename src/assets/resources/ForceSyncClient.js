var __forceSyncClientInstance = undefined;

var getForceSyncClient = function() {
  if (__forceSyncClientInstance === undefined) {
    __forceSyncClientInstance = new ForceSyncClient();
  }

  return __forceSyncClientInstance;
};

var ForceSyncClient = function() {
  this.userId = undefined;
  this.jsClient = undefined;
  this.smartstoreClient = undefined;
};

ForceSyncClient.prototype.CACHE_MODE = Force.CACHE_MODE;
ForceSyncClient.prototype.MERGE_MODE = Force.MERGE_MODE;
ForceSyncClient.prototype.Error = Force.Error;

ForceSyncClient.prototype.init = function() {
  Force.init();
  this.jsClient = Force.forceJsClient;
  this.smartstoreClient = Force.smartstoreClient;
};

ForceSyncClient.prototype.syncRemoteObjectWithCache = function() {
  return this.convertToJQueryPromise(Force.syncRemoteObjectWithCache.apply(Force, arguments));
};

ForceSyncClient.prototype.fetchSObjectsFromServer = function() {
  return Force.fetchSObjectsFromServer.apply(Force, arguments);
};

ForceSyncClient.prototype.convertToJQueryPromise = function(nativePromise) {
  return new Promise((resolve, reject) => {
    nativePromise.then(resolve, reject);
  });
};