import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';
/**
 * Generated class for the UniquePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'unique',
  pure: false
})
export class UniquePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: any[], uniqueBy:string): any{
    if(value!== undefined && value!== null){
      if(uniqueBy == 'Priority__c')
      return _.uniqBy(_.filter(value, (v)=> v.Account.Priority__c ), 'Account.Priority__c');
      else if(uniqueBy == 'Subtype__c')
      return _.uniqBy(_.filter(value, (v)=> v.Subtype__c ), 'Subtype__c');
     else if(uniqueBy == 'Specialty1__c')
     return _.uniqBy(_.filter(value, (v)=> {console.log(v); v[0].Specialty1__c } ), 'Specialty1__c');
     else if(uniqueBy == 'RecordType.Name')
     return _.uniqBy(_.filter(value, (v)=> v.RecordType.Name ), 'RecordType.Name');
     else if(uniqueBy == 'Org_Brick_Name__c')
     return _.uniqBy(_.filter(value, (v)=> {console.log(v); v[0].Org_Brick_Name__c}), 'Org_Brick_Name__c');
     else if(uniqueBy == 'Type__c')
     return _.uniqBy(_.filter(value, (v)=> v.Type__c ), 'Type__c');
     else if(uniqueBy == 'Customer__r.Account.Priority__c')
       return _.uniqBy(_.filter(value, (v)=> { v.Customer__r.Account.Priority__c }), 'Customer__r.Account.Priority__c');
     
       else if(uniqueBy == 'Patch__r.Name')
       return _.uniqBy(_.filter(value, (v)=>  v.Patch__r.Name ),'Patch__r.Name');

       else if(uniqueBy == 'RecordType.Name')
       return _.uniqBy(_.filter(value, (v)=> { v.RecordType.Name }), 'RecordType.Name');

    }

    return value;
  
}
}
