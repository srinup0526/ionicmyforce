import { Pipe, PipeTransform } from '@angular/core';
//import { ContactPage } from '../../pages/contact/contact';
/**
 * Generated class for the SelectboxPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'selectbox',
})
export class SelectboxPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: any, contacts?: any): any {
    return contacts ? items.filter(contacts => contacts.Account.Priority__c === contacts) : items;
}

}
