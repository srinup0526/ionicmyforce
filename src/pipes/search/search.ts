import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'search',
})
export class SearchPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(contacts: any, term:any) :any{
    if ( term === undefined ) return contacts;
    return contacts.filter(function(contact){
      return contact.name.toLowerCase().include(term.toLowerCase());
    })
  }
}
