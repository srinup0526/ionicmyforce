import { NgModule } from '@angular/core';
import { SearchPipe } from './search/search';
import { SelectboxPipe } from './selectbox/selectbox';
import { SortPipe } from './sort/sort';
import { UniquePipe } from './unique/unique';
import { MomentPipe } from './moment/moment';
import { SafeSrcPipe } from './safe/safe-src';
//import { SelectboxPipe } from './selectbox/selectbox';
@NgModule({
	declarations: [SearchPipe,
    SelectboxPipe,
    SortPipe,
    UniquePipe,
    MomentPipe,
    SafeSrcPipe
    ],
	imports: [],
	exports: [SearchPipe,
    SelectboxPipe,
    SortPipe,
    UniquePipe,
    MomentPipe,
    SafeSrcPipe
   ]
})
export class PipesModule {}
