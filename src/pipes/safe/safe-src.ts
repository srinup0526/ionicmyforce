import { Pipe } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({name: 'safeSrc'})
export class SafeSrcPipe {
  constructor(private sanitizer:DomSanitizer){}
  
  transform(html) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(html);
  }
}
