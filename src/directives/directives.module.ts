import { NgModule } from '@angular/core';
import { NgxResizeWatcherDirective } from './ngx-resize-watcher/ngx-resize-watcher';
@NgModule({
	declarations: [NgxResizeWatcherDirective],
	imports: [],
	exports: [NgxResizeWatcherDirective]
})
export class DirectivesModule {}
