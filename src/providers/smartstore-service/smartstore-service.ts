import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
declare var cordova:any;
import * as Constants from '../../services/constants';
import moment from 'moment';
import {Storage} from '@ionic/storage'

/*
  Generated class for the SmartstoreServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SmartstoreServiceProvider {

  defaultPageSize:number = 500;
  closestVisitsCount:number = 2;
  loggedInUserDetails:any;
  atcArray:any = [];

  public smartStore(): any {
    return cordova.require("com.salesforce.plugin.smartstore")
  }
  medicalContactPersonId:string='';

  constructor(public http: HttpClient, public storage:Storage) {
    console.log('Hello SmartstoreServiceProvider Provider');
    this.storage.get("configurationData").then(config=>{
    console.log("config",config);
    this.medicalContactPersonId = config?config.sampleManagementSettings.MedicalContactId:null;
    this.storage.get("currentUserDetails").then(data => { this.loggedInUserDetails = data; 
      console.log("UserData",this.loggedInUserDetails);
      // fix issue appeared
      this.atcArray = this.loggedInUserDetails && this.loggedInUserDetails.ATC_Class__c ? this.loggedInUserDetails.ATC_Class__c.split(' ') : [];
      console.log("this.atcArray",this.atcArray);
       });
  })
  }

  getsurveyQuestionaireDetails(surveyId:string)
  {
    console.log("surveyId",surveyId);
    let promise: Promise<{ records: any[] }> = new Promise(
      (resolve, reject) => {
        var querySpec = this.smartStore().buildExactQuerySpec("Survey__c",surveyId);
        let success = results => {
          console.log("surveyQuestinaireDetails",results);
          resolve({ records: results.currentPageOrderedEntries });
        };
        this.smartStore().querySoup(
          Constants.SurveyQuestionnaire,
          querySpec,
          success,
          reject
          );

      }
      );
    return promise;
  }

  getAllContactReferences(isTargetCustomer='Yes'): Promise<any> {
    console.log('isTargetCustomer',isTargetCustomer);
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {"+Constants.Reference+":_soup} FROM {"+Constants.Reference+"}, {"+Constants.Contact+"} WHERE {"+Constants.Reference+":Customer__c} = {"+Constants.Contact+":Id} and {"+Constants.Reference+":Target_Customer__c}= '"+isTargetCustomer+"' and {"+Constants.Reference+":C_Status_Reference__c}=3", this.defaultPageSize);
      console.log("querySpec",querySpec);
      this.smartStore().runSmartQuery(querySpec, function(cursor) { 
        // result should be [[ n ]] if there are n employees
        console.log("cursor",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });
    });
  }

  getAppIdandSoupEntryId(id: string, _soupEntryId:string) {
    console.log("SmartstoreServiceProvider.getAppID");
    console.log("Id",id);
    console.log("_soupEntryId",_soupEntryId);
 // console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {CallReport:_soup} from {CallReport} where {CallReport:_soupEntryId}="+_soupEntryId+" OR {CallReport:Id}='"+id+"'";
   // var pageSize = 10;
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,1);
      let success = results => {
        console.log("AppointmentDetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  removeAppointment(row:any)
  {
    let promise: Promise<{ result: any }> = new Promise(
    (resolve, reject) => {
      let success = results => {
        console.log("referiddetails",results);
        resolve({ result: results.currentPageOrderedEntries });
      };
      let rowId = {'id':"'"+row.Id+"'"};
      console.log("row",row)
      this.smartStore().removeFromSoup(
        Constants.CallReport,
        [row._soupEntryId],
        success,
        reject
      );
      
    }
  );
    return promise;
  }
  removeRedFlag(row:any)
  {
    let promise: Promise<{ result: any }> = new Promise(
    (resolve, reject) => {
      let success = results => {
        console.log("redFlagDetails",results);
        resolve({ result: results.currentPageOrderedEntries });
      };
      console.log("row",row)
      this.smartStore().removeFromSoup(
        Constants.RedFlag,
        [row._soupEntryId],
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  getCallListForSpecificContact(customerId:string)
  {
    let promise: Promise<{ records: any[] }> = new Promise(
      (resolve, reject) => {
        var smartSql =  "select {"+Constants.CallReport+":_soup} from {"+Constants.CallReport+"} where {"+Constants.CallReport+":Contact1__c}='"+customerId+"' Order By {"+Constants.CallReport+":Date_Time_Planned__c} Desc limit 1";
        var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,1);
        let success = results => {
          console.log("PharmaEventiddetails",results);
          resolve({ records: results.currentPageOrderedEntries });
        };
        this.smartStore().runSmartQuery(
          querySpec,
          success,
          reject
          );
      }
      );
    return promise;

  }

  getpharmaproductId(id: string) {
    console.log("SmartstoreServiceProvider.getpharmaproductId");
  console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var querySpec = this.smartStore().buildExactQuerySpec("Id",id);
      let success = results => {
        console.log("referiddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().querySoup(
        Constants.PharmaEvent,
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }
  loadPharmamessages(): Promise<any> {
    console.log('loadPharmamessages');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.ProductMessage, querySpec, success, reject);
    });
  }

  loadproducts(): Promise<any> {
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) =>{
        console.log("this.atcArray",this.atcArray);
        let productArray = [];
        if(results.currentPageOrderedEntries.length>0)
        {
          results.currentPageOrderedEntries.forEach(val=>{
            if(val.ATC_Class__c && val.ATC_Class__c!=null)
            {
              if(this.atcArray.indexOf(val.ATC_Class__c)>=-1)
              {
                productArray.push(val);
              }
            }
          });
          results.currentPageOrderedEntries = productArray;
          console.log("results",results);
          resolve({ records: results.currentPageOrderedEntries });
        }
        else
        {
          results.currentPageOrderedEntries = productArray;
          console.log("results",results);
          resolve({ records: results.currentPageOrderedEntries });
        }
      }
      
      this.smartStore().querySoup(Constants.Product, querySpec, success, reject);
    });
  }
  loadBrick(): Promise<any> {
    console.log('loadBricks');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.Brick, querySpec, success, reject);
    });
  }

  loadAllUsers(): Promise<any> {
    console.log('loadAllUsers');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.User, querySpec, success, reject);
    });
  }

  loadMtp(): Promise<any> {
    console.log('loadMtp');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.MTP, querySpec, success, reject);
    });
  }

  deleteMTPIdandSoupEntryId(id: string, _soupEntryId:string) {
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      let success = results => {
        console.log("delete MTPiddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      let failure = (error)=> {
        console.log("Soup deleting Having an issue",error);
      }
      this.smartStore().removeFromSoup(
        Constants.MTP,
        [_soupEntryId],
        success,
        failure,
      );
    }
  );
    return promise;
  }
  deleteMedicalqueryIdandSoupEntryId(id: string, _soupEntryId:string) {
    
    let promise: Promise<{ records: any[] }> = new Promise(
      (resolve, reject) => {
        let success = results => {
          console.log("delete Medicalqueryiddetails",results);
          resolve({ records: results.currentPageOrderedEntries });
        };
        let failure = (error)=> {
          console.log("Soup deleting Having an issue",error);
        }
        this.smartStore().removeFromSoup(
          Constants.MedicalQuery,
          [_soupEntryId],
          success,
          failure,
        );
      }
    );
      return promise;
    }
  deletePharmaEventIdandSoupEntryId(id: string, _soupEntryId:string) {
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      let success = results => {
        console.log("delete PharmaEventiddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      let failure = (error)=> {
        console.log("Soup deleting Having an issue",error);
      }
      this.smartStore().removeFromSoup(
        Constants.PharmaEvent,
        [_soupEntryId],
        success,
        failure,
      );
      
    }
  );
    return promise;
  }

  getPharmaeventIdandSoupEntryId(id: string, _soupEntryId:string) {
    let promise: Promise<{ records: any[] }> = new Promise(
      (resolve, reject) => {
        var smartSql =  "select {"+Constants.PharmaEvent+":_soup} from {"+Constants.PharmaEvent+"} where {"+Constants.PharmaEvent+":_soupEntryId}="+_soupEntryId+" OR {"+Constants.PharmaEvent+":Id}='"+id+"'";
        var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,1);
        let success = results => {
          console.log("PharmaEventiddetails",results);
          resolve({ records: results.currentPageOrderedEntries });
        };
        this.smartStore().runSmartQuery(
          querySpec,
          success,
          reject
          );
      });
    return promise;
  }

  getMTPEventId(id: string,soupEntryId:number) {
    console.log("SmartstoreServiceProvider.getMTPEventId");
    console.log("Id",id);
    
 // console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {CallReport:_soup} from {CallReport} where {CallReport:MTP__c}='"+id+"' or {CallReport:MTP__c}="+soupEntryId;
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,this.defaultPageSize);
      let success = results => {
        console.log("MTPEventiddetails",results.currentPageOrderedEntries);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
        querySpec,
        success,
        reject
      );
    }
  );
    return promise;
  }
  getPatchCustomerId(id: string) {
    console.log("SmartstoreServiceProvider.getPatchCustomerId");
  console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {"+Constants.PatchCustomer+":_soup} from {"+Constants.PatchCustomer+"} where {"+Constants.PatchCustomer+":Patch__c}='"+id+"'";
   // var pageSize = 10;
   console.log("smartSql",smartSql);
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,10000);
      let success = results => {
        console.log("patchcustomeriddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }
  getMTPIdandSoupEntryId(id: string, _soupEntryId:string) {
    console.log("SmartstoreServiceProvider.getMTPID");
    console.log("Id",id);
    console.log("_soupEntryId",_soupEntryId);
 // console.log("IDData",id);
    id = id?id:'';
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {MTP:_soup} from {MTP} where {MTP:_soupEntryId}="+_soupEntryId+" OR {MTP:Id}='"+id+"'";
   // var pageSize = 10;
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,1);
      let success = results => {
        console.log("MTPiddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  getPatchIdandSoupEntryId(id: string, _soupEntryId:string) {
    console.log("SmartstoreServiceProvider.getPatchID");
    console.log("Id",id);
    console.log("_soupEntryId",_soupEntryId);
 // console.log("IDData",id);
   // id = id?id:'';
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {PatchCustomer:_soup} from {PatchCustomer} where {PatchCustomer:_soupEntryId}="+_soupEntryId+" OR {PatchCustomer:Id}='"+id+"'";
   // var pageSize = 10;
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,1);
      let success = results => {
        console.log("PatchCustomeriddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  getTOTIdandSoupEntryId(id: string, _soupEntryId:string) {
    console.log("SmartstoreServiceProvider.getTOTidandSoupEntryId");
    console.log("Id",id);
    console.log("_soupEntryId",_soupEntryId);
 // console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {Tot:_soup} from {Tot} where {Tot:_soupEntryId}="+_soupEntryId+" OR {Tot:Id}='"+id+"'";
   // var pageSize = 10;
   console.log("smartSql",smartSql);
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,1);
      let success = results => {
        console.log("TOTiddetails resultss",results, results.currentPageOrderedEntries);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
        
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  loadPatch(): Promise<any> {
    console.log('loadPatch');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.Patch, querySpec, success, reject);
    });
  }

  loadPatchCustomer(): Promise<any> {
    console.log('loadPatchCustomer');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.PatchCustomer, querySpec, success, reject);
    });
  }

  getRedFlagInfo(customer:string)
  {
    console.log("customer",customer);
    let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var querySpec = this.smartStore().buildExactQuerySpec("CustomerName__c",customer);
      let success = results => {
        console.log("redFlagInfo",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().querySoup(
        Constants.RedFlag,
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  getMedicalcommentsid(id: string) {
    console.log("SmartstoreServiceProvider.getMedicalcommentsid");
    console.log("Id",id);
    
 // console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {MedicalComment:_soup} from {MedicalComment} where {MedicalComment:Id}='"+id+"'";
   // var pageSize = 10;
   console.log("smartSql",smartSql);
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,10000);
      let success = results => {
        console.log("MedicalcommentsId",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  loadMedicalComments(): Promise<any> {
    console.log('loadPharmamessages');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.MedicalComment, querySpec, success, reject);
    });
  }

  getMedicalQueryIdandSoupEntryId(id: string, _soupEntryId:string) {
    console.log("SmartstoreServiceProvider.getMedicalQueryIdandSoupEntryId");
    console.log("Id",id);
    console.log("_soupEntryId",_soupEntryId);
 // console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {MedicalQuery:_soup} from {MedicalQuery} where {MedicalQuery:_soupEntryId}="+_soupEntryId+" OR {MedicalQuery:Id}='"+id+"'";
   // var pageSize = 10;
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,1);
      let success = results => {
        console.log("medicalqueryiddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
       
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  deleteTOTIdandSoupEntryId(id: string, _soupEntryId:string) {
    console.log("SmartstoreServiceProvider.deleteIdandSoupEntryId");
    console.log("Id",id);
    console.log("_soupEntryId",_soupEntryId);
 // console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      // var smartSql =  "select {TOT:_soup} from {TOT} where {TOT:_soupEntryId}="+_soupEntryId+" OR {TOT:Id}='"+id+"'";
     // var pageSize = 10;
     // var querySpec = (navigator as sdkNavigator).smartstore.buildSmartQuerySpec("Id",id ,"_soupEntryId",_soupEntryId);
      let success = results => {
        console.log("delete TOTiddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      let failure = (error)=> {
        console.log("Soup deleting Having an issue",error);
      }
      this.smartStore().removeFromSoup(
        Constants.Tot,
        [_soupEntryId],
        success,
        failure,
      );
      
    }
  );
    return promise;
  }


  loadAllmedicalquery(): Promise<any> {
    console.log('loadAllmedicalquery');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.MedicalQuery, querySpec, success, reject);
    });
  }

  loadrepmedicalquery(): Promise<any> {
    console.log('loadrepmedicalquery');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.MedicalQuery, querySpec, success, reject);
    });
  }

  loadmedmedicalquery(): Promise<any> {
    console.log('loadmedmedicalquery');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.MedicalQuery, querySpec, success, reject);
    });
  }

  loadDraftmedicalquery(): Promise<any> {
    console.log('loadDraftmedicalquery');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.MedicalQuery, querySpec, success, reject);
    });
  }
  loadClosedmedicalquery(): Promise<any> {
    console.log('loadClosedmedicalquery');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.MedicalQuery, querySpec, success, reject);
    });
  }


  loadmedicalquery(): Promise<any> {
    console.log('loadmedicalquery');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.MedicalQuery, querySpec, success, reject);
    });
  }
  getMedicalqueryId(id: string) {
    console.log("SmartstoreServiceProvider.getMTPID");
 // console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var querySpec = this.smartStore().buildExactQuerySpec("Id",id);
      let success = results => {
        console.log("Madicalqueryiddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().querySoup(
        Constants.MedicalQuery,
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  loadAllmedicalcontactperson(): Promise<any> {
    console.log('loadAllmedicalcontactperson');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {User:_soup} FROM {User} WHERE {User:UserRoleId}="+this.medicalContactPersonId, this.defaultPageSize);
      console.log("querySpec",querySpec);
      this.smartStore().runSmartQuery(querySpec, function(cursor) { 
        // result should be [[ n ]] if there are n employees
        console.log("cursor",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });
    });
  }

  loadMedia(): Promise<any> {
    console.log('loadMedia');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.Presentation, querySpec, success, reject);
    });
  }

  loadTot(): Promise<any> {
    console.log('loadTot');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.Tot, querySpec, success, reject);
    });
  }
  loadContacts(): Promise<any> {
    console.log('loadContacts');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.Contact, querySpec, success, reject);
    });
  }
  loadReference(): Promise<any> {
    console.log('loadReference');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.Reference, querySpec, success, reject);
    });
  }

  loadorg(): Promise<any> {
    console.log('loadReference');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.Organization, querySpec, success, reject);
    });
  }

  getContactReference(id: string) {
    console.log("IDData",id);

    let promise: Promise<{ records: any[] }> = new Promise(
      (resolve, reject) => {
        var querySpec = this.smartStore().buildExactQuerySpec("Customer__c",id);
        let success = results => {
          console.log("referiddetails",results);
          resolve({ records: results.currentPageOrderedEntries });
        };
        this.smartStore().querySoup(
          Constants.Reference,
          querySpec,
          success,
          reject
          );

      }
      );
    return promise;
  }

  getContact(id: string) {
    console.log("SmartstoreServiceProvider.getContact");
    
    let promise: Promise<{ records: any[] }> = new Promise(
      (resolve, reject) => {
        var querySpec = this.smartStore().buildExactQuerySpec(
          "Id",
          id
        );
        let success = results => {
          console.log(results);
          resolve({ records: results.currentPageOrderedEntries });
        };
        this.smartStore().querySoup(
          Constants.Contact,
          querySpec,
          success,
          reject
        );
      }
    );
    return promise;
  }

  getReference(id: string) {
    console.log("SmartstoreServiceProvider.getReferenceID");
    console.log("IDData",id);
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var querySpec = this.smartStore().buildExactQuerySpec("Organisation__c",id);
      let success = results => {
        console.log("referiddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().querySoup(
        Constants.Reference,
        querySpec,
        success,
        reject
      );
    }
  );
    return promise;
  }
  getOrganizationInfieldActivities(id: string) {
    console.log("SmartstoreServiceProvider.getOrganizationInfieldActivities");
    console.log("Id",id);
    
 // console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {"+Constants.CallReport+":_soup} from {"+Constants.CallReport+"} where {"+Constants.CallReport+":Organisation__c}='"+id+"'";
   // var pageSize = 10;
   console.log("smartSql",smartSql);
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,10000);
      let success = results => {
        console.log("InfieldActivitiesOrgdetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  getOrganization(id: string) {
    console.log("SmartstoreServiceProvider.getOrganizationID");
    let promise: Promise<{ records: any[] }> = new Promise(
      (resolve, reject) => {
        var querySpec = this.smartStore().buildExactQuerySpec("Id",id);
        let success = results => {
          console.log("Orgiddetails",results);
          resolve({ records: results.currentPageOrderedEntries });
        };
        this.smartStore().querySoup(
          Constants.Organization,
          querySpec,
          success,
          reject
        );
      }
    );
    return promise;
  }

  loadSurvey(): Promise<any> {
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.Survey, querySpec, success, reject);
    });
  }

  getsurveyId(id: string) {
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var querySpec = this.smartStore().buildExactQuerySpec("Id",id);
      let success = results => {
        console.log("surveyiddetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().querySoup(
        Constants.Survey,
        querySpec,
        success,
        reject
      );
    });
    return promise;
  }

  getContactInfieldActivities(id: string) {
    console.log("SmartstoreServiceProvider.getOrganizationInfieldActivities");
    console.log("Id",id);
    
 // console.log("IDData",id);
    
  let promise: Promise<{ records: any[] }> = new Promise(
    (resolve, reject) => {
      var smartSql =  "select {CallReport:_soup} from {CallReport} where {CallReport:Contact1__c}='"+id+"'";
   // var pageSize = 10;
   console.log("smartSql",smartSql);
      var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,this.defaultPageSize);
      let success = results => {
        console.log("InfieldActivitiesContactdetails",results);
        resolve({ records: results.currentPageOrderedEntries });
      };
      this.smartStore().runSmartQuery(
        querySpec,
        success,
        reject
      );
      
    }
  );
    return promise;
  }

  loadActivites(): Promise<any> {
    console.log('loadActivites');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'descending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.CallReport, querySpec, success, reject);
    });
  }

  appointmentsToday(): Promise<any> {
    console.log('appointmentsToday');
    return new Promise((resolve, reject) => {
      let startDay = this.originalStartOfDate(new Date());
      let endDay = this.originalEndOfDate(new Date())
      console.log("startDay",startDay);
      console.log("endDay",endDay);
      console.log("query","SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='Appointment' and {CallReport:Date_Time_Planned__c}>= DATETIME('"+startDay+"','localtime') and {CallReport:Date_Time_Planned__c}<=DATETIME('"+endDay+"','localtime')");
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='Appointment' and {CallReport:Date_Time_Planned__c}>= DATETIME('"+startDay+"','localtime') and {CallReport:Date_Time_Planned__c}<=DATETIME('"+endDay+"','localtime')", this.defaultPageSize);
      this.smartStore().runSmartQuery(querySpec, function(cursor) { 
        // result should be [[ n ]] if there are n employees
        console.log("cursor",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });
    });
  }

  closestAppointmentsToday(): Promise<any> {
    console.log('appointmentsToday');
    return new Promise((resolve, reject) => {
      let startDay = this.originalDateTime(new Date());
      console.log("startDay",startDay);
      console.log("query","SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='Appointment' and {CallReport:Date_Time_Planned__c}>= DATETIME('"+startDay+"','localtime') limit 2 Order By  {CallReport:Date_Time_Planned__c}");
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='Appointment' and {CallReport:Date_Time_Planned__c}>= DATETIME('"+startDay+"','localtime')  Order By  {CallReport:Date_Time_Planned__c}", this.closestVisitsCount);
      this.smartStore().runSmartQuery(querySpec, function(cursor) { 
        console.log("closest visits data",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });
    });
  }

  allAppointments(): Promise<any> {
    console.log('allAppointments');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='Appointment'", this.defaultPageSize);
      this.smartStore().runSmartQuery(querySpec, function(cursor) {
        console.log("cursor",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });
    });
  }

  appointmentsPast(): Promise<any> {
    console.log('appointmentsPast');
    return new Promise((resolve, reject) => {
      console.log("query","SELECT {callReport:_soup} FROM {callReport} WHERE {callReport:Type__c}='Appointment' and {callReport:Date_Time_Planned__c}<= DATETIME('now','localtime')");
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='Appointment' and {CallReport:Date_Time_Planned__c}<= DATETIME('now','localtime')", this.defaultPageSize);
      this.smartStore().runSmartQuery(querySpec, function(cursor) { 
        // result should be [[ n ]] if there are n employees
        console.log("cursor",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });
    });
  }
  appointmentsTomorrow(): Promise<any> {
    console.log('appointmentsTomorrow');
    return new Promise((resolve, reject) => {
      var date = new Date()
      date.setDate(date.getDate() + 1)
      let startDay = this.originalStartOfDate(date);
      let endDay = this.originalEndOfDate(date)
      console.log("startDay",startDay);
      console.log("endDay",endDay);
      console.log("query","SELECT {callReport:_soup} FROM {callReport} WHERE {callReport:Type__c}='Appointment' and {callReport:Date_Time_Planned__c}>= DATETIME('"+startDay+"','localtime') and {callReport:Date_Time_Planned__c}<=DATETIME('"+endDay+"','localtime')");
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='Appointment' and {CallReport:Date_Time_Planned__c}>= DATETIME('"+startDay+"','localtime') and {CallReport:Date_Time_Planned__c}<=DATETIME('"+endDay+"','localtime')", this.defaultPageSize);
      this.smartStore().runSmartQuery(querySpec, function(cursor) { 
        // result should be [[ n ]] if there are n employees
        console.log("cursor",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });
    });
  }
  callOnetoOne(): Promise<any> {
    console.log('callOnetoOne');
    return new Promise((resolve, reject) => {
      console.log("query","SELECT {callReport:_soup} FROM {callReport} WHERE {callReport:Type__c}='1:1'");
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='1:1'", this.defaultPageSize);
      this.smartStore().runSmartQuery(querySpec, function(cursor) { 
        // result should be [[ n ]] if there are n employees
        console.log("cursor",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });

      // var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', 50);
      // let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      // this.smartStore().querySoup(this.CallOnetoOneName, querySpec, success, reject);
    });
  }
  callOnetoOneTomorrow(): Promise<any> {
    console.log('callOnetoOneTomorrow');
    return new Promise((resolve, reject) => {
      var date = new Date()
      date.setDate(date.getDate() + 1)
      let startDay = this.originalStartOfDate(date);
      let endDay = this.originalEndOfDate(date)
      console.log("callOnetoOneTomorrow startDay",startDay);
      console.log("callOnetoOneTomorrow endDay",endDay);
      console.log("query","SELECT {callReport:_soup} FROM {callReport} WHERE {callReport:Type__c}='1:1' and {callReport:Date_Time_Actual__c}>= DATETIME('"+startDay+"','localtime') and {callReport:Date_Time_Actual__c}<=DATETIME('"+endDay+"','localtime')");
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='1:1' and {CallReport:Date_Time_Actual__c}>= DATETIME('"+startDay+"','localtime') and {CallReport:Date_Time_Actual__c}<=DATETIME('"+endDay+"','localtime')", this.defaultPageSize);
      this.smartStore().runSmartQuery(querySpec, function(cursor) { 
        // result should be [[ n ]] if there are n employees
        console.log("cursor",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });
    });
  }
  callOnetoOneToday(): Promise<any> {
    console.log('callOnetoOneToday');
    return new Promise((resolve, reject) => {
      let startDay = this.originalStartOfDate(new Date());
      let endDay = this.originalEndOfDate(new Date())
      console.log("callOnetoOneTomorrow startDay",startDay);
      console.log("callOnetoOneTomorrow endDay",endDay);
      console.log("query","SELECT {callReport:_soup} FROM {callReport} WHERE {callReport:Type__c}='1:1' and {callReport:Date_Time_Actual__c}>= DATETIME('"+startDay+"','localtime') and {callReport:Date_Time_Actual__c}<=DATETIME('"+endDay+"','localtime')");
      var querySpec = this.smartStore().buildSmartQuerySpec(
        "SELECT {CallReport:_soup} FROM {CallReport} WHERE {CallReport:Type__c}='1:1' and {CallReport:Date_Time_Actual__c}>= DATETIME('"+startDay+"','localtime') and {CallReport:Date_Time_Actual__c}<=DATETIME('"+endDay+"','localtime')", this.defaultPageSize);
      this.smartStore().runSmartQuery(querySpec, function(cursor) { 
        // result should be [[ n ]] if there are n employees
        console.log("cursor",cursor);
        resolve({ records: cursor.currentPageOrderedEntries });
      });
    });
  }


  originalStartOfDate(date:Date) {
    let startOfDay = moment(date)
    startOfDay.hours(0)
    startOfDay.minutes(0)
    startOfDay.seconds(0)
    startOfDay.milliseconds(0)
    console.log("formattedStartDate",startOfDay.format("YYYY-MM-DDTHH:mm:ss.SSS"))
    return startOfDay.format("YYYY-MM-DDTHH:mm:ss.SSS")
  }

  originalDateTime(date:Date)
  {
    let startOfDay = moment(date)
    startOfDay.hours()
    startOfDay.minutes()
    startOfDay.seconds()
    startOfDay.milliseconds()
    console.log("formatted originalDateTime",startOfDay.format("YYYY-MM-DDTHH:mm:ss.SSS"))
    return startOfDay.format("YYYY-MM-DDTHH:mm:ss.SSS")
  }

  
  originalEndOfDate(date:Date) {
    let endOfDay = moment(date)
    endOfDay.hours(23)
    endOfDay.minutes(59)
    endOfDay.seconds(59)
    endOfDay.milliseconds(999)
    console.log("formattedStartDate",endOfDay.format("YYYY-MM-DDTHH:mm:ss.SSS"))
    return endOfDay.format("YYYY-MM-DDTHH:mm:ss.SSS")
  }

  loadPharmaEvent(): Promise<any> {
    console.log('loadPharmaEvent');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Id', 'ascending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.PharmaEvent, querySpec, success, reject);
    });
  }

  loadCallReports(): Promise<any> {
    console.log('loadCallReports');
    return new Promise((resolve, reject) => {
      var querySpec = this.smartStore().buildAllQuerySpec('Date_Time_Planned__c', 'descending', this.defaultPageSize);
      let success = (results) => resolve({ records: results.currentPageOrderedEntries });
      
      this.smartStore().querySoup(Constants.CallReport, querySpec, success, reject);
    });
  }

  getEntityByIdorSoupEntryId(soupName:string,Id:any,_soupEntryId:any)
  {
    console.log("soupName",soupName);
    console.log("Id",Id);
    console.log("_soupEntryId",_soupEntryId);
    let promise: Promise<{ records: any[] }> = new Promise(
      (resolve, reject) => {
        var smartSql =  "select {"+soupName+":_soup} from {"+soupName+"} where {"+soupName+":Id}='"+Id+"' or {"+soupName+":_soupEntryId}="+_soupEntryId;
        // var pageSize = 10;
        console.log("smartSql",smartSql);
        var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,1);
        let success = results => {
          console.log("InfieldActivitiesContactdetails",results);
          resolve({ records: results.currentPageOrderedEntries });
        };
        this.smartStore().runSmartQuery(
          querySpec,
          success,
          reject
          );

      }
      );
    return promise;
  }

  getPharmaBasedAttendees(Id:any,_soupEntryId:any)
  {
    let promise: Promise<{ records: any[] }> = new Promise(
      (resolve, reject) => {
        var smartSql =  "select {"+Constants.PEAttendee+":_soup} from {"+Constants.PEAttendee+"} where {"+Constants.PEAttendee+":Pharma_Event__c}='"+Id+"' or {"+Constants.PEAttendee+":Pharma_Event__c}='"+_soupEntryId+"'";
        // var pageSize = 10;
        console.log("smartSql",smartSql);
        var querySpec = this.smartStore().buildSmartQuerySpec(smartSql,this.defaultPageSize);
        let success = results => {
          console.log("getPharmaBasedAttendees",results);
          resolve({ records: results.currentPageOrderedEntries });
        };
        this.smartStore().runSmartQuery(
          querySpec,
          success,
          reject
          );
      });
    return promise;
  }



}
