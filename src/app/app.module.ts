import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler, Platform, Config} from 'ionic-angular';
import { MyApp } from './app.component';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { OrderListPage } from '../pages/order-management/order-list/order-list';
import { OrderCardPage } from '../pages/order-management/order-card/order-card';
import { OrderCardCreatePage } from '../pages/order-management/order-card-create/order-card-create';
import { OrderCardCreateSamplePage } from '../pages/order-management/order-card-create-sample/order-card-create-sample';
import { OrderAccountsPage } from '../pages/order-management/order-accounts/order-accounts';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { HelpdeskPage } from '../pages/helpdesk/helpdesk';
import { ActivitiesPage } from '../pages/activities/activities';
import { OrganizationPage } from'../pages/organization/organization';
import { MtpPage } from '../pages/mtp/mtp';
import { TotPage } from '../pages/tot/tot';
import { MedicalQueryPage } from '../pages/medical-query/medical-query';
import { CalendarPage } from '../pages/calendar/calendar';
import { IndiaActivitiesPage } from '../pages/india-activities/india-activities';

import { SurveyPage } from '../pages/survey/survey';
import { CallReportPage } from '../pages/call-report/call-report';
import { ConvertCallReportPage } from '../pages/convert-call-report/convert-call-report';
import { EditCallReportPage } from '../pages/edit-call-report/edit-call-report';
import { CallReportDetailsPage } from '../pages/call-report-details/call-report-details';
import { TotdetailsPage } from '../pages/totdetails/totdetails';
import { PharmadetailsPage } from '../pages/pharmadetails/pharmadetails';
import { ContactdetailsPage } from '../pages/contactdetails/contactdetails';
import { AppointmentPage } from '../pages/appointment/appointment'
import { OrganizationdetailsPage } from '../pages/organizationdetails/organizationdetails';
import { AppointmentDetailsPage } from '../pages/appointment-details/appointment-details';
import { IndiaConvertCallReportPage } from '../pages/india-convert-call-report/india-convert-call-report';
import { IndiaAppointmentDetailsPage  } from '../pages/india-appointment-details/india-appointment-details';
import { IndiaCallreportDetailsPage  } from '../pages/india-callreport-details/india-callreport-details';
import { AddpharmaeventPage } from '../pages/addpharmaevent/addpharmaevent';
import { MtpdetailsPage } from '../pages/mtpdetails/mtpdetails';
import { PopoverComponent } from '../components/popover/popover';
import { FilterPanelComponent } from '../components/filter/filter-panel/filter-panel';
import { AddtotPage } from '../pages/addtot/addtot';
import { AddMedicalQueryPage } from '../pages/add-medical-query/add-medical-query'
import { MedicalQueryDetailsPage } from '../pages/medical-query-details/medical-query-details';
import { EditAppointmentPage } from '../pages/edit-appointment/edit-appointment';
import { SurveydetailsPage } from '../pages/surveydetails/surveydetails';
import { AddRedFlagPage } from '../pages/add-red-flag/add-red-flag';
import { PatchPage } from '../pages/patch/patch';
import { PatchdetailsPage } from '../pages/patchdetails/patchdetails';
import { SignaturePage } from '../pages/signature/signature';
import { DataChangeRequestsPage } from '../pages/data-change-requests/data-change-requests';
import { OrganizationEditCardPage } from '../pages/organization-edit-card/organization-edit-card';
import { OrganizationChinaEditCardPage } from '../pages/organization-china-edit-card/organization-china-edit-card';
import { SingleUserListPage } from '../pages/single-user-list/single-user-list';
import { PowerscorecardPage } from '../pages/powerscorecard/powerscorecard';
import { DoctorconsentsPage } from '../pages/doctorconsents/doctorconsents';
import { DoctorconsentsDetailsPage } from '../pages/doctorconsents-details/doctorconsents-details';
import { IndiaAppointmentPage } from '../pages/india-appointment/india-appointment';
import { PowerscorecardDetailsPage } from '../pages/powerscorecard-details/powerscorecard-details';

import { FormsModule } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { ProgressBarComponent } from '../components/progress-bar/progress-bar';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SmartstoreServiceProvider } from '../providers/smartstore-service/smartstore-service';
import { ConfirmationDialogService } from '../pages/calendar/confirmation-dialog/confirmation-dialog.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { IonicSelectableModule } from 'ionic-selectable';
import { SyncPopupComponent } from '../components/sync-popup/sync-popup.component';
import { DatabaseManager } from '../services/db/DatabaseManager';
import { SyncMap } from './../services/synchronisation/SyncMap';
import { SyncManager } from './../services/synchronisation/SyncManager';
import { DeviceManager } from './../services/common/DeviceManager';
import { LogManager } from './../services/common/LogManager';
import { EmailManager } from './../services/common/EmailManager';
import { FileService } from './../services/common/FileService';
import { LockManager } from './../services/common/LockManager';
import { PinManager } from './../services/common/PinManager';
import { EmailSender } from './../pages/helpdesk/component/email-sender/email-sender';

import { AttachmentLoadManager } from './../services/common/attachments/AttachmentLoadManager';
import { AttachmentFileManager } from './../services/common/attachments/AttachmentFileManager';
import { AttachmentsManager } from './../services/common/attachments/AttachmentsManager';

import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { EmailComposer } from '@ionic-native/email-composer';
import { Camera } from '@ionic-native/camera';

import { FaqComponent } from './../pages/helpdesk/component/faq/faq.component';
import { LastLogComponent } from './../pages/helpdesk/component/last-log/last-log.component';
import { FaqService } from './../services/modules/faq';
import { LocalizationManager } from './../services/common/localizations/LocalizationManager';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { SelectLanguagePopup } from './../components/popups/SelectLanguagePopup';
import { CustomLoader } from './../services/common/localizations/CustomLoader';
import { PinConfirmComponent } from '../components/pin/pin-confirm/pin-confirm';
import { PinComponent } from '../components/pin/pin/pin';
import { BottomMenuComponent } from "../components/bottom-menu/bottom-menu";
import {MediaPage} from '../pages/media/media.page';
import { ConfirmationDialogComponent } from '../pages/calendar/confirmation-dialog/confirmation-dialog.component';
import {PresentationListItemComponent} from "../pages/media/presentation-list-item/presentation-list-item.component";
import { TableHeaderItemComponent } from './../components/table/table-header-item/table-header-item';
import { TableHeaderComponent } from './../components/table/table-header/table-header';
import { LazyTableComponent } from './../components/table/lazy-table/lazy-table';
import { TableBodyComponent } from './../components/table/table-body/table-body';
import { TableRowComponent } from './../components/table/table-row/table-row';

import {TradeModuleComponent} from './../components/trade-module/trade-module';

import { FilterPanelDateComponent } from './../components/filter/filter-panel-date/filter-panel-date';
import { FilterPanelDateRangeComponent } from './../components/filter/filter-panel-date-range/filter-panel-date-range';
import { FilterPanelInputComponent } from './../components/filter/filter-panel-input/filter-panel-input';
import { FilterPanelPicklistComponent } from './../components/filter/filter-panel-picklist/filter-panel-picklist';
import { FilterPanelCheckboxComponent } from './../components/filter/filter-panel-checkbox/filter-panel-checkbox';

import { OrderItemListComponent } from './../components/order-management/order-item-list/order-item-list';
import { OrderSampleItemListComponent } from './../components/order-management/order-sample-item-list/order-sample-item-list';
import { CreateOrderLinePopupComponent } from './../components/order-management/create-order-line-popup/create-order-line-popup';
import { ReferenceSelectPopupComponent } from './../components/reference-select-popup/reference-select-popup';

import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { SignaturePadModule } from 'angular2-signaturepad';

import {DynamicAgendaPage} from "../pages/dynamic-agenda/dynamic-agenda";
import {ContactEditCardPage} from "../pages/contact-edit-card/contact-edit-card";
import {ContactChinaEditCardPage} from "../pages/contact-china-edit-card/contact-china-edit-card";
import {ScenarioDetailsPage} from "../pages/scenario-details/scenario-details";
import {DataChangeRequestsContactCardPage} from "../pages/data-change-requests-contact-card/data-change-requests-contact-card";
import {DataChangeRequestsOrganizationCardPage} from "../pages/data-change-requests-organization-card/data-change-requests-organization-card";
import {DataChangeRequestsReferenceCardPage} from "../pages/data-change-requests-reference-card/data-change-requests-reference-card";
import {ReferenceEditCardPage} from "../pages/reference-edit-card/reference-edit-card";
import {ContactListSelectPage} from "../pages/contact-list-select/contact-list-select";
import {DataChangeRequestsSegmentationListPage} from "../pages/data-change-requests-segmentation-list/data-change-requests-segmentation-list";
import {SegmentationEditCardPage} from "../pages/segmentation-edit-card/segmentation-edit-card";
import {DataChangeRequestsSegmentationCardPage} from "../pages/data-change-requests-segmentation-card/data-change-requests-segmentation-card";
import {ScenarioDetailsPageModule} from "../pages/scenario-details/scenario-details.module";

import {FadeIn} from "../utils/animation/FadeIn";
import {FadeOut} from "../utils/animation/FadeOut";

// Pipes started here
import { SearchPipe } from '../pipes/search/search';
import { SortPipe } from '../pipes/sort/sort';
import { UniquePipe } from '../pipes/unique/unique';
import { SelectboxPipe } from '../pipes/selectbox/selectbox';
import { SafeSrcPipe } from '../pipes/safe/safe-src';
import { DatePipe } from '@angular/common';


/*     Collections start    */

import { DeviceCollection } from './../collections/DeviceCollection';
import { OrganizationsCollection } from './../collections/OrganizationsCollection';
import { PresentationsCollection } from './../collections/PresentationsCollection';
import { ProductPresentationsCollection } from './../collections/ProductPresentationsCollection';
import { ProductsCollection } from './../collections/ProductsCollection';
import { UsersCollection } from './../collections/UsersCollection';
import { FAQAttachmentCollection } from './../collections/FaqAttachmentsCollection';
import { FAQCollection } from './../collections/FaqCollection';
import { IqviaAccountSkuTasksCollection } from './../collections/IqviaAccountSkuTasksCollection';
import { IqviaQuestionsCollection } from './../collections/IqviaQuestionsCollection';
import { IqviaTaskAdjustmentsCollection } from './../collections/IqviaTaskAdjustmentsCollection';
import { ReferenceCollection } from './../collections/references/ReferenceCollection';
import { NonTargetReferencesCollection } from './../collections/references/NonTargetReferencesCollection';
import { TargetReferencesCollection } from './../collections/references/TargetReferencesCollection';
import { SurveyCollection } from './../collections/SurveyCollection';
import { SurveyQuestionnaireCollection } from './../collections/SurveyQuestionnaireCollection';
import { SurveyQuestionnaireDetailCollection } from './../collections/SurveyQuestionnaireDetailCollection';
import { ContactsCollection } from './../collections/ContactsCollection';
import { TargetFrequenciesCollection } from './../collections/TargetFrequenciesCollection';
import { MarketingCyclesCollection } from './../collections/MarketingCyclesCollection';
import { CoachingUsersCollection } from './../collections/CoachingUsersCollection';
import { PatchOrganizationCollection } from './../collections/PatchOrganizationCollection';

import {BricksCollection} from './../collections/BricksCollection';
import { CoachingPlanCollection } from './../collections/CoachingPlanCollection';
import { JointVisitPlanCollection } from './../collections/JointVisitPlanCollection';
import {BuTeamPersonProfilesCollection} from './../collections/BuTeamPersonProfilesCollection';
import {DoctorConsentCollection} from './../collections/DoctorConsentCollection';
import {DocumentCollection} from './../collections/DocumentCollection';
import {LocationCollection} from './../collections/LocationCollection';


// import {MarketingCyclesCollection} from './../collections/MarketingCyclesCollection';
import {MarketingMessagesCollection} from './../collections/MarketingMessagesCollection';
import {MedicalCommentsCollection} from './../collections/MedicalCommentsCollection';
import {MedicalQueriesCollection} from './../collections/MedicalQueriesCollection/MedicalQueriesCollection';
import {MTPCollection} from './../collections/MTPCollection';
// import {PEAbbottAttendeesCollection} from './../collections/PEAbbottAttendeesCollection';
import {PEAttendeesCollection} from './../collections/PEAttendeesCollection';
// import {PEEventExpenseCollection} from './../collections/PEEventExpenseCollection';
import {PharmaEventsCollection} from './../collections/PharmaEventsCollection';
// import {ProductInPortfoliosCollection} from './../collections/ProductInPortfoliosCollection';
import {ProductItemsCollection} from './../collections/ProductItemsCollection';
// import {ProductListingHistoriesCollection} from './../collections/ProductListingHistoriesCollection';
// import {ProductSegmentationsHistoriesCollection} from './../collections/ProductSegmentationsHistoriesCollection';
// import {ProductSegmentationsCollection} from './../collections/ProductSegmentationsCollection';
import {RedFlagCollection} from './../collections/RedFlagCollection';
import {PatchCollection} from './../collections/PatchCollection';
// //import {TargetFrequenciesCollection} from '../../collections/TargetFrequenciesCollection';
// import {TargetCollection} from './../collections/TargetCollection';
import {TotsCollection} from './../collections/tots-collection/tots-collection';
import {TotsClosedCollection} from './../collections/tots-collection/tots-closed-collection';
import {TotsOpenCollection} from './../collections/tots-collection/tots-open-collection';
import {TotsSubmitCollection} from './../collections/tots-collection/tots-submit-collection';


import {ClosedQueries} from "./../collections/MedicalQueriesCollection/ClosedQueries";
import {DraftQueries} from "./../collections/MedicalQueriesCollection/DraftQueries";

import {QueriesSubmittedToMedical} from "./../collections/MedicalQueriesCollection/QueriesSubmittedToMedical";
import {QueriesSubmittedToRep} from "./../collections/MedicalQueriesCollection/QueriesSubmittedToRep";
import {OrganizationListSelectPage} from "./../pages/organization-list-select/organization-list-select";
import { SalesPlanningListDetailsPage } from "./../pages/sales-planning-list-details/sales-planning-list-details";
import {Autoresize} from "./../directives/autoresize";

import {CallReportsCollection} from './../collections/CallReportsCollection/CallReportsCollection';
import {CallsCollection} from './../collections/CallReportsCollection/CallsCollection';
import {CallsTodayCollection} from './../collections/CallReportsCollection/CallsTodayCollection';
import { AppointmentsCollection } from './../collections/CallReportsCollection/AppointmentsCollection';
import { AppointmentsPastCollection } from './../collections/CallReportsCollection/AppointmentsPastCollection';
import { AppointmentsTodayCollection } from './../collections/CallReportsCollection/AppointmentsTodayCollection';
import { AppointmentsTomorrowCollection } from './../collections/CallReportsCollection/AppointmentsTomorrowCollection';
import { PatchCustomerCollection } from './../collections/PatchCustomerCollection';

import { RecordTypeCollection } from './../collections/RecordTypeCollection';
import { SalesPlanningCollection } from './../collections/SalesPlanningCollection';
import { SalesPlanningListCollection } from './../collections/SalesPlanningListCollection';
import { SalesPlanningProductCollection } from './../collections/SalesPlanningProductCollection';
import { PatchCustomerReferenceCollection } from './../collections/PatchCustomerReferenceCollection';
/*      Collections end     */


/*     Picklist Managers start    */

import { OrderPicklistManager } from '../services/db/picklist-managers/OrderPicklistManager';
import { OrderStatusPickListDatasource } from '../services/db/picklist-managers/datasource/OrderStatusPicklistDatasource';
import {AttendeesListSelectPage} from "../pages/attendees-list-select/attendees-list-select";
import {ProductListSelectPage} from "../pages/product-list-select/product-list-select";
//import {UserListSingleSelectPage} from "../pages/user-list-single-select/user-list-single-select";
import {UserListMultiSelectPage} from "../pages/user-list-multi-select/user-list-multi-select";
import { BrickPickListManager } from '../services/db/picklist-managers/BrickPicklistManager';
import { PoweScorecardPicklistManager } from '../services/db/picklist-managers/PoweScorecardPicklistManager';
import { BrickPickListDatasource } from '../services/db/picklist-managers/datasource/BrickPicklistDatasource';
import { ContactSpecialtiesPriorityPickListManager } from '../services/db/picklist-managers/ContactSpecialtiesPriorityPicklistManager';
import { PriorityPickListDatasource } from '../services/db/picklist-managers/datasource/PriorityPicklistDatasource';
import { SpecialtyPickListDatasource } from '../services/db/picklist-managers/datasource/SpecialtyPicklistDatasource';
import { SubtypePickListDatasource } from '../services/db/picklist-managers/datasource/SubtypePicklistDatasource';
import { TOTPicklistManager } from '../services/db/picklist-managers/TOTPicklistManager';
import { TOTPickListDatasource } from '../services/db/picklist-managers/datasource/TOTPickListDatasource';
import { TOTEventPickListDatasource } from '../services/db/picklist-managers/datasource/TOTEventPickListDatasource';
import { PEPicklistManager } from '../services/db/picklist-managers/PEPicklistManager';
import { PEDiscussionTypeDatasource } from '../services/db/picklist-managers/datasource/PEDiscussionTypeDatasource';
import { PEStagePickListDatasource } from '../services/db/picklist-managers/datasource/PEStagePickListDatasource';
import { PETypeEventPickListDatasource } from '../services/db/picklist-managers/datasource/PETypeEventPickListDatasource';
import { CallReportPicklistManager } from '../services/db/picklist-managers/CallReportPicklistManager';
import { CallReportStatusPickListDatasource } from '../services/db/picklist-managers/datasource/CallReportStatusPickListDatasource';
import { CallReportTypePickListDatasource } from '../services/db/picklist-managers/datasource/CallReportTypePickListDatasource';
import { CallReportCoachingVisitPickListDatasource } from '../services/db/picklist-managers/datasource/CallReportCoachingVisitPickListDatasource';
import { CallReportPrio1ReactionsPickListDatasource } from '../services/db/picklist-managers/datasource/CallReportPrio1ReactionsPickListDatasource';
import { CallReportTypeOfVisitPickListDatasource } from '../services/db/picklist-managers/datasource/CallReportTypeOfVisitPickListDatasource';
import { MQPickListManager } from '../services/db/picklist-managers/MQPickListManager';
import { MQPickListDatasource } from '../services/db/picklist-managers/datasource/MQPickListDatasource';
import { PatchRecordTypePickListManager } from '../services/db/picklist-managers/PatchRecordTypePickListManager';
import { PatchPicklistDatasource } from '../services/db/picklist-managers/datasource/PatchPicklistDatasource';
import { PatchRecordTypePicklistDatasource } from '../services/db/picklist-managers/datasource/PatchRecordTypePicklistDatasource';
import {BrandMatrixPageModule} from "../pages/brand-matrix/brand-matrix.module";
import { PoweScorecardMonthPicklisttDatasource } from '../services/db/picklist-managers/datasource/PoweScorecardMonthPicklisttDatasource';
import { PoweScorecardYearPicklisttDatasource } from '../services/db/picklist-managers/datasource/PoweScorecardYearPicklisttDatasource';
import { PoweScorecardDesigPicklisttDatasource } from '../services/db/picklist-managers/datasource/PoweScorecardDesigPicklisttDatasource';
import { CallReportActivityTypePickListDatasource } from '../services/db/picklist-managers/datasource/CallReportActivityTypePickListDatasource';
import {TourPlanPage } from '../pages/tour-plan/tour-plan';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

// Add sales plan module
import { SalesplanningPage } from '../pages/salesplanning/salesplanning';
import { SalesplanmodelPage } from '../pages/salesplanmodel/salesplanmodel';
// import { PowerScorecardPage } from '../pages/power-scorecard/power-scorecard';
import { SalesplanfilterComponent } from '../components/salesplanfilter/salesplanfilter';
import { SalesplanestimatedComponent } from '../components/salesplanestimated/salesplanestimated'

//Lunchon meetings import
import { LuncheonmeetingsPage } from '../pages/luncheonmeetings/luncheonmeetings';

import {ClickOutsideEmitterDirective} from "../directives/ClickOutsideEmitterDirective";
import { AddluncheonmeetingsPage } from '../pages/addluncheonmeetings/addluncheonmeetings';
import { LuncheondetailsPage } from '../pages/luncheondetails/luncheondetails';
import { SalesplanpopoverComponent } from '../components/salesplanpopover/salesplanpopover';
/*      Picklist Managers end     */

/*      Picklist Managers end     */

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    OrderListPage,
    OrderCardPage,
    OrderCardCreatePage,
    OrderCardCreateSamplePage,
    OrderAccountsPage,
    HomePage,
    TabsPage,
    TourPlanPage,
    HelpdeskPage,
    DynamicAgendaPage,
    AttendeesListSelectPage,
    ProductListSelectPage,
  //  UserListSingleSelectPage,
    UserListMultiSelectPage,
    ProgressBarComponent,
    ActivitiesPage,
    OrganizationPage,
    MtpPage,
    TotPage,
    MediaPage,
    MedicalQueryPage,
    CalendarPage,
    IndiaActivitiesPage,
    SurveyPage,
    CallReportPage,
    ConvertCallReportPage,
    IndiaConvertCallReportPage,
    EditCallReportPage,
    CallReportDetailsPage,
    TotdetailsPage,
    PharmadetailsPage,
    SearchPipe,SortPipe,UniquePipe,SelectboxPipe,
    SafeSrcPipe,
    ConfirmationDialogComponent,
    ContactdetailsPage,
    AppointmentPage,
    OrganizationdetailsPage,
    AppointmentDetailsPage,
    IndiaAppointmentDetailsPage,
    IndiaCallreportDetailsPage,
    AddpharmaeventPage,
    MtpdetailsPage,
    PopoverComponent,
    FilterPanelComponent,
    FilterPanelDateComponent,
    FilterPanelDateRangeComponent,
    FilterPanelInputComponent,
    FilterPanelPicklistComponent,
    FilterPanelCheckboxComponent,
    AddtotPage,
    SignaturePage,
    IndiaAppointmentPage,
    AddMedicalQueryPage,
    MedicalQueryDetailsPage,
    SurveydetailsPage,
    SyncPopupComponent,
    FaqComponent,
    LastLogComponent,
    PresentationListItemComponent,
    BottomMenuComponent,
    PinConfirmComponent,
    PinComponent,
    TableHeaderItemComponent,
    TableHeaderComponent,
    LazyTableComponent,
    TableBodyComponent,
    OrderSampleItemListComponent,
    CreateOrderLinePopupComponent,
    ReferenceSelectPopupComponent,
    OrderItemListComponent,
    TableRowComponent,
    ClickOutsideEmitterDirective,
    Autoresize,
    AddRedFlagPage,
    PatchdetailsPage,
    TradeModuleComponent,
    PatchPage,
    SingleUserListPage,
    SalesplanningPage,
    // PowerScorecardPage,
    SalesplanmodelPage,
    SalesplanfilterComponent,
    SalesplanestimatedComponent,
    PowerscorecardPage,
    DoctorconsentsPage,
    DoctorconsentsDetailsPage,
    PowerscorecardDetailsPage,
    DataChangeRequestsPage,
    ContactEditCardPage,
    ContactChinaEditCardPage,
    DataChangeRequestsContactCardPage,
    OrganizationListSelectPage,
    SalesPlanningListDetailsPage,
    OrganizationEditCardPage,
    OrganizationChinaEditCardPage,
    DataChangeRequestsOrganizationCardPage,
    DataChangeRequestsReferenceCardPage,
    ReferenceEditCardPage,
    ContactListSelectPage,
    DataChangeRequestsSegmentationListPage,
    SegmentationEditCardPage,
    DataChangeRequestsSegmentationCardPage,
    LuncheonmeetingsPage,
    AddluncheonmeetingsPage,
    LuncheondetailsPage,
    SalesplanpopoverComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false,
      swipeBackEnabled: false,
      mode: 'ios'
    }),
    IonicStorageModule.forRoot(),
    NgxDatatableModule,
    NgbModalModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CustomLoader,
        deps: [HttpClient, Platform, FileService]
      }
    }),
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule,
    IonicSelectableModule,
    FormsModule,
    ScenarioDetailsPageModule,
    VirtualScrollerModule,
    SignaturePadModule,
    AngularMultiSelectModule,
    SignaturePadModule,
    BrandMatrixPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    OrderListPage,
    OrderCardPage,
    OrderCardCreatePage,
    OrderCardCreateSamplePage,
    OrderAccountsPage,
    HomePage,
    TabsPage,
    HelpdeskPage,
    DynamicAgendaPage,
    AttendeesListSelectPage,
    ProductListSelectPage,
    //UserListSingleSelectPage,
    UserListMultiSelectPage,
    ActivitiesPage,
    OrganizationPage,
    MtpPage,
    TotPage,
    TourPlanPage,
    MediaPage,
    MedicalQueryPage,
    CalendarPage,
    IndiaActivitiesPage,
    SurveyPage,
    CallReportPage,
    ConvertCallReportPage,
    IndiaConvertCallReportPage,
    CallReportDetailsPage,
    EditCallReportPage,
    TotdetailsPage,
    PharmadetailsPage,
    ConfirmationDialogComponent,
    ContactdetailsPage,
    AppointmentPage,
    IndiaAppointmentPage,
    OrganizationdetailsPage,
    AppointmentDetailsPage,
    IndiaAppointmentDetailsPage,
    IndiaCallreportDetailsPage,
    AddpharmaeventPage,
    MtpdetailsPage,
    PopoverComponent,
    FilterPanelComponent,
    FilterPanelDateComponent,
    FilterPanelDateRangeComponent,
    FilterPanelInputComponent,
    FilterPanelPicklistComponent,
    FilterPanelCheckboxComponent,
    AddtotPage,
    SignaturePage,
    AddMedicalQueryPage,
    MedicalQueryDetailsPage,
    SurveydetailsPage,
    SyncPopupComponent,
    FaqComponent,
    LastLogComponent,
    PresentationListItemComponent,
    BottomMenuComponent,
    PinConfirmComponent,
    PinComponent,
    TableHeaderItemComponent,
    TableHeaderComponent,
    LazyTableComponent,
    TableBodyComponent,
    OrderSampleItemListComponent,
    CreateOrderLinePopupComponent,
    ReferenceSelectPopupComponent,
    OrderItemListComponent,
    TableRowComponent,
    AddRedFlagPage,
    PatchdetailsPage,
    PatchPage,
    SingleUserListPage,
    SalesplanningPage,
    // PowerScorecardPage,
    SalesplanmodelPage,
    SalesplanfilterComponent,
    SalesplanestimatedComponent,
    PowerscorecardPage,
    DoctorconsentsPage,
    DoctorconsentsDetailsPage,
    PowerscorecardDetailsPage,
    DataChangeRequestsPage,
    ContactEditCardPage,
    ContactChinaEditCardPage,
    DataChangeRequestsContactCardPage,
    OrganizationListSelectPage,
    SalesPlanningListDetailsPage,
    OrganizationEditCardPage,
    OrganizationChinaEditCardPage,
    DataChangeRequestsOrganizationCardPage,
    DataChangeRequestsReferenceCardPage,
    ReferenceEditCardPage,
    ContactListSelectPage,
    DataChangeRequestsSegmentationListPage,
    SegmentationEditCardPage,
    DataChangeRequestsSegmentationCardPage,
    LuncheonmeetingsPage,
    AddluncheonmeetingsPage,
    LuncheondetailsPage,
    SalesplanpopoverComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SmartstoreServiceProvider,
    ConfirmationDialogService,
    DatePipe,
    OrderPicklistManager,
    TOTPicklistManager,
    PEPicklistManager,
    PEDiscussionTypeDatasource,
    PEStagePickListDatasource,
    PETypeEventPickListDatasource,
    CallReportPicklistManager,
    CallReportStatusPickListDatasource,
    CallReportTypePickListDatasource,
    CallReportCoachingVisitPickListDatasource,
    CallReportPrio1ReactionsPickListDatasource,
    CallReportTypeOfVisitPickListDatasource,
    BrickPickListManager,
    PoweScorecardPicklistManager,
    ContactSpecialtiesPriorityPickListManager,
    DatabaseManager,
    SyncMap,
    SyncManager,
    DeviceManager,
    LogManager,
    EmailManager,
    Camera,
    File,
    HttpClient,
    FileTransfer,
    FileService,
    AttachmentLoadManager,
    AttachmentFileManager,
    AttachmentsManager,
    EmailComposer,
    DeviceCollection,
    SurveyCollection,
    SurveyQuestionnaireCollection,
    SurveyQuestionnaireDetailCollection,
    PresentationsCollection,
    OrganizationsCollection,
    ProductPresentationsCollection,
    ProductsCollection,
    UsersCollection,
    FAQAttachmentCollection,
    FAQCollection,
    IqviaAccountSkuTasksCollection,
    IqviaQuestionsCollection,
    IqviaTaskAdjustmentsCollection,
    ReferenceCollection,
    PatchCustomerReferenceCollection,
    NonTargetReferencesCollection,
    TargetReferencesCollection,
    ContactsCollection,
    TargetFrequenciesCollection,
    MarketingCyclesCollection,
    BricksCollection,
    CoachingPlanCollection,
    JointVisitPlanCollection,
    CallReportsCollection,
    CallsCollection,
    CallsTodayCollection,
    TotsCollection,
    TotsClosedCollection,
    TotsOpenCollection,
    TotsSubmitCollection,
    BuTeamPersonProfilesCollection,
    DoctorConsentCollection,
    DocumentCollection,
    LocationCollection,
    PatchCollection,
    PatchCustomerCollection,
    ClosedQueries,
    DraftQueries,
    QueriesSubmittedToMedical,
    QueriesSubmittedToRep,
    // MarketingCyclesCollection,
    MarketingMessagesCollection,
    MedicalCommentsCollection,
    MedicalQueriesCollection,
    MTPCollection,
    // PEAbbottAttendeesCollection,
    PEAttendeesCollection,
    // PEEventExpenseCollection,
    PharmaEventsCollection,
    // ProductInPortfoliosCollection,
    ProductItemsCollection,
    // ProductListingHistoriesCollection,
    // ProductSegmentationsHistoriesCollection,
    // ProductSegmentationsCollection,
    RedFlagCollection,
    PatchOrganizationCollection,
    LockManager,
    PinManager,
    FaqService,
    LocalizationManager,
    SelectLanguagePopup,
    CustomLoader,
    EmailSender,
    OrderStatusPickListDatasource,
    BrickPickListDatasource,
    PriorityPickListDatasource,
    SpecialtyPickListDatasource,
    SubtypePickListDatasource,
    TOTPickListDatasource,
    TOTEventPickListDatasource,
    AppointmentsCollection,
    AppointmentsPastCollection,
    AppointmentsTodayCollection,
    AppointmentsTomorrowCollection,
    AppointmentsTomorrowCollection,
    MQPickListManager,
    MQPickListDatasource,
    PatchRecordTypePickListManager,
    PatchPicklistDatasource,
    PatchRecordTypePicklistDatasource,
    CallReportActivityTypePickListDatasource,
    CoachingUsersCollection,
    SalesPlanningCollection,
    RecordTypeCollection,
    PoweScorecardMonthPicklisttDatasource,
    PoweScorecardYearPicklisttDatasource,
    PoweScorecardDesigPicklisttDatasource
  ]
})
export class AppModule {
  constructor(private config: Config) {
    this.setTransitionAnimation();
  }

  private setTransitionAnimation() {
    this.config.setTransition('modal-fade-in', FadeIn);
    this.config.setTransition('modal-fade-out', FadeOut);
  }
}
