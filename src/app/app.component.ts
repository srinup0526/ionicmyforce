import { Component, ViewChild } from '@angular/core';
import {Platform, Nav, Events} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { OAuth, DataService } from 'forcejs';
import { Storage } from '@ionic/storage';
import { DeviceManager } from '../services/common/DeviceManager';
declare var cordova: any;
declare var $:any;
declare var getForceSyncClient:any;
declare var forceJsClient:any;
declare var ForceStoreCache:any;
declare var window: any;

import { DatabaseManager } from './../services/db/DatabaseManager';
import { PinManager } from './../services/common/PinManager';
import { LockManager } from './../services/common/LockManager';
import { Settings } from './../services/db/Settings';
import { PresentationFileManager } from '../pages/media/PresentationFileManager';
import { LocalizationManager } from "../services/common/localizations/LocalizationManager";
import { BottomMenuComponent } from "../components/bottom-menu/bottom-menu";
import { PinConfirmComponent } from "../components/pin/pin-confirm/pin-confirm";
import { PinComponent } from "../components/pin/pin/pin";
import { PresentationLoader } from "../pages/media/PresentationLoader";
import { StoreConfig } from "../services/synchronisation/StoreConfig";

const SalesforceSDKInfo = cordova.require('com.salesforce.plugin.sdkinfo');


@Component({
  templateUrl: 'app.html'
})

export class MyApp{
  @ViewChild(BottomMenuComponent) bottomMenu: BottomMenuComponent;
  @ViewChild(PinConfirmComponent) pinConfirm: PinConfirmComponent;
  @ViewChild(PinComponent) pinComponent: PinComponent;
  @ViewChild(Nav) nav: Nav;
  rootPage:any = TabsPage;
  _indexSpec: any = [
  {
    path: "id",
    type:'string'
  }
  ];

  constructor(private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    public  storage:Storage,
    private databaseManager: DatabaseManager,
    private pinManager: PinManager,
    private lockManager: LockManager,
    private presentationFileManager: PresentationFileManager,
    private localizationManager: LocalizationManager,
    private events: Events) {
    this.initializeApp();
  }

  private initializeApp() {
    this.platform.ready().then(() => {
      this.splashScreen.show();
      this.statusBar.styleDefault();
      this.initializeDeviceInfo();
      return this.presentationFileManager.initFileSystem()
      .then(() => PresentationLoader.init(this.presentationFileManager))
      .then(() => this.setupSalesforceCredentials())
      .then(() => this.setupStore())
      .then(() => {
        return this.initializeDatabase()
        .then(() => this.reloadSettings());
      })
      .then(() => this.initializeLocalization())
      .then(() => this.fireLoadDBDoneEvent())
      .then(() => this.showActualScreen())
      .then(() => this.splashScreen.hide())
      .catch((err) => {
        console.error(err);
        this.splashScreen.hide();
      });
    });
  }

  private setupSalesforceCredentials() {
    return new Promise((resolve, reject) => {
      const force = getForceSyncClient();
      force.init();

      force.jsClient.login()
      .then(() => {
        const oauthPlugin = cordova.require('com.salesforce.plugin.oauth');

        oauthPlugin.getAuthCredentials((creds) => {
          force.userId = creds.userId;
          return resolve(creds);
        }, reject);
      }, reject);
    });
  }

  _clearSettings()
  {
    return true;
  }
  _disableTradeModule()
  {
    return true;
  }

  _disablePortfolioModule()
  {
    return true;
  }

  private initializeDatabase() {
    return this.databaseManager.initializeDatabase();
  }

  private reloadSettings() {
    return Settings.getInstance().reload();
  }

  private initializeDeviceInfo(): void {
    SalesforceSDKInfo.getInfo((versionInfo) => {
      DeviceManager.setDevice(window.device);
      DeviceManager.setAppVersion(versionInfo.appVersion);
    });
  }

  private showActualScreen() {
    this.lockManager.init(this.pinComponent);

    this.pinManager.isPinExists()
    .then((isExists) => {
      if (isExists) {
        this.lockManager.isLocked()
        .then((isLocked) => {
          if (isLocked) {
            this.lockManager.lock();
          }
          else {
            this.lockManager.start();

            this.pinConfirm.hide();
          }
        })
      }
      else {
        this.pinConfirm.show();
      }
    })
  }
  
  private setupStore() {
    const force = getForceSyncClient();
    
    StoreConfig.setStoreName(force.userId);
  }

  private initializeLocalization() {
    this.localizationManager.setDevLanguage();
    return this.localizationManager.getLanguageFromSettings()
      .then((lang) => {
        if(lang) {
          return this.localizationManager.setLanguage(lang);
        }
        return this.localizationManager.setDefaultLanguage();
      });
  }

  private fireLoadDBDoneEvent() {
    this.events.publish('common:dataBaseReady', Date.now());
  }
}
