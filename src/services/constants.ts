export const API_ENDPOINT= 'http://localhost:3000/';   
export const CallReport = "CallReport";
export const SampleDelivery = "SampleDelivery";
export const SampleStock = "SampleStock";
export const SurveyQuestionnaireDetail = "SurveyQuestionnaireDetail";
export const SurveyQuestionnaire = "SurveyQuestionnaire";
export const Survey = "Survey";
export const OrderManagement = "OrderManagement";
export const OrderLineItems = "OrderLineItems";
export const MedicalQuery = "MedicalQuery";
export const MedicalComment = "MedicalComment";
export const Contact = "Contact";
export const MarketingCycle = "MarketingCycle";
export const MarketingMessage = "MarketingMessage";
export const Brick = "Brick";
export const Organization = "Organisation";
export const PEAbbottAttendee = "PEAbbottAttendee";
export const PEAttendee = "PEAttendee";
export const PharmaEvent = "PharmaEvent";
export const PEEventExpense = "PEEventExpense";
export const Product = "Product";
export const Reference = "Reference";
export const TargetFrequency = "TargetFrequency";
export const Target = "Target";
export const Tot = "Tot";
export const User = "User";
export const Presentation = "Presentation";
export const Device = "Device";
export const CLMCallReportData = "CLMCallReportData";
export const Scenario = "Scenario";
export const PromotionAccount = "PromotionAccount";
export const PromotionTaskAccount = "PromotionTaskAccount";
export const TaskAdjustment = "TaskAdjustment";
export const ProductItem = "ProductItem";
export const PromotionSku = "PromotionSku";
export const PromotionMechanic = "PromotionMechanic";
export const MechanicAdjustment = "MechanicAdjustment";
export const MechanicEvaluationAccount = "MechanicEvaluationAccount";
export const PhotoAdjustment = "PhotoAdjustment";
export const PromotionNote = "PromotionNote";
export const PromotionAttachment = "PromotionAttachment";
export const PhotoAttachment = "PhotoAttachment";
export const LocalImage = "LocalImage";
export const ProductInPortfolio = "ProductInPortfolio";
export const ProfileProductInPortfolio = "ProfileProductInPortfolio";
export const PatientDisease = "PatientDisease";
export const Location = "Location";
export const ProductPresentation = "ProductPresentation";
export const MTP = "MTP";
export const PrescribingHabit = "PrescribingHabit";
export const SlideConfiguration = "SlideConfiguration";
export const PresentationSlide = "PresentationSlide";
export const DoctorsConsent = "DoctorsConsent";
export const DataChangeRequest = "DataChangeRequest";
export const ProductSegmentation = "ProductSegmentation";
export const ProductSegmentationHistory = "ProductSegmentationHistory";
export const ProductListing = "ProductListing";
export const ProductListingHistory = "ProductListingHistory";
export const RecordType = "RecordType";
export const Document = "Document";
export const Attachment = "Attachment";
export const RedFlag = "RedFlag";
export const FAQ = "FAQ";
export const FaqAttachment = "FaqAttachment";
export const Appointment = "Appointment";
export const oneToone = "1:1";
export const ProductMessage = "ProductMessage";
export const Draft = "Draft";
export const Patch = "Patch";
export const PatchCustomer = "PatchCustomer";

export const Databases = [
	 "BuTeamPersonProfile",
     "CallReport",
     "SampleDelivery",
     "SampleStock",
	 "SurveyQuestionnaireDetail",
	 "SurveyQuestionnaire",
	 "Survey",
	 "OrderManagement",
	 "OrderLineItems",
	 "MedicalQuery",
	 "MedicalComment",
	 "Contact",
	 "MarketingCycle",
	 "MarketingMessage",
	 "Brick",
	 "Organization",
	 "PEAbbottAttendee",
	 "PEAttendee",
	 "PharmaEvent",
	 "PEEventExpense",
	 "Product",
	 "Reference",
	 "TargetFrequency",
	 "Target",
	 "Tot",
	 "User",
	 "Presentation",
	 "Device",
	 "CLMCallReportData",
	 "Scenario",
	 "PromotionAccount",
	 "PromotionTaskAccount",
	 "TaskAdjustment",
	 "ProductItem",
	 "PromotionSku",
	 "PromotionMechanic",
	 "MechanicAdjustment",
	 "MechanicEvaluationAccount",
	 "PhotoAdjustment",
	 "PromotionNote",
	 "PromotionAttachment",
	 "PhotoAttachment",
	 "LocalImage",
	 "ProductInPortfolio",
	 "ProfileProductInPortfolio",
	 "PatientDisease",
	 "Location",
	 "ProductPresentation",
	 "MTP",
	 "PrescribingHabit",
	 "SlideConfiguration",
	 "PresentationSlide",
	 "DoctorsConsent",
	 "DataChangeRequest",
	 "ProductSegmentation",
	 "ProductSegmentationHistory",
	 "ProductListing",
	 "ProductListingHistory",
	 "RecordType",
	 "Document",
	 "Attachment",
	 "RedFlag",
   "FAQ",
   "Patch",
   "PatchCustomer",
	 "FaqAttachment"
];
export const soups = {  "soups": [
    {
      "soupName": "Contact",
      "sfdcObjectName":"Contact",
      "indexes": 
      [
        {"path": "Id","type":"string"},
        {"path": "Name","type":"full_text"},
        {"path": "FirstName","type":"full_text"},
        {"path": "LastName","type":"full_text"},
        {"path": "C_Job_Title__c","type":"string"},
        {"path": "Account.Id","type":"string"},
        {"path": "Account.RecordType.Name","type":"string"},
        {"path": "Person_Type__c","type":"string"},
        {"path": "Account.Status__c","type":"string"},
        {"path": "AccountId","type":"string"},
        {"path": "Gender__c","type":"string"},
        {"path": "Year_of_Graduation__c","type":"string"},
        {"path": "MobilePhone","type":"string"},
        {"path": "HomePhone","type":"string"},
        {"path": "Email","type":"string"},
        {"path": "KOL__c","type":"string"},
        {"path": "Account.C_Specialty_1__c","type":"string"},
        {"path": "Account.Priority__c","type":"string"},
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },
    {
      "soupName": "Organisation",
      "configAvailable":true,
      "sfdcObjectName":"Account",
      "indexes": 
      [
        {"path": "Id","type":"string"},
        {"path": "Name","type":"full_text"},
        {"path": "External_Id__c","type":"string"},
        {"path": "Brick__c","type":"string"},
        {"path": "Status__c","type":"string"},
        {"path": "Subtype__c","type":"string"},
        {"path": "RecordType.Id","type":"string"},
        {"path": "RecordType.Name","type":"string"},
        {"path": "BillingCountry","type":"string"},
        {"path": "BillingCity","type":"string"},
        {"path": "BillingState","type":"string"},
        {"path": "BillingStreet","type":"string"},
        {"path": "BillingPostalCode","type":"string"},
        {"path": "Phone","type":"string"},
        {"path": "C_Specialty_1__c","type":"string"},
        {"path": "C_Specialty_2__c","type":"string"},
        {"path": "IsPersonAccount","type":"string"},
        {"path": "C_Juridic_Group__c","type":"string"},
        {"path": "Priority__c","type":"string"},
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },
    {
      "soupName": "Reference",
      "sfdcObjectName":"Reference__c",
      "indexes": 
      [
        { "path": "Id","type": "string"},
        { "path": "Customer__r.Name","type": "string"},
        { "path": "Customer__c","type": "string"},
        { "path": "Organisation__c","type": "string"},
        { "path": "Customer__r.Account.Id","type": "string"},
        { "path": "Customer__r.Account.RecordType.Name","type": "string"},
        { "path": "Customer__r.Person_Type__c","type": "string"},
        { "path": "Primary__c","type": "string"},
        { "path": "C_Status_Reference__c","type": "string"},
        { "path": "Organisation__r.Name","type": "string"},
        { "path": "Organisation__r.BillingCity","type": "string"},
        { "path": "Organisation__r.Phone","type": "string"},
        { "path": "Organisation__r.BillingCountry","type": "string"},
        { "path": "Organisation__r.BillingStreet","type": "string"},
        { "path": "Organisation__r.Brick__c","type": "string"},
        { "path": "Customer__r.FirstName","type": "string"},
        { "path": "Customer__r.LastName","type": "string"},
        { "path": "Name","type": "full_text"},
        { "path": "Target_Customer__c","type": "string"},
        { "path": "Customer__r.Account.Priority__c","type": "full_text"},
        { "path": "Specialty1__c", "type":"string"},
        { "path": "Org_Brick_Name__c", "type":"string"},
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },
    {
      "soupName": "User",
      "sfdcObjectName":"User",
      "configAvailable":true,
      "indexes": 
      [
        {"path":"ATC_Class__c","type":"string"},
        {"path":"Business_Unit__c","type":"string"},
        {"path":"BU_Area__c","type":"string"},
        {"path":"Country","type":"string"},
        {"path":"Custom_Approver__c","type":"string"},
        {"path":"Custom_Object_Country__c","type":"string"},
        {"path":"Day_Off_1__c","type":"string"},
        {"path":"Day_Off_2__c","type":"string"},
        {"path":"DefaultCurrencyIsoCode","type":"string"},
        {"path":"Department","type":"string"},
        {"path":"Email","type":"string"},
        {"path":"EmployeeNumber","type":"string"},
        {"path":"FirstName","type":"string"},
        {"path":"Id","type":"string"},
        {"path":"IsActive","type":"string"},
        {"path":"isConsents__c","type":"string"},
        {"path":"isMedicalQueryUser__c","type":"string"},
        {"path":"isPortfolioSellingUser__c","type":"string"},
        {"path":"isSampleManagementUser__c","type":"string"},
        {"path":"isSurveyModuleUser__c","type":"string"},
        {"path":"JointVisittype__c","type":"string"},
        {"path":"LastName","type":"string"},
        {"path":"ManagerId","type":"string"},
        {"path":"MobilePhone","type":"string"},
        {"path":"Month__c","type":"string"},
        {"path":"MR_DirectManager__c","type":"string"},
        {"path":"MTPCreationenabled__c","type":"string"},
        {"path":"Name","type":"string"},
        {"path":"No_of_Working_Days__c","type":"string"},
        {"path":"OrderManagementEnabled__c","type":"string"},
        {"path":"Phone","type":"string"},
        {"path":"PostalCode","type":"string"},
        {"path":"TradeModuleUser__c","type":"string"},
        {"path":"type_of_Medical_Query__c","type":"string"},
        {"path": "__local__", "type": "string"},
        {"path": "__locally_created__", "type": "string"},
        {"path": "__locally_updated__", "type": "string"},
        {"path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "CallReport",
      "sfdcObjectName":"Call_Report__c",
      "configAvailable":true,
      "indexes": 
        [
          {"type":"string",                               "path":"Id"},
          {"type":"string",                               "path":"Organisation__c",           "upload": "true"},
          {"type":"string",                               "path":"Contact1__c",               "upload": "true"},
          {"type":"string",                               "path":"MTP__c",                    "upload": "true"},
          {"type":"string",                               "path":"Date_Time_Planned__c",      "upload": "true"},
          {"type":"string",                               "path":"Date_Time_Actual__c",       "upload": "true"},
          {"type":"string",                               "path":"Date_time_Visit_End__c",    "upload": "true"},
          {"type":"string",                               "path":"Created_from_Mobile__c",                           "upload": "true"},
          {"type":"string",                               "path":"Created_Offline__c",                               "upload": "true"},
          {"type":"string",                               "path":"Is_Target_Call__c"},
          {"type":"string",                               "path":"Type__c",                   "upload": "true"},
          {"type":"string",                               "path":"RecordTypeId",                                     "upload": "true"},
          {"type":"string",                               "path":"Duration__c",                                      "upload": "true"},
          {"type":"string",                               "path":"Target_Priority__c"},
          {"type":"string",                               "path":"Coaching_Visit__c",                                "upload": "true"},
          {"type":"string",                               "path":"Coaching_Visit_User__c",                           "upload": "true"},
          {"type":"string",                               "path":"User__c",                   "upload": "true"},
          {"type":"string",                               "path":"GeneralComments__c",                               "upload": "true"},
          {"type":"string",                               "path":"Call_Objective__c",                                "upload": "true"},
          {"type":"string",                               "path":"Next_Call_Objective__c",                           "upload": "true"},
          {"type":"string",                               "path":"Promotional_Items_Prio_1__c",                      "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Product__c",                                "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_1__c",                               "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Marketing_Message_1__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Marketing_Message_2__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Marketing_Message_3__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Marketing_Message_4__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Marketing_Message_5__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Reactions_To_Marketing_Message_1__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Reactions_To_Marketing_Message_2__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Reactions_To_Marketing_Message_3__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Reactions_To_Marketing_Message_4__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_1_Reactions_To_Marketing_Message_5__c",       "upload": "true"},
          {"type":"string",                               "path":"Promotional_Items_Prio_2__c",                      "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Product__c",                                "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_2__c",                               "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Marketing_Message_1__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Marketing_Message_2__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Marketing_Message_3__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Marketing_Message_4__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Marketing_Message_5__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Reactions_To_Marketing_Message_1__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Reactions_To_Marketing_Message_2__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Reactions_To_Marketing_Message_3__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Reactions_To_Marketing_Message_4__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_2_Reactions_To_Marketing_Message_5__c",       "upload": "true"},
          {"type":"string",                               "path":"Promotional_Items_Prio_3__c",                      "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Product__c",                                "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_3__c",                               "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Marketing_Message_1__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Marketing_Message_2__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Marketing_Message_3__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Marketing_Message_4__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Marketing_Message_5__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Reactions_To_Marketing_Message_1__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Reactions_To_Marketing_Message_2__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Reactions_To_Marketing_Message_3__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Reactions_To_Marketing_Message_4__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_3_Reactions_To_Marketing_Message_5__c",       "upload": "true"},
          {"type":"string",                               "path":"Promotional_Items_Prio_4__c",                      "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Product__c",                                "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_4__c",                               "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Marketing_Message_1__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Marketing_Message_2__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Marketing_Message_3__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Marketing_Message_4__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Marketing_Message_5__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Reactions_To_Marketing_Message_1__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Reactions_To_Marketing_Message_2__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Reactions_To_Marketing_Message_3__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Reactions_To_Marketing_Message_4__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_4_Reactions_To_Marketing_Message_5__c",       "upload": "true"},
          {"type":"string",                               "path":"Promotional_Items_Prio_5__c",                      "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Product__c",                                "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_5__c",                               "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Marketing_Message_1__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Marketing_Message_2__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Marketing_Message_3__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Marketing_Message_4__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Marketing_Message_5__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Reactions_To_Marketing_Message_1__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Reactions_To_Marketing_Message_2__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Reactions_To_Marketing_Message_3__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Reactions_To_Marketing_Message_4__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_5_Reactions_To_Marketing_Message_5__c",       "upload": "true"},
          {"type":"string",                               "path":"Promotional_Items_Prio_6__c",                      "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Product__c",                                "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_6__c",                               "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Marketing_Message_1__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Marketing_Message_2__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Marketing_Message_3__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Marketing_Message_4__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Marketing_Message_5__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Reactions_To_Marketing_Message_1__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Reactions_To_Marketing_Message_2__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Reactions_To_Marketing_Message_3__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Reactions_To_Marketing_Message_4__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_6_Reactions_To_Marketing_Message_5__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_7_Product__c",                                "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_7__c",                               "upload": "true"},
          {"type":"string",                               "path":"Prio_7_Marketing_Message_1__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_7_Marketing_Message_2__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_7_Marketing_Message_3__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_7_Reactions_To_Marketing_Message_1__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_7_Reactions_To_Marketing_Message_2__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_7_Reactions_To_Marketing_Message_3__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_7_Reactions_To_Marketing_Message_4__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_7_Reactions_To_Marketing_Message_5__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_8_Product__c",                                "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_8__c",                               "upload": "true"},
          {"type":"string",                               "path":"Prio_8_Marketing_Message_1__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_8_Marketing_Message_2__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_8_Marketing_Message_3__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_8_Reactions_To_Marketing_Message_1__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_8_Reactions_To_Marketing_Message_2__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_8_Reactions_To_Marketing_Message_3__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_8_Reactions_To_Marketing_Message_4__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_8_Reactions_To_Marketing_Message_5__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_9_Product__c",                                "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_9__c",                               "upload": "true"},
          {"type":"string",                               "path":"Prio_9_Marketing_Message_1__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_9_Marketing_Message_2__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_9_Marketing_Message_3__c",                    "upload": "true"},
          {"type":"string",                               "path":"Prio_9_Reactions_To_Marketing_Message_1__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_9_Reactions_To_Marketing_Message_2__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_9_Reactions_To_Marketing_Message_3__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_9_Reactions_To_Marketing_Message_4__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_9_Reactions_To_Marketing_Message_5__c",       "upload": "true"},
          {"type":"string",                               "path":"Prio_10_Product__c",                               "upload": "true"},
          {"type":"string",                               "path":"Note_for_Prio_10__c",                              "upload": "true"},
          {"type":"string",                               "path":"Prio_10_Marketing_Message_1__c",                   "upload": "true"},
          {"type":"string",                               "path":"Prio_10_Marketing_Message_2__c",                   "upload": "true"},
          {"type":"string",                               "path":"Prio_10_Marketing_Message_3__c",                   "upload": "true"},
          {"type":"string",                               "path":"Prio_10_Reactions_To_Marketing_Message_1__c",      "upload": "true"},
          {"type":"string",                               "path":"Prio_10_Reactions_To_Marketing_Message_2__c",      "upload": "true"},
          {"type":"string",                               "path":"Prio_10_Reactions_To_Marketing_Message_3__c",      "upload": "true"},
          {"type":"string",                               "path":"Prio_10_Reactions_To_Marketing_Message_4__c",      "upload": "true"},
          {"type":"string",                               "path":"Prio_10_Reactions_To_Marketing_Message_5__c",      "upload": "true"},
          {"type":"string",                               "path":"Signature__c",                                     "upload": "true"},
          {"type":"string",                               "path":"Signature_taken__c",                               "upload": "true"},
          {"type":"string",                               "path":"CallWithIPad__c",                                  "upload": "true"},
          {"type":"string",                               "path":"RealCallDuration__c",                              "upload": "true"},
          {"type":"string",                               "path":"User__r.FirstName"},
          {"type":"string",                               "path":"User__r.LastName"},
          {"type":"string",                               "path":"User__r.Name"},
          {"type":"string",                               "path":"Contact1__r.FirstName"},
          {"type":"string",                               "path":"Contact1__r.Name"},
          {"type":"string",                               "path":"Contact1__r.Account.RecordType.Name"},
          {"type":"string",                               "path":"Contact1__r.Account.C_Specialty_1__c"},
          {"type":"string",                               "path":"Contact1__r.Account.Priority__c"},
          {"type":"string",                               "path":"Organisation__r.Name"},
          {"type":"string",                               "path":"Organisation__r.BillingCity"},
          {"type":"string",                               "path":"Organisation__r.BillingStreet"},
          {"type":"string",                               "path":"Type_of_visit__c",                                 "upload": "true"},
          {"type":"string",                               "path":"CLM_Tool_Id__c",                                   "upload": "true"},
          {"type":"string",                               "path":"Manager_Comments__c"},
          {"type":"string",                               "path":"Joint_Visit_Participants__c",                      "upload": "true"},
          {"type":"string",                               "path":"Call_Report_Status__c",                            "upload": "true"},
          {"type":"string",                               "path":"Is_Coaching_Visit_Activities__c",                  "upload": "true"},
          {"type":"string",                               "path":"Is_Invite_To_Event__c",                            "upload": "true"},
          {"type":"string",                               "path":"Is_One_To_One_Calls__c",                           "upload": "true"},
          {"type":"string",                               "path":"Is_Converted_From_Appointment__c",                 "upload": "true"},
          {"type":"string",                               "path":"Customer_Status__c"},
          {"type":"string",                               "path":"Joint_Visit__c"},
          { "path": "__local__", "type": "string"},
          { "path": "__locally_created__", "type": "string"},
          { "path": "__locally_updated__", "type": "string"},
          { "path": "__locally_deleted__", "type": "string"}
        ]

    },

    {
      "soupName": "MTP",
      "sfdcObjectName":"MTP__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "Month__c","type": "string"},
        {"path": "Year__c","type": "string"},
        {"path": "Status__c","type": "string"},
        {"path": "Start_Date__c","type": "string"},
        {"path": "End_Date__c","type": "string"},
        {"path": "Med_Rep__c","type": "string"},
        {"path": "CreatedDate","type": "string"},
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "Product",
      "sfdcObjectName":"Pharma_Product__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "full_text"},
        {"path": "ATC_Class__c","type": "string"},
        {"path": "Presentation__c","type": "string"},
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },
    {
      "soupName": "ProductMessage",
      "sfdcObjectName":"Pharma_Product_Messages__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "full_text"},
        {"path": "Complete_Marketing_Message__c","type": "string"},
        {"path": "Country__c","type": "string"},
        {"path": "End_Date__c","type": "string"},
        {"path": "Pharma_Product__c","type": "string"},
        {"path": "ReactionsToMarketingMessage__c","type": "string"},
        {"path": "Start_Date__c","type": "string"},
        {"path": "Status__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "PharmaEvent",
      "sfdcObjectName":"Events__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "OwnerId","type": "string"},
        {"path": "Created_Offline__c","type": "string"},
        {"path": "Owner.FirstName","type": "string"},

        {"path": "Owner.LastName","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "Type_of_Event__c","type": "string"},
        {"path": "Location__c","type": "string"},

        {"path": "Start_Date__c","type": "string"},
        {"path": "End_Date__c","type": "string"},
        {"path": "Closed_Date_Time__c","type": "string"},
        {"path": "Closed_Date_Difference__c","type": "string"},

        {"path": "Stage__c","type": "string"},
        {"path": "DiscussionType__c","type": "string"},
        {"path": "Estimated_Budget__c","type": "string"},
        {"path": "Planned_Budget__c","type": "string"},

        {"path": "Planned_number_of_participants__c","type": "string"},
        {"path": "Status__c","type": "string"},
        {"path": "Business_Unit__c","type": "string"},
        {"path": "Objectives__c","type": "string"},

        {"path": "Agenda__c","type": "string"},
        {"path": "Speaker_s__c","type": "string"},
        {"path": "Evaluation__c","type": "string"},
        {"path": "Product_Prio1__c","type": "string"},

        {"path": "Product_Prio2__c","type": "string"},
        {"path": "Product_Prio3__c","type": "string"},
        {"path": "Product_Prio_4__c","type": "string"},
        {"path": "CLM_Tool_Id__c","type": "string"},

        {"path": "TG_and_E_Form__c","type": "string"},
        {"path": "Invoice_Receipt__c","type": "string"},
        {"path": "Invoice_Receipt1__c","type": "string"},
        {"path": "Invoice_Receipt2__c","type": "string"},

        {"path": "Invoice_Receipt3__c","type": "string"},
        {"path": "Attendance_Sheet__c","type": "string"},
        {"path": "Attendance_Sheet_1__c","type": "string"},
        {"path": "Summary_of_the_event__c","type": "string"},

        {"path": "No_of_HCPs__c","type": "string"},
        {"path": "No_of_Mylan_Staff__c","type": "string"},
        {"path": "Cost_of_each_meal__c","type": "string"},
        {"path": "Cost_of_Meal_per_person_USD__c","type": "string"},

        {"path": "Name_of_the_Presenter__c","type": "string"},
        {"path": "Mylan_Participants__c","type": "string"},
        {"path": "Mylan_Participants_Emails__c","type": "string"},
        {"path": "Type_of_Hospital__c","type": "string"},

        {"path": "Budget__c","type": "string"},
        {"path": "Any_Other_Expenses__c","type": "string"},
        {"path": "Planned_OE_Text_1__c","type": "string"},
        {"path": "Planned_OE_Text_2__c","type": "string"},

        {"path": "Planned_OE_Text_3__c","type": "string"},
        {"path": "Planned_OE_Text_4__c","type": "string"},
        {"path": "Planned_OE_Text_5__c","type": "string"},
        {"path": "Planned_OE_USD_1__c","type": "string"},

        {"path": "Planned_OE_USD_2__c","type": "string"},
        {"path": "Planned_OE_USD_3__c","type": "string"},
        {"path": "Planned_OE_USD_4__c","type": "string"},
        {"path": "Planned_OE_USD_5__c","type": "string"},

        {"path": "Total_Cost_of_Event_Local_Currency__c","type": "string"},
        {"path": "Total_Cost_of_Event_USD__c","type": "string"},


        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "Survey",
      "sfdcObjectName":"Survey__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "full_text"},
        {"path": "Survey_type__c","type": "string"},
        {"path": "Start_Date__c","type": "string"},

        {"path": "End_Date__c","type": "string"},
        {"path": "Active__c","type": "string"},
        {"path": "CreatedDate","type": "string"},
        {"path": "Pharma_Product__c","type": "string"},

        {"path": "Product_ATC_Class__c","type": "string"},
        {"path": "Business_Unit__c","type": "string"},
        {"path": "Currency__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "SampleDelivery",
      "sfdcObjectName":"Sample_Delivery__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Remarks__c","type": "string"},
        {"path": "MR_Approved__c","type": "string"},
        {"path": "Product_Formulary__c","type": "string"},

        {"path": "Items_delivered__c","type": "string"},
        {"path": "Call_Report__c","type": "string"},
        {"path": "Signature__c","type": "string"},
        {"path": "Contact_ID__c","type": "string"},

        {"path": "Sample_Stock__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "SampleStock",
      "sfdcObjectName":"Sample_Stock__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Number_of_samples_left__c","type": "string"},
        {"path": "Product_Formulary__c","type": "string"},
        {"path": "Product_Lot_Formulary__c","type": "string"},

        {"path": "Name","type": "full_text"},
        {"path": "Lot_Item_Number__c","type": "string"},
        {"path": "Stock_Status__c","type": "string"},
        {"path": "Lot_Expiry_Date__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "SurveyQuestionnaire",
      "sfdcObjectName":"SurveyQuestionnaire__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Question__c","type": "string"},
        {"path": "Picklist_Value__c","type": "string"},
        {"path": "Type__c","type": "string"},

        {"path": "Survey__c","type": "string"},
        {"path": "Sort_Order__c","type": "string"},
        {"path": "Non_Customer__c","type": "string"},
        {"path": "Required__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },


    {
      "soupName": "SurveyQuestionnaireDetail",
      "sfdcObjectName":"SurveyQuestionnaireDetail__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Account__c","type": "string"},
        {"path": "Customer__c","type": "string"},
        {"path": "Survey_Questionnaire__c","type": "string"},

        {"path": "Real_Value__c","type": "string"},
        {"path": "Question__c","type": "string"},
        {"path": "StringRealValue__c","type": "string"},
        {"path": "NumberRealValue__c","type": "string"},

        {"path": "Survey__c","type": "string"},
        {"path": "Non_Customer__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },


    {
      "soupName": "RecordType",
      "sfdcObjectName":"RecordType",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "DeveloperName","type": "string"},
        {"path": "IsActive","type": "string"},
        {"path": "Name","type": "string"},

        {"path": "SobjectType","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "ProductItem",
      "sfdcObjectName":"Product_Items__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Description__c","type": "string"},
        {"path": "IMS_sku_code__c","type": "string"},
        {"path": "Name","type": "string"},

        {"path": "Parallel_Import__c","type": "string"},
        {"path": "Sales_1_month_ago__c","type": "string"},
        {"path": "Sales_2_months_ago__c","type": "string"},
        {"path": "Sales_3_months_ago__c","type": "string"},

        {"path": "Sales_4_months_ago__c","type": "string"},
        {"path": "Sales_YTD__c","type": "string"},
        {"path": "Product_Brand_Name__c","type": "string"},
        {"path": "Product_Detail_Code__c","type": "string"},
        {"path": "Product_Type_detect__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "Presentation",
      "sfdcObjectName":"Presentation__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "Description__c","type": "string"},
        {"path": "Version__c","type": "string"},

        {"path": "DownloadUrl__c","type": "string"},
        {"path": "Active__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "PresentationSlide",
      "sfdcObjectName":"PresentationSlide__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "Presentation__c","type": "string"},
        {"path": "PreviewId__c","type": "string"},

        {"path": "SlidePreview__c","type": "string"},
        {"path": "Thumbnail__c","type": "string"},
        {"path": "ThumbnailId__c","type": "string"},
        {"path": "Title__c","type": "string"},
        {"path": "UniqueId__c","type": "string"},
        {"path": "LastModifiedDate","type": "string"},
        
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "PEEventExpense",
      "sfdcObjectName":"Event_Costs__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "RecordTypeId","type": "string"},
        {"path": "Pharma_Event__c","type": "string"},

        {"path": "Cost_of_each_meal__c","type": "string"},
        {"path": "Cost_of_Meal_per_person_USD__c","type": "string"},
        {"path": "No_of_HCPs__c","type": "string"},
        {"path": "No_of_Mylan_staff__c","type": "string"},
        {"path": "Total_of_Attendees__c","type": "string"},
        {"path": "Total_Cost_In_Local__c","type": "string"},

        {"path": "Total_Cost_USD__c","type": "string"},
        {"path": "Actual_OE_Text_1__c","type": "string"},
        {"path": "Actual_OE_Text_2__c","type": "string"},
        {"path": "Actual_OE_Text_3__c","type": "string"},
        {"path": "Actual_OE_Text_4__c","type": "string"},
        {"path": "Actual_OE_Text_5__c","type": "string"},

        {"path": "Actual_OE_Local_1__c","type": "string"},
        {"path": "Actual_OE_Local_2__c","type": "string"},
        {"path": "Actual_OE_Local_3__c","type": "string"},
        {"path": "Actual_OE_Local_4__c","type": "string"},
        {"path": "Actual_OE_Local_5__c","type": "string"},
        {"path": "Actual_OE_USD_1__c","type": "string"},

        {"path": "Actual_OE_USD_2__c","type": "string"},
        {"path": "Actual_OE_USD_3__c","type": "string"},
        {"path": "Actual_OE_USD_4__c","type": "string"},
        {"path": "Actual_OE_USD_5__c","type": "string"},
        {"path": "Total_Cost_of_Event_Local_Currency__c","type": "string"},
        {"path": "Total_Cost_of_Event_USD__c","type": "string"},
        {"path": "totalNumberOfHCPs__c","type": "string"},
        
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "PEAttendee",
      "sfdcObjectName":"Luncheon_Attendances__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Pharma_Event__c","type": "string"},
        {"path": "Attendee__c","type": "string"},
        {"path": "Hospital_Name__r.Name","type": "string"},

        {"path": "Hospital_Name__r.BillingCity","type": "string"},
        {"path": "Hospital_Name__r.Phone","type": "string"},
        {"path": "Hospital_Name__r.BillingCountry","type": "string"},
        {"path": "Hospital_Name__r.BillingStreet","type": "string"},
        {"path": "Type_of_Hospital__c","type": "string"},
        {"path": "Signature__c","type": "string"},

        {"path": "Whether_attendee_had_lunch__c","type": "string"},
        {"path": "Non_Customer_data__c","type": "string"},
        {"path": "ConfirmHCPCount__c","type": "string"},
        {"path": "Non_Organsation__c","type": "string"},
        
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "OrderManagement",
      "sfdcObjectName":"Order__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "Account__c","type": "string"},
        {"path": "Account__r.Name","type": "string"},

        {"path": "Special_Price__c","type": "string"},
        {"path": "Status__c","type": "string"},
        {"path": "Billing_Address__c","type": "string"},
        {"path": "Date_Time_Created__c","type": "string"},
        {"path": "Date_Time_Sent_to_Distributor__c","type": "string"},
        {"path": "Notes__c","type": "string"},

        {"path": "Order_Sales_Amount__c","type": "string"},
        {"path": "Order_Total_Quantity__c","type": "string"},
        {"path": "Shipping_Address__c","type": "string"},
        {"path": "Signature__c","type": "string"},

        {"path": "RecordTypeId","type": "string"},
        {"path": "RecordType.Name","type": "string"},
        {"path": "Sample_No__c","type": "string"},
        {"path": "PO_No__c","type": "string"},

        {"path": "New_Address__c","type": "string"},
        {"path": "Remarks__c","type": "string"},
        {"path": "Non_Customer__c","type": "string"},
        
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "OrderLineItems",
      "sfdcObjectName":"Order_Line_Item__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Order__c","type": "string"},
        {"path": "Quantity__c","type": "string"},
        {"path": "Sales_Amount__c","type": "string"},

        {"path": "Sales_Price__c","type": "string"},
        {"path": "SKU_Name__c","type": "string"},
        {"path": "SKU_Name__r.Name","type": "string"},
        
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },


    {
      "soupName": "MedicalQuery",
      "sfdcObjectName":"Medical_Query__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Creation_Date_Time__c","type": "string"},
        {"path": "Customer__c","type": "string"},
        {"path": "Non_Customer__c","type": "string"},

        {"path": "MQ_Speciality__c","type": "string"},
        {"path": "Customer__r.Name","type": "string"},
        {"path": "Organization__c","type": "string"},

        {"path": "Organization__r.Name","type": "string"},
        {"path": "Customer__r.Email","type": "string"},
        {"path": "Email1__c","type": "string"},

        {"path": "Customer__r.Phone","type": "string"},
        {"path": "Mobile_Phone__c","type": "string"},
        {"path": "Mylan_Medical_Contact__c","type": "string"},

        {"path": "Mylan_Medical_Contact__r.Name","type": "string"},
        {"path": "Pharma_Product__c","type": "string"},
        {"path": "Pharma_Product__r.Name","type": "string"},

        {"path": "Query_description__c","type": "string"},
        {"path": "Status__c","type": "string"},
        {"path": "Type_of_Medical_Query__c","type": "string"},

        {"path": "User__c","type": "string"},
        {"path": "User__r.Name","type": "string"},
        {"path": "Mylan_Medical_Contact_Persons__c","type": "string"},
        {"path": "Mylan_Medical_Contact_Persons_Email__c","type": "string"},
        
        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "MedicalComment",
      "sfdcObjectName":"Medical_Comment__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Comment_Description__c","type": "string"},
        {"path": "Creation_Date_Time__c","type": "string"},
        {"path": "Feedback__c","type": "string"},

        {"path": "Medical_Query__c","type": "string"},
        {"path": "CreatedById","type": "string"},
        {"path": "CreatedBy.Name","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },


    {
      "soupName": "DoctorsConsent",
      "sfdcObjectName":"Consent__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "First_Name__c","type": "string"},
        {"path": "Last_Name__c","type": "string"},
        {"path": "E_mail_address__c","type": "string"},

        {"path": "Cell_phone_number__c","type": "string"},
        {"path": "Doctor_ID_code__c","type": "string"},
        {"path": "Name_of_organization__c","type": "string"},

        {"path": "Organisation_ID__c","type": "string"},
        {"path": "Signature__c","type": "string"},
        {"path": "Specialty__c","type": "string"},

        {"path": "Brick_Name__c","type": "string"},
        {"path": "Non_Organisation__c","type": "string"},
        {"path": "Country__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "Device",
      "sfdcObjectName":"Mobile_Devices__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Device_ID__c","type": "string"},
        {"path": "Erased__c","type": "string"},
        {"path": "Request_Erase__c","type": "string"},

        {"path": "Last_Syncronization__c","type": "string"},
        {"path": "Last_User__c","type": "string"},
        {"path": "Model__c","type": "string"},

        {"path": "OS_Version__c","type": "string"},
        {"path": "Version__c","type": "string"},
        {"path": "Last_Debug_Log__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "CLMCallReportData",
      "sfdcObjectName":"Clm_CallReportData__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "KpiSrcJson__c","type": "string"},
        {"path": "TimeOnSlides__c","type": "string"},

        {"path": "CallReport__c","type": "string"},
        {"path": "Product__c","type": "string"},
        {"path": "Product__r.Name","type": "string"},

        {"path": "Presentation__c","type": "string"},
        {"path": "Presentation__r.Name","type": "string"},
        {"path": "CLM_Tool_Id__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },


    {
      "soupName": "Brick",
      "sfdcObjectName":"Account",
      "configAvailable":true,
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "Short_Description__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },


    {
      "soupName": "Attachment",
      "sfdcObjectName":"Attachment",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "Body","type": "string"},

        {"path": "ContentType","type": "string"},
        {"path": "Description","type": "string"},
        {"path": "ParentId","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "Tot",
      "sfdcObjectName":"Time_off_Territory__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "OwnerId","type": "string"},
        {"path": "Created_Offline__c","type": "string"},

        {"path": "Owner.LastName","type": "string"},
        {"path": "Owner.FirstName","type": "string"},
        {"path": "All_Day__c","type": "string"},

        {"path": "Start_Date__c","type": "string"},
        {"path": "End_Date__c","type": "string"},
        {"path": "Type_First_Quarter__c","type": "string"},
        {"path": "Type_Second_Quarter__c","type": "string"},
        {"path": "Type_Third_Quarter__c","type": "string"},
        {"path": "Type_Fourth_Quarter__c","type": "string"},

        {"path": "Type__c","type": "string"},
        {"path": "Description__c","type": "string"},
        {"path": "IsSubmittedForApproval__c","type": "string"},
        {"path": "CLM_Tool_Id__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "PatchCustomer",
      "sfdcObjectName":"Patch_Customer__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Patch__r.Name","type": "string"},
        {"path": "Contact__c","type": "string"},
        {"path": "Contact__r.Name","type": "string"},
        {"path": "Class__c","type": "string"},
        {"path": "Frequency__c","type": "string"},
        {"path": "RecordType.Name","type": "string"},
        {"path": "Patch__c","type": "string"},
        {"path": "City__c","type": "string"},
        {"path": "Status__c","type": "string"},
        {"path": "Patch__r.Station__c","type": "string"},
        {"path": "State__c","type": "string"},
        {"path": "Speciality__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },

    {
      "soupName": "Patch",
      "sfdcObjectName":"Patch__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "Name","type": "string"},
        {"path": "State__c","type": "string"},
        {"path": "City__c","type": "string"},
        {"path": "CreatedDate","type": "string"},
        {"path": "Station__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    },
    {
      "soupName": "RedFlag",
      "sfdcObjectName":"Red_Flag__c",
      "indexes": 
      [
        {"path": "Id","type": "string"},
        {"path": "redFlagType__c","type": "string"},
        {"path": "CustomerName__c","type": "string"},

        {"path": "Contact_Email__c","type": "string"},
        {"path": "Contact_Number__c","type": "string"},
        {"path": "Description__c","type": "string"},

        {"path": "CallReportId__c","type": "string"},
        {"path": "Photo__c","type": "string"},

        { "path": "__local__", "type": "string"},
        { "path": "__locally_created__", "type": "string"},
        { "path": "__locally_updated__", "type": "string"},
        { "path": "__locally_deleted__", "type": "string"}
    ]
    }



  ]
};
// import {Contact} from '../models/contact'

// export const DB = [
// Contact];