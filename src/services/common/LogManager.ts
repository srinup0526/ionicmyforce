import { DeviceManager } from '../common/DeviceManager';
import AutosyncSettingsManager from '../autosync/AutosyncSettingsManager';
import SettingsManager from './../db/SettingsManager';
import * as moment from 'moment';
import {LocalizationManager} from './../../services/common/localizations/LocalizationManager';
import {Injectable} from "@angular/core";


@Injectable()
export class LogManager {
  private static logStorageKey: string = 'LastDebugLog';
  public static stepIndexKey: string = 'stepIndex';
  public static lineSeparator: string = '\n';
  public static startDateFormat: string = 'DD.MM.YYYY HH:mm';
  public static logDateFormat: string = 'HH:mm:ss:SSS';
  public static errorMessagesLog: string = '';
  public static log: string = '';
  private static maxLogLength: number = 32768;
  
  constructor(private localizationManager: LocalizationManager){}
  
  initLog() {
    this.prepareFirstLog()
      .then((value) => {
        LogManager.log = value;
      });
  }
  
  async appendInfoLog(message) {
    const logScreenLabel = await this.localizationManager.promiseLocale('helpdesk.LogScreen.Info');
    
    LogManager.log += `${ logScreenLabel } ${ this.formatedDate() }-${ message }${ LogManager.lineSeparator }`;
    
    return this.saveDebugLog(LogManager.log + LogManager.lineSeparator);
  }

  updateStepsCount(stepsCount) {
    LogManager.log = LogManager.log.replace(LogManager.stepIndexKey, stepsCount);
    
    return this.saveDebugLog(LogManager.log + LogManager.lineSeparator);
  }

  async appendError(error) {
    const errorLabel = await this.localizationManager.promiseLocale('helpdesk.LogScreen.Error');
    
    LogManager.log += `${ errorLabel } ${ this.formatedDate() }${ LogManager.lineSeparator }${ this.prepareLogData(error) }${ LogManager.lineSeparator }`;
    
    return this.saveDebugLog(LogManager.log);
  }

  async appendWarning(warning) {
    const warningLabel = await this.localizationManager.promiseLocale('helpdesk.LogScreen.Warning');
    
    LogManager.log += `${ warningLabel } ${ this.formatedDate() }${ LogManager.lineSeparator }${ this.prepareLogData(warning) }${ LogManager.lineSeparator }`;
    
    return this.saveDebugLog(LogManager.log);
  }

  getDestinationEmail() {
    return SettingsManager.getValueByKey('supportEmail');
  }

  async getLogForSupport(): Promise<string> {
    const deviceIdLabel = await this.localizationManager.promiseLocale('helpdesk.LogScreen.DeviceId');
    
    return this.getLastDebugLog()
      .then((logData) => {
        logData = logData ? logData.split(LogManager.lineSeparator) : [];

        const deviceId = `${ deviceIdLabel }: ${ DeviceManager.deviceId() }`;

        logData.unshift(deviceId);

        return logData.join('<br/>');
      });
  }

  saveDebugLog(log) {
    return SettingsManager.setValueByKey(LogManager.logStorageKey, log).then(() => console.log(log));
  }

  getLastDebugLog() {
    return SettingsManager.getValueByKey(LogManager.logStorageKey);
  }

  private prepareLogData(error) {
    return JSON.stringify(error);
  }
  
  private setLastDebugLog(lastDebugLog): string {
    if (lastDebugLog.length > LogManager.maxLogLength) {
      lastDebugLog = lastDebugLog.substring(lastDebugLog.length - LogManager.maxLogLength + 5, lastDebugLog.length - 1);
      lastDebugLog = `...${ lastDebugLog }`;
    }
    return LogManager.log = lastDebugLog;
  }

  private formatedDate() {
    return moment().format(LogManager.logDateFormat);
  }
  
  private async prepareFirstLog() {
    const osVersionLabel = await this.localizationManager.promiseLocale('helpdesk.OSVersion');
    const appVersionLabel = await this.localizationManager.promiseLocale('helpdesk.LogScreen.AppVersion');
    
    const date = `${ moment().format(LogManager.startDateFormat) }${ LogManager.lineSeparator }`;
    const OSVersion = `${ osVersionLabel }-${ DeviceManager.osVersion() }${ LogManager.lineSeparator }`;
    const appVersion = `${ appVersionLabel }-${ DeviceManager.appVersion() }${ LogManager.lineSeparator }`;
    const firstLog = date + OSVersion + appVersion;
    
    this.saveDebugLog(firstLog).then(() => console.log('SAVED'));
    
    return firstLog;
  }
}
