import { Injectable } from '@angular/core';
import { EmailComposer, EmailComposerOptions } from '@ionic-native/email-composer';
import {Utils} from '../../utils/Utils';


@Injectable()
export class EmailManager {
  public static DEFAULT_EMAILS = [
    'mylansfdcsupport@mylan.com',
    'GIDC.L2Support_CRMApplication@mylan.com',
    'India_EM_Commercial_IT@mylan.com'
  ];

  constructor(private emailComposer: EmailComposer) {}

  public sendMail(emailOptions: EmailComposerOptions, doPermissionRequest: boolean = true): Promise<any> {
    return this.emailComposer.hasPermission()
      .then((isAvailable: boolean) => {
      
        if (isAvailable) {
      
          return this.open(emailOptions);
      
        } else {
          
          if(!doPermissionRequest){
            return console.warn('Email not available!');
          }
          
          return this.emailComposer.requestPermission()
            .then(() => {
              return this.sendMail(emailOptions, false);
            })
        }
    })
      .catch((error)=>{
        console.log(error);
      })
  }
  
  private open(emailOptions: EmailComposerOptions): Promise<any> {
    const email = this.getEmailOptions(emailOptions);
    return this.emailComposer.open(email);
  }

  private getEmailOptions(emailOptions: EmailComposerOptions): EmailComposerOptions {
    emailOptions.to =  (emailOptions.to  && [].concat(emailOptions.to )) || EmailManager.DEFAULT_EMAILS;

    return Utils.extend({
      to: [],
      cc: [],
      bcc: [],
      attachments: [],
      subject: '',
      body: '',
      isHtml: false
    }, emailOptions);
  }
}
