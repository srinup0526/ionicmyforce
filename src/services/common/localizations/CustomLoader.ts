import { TranslateLoader } from '@ngx-translate/core';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { Platform } from "ionic-angular";
import { FileService } from "./../FileService";
import { Observable } from "rxjs/Rx";
import {Injectable} from "@angular/core";


@Injectable()
export class CustomLoader implements TranslateLoader {
  
  constructor(
    private http: HttpClient,
    private platform: Platform,
    private fileService: FileService
  ) {
  }

  getTranslation(lang: string): Observable<any> {
    if (this.platform.is('cordova')) {
      let promise = new Promise((resolve, reject) => {
        this.fileService.getFileFromApplication(`www/assets/i18n/${lang}.json`)
          .then((data) => {
            resolve(JSON.parse(<string>data));
          })
          .catch( (err) => {
            console.error(err);
            reject(err);
          });
      });
      
      return Observable.fromPromise(promise);
    } else {
      return this.http.get(`assets/i18n/${lang}.json`)
        .map((res: Response) => res.json());
    }
  }
}
