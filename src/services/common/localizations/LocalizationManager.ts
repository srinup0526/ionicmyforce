import { TranslateModule, TranslateLoader, TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CustomLoader } from './CustomLoader';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Platform} from "ionic-angular";
import {FileService} from "../FileService";
import SettingsManager from './../../db/SettingsManager';

enum LANGUAGES {
  general = 'dev',
  english = 'en',
  french = 'fr',
  chinese = 'zh',
  russian = 'ru'
}

@Injectable()
export class LocalizationManager {
  
  static readonly LANG_SETTINGS_KEY: string = 'language';

  constructor(private translateService: TranslateService,
              private http: HttpClient,
              private platform: Platform,
              private fileService: FileService) {
    
    this.languageChangeListener();
  }
  
  
  public setDefaultLanguage() {
    return this.setLanguage(LANGUAGES.english);
  }

  public setDevLanguage() {
    this.translateService.setDefaultLang(LANGUAGES.general)
  }

  public setLanguage(language: string): Promise<any> {
    this.translateService.use(language);
    this.addCssClassToBody(language);
    return this.setLanguageToSettings(language);
  }

  public getCurrentLanguage(): string {
    return this.translateService.currentLang;
  }
  
  public locale(key: string, params?): Observable<string> {
    return this.translateService.get(key, params);
  }

  private languageChangeListener() {
    this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      console.log("Language change to ->", event.lang);
    });
  }

  public getLanguages(): Array<{key: string, label: string}> {
    let map = [];

    for (let languageKey in LANGUAGES) {
      map.push({
        key: LANGUAGES[languageKey],
        label: languageKey
      });
    }

    return map.filter((language) => {
      return language.key != LANGUAGES.general;
    });
  }
  
  public promiseLocale(key: string, params?): Promise<string> {
    return new Promise((resolve, reject) => {
      this.locale(key, params)
        .take(1)
        .subscribe((value) => {
          resolve(value);
        });
    });
  }
  
  public getSeveralLocales(list: Array<string>): Promise<{[key:string]: string}> {
    let languagesLocales = {};
    
    return Promise.all(list.map((key: string) => {
      return new Promise((resolve, reject) => {
        this.locale(key)
          .take(1)
          .subscribe((value) => {
            resolve(value);
            languagesLocales[key] = value;
          });
      });
    }))
    .then(() => {
        return languagesLocales;
    });
  }
  
  public getLanguageFromSettings() {
    return SettingsManager.getValueByKey(LocalizationManager.LANG_SETTINGS_KEY);
  }
  
  private addCssClassToBody(selectLanguage: string): void {
    let body = document.body;
    
    Object
      .keys(LANGUAGES)
      .forEach((languageKey) => {
        body.classList.remove(LANGUAGES[languageKey]);
    });
    
    body.classList.add(selectLanguage);
  }
  
  private setLanguageToSettings(language): Promise<any> {
    return SettingsManager.setValueByKey(LocalizationManager.LANG_SETTINGS_KEY, language)
  }
}
