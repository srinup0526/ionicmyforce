import {LoadingController, Loading } from 'ionic-angular';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: "root"
})
export class Loader {
  
  constructor(private loadingCtrl: LoadingController){
  }
  
  public run(callback): Promise<any> {
    const loader = this.getLoader();
    
    return loader.present()
      .then(() => callback())
      .then((result) => {
        return loader.dismiss()
          .then(() => result);
      })
      .catch((error) => {
        return loader.dismiss()
          .then(() => {
            console.error(error);
  
            return error;
          });
      });
  }
  
  private getLoader(): Loading {
    return this.loadingCtrl.create({
      content: 'Please wait',
      duration: 100
    })
  }
}
