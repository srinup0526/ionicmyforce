import SettingsManager from './../db/SettingsManager';
import { Injectable } from '@angular/core';


@Injectable()
export class PinManager {
  private static readonly KEY = 'pin';
  private static readonly ATTEMPTS_KEY = 'pinAttempts';
  
  constructor() {}

  public isPinMatch(pin): Promise<boolean> {
    return this.getPin()
      .then((currentPin) => {
        return currentPin != '' && currentPin == pin;
      });
  }
  
  public isPinExists(): Promise<boolean> {
    return this.getPin()
      .then((currentPin) => {
        return !!currentPin && currentPin != '';
      })
  }
  
  removePin(): Promise<any> {
    return this.setPin("");
  }
  
  public getPin(): Promise<any> {
    return SettingsManager.getValueByKey(PinManager.KEY);
  }

  public setPin(pin: string): Promise<any> {
    return SettingsManager.setValueByKey(PinManager.KEY, pin);
  }
  
  public setPinAttempts(attempts: string): Promise<any>  {
    return SettingsManager.setValueByKey(PinManager.ATTEMPTS_KEY, attempts);
  }
  
  getPinAttempts(): Promise<any> {
    return SettingsManager.getValueByKey(PinManager.ATTEMPTS_KEY);
  }
}
