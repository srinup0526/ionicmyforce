import {Utils} from '../../utils/Utils';


export class Query {
  static readonly TRUE: string = Utils.isIOS() ? '1' : `'true'`;
  static readonly FALSE: string = Utils.isIOS() ? '0' : `'false'`;
  static readonly ALL: string = '_soup';
  static readonly AND: string = 'AND';
  static readonly OR: string = 'OR';
  static readonly IN: string = 'IN';
  static readonly NOT_IN: string = 'NOT IN';
  static readonly EQ: string = '=';
  static readonly NE: string = '!=';
  static readonly GR: string = '>';
  static readonly LR: string = '<';
  static readonly GRE: string = '>=';
  static readonly LRE: string = '<=';
  static readonly ASC: string = 'ASC';
  static readonly DESC: string = 'DESC';
  static readonly IS: string = 'IS';
  static readonly ISNT: string = 'IS NOT';
  static readonly BETWEEN: string = 'BETWEEN';
  query: string;
  soup: string;
  isWhereUsed: boolean;
  isConditionUsed: boolean;
  isBracketStart: boolean;
  isBracketEnd: boolean;

  constructor(soup = '') {
    this.soup = soup;
    this.query = '';
    this.isWhereUsed = false;
    this.isConditionUsed = false;
    this.isBracketStart = false;
    this.isBracketEnd = false;
  }


  customQuery(queryString) {
    this.query += queryString;

    if (this.query.indexOf(' WHERE ') > 0) {
      this.isWhereUsed = true;
    }

    return this;
  }

  selectFrom(soup, fields: any = Query.ALL) {
    this.soup = soup;

    if (fields === Query.ALL) {
      this.query += `SELECT {${this.soup}:${fields}} FROM {${this.soup}}`;
    } else {
      const selectConditions = fields.map((field) => `{${this.soup}:${field}}`);
      this.query += `SELECT ${selectConditions.join(',')} FROM {${this.soup}}`;
    }
    return this;
  }

  selectCountFrom(soup) {
    this.soup = soup;
    this.query += `SELECT COALESCE(count(*), 0) FROM {${this.soup}}`;
    return this;
  }


  selectMaxFrom(soup, field = 'Id') {
    this.soup = soup;
    this.query += `SELECT COALESCE(max({${this.soup}:${field}}), 0) FROM {${this.soup}}`;
    return this;
  }


  whereCustomCondition(customQuery) {
    this.query += this._whereCondition() + '(' + customQuery + ')';
    this.isConditionUsed = false;
    return this;
  }

  where(fieldsValues, eqCondition = Query.EQ, joinWith = Query.AND) {
    const whereConditions = [];

    if (!Object.keys(fieldsValues).length) {
      return this;
    }

    for (const field of Object.keys(fieldsValues)) {
      const value = fieldsValues[field];

      whereConditions.push(`{${this.soup}:${field}} ${eqCondition} ${this.valueOf(value)}`);
    }

    this.query += this._whereCondition() + '(' + whereConditions.join(` ${joinWith} `) + ')';

    this.isConditionUsed = false;

    return this;
  }


  whereNull(fieldName) {
    this.query += `${this._whereCondition()}{${this.soup}:${fieldName}} ${Query.IS} NULL`;
    this.isConditionUsed = false;
    return this;
  }

  whereNotNull(fieldName) {
    this.query += `${this._whereCondition()}{${this.soup}:${fieldName}} ${Query.ISNT} NULL`;
    this.isConditionUsed = false;
    return this;
  }


  whereNotLike(fieldsValues, joinWith = Query.OR) {
    const notLikeConditions = [];

    for (const field of Object.keys(fieldsValues)) {
      const value = fieldsValues[field];
      notLikeConditions.push(`{${this.soup}:${field}} NOT LIKE '%${this._ecranisedValue(value)}%'`);
    }

    this.query += this._whereCondition() + '(' + notLikeConditions.join(` ${joinWith} `) + ')';
    this.isConditionUsed = false;
    return this;
  }

  whereLike(fieldsValues, joinWith = Query.OR) {
    const likeConditions = [];

    for (const field of Object.keys(fieldsValues)) {
      const value = fieldsValues[field];
      likeConditions.push(`{${this.soup}:${field}} LIKE  '%${this._ecranisedValue(value)}%'`);
    }
    this.query += this._whereCondition() + '(' + likeConditions.join(` ${joinWith} `) + ')';
    this.isConditionUsed = false;
    return this;
  }

  whereLikeStartWith(fieldsValues, joinWith = Query.OR) {
    const likeConditions = [];

    for (const field of Object.keys(fieldsValues)) {
      const value = fieldsValues[field];
      likeConditions.push(`{${this.soup}:${field}} LIKE '${this._ecranisedValue(value)}%'`);
    }

    this.query += this._whereCondition() + '(' + likeConditions.join(` ${joinWith} `) + ')';
    this.isConditionUsed = false;
    return this;
  }

  whereIn(field, values) {
    values = values.map((value) => this.valueOf(value));
    this.query += this._whereCondition() + ` {${this.soup}:${field}} ${Query.IN} (${values.join(', ')})`;
    return this;
  }

  whereNotIn(field, values) {
    values = values.map((value) => this.valueOf(value));
    this.query += this._whereCondition() + ` {${this.soup}:${field}} ${Query.NOT_IN} (${values.join(', ')})`;
    return this;
  }


  whereBetween(fieldName, minValue, maxValue) {
    this.query += `${this._whereCondition()}{${this.soup}:${fieldName}} ${Query.BETWEEN} '${minValue}' ${Query.AND} '${maxValue}'`;
    this.isConditionUsed = false;
    return this;
  }

  orderBy(fields, order = Query.ASC, toInteger = false) {
    const intStr = toInteger ? ' + 0 ' : '';

    const orderConditions = fields.map((field) => {
      if (Utils.isIOS()) {
        return `{${this.soup}:${field}} ${intStr} COLLATE NOCASE ${order}`;
      }

      return `{${this.soup}:${field}} = 'null' ${order}, {${this.soup}:${field}} ${intStr} COLLATE NOCASE ${order}`;
    });

    this.query += ' ORDER BY ' + orderConditions.join(',');

    return this;
  }

  bracketStart() {
    this.isBracketStart = true;
    return this;
  }


  bracketEnd() {
    this.query += ' ) ';
    return this;
  }

  _bracketStartCondition() {
    if (this.isBracketStart) {
      this.isBracketStart = false;

      return ' ( ';
    } else {

      return '';
    }
  }

  limit(count) {
    this.query += ` LIMIT ${count}`;
    return this;
  }

  and() {
    this.query += this._andQuery();
    return this;
  }

  _andQuery() {
    this.isConditionUsed = true;
    return ` ${Query.AND} ` + this._bracketStartCondition();
  }

  or() {
    this.query += ` ${Query.OR} `;
    this.isConditionUsed = true;
    return this;
  }

  valueOf(value) {
    switch (value) {
      case true:
        return Query.TRUE;
      case false:
        return Query.FALSE;
      default:
        return `'${this._ecranisedValue(value)}'`;
    }
  }

  _ecranisedValue(value) {
    if (typeof value === 'string') {
      return value.replace(new RegExp(`\\'`, 'gim'), `''`);
    }

    return value;
  }

  _whereCondition() {
    if (this.isWhereUsed) {
      if (this.isConditionUsed) {
        return ' ' + this._bracketStartCondition();
      } else {
        return this._andQuery();
      }
    } else {
      this.isWhereUsed = true;

      return ' WHERE ' + this._bracketStartCondition();
    }
  }

  toString() {
    return this.query;
  }
}
