import {AttachmentFileManager} from './AttachmentFileManager';
import {Injectable} from '@angular/core';

declare var window;


@Injectable()
export class AttachmentsManager {
  static cordovaRef = window.PhoneGap || window.Cordova || window.cordova;

  constructor(private attachmentFileManager: AttachmentFileManager) {}

  public openAttachment(attachment) {
    let filePath = this.attachmentFileManager.getFilePath(attachment.body, attachment.title);

    if (filePath) {
      filePath = filePath.replace('file://', '');

      return this.open(filePath, attachment.contentType);
    }

    return Promise.reject(new Error(`Cannot get path to the attachment file ${attachment.title}`));
  }

  public open(path, mimeType): Promise<any> {
    console.log(path, mimeType);
    return this.exec('open', {filePath: path, mimeType: mimeType || ''});
  }

  private exec(action, params) {
    return new Promise((resolve, reject) => {
      AttachmentsManager.cordovaRef.exec(resolve, reject, 'AttachmentsViewer', action, [params]);
    });
  }
}
