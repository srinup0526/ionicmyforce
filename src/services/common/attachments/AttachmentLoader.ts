import {SforceDataContext} from './../../../collections/SforceDataContext';
import { Utils } from '../../../utils/Utils';
import { AttachmentFileManager } from './AttachmentFileManager';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';


enum states {
  ERROR = -1,
  INITED = 0,
  DOWNLOAD = 1,
  FINISHED = 2
}

enum FILE_TRANSFER_ERROR_CODE {
  FILE_NOT_FOUND_ERR = 1,
  INVALID_URL_ERR = 2,
  CONNECTION_ERR = 3,
  ABORT_ERR = 4,
  NOT_MODIFIED_ERR = 5
}

export class AttachmentLoader {
  public static states = states;
  public attachment;

  public onStateChange;
  public onFailLoad;
  public onSuccessLoad;

  private status: states;
  private fileSize: number;
  private loadedSize: number;
  private sourceUrl: string;
  private destPath: string;
  private fileTransferObject: FileTransferObject;


  constructor(attachment, private fileManager: AttachmentFileManager, private fileTransfer: FileTransfer) {
    this.status = AttachmentLoader.states.INITED;
    this.attachment = attachment;
    this.fileSize = this.attachment.bodyLength || 0;
    this.loadedSize = 0;
    this.sourceUrl = this.attachment.body;
    this.destPath = this.fileManager.getFilePath(this.attachment.body, this.attachment.title, false);
  }

  private getRequestHeaders(credentials): {headers: {Authorization: string}} {
    return {
      headers: {
        Authorization: `OAuth ${credentials.accessToken}`
      }
    };
  }

  private progressChanged(progress): void {
    if (progress.total > 0) {
      this.fileSize = progress.total;
    }

    this.loadedSize = progress.loaded;

    if (this.onStateChange) {
      this.onStateChange(this.status, {current: this.loadedSize, total: this.fileSize});
    }
  }

  private errorHandler(error): void {
    this.status = AttachmentLoader.states.ERROR;
    this.fileManager.removeFile(this.destPath);

    if (!Utils.deviceIsOnline()) {
      error = {code: FILE_TRANSFER_ERROR_CODE.CONNECTION_ERR};
    }
    if (this.onFailLoad) {
      this.onFailLoad(error);
    }
  }

  private successHandler(entry): Promise<any> {
    return this.fileManager.moveToPersistent(entry.fullPath)
      .then((moveEntry) => {
        this.status = AttachmentLoader.states.FINISHED;
        this.loadedSize = this.fileSize;

        if (this.onStateChange) {
          this.onStateChange(this.status, {current: this.loadedSize, total: this.fileSize});
        }
        if (this.onSuccessLoad) {
          this.onSuccessLoad(moveEntry);
        }
      })
      .catch(this.errorHandler.bind(this));
  }

  public download(): Promise<any> {
    const fileTransferObject = this.fileTransfer.create();

    return SforceDataContext.getAuthCredentials()
      .then((credentials) => {

        const headers = this.getRequestHeaders(credentials);
        const url = credentials.instanceUrl + this.sourceUrl;

        this.status = AttachmentLoader.states.DOWNLOAD;

        fileTransferObject.onProgress = this.progressChanged.bind(this);

        console.log(this.destPath);

        return fileTransferObject.download(url, this.destPath, true, headers)
          .then((entry) => {
            return this.successHandler(entry);
          })
          .catch((error) => {
            return this.errorHandler(error);
          });
      })
      .catch((error) => {
        return this.errorHandler(error);
      });
  }

  public abort(): void {
    if (!this.fileTransferObject) {
      return;
    }

    return this.fileTransferObject.abort();
  }

}
