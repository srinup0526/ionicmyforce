import { AttachmentLoader } from './AttachmentLoader';
import {Injectable} from '@angular/core';
import { AttachmentFileManager } from './AttachmentFileManager';
import { FileTransfer } from '@ionic-native/file-transfer';


@Injectable()
export class AttachmentLoadManager{

  private _queue;

  constructor(private fileManager: AttachmentFileManager, private fileTransfer: FileTransfer) {
    this._queue = {};
  }

  public getLoaderForAttachment(attachment) {
    if (!attachment) {
      return null;
    }

    const loader = this._queue[attachment.id];

    return loader ? loader : null;
  }

  public getQueuedPresentations(): Array<any> {
    return Object.keys(this._queue);
  }

  public queue(attachment, callbacks) {
    let loader = null;

    if (!this._queue[attachment.id]) {
      loader = new AttachmentLoader(attachment, this.fileManager, this.fileTransfer);

      ['onStateChange', 'onFail', 'onSuccess'].forEach((callbackName) => {
        if (callbacks && callbacks[callbackName]) {
          loader[callbackName] = callbacks[callbackName];
        }

        if (callbackName !== 'onStateChange') {
          loader[`${callbackName}Load`] = () => {
            this.dequeue(attachment);

            if (loader[callbackName]) {
              // @ts-ignore
              loader[callbackName].apply(loader[callbackName], arguments);
            }
          };
        }
      });

      this._queue[attachment.id] = loader;
    } else {
      console.log(`AttachmentLoader ${attachment.id} already queued!`);
    }
    return loader;
  }

  public queueInvoke(attachment, callbacks) {
    const loader = this.queue(attachment, callbacks);

    if (loader) {
      return loader.download();
    }

    callbacks.onFail(new Error(`AttachmentLoader ${attachment.id} already queued`));
  }

  private dequeue(attachment) {
    const loader = this._queue[attachment.id];

    delete this._queue[attachment.id];

    return loader;
  }

  private dequeueInvoke(attachment) {
    const loader = this.dequeue(attachment);
    if (!loader) {
      return null;
    }

    if (loader) {
      loader.abort();
    }
    return loader;
  }

}
