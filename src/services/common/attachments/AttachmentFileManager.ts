import { Utils } from '../../../utils/Utils';
import {Injectable} from '@angular/core';
import {FileService} from '../FileService';

@Injectable()
export class AttachmentFileManager {
  private readonly attachmentsRoot: string = 'attachments';
  private readonly temporaryRoot: string = 'temporary';

  constructor(private fileService: FileService) {}

  get rootPath() {
    const storageDirectory = this.fileService.storageDirectory || '';
    return AttachmentFileManager.processRootPath(storageDirectory);
  }

  public static processRootPath(url: string): string {
    url = url.replace('file://localhost', '');

    return url;
  }

  private pathToTemporary(): string {
    return `${this.rootPath}/${this.temporaryRoot}`;
  }

  private pathToAttachments(): string {
    return `${this.rootPath}/${this.attachmentsRoot}`;
  }

  public extractFileName(filePath: string): string {
    const regExp = /Attachment\/(.*)\/Body/gim;
    const regExpData = regExp.exec(filePath);

    return regExpData ? regExpData[1] : filePath.replace(/\W/gim, '');
  }

  public extractFileExtension(fileName: string): string {
    const regExp = /\.([\w\d]+$)/gim;
    const regExpData = regExp.exec(fileName);

    return regExpData ? regExpData[1] : fileName.replace(/\W/gim, '');
  }

  public getFilePath(url: string, name: string, isPersistent: boolean = true): string {
    const fileName = this.extractFileName(url);
    const fileExt = this.extractFileExtension(name);
    const rootDir = isPersistent ? this.pathToAttachments() : this.pathToTemporary();
    const filePath = `${rootDir}/${fileName}.${fileExt}`;

    return filePath;
  }

  public moveToPersistent(sourcePath: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const absoluteSourcePath = AttachmentFileManager.processRootPath(sourcePath);
      const pathToAttachments = this.attachmentsRoot;
      return this.fileService.getDirectoryByAbsolute(this.rootPath)
        .then((dirEntry) => {
          dirEntry.getDirectory(pathToAttachments, {create: true, exclusive: false}, (attachmentsDirEntry) => {
            return this.fileService.getFileEntry(absoluteSourcePath, {create: false, exclusive: false})
              .then((fileEntry) => {
                return fileEntry.moveTo(attachmentsDirEntry, fileEntry.name, resolve, reject);
              })
              .catch(reject);
        });
      })
      .catch(reject);
    });
  }

  public fileExist(directory: string, file: string): Promise<any> {
    return this.fileService.checkFile(directory, file);
  }

  public removeFile(filePath: string): Promise<any> {
    return this.fileService.removeLocalFile(filePath);
  }

  public removeFolder(path: string): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.fileService.removeDirectoryByPath(path)
        .then((data) => resolve(data))
        .catch((fileError) => {
          if (fileError.code === 12 || fileError.code === 1) {
            return resolve();
          }
          return reject(fileError);
        });
    });
  }

  public wipeStorage(): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.fileService.getDirectory(this.attachmentsRoot)
        .then(this.removeDirectory.bind(this))
        .then(resolve)
        .catch((error) => {
          if (error.code === 12 || error.code === 1) {
            resolve(null);
          } else {
            reject(error);
          }
        });
    });
  }

  private removeDirectory(dirEntry) {
    return new Promise((resolve, reject) => {
      dirEntry.removeRecursively(resolve, reject);
    });
  }
}
