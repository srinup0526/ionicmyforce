import {Injectable} from '@angular/core';
import { File as IonicNativeFile, Entry, FileError, DirectoryEntry, FileEntry, IFile } from '@ionic-native/file';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { Platform } from '@ionic/angular';

const EMPTY_FILE_URL = '';
const STORAGE_NOT_AVAILABLE_MESSAGE = 'File storage is not available for current environment. '
  + 'Images will not be available offline';
declare const Ionic: any;


@Injectable()
export class FileService {
  private isCordova: boolean;
  private ft: FileTransferObject;

  constructor(private fileSystem: IonicNativeFile,
              private fileTransfer: FileTransfer,
              private platform: Platform) {

    this.isCordova = this.platform.is('cordova');

    if (!this.isStorageAvailable) {
      console.warn(STORAGE_NOT_AVAILABLE_MESSAGE);
    }
  }

  static getFileName(fireLink: string): string {
    return fireLink.slice(fireLink.lastIndexOf('/') + 1);
  }

  get storageDirectory() {
    return this.getStorageDirectory();
  }
  
  get applicationDirectory() {
    return this.fileSystem.applicationDirectory;
  }

  get isStorageAvailable() {
    return !!this.storageDirectory;
  }
  
  public getFileFromApplication(path: string): Promise<any> {
    return this.fileSystem.readAsText(this.applicationDirectory, path);
  }

  public downloadFile(downloadUrl: string, localFilePath: string, progressCallBack?: any): Promise<string> {
    this.ft = this.fileTransfer.create();
    if (progressCallBack) this.ft.onProgress(progressCallBack);
    return this.ft.download(downloadUrl, localFilePath)
      .then((entry: Entry) => {
        const url = Ionic.WebView && Ionic.WebView.convertFileSrc(entry.nativeURL) || entry.nativeURL;
        return url;
      });
  }

  public abortDownload(): void {
    this.ft.abort();
  }

  public getDirectoryByAbsolute(absPath: string): Promise<DirectoryEntry> {
    return this.fileSystem.resolveDirectoryUrl(absPath);
  }

  public getDirectory(path: string): Promise<DirectoryEntry> {
    const dirPath = this.getStorageDirectory() + path;
    return this.fileSystem.resolveDirectoryUrl(dirPath);
  }

  public getDirectoryFilesList(dirEntry: DirectoryEntry): Promise<any> {
    return new Promise((resolve, reject) => {
      const directoryReader = dirEntry.createReader();
      directoryReader.readEntries(resolve, reject);
    });
  }

  public removeDirectory(dirEntry: DirectoryEntry): Promise<void> {
    return new Promise((resolve, reject) => {
      dirEntry.removeRecursively(resolve, reject);
    });
  }

  public removeFile(entry: Entry): Promise<void> {
    return new Promise((resolve, reject) => {
      entry.remove(resolve, reject);
    });
  }

  private createLocalFilePath(storageFilePath: string): string {
    return `${this.getStorageDirectory()}${storageFilePath}`;
  }

  public getStorageDirectory(): string {
    return this.fileSystem.externalDataDirectory || this.fileSystem.dataDirectory;
  }


  private loadLocalFile(localFilePath: string): Promise<string> {
    return this.fileSystem.resolveLocalFilesystemUrl(localFilePath)
      .then((entry: Entry) => {
        return entry.isFile ? entry.nativeURL : '';
      })
      .catch((error: FileError) => {
        const errorMessage: string = error.message || error.toString();

        throw new Error(
          `Failed to get local file path for file '${localFilePath}': ${errorMessage}`);
      });
  }

  public removeLocalFile(remoteFilePath: string): Promise<any> {
    let localFilePath;

    if (remoteFilePath.indexOf('file:///') === 0) {
      localFilePath = remoteFilePath;
    } else {
      localFilePath = this.createLocalFilePath(remoteFilePath);
    }
    return this.fileSystem.resolveLocalFilesystemUrl(localFilePath)
      .then((entry: Entry) => this.removeFile(entry))
      .catch(error => {

        if (error.code === 1 || error.code === 12) {
          return Promise.resolve('Remove local file. File not found');
        }
        console.warn(`Remove local file. There are some problems with local removing file ${localFilePath}`, error);
      });

  }

  public getFileEntry(path: string, options?): Promise<FileEntry> {
    const fileName = FileService.getFileName(path);

    return this.getDirectory(path.replace(fileName, ''))
      .then((dirEntry: DirectoryEntry) => this.fileSystem.getFile(dirEntry, fileName, options));
  }

  public getFile(path: string, newFileName?: string): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.getFileEntry(path)
        .then((fileEntry: FileEntry) => {

          fileEntry.file((cordovaFile: IFile) => {
            const reader = new FileReader();
            reader.onloadend = () => {

              if (reader.error) {
                reject(reader.error);
              } else {
                const blob: any = new Blob([reader.result], {type: cordovaFile.type});
                blob.lastModifiedDate = new Date();
                blob.name = newFileName || cordovaFile.name;
                resolve(blob as File);
              }
            };
            reader.readAsArrayBuffer(cordovaFile);
          });
        })
        .catch(error => {
          console.error('Get file object. ', error);

          reject(error);
        });
    });
  }

  public checkFile(directory: string, file: string): Promise<boolean> {
    return this.fileSystem.checkFile(directory, file);
  }

  public writeFile(file: Blob, path: string, fileName: string): Promise<FileEntry> {
    return this.createRecursivePath(path)
      .then((dirPath) => {
        return this.fileSystem.writeFile(dirPath, fileName, file, {replace: true});
      })
      .then((fileEntry: FileEntry) => fileEntry);
  }


  private createRecursivePath(path: string): Promise<string> {
    const dirTree = path.split('/').filter(dir => !!dir);
    const dirName = dirTree.pop();
    const dirPath = this.createLocalFilePath(dirTree.join('/'));

    return this.fileSystem.checkDir(dirPath, dirName)
      .then(isExist => {
        return dirPath;
      })
      .catch(err => {
        return this.fileSystem.createDir(dirPath, dirName, true)
          .then(response => {
            return dirPath;
          });
      });
  }

  public removeDirectoryByPath(path: string): Promise<void> {
    return this.getDirectory(path)
      .then((dirEntry: DirectoryEntry) => {
        return this.removeDirectory(dirEntry);
      });
  }
}
