export class DeviceManager {
  static device;
  static _appVersion: string;

  static deviceModel(): string {
    if (DeviceManager.device) {
      return DeviceManager.device.model;
    }
    return 'Mobile device';
  }

  static osVersion(): string {
    if (DeviceManager.device) {
      return `${DeviceManager.device.platform} ${DeviceManager.device.version}`;
    }

    return '1.0';
  }

  static appVersion(): string {
    if (DeviceManager._appVersion) {
      return DeviceManager._appVersion;
    }

    return '1.0';
  }

  static deviceId(): string {
    if (DeviceManager.device) {
      return DeviceManager.device.uuid;
    }
    return '40:F3:08:62:D4:2D';
  }
  
  static setDevice(device): void{
    DeviceManager.device = device;
  }

  static setAppVersion(appVersion: string): void{
    if(appVersion) {
      DeviceManager._appVersion = DeviceManager.getValidatedVersion(appVersion);
    } else {
      DeviceManager._appVersion = '1.0';
    }
  }
  
  static getValidatedVersion(appVersion: string): string {
    const versionRegex = /\((\d+(:?\.\d+)*).*\)/;
    
    if (versionRegex.test(appVersion)) {
      return versionRegex.exec(appVersion)[1];
    }

    return appVersion;
  }
}


