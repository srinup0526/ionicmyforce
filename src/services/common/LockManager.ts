import SettingsManager from './../db/SettingsManager';
import { Nav } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { PinComponent } from "../../components/pin/pin/pin";

@Injectable()
export class LockManager {
  private isActive: boolean;
  private pin: PinComponent;
  private timeStamp: Date;
  private lockTime: number;
  private timer;
  
  constructor() {}
  
  public init(pin: PinComponent) {
    this.isActive = false;
    this.pin = pin;
    this.lockTime = 15 * 60 * 1000;
  }
  
  public initialize(): void {
    this.addEventListener();
    this.startTimer();
  }
  
  public destroy() {
    this.clearTimer();
    this.removeEventListener();
    this.timer = null;
  }
  
  public lock(): Promise<any> {
    this.stop();
    
    return this.setIsLocked(true)
      .then(() => this.gotoPinPage());
  }
  
  public unlock(): Promise<any> {
    this.start();
    
    return this.setIsLocked(false)
      .then(() => this.gotoMainPage());
  }
  
  public isLocked(): Promise<any> {
    return this.getIsLocked();
  }
  
  public start() {
    this.isActive = true;
    this.initialize();
  }
  
  public stop() {
    this.isActive = false;
    this.destroy();
  }
  
  private clearTimer(): void {
    clearTimeout(this.timer);
  }
  
  private startTimer(): void {
    this.clearTimer();
    this.timeStamp = new Date();
    this.timer = this.executeAsync(this.lock.bind(this), this.lockTime);
  }
  
  private executeAsync(callback, timeout: number = 10) {
    if (callback) {
      return setTimeout(callback, timeout);
    }
  }
  
  private addEventListener(): void {
    document.addEventListener('touchend', this.startTimer.bind(this));
  }
  
  private removeEventListener(): void {
    document.removeEventListener('touchend', this.startTimer.bind(this));
  }
  
  private setIsLocked(value): Promise<any> {
    return SettingsManager.setValueByKey('IsLocked', value);
  }
  
  private getIsLocked(): Promise<any> {
    return SettingsManager.getValueByKey('IsLocked');
  }
  
  private gotoMainPage(): void {
    this.pin.hide();
  }
  
  private gotoPinPage(): void {
    this.pin.show();
  }
}
