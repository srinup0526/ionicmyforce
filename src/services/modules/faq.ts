import { Injectable } from '@angular/core';
import {FAQCollection} from "../../collections/FaqCollection";
import {FAQAttachmentCollection} from "../../collections/FaqAttachmentsCollection";
import { FAQ } from './../../models/Faq';

@Injectable()
export class FaqService {
  constructor(private faqCollection: FAQCollection, private faqAttachmentCollection: FAQAttachmentCollection) {}

  public fetchFAQ(): Promise<Array<FAQ>> {
    return this.faqCollection.fetchAllOrdered()
      .then(this.fetchAttachments.bind(this));
  }

  private fetchAttachments(list: Array<FAQ>): Promise<Array<FAQ>> {
    const deferredArray = list.map((faq) => {
      return this.faqAttachmentCollection.getAllAttachmentsForFaqId(faq.id)
        .then((attachments) => faq.setAttachments(attachments));
    });

    return Promise.all(deferredArray)
      .then(() => {
        return list;
      });
  }
}
