import { Injectable } from '@angular/core';
import {IqviaQuestionsCollection} from "../../collections/IqviaQuestionsCollection";
import {IqviaAccountSkuTasksCollection} from "../../collections/IqviaAccountSkuTasksCollection";
import {IqviaTaskAdjustmentsCollection} from "../../collections/IqviaTaskAdjustmentsCollection";

import { IqviaQuestion } from './../../models/IqviaQuestion';
import { IqviaAccountSkuTask } from './../../models/IqviaAccountSkuTask';
import { IqviaTaskAdjustment } from './../../models/IqviaTaskAdjustment';

@Injectable()
export class TradeModuleService {
  constructor(private iqviaQuestionsCollection: IqviaQuestionsCollection,
              private iqviaAccountSkuTasksCollection: IqviaAccountSkuTasksCollection,
              private iqviaTaskAdjustmentsCollection: IqviaTaskAdjustmentsCollection) {}

  public fetchQuestions(): Promise<Array<IqviaQuestion>> {
    return this.iqviaQuestionsCollection.fetchAllQuestions();
  }

  private fetchAccountSkuTasks(organizationId: string, date: string): Promise<Array<IqviaAccountSkuTask>> {
    return this.iqviaQuestionsCollection.fetchAllForOrganizationByDate(organizationId, date);
  }

  public fetchTaskAdjustments(organizationId: string): Promise<Array<IqviaTaskAdjustment>> {
    return this.iqviaTaskAdjustmentsCollection.fetchAll()
      .then(this.iqviaTaskAdjustmentsCollection.getAllEntitiesFromResponse.bind(this));
  }
}
