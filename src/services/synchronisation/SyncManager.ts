import {Utils} from '../../utils/Utils';
import {EntitiesCollection} from './../../collections/EntitiesCollection';
import {DeviceCollection} from './../../collections/DeviceCollection';
import {Settings} from './../db/Settings';
import SettingsManager from './../db/SettingsManager';
import {LogManager} from './../common/LogManager';
import {SyncMap} from './SyncMap';
import {SforceDataContext} from './../../collections/SforceDataContext';
import {DatabaseManager} from './../../services/db/DatabaseManager';
import {ConfigurationManager} from './../../services/db/ConfigurationManager';
import AutosyncSettingsManager from './../../services/autosync/AutosyncSettingsManager';
import {ISyncManager} from './ISyncManager';
import {PicklistManager} from './../../services/db/picklist-managers/base/PicklistManagers';
import {LocalizationManager} from './../../services/common/localizations/LocalizationManager';
import {Injectable} from '@angular/core';
import {UsersCollection} from "../../collections/UsersCollection";


@Injectable()
export class SyncManager implements ISyncManager {
  static readonly SYNC_KEY: string = 'synchronizationStatus';
  static readonly SYNC_STATUS_PROGRESS: string = 'progress';
  static readonly SYNC_STATUS_SUCCESS: string = 'success';
  static readonly SYNC_STATUS_FAIL: string = 'fail';

  STEPS_COUNT: number;
  statusCB: any;

  stepIndex: number;
  isErrorDuringSync: boolean;
  isErrorDuringUpload: boolean;

  constructor(private syncMap: SyncMap,
              private databaseManager: DatabaseManager,
              private localizationManager: LocalizationManager,
              private syncLogManager: LogManager,
              private usersCollection: UsersCollection,
              private deviceCollection: DeviceCollection) {
  }

  public startLoading(statusCB): Promise<any> {
    this.setStepCount();
    this.statusCB = statusCB;
    return this.load();
  }

  public load(): Promise<any> {
    this.syncLogManager.initLog();

    this.stepIndex = 0;
    this.isErrorDuringSync = false;
    this.isErrorDuringUpload = false;
    
    return this.downloadDeviceAndCheckErase();
  }
  
  private setStepCount() {
    this.STEPS_COUNT = this.syncMap.map && this.syncMap.map.length ? this.syncMap.map.length : 100;
  }

  private async downloadDeviceAndCheckErase(): Promise<any> {
    const checkDeviceLabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.CheckDevice');
    
    this.updateStatus(checkDeviceLabel);

    return this.deviceCollection.serverConfig()
      .then((config) => {
        return this.deviceCollection.sync('read', null, { config, cache: this.deviceCollection.cache });
      })
      .then(() => this.deviceCollection.getDevice())
      .then((device) => {
        if (device && device.requestErase) {
          return this.eraseDeviceAndLogout(device);
        }
        return AutosyncSettingsManager.addToQueueWithContinueAfterFuncPromise(this.makeSync.bind(this), this.onAutosyncProcess.bind(this));
      })
      .catch((error) => {
        console.error(error);
      });
  }

  private async onAutosyncProcess(table, count) {
    const synchronizationPopupUploadingLabel = await this.localizationManager.promiseLocale('synchronizationPopup.Uploading');
    const synchronizationPopupTableLabel = await this.localizationManager.promiseLocale('synchronizationPopup.Table.' + table);
    
    return this.statusCB(`${synchronizationPopupUploadingLabel} ${synchronizationPopupTableLabel}`, count);
  }

  private eraseDeviceAndLogout(device) {
    device.erased = true;

    return this.deviceCollection.updateEntity(device)
      .then(() => this.deviceCollection.serverConfig())
      .then((config) => {
        const options = {
          config,
          cache: this.deviceCollection.cache,
          each: this.onEntityUpload
        };

        return this.deviceCollection.sync('upsert', null, options);
      })
      .then(() =>
        this.databaseManager
        .clearDatabase()
        .then(() => SforceDataContext.logout()));
  }

  public generateSyncSteps(settings) {
    return this.syncMap.map.reduce((promise, syncStep: any) => {
      console.log("syncStep",syncStep);
      console.log("syncstepConditional",syncStep.conditional);
      if (syncStep.conditional && !Utils.isPositiveConditionBySettings(settings, syncStep.conditional)) {
        return promise.then(this.skipStep.bind(this));
      }

      const targetInstance = (syncStep.collection || syncStep.picklist || syncStep.manager);
      const target = targetInstance;
      let method;

      if (syncStep.collection && syncStep.action === 'upload') {
        method = this.uploadEntitiesForCollection.bind(this);
      } else if (syncStep.collection && syncStep.action === 'download') {
        method = this.downloadEntitiesForCollection.bind(this);
      } else if (!!syncStep.manager) {
        method = this.doManagerTask;
      } else {
        method = this.downloadPicklistWithDatasource.bind(this);
      }

      return promise.then(() => {
        return method(target);
      });
    }, Promise.resolve());
  }

  // TODO: refactor sync by group (make one sync function to each module) if it possible

  async makeSync() {
    const params = {count: LogManager.stepIndexKey};
    const infoLog = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.SyncStart', params);

    return Promise.resolve(this.saveInfoLog(infoLog))
      .then(this.setSynchronizationStatusProgress.bind(this))
      .then(this.syncUsers.bind(this))
      .then(this.downloadConfigurationData.bind(this))
      .then(this.processConfiguration.bind(this))
      .then(() => SettingsManager.getSettings())
      .then(this.generateSyncSteps.bind(this))
      .then(this.displaySynchronizationFinishedPopup.bind(this))
      .catch(this.displaySynchronizationFinishedPopup.bind(this));
  }

  private displaySynchronizationFinishedPopup() {
    if (!this.isErrorDuringSync) {
      return this.onSynchronisationSucceeded();
    }

    return this.onSynchronisationFailed();
  }

  private skipStep() {
    --this.STEPS_COUNT;

    return Promise.resolve();
  }

  private async saveSuccessLogAfterDownloadCollection(totalSize, collection) {
    const tableLabel = await this.localizationManager.promiseLocale(`synchronizationPopup.Table.${collection.scheme.table}`);

    const infoLogLabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.DownloadOk', {
      name: tableLabel,
      count: totalSize
    });
    
    return this.saveInfoLog(infoLogLabel);
  }

  private async saveErrorLogAfterDownloadCollection(error, collection) {
    const tableLabel = await this.localizationManager.promiseLocale(`synchronizationPopup.Table.${collection.scheme.table}`);
  
    const infoLog = await this.localizationManager.promiseLocale( 'synchronizationPopup.LogMessage.DownloadFail', {
      name: tableLabel
    });
    
    console.error(error);
    
    return this.saveInfoLog(infoLog)
      .then(() => {
        if (error) {
          return this.saveErrorLog(error.responseJSON || error.statusText || error);
        }

        return Promise.resolve();
      });
  }

  private async downloadEntitiesForCollection(collection) {
    const tableLabel = await this.localizationManager.promiseLocale(`synchronizationPopup.Table.${collection.scheme.table}`);
    
    this.updateStatus(tableLabel);

    const onlyIds = false;
    const loadDynamicLayout = true;

    return collection.serverConfig(onlyIds, loadDynamicLayout)
      .then((config) => {
        return collection.sync('read', null, {config: config, cache: collection.cache});
      })
      .then(
        ((totalSize) => this.saveSuccessLogAfterDownloadCollection(totalSize, collection)),
        ((error) => this.saveErrorLogAfterDownloadCollection(error, collection))
      );
  }

  private doManagerTask(manager) {
    return manager.doTaskDuringSync();
  }

  private async uploadEntitiesForCollection(collection) {
    const tableLabel = await this.localizationManager.promiseLocale(`synchronizationPopup.Table.${collection.scheme.table}`);
    const uploadLabel =  await this.localizationManager.promiseLocale('synchronizationPopup.Uploading');
  
    this.updateStatus(`${uploadLabel} ${tableLabel}`);

    return collection.serverConfig()
      .then((config) => {
        return collection.sync('upsert', null, {config, cache: collection.cache, each: this.onEntityUpload.bind(this)});
      })
      .then((async (records) => {
          const uploadOklabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.UploadOk', {
            name: tableLabel,
            count: records && records.length ? records.length : 0
          });
          
          return this.saveInfoLog(uploadOklabel);
        }),
        ((error) => this.saveErrorLogAfterDownloadCollection(error, collection))
      );
  }

  private async onEntityUpload(entity, error) {
    if (!error) {
      return;
    }

    if (error.type === EntitiesCollection.TYPE_WARNING) {
      let warningParams: any = {
        type: entity.sobjectType,
        entity: entity.id
      };
  
      warningParams = Utils.extend(warningParams, this.parseErrorData(error));
  
      const warningUploadingLabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.WarningUploading', warningParams);
  
      return this.saveWarning(warningUploadingLabel);
    } else {
      this.isErrorDuringUpload = true;

      let errorParams: any = {
        type: entity.sobjectType,
        entity: entity.id
      };

      errorParams = Utils.extend(errorParams, this.parseErrorData(error));
  
      if(!errorParams.code && errorParams.message  == 'error by status'){
        errorParams.message = error.toString();
        errorParams.code = "in message";
      }
  
      const errorUploadingLabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.ErrorUploading', errorParams);
      
      return this.saveErrorLog(errorUploadingLabel, true);
    }
  }

  parseErrorData(error) {
    const errorInfo: any = {};

    if (error.details && error.details.message) {
      errorInfo.message = error.details.message;
    } else {
      errorInfo.message = error.message || 'error by status';
    }

    if (error.details && error.details.errorCode) {
      errorInfo.code = error.details.errorCode;
    } else {
      errorInfo.code = error.errorCode || error.status;
    }

    return errorInfo;
  }

  private updateStatus(status) {
    let count = Math.round((this.stepIndex / (this.STEPS_COUNT - 1)) * 100);

    if (count > 100) {
      console.log('>>>> NEED + 1 increment');
      count = 100;
    }

    this.statusCB(status, count);

    if (this.stepIndex < this.STEPS_COUNT - 1) {
      ++this.stepIndex;
    }
  }

  private saveWarning(warning) {
    return this.syncLogManager.appendWarning(warning)
      .then((warn) => Promise.reject(warn));
  }

  private saveErrorLog(error, isUploadError?) {
    // $.fn.dpToast JSON.stringify error
    console.error(error);

    if (!isUploadError) {
      this.isErrorDuringSync = true;
    }

    return this.syncLogManager.appendError(error)
      .then((err) => Promise.reject(err));
  }

  private saveInfoLog(infoLog) {
    return this.syncLogManager.appendInfoLog(infoLog);
  }

  private async saveSuccessLogAfterDownloadConfigurationData(records) {
    const configurationLabel = await this.localizationManager.promiseLocale('synchronizationPopup.Configuration');
    
    const downloadLabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.DownloadOk', {
      name: configurationLabel,
      count: ''
    });
    
    return this.saveInfoLog(downloadLabel);
  }

  private async downloadConfigurationData() {
    const configurationLabel = await this.localizationManager.promiseLocale('synchronizationPopup.Configuration');
    
    this.updateStatus(configurationLabel);

    return Promise.all([
      SettingsManager.loadConfig(),
      ConfigurationManager.loadConfig()
      ])
      .then(this.saveSuccessLogAfterDownloadConfigurationData.bind(this))
      .then(() => {
        return Settings.getInstance().reload();
      })
      .catch((error) => {
        console.error(error);

        return this.saveErrorLog(error);
      });
  }

  private saveSuccessLogAfterDownloadPickList(records, datasource) {
    return this.appendSuccessPicklistDownloadSyncLog(datasource.targetModel().table, records.length);
  }

  private async saveErrorLogAfterDownloadPickList(error, datasource) {
    const tableLabel = await this.localizationManager.promiseLocale(`synchronizationPopup.Table.${datasource.targetModel().table}`);
    
    const infoLog = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.DownloadFail', {
      name: tableLabel
    });
    
    return this.saveInfoLog(infoLog)
      .then(() => this.saveErrorLog(error));
  }

  private async downloadPicklistWithDatasource(datasource) {
    const pickListLabel = await this.localizationManager.promiseLocale('synchronizationPopup.PickList');
    const tableLabel = await this.localizationManager.promiseLocale(`synchronizationPopup.Table.${datasource.targetModel().table}`);
    
    this.updateStatus(`${tableLabel} ${pickListLabel}`);

    return PicklistManager.loadPicklist(datasource.targetModel().sfdcTable, datasource.fieldNames())
      .then(
        ((records) => this.saveSuccessLogAfterDownloadPickList(records, datasource)),
        ((error) => this.saveErrorLogAfterDownloadPickList(error, datasource))
      );
  }

  private async appendSuccessPicklistDownloadSyncLog(picklistEntityTableName, recordsCount) {
    const pickListLabel = await this.localizationManager.promiseLocale('synchronizationPopup.PickList');
    const tableLabel = await this.localizationManager.promiseLocale(`synchronizationPopup.Table.${picklistEntityTableName}`);
    const picklistName = `${tableLabel} ${pickListLabel}`;
    const downloadOkLabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.DownloadOk', {
      name: picklistName,
      count: recordsCount
    });
  
    this.saveInfoLog(downloadOkLabel);
  }

  private onSynchronisationSucceeded() {
    this.syncLogManager.updateStepsCount(this.stepIndex - 1);

    return this.setSynchronisationSucceededLog()
      .then(() => this.deviceCollection.updateDeviceInfo())
      .then(() => this.setLastSucceededSyncronisationDateTime())
      .then(() => this.setActiveUserFullName())
      .then(() => this.uploadEntitiesForCollection(this.deviceCollection))
      // .then(() => AlarmManager.scheduleNextVisits())
      .then(this.setSynchronizationStatusSuccess.bind(this))
      .catch((error) => console.error(error));
  }

  private async setSynchronisationSucceededLog() {
    const errorLabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.Error');
    const successLabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.Success');
    
    if (this.isErrorDuringUpload) {
      return this.saveInfoLog(errorLabel);
    }

    return this.saveInfoLog(successLabel);
  }

  private setLastSucceededSyncronisationDateTime() {
    return this.deviceCollection.getDevice()
      .then((device) => {
        return Settings.getInstance().setLastSucceededSyncDateTime(device.lastSyncronisation);
      });
  }

  private async onSynchronisationFailed() {
    const errorLabel = await this.localizationManager.promiseLocale('synchronizationPopup.LogMessage.Error');
    
    this.syncLogManager.updateStepsCount(this.stepIndex - 1);

    return this.saveInfoLog(errorLabel)
      .then(() => this.deviceCollection.updateDeviceInfo())
      .then(() => this.uploadEntitiesForCollection(this.deviceCollection))
      .then(this.setSynchronizationStatusFail.bind(this))
      .then((failed) => Promise.reject(failed));
  }

  private processConfiguration() {
    return ConfigurationManager.getConfig('isDynamicAgendaEnabled')
      .then((value) => SettingsManager.setValueByKey('isDynamicAgendaEnabled', value))
      .then(() => ConfigurationManager.getConfig('isEdetailingEnabled'))
      .then((value) => SettingsManager.setValueByKey('isEdetailingEnabled', value))
      .then(() => ConfigurationManager.getConfig('isJuridicGroupEnabled'))
      .then((value) => SettingsManager.setValueByKey('isJuridicGroupEnabled', value))
      .then(() => ConfigurationManager.getConfig('tourPlanningSettings'))
      .then((value) => SettingsManager.setValueByKey('tourPlanningSettings', value))
      .then(() => ConfigurationManager.getConfig('callReportTypeSettings'))
      .then((value) => SettingsManager.setValueByKey('callReportTypeSettings', value))
      .then(() => ConfigurationManager.getConfig('isMtpModuleEnabled'))
      .then((value) => SettingsManager.setValueByKey('isMtpModuleEnabled', value))
      .then(() => ConfigurationManager.getConfig('sampleManagementSettings'))
      .then((value) => SettingsManager.setValueByKey('sampleManagementSettings', value))
      .then(() => ConfigurationManager.getConfig('isOrderManagementEnabled'))
      .then((value) => SettingsManager.setValueByKey('isOrderManagementEnabled', value))
      .then(() => ConfigurationManager.getConfig('isMedicalModuleEnabled'))
      .then((value) => SettingsManager.setValueByKey('isMedicalModuleEnabled', value))
      .then(() => ConfigurationManager.getConfig('isConsents'))
      .then((value) => SettingsManager.setValueByKey('isConsents', value))
      .then(() => ConfigurationManager.getConfig('isItRedFlagRequired'))
      .then((value) => SettingsManager.setValueByKey('isItRedFlagRequired', value))
      .then(() => ConfigurationManager.getConfig('OrderManagementSettings'))
      .then((value) => SettingsManager.setValueByKey('OrderManagementSettings', value))
      .then(() => ConfigurationManager.getConfig('sampleManagementSettings2'))
      .then((value) => SettingsManager.setValueByKey('sampleManagementSettings2', value))
      .then(() => ConfigurationManager.getConfig('isSurveyModuleEnabled'))
      .then((value) => SettingsManager.setValueByKey('isSurveyModuleEnabled', value))
      .then(() => ConfigurationManager.getConfig('isDCREnabled'))
      .then((value) => SettingsManager.setValueByKey('isDCREnabled', value))
      .then(() => ConfigurationManager.getConfig('sampleManagementSettings'))
      .then((value) => SettingsManager.setValueByKey('isSampleManagementEnabled', value))
      .then(() => ConfigurationManager.getConfig('sampleManagementSettings'))
      .then((sampleSettings) => {
        const value = !!sampleSettings && sampleSettings.isGeolocationEnabled;
        return SettingsManager.setValueByKey('isGeolocationEnabled', value);
      })
      .then(() => ConfigurationManager.getConfig('prescribingHabitsSettings'))
      .then((prescribingHabitsSettings) => {
        const value = prescribingHabitsSettings && (prescribingHabitsSettings.allowToCreate || prescribingHabitsSettings.allowToDelete || prescribingHabitsSettings.allowToRead || prescribingHabitsSettings.allowToUpdate);
        return SettingsManager.setValueByKey('isPrescribingHabitsEnabled', value);
      });
  }

  private setSynchronizationStatusProgress() {
    return SettingsManager.setValueByKey(SyncManager.SYNC_KEY, SyncManager.SYNC_STATUS_PROGRESS);
  }

  private setSynchronizationStatusSuccess() {
    return SettingsManager.setValueByKey(SyncManager.SYNC_KEY, SyncManager.SYNC_STATUS_SUCCESS);
  }

  private setSynchronizationStatusFail() {
    return SettingsManager.setValueByKey(SyncManager.SYNC_KEY, SyncManager.SYNC_STATUS_FAIL);
  }

  private setActiveUserFullName() {
    return SforceDataContext.getActiveUser()
      .then((activeUser) => {
        if (activeUser) {
          const fullName = activeUser.fullName();

          return SettingsManager.setActiveUserFullName(fullName);
        }
      });
  }
  
  private syncUsers() {
    return this.downloadEntitiesForCollection(this.usersCollection);
  }
}
