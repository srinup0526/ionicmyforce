import { ProductPresentationsCollection } from '../../collections/ProductPresentationsCollection';
import { PresentationsCollection } from '../../collections/PresentationsCollection';
import { ProductsCollection } from '../../collections/ProductsCollection';
import { OrdersCollection } from '../../collections/OrdersCollection';
import { OrdersLineCollection } from '../../collections/OrdersLineCollection';
import { SurveyCollection } from '../../collections/SurveyCollection';
import { SurveyQuestionnaireCollection } from '../../collections/SurveyQuestionnaireCollection';
import { SurveyQuestionnaireDetailCollection } from '../../collections/SurveyQuestionnaireDetailCollection';
import { ReferenceCollection } from '../../collections/references/ReferenceCollection';
import { FAQCollection } from '../../collections/FaqCollection';
import { FAQAttachmentCollection } from '../../collections/FaqAttachmentsCollection';
import { IqviaAccountSkuTasksCollection } from '../../collections/IqviaAccountSkuTasksCollection';
import { IqviaQuestionsCollection } from '../../collections/IqviaQuestionsCollection';
import { IqviaTaskAdjustmentsCollection } from '../../collections/IqviaTaskAdjustmentsCollection';
import { OrderPicklistManager } from '../db/picklist-managers/OrderPicklistManager';
import { TOTPicklistManager } from '../db/picklist-managers/TOTPicklistManager';
import { OrganizationChinaPickListManager } from '../db/picklist-managers/OrganizationChinaPickListManager';
import { DataChangeRequestPickListManager } from '../db/picklist-managers/DataChangeRequestPickListManager';

import { Injectable } from '@angular/core';
import { ContactsCollection } from "../../collections/ContactsCollection";
import { ContactPicklistManager } from "../../services/db/picklist-managers/ContactPicklistManager";
import { ContactChinaPicklistManager } from "../../services/db/picklist-managers/ContactChinaPicklistManager";
import { TargetFrequenciesCollection } from './../../collections/TargetFrequenciesCollection';
import { MarketingCyclesCollection } from './../../collections/MarketingCyclesCollection';
import { BricksCollection } from './../../collections/BricksCollection';
import {BuTeamPersonProfilesCollection} from "../../collections/BuTeamPersonProfilesCollection";
import {OrganizationsCollection} from "../../collections/OrganizationsCollection";
import {OrganizationPickListManager} from "../../services/db/picklist-managers/OrganizationPicklistmanager";

import {CallReportsCollection} from '../../collections/CallReportsCollection/CallReportsCollection';
import {DoctorConsentCollection} from '../../collections/DoctorConsentCollection';
import {DocumentCollection} from '../../collections/DocumentCollection';
import {LocationCollection} from '../../collections/LocationCollection';
import { CoachingPlanCollection } from './../../collections/CoachingPlanCollection';
import { JointVisitPlanCollection } from './../../collections/JointVisitPlanCollection';
import {MarketingMessagesCollection} from '../../collections/MarketingMessagesCollection';
import {MedicalCommentsCollection} from '../../collections/MedicalCommentsCollection';
import {MedicalQueriesCollection} from '../../collections/MedicalQueriesCollection/MedicalQueriesCollection';
import {DataChangeRequestsCollection} from '../../collections/DataChangeRequestsCollection';
import {RecordTypeCollection} from '../../collections/RecordTypeCollection';
import {MTPCollection} from '../../collections/MTPCollection';
import {PEAbbottAttendeesCollection} from '../../collections/PEAbbottAttendeesCollection';
import {PEAttendeesCollection} from '../../collections/PEAttendeesCollection';
import {PEEventExpenseCollection} from '../../collections/PEEventExpenseCollection';
import {PharmaEventsCollection} from '../../collections/PharmaEventsCollection';
import {ProductInPortfoliosCollection} from '../../collections/ProductInPortfoliosCollection';
import {ProductItemsCollection} from '../../collections/ProductItemsCollection';
import {ProductListingHistoriesCollection} from '../../collections/ProductListingHistoriesCollection';
import {ProductSegmentationsHistoriesCollection} from '../../collections/ProductSegmentationsHistoriesCollection';
import {ProductSegmentationsCollection} from '../../collections/ProductSegmentationsCollection';
import {RedFlagCollection} from '../../collections/RedFlagCollection';
import { PatchOrganizationCollection } from '../../collections/PatchOrganizationCollection';
import { PatchCollection } from '../../collections/PatchCollection';
import { SalesPlanningCollection } from '../../collections/SalesPlanningCollection';
import { SalesPlanningListCollection } from '../../collections/SalesPlanningListCollection';
import { SalesPlanningProductCollection } from '../../collections/SalesPlanningProductCollection';
import { PatchCustomerReferenceCollection } from '../../collections/PatchCustomerReferenceCollection';
//import {TargetFrequenciesCollection} from '../../collections/TargetFrequenciesCollection';
import {TargetCollection} from '../../collections/TargetCollection';
import {PatchCustomerCollection} from '../../collections/PatchCustomerCollection';
import {TotsCollection} from '../../collections/tots-collection/tots-collection';
import { PEPicklistManager } from '../../services/db/picklist-managers/PEPicklistManager';
import { CallReportPicklistManager } from '../../services/db/picklist-managers/CallReportPicklistManager';
import { MQPickListManager } from '../../services/db/picklist-managers/MQPickListManager';
import { PatchRecordTypePickListManager } from '../../services/db/picklist-managers/PatchRecordTypePickListManager';
import { ProductSegmentationPickListManager } from '../../services/db/picklist-managers/ProductSegmentationPickListManager';
import { PoweScorecardPicklistManager } from '../../services/db/picklist-managers/PoweScorecardPicklistManager';
import {BrandPlanningsCollection} from "../../collections/BrandPlanningsCollection";


@Injectable()
export class SyncMap {
  constructor(
    private bricksCollection: BricksCollection,
    private productsCollection: ProductsCollection,
    private productPresentationsCollection: ProductPresentationsCollection,
    private presentationsCollection: PresentationsCollection,
    private brandPlanningsCollection: BrandPlanningsCollection,
    private ordersCollection: OrdersCollection,
    private ordersLineCollection: OrdersLineCollection,
    private orderPicklistManager: OrderPicklistManager,
    private totPicklistManager: TOTPicklistManager,
    private faqCollection: FAQCollection,
    private faqAttachmentCollection: FAQAttachmentCollection,
    private referenceCollection: ReferenceCollection,
    private iqviaAccountSkuTasksCollection: IqviaAccountSkuTasksCollection,
    private iqviaQuestionsCollection: IqviaQuestionsCollection,
    private iqviaTaskAdjustmentsCollection: IqviaTaskAdjustmentsCollection,
    private surveyCollection: SurveyCollection,
    private surveyQuestionnaireCollection: SurveyQuestionnaireCollection,
    private surveyQuestionnaireDetailCollection: SurveyQuestionnaireDetailCollection,
    private contactsCollection: ContactsCollection,
    private targetFrequenciesCollection: TargetFrequenciesCollection,
    private marketingCyclesCollection: MarketingCyclesCollection,
    private buTeamPersonProfilesCollection: BuTeamPersonProfilesCollection,
    private organizationsCollection: OrganizationsCollection,
    private organizationChinaPickListManager: OrganizationChinaPickListManager,
    private callReportsCollection:CallReportsCollection,
    private doctorConsentCollection: DoctorConsentCollection,
    private documentCollection: DocumentCollection,
    private locationCollection: LocationCollection,
    private totsCollection: TotsCollection,
    private medicalCommentsCollection: MedicalCommentsCollection,
    private medicalQueriesCollection: MedicalQueriesCollection,
    private productItemsCollection: ProductItemsCollection,
    private pharmaEventsCollection: PharmaEventsCollection,
    private mtpCollection : MTPCollection,
    private pePicklistManager : PEPicklistManager,
    private callReportPicklistManager : CallReportPicklistManager,
    private peAttendeesCollection : PEAttendeesCollection,
    private mqPickListManager : MQPickListManager,
    private patchCustomerCollection : PatchCustomerCollection,
    private patchRecordTypePickListManager:PatchRecordTypePickListManager,
    private redFlagCollection:RedFlagCollection,
    private coachingPlanCollection:CoachingPlanCollection,
    private jointVisitPlanCollection: JointVisitPlanCollection,
    private salesplanningCollection: SalesPlanningCollection,
    private salesplanningListCollection: SalesPlanningListCollection,
    private salesplanningProductCollection: SalesPlanningProductCollection,
    private patchOrganizationCollection:PatchOrganizationCollection,
    private poweScorecardPicklistManager: PoweScorecardPicklistManager,
    private patchCollection:PatchCollection,
    private marketingMessagesCollection : MarketingMessagesCollection,
    private dataChangeRequestsCollection: DataChangeRequestsCollection,
    private dataChangeRequestPickListManager: DataChangeRequestPickListManager,
    private contactPicklistManager: ContactPicklistManager,
    private contactChinaPicklistManager: ContactChinaPicklistManager,
    private organizationPickListManager: OrganizationPickListManager,
    private recordTypeCollection: RecordTypeCollection,
    private productSegmentationsCollection: ProductSegmentationsCollection,
    private productSegmentationPickListManager: ProductSegmentationPickListManager,
    private patchCustomerReferenceCollection:PatchCustomerReferenceCollection
  ) {}

  get map(): Array<any> {
    return [
      { action: 'download', collection: this.marketingCyclesCollection},
      { action: 'download', collection: this.recordTypeCollection},
      { action: 'download', collection: this.bricksCollection},
      { action: 'download', collection: this.buTeamPersonProfilesCollection},
      { action: 'download', collection: this.productsCollection},
      { action: 'download', collection: this.productPresentationsCollection},
      { action: 'download', collection: this.presentationsCollection},
      { action: 'download', collection: this.brandPlanningsCollection, conditional: 'isBrandmatrixEnabled'},
      { action: 'upload', collection: this.ordersCollection, conditional: 'isOrderManagementEnabled'},
      { action: 'upload', collection: this.ordersLineCollection, conditional: 'isOrderManagementEnabled'},
      { action: 'download', collection: this.ordersCollection, conditional: 'isOrderManagementEnabled'},
      { action: 'download', collection: this.ordersLineCollection, conditional: 'isOrderManagementEnabled'},
      { action: 'download', collection: this.surveyCollection, conditional: 'isSurveyModuleEnabled'},
      { action: 'download', collection: this.surveyQuestionnaireCollection, conditional: 'isSurveyModuleEnabled'},
      { action: 'upload',   collection: this.surveyQuestionnaireDetailCollection, conditional: 'isSurveyModuleEnabled'},
      { action: 'download', collection: this.surveyQuestionnaireDetailCollection, conditional: 'isSurveyModuleEnabled'},
      { action: 'download', collection: this.faqCollection},
      { action: 'download', collection: this.faqAttachmentCollection},
      { action: 'download', collection: this.organizationsCollection, conditional:'isNotAnIndianUser'},
      { action: 'upload', collection: this.dataChangeRequestsCollection, conditional: 'isDCREnabled'},
      { action: 'download', collection: this.dataChangeRequestsCollection, conditional: 'isDCREnabled'},
      { action: 'download', collection: this.contactsCollection, conditional:'isNotAnIndianUser'},
      { action: 'download', collection: this.patchOrganizationCollection, conditional:'isIndiaUser'},
      { action: 'download', collection: this.targetFrequenciesCollection},
      { action: 'download', collection: this.referenceCollection, conditional:'isNotAnIndianUser'},
      { action: 'download', collection: this.patchCustomerReferenceCollection, conditional:'isIndiaUser'},
      { action: 'upload',   collection: this.totsCollection},
      { action: 'download', collection: this.totsCollection},
      // { action: 'download', collection: this.productItemsCollection},
      { action: 'upload', collection: this.pharmaEventsCollection},
      { action: 'download', collection: this.productItemsCollection, conditional:'isNotAnIndianUser'},
      { action: 'upload',   collection: this.pharmaEventsCollection},
      { action: 'download', collection: this.pharmaEventsCollection},
      { action: 'download', collection: this.productSegmentationsCollection, conditional: 'isChinaUser' },

      { action: 'upload',   collection: this.salesplanningCollection, conditional:'isIndiaUser'},
      { action: 'download', collection: this.salesplanningCollection, conditional:'isIndiaUser'},

      { action: 'upload',   collection: this.salesplanningListCollection, conditional:'isIndiaUser'},
      { action: 'download', collection: this.salesplanningListCollection, conditional:'isIndiaUser'},
      { action: 'upload',   collection: this.salesplanningProductCollection, conditional:'isIndiaUser'},
      { action: 'download', collection: this.salesplanningProductCollection, conditional:'isIndiaUser'},

      { action: 'upload',   collection: this.callReportsCollection},
      { action: 'download', collection: this.callReportsCollection},

      { action: 'download', collection: this.iqviaAccountSkuTasksCollection, conditional: 'isIQVIAModuleEnabled'},
      { action: 'download', collection: this.iqviaQuestionsCollection,       conditional: 'isIQVIAModuleEnabled'},
      { action: 'upload',   collection: this.iqviaTaskAdjustmentsCollection, conditional: 'isIQVIAModuleEnabled'},
      { action: 'download', collection: this.iqviaTaskAdjustmentsCollection, conditional: 'isIQVIAModuleEnabled'},

      { action: 'upload',   collection: this.peAttendeesCollection},
      { action: 'download', collection: this.peAttendeesCollection},
      { action: 'download', collection: this.patchCustomerCollection, conditional:'isIndiaUser'},
      { action: 'download', collection: this.patchCollection, conditional:'isIndiaUser'},
      { action: 'download', collection: this.coachingPlanCollection, conditional:'isIndiaUser'},
      { action: 'download', collection: this.jointVisitPlanCollection, conditional:'isIndiaUser'},
      { action: 'upload',   collection: this.mtpCollection, conditional: 'isMtpModuleEnabled'},
      { action: 'download', collection: this.mtpCollection, conditional: 'isMtpModuleEnabled'},
      { action: 'upload',   collection: this.medicalQueriesCollection, conditional: 'isMedicalModuleEnabled'},
      { action: 'upload',   collection: this.medicalCommentsCollection, conditional: 'isMedicalModuleEnabled'},
      { action: 'download', collection: this.medicalQueriesCollection, conditional: 'isMedicalModuleEnabled'},
      { action: 'download', collection: this.medicalCommentsCollection, conditional: 'isMedicalModuleEnabled'},
      { action: 'upload',   collection: this.redFlagCollection, conditional: 'isItRedFlagRequired'},
      { action: 'download', collection: this.redFlagCollection, conditional: 'isItRedFlagRequired'},
      { picklist: this.orderPicklistManager, conditional: 'isOrderManagementEnabled' },
      { picklist: this.contactPicklistManager, conditional: '!isChinaUser' },
      { picklist: this.contactChinaPicklistManager, conditional: 'isChinaUser'},
      { picklist: this.orderPicklistManager, conditional: 'isOrderManagementEnabled' },
      { picklist: this.organizationPickListManager, conditional: '!isChinaUser' },
      { picklist: this.organizationChinaPickListManager, conditional: 'isChinaUser' },
      { picklist: this.mqPickListManager, conditional: 'isMedicalModuleEnabled' },
      { picklist: this.dataChangeRequestPickListManager, conditional: 'isDCREnabled' },
      { picklist: this.totPicklistManager },
      { picklist: this.pePicklistManager },
      { picklist: this.callReportPicklistManager },
      { picklist: this.poweScorecardPicklistManager, conditional:'isIndiaUser' },
      { picklist: this.productSegmentationPickListManager, conditional: 'isChinaUser'  },
    //  { picklist: this.patchRecordTypePickListManager },
      { action: 'upload', collection: this.doctorConsentCollection, conditional: 'isConsents'},
      { action: 'download', collection: this.doctorConsentCollection, conditional: 'isConsents'},
      // { action: 'download', collection: this.documentCollection},
      // { action: 'download', collection: this.marketingCyclesCollection},
       { action: 'download', collection: this.marketingMessagesCollection},
      // { action: 'upload', collection: this.locationCollection, conditional: 'isGPSTrackingEnabled'},
      // { action: 'download', collection: this.locationCollection, conditional: 'isGPSTrackingEnabled'},
      // { action: 'upload', collection: this.peAbbottAttendeesCollection},
      // { action: 'download', collection: this.peAbbottAttendeesCollection},

      // { action: 'upload', collection: this.peEventExpenseCollection},
      // { action: 'download', collection: this.peEventExpenseCollection},
      // { action: 'upload', collection: this.pharmaEventsCollection},
      // { action: 'download', collection: this.pharmaEventsCollection},
      // { action: 'download', collection: this.targetCollection},
      // { action: 'download', collection: this.productInPortfoliosCollection, conditional: 'isPortfolioSellingEnabled'},
      // { action: 'download', collection: this.productItemsCollection},
      // { action: 'download', collection: this.productListingHistoriesCollection, conditional: 'isChinaUser'},
      // { action: 'download', collection: this.productSegmentationsHistoriesCollection, conditional: 'isChinaUser'},
      // { action: 'upload', collection: this.redFlagCollection, conditional: 'isItRedFlagRequired'},
      // { action: 'download', collection: this.redFlagCollection, conditional: 'isItRedFlagRequired'},
    ];
  }
}

