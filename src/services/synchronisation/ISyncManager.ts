export interface ISyncManager {
  isErrorDuringSync: boolean;
  startLoading(statusCB: (message: string, percentage: number) => void): Promise<any>;
}