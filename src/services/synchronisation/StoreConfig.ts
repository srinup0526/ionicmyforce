
export class StoreConfig{
  static instance: Config;
  
  constructor() {}
  
  static getConfig() {
    if (!StoreConfig.instance) {
      StoreConfig.instance = new Config();
    }
    
    return StoreConfig.instance;
  }
  
  static setStoreName(storeName: string) {
    this.getConfig()
      .setStoreName(storeName);
  }
}

class Config {
  public isGlobalStore: boolean;
  public storeName: string;
  
  constructor() {
    this.isGlobalStore = true;
  }
  
  public setStoreName(storeName: string): void{
    this.storeName = storeName
  }
}
