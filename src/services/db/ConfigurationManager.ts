declare function getForceSyncClient();

export class ConfigurationManager {
  static _configId: number = 1;
  static _identityKey: string = 'id';
  static _restPath: string = '/services/apexrest/clmconfiguration';
  static _soupName: string = 'Configuration';
  static _indexSpec = [
    {
      path: ConfigurationManager._identityKey,
      type: 'string'
    }
  ];

  static loadConfig() {
    return ConfigurationManager._initSoup()
    .then(() => getForceSyncClient().jsClient.apexrest(ConfigurationManager._restPath))
    .then((configurationData) => {
      configurationData[ConfigurationManager._identityKey] = ConfigurationManager._configId;
      return getForceSyncClient()
        .smartstoreClient
        .upsertSoupEntriesWithExternalId(ConfigurationManager._soupName, [configurationData], ConfigurationManager._identityKey);
    });
  }

  static getConfig(segmentKey?) {
    return ConfigurationManager._initSoup()
      .then(() => {
        return getForceSyncClient()
          .smartstoreClient
          .retrieveSoupEntries(ConfigurationManager._soupName, [ConfigurationManager._configId]);
      })
      .then((entities) => {
        let config = entities.length ? entities[0] : null;

        if (segmentKey && config) {
          config = config[segmentKey];
        }

        return config;
      });
  }

  static _initSoup() {
    return getForceSyncClient().smartstoreClient.soupExists(ConfigurationManager._soupName)
    .then((soupExist) => {
      if (soupExist) {
        return;
      }

      return getForceSyncClient().smartstoreClient.registerSoup(ConfigurationManager._soupName, ConfigurationManager._indexSpec);
    });
  }
}
