import { Query } from '../common/Query';
import { Utils } from '../../utils/Utils';
import moment from 'moment';
import {StoreConfig} from "../../services/synchronisation/StoreConfig";

declare let Force;
declare let navigator;
declare let $;

declare function getForceSyncClient();


class SettingsManager {
  private soupName: string;
  private syncDownStatesKey: string;
  private restPath: string;
  private identityKey: string;
  private identityKeyValue;
  private isSoupInited;
  private queueForSave: Array<any>;
  private isLocked: boolean;
  public settings;

  constructor() {
    this.soupName =  'Settings';
    this.syncDownStatesKey = 'syncDownStates';
    this.restPath = '/clmconfiguration';
    this.identityKey = 'key';
    this.identityKeyValue = 1;
    this.isSoupInited = null;
    this.queueForSave = [];
    this.isLocked = false;
    this.settings = null;
  }

  _indexSpec() {
    return [
      {
        path: this.identityKey,
        type: 'string'
      }
    ];
  }

  _pairKeyValue(key = this.identityKey, value = this.identityKeyValue) {
    const pairKey = {};

    pairKey[key] = value;

    return pairKey;
  }

  _fetchSettingsQuery() {
    const query = new Query();
    query.selectFrom(this.soupName);

    return Promise.resolve(navigator.smartstore.buildSmartQuerySpec(query.toString()));
  }

  _parseResponse(response) {
    let settings = response.currentPageOrderedEntries.reduce(((result, segment) => result.concat(segment)), []);
    settings = settings.length ? settings[0] : {};

    return settings;
  }

  _initSoup() {
    if (this.isSoupInited || !getForceSyncClient().smartstoreClient) {
      return Promise.resolve();
    }

    return getForceSyncClient().smartstoreClient.soupExists(StoreConfig.getConfig(), this.soupName)
      .then((soupExist) => {
        if (soupExist) {
          this.isSoupInited = true;
          return Promise.resolve();
        }
        return getForceSyncClient().smartstoreClient.registerSoup(StoreConfig.getConfig(), this.soupName, this._indexSpec());
      })
      .then(() => {
        this.isSoupInited = true;
      });
  }

  _fetchSettings(force = false) {
    if (this.settings && !force) {
      return Promise.resolve(this.settings);
    }

    return this._fetchSettingsQuery()
      .then((query) => getForceSyncClient().smartstoreClient.runSmartQuery(StoreConfig.getConfig(), query))
      .then(this._parseResponse.bind(this))
      .then((settings) => {
        this.settings = settings;
        return settings;
      });
  }

  loadConfig() {
    this.settings = null;

    return Force.forceJsClient.apexrest({path: this.restPath, method: 'GET'})
    .then(this.setSettings.bind(this));
  }


  getSettings(force: boolean = false) {
    return this._initSoup()
      .then(() => this._fetchSettings(force));
  }

  setSettings(settings = {}) {
    if (this.isLocked) {
      return Promise.resolve(this.queueForSave.push(settings));
    }

    this.isLocked = true;
    return this.getSettings()
      .then((actualSettings) => {
        const extSettings = Utils.extend(actualSettings, settings);
        this.settings = Utils.extend(extSettings, this._pairKeyValue());
      })
      .then(() => {
        return getForceSyncClient().smartstoreClient.upsertSoupEntriesWithExternalId(StoreConfig.getConfig(), this.soupName, [this.settings], this.identityKey);
      })
      .then(() => {
        this.isLocked = false;
        const queueForSave = this.queueForSave.shift();

        if (queueForSave) {
          return this.setSettings(queueForSave);
        }
      });
  }

  setValueByKey(key, value) {
    const setting = this._pairKeyValue(key, value);
    return this.setSettings(setting);
  }

  getValueByKey(key) {
    return this.getSettings()
      .then((settings) => settings[key]);
  }

  forceGetValueByKey(key, value?) {
    const force = true;

    return this.getSettings(force)
      .then((settings) => settings[key]);
  }

  clearData() {
    this.settings = null;

    return getForceSyncClient().smartstoreClient.soupExists(StoreConfig.getConfig(), this.soupName)
      .then((soupExists) => {
        if (soupExists) {
          return getForceSyncClient().smartstoreClient.clearSoup(StoreConfig.getConfig(), this.soupName);
        }
      });
  }

  getSuccessSyncDownIds() {
    return this.getValueByKey(this.syncDownStatesKey)
      .then((syncStates) => syncStates || {});
  }

  setSuccessSyncDownIds(syncStates) {
    return this.setValueByKey(this.syncDownStatesKey, syncStates);
  }

  setSuccessSyncDownIdForCollection(collectionName, syncStateId) {
    return this.getSuccessSyncDownIds()
      .then((syncStates) => {
        syncStates[collectionName] = syncStateId;
        return this.setSuccessSyncDownIds(syncStates);
      });
  }

  getSuccessSyncDownIdForCollection(collectionName) {
    return this.getSuccessSyncDownIds()
      .then((syncStates) => syncStates[collectionName]);
  }

  getLastSucceededSyncDateTime() {
    return this.getValueByKey('lastSyncDate');
  }

  getActiveUserFullName() {
    return this.getValueByKey('activeUserFullName');
  }

  setActiveUserFullName(activeUserFullName) {
    return this.setValueByKey('activeUserFullName', activeUserFullName);
  }

  getTourPlanningSettings(){
    return this.getValueByKey('tourPlanningSettings')
    .then((settings)=> {
      let duration:any;
      let defaultDuration = [5,10,15,20,25,30,35,40,45,50,55,60,90,120,240,300,480]
      let defaultBreakTimeValue = (settings['defaultBreakTimeValue'] && parseInt(settings['defaultBreakTimeValue'])) || 15;
      let defaultDurationValue = (settings['defaultDurationValue'] && parseInt(settings['defaultDurationValue'])) || 30;
      let defaultLunchStartValue = Utils.timeFromString(settings['defaultLunchStartValue'] || '13:00');
      let defaultLunchEndValue = Utils.timeFromString(settings['defaultLunchEndValue'] || '14:00');
      if(settings['duration'])
      {
        duration = defaultDuration;
      }
      else
      {
        if(settings['duration'].indexOf(',') != -1)
        {
          duration = settings['duration'].split(',').map(value=>{ return parseInt(value) });
        }
        else
        {
          duration = settings['duration'].split(';').map(value=>{ return parseInt(value) });
        }
      }
      duration = duration.length>0?duration:defaultDuration;
      return $.when({
        breakDuration: moment({minutes: defaultBreakTimeValue}),
        callDuration: moment({minutes: defaultDurationValue}),
        lunchTimeStart: defaultLunchStartValue,
        lunchTimeEnd: defaultLunchEndValue,
        lastVisitTimeEnd: moment({hours: 20, minutes: 0}),
        duration: duration
      });
    });
}
}


export default new SettingsManager();
