import { Query } from '../common/Query';
import { Utils } from '../../utils/Utils';
import {StoreConfig} from "../../services/synchronisation/StoreConfig";

declare let navigator;

declare function getForceSyncClient();


class StorageManager {
  private soupName: string;
  private identityKey: string;
  private identityKeyValue: number;
  private isSoupInited;
  private queueForSave: Array<any>;
  private isLocked: boolean;

  constructor() {
    this.soupName = 'Storage';
    this.identityKey = 'id';
    this.identityKeyValue = 1;
    this.isSoupInited = null;
    this.queueForSave = [];
    this.isLocked = false;
  }

  _indexSpec() {
    return [
      {
        path: this.identityKey,
        type: 'string'
      }
    ];
  }

  _pairKeyValue(key = this.identityKey, value = this.identityKeyValue) {
    const pairKey = {};

    pairKey[key] = value;

    return pairKey;
  }

  _fetchSettingsQuery() {
    const query = new Query();

    query.selectFrom(this.soupName).where(this._pairKeyValue());

    return Promise.resolve(navigator.smartstore.buildSmartQuerySpec(query.toString()));
  }

  _parseResponse(response) {
    let settings = response.currentPageOrderedEntries.reduce(((result, segment) => result.concat(segment)), []);

    settings = settings.length ? settings[0] : {};
    return Promise.resolve(settings);
  }

  _initSoup() {
    if (this.isSoupInited) {
      return Promise.resolve();
    }

    return getForceSyncClient().smartstoreClient.soupExists(StoreConfig.getConfig(), this.soupName)
      .then((soupExist) => {
        if (soupExist) {
          this.isSoupInited = true;
          return Promise.resolve();
        }
        return getForceSyncClient().smartstoreClient.registerSoup(StoreConfig.getConfig(), this.soupName, this._indexSpec());
      })
      .then(() => {
        this.isSoupInited = true;
      });
  }

  _fetchSettings() {
    return this._fetchSettingsQuery()
      .then((query) => getForceSyncClient().smartstoreClient.runSmartQuery(StoreConfig.getConfig(), query))
      .then(this._parseResponse.bind(this));
  }

  getSettings() {
    return this._initSoup()
      .then(this._fetchSettings.bind(this));
  }

  setSettings(settings = {}) {
    if (this.isLocked) {
      return Promise.resolve(this.queueForSave.push(settings));
    }
    this.isLocked = true;

    return this.getSettings()
      .then((actualSettings) => {
        const extendedSettings = Utils.extend(actualSettings, settings);
        return Utils.extend(extendedSettings, this._pairKeyValue());
      })
      .then((settings) => {
        return getForceSyncClient().smartstoreClient.upsertSoupEntriesWithExternalId(StoreConfig.getConfig(), this.soupName, [settings], this.identityKey);
      })
      .then(() => {
        this.isLocked = false;
        const queueForSave = this.queueForSave.shift();

        if (queueForSave) {
          return this.setSettings(queueForSave);
        }
      });
  }

  setValueByKey(key, value) {
    const setting = this._pairKeyValue(key, value);

    return this.setSettings(setting);
  }

  getValueByKey(key) {
    return this.getSettings()
      .then((settings) => settings[key]);
  }

  clearData() {
    return getForceSyncClient().smartstoreClient.soupExists(StoreConfig.getConfig(), this.soupName)
      .then((soupExists) => {
        if (soupExists) {
          return getForceSyncClient().smartstoreClient.clearSoup(StoreConfig.getConfig(), this.soupName);
        }
      });
  }
}

export default new StorageManager();


