import SettingsManager from './SettingsManager';

// TODO: SettingsCache and SettingsManager should be merged to one class.
//       For now we need to keep cache separatly to not break existing logic.
class Settings {
  static IS_DATA_CHANGE_REQUESTS_MODULE_ENABLED_KEY: string;
  static IS_DYNAMIC_AGENDA_ENABLED_KEY: string;
  static IS_EDETAILING_ENABLED_KEY: string;
  static IS_TRADE_MODULE_ENABLED_KEY: string;
  static IS_PORTFOLIO_SELLING_MODULE_ENABLED_KEY: string;
  static IS_SAMPLE_MODULE_ENABLED_KEY: string;
  static IS_TOT_MODULE_ENABLED_KEY: string;
  static IS_ORDER_MANAGEMENT_MODULE_ENABLED_KEY: string;
  static IS_DEVELOPMENT_PLANS_MODULE_ENABLED_KEY: string;
  static IS_GPS_TRACKING_ENABLED_KEY: string;
  static SHOW_CONTACT_DESCRIPTION: string;
  static IS_GROUP_CALL_ENABLED: string;
  static IS_CONSENTS: string;
  static IS_MTP_MODULE_ENABLED: string;
  static IS_ORDER_MANAGEMENT_ENABLED: string;
  static IS_MEDICAL_MODULE_ENABLED: string;
  static IS_SURVEY_MODULE_ENABLED: string;
  static IS_CALL_REPORT_FROM_MEDIA_ENABLED: string;
  static IS_CHINA_USER: string;
  static IS_DASHBOARD_ENABLED: string;
  static IS_PHARMA_EVENT_MODULE_READ_ONLY: string;
  static IS_REACTIONS_TO_MARKETING_MESSAGE: string;
  static PHARMA_EVENT_SETTINGS: string;
  static IS_NEW_PHARMA_MODULE_ENABLED: string;
  static IS_IQVIA_MODULE_ENABLED: string;
  static LAST_SYNC_DATE_KEY: string;
  static ORDER_SALES_RECORD_TYPE_ID: string;
  static ORDER_SAMPLE_RECORD_TYPE_ID: string;
  static SAMPLE_MANAGEMENT_SETTINGS: string;
  static IS_INDIA_USER: string;
  static IS_NOT_AN_INDIAN_USER: string;
  static IS_IT_REDFLAG_REQUIRED: string;
  static instance: SettingsImpl;
  static IS_BRAND_MATRIX_MODULE_ENABLED: string;


  static initClass() {
    Settings.IS_DATA_CHANGE_REQUESTS_MODULE_ENABLED_KEY = 'isDCREnabled';
    Settings.IS_DYNAMIC_AGENDA_ENABLED_KEY = 'isDynamicAgendaEnabled';
    Settings.IS_EDETAILING_ENABLED_KEY = 'isEdetailingEnabled';
    Settings.IS_TRADE_MODULE_ENABLED_KEY = 'isTradeModuleEnabled';
    Settings.IS_PORTFOLIO_SELLING_MODULE_ENABLED_KEY = 'isPortfolioSellingModuleEnabled';
    Settings.IS_SAMPLE_MODULE_ENABLED_KEY = 'isSampleModuleEnabled';
    Settings.IS_TOT_MODULE_ENABLED_KEY = 'isToTModuleEnabled';
    Settings.IS_ORDER_MANAGEMENT_MODULE_ENABLED_KEY = 'isOrderManagementModuleEnabled';
    Settings.IS_DEVELOPMENT_PLANS_MODULE_ENABLED_KEY = 'isDevelopmentPlansModuleEnabled';
    Settings.IS_GPS_TRACKING_ENABLED_KEY = 'isGPSTrackingEnabled';
    Settings.SHOW_CONTACT_DESCRIPTION = 'showContactDescription';
    Settings.IS_GROUP_CALL_ENABLED = 'IsGroupCallEnabled';
    Settings.IS_CONSENTS = 'isConsents';
    Settings.IS_INDIA_USER = 'isIndiaUser';
    Settings.IS_NOT_AN_INDIAN_USER = 'isNotAnIndianUser';
    Settings.IS_MTP_MODULE_ENABLED = 'isMtpModuleEnabled';
    Settings.IS_ORDER_MANAGEMENT_ENABLED = 'isOrderManagementEnabled';
    Settings.IS_MEDICAL_MODULE_ENABLED = 'isMedicalModuleEnabled';
    Settings.IS_SURVEY_MODULE_ENABLED = 'isSurveyModuleEnabled';
    Settings.IS_CALL_REPORT_FROM_MEDIA_ENABLED = 'isCallReportFromMediaEnabled';
    Settings.IS_CHINA_USER = 'isChinaUser';
    Settings.IS_DASHBOARD_ENABLED = 'isDashboardEnabled';
    Settings.IS_PHARMA_EVENT_MODULE_READ_ONLY = 'isPharmaEventModuleReadOnly';
    Settings.IS_REACTIONS_TO_MARKETING_MESSAGE = 'isReactionsToMarketingMessage';
    Settings.IS_IT_REDFLAG_REQUIRED = 'isItRedFlagRequired';
    


    Settings.PHARMA_EVENT_SETTINGS = 'pharmaEventSettings';
    Settings.IS_NEW_PHARMA_MODULE_ENABLED = 'isNewPharmaModuleEnable';
    
    Settings.ORDER_SALES_RECORD_TYPE_ID = 'OrderSalesRecordTypeId';
    Settings.ORDER_SAMPLE_RECORD_TYPE_ID = 'OrderSampleRecordTypeId';
    
    Settings.SAMPLE_MANAGEMENT_SETTINGS = 'sampleManagementSettings';

    Settings.IS_IQVIA_MODULE_ENABLED = 'isIQVIAModuleEnabled';

    Settings.LAST_SYNC_DATE_KEY = 'lastSyncDate';

    Settings.instance = null;

    Settings.IS_BRAND_MATRIX_MODULE_ENABLED = 'isBrandmatrixEnabled';
  }

  static getInstance() {
    if (!Settings.instance) {
      Settings.initClass();
      Settings.instance = new SettingsImpl();
    }
    return Settings.instance;
  }
}

class SettingsImpl {
  private settings;

  constructor() {
    this.settings = null;
  }

  reload() {
    this.settings = undefined;

    return SettingsManager._fetchSettings()
      .then((settings) => {
        this.settings = this._parseSettings(settings);
        return this._logCachedSettings(this.settings);
      });
  }

  isDashboardEnabled() {
    return this.getSettingByKey(Settings.IS_DASHBOARD_ENABLED);
  }

  isChinaUser() {
    return this.getSettingByKey(Settings.IS_CHINA_USER);
  }

  isGpsTrackingEnabled() {
    return this.getSettingByKey(Settings.IS_GPS_TRACKING_ENABLED_KEY);
  }

  isDataChangeRequestsModuleEnabled() {
    return this.getSettingByKey(Settings.IS_DATA_CHANGE_REQUESTS_MODULE_ENABLED_KEY);
  }

  isDynamicAgendaEnabled() {
    return this.getSettingByKey(Settings.IS_DYNAMIC_AGENDA_ENABLED_KEY);
  }

  isEdetailingEnabled() {
    return this.getSettingByKey(Settings.IS_EDETAILING_ENABLED_KEY);
  }

  isTradeModuleEnabled() {
    return this.getSettingByKey(Settings.IS_TRADE_MODULE_ENABLED_KEY);
  }

  isItRedflagRequired() {
     return this.getSettingByKey(Settings.IS_IT_REDFLAG_REQUIRED); 
  }

  isPortfolioSellingModuleEnabled() {
    return this.getSettingByKey(Settings.IS_PORTFOLIO_SELLING_MODULE_ENABLED_KEY);
  }

  isSampleModuleEnabled() {
    return this.getSettingByKey(Settings.IS_SAMPLE_MODULE_ENABLED_KEY);
  }

  isToTModuleEnabled() {
    return this.getSettingByKey(Settings.IS_TOT_MODULE_ENABLED_KEY);
  }

  isOrderManagementModuleEnabled() {
    return this.getSettingByKey(Settings.IS_ORDER_MANAGEMENT_MODULE_ENABLED_KEY);
  }

  isDevelopmentPlansModuleEnabled() {
    return this.getSettingByKey(Settings.IS_DEVELOPMENT_PLANS_MODULE_ENABLED_KEY);
  }

  showContactDescription() {
    return this.getSettingByKey(Settings.SHOW_CONTACT_DESCRIPTION);
  }

  isGroupCallEnabled() {
    return this.getSettingByKey(Settings.IS_GROUP_CALL_ENABLED);
  }

  isIndiaUser()
  {
    return this.getSettingByKey(Settings.IS_INDIA_USER);
  }
  isNotAnIndianUser()
  {
    console.log("notanindianuser",this.getSettingByKey(Settings.IS_NOT_AN_INDIAN_USER));
    return this.getSettingByKey(Settings.IS_NOT_AN_INDIAN_USER);
  }
  setLastSucceededSyncDateTime(lastSyncDate) {
    return this.setSettingByKey(Settings.LAST_SYNC_DATE_KEY, lastSyncDate)
  }

  getLastSucceededSyncDateTime() {
    return this.getSettingByKey(Settings.LAST_SYNC_DATE_KEY);
  }

  isConsents() {
    return this.getSettingByKey(Settings.IS_CONSENTS);
  }

  isMtpModuleEnabled() {
    return this.getSettingByKey(Settings.IS_MTP_MODULE_ENABLED);
  }

  isOrderManagementEnabled() {
    return this.getSettingByKey(Settings.IS_ORDER_MANAGEMENT_ENABLED);
  }

  isMedicalModuleEnabled() {
    return this.getSettingByKey(Settings.IS_MEDICAL_MODULE_ENABLED);
  }

  isSurveyModuleEnabled() {
    return this.getSettingByKey(Settings.IS_SURVEY_MODULE_ENABLED);
  }

  isCallReportFromMediaEnabled() {
    return this.getSettingByKey(Settings.IS_CALL_REPORT_FROM_MEDIA_ENABLED);
  }

  isPharmaEventModuleReadOnly() {
    return this.getSettingByKey(Settings.IS_PHARMA_EVENT_MODULE_READ_ONLY);
  }

  isReactionsToMarketingMessage() {
    return this.getSettingByKey(Settings.IS_REACTIONS_TO_MARKETING_MESSAGE);
  }

  isIQVIAModuleEnabled() {
    return this.getSettingByKey(Settings.IS_IQVIA_MODULE_ENABLED);
  }

  isNewPharmaModuleEnable() {
    const pharmaEventSettings = this.getSettingByKey(Settings.PHARMA_EVENT_SETTINGS) || {};
    return pharmaEventSettings[Settings.IS_NEW_PHARMA_MODULE_ENABLED];
  }
  
  getOrderSalesRecordTypeId() {
    return this.getSettingByKey(Settings.ORDER_SALES_RECORD_TYPE_ID);
  }
  
  getOrderSamplesRecordTypeId() {
    return this.getSettingByKey(Settings.ORDER_SAMPLE_RECORD_TYPE_ID);
  }
  
  getOrderSampleManagementSettings() {
    return this.getSettingByKey(Settings.SAMPLE_MANAGEMENT_SETTINGS);
  }

  isBrandMatrixModuleEnabled() {
    return this.getSettingByKey(Settings.IS_BRAND_MATRIX_MODULE_ENABLED);
  }


  getSettingByKey(key) {
    if (!this.settings) {
      throw new Error('Settings are not cached. ' +
         'Please, check application initialization and synchronization, settings should be inited/reset there.');
    }
    return this.settings[key];
  }

   setSettingByKey(key, value) {
     this.settings[key] = value;
     return SettingsManager.setValueByKey(key, value);
   }

  _parseSettings(settings) {
    const result = {};
    let key;

    for (key in settings) {
      if (settings[key] === void 0) {
        result[key] = false;
      } else {
        result[key] = settings[key];
      }
    }

    return result;
  }

  _logCachedSettings(settings) {
    let message = 'Following setting values where loaded:';
    let key;

    for (key in settings) {
      message += `\n '${key}': ${settings[key]}`;
    }

    console.info(message);
  }

   prepareFieldSetsMap(fieldSets) {
     if (fieldSets) {
       let result = {};

       fieldSets.forEach((prsdFieldSet) => {
           result[prsdFieldSet.fieldPath] = prsdFieldSet;
       });

       return result;
     }

     return null;
   }
}

export {
  Settings,
  SettingsImpl
}
