import {Utils} from '../../utils/Utils';
import SettingsManager from './SettingsManager';
import StorageManager from './StorageManager';
import {SforceDataContext} from './../../collections/SforceDataContext';
import { UserScheme} from '../../models/scheme/UserScheme';
import { RecordTypeScheme } from '../../models/scheme/RecordRypeScheme';
import { DeviceScheme } from '../../models/scheme/DeviceScheme';
import { ProductScheme } from '../../models/scheme/ProductScheme';
import { PresentationScheme } from '../../models/scheme/PresentationScheme';
import { ProductPresentationScheme } from '../../models/scheme/ProductPresentationScheme';
import { OrderScheme } from '../../models/scheme/OrderScheme';
import { OrderLineScheme } from '../../models/scheme/OrderLineScheme';
import { SurveyScheme } from '../../models/scheme/SurveyScheme';
import { SurveyQuestionnaireScheme } from '../../models/scheme/SurveyQuestionnaireScheme';
import { SurveyQuestionnaireDetailScheme } from '../../models/scheme/SurveyQuestionnaireDetailScheme';
import { PicklistManager } from '../../services/db/picklist-managers/base/PicklistManagers';
import { AttachmentFileManager } from '../../services/common/attachments/AttachmentFileManager';
import {FaqScheme} from "../../models/scheme/FaqScheme";
import {FaqAttachmentScheme} from "../../models/scheme/FaqAttachmentScheme";
import { Injectable } from '@angular/core';
import {PresentationFileManager} from "../../pages/media/PresentationFileManager";
import {IqviaAccountSkuTaskScheme} from '../../models/scheme/IqviaAccountSkuTaskScheme';
import {IqviaQuestionScheme} from '../../models/scheme/IqviaQuestionScheme';
import {IqviaTaskAdjustmentScheme} from '../../models/scheme/IqviaTaskAdjustmentScheme';
import {ReferenceScheme} from "../../models/scheme/ReferenceScheme";
import {PatchCustomerReferenceScheme} from "../../models/scheme/PatchCustomerReferenceScheme";
import {ContactScheme} from "../../models/scheme/ContactScheme";
import {TargetFrequencyScheme} from "../../models/scheme/TargetFrequencyScheme";
import {MarketingCycleScheme} from "../../models/scheme/MarketingCycleScheme";
import {ScenarioScheme} from "../../models/scheme/ScenarioScheme";
import {BrickScheme} from "../../models/scheme/BrickScheme";
import {BuTeamPersonProfileScheme} from "../../models/scheme/BuTeamPersonProfileScheme";
import {OrganizationScheme} from "../../models/scheme/OrganizationScheme";
import {StoreConfig} from "../../services/synchronisation/StoreConfig";

import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import { CLMCallReportDataScheme } from '../../models/scheme/CLMCallReportDataScheme';
import { DoctorsConsentScheme } from '../../models/scheme/DoctorsConsentScheme';
import { DocumentScheme } from '../../models/scheme/DocumentScheme';
import { LocationScheme } from '../../models/scheme/LocationScheme';
import { CoachingPlanScheme } from '../../models/scheme/CoachingPlanScheme';
import { JointVisitPlanScheme } from '../../models/scheme/JointVisitPlanScheme';
import { PatchScheme } from '../../models/scheme/PatchScheme';
// import { MarketingCycleScheme } from '../../models/scheme/MarketingCycleScheme';
import { MarketingMessageScheme } from '../../models/scheme/MarketingMessageScheme';
import { MedicalCommentScheme } from '../../models/scheme/MedicalCommentScheme';
import { MedicalQueryScheme } from '../../models/scheme/MedicalQueryScheme';
import { MTPScheme } from '../../models/scheme/MTPScheme';
import { PatchCustomerScheme } from '../../models/scheme/PatchCustomerScheme';
// import { OrganizationScheme } from '../../models/scheme/OrganizationScheme';
// import { PEAbbottAttendeeScheme } from '../../models/scheme/PEAbbottAttendeeScheme';
import { PEAttendeeScheme } from '../../models/scheme/PEAttendeeScheme';
// import { PEEventExpenseScheme } from '../../models/scheme/PEEventExpenseScheme';
// import { PharmaEventScheme } from '../../models/scheme/PharmaEventScheme';
// import { ProductInPortfolioScheme } from '../../models/scheme/ProductInPortfolioScheme';
import { ProductItemScheme } from '../../models/scheme/ProductItemScheme';
// import { ProductListingHistoryScheme } from '../../models/scheme/ProductListingHistoryScheme';
// import { ProductSegmentationHistoryScheme } from '../../models/scheme/ProductSegmentationHistoryScheme';
import { RedFlagScheme } from '../../models/scheme/RedFlagScheme';
import { ProductSegmentationScheme } from '../../models/scheme/ProductSegmentationScheme';
// import { TargetFrequencyScheme } from '../../models/scheme/TargetFrequencyScheme';
// import { TargetScheme } from '../../models/scheme/TargetScheme';
// import { TaskAdjustmentScheme } from '../../models/scheme/TaskAdjustmentScheme';
import { TotScheme } from '../../models/scheme/TotScheme';
import { PharmaEventScheme } from '../../models/scheme/PharmaEventScheme';
import {BrandPlanningScheme} from "../../models/scheme/BrandPlanningScheme";
//import { TourPlanningEntityScheme } from '../../models/scheme/TourPlanningEntityScheme';
import { DataChangeRequestScheme } from '../../models/scheme/DataChangeRequestScheme';


import { SalesPlanningScheme } from '../../models/scheme/SalesPlanningScheme';
import { SalesPlanningListScheme } from '../../models/scheme/SalesPlanningListScheme';
import { SalesPlanningProductScheme } from '../../models/scheme/SalesPlanningProductScheme';
import {PatchOrganizationScheme} from "../../models/scheme/PatchOrganizationScheme";

declare let Force;
declare function getForceSyncClient();

@Injectable()
export class DatabaseManager {
  models: Array<any>;

  constructor(private attachmentFileManager: AttachmentFileManager,
              private presentationFileManager: PresentationFileManager) {
    this.models = [
      UserScheme,
      BrickScheme,
      RecordTypeScheme,
      DeviceScheme,
      BuTeamPersonProfileScheme,
      CLMCallReportDataScheme,
      DoctorsConsentScheme,
      DocumentScheme,
      LocationScheme,
      BrandPlanningScheme,
      OrderLineScheme,
      // MarketingCycleScheme,
      MarketingMessageScheme,
      MedicalCommentScheme,
      MedicalQueryScheme,
      MTPScheme,
      PharmaEventScheme,
      CallReportScheme,
      // OrganizationScheme,
      // PEAbbottAttendeeScheme,
      PEAttendeeScheme,
      // PEEventExpenseScheme,
      // PharmaEventScheme,
      // ProductInPortfolioScheme,
      ProductItemScheme,
      // ProductListingHistoryScheme,
      // ProductSegmentationHistoryScheme,
      // ProductSegmentationScheme,
      RedFlagScheme,
      PatchScheme,
      ProductSegmentationScheme,
      // RedFlagScheme,
      // TargetFrequencyScheme,
      // TargetScheme,
      // TaskAdjustmentScheme,
      TotScheme,
      CoachingPlanScheme,
      JointVisitPlanScheme,
      ProductScheme,
      PresentationScheme,
      ProductPresentationScheme,
      OrderScheme,
      SurveyScheme,
      SurveyQuestionnaireScheme,
      SurveyQuestionnaireDetailScheme,
      FaqScheme,
      PatchCustomerScheme,
      PatchOrganizationScheme,
      FaqAttachmentScheme,
      IqviaAccountSkuTaskScheme,
      IqviaQuestionScheme,
      IqviaTaskAdjustmentScheme,
      ReferenceScheme,
      PatchCustomerReferenceScheme,
      ContactScheme,
      TargetFrequencyScheme,
      MarketingCycleScheme,
      ScenarioScheme,
      BuTeamPersonProfileScheme,
      OrganizationScheme,
      DataChangeRequestScheme,
      SalesPlanningScheme,
      SalesPlanningListScheme,
      SalesPlanningProductScheme,
      CoachingPlanScheme,
      JointVisitPlanScheme
    ];
  }

  initializeDatabase() {
    return this.isDifferentUserLoggedIn()
      .then((isDifferentUser) => {
        if (isDifferentUser) {
          return this.clearDatabase()
            .then(() => this.setupDatabase())
            .then(() => this.clearSettings())
            .then(() => this.clearStorage());
        }
        return this.setupDatabase()
      })
      .then(this.saveCurrentUserAndLoginHost.bind(this));
  }

  clearDatabase() {
    return this.clearDBSchemaModels()
      .then(this.resetSyncStates.bind(this))
      .then(this.clearPicklists.bind(this))
      // .then(this.resetAlarmNotifications)
      // .then(this.clearPresentationsStore.bind(this))
      // .then(this.clearAttachmentsStore.bind(this))
      .then(() => SforceDataContext.cleanup());
  }

  private clearDBSchemaModels() {
    return Utils.runSequentially(this.models.map((model) => (() => this.dropSoup(model))));
  }

  private dropSoup(model) {
    return Force.smartstoreClient.soupExists(StoreConfig.getConfig(), model.table)
      .then((soupExists) => {
        if (soupExists) {
          return Force.smartstoreClient.removeSoup(StoreConfig.getConfig(), model.table)
            .then(() => this.createSoup(model));
        }
      });
  }

  private resetSyncStates() {
    return SettingsManager.setSuccessSyncDownIds({});
  }

  private initSoup(model) {
    return Force.smartstoreClient.soupExists(StoreConfig.getConfig(), model.table)
      .then((soupExists) => {
        if (soupExists) {
          return;
        }
        const isGlobalStore = StoreConfig.getConfig().isGlobalStore;
        const storeName = StoreConfig.getConfig().storeName;
      
        const cache = new Force.StoreCache(model.table, model.indexSpec, "Id", isGlobalStore, storeName);
      
        return cache.init();
      });
  }

  private createSoup(model) {
    return this.initSoup(model);
  }

  private clearSettings() {
    return SettingsManager.clearData();
  }

  private clearStorage() {
    return StorageManager.clearData();
  }

  private clearPicklists() {
    return PicklistManager.clearData();
  }

  // _resetAlarmNotifications() {
  //   return AlarmManager.cancelNotification();
  // }

  private stopDataLoads() {
    // PresentationMultiloader.pendingDownload = [];
    // return DataLoadManager.stopAll();
  }

  private clearPresentationsStore(): Promise<any> {
    // this.stopDataLoads();
    return this.presentationFileManager.wipePresentationsStore();
  }

  private clearAttachmentsStore() {
    return this.attachmentFileManager.wipeStorage();
  }

  private setupDatabase() {
    const func = this.models.map((model) => () => this.createSoup(model));
    return Utils.runSequentially(func);
  }

  private isDifferentUserLoggedIn() {
    return SettingsManager.forceGetValueByKey('UserId')
      .then((userId) => {
        return !userId || userId !== getForceSyncClient().userId ;
      });
  }

  private saveCurrentUserAndLoginHost() {
    return SforceDataContext.getAuthCredentials()
      .then((creds) => {
        return SettingsManager.setValueByKey('UserId', creds.userId)
          .then(() => {
            // continue only after user data will be stored in database
            return SettingsManager.setValueByKey('LoginHost', creds.loginUrl);
          });
      });
  }
}
