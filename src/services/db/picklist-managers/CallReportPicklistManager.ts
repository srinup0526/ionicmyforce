import { CallReportScheme } from '../../../models/scheme/CallReportScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable()
export class CallReportPicklistManager extends PicklistDatasourceManager {

  targetModel() {
    return CallReportScheme;
  }

  fieldNames() {
    return [CallReportScheme.fields.type.sfdc,CallReportScheme.fields.typeOfVisit.sfdc,CallReportScheme.fields.coachingVisit.sfdc, CallReportScheme.fields.prio1ReactionsToMarketingMessage1.sfdc, CallReportScheme.fields.status.sfdc, CallReportScheme.fields.activityType.sfdc];
  }
}
