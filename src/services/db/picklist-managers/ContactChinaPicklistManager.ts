import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";
import {ContactScheme} from "../../../models/scheme/ContactScheme";


@Injectable({
  providedIn: 'root'
})
export class ContactChinaPicklistManager extends PicklistDatasourceManager {

  targetModel() {
    return {
      sfdcTable: ContactScheme.sfdcTable,
      table: ContactScheme.table
    };
  }

  fieldNames() {
    return [
      ContactScheme.fields.hcpStatus.sfdc,
      ContactScheme.fields.gender.sfdc,
      ContactScheme.fields.kol.sfdc,
      ContactScheme.fields.statusDescription.sfdc
    ];
  }
}
