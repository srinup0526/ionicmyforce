import { MTPScheme} from '../../../models/scheme/MTPScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable()
export class MTPPicklistManager extends PicklistDatasourceManager {

  targetModel() {
    return MTPScheme;
  }

  fieldNames() {
    return [MTPScheme.fields.month.sfdc, MTPScheme.fields.year.sfdc, MTPScheme.fields.status.sfdc];
  }
}
