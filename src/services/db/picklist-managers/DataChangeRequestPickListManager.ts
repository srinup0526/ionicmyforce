import {DataChangeRequestScheme} from '../../../models/scheme/DataChangeRequestScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class DataChangeRequestPickListManager extends PicklistDatasourceManager {

  targetModel() {
    return DataChangeRequestScheme;
  }

  fieldNames() {
    return [
      DataChangeRequestScheme.fields.dcrOrganizationCNHospitalGrade.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationCNHospitalLevel.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationCNProperty.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationCNTargetHospital.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationCNType.sfdc,
      DataChangeRequestScheme.fields.dcrContactCJobTitle.sfdc,
      DataChangeRequestScheme.fields.dcrContactGender.sfdc,
      DataChangeRequestScheme.fields.dcrContactInclinationToMylan.sfdc,
      DataChangeRequestScheme.fields.dcrContactNumberOfPatientsPerDay.sfdc,
      DataChangeRequestScheme.fields.dcrContactPersonType.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationBrick.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationCJuridicGroup.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationCSpecialty1.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationCSpecialty2.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationNumberOfPatientsPerDay.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationPriority.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationRecordType.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationStatus.sfdc,
      DataChangeRequestScheme.fields.dcrOrganizationSubtype.sfdc,
      DataChangeRequestScheme.fields.dcrReferenceCStatus.sfdc,
      DataChangeRequestScheme.fields.dcrStatus.sfdc,
      DataChangeRequestScheme.fields.dcrType.sfdc
    ];
  }
  
  getStatusLabelByValue(value) {
    return this.getPickLists()
      .then(() => {
        return this.getLabelByValue(DataChangeRequestScheme.fields.dcrReferenceCStatus.sfdc, value);
      });
  }

}
