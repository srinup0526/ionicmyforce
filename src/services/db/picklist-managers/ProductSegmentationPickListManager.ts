import {ProductSegmentationScheme} from '../../../models/scheme/ProductSegmentationScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class ProductSegmentationPickListManager extends PicklistDatasourceManager {
  
  targetModel() {
    return ProductSegmentationScheme;
  }
  
  fieldNames() {
    return [
      ProductSegmentationScheme.fields.segmentation1.sfdc,
      ProductSegmentationScheme.fields.segmentation2.sfdc
    ];
  }
}

