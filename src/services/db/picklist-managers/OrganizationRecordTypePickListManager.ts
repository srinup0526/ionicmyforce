import {OrganizationScheme} from '../../../models/scheme/OrganizationScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class OrganizationRecordTypePickListManager extends PicklistDatasourceManager{
  targetModel() {
    return OrganizationScheme;
  }
  
  fieldNames() {
    return [OrganizationScheme.RECORD_TYPE_FIELD_NAME, OrganizationScheme.SUBTYPE_FIELD_NAME];
  }
}
