import {BrickScheme} from '../../../models/scheme/BrickScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {PicklistManager} from './base/PicklistManagers';
import {Injectable} from "@angular/core";
import { LocalizationManager } from './../../common/localizations/LocalizationManager';
import {BricksCollection} from "../../../collections/BricksCollection";


@Injectable()
export class BrickPickListManager extends PicklistDatasourceManager{
  
  constructor(localizationManager: LocalizationManager, private bricksCollection: BricksCollection){
    super(localizationManager);
  }
  
  targetModel() {
    return BrickScheme;
  }
  
  fieldNames() {
    return [BrickScheme.fields.shortDescription.local];
  }
  
  getPickLists() {
    if(this.pickLists && this.pickLists[this.targetModel().table]) {
      return Promise.resolve(this.pickLists);
    }
    
    return this.bricksCollection.fetchAll()
      .then(this.bricksCollection.getAllEntitiesFromResponse.bind(this.bricksCollection))
      .then((bricks) => {
        const field = this.fieldNames()[0];
  
        const pickLists = bricks.map((brick) => ({
          label: brick[field],
          value: brick.id
        }));
     
        this.pickLists = this.pickLists || {};
  
        this.pickLists[field] = pickLists;
        
        return this.pickLists;
      });
  }

}
