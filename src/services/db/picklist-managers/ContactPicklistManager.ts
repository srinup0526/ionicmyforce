import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";
import {ContactScheme} from "../../../models/scheme/ContactScheme";


@Injectable({
  providedIn: 'root'
})
export class ContactPicklistManager extends PicklistDatasourceManager {

  targetModel() {
    return {
      sfdcTable: ContactScheme.sfdcTable,
      table: ContactScheme.table
    };
  }

  fieldNames() {
    return [ContactScheme.fields.kol.sfdc];
  }
}
