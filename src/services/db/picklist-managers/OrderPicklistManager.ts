import {OrderScheme} from '../../../models/scheme/OrderScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable()
export class OrderPicklistManager extends PicklistDatasourceManager {

  targetModel() {
    return OrderScheme;
  }

  fieldNames() {
    return [OrderScheme.fields.status.sfdc];
  }
}
