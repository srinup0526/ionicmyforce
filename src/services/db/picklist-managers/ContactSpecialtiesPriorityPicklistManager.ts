import {ContactScheme} from '../../../models/scheme/ContactScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable()
export class ContactSpecialtiesPriorityPickListManager extends PicklistDatasourceManager {
  
  targetModel() {
    return ContactScheme;
  }
  
  fieldNames() {
    return  [ContactScheme.SPECIALTY_FIELD_NAME, ContactScheme.BU_SPECIALTY_FIELD_NAME, ContactScheme.PRIORITY_FIELD_NAME, ContactScheme.SUBTYPE_FIELD_NAME];
  }
}
