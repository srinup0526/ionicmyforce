import {PicklistManager} from './PicklistManagers';
import {Injectable} from "@angular/core";
import {LocalizationManager} from './../../../../services/common/localizations/LocalizationManager';

declare var _;


@Injectable()
export class PicklistDatasourceManager {
  private table: string;
  private fields;
  protected pickLists;

  constructor(private localizationManager: LocalizationManager) {}

  targetModel() {
    if (this.table) {
      return {
        sfdcTable: this.table
      };
    }
    throw new Error('Should be overridden');
  }

  fieldNames() {
    if (this.fields) {
      return this.fields;
    }
    
    throw new Error('Should be overridden');
  }

  getPickLists() {
    return PicklistManager.getPicklist(this.targetModel().sfdcTable, this.fieldNames())
      .then((pickLists) => this.pickLists = pickLists);
  }

  getPickList(pickListName) {
    return this.getPickLists()
      .then((pickLists) => pickLists[pickListName]);
  }

  getLabelByValue(pickListName, value) {
    return this.getPickList(pickListName)
      .then((pickList) => {
        return _(pickList).find((element) => element.value === value);
      })
      .then((element) => {
        if (element) {
          return element.label || element.value;
        } else if (value === false) {
          return this.localizationManager.promiseLocale('common.defaultSelectValue');
        } else {
          return value;
        }
      });
  }

  async getLabelByValueInPickLists(pickLists, pickListName, value) {
    const picklist = pickLists[pickListName];

    if (!picklist) {
      throw new Error('Could not find picklist "' + pickListName + '"');
    }

    const element = picklist.find((element) => element.value === value);

    if (element) {
      return element.label || element.value;
    }
    else if (value === false) {
      return await this.localizationManager.promiseLocale('common.defaultSelectValue');
    } else {
      return value;
    }
  }
}
