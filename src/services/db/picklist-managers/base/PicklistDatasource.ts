import {LocalizationManager} from './../../../../services/common/localizations/LocalizationManager';
import {Injectable} from "@angular/core";
declare var _;


@Injectable()
export class PickListDatasource {
  private manager;
  private name;
  private items;

  constructor(protected localizationManager: LocalizationManager) {}

  public async getItems() {
    if (this.items) {
      return Promise.resolve(this.items);
    }

    return this.pickListManager()
      .getPickList(this.pickListName())
      .then(this.preparePickListItems.bind(this))
      .then((items) => {
        return this.items = items;
      });
  }

  public getItemForSelectedValue(value) {
    return this.getItems()
      .then((items) => _(items).find((item) => item.id === value))
      .then((item) => {
        if (item) {
          return item;
        }
        else if (value) {
          return {
            id: value,
            description: value
          };
        }

        return null;
      });
  }

  public pickListName() {
    if (this.name) {
      return this.name;
    }
    throw new Error('This method should be overridden.');
  }

  public pickListManager() {
    if (this.manager) {
      return this.manager;
    }
    throw new Error('This method should be overridden.');
  }

  public async preparePickListItems(pickList) {
    const noneValue = [await this.getNoneValue()];

    if (!_.isEmpty(pickList)) {
      return noneValue.concat(pickList.map((el) => ({ id: el.value, description: el.label })));
    } else {
      return noneValue;
    }
  }

  private async getNoneValue() {
    const defaultItemLabel = await this.localizationManager.promiseLocale('common.defaultSelectValue');
    
    return {
      id: null,
      description: defaultItemLabel
    };
  }
}
