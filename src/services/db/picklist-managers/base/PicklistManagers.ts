import { Query } from '../../../common/Query';
import {StoreConfig} from "../../../../services/synchronisation/StoreConfig";

declare function getForceSyncClient();
declare var _;
declare var navigator;


export class PicklistManager {
  static restPath: string = '/picklist';
  static soupName: string = 'PickList';
  static identityKey: string = 'objectFieldName';

  static indexSpec: Array<{path: string, type: string}> = [
    {
      path: PicklistManager.identityKey,
      type: 'string'
    }
  ];

  static cache = {};

  static loadPicklist(objectType, fieldNames = []) {
    PicklistManager.clearCache(objectType);

    return PicklistManager.initSoup()
      .then(() => PicklistManager.loadPickListWithDB(objectType, fieldNames))
      .then(PicklistManager.upsertPickList.bind(PicklistManager));
  }

  static clearCache(objectType) {
    if (PicklistManager.cache[objectType]) {
      delete PicklistManager.cache[objectType];
    }
  }

  static getPicklist(objectType, fieldNames = []) {
    if (PicklistManager.isCached(objectType, fieldNames)) {
      return Promise.resolve(PicklistManager.cache[objectType]);
    }

    return getForceSyncClient().smartstoreClient.soupExists(StoreConfig.getConfig(), PicklistManager.soupName)
      .then((soupExist) => {
        if (soupExist) {
          const searchQuery = PicklistManager.generateFetchQuery(objectType, fieldNames);

          return getForceSyncClient().smartstoreClient.runSmartQuery(StoreConfig.getConfig(), searchQuery)
            .then((pickListCollection) => {
              pickListCollection = PicklistManager.processResult(pickListCollection);

              PicklistManager.addToCache(objectType, pickListCollection);

              return pickListCollection;
            });
        } else {
          return [];
        }
      });
  }

  static clearData() {
    return getForceSyncClient().smartstoreClient.soupExists(StoreConfig.getConfig(), PicklistManager.soupName)
      .then((soupExists) => {
        if (!soupExists) {
          return;
        }

        return getForceSyncClient().smartstoreClient.removeSoup(StoreConfig.getConfig(), PicklistManager.soupName)
          .then(() => PicklistManager.initSoup());
      });
  }

  static isCached(objectType, fieldNames) {
    if (!fieldNames.length) {
      return false;
    }

    if (!PicklistManager.cache[objectType]) {
      return false;
    }

    return fieldNames.every((name) => {
      return !!PicklistManager.cache[objectType][name];
    });
  }

  static addToCache(objectType, pickListCollection) {
    PicklistManager.cache[objectType] = PicklistManager.cache[objectType] || {};

    Object.keys(pickListCollection).forEach((key) => {
      PicklistManager.cache[objectType][key] = pickListCollection[key];
    });
  }

  static initSoup() {
    return getForceSyncClient().smartstoreClient.soupExists(StoreConfig.getConfig(), PicklistManager.soupName)
    .then((soupExist) => {
      if (soupExist) {
        return;
      }

      return getForceSyncClient().smartstoreClient.registerSoup(StoreConfig.getConfig(), PicklistManager.soupName, PicklistManager.indexSpec);
    });
  }

  static getRequestParams(objectType, fieldNames) {
    const params = {
      sobjectType: objectType,
      pickListFieldAPINames: fieldNames
    };

    return JSON.stringify(params);
  }

  static composeIdentityKey(keyComponents = []) {
    return keyComponents.join(':');
  }

  static processBeforeSave(pickListObject) {
    return _.map(pickListObject.picklists, (listItem) => {
      listItem[PicklistManager.identityKey] = PicklistManager.composeIdentityKey([pickListObject['objectName'], listItem['fieldName']]);
      return listItem;
    });
  }


  static generateSearchCriterias(objectType, fieldNames) {
    return _.map(fieldNames, (fieldName) => {
      return PicklistManager.composeIdentityKey([ objectType, fieldName ]);
    });
  }

  static generateFetchQuery(objectType, fieldNames) {
    const criterias = PicklistManager.generateSearchCriterias(objectType, fieldNames);
    const query = new Query(PicklistManager.soupName);
    const PAGE_SIZE = 99999;

    query.selectFrom(PicklistManager.soupName)
      .whereIn(PicklistManager.identityKey, criterias);

    return navigator.smartstore.buildSmartQuerySpec(query.toString(), PAGE_SIZE);
  }

  static processResult(pickListCollection) {
    const result = {};

    _.each(pickListCollection['currentPageOrderedEntries'], (group) => {
      _.each(group, (pickListGroup) => {
        result[pickListGroup['fieldName']] = pickListGroup['picklistOptions'];
      });
    });

    return result;
  }

  static loadPickListWithDB(objectType, fieldNames) {
    const params = {
      path: PicklistManager.restPath,
      method: 'POST',
      params: PicklistManager.getRequestParams(objectType, fieldNames)
    };

    return getForceSyncClient().jsClient.apexrest(params);
  }

  static upsertPickList(pickListObject) {
    pickListObject = PicklistManager.processBeforeSave(pickListObject);

    return getForceSyncClient().smartstoreClient
      .upsertSoupEntriesWithExternalId(StoreConfig.getConfig(), PicklistManager.soupName, pickListObject, PicklistManager.identityKey);
  }
}
