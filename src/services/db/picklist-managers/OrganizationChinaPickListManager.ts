import {OrganizationScheme} from '../../../models/scheme/OrganizationScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class OrganizationChinaPickListManager extends PicklistDatasourceManager {
  targetModel() {
    return {
      sfdcTable: OrganizationScheme.sfdcTable,
      table: OrganizationScheme.table
    };
  }
  
  fieldNames() {
    return [
      OrganizationScheme.fields.status.sfdc,
      OrganizationScheme.fields.statusDescription.sfdc,
      OrganizationScheme.fields.organizationType.sfdc,
      OrganizationScheme.fields.organizationProperty.sfdc,
      OrganizationScheme.fields.hospitalGrade.sfdc,
      OrganizationScheme.fields.hospitalLevel.sfdc,
      OrganizationScheme.fields.targetHospital.sfdc,
      OrganizationScheme.fields.globalPriority.sfdc
    ]
  }
}
