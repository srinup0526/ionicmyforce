import { CallReportScheme } from '../../../../models/scheme/CallReportScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { CallReportPicklistManager } from './../CallReportPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class CallReportPrio1ReactionsPickListDatasource extends PickListDatasource {
  
  constructor(private callReportPicklistManager: CallReportPicklistManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return CallReportScheme.fields.prio1ReactionsToMarketingMessage1.sfdc;
  }

  pickListManager() {
    return this.callReportPicklistManager;
  }
}
