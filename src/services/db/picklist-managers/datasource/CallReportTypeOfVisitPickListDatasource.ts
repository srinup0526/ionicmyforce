import { CallReportScheme } from '../../../../models/scheme/CallReportScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { CallReportPicklistManager } from './../CallReportPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class CallReportTypeOfVisitPickListDatasource extends PickListDatasource {
  
  constructor(private callReportPicklistManager: CallReportPicklistManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return CallReportScheme.fields.typeOfVisit.sfdc;
  }

  pickListManager() {
    return this.callReportPicklistManager;
  }
}
