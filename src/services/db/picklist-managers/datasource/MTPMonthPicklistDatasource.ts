import { MTPScheme } from '../../../../models/scheme/MTPScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { MTPPicklistManager } from './../MTPPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class MTPMonthPicklistDatasource extends MTPPicklistManager {
  
  constructor(private mtpPicklistManager: MTPPicklistManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return MTPScheme.fields.status.sfdc;
  }

  pickListManager() {
    return this.mtpPicklistManager;
  }
}
