import { PatchCustomerScheme } from '../../../../models/scheme/PatchCustomerScheme';
import { PatchScheme } from '../../../../models/scheme/PatchScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { PatchRecordTypePickListManager } from './../PatchRecordTypePickListManager';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class PatchPicklistDatasource extends PickListDatasource {
  
  constructor(private patchRecordTypePickListManager: PatchRecordTypePickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return PatchScheme.fields.name.local;
  }
  
  pickListManager() {
    return this.patchRecordTypePickListManager;
  }
}
