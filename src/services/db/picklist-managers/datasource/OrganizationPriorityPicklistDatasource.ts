import { OrganizationScheme } from '../../../../models/scheme/OrganizationScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { OrganizationChinaPickListManager } from './../OrganizationChinaPickListManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class OrganizationPriorityPicklistDatasource extends PickListDatasource {
  
  constructor(private organizationChinaPickListManager: OrganizationChinaPickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return OrganizationScheme.PRIORITY_FIELD_NAME;
  }
  
  pickListManager() {
    return this.organizationChinaPickListManager;
  }
}
