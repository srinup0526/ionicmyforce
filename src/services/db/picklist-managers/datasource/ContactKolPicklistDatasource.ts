import { PickListDatasource } from './../base/PicklistDatasource';
import { ContactPicklistManager } from './../ContactPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";
import {ContactScheme} from "../../../../models/scheme/ContactScheme";


@Injectable({
  providedIn: 'root'
})
export class ContactKolPicklistDatasource extends PickListDatasource {
  
  constructor(private contactPicklistManager: ContactPicklistManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return ContactScheme.fields.kol.sfdc;
  }

  pickListManager() {
    return this.contactPicklistManager;
  }
}
