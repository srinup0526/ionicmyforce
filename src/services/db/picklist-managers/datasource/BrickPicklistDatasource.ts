import {BrickScheme} from '../../../../models/scheme/BrickScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { BrickPickListManager } from './../BrickPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class BrickPickListDatasource extends PickListDatasource {
  
  constructor(private brickPickListManager: BrickPickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return BrickScheme.fields.shortDescription.local;
  }
  
  pickListManager() {
    return this.brickPickListManager;
  }
}
