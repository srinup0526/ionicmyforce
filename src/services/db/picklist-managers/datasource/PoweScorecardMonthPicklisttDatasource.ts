import { CoachingPlanScheme } from '../../../../models/scheme/CoachingPlanScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { PoweScorecardPicklistManager } from './../PoweScorecardPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class PoweScorecardMonthPicklisttDatasource extends PickListDatasource {
  
  constructor(private poweScorecardPicklistManager: PoweScorecardPicklistManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return CoachingPlanScheme.fields.month.sfdc;
  }
  
  pickListManager() {
    return this.poweScorecardPicklistManager;
  }
}
