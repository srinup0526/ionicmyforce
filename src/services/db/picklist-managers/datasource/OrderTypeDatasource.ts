import { OrderScheme } from '../../../../models/scheme/OrderScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { OrderPicklistManager } from './../OrderPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";
import {OrdersTypeList} from "./../../../../pages/order-management/order-list/OrdersTypeList";


@Injectable({
  providedIn: 'root'
})
export class OrderTypeDatasource extends PickListDatasource {
  
  constructor(localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  getItems() {
    return OrdersTypeList.localizedResources(this.localizationManager)
      .then((orderTypes) => {
        const picklistValues = orderTypes.map((type) => ({
          id: type.id,
          description: type.description
        }));
        
        return picklistValues;
      })
  }
}
