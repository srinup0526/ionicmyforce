import { TotScheme } from '../../../../models/scheme/TotScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { TOTPicklistManager } from './../TOTPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class TOTEventPickListDatasource extends PickListDatasource {
  
  constructor(private totPicklistManager: TOTPicklistManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return [TotScheme.fields.firstQuarterEvent.sfdc];
  }

  pickListManager() {
    return this.totPicklistManager;
  }
}
