import { OrganizationScheme } from '../../../../models/scheme/OrganizationScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { OrganizationRecordTypePickListManager } from './../OrganizationRecordTypePickListManager';
import { Injectable } from "@angular/core";
import {OrganizationPickListManager} from "../OrganizationPicklistmanager";


@Injectable({
  providedIn: 'root'
})
export class OrganizationJuridicGroupPicklistDatasource extends PickListDatasource {
  
  constructor(private organizationPickListManager: OrganizationPickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return OrganizationScheme.fields.juridicGroup.sfdc;
  }
  
  pickListManager() {
    return this.organizationPickListManager;
  }
}
