import { DataChangeRequestScheme } from '../../../../models/scheme/DataChangeRequestScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { DataChangeRequestPickListManager } from './../DataChangeRequestPickListManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class DataChangeRequestRefStatusPicklistDatasource extends PickListDatasource {
  
  constructor(private dcrPickListManager: DataChangeRequestPickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }

  pickListName() {
    return DataChangeRequestScheme.fields.dcrReferenceCStatus.sfdc;
  }

  pickListManager() {
    return this.dcrPickListManager;
  }
}

