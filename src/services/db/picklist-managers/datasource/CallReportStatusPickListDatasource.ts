import { CallReportScheme } from '../../../../models/scheme/CallReportScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { CallReportPicklistManager } from './../CallReportPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class CallReportStatusPickListDatasource extends PickListDatasource {
  
  constructor(private callReportPicklistManager: CallReportPicklistManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return CallReportScheme.fields.status.sfdc;
  }

  pickListManager() {
    return this.callReportPicklistManager;
  }
}
