import { OrderScheme } from '../../../../models/scheme/OrderScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { OrderPicklistManager } from './../OrderPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class OrderStatusPickListDatasource extends PickListDatasource {
  
  constructor(private orderPicklistManager: OrderPicklistManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return OrderScheme.fields.status.sfdc;
  }

  pickListManager() {
    return this.orderPicklistManager;
  }
}
