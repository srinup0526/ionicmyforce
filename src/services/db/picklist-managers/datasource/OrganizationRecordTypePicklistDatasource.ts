import { OrganizationScheme } from '../../../../models/scheme/OrganizationScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { OrganizationRecordTypePickListManager } from './../OrganizationRecordTypePickListManager';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class OrganizationRecordTypePicklistDatasource extends PickListDatasource {
  
  constructor(private organizationPickListManager: OrganizationRecordTypePickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return OrganizationScheme.RECORD_TYPE_FIELD_NAME;
  }
  
  pickListManager() {
    return this.organizationPickListManager;
  }
}
