import { OrganizationScheme } from '../../../../models/scheme/OrganizationScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { OrganizationRecordTypePickListManager } from './../OrganizationRecordTypePickListManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class OrganizationSubtypePicklistDatasource extends PickListDatasource {
  
  constructor(private organizationPickListManager: OrganizationRecordTypePickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return OrganizationScheme.SUBTYPE_FIELD_NAME;
  }
  
  pickListManager() {
    return this.organizationPickListManager;
  }
}
