import { DataChangeRequestScheme } from '../../../../models/scheme/DataChangeRequestScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { DataChangeRequestPickListManager } from './../DataChangeRequestPickListManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class DataChangeRequestTypePicklistDatasource extends PickListDatasource {
  
  constructor(private dcrPickListManager: DataChangeRequestPickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  getItems() {
    return this.dcrPickListManager.getPickList(DataChangeRequestScheme.fields.dcrType.sfdc)
      .then((picklist) => {
        return this.preparePickListItems(picklist);
      })
  }
  
}
