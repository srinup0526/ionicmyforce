import { PharmaEventScheme } from '../../../../models/scheme/PharmaEventScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { PEPicklistManager } from './../PEPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class PETypeEventPickListDatasource extends PickListDatasource {
  
  constructor(private pePicklistManager: PEPicklistManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return [PharmaEventScheme.fields.eventType.sfdc];
  }

  pickListManager() {
    return this.pePicklistManager;
  }
}
