import {MedicalQueryScheme} from '../../../../models/scheme/MedicalQueryScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { MQPickListManager } from './../MQPickListManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class MQPickListDatasource extends PickListDatasource {
  
  constructor(private mqPickListManager: MQPickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return MedicalQueryScheme.fields.typeofquery.sfdc;
  }
  
  pickListManager() {
    return this.mqPickListManager;
  }
}
