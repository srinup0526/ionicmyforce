import { ContactScheme } from '../../../../models/scheme/ContactScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { ContactSpecialtiesPriorityPickListManager } from './../ContactSpecialtiesPriorityPicklistManager';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { Injectable } from "@angular/core";


@Injectable()
export class SpecialtyPickListDatasource extends PickListDatasource {
  
  constructor(private contactSpecialtiesPriorityPickListManager: ContactSpecialtiesPriorityPickListManager,
              localizationManager: LocalizationManager) {
    super(localizationManager);
  }
  
  pickListName() {
    return ContactScheme.SPECIALTY_FIELD_NAME;
  }
  
  pickListManager() {
    return this.contactSpecialtiesPriorityPickListManager;
  }
}
