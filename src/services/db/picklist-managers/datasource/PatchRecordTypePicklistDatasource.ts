import { PatchCustomerScheme } from '../../../../models/scheme/PatchCustomerScheme';
import { PickListDatasource } from './../base/PicklistDatasource';
import { LocalizationManager } from './../../../../services/common/localizations/LocalizationManager';
import { PatchRecordTypePickListManager } from './../PatchRecordTypePickListManager';
import { Injectable } from "@angular/core";
import { RecordTypeCollection } from '../../../../collections/RecordTypeCollection'



@Injectable({
  providedIn: 'root'
})
export class PatchRecordTypePicklistDatasource extends PickListDatasource {
  
  constructor(private patchRecordTypePickListManager: PatchRecordTypePickListManager,
              localizationManager: LocalizationManager, private recordTypeCollection:RecordTypeCollection) {
    super(localizationManager);
  }
  
  pickListName() {
    return PatchCustomerScheme.RECORD_TYPE_FIELD_NAME;
  }
  
  pickListManager() {
    return this.patchRecordTypePickListManager;
  }

  getItems() {
    return this.recordTypeCollection.getRecordTypesBysObjectTypeId(PatchCustomerScheme.sfdcTable)
      .then((recordTypes) => {
        console.log("recordTypes",recordTypes);
        const picklistValues = [{id:'',description:'None'}];
        recordTypes.map((type) => (picklistValues.push({
          id: type.id,
          description: type.name
        })));
        
        return picklistValues;
      })
  }
}
