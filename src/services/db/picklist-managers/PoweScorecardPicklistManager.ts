import { CoachingPlanScheme } from '../../../models/scheme/CoachingPlanScheme';
import { PicklistDatasourceManager } from './base/PicklistDatasourceManager';
import { Injectable } from "@angular/core";


@Injectable()
export class PoweScorecardPicklistManager extends PicklistDatasourceManager {
  
  targetModel() {
    return CoachingPlanScheme;
  }
  
  fieldNames() {
    return  [CoachingPlanScheme.fields.month.sfdc, CoachingPlanScheme.fields.year.sfdc, CoachingPlanScheme.fields.designation.sfdc];
  }
}
