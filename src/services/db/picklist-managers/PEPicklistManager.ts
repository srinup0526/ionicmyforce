import {PharmaEventScheme} from '../../../models/scheme/PharmaEventScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable()
export class PEPicklistManager extends PicklistDatasourceManager {

  targetModel() {
    return PharmaEventScheme;
  }

  fieldNames() {
    return [PharmaEventScheme.fields.stage.sfdc,PharmaEventScheme.fields.eventType.sfdc,PharmaEventScheme.fields.discussionType.sfdc];
  }
}
