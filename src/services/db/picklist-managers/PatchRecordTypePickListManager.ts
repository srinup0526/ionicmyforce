import { PatchScheme } from '../../../models/scheme/PatchScheme';
import { PicklistDatasourceManager } from './base/PicklistDatasourceManager';
import { Injectable } from "@angular/core";
import { PatchCollection } from '../../../collections/PatchCollection';
import { LocalizationManager } from './../../common/localizations/LocalizationManager';



@Injectable({
  providedIn: 'root'
})
export class PatchRecordTypePickListManager extends PicklistDatasourceManager{

  
  constructor(localizationManager: LocalizationManager, private patchCollection: PatchCollection){
    super(localizationManager);
  }

  targetModel() {
    return PatchScheme;
  }
  
  fieldNames() {
    return [PatchScheme.fields.name.local];
  }

  getPickLists() {
    if(this.pickLists && this.pickLists[this.targetModel().table]) {
      return Promise.resolve(this.pickLists);
    }
    
    return this.patchCollection.fetchAll()
      .then(this.patchCollection.getAllEntitiesFromResponse.bind(this.patchCollection))
      .then((patches) => {
        const field = this.fieldNames()[0];
  
        const pickLists = patches.map((patch) => ({
          label: patch[field],
          value: patch.id
        }));
     
        this.pickLists = this.pickLists || {};
  
        this.pickLists[field] = pickLists;
        
        return this.pickLists;
      });
  }
}
