import {OrganizationScheme} from '../../../models/scheme/OrganizationScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class OrganizationPickListManager extends PicklistDatasourceManager {
  targetModel() {
    return {
      sfdcTable: OrganizationScheme.sfdcTable,
      table: OrganizationScheme.table
    };
  }

  fieldNames() {
    return [
      OrganizationScheme.fields.juridicGroup.sfdc,
      OrganizationScheme.fields.status.sfdc
    ]
  }
}
