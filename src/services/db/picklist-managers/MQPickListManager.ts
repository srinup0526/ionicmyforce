import {MedicalQueryScheme} from '../../../models/scheme/MedicalQueryScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {PicklistManager} from './base/PicklistManagers';
import {Injectable} from "@angular/core";
import { LocalizationManager } from './../../common/localizations/LocalizationManager';
import {MedicalQueriesCollection} from "../../../collections/MedicalQueriesCollection/MedicalQueriesCollection";


@Injectable()
export class MQPickListManager extends PicklistDatasourceManager{
  
  constructor(localizationManager: LocalizationManager, private mqCollection: MedicalQueriesCollection){
    super(localizationManager);
  }
  
  targetModel() {
    return MedicalQueryScheme;
  }
  
  fieldNames() {
    return [MedicalQueryScheme.fields.typeofquery.sfdc];
  }
  
  // getPickLists() {
  //   if(this.pickLists && this.pickLists[this.targetModel().table]) {
  //     return Promise.resolve(this.pickLists);
  //   }
    
  //   return this.mqCollection.fetchAll()
  //     .then(this.mqCollection.getAllEntitiesFromResponse.bind(this.mqCollection))
  //     .then((queries) => {
  //       const field = this.fieldNames()[0];
  
  //       const pickLists = queries.map((query) => ({
  //         label: query[field],
  //         value: query.id
  //       }));
     
  //       this.pickLists = this.pickLists || {};
  
  //       this.pickLists[field] = pickLists;
        
  //       return this.pickLists;
  //     });
  // }

}
