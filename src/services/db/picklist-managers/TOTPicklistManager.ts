import {TotScheme} from '../../../models/scheme/TotScheme';
import {PicklistDatasourceManager} from './base/PicklistDatasourceManager';
import {Injectable} from "@angular/core";


@Injectable()
export class TOTPicklistManager extends PicklistDatasourceManager {

  targetModel() {
    return TotScheme;
  }

  fieldNames() {
    return [TotScheme.fields.firstQuarterEvent.sfdc,TotScheme.fields.type.sfdc];
  }
}
