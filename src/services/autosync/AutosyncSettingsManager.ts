// AutosyncSyncManager = require 'common/autosync/autosync-SyncManager'
import {Utils} from '../../utils/Utils';
import SettingsManager from './../db/SettingsManager';


class AutosyncSettingsManager {
  INTERVAL_ID: any;
  DEFAULT_DURATION: number;
  SETTINGS_FIELD_NAME;
  SETTINGS_FIELD_NAME_DURATION;
  SETTINGS_FIELD_LAST_SYNC_DATE;
  isLoading: boolean;
  isPaused: boolean;
  subscribeStack: Array<any>;
  onPorgressCallback;
  autosyncSyncManager;

  constructor() {
    this.INTERVAL_ID = 0;
    this.DEFAULT_DURATION = 5;
    this.SETTINGS_FIELD_NAME = 'isAutosyncEnable';
    this.SETTINGS_FIELD_NAME_DURATION = 'AutosyncDuration';
    this.SETTINGS_FIELD_LAST_SYNC_DATE = 'AutosyncLastSyncDate';
    this.isLoading = false;
    this.isPaused = false;
    this.subscribeStack = [];
  }

  isEnable() {
    return SettingsManager.getValueByKey(this.SETTINGS_FIELD_NAME);
  }

  enable() {
    this.runAutosync();
    SettingsManager.setValueByKey(this.SETTINGS_FIELD_NAME, true);
  }

  disable() {
    this.stopAutosync();
    SettingsManager.setValueByKey(this.SETTINGS_FIELD_NAME, false);
  }

  setSyncLastDate() {
    const date = Utils.currentDateToSalesForceDateTimeFormat();

    return SettingsManager.setValueByKey(this.SETTINGS_FIELD_LAST_SYNC_DATE, date)
      .then(() => date);
  }

  getSyncLastDate() {
    return SettingsManager.getValueByKey(this.SETTINGS_FIELD_LAST_SYNC_DATE)
      .then((date) => date ? date : this.setSyncLastDate());
  }

  changeSyncDuration(duration) {
    return SettingsManager.setValueByKey(this.SETTINGS_FIELD_NAME_DURATION, duration)
      .then(this.restartAutosync.bind(this));
  }

  getSyncDuration() {
    return SettingsManager.getValueByKey(this.SETTINGS_FIELD_NAME_DURATION)
      .then((duration) => {
        if (duration) {
          return duration;
        }

        return SettingsManager.setValueByKey(this.SETTINGS_FIELD_NAME_DURATION, this.DEFAULT_DURATION)
          .then(() => duration);
      });
  }

  restartAutosync() {
    this.stopAutosync();
    return this.runAutosync();
  }

  runAutosync() {
    return this.getSyncDuration()
      .then((duration) => {
        const msDuration = duration * 60 * 1000;
        this.INTERVAL_ID = setInterval(this.sync, msDuration);
        console.log('*** AUTO SYNC RUNNED WITH DURATION (min): ' + duration);
      });
  }

  stopAutosync() {
    clearInterval(this.INTERVAL_ID);
    console.log('*** AUTO SYNC STOPPED ');
  }

  init() {
    // window.AutosyncSettingsManager = this; // TODO: FOR DEVELOPING

    // this.autosyncSyncManager = new AutosyncSyncManager();
    // this.autosyncSyncManager.onProgress = this.onProgress;

    return this.isEnable()
      .then((value) => {
        console.log('*** AUTO SYNC ENABLED: ' + value);

        if (!value) {
          return;
        }

        this.runAutosync();
        return this.sync();
      });
  }

  onProgress(tableName, progressPercentage) {
    if (this.onPorgressCallback) {
      this.onPorgressCallback(tableName, progressPercentage);
    }
  }

  resetLoadingFlag() {
    this.isLoading = false;
  }

  setLoadingFlag() {
    this.isLoading = true;
  }

  pauseAutoSync() {
    console.log('AutoSync paused');
    this.isPaused = true;
  }

  continueAutoSync() {
    console.log('AutoSync continue');
    this.isPaused = false;
  }

  subscribeOnSyncFinish(callback) {
    if (this.subscribeStack.indexOf(callback) === -1) {
      this.subscribeStack.push(callback);
    }
  }

  unsubscribeOnSyncFinish(callback) {
    const index = this.subscribeStack.indexOf(callback);

    if (index !== -1) {
      return this.subscribeStack.splice(index, 1);
    }
  }

  broadcastOnSyncFinish() {
    return this.subscribeStack
      .filter((callback) => typeof callback === 'function')
      .forEach((callback) => callback());
  }

  addToQueueWithContinueAfterFuncPromise(funcPromise, onProgress) {
    return new Promise((resolve, reject) => {
      if (this.isLoading) {
        const onFinish = () => {
          this.unsubscribeOnSyncFinish(onFinish);
          this.onPorgressCallback = null;
          console.log('onFinish');
          resolve();
        };
        this.subscribeOnSyncFinish(onFinish);
        this.onPorgressCallback = onProgress;
      } else {
        resolve();
      }
    })
      .then(() => {
        this.pauseAutoSync();
        return funcPromise()
          .then(this.continueAutoSync.bind(this), this.continueAutoSync.bind(this));
      });
  }

  sync() {
    if (this.isPaused) {
      return;
    }

    return this.isEnable()
      .then((isEnable) => {
        if (isEnable) {
          if (this.isLoading) {
            console.log('Autosync running. Restarting ...');
            this.restartAutosync();
            return;
          }

          this.setLoadingFlag();

          return this.autosyncSyncManager.sync()
            .then(this.resetLoadingFlag.bind(this), this.resetLoadingFlag.bind(this))
            .then(this.broadcastOnSyncFinish.bind(this), this.broadcastOnSyncFinish.bind(this));
        } else {
          console.log('Autosync is disabled.');
        }
      });
  }
}

export default new AutosyncSettingsManager();
