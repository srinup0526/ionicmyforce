import { EntitiesCollection } from './EntitiesCollection';
import { SforceDataContext } from './SforceDataContext';
import { Utils } from '../utils/Utils';
import {BrandPlanning} from '../models/BrandPlanning';
import {BrandPlanningScheme} from '../models/scheme/BrandPlanningScheme';
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class BrandPlanningsCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    super();
    this.model = BrandPlanning;
    this.scheme = BrandPlanningScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise
    .then((config) => {
      return SforceDataContext.getActiveUser()
        .then((currentUser) => {
          config.query = [
            config.query,
            'WHERE',
            `CurrencyIsoCode = '${currentUser.currency}'`,
            'AND',
            `${this.scheme.fields.year.sfdc} = '${Utils.getYear()}'`,
            'AND',
            `${this.scheme.fields.status.sfdc} = '${BrandPlanning.APPROVE_STATUS_VALUE}'`
          ].join(' ');

          return config;
        });
      });
  }


  parseModel(result) {
    result[this.scheme.fields.customer.sfdc] = result.Customer__r && result.Customer__r.Contact__c || '';
    result[this.scheme.fields.year.sfdc] = result.BranMatrix_Cycle__r && result.BranMatrix_Cycle__r.Year__c || '';
    result[this.scheme.fields.cycle.sfdc] = result.BranMatrix_Cycle__r && result.BranMatrix_Cycle__r.Cycle__c || '';
    result[this.scheme.fields.status.sfdc] = result.BranMatrix_Cycle__r && result.BranMatrix_Cycle__r.Status__c || '';

    return super.parseModel(result);
  }

  didFinishDownloading(records) {
    return records;
  }


  fetchCurrentForAccount(customerId: string): Promise<BrandPlanning[]> {
    const whereFields = {};
    whereFields[this.scheme.fields.customer.sfdc] = customerId;
    whereFields[this.scheme.fields.year.sfdc] = Utils.getYear();
    whereFields[this.scheme.fields.cycle.sfdc] = BrandPlanning.getCycleNameFromQuarter(Utils.getQuarter());
    return this.fetchAllWhere(whereFields)
      .then((response) => this.getAllEntitiesFromResponse(response))
      .catch(error => {
        console.error('fetchCurrentForAccount brandPlannings error', customerId, error);
        return [];
      });
  }
}
