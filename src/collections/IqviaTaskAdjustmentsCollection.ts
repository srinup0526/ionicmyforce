import {EntitiesCollection} from './EntitiesCollection';
import {IqviaTaskAdjustment} from './../models/IqviaTaskAdjustment';
import {IqviaTaskAdjustmentScheme} from './../models/scheme/IqviaTaskAdjustmentScheme';
import { Injectable } from '@angular/core';
import {CallReportsCollection} from "./CallReportsCollection/CallReportsCollection";

@Injectable()
export class IqviaTaskAdjustmentsCollection extends EntitiesCollection {

  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = IqviaTaskAdjustment;
    this.scheme = IqviaTaskAdjustmentScheme;
    this.init();
  }

  public prepareServerConfig(configPromise) {
    return configPromise
      .then((config) => {
        return config;
      });
  }

  public fetchTaskAdjustmentsByCallReport(callReport: any = null){
    if(callReport){
      return this.fetchAllWhereIn(this.scheme.fields.callReport.sfdc, [callReport.id, callReport._soupEntryId])
        .then((response) => {
          console.log('fetchTaskAdjustmentsByCallReport', response);
          return this.getAllEntitiesFromResponse(response);
        })
    }
  else
    return Promise.resolve([]);
  }

  didStartUploading(entities) {
    let callReportsCollection = new CallReportsCollection();
    return callReportsCollection.linkEntitiesToEntity(entities, 'callReport');
  }

  didFinishDownloading(records) {
    return records;
  }
}
