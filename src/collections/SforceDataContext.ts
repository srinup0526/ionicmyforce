// import MarketingCyclesCollection = require('models/bll/marketing-cycles-collection');
import { Utils } from '../utils/Utils';
import SettingsManager from '../services/db/SettingsManager';
import StorageManager from '../services/db/StorageManager';
import { UsersCollection } from './UsersCollection';

declare var cordova: any;
declare var force: any;
const SFOAuthPlugin: any = cordova.require('com.salesforce.plugin.oauth');

export class SforceDataContext {
  private static currentCycle = null;
  private static activeTarget = null;

  static readonly CURRENCY_RUB: string = 'RUB';
  static mcsCollection: any; // = new MarketingCyclesCollection();
  static usersCollection: any = new UsersCollection();
  static activeUser;

  static cleanup() {
    SforceDataContext.activeUser = null;
    SforceDataContext.currentCycle = null;
    return SforceDataContext.activeTarget = null;
  }

  static getActiveUser() {
    if (SforceDataContext.activeUser != null) {
      return Promise.resolve(SforceDataContext.activeUser);
    } else {
      return SforceDataContext.usersCollection.loadActiveUser()
        .then((activeUser) => {
          SforceDataContext.activeUser = activeUser;
          return SforceDataContext.activeUser;
        });
    }
  }

  static activeUserType() {
    return SforceDataContext.getActiveUser()
      .then((activeUser) => activeUser.profileName);
  }

  static isVeropharmUser() {
    return SforceDataContext.getActiveUser()
      .then(activeUser => activeUser.currency === SforceDataContext.CURRENCY_RUB);
  }

  static reloadActiveUser() {
    SforceDataContext.activeUser = null;
    return SforceDataContext.getActiveUser();
  }

  static currentMarketingCycle() {
    if (!SforceDataContext.currentCycle) {
      const today = Utils.currentDate();

      return SforceDataContext.getActiveUser()
        .then(activeUser => SforceDataContext.mcsCollection.fetchByDateAndCurrency(today, activeUser.currency))
        .then(currentCycle => {
          SforceDataContext.currentCycle = currentCycle;
          return SforceDataContext.currentCycle;
        });
    } else {
      return Promise.resolve(SforceDataContext.currentCycle);
    }
  }

  static getAuthCredentials(): Promise<any> {
    return new Promise((resolve, reject) => {
      SFOAuthPlugin.getAuthCredentials(resolve, reject);
    });
  }

  static refreshToken() {
    return new Promise((resolve, reject) => {
      force.refreshToken(resolve, reject);
    });
  }

  static logout() {
    // TODO alarmManager
    // AlarmManager.cancelNotification();
    // TODO StorageManager as service
    // TODO SettingsManager as service
    return StorageManager.clearData().then(() => {
      return SettingsManager.settings = null;
    })
      .then(SFOAuthPlugin.logout, SFOAuthPlugin.logout);
  }
}
