import { EntitiesCollection } from './EntitiesCollection';
import { Attachment } from './../models/Attachment';
import { AttachmentScheme } from './../models/scheme/AttachmentScheme';
import { Query } from '../services/common/Query';


export class AttachmentCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = Attachment;
    this.scheme = AttachmentScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }


  didFinishDownloading(records) {
    return records;
  }
}
