import { EntitiesCollection } from './EntitiesCollection';
import { SalesPlanProduct } from "../models/SalesPlanProduct";
import { SalesPlanningProductScheme } from "../models/scheme/SalesPlanningProductScheme";
import {Injectable} from "@angular/core";
import { SalesPlanningListCollection } from './SalesPlanningListCollection'


@Injectable({
  providedIn: 'root'
})
export class SalesPlanningProductCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = SalesPlanProduct;
    this.scheme = SalesPlanningProductScheme;
    this.init();
  }

  // public parseModel(result) {
  //   return super.parseModel(result);
  // }
  parseModel(result) {
    console.log("before result",result);
    if (result.Product_Name__r) {
      result[this.scheme.fields.productName.sfdc] = result.Product_Name__r.Name;
    }
    console.log("after result",result);
    return super.parseModel(result);
  }
  prepareServerConfig(configPromise) {
    return configPromise;
  }

  fetchSalesPlanningListById(salesPlanList)
  {
    const query = this._fetchAllQuery();
    
    query.whereIn(this.scheme.fields.salesPlanningList.sfdc,[salesPlanList.id, salesPlanList._soupEntryId]);
    console.log("query",query);
    console.log("query.query",query.query);
    return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }

  didStartUploading(entities:any)
  {
    let salesPlanningListCollection =  new SalesPlanningListCollection;
    return salesPlanningListCollection.linkEntitiesToEntity(entities, 'salesPlanningList')
  }

  didFinishDownloading(records) {
    return records;
  }
}