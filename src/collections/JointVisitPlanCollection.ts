import { EntitiesCollection } from './EntitiesCollection';
import { JointVisitPlan } from './../models/JointVisitPlan';
import { JointVisitPlanScheme } from './../models/scheme/JointVisitPlanScheme';
import { Query } from '../services/common/Query';
import { CoachingPlanCollection } from "../collections/CoachingPlanCollection";
import { ConfigurationManager } from './../services/db/ConfigurationManager';
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class JointVisitPlanCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    super();
    this.model = JointVisitPlan;
    this.scheme = JointVisitPlanScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;//.then((config) => {
        // return ConfigurationManager.getConfig('brickRecordTypeId')
        //     .then((brickRecordTypeId) => {
        //       config.query += ` WHERE RecordTypeId = '${brickRecordTypeId}'`;
        //       return config;
        //     });
      //   return config;
      // });
  }

  parseModel(result) {
    if (result.MedRep__r) {
      result[this.scheme.fields.medrepName.sfdc] = result.MedRep__r.Name;
    }
    if (result.FLM__r) {
      result[this.scheme.fields.flmName.sfdc] = result.FLM__r.Name;
    }
    if (result.Development_Plan__r) {
      result[this.scheme.fields.developmentPlanName.sfdc] = result.Development_Plan__r.Name;
    }
    return super.parseModel(result);
  }

  fetchEntityByCoachingPlan(coachingPlanId)
  {
    const query = this._fetchAllQuery();
    const fetchData = {};
    fetchData[this.scheme.fields.developmentplan.sfdc] = coachingPlanId;
    query.where(fetchData);
    return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }

  didFinishDownloading(records) {
    return records;
  }
}
