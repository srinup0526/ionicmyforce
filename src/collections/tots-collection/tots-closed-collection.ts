import { TotsCollection } from '../../collections/tots-collection/tots-collection';
import { Query } from './../../services/common/Query';


export class TotsClosedCollection extends TotsCollection {

    constructor()
    {
        super();
        console.log("this.scheme",this.scheme);
    }

    _dataType()
    {
        const fieldValue={};
        fieldValue["Type__c"] = this.scheme.TYPE_CLOSED;
        return fieldValue;
    }
    _fetchAllQuery() {
        const dataType = this._dataType();
        console.log("dataType",dataType);
        const query = new Query().selectFrom(this.scheme.table);
        console.log("query",query)
        if(dataType==TotsCollection.ALL_TYPES) { return query }else { return query.where(dataType) }
    }
  
}
