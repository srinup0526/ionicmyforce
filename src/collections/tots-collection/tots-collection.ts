import { EntitiesCollection } from './../EntitiesCollection';
import { Tot } from './../../models/tot';
import { TotScheme } from './../../models/scheme/TotScheme';
import { Query } from './../../services/common/Query';
import {ConfigurationManager} from '../../services/db/ConfigurationManager';
import { Utils } from '../../utils/Utils';
import moment from 'moment';
import * as _ from 'lodash';
declare let $:any;
import {PicklistManager} from './../../services/db/picklist-managers/base/PicklistManagers';
import {StoreConfig} from "./../../services/synchronisation/StoreConfig";
declare function getForceSyncClient();

export class TotsCollection extends EntitiesCollection {
  public model;
  public scheme;
 
  public static readonly ALL_TYPES: string = 'ALL_TYPES';
  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = Tot;
    this.scheme = TotScheme;
    this.init();
    this.cache.noMerge = false;
  }
  prepareServerConfig(configPromise) {
    return configPromise.then((config) =>{
    return ConfigurationManager.getConfig('numberOfMonthsForCalls')
      .then((numberOfMonthsForCalls) => {
        const minTotDate = Utils.originalDate(moment().subtract(numberOfMonthsForCalls,'months').date(1));
        config.query += ` WHERE ${this.scheme.fields.endDate.sfdc} >= ${minTotDate}`;
        return config;
      })
    })
  }

  public preparePicklists() {
    PicklistManager.clearCache(this.scheme.sfdcTable);
    
    return PicklistManager.initSoup()
      .then(this.fetchUniqueEventQuarterType.bind(this))
      .then((picklistObject) => {
        console.log("picklistObject",picklistObject);
        let eventObject = {
          fieldName: this.scheme.fields.firstQuarterEvent.sfdc,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.fields.firstQuarterEvent.sfdc}`,
          picklistOptions: picklistObject.eventPicklist.slice(0)
        };
      
        let typeObject = {
          fieldName: this.scheme.fields.type.sfdc,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.fields.type.sfdc}`,
          picklistOptions: picklistObject.typePicklist.slice(0)
        };
        
        return getForceSyncClient().smartstoreClient.upsertSoupEntriesWithExternalId(
          StoreConfig.getConfig(),
          PicklistManager.soupName,
          [eventObject, typeObject],
          PicklistManager.identityKey
        );
      });
  }

  private mapPicklistObjectsArray(records) {
    return records
      .filter((record) => record)
      .map((record) => ({label: record, value: record}))
  }

  private fetchUniqueEventQuarterType() {
    let eventArray = [];
    let typeArray = [];

    return this.distinctQuery(this.scheme.fields.firstQuarterEvent.sfdc)
    .then((response) => {
      eventArray = this.mapPicklistObjectsArray(response.records);
      
      return this.distinctQuery(this.scheme.fields.firstQuarterEvent.sfdc);
    })
    .then ((response) => {
      typeArray = this.mapPicklistObjectsArray(response.records);
      
      return this.distinctQuery(this.scheme.fields.type.sfdc);
    })
    .then((response) => {
      return {
        eventPicklist: eventArray,
        typePicklist: typeArray
      }
    })
  }
  

  didFinishDownloading(records) {
    return records;
  }
  parseModel(result){
    if(result.Owner)
    {
      result[this.scheme.fields.userFirstName.sfdc] = result.Owner.FirstName;
      result[this.scheme.fields.userLastName.sfdc] = result.Owner.LastName;
    }
    
    return super.parseModel(result);
  }

  
  _dataType()
  {
    const fieldValue={};
    
    // fieldValue[this.scheme.sfdc.type] = TotsCollection.ALL_TYPES;
    
    return fieldValue;
  }

  _fetchAllQuery() {
      const dataType = this._dataType()
      const query = new Query().selectFrom(this.scheme.table);
      
      if(dataType==TotsCollection.ALL_TYPES) { return query }else { return query.where(dataType) }
  }
  warningErrorCodes():any{
    return _.union(super.warningErrorCodes, ['ENTITY_IS_LOCKED']);
  }
  
  handleErrorForEntity(error, entity){
    return $.when(super.handleErrorForEntity(error, entity))
    .then((res)=>
    {
       if(entity && error.errorCode == 'ENTITY_IS_LOCKED'){
        return super.markEntityAsNotModified(entity);
       }
    })
  }
}
