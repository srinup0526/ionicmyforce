
import { TotsCollection } from '../../collections/tots-collection/tots-collection';
import { Query } from './../../services/common/Query';


export class TotsOpenCollection extends TotsCollection {

    _dataType()
    {
        const fieldValue={};
        fieldValue["Type__c"] = this.scheme.TYPE_OPEN;
        return fieldValue;
    }

    _fetchAllQuery() {
        const dataType = this._dataType()
        const query = new Query().selectFrom(this.scheme.table);
        
        if(dataType==TotsCollection.ALL_TYPES) { return query }else { return query.where(dataType) }
    }

}
