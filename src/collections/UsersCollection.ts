import { EntitiesCollection } from './EntitiesCollection';
import SettingsManager from './../services/db/SettingsManager';
import {UserScheme} from '../models/scheme/UserScheme';
import {User} from './../models/User';

declare function getForceSyncClient();


export class UsersCollection extends EntitiesCollection {
  public model;

  constructor() {
    super();
    this.model = User;
    this.scheme = UserScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
      config.query += ' WHERE IsActive = true';
      return config;
    });
  }

  parseModel(result) {
    if (result.Manager) {
      result[this.scheme.fields.managerId.sfdc] = result.Manager.Id;
      result[this.scheme.fields.managerFirstName.sfdc] = result.Manager.FirstName;
      result[this.scheme.fields.managerLastName.sfdc] = result.Manager.LastName;
    }

    return super.parseModel(result);
  }

  _fetchAllQuery() {
    const activeUser = {};
    activeUser[this.scheme.fields.isActive.sfdc] = true;
    return super._fetchAllQuery().where(activeUser);
  }

  public loadActiveUser() {
    const fieldsValues = {};

    fieldsValues[this.scheme.fields.id.sfdc] = getForceSyncClient().userId;

    return this.fetchAllLike(fieldsValues)
      .then(this.getAllEntitiesFromResponse.bind(this))
      .then((records) => {
        const result = records.filter((record) => {
          return record.id.indexOf(getForceSyncClient().userId) === 0;
        });
        return result[0];
      });
  }

  didFinishDownloading(records) {
    return this.loadActiveUser()
      .then((user) => {
        const isChinaUser = user.isChinaUser();
        return SettingsManager.setValueByKey('isChinaUser', isChinaUser);
      })
      .then(() => records);
  }

  fetchParticipantUsers(usersString = '') {
    return this.fetchWithQuery(this._getParticipantUsersFetchQuery(usersString))
      .then(this.getAllEntitiesFromResponse.bind(this));
  }

  _getParticipantUsersFetchQuery(usersString) {
    return this._fetchAllQuery()
      .whereIn(this.scheme.fields.name.sfdc, usersString.split('; '));
  }

  public fetchForUserIds (userIds) {
    const query = this._fetchAllQuery()
      .whereIn(this.scheme.fields.id.sfdc, userIds);
    
    return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }
}
