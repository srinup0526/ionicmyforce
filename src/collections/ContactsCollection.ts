import {Injectable} from "@angular/core";
import {EntitiesCollection} from './EntitiesCollection';
import {Contact} from './../models/Contact';
import {ContactScheme} from './../models/scheme/ContactScheme';
import {TargetFrequencyScheme} from './../models/scheme/TargetFrequencyScheme';
import {TargetFrequenciesCollection} from './TargetFrequenciesCollection';
import {BuTeamPersonProfilesCollection} from './BuTeamPersonProfilesCollection';
import {SforceDataContext} from './SforceDataContext';
import {Utils} from "../utils/Utils";
import {PicklistManager} from './../services/db/picklist-managers/base/PicklistManagers';
import {Query} from "../services/common/Query";
import {StoreConfig} from "./../services/synchronisation/StoreConfig";
declare function getForceSyncClient();


@Injectable()
export class ContactsCollection extends EntitiesCollection{
  public model;
  public scheme;
  
  constructor() {
    super();
    this.model = Contact;
    this.scheme = ContactScheme;
    this.init();
  }
  
  public parseModel(result) {
    if (result.Account) {
      result[this.scheme.fields.accountId.sfdc] = result.Account.Id;
      result[this.scheme.fields.recordType.sfdc] = result.Account.RecordType.Name;
      result[this.scheme.fields.specialty.local] = result.Account.C_Specialty_1__c;
      result[this.scheme.fields.organizationName.local] = result.Account.Name;
      result[this.scheme.fields.remoteOrganizationName.sfdc] = result.Account.Name;
      
    }
  
    const targetFrequenciesCollection = new TargetFrequenciesCollection();
    
    if(result.TMF1__r) {
      result.lastDateTargetFrequency = targetFrequenciesCollection.parseModel(result.TMF1__r);
      delete result.TMF1__r;
    } else if(result.lastDateTargetFrequency) {
      result.lastDateTargetFrequency = targetFrequenciesCollection.parseModel(result.lastDateTargetFrequency);
    }
    
    return super.parseModel(result);
  }
  
  public prepareServerConfig(configPromise) {
    return configPromise
      .then((config) => {
        return SforceDataContext.getActiveUser()
          .then((activeUser) => {
            const today = Utils.currentDate();
            const isActive = TargetFrequencyScheme.fields.isActive.sfdc;
            const mcr = 'Marketing_Cycle__r';
  
            const mcStartDate = 'Start_Date__c';
            const mcEndDate = 'End_Date__c';
            const mcCurrency = 'CurrencyIsoCode';
            
          return this._mapFieldsList()
            .then((fields) => {
              config.query =
                `SELECT ${fields.join(',')}, (SELECT ${TargetFrequencyScheme.sfdcFields.join(',')} ` +
                'FROM TMF1__r ' +
                `WHERE ${isActive} = true AND ${mcr}.${mcStartDate} <= ${today} AND ${mcr}.${mcEndDate} >= ${today} AND ${mcr}.${mcCurrency} = '${activeUser.currency}') ` +
                `FROM ${this.scheme.sfdcTable} ` +
                `WHERE ${this.scheme.fields.status.sfdc} = '${this.scheme.STATUS_ACTIVE}'`;
              
              return config;
            })
          })
      })
  }
  
  public fetchForContactIds (contactIds) {
    const query = this._fetchAllQuery()
      .whereIn(this.scheme.fields.id.sfdc, contactIds);
    
    return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }
  
  public preparePicklists() {
    PicklistManager.clearCache(this.scheme.sfdcTable);
    
    return PicklistManager.initSoup()
      .then(this.fetchUniqueSpecialtyPriority.bind(this))
      .then((picklistObject) => {
      
        let priorityObject = {
          fieldName: this.scheme.PRIORITY_FIELD_NAME,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.PRIORITY_FIELD_NAME}`,
          picklistOptions: picklistObject.priority.slice(0)
        };
      
        let specialtyObject = {
          fieldName: this.scheme.SPECIALTY_FIELD_NAME,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.SPECIALTY_FIELD_NAME}`,
          picklistOptions: picklistObject.specialty.slice(0)
        };
      
        let buSpecialtyObject = {
          fieldName: this.scheme.BU_SPECIALTY_FIELD_NAME,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.BU_SPECIALTY_FIELD_NAME}`,
          picklistOptions: picklistObject.buSpecialty.slice(0)
        };
      
        let subtypeObject = {
          fieldName: this.scheme.SUBTYPE_FIELD_NAME,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.SUBTYPE_FIELD_NAME}`,
          picklistOptions: picklistObject.subtype.slice(0)
        };
        
        return getForceSyncClient().smartstoreClient.upsertSoupEntriesWithExternalId(
          StoreConfig.getConfig(),
          PicklistManager.soupName,
          [priorityObject, specialtyObject, buSpecialtyObject, subtypeObject],
          PicklistManager.identityKey
        );
      });
  }
  
  public saveRecordToCache(cache, records) {
    return this.updateContacts(cache, records);
  }
  
  
  public getContactByAccountId(accountId: string): Promise<Contact> {
    const whereField = {};
    
    whereField[this.scheme.fields.accountId.sfdc] = accountId;
    
    return this.fetchAllWhere(whereField)
      .then(this.getEntityFromResponse.bind(this));
  }
  
  private updateContacts(cache, contacts) {
    contacts = contacts.map((contact) => {
      if(contact.Account){
        contact.specialty = contact.Account.C_Specialty_1__c;
      }
      return contact;
    });
    const buTeamPersonProfilesCollection = new BuTeamPersonProfilesCollection();
    const contactOrganisationIds = contacts.map((contact)=>contact[this.scheme.fields.organizationSfId.sfdc]);
  
    return SforceDataContext.getActiveUser()
      .then((activeUser) => {
        return buTeamPersonProfilesCollection.fetchForUserUnitAndContacts(activeUser.businessUnit, contactOrganisationIds);
      })
      .then((profiles) => {
        contacts.forEach((contact) => {
          this.updateContactAsNonTarget(contact);
          this.updateContactTargetFrequency(contact);
          this.updateContactBuSpecialtyAndPriority(contact, this.profilesByOrganisationIds(profiles))
        })
      })
      .then(() => {
        return cache.saveAll(contacts);
      });
  }
  
  public didFinishDownloading(records) {
    return records;
  }
  
  
  private updateContactAsNonTarget(contact) {
    contact.isTargetCustomer = false
  }
  
  private updateContactTargetFrequency(contact) {
    if (contact.TMF1__r) {
      contact.TMF1__r = this.getTargetFrequencyInMarketingCycle(contact.TMF1__r.records) ;
    }
  }
  
  private getTargetFrequencyInMarketingCycle(targetFrequenciesInMarketingCycle = []) {
    const lastCallDate = TargetFrequencyScheme.fields.lastCallReportDate.sfdc;
    let lastTf = null;
  
    targetFrequenciesInMarketingCycle.forEach((currentTf) => {
      if((lastTf == null) || (currentTf && lastTf[lastCallDate] && (lastTf[lastCallDate] < currentTf[lastCallDate]))) {
        lastTf = currentTf;
      }
    });
    return lastTf;
  }
  
  private updateContactBuSpecialtyAndPriority(contact, profilesByOrgIds) {
    let profile = profilesByOrgIds[contact[this.scheme.fields.organizationSfId.sfdc]];
    
    if(!profile) {
      profile = {};
    }
    
    contact.buSpecialty = profile.specialty || '';
    contact.priority = profile.priority || '';
  }
  
  private profilesByOrganisationIds(profiles) {
    let profilesByOrgIds = {};
    
    profiles.forEach((profile) => profilesByOrgIds[profile.organizationSfid] = profile);
    
    return profilesByOrgIds;
  }
  
  private fetchUniqueSpecialtyPriority() {
    let specialtyArray = [];
    let buSpecialtyArray = [];
    let priorityArray = [];
    let subtype = [];

    return this.distinctQuery(this.scheme.SUBTYPE_FIELD_NAME)
    .then((response) => {
      subtype = this.mapPicklistObjectsArray(response.records);
      
      return this.distinctQuery(this.scheme.SPECIALTY_FIELD_NAME);
    })
    .then ((response) => {
      specialtyArray = this.mapPicklistObjectsArray(response.records);
      
      return this.distinctQuery(this.scheme.BU_SPECIALTY_FIELD_NAME);
    })
    .then((response) => {
      buSpecialtyArray = this.mapPicklistObjectsArray(response.records);
      
      return this.distinctQuery(this.scheme.PRIORITY_FIELD_NAME);
    })
    .then((response) => {
      priorityArray = this.mapPicklistObjectsArray(response.records);
      
      return {
        subtype: subtype,
        specialty: specialtyArray,
        buSpecialty: buSpecialtyArray,
        priority: priorityArray
      }
    })
  }
  
  private distinctQuery(fieldName) {
    const  maxValue = 99999999;
    const query = new Query()
      .customQuery(`SELECT DISTINCT {${this.scheme.table}:${fieldName}} FROM {${this.scheme.table}} WHERE {${this.scheme.table}:${fieldName}} != 'null'`);
    
    const smartSql = this._createSmartSqlQuery(query, this.scheme.table, maxValue);
    
    return this.fetchRemoteObjectsFromCache(this.cache, smartSql, false);
  }
  
  private mapPicklistObjectsArray(records) {
    return records
      .filter((record) => record)
      .map((record) => ({label: record, value: record}))
  }

}

