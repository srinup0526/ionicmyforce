import { EntitiesCollection } from './EntitiesCollection';
import { TargetFrequency } from './../models/TargetFrequency';
import { TargetFrequencyScheme } from './../models/scheme/TargetFrequencyScheme';
import { SforceDataContext } from './SforceDataContext';
import { MarketingCycleScheme } from './../models/scheme/MarketingCycleScheme';
import { ContactScheme } from './../models/scheme/ContactScheme';
import { Injectable } from "@angular/core";
import { Utils } from './../utils/Utils';
import { Query } from '../services/common/Query';
import { ContactsCollection } from "./ContactsCollection";
declare var _: any;


@Injectable()
export class TargetFrequenciesCollection extends EntitiesCollection {
  public model;
  public scheme;
  
  constructor() {
    super();
    this.model = TargetFrequency;
    this.scheme = TargetFrequencyScheme;
    this.init();
  }
  
  public parseModel(result) {
    if (result.Target__r && result.Target__r.MedRep__r) {
      result[this.scheme.fields.medrepId.sfdc] = result.Target__r.MedRep__r.Id;
      result[this.scheme.fields.medrepFirstName.sfdc] = result.Target__r.MedRep__r.FirstName;
      result[this.scheme.fields.medrepLastName.sfdc] = result.Target__r.MedRep__r.LastName;
    }
    return super.parseModel(result);
  }
  
  public prepareServerConfig(configPromise) {
    return configPromise
      .then((config) => {
        return SforceDataContext.getActiveUser()
          .then((activeUser) => {
            const today = Utils.currentDate();
            const mcr = 'Marketing_Cycle__r';
            
            const mcStartDate = MarketingCycleScheme.fields.startDate.sfdc;
            const mcEndDate = MarketingCycleScheme.fields.endDate.sfdc;
            const mcCurrency = MarketingCycleScheme.fields.currencyIsoCode.sfdc;
  
            config.query += ` WHERE ${this.scheme.fields.isActive.sfdc} = true AND ${mcr}.${mcStartDate} <= ${today} AND ${mcr}.${mcEndDate} >= ${today} AND ${mcr}.${mcCurrency} = '${activeUser.currency}'`;
            
            return config;
          });
      });
  }
  
  public didFinishDownloading(records) {
    return this.updateTargetContacts(records);
  }
  
  private updateTargetContacts(targetFrequencies) {
    return this.fetchUnparsedWithQuery(this.queryForCurrentMCTFsContacts())
    .then(this.getAllUnparsedEntitiesFromResponse.bind(this))
    .then((contacts) => SforceDataContext.getActiveUser()
      .then((activeUser) => [contacts, activeUser])
    )
    .then(([contacts, activeUser]) => {
      const filteredTargetFrequenciesByMedrep = this.filterTargetFrequenciesByMedrep(targetFrequencies, activeUser);
      const tfsByCustomerIds = this.targetFrequenciesByCustomerIds(filteredTargetFrequenciesByMedrep);
      
      const updatedContacts = contacts.map((contact) => {
        this.updateContactAsTarget(contact);
        this.updateContactPriorities(contact, tfsByCustomerIds);
        return contact;
      });
      const contactsCollection = new ContactsCollection();
      const keepUnparsed = true;
      
      return contactsCollection.upsertEntitiesSilently(updatedContacts, keepUnparsed);
    });
  }
 
  
  private queryForCurrentMCTFsContacts() {
    const contacts = ContactScheme.table;
    const mcs = MarketingCycleScheme.table;
    const tfs = this.scheme.table;
    
    const contactsId = ContactScheme.fields.id.sfdc;
    const mcsId = MarketingCycleScheme.fields.id.sfdc;
    
    const tfsCustomerId = this.scheme.fields.customerSfId.sfdc;
    const tfsMCId = this.scheme.fields.marketingCycleSfId.sfdc;
    
    const mcStartDate = MarketingCycleScheme.fields.startDate.sfdc;
    const mcEndDate = MarketingCycleScheme.fields.endDate.sfdc;
    const today = Utils.currentDate();
    const query = new Query(this.scheme.table);
    
    return query.customQuery(
      `SELECT {${contacts}:${Query.ALL}} ` +
      `FROM {${contacts}}, {${tfs}}, {${mcs}} ` +
      `WHERE {${tfs}:${tfsCustomerId}} = {${contacts}:${contactsId}} ` +
      `AND {${tfs}:${tfsMCId}} = {${mcs}:${mcsId}} AND {${mcs}:${mcStartDate}} <= ${query.valueOf(today)} AND {${mcs}:${mcEndDate}} >= ${query.valueOf(today)}`
    )
  }
  
  private targetFrequenciesByCustomerIds(tfs) {
    const tfsByCustomerIds = {};
    
    tfs.forEach((tf) => tfsByCustomerIds[tf[this.scheme.fields.customerSfId.sfdc]] = tf);
    
    return tfsByCustomerIds;
  }
  
  private filterTargetFrequenciesByMedrep(tfs, medrep) {
    return _(tfs).filter((tf) => tf[this.scheme.fields.medrepId.sfdc] == medrep.id);
  }
  
  private updateContactAsTarget(contact) {
    contact.isTargetCustomer = true;
  }
  
  private updateContactPriorities(contact, tfsByCustomerIds) {
    const tf = tfsByCustomerIds[contact[this.scheme.fields.id.sfdc]];
  
    if (tf) {
      return contact.priority = tf[ this.scheme.fields.priority.sfdc];
    }
  }
  
}
