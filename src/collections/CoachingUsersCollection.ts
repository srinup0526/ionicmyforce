import { UsersCollection } from './UsersCollection';
import * as _ from 'lodash';
import { SforceDataContext } from './SforceDataContext'


export class CoachingUsersCollection extends UsersCollection {

	constructor()
	{
		super();
	}

	// fetchAll() {
	// 	return SforceDataContext.getActiveUser()
	// 	.then(activeUser=>{
	// 		console.log("activeUser",activeUser);
	// 		return super.fetchAll().then(users=>{
	// 			console.log("users",users)
	// 			return this._findManagers(users.records,activeUser.managerId)
	// 		})
	// 	})
	// }

	_findManagers(users, managerId, managersRecords = {}){
		const currentManager = _.first(users.filter(user=>{ return user.id==managerId}));
		console.log("currentManager",currentManager);
		console.log("managersRecords",managersRecords);
		if(currentManager)
		{
			managersRecords[currentManager.id] = currentManager;
			return this._findManagers(users,currentManager.managerId,managersRecords);
		}
		else
		{
			return managersRecords;
		}
	}

	modifyResponse(users){
		return SforceDataContext.getActiveUser()
		.then(activeUser=>{
			const managers = this._findManagers(users.records,activeUser.managerId);
			console.log("managers",managers);
			users.records = users.records.filter((record) => { return managers[record.id]});
			console.log("users",users);
			return users;
		})
	}
}