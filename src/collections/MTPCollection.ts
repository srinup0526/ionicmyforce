import { EntitiesCollection } from './EntitiesCollection';
import { MTP } from './../models/MTP';
import { MTPScheme } from './../models/scheme/MTPScheme';
import { Query } from '../services/common/Query';
import { ReferenceCollection } from './references/ReferenceCollection';
import { ConfigurationManager } from './../services/db/ConfigurationManager';
import { CallReportsCollection } from './CallReportsCollection/CallReportsCollection';
import { MTPMonthPicklistDatasource } from './../services/db/picklist-managers/datasource/MTPMonthPicklistDatasource';
import { SforceDataContext } from './SforceDataContext';
import { Utils } from './../utils/Utils';
import moment from 'moment';
import SettingsManager from './../services/db/SettingsManager';
declare function getForceSyncClient();
declare var $:any;





export class MTPCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = MTP;
    this.scheme = MTPScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
      return SforceDataContext.getActiveUser()
      .then((activeUser) => {
        return ConfigurationManager.getConfig('numberOfMonthsForCalls')
        .then((numberOfMonthsForCalls) => {
          const minMTPDate = Utils.originalDate(moment().subtract(numberOfMonthsForCalls, 'months'))
          config.query += ` WHERE ${this.scheme.fields.currency.sfdc} = '${activeUser.currency}'`;
          config.query += ` AND ${this.scheme.fields.medRep.sfdc} = '${activeUser.id}'`;
          config.query += ` AND ${this.scheme.fields.endDate.sfdc} >= ${minMTPDate}`;
          return config;
        });
      });
    });
  }

  public parseModel(result) {
    return super.parseModel(result);
  }

  findMTPForMonth(month, year)
  {
    const whereClause = {}
    whereClause[this.scheme.fields.month.sfdc] = month
    whereClause[this.scheme.fields.year.sfdc] = year
    console.log("validateInput",whereClause);
    return this.fetchAllWhere(whereClause)
    .then(response=> { return this.getEntityFromResponse(response); });
  }

  // isMTPForMonthExists(month, year){
  //   return this.findMTPForMonth(month, year)
  //   .then(existingMTP => {return !!existingMTP;})
  // }

  // shouldUseMTP(){
  //   return SettingsManager.getValueByKey('isMtpModuleEnabled')
  //     .then(isEnabled => {return !!isEnabled})
  //   }

    // getMTPForDate(date){
    //   date = moment(date);
    //   return this.findMTPForMonth(this.model.monthValuesList[date.month()], date.year());
    // }

    // canCreateCallReport()
    // {
    //   return this.getMTPForDate(new Date)
    //   .then((mtp) => { return (mtp && mtp.isApproved())?true:false;
    //   });
    // }

    // canAttachToMTP(mtp){
    //   return this.model.editableStatuses.indexOf(mtp.status) != -1;
    // }

    // canCloneIfMTP(mtp)
    // {
    //  return this.model.allowCloningStatuses.indexOf(mtp.status) != -1; 
    // }

    // createMTPFromDate(date)
    // {
    //   date = moment(date);
    //   return this.createNewMTP(this.model.monthValuesList[date.month()], date.year())
    // }

    // createNewMTP(month, year)
    // {
    //   this._readIsMtpApprovalEnabled()
    //   .then(isMtpApprovalEnabled=>{
    //     return SforceDataContext.activeUser()
    //     .then((activeUser) => {
    //       this.mtpMonthPickListSource.getMonthNumberByName(month)
    //       .then((monthNumber)=>{
    //         const mtp = {};
    //         mtp['attributes'] = {type: this.scheme.table};
    //         mtp[this.scheme.fields.month.sfdc] = month;
    //         mtp[this.scheme.fields.year.sfdc] = year;
    //         mtp[this.scheme.fields.name.sfdc] = "${month} ${year}";
    //         mtp["monthYearKey"] = "#{month}#{year}";
    //         const date = (new Date(year, monthNumber));
    //         mtp[this.scheme.fields.startDate.sfdc] = Utils.formatDateVisit(moment(date).startOf('month'))
    //         mtp[this.scheme.fields.endDate.sfdc] = Utils.formatDateVisit(moment(date).endOf('month'))
    //         mtp[this.scheme.fields.medRep.sfdc] = activeUser.id
    //         mtp[this.scheme.fields.status.sfdc] = isMtpApprovalEnabled==true?MTPScheme.APPROVAL_STATUS_DRAFT:MTPScheme.APPROVAL_STATUS_APPROVED;
    //         this.createEntity(mtp)
    //         .then(this.parseEntity)
    //       });
    //     });
    //   })
    // }
    // getEventsForMTPWhere(mtp, conditions)
    // {
    //   const callReportCollection = new CallReportsCollection();

    //   let query = callReportCollection._fetchAllQuery()
    //   .whereIn(callReportCollection.scheme.fields.mtp.sfdc, [mtp.id, mtp.attributes._soupEntryId]);
    //   if(conditions)
    //   {
    //     query = query.where(conditions);
    //   }
    //   return callReportCollection.fetchWithQuery(query)
    //   .then((response=> { return callReportCollection.getAllEntitiesFromResponse(response); }));
    // }

    didStartUploading(records){
    return this._readIsMtpApprovalEnabled()
    .then((isMtpApprovalEnabled) =>
    {
      if(isMtpApprovalEnabled)
      {
        return records.map(record =>{
          record.status == this.scheme.APPROVAL_STATUS_SUBMITTED_OFFLINE?record.status=this.scheme.APPROVAL_STATUS_PENDING:record.status;
          return record;
        }); 
      }
      else
      {
        return records;
      }
    })
      
    }

  _readIsMtpApprovalEnabled()
  {
    return ConfigurationManager.getConfig('sampleManagementSettings')
    .then((sampleManagementSettings) => {
      return sampleManagementSettings?sampleManagementSettings.isMtpApprovalEnabled:false;
    })
  }

  // _uploadQuery(entity)
  // {
  //   return SforceDataContext.getActiveUser()
  //   .then((activeUser)=> {
  //     let query = `SELECT Id FROM ${this.scheme.sfdcTable}`;
  //     query += ` WHERE ${this.scheme.fields.medRep.sfdc} = '${activeUser.id}'`;
  //     query += ` AND ${this.scheme.fields.month.sfdc } = '${entity.month}'`;
  //     query += ` AND ${this.scheme.fields.year.sfdc} = '${entity.year}'`;
  //     return query;
  //   });
  // }

  // _checkExistMTPOnServer(method, entity, options){
  //   this._uploadQuery(entity)
  //   .then((query) => {
  //     let onSuccess = (response) => {
  //       if(response.totalSize)
  //       {
  //         entity.id = response.records[0][this.scheme.fields.id.sfdc]
  //         entity.attributes = entity.attributes.assign({
  //           __local__: false,
  //           __locally_created__: false,
  //           __locally_updated__: false,
  //           __locally_deleted__: false
  //         });
  //         let entities = entity;
  //         this._removeRelatedAppointments([entities])
  //         .then((entity)=>{ options.success(entity)});
  //       }
  //       else
  //       {
  //         entity.sync(method, entity, options)
  //       }
  //     }
  //     getForceSyncClient().jsClient.query(query)
  //     .then(()=> onSuccess, options.error)
  //   })
  // }

  // _uploadEntity(method, entity, options)
  // {
  //   if(entity.attributes.__locally_created__)
  //   {
  //     this._checkExistMTPOnServer(method, entity, options)
  //   }
  //   else
  //   {
  //     entity.sync(method, entity, options)
  //   }
  // }

  // _fetchAvailableBrickIds() { 
  //   const referenceCollection: ReferenceCollection = new ReferenceCollection();
  //   return referenceCollection
  //     .fetchAll()
  //     .then((records) => {
  //       return referenceCollection.getAllEntitiesFromResponse(records);
  //     })
  //     .then((references) => {
  //       const brickIds = references.map((reference)=> { return reference.organizationBrick});
  //       //return _.uniq brickIds
  //       return brickIds;
  //     });
  // }

  // didPageFinishDownloading(records){
  //   records = this._mapMonthYearKeys(records);
  //   return this._findLocalDuplicates(records)
  //   .then((res)=> this._removeDuplicates(res))
  //   .then((res)=> this._removeRelatedAppointments(res))
  //   .then((res)=> this.super(res))
  //   .then((records)=> { return records})
  // }

  didFinishDownloading(records) {
    return records; //this._removeOutOfDateEntities(records);
  }

  _removeOutOfDateEntities(remoteRecords)
  {
    return this.fetchAll()
    .then((res => { return this.getAllEntitiesFromResponse(res);}))
    .then((localRecords =>  { this._getOutOfDateEntities(remoteRecords,localRecords);}))
    .then((records => { return  this.removeEntities(records)}))
    .then((records => { return remoteRecords;}))
  }

  _getOutOfDateEntities(remoteRecords, localRecords)
  {
    let remoteRecordsIds = remoteRecords.map(record=>{ return record.Id});
    return localRecords.filter((record) =>{ return (remoteRecordsIds.indexOf(record.id)==-1 && !record.attributes.__locally_created__)});
  }

  // _mapMonthYearKeys(records)
  // {
  //   return records.map((record)=>{
  //     record.monthYearKey = "${record[this.scheme.fields.month.sfdc]}${record[this.scheme.fields.year.sfdc]}"
  //     return record;
  //   })
  // }

  // _findLocalDuplicates(records){
  //   if(records.length>0)
  //   {
  //     let recordKeys = records.map((record)=>record.monthYearKey)
  //     const local = "__locally_created__ : true";
  //     const query = new Query()
  //     .selectFrom(this.scheme.table)
  //     .where(local)
  //     .and()
  //     .whereIn('monthYearKey', recordKeys)
  //     return this.fetchWithQuery(query)
  //     .then((response) => { return this.getAllEntitiesFromResponse(response)});
  //   }
  //   else
  //   {
  //     return $.when([]);
  //   }
  // }

  // _removeDuplicates(records)
  // {
  //   return this.removeEntities(records)
  //   .then((records)=> { return records; })
  // }

  // _removeRelatedAppointments(records)
  // {
  //   if(records.length>0)
  //   {
  //     const callReportCollection = new CallReportsCollection
  //     let mtpIds = [];
  //     mtpIds = records.reduce((reducer, record)=>reducer.concat([record.id, record.attributes._soupEntryId]))
  //     return callReportCollection.fetchAllWhereIn(callReportCollection.model.sfdc.mtp, mtpIds)
  //     .then(response=>{ callReportCollection.getAllEntitiesFromResponse(response); }) 
  //     .then(records=> { return callReportCollection.removeEntities(records); })
  //   }
  //   else
  //   {
  //     return $.when([]);
  //   }
  // }

}
