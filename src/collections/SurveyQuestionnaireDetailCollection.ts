import { EntitiesCollection } from './EntitiesCollection';
import { SurveyQuestionnaireDetail } from './../models/SurveyQuestionnaireDetail';
import { SurveyQuestionnaireDetailScheme } from './../models/scheme/SurveyQuestionnaireDetailScheme';
import { Query } from '../services/common/Query';


export class SurveyQuestionnaireDetailCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = SurveyQuestionnaireDetail;
    this.scheme = SurveyQuestionnaireDetailScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  parseModel(result) {
    if (result.Customer__r) {
      result[this.scheme.fields.remoteCustomerName.sfdc] = result.Customer__r.Name;
    }
    return super.parseModel(result);
  }

  didFinishDownloading(records) {
    return records;
  }

  fetchBySurveyId(surveyId)
  {
    const fieldValues = {};
    console.log("surveyId",surveyId);
    fieldValues[this.scheme.fields.surveyid.sfdc] = surveyId;
    return this.fetchAllWhere(fieldValues).then(res=> { return this.getAllEntitiesFromResponse(res);});
  }
}
