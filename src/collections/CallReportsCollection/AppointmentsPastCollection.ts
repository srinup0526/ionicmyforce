import { AppointmentsCollection } from './AppointmentsCollection';
import { Query } from '../../services/common/Query';
import { Utils } from '../../utils/Utils';



export class AppointmentsPastCollection extends AppointmentsCollection {


  constructor()
  {
    super();
  }
  
  _fetchAllQuery()
  {
    //return this.super()
    let today = Utils.originalStartOfToday();
    let todayCondition = {}
    todayCondition["Date_Time_Planned__c"] = today
    return super._fetchAllQuery()
    .where(todayCondition, Query.LR)
  }
    

}
