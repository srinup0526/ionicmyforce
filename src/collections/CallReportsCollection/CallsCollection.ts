import { CallReportsCollection } from './CallReportsCollection';
import { EntitiesCollection } from '../EntitiesCollection';
import { CallReport } from '../../models/CallReport';
import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import { Query } from '../../services/common/Query';
import { ReferenceCollection } from '../references/ReferenceCollection';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import { Utils } from '../../utils/Utils';
import { UsersCollection } from './../UsersCollection';
import { DurationFilter } from './../../DurationFilter/DurationFilter';

declare function getForceSyncClient();



export class CallsCollection extends CallReportsCollection {
  public model;
  public scheme;
  public appointmentRecordTypeId:string;

  constructor()
  {
    super();
  }
  
  _dataType(){
    const fieldValue = {}
    fieldValue["Type__c"] = this.scheme.TYPE_ONE_TO_ONE;
    return fieldValue;
  }

  _buildGetLastCallReportQuery(contactId, organizationId) {
    const keyValue = this._dataType();

    keyValue[this.scheme.fields.contactSfId.sfdc] = contactId;
    keyValue[this.scheme.fields.organizationSfId.sfdc] = organizationId;

    return this._fetchAllQuery()
      .where(keyValue)
      .orderBy([this.scheme.fields.dateTimeVisitStart.sfdc], Query.DESC)
      .limit(1);
  }

  getLastCallReportForContactAndOrganization(contactId, organizationId){
    const query = this._buildGetLastCallReportQuery(contactId, organizationId);

    return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this))
      .then((records) => {
        return records ? records[0] : null;
      });
  }


  // getLastCallReportsForPairsContactIdOrganizationId: (contactOrganizationPairArray) =>
  //   MAX_ENTITIES = 9999999999
  //   query = new Query(@model.sfdc.table)
  //   contactsQuery = contactOrganizationPairArray.map((contactOrganizationPair) =>
  //     query.valueOf(contactOrganizationPair.contactId)
  //   ).join(',')
  //   if contactsQuery
  //     query.customQuery(
  //       "SELECT {CallReport:Contact1__c},{CallReport:Organisation__c},max({CallReport:Date_Time_Actual__c}) FROM {CallReport} " +
  //       "WHERE {CallReport:Type__c} = '1:1' AND {CallReport:Contact1__c} IN (#{contactsQuery}) " +
  //       "GROUP BY {CallReport:Organisation__c}, {CallReport:Contact1__c}"
  //     )
  //     smartSql = @_createSmartSqlQuery query, @model.table, MAX_ENTITIES
  //     Force.smartstoreClient.runSmartQuery smartSql
  //     .then (response) =>
  //       response.records = _.flatten response.currentPageOrderedEntries
  //       records = {}
  //       for i in [0 .. response.records.length - 1] by 3
  //         records[response.records[i]] = {} unless records[response.records[i]]
  //         records[response.records[i]][response.records[i + 1]] =
  //           lastCallDate:  response.records[i + 2]
  //       Force.smartstoreClient.closeCursor true, response
  //       .then => records
  //   else
  //     $.when []

}
