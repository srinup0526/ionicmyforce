import { CallReportsCollection } from './CallReportsCollection';
import { EntitiesCollection } from '../EntitiesCollection';
import { CallReport } from '../../models/CallReport';
import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import { Query } from '../../services/common/Query';
import { ReferenceCollection } from '../references/ReferenceCollection';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import { Utils } from '../../utils/Utils';
import { UsersCollection } from './../UsersCollection';
import { DurationFilter } from './../../DurationFilter/DurationFilter';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { CallsCollection } from '../../collections/CallReportsCollection/CallsCollection';



declare function getForceSyncClient();



export class AppointmentsCollection extends CallReportsCollection {
  public model;
  public scheme;
  public appointmentRecordTypeId:string;
  public callsCollection:CallsCollection;

  constructor()
  {
    super();
    this.scheme = CallReportScheme;
    this.model = CallReport;
    this.callsCollection = new CallsCollection();
  }
  _dataType(){
    const fieldValue = {};
    fieldValue[CallReportScheme.fields.type.sfdc] = CallReportScheme.TYPE_APPOINTMENT;
    return fieldValue;
  }
  getAllAppointmentsFor(reference, date){
    const keyValue = this._dataType();
    keyValue[this.scheme.fields.contactSfId.sfdc] = reference.contactSfId;
    keyValue[this.scheme.fields.organizationSfId.sfdc] = reference.organizationSfId;
    const startDate = Utils.originalStartOfDate(date);
    const endDate = Utils.originalEndOfDate(date);
    const startOfTodayCondition = {};
    startOfTodayCondition[this.scheme.fields.dateTimePlanned.sfdc] = startDate;
    const endOfTodayCondition = {}
    endOfTodayCondition[this.scheme.fields.dateTimePlanned.sfdc] = endDate;
    const query = new Query().selectFrom(this.scheme.table)
    .where(keyValue).and()
    .where(startOfTodayCondition, Query.GRE).and()
    .where(endOfTodayCondition, Query.LRE)
    return this.fetchUnparsedWithQuery(query)
    .then((response)=>{ return this.getAllUnparsedEntitiesFromResponse(response)});
  }

  fetchUnparsedWhere(keyValue){
    const query = new Query().selectFrom(this.scheme.table).where(keyValue);
    return this.fetchUnparsedWithQuery(query);
  }

  fetchClosest()
  {
    return this.fetchWithQuery(this._closestQuery())
  }
  _closestQuery()
  {
    const whereCondition = {}
    whereCondition[CallReportScheme.fields.dateTimePlanned.sfdc] = Utils.originalDateTime(new Date);
    const query = new Query();
    return query.selectFrom(this.scheme.table).where(this._dataType()).where(whereCondition, Query.GR).orderBy([CallReportScheme.fields.dateTimePlanned.sfdc]);
  }

  createAppointmentByReference(reference, dateTimePlanned, mtpId = ''){
    return this._fillAppointmentData(reference, dateTimePlanned, mtpId)
    .then(response=> { return this.createEntity(response); })
  }

  createAppointmentByReferenceFromCalendar(reference, dateTimePlanned, mtpId = ''){
    return this._fillAppointmentData(reference, dateTimePlanned, mtpId);
  }

  _setAppointmentRecordType(appointment)
  {
    return ConfigurationManager.getConfig()
    .then((config) => {
      appointment[this.scheme.fields.recordTypeId.sfdc] = this.appointmentRecordTypeId = config.appointmentRecordTypeId;
      return appointment;
    })
  }

  _setAppointmentUser(appointment)
  {
    return SforceDataContext.getActiveUser()
    .then((activeUser) => {
      appointment[this.scheme.fields.userFirstName.sfdc] = activeUser.firstName;
      appointment[this.scheme.fields.userLastName.sfdc] = activeUser.lastName;
      appointment[this.scheme.fields.userSfid.sfdc] = activeUser.id;
      return appointment;
    })
  }

  _fillAppointmentData(reference, dateTimePlanned, mtpId)
  {
    const appointment = {};
    appointment['attributes'] = {type: this.scheme.table}
    appointment[this.scheme.fields.createdOffline.sfdc] = true;
    appointment[this.scheme.fields.dateTimePlanned.sfdc] = Utils.originalDateTime(dateTimePlanned);
    appointment[this.scheme.fields.organizationSfId.sfdc] = reference.organizationSfId;
    appointment[this.scheme.fields.remoteOrganizationName.sfdc ] = reference.organizationName;
    appointment[this.scheme.fields.organizationName.sfdc] = reference.organizationName;
    appointment[this.scheme.fields.organizationCity.sfdc] = reference.organizationCity;
    appointment[this.scheme.fields.organizationAddress.sfdc] = reference.organizationAddress;
    appointment[this.scheme.fields.contactSfId.sfdc] = reference.contactSfId;
    appointment[this.scheme.fields.remoteContactFirstName.sfdc] = reference.contactFirstName;
    appointment[this.scheme.fields.remoteContactLastName.sfdc] = reference.contactLastName;
    appointment[this.scheme.fields.contactFirstName] = reference.contactFirstName;
    appointment[this.scheme.fields.contactLastName] = reference.contactLastName;
    appointment[this.scheme.fields.contactRecordType.sfdc] = reference.contactRecordType;
    appointment[this.scheme.fields.type.sfdc] = this.scheme.TYPE_APPOINTMENT;
    appointment[this.scheme.fields.mtp.sfdc] = mtpId;
    return this._setAppointmentRecordType(appointment)
    .then(appoinment=> { return this._setAppointmentUser(appoinment)})
    //.then(appointment=> { return this._setDuration(appointment); })
    .then(appointment=> { return this._setCallObjective(appointment); })
  }

  _setDuration(appointment){
    const durationFilter = new DurationFilter();
    return durationFilter.resources()
    .then(resources=>{ 
      console.log("resources",resources);
      appointment[this.scheme.fields.duration.sfdc] = resources.defaultValue.value;
      return appointment;
    })
  }

  _setCallObjective(appointment){
    return this.callsCollection.getLastCallReportForContactAndOrganization(appointment[this.scheme.fields.contactSfId.sfdc], appointment[this.scheme.fields.organizationSfId.sfdc])
    .then(callReport=> {
      appointment[this.scheme.fields.callObjective.sfdc] = callReport?callReport.nextCallObjective:'';
      return appointment;
    })
  }


}
