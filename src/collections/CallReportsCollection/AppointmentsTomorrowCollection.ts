import { AppointmentsCollection } from './AppointmentsCollection';
import { Query } from '../../services/common/Query';
import { Utils } from '../../utils/Utils';



export class AppointmentsTomorrowCollection extends AppointmentsCollection {


  constructor()
  {
    super();
  }
  
  _fetchAllQuery()
  {
    //return this.super()
  	const startOfToday = Utils.originalStartOfTomorrow();
    const endOfToday = Utils.originalEndOfTomorrow()
    const startOfTodayCondition = {}
    startOfTodayCondition["Date_Time_Planned__c"] = startOfToday;
    const endOfTodayCondition = {}
    endOfTodayCondition["Date_Time_Planned__c"] = endOfToday;
    return super._fetchAllQuery()
    .where(startOfTodayCondition, Query.GRE)
    .and()
    .where(endOfTodayCondition, Query.LRE)
  }
    
    

}
