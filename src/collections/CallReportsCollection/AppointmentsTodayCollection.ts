import { AppointmentsCollection } from './AppointmentsCollection';
import { Query } from '../../services/common/Query';
import { Utils } from '../../utils/Utils';



export class AppointmentsTodayCollection extends AppointmentsCollection {

  constructor()
  {
    super();
  }
  
  _fetchAllQuery()
  {
    //return this.super()
  	const startOfToday = Utils.originalStartOfToday();
    const endOfToday = Utils.originalEndOfToday()
    const startOfTodayCondition = {}
    startOfTodayCondition[this.scheme.fields.dateTimePlanned.sfdc] = startOfToday;
    const endOfTodayCondition = {}
    endOfTodayCondition[this.scheme.fields.dateTimePlanned.sfdc] = endOfToday;
    return super._fetchAllQuery()
    .where(startOfTodayCondition, Query.GRE)
    .and()
    .where(endOfTodayCondition, Query.LRE)
  }
    
    

}
