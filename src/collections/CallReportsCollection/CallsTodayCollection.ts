import { CallsCollection } from './CallsCollection';
import { Query } from '../../services/common/Query';
import { Utils } from '../../utils/Utils';



export class CallsTodayCollection extends CallsCollection {

  constructor()
  {
    super();
  }
  
  _fetchAllQuery()
  {
    //return this.super._fetchAllQuery();
  	const startOfToday = Utils.originalStartOfToday();
    const endOfToday = Utils.originalEndOfToday()
    const startOfTodayCondition = {}
    startOfTodayCondition["Date_Time_Planned__c"] = startOfToday;
    const endOfTodayCondition = {}
    endOfTodayCondition["Date_Time_Actual__c"] = endOfToday;
    return super._fetchAllQuery()
    .where(startOfTodayCondition, Query.GRE)
    .and()
    .where(endOfTodayCondition, Query.LRE)
  }
    
    

}
