import { EntitiesCollection } from '../EntitiesCollection';
import { CallReport } from '../../models/CallReport';
import { CallReportScheme } from '../../models/scheme/CallReportScheme';
import { Query } from '../../services/common/Query';
import { ConfigurationManager } from '../../services/db/ConfigurationManager';
import { Utils } from '../../utils/Utils';
import { MTPCollection } from './../MTPCollection';
import { ProductsCollection } from './../ProductsCollection';

import moment from 'moment';
import { SforceDataContext } from './../SforceDataContext';
import {ContactsCollection} from "../ContactsCollection";
declare var $:any;
declare var _: any;




export class CallReportsCollection extends EntitiesCollection {
  public model;
  public scheme;
  public mtpCollection: MTPCollection;
  public productsCollection:ProductsCollection;

  public static readonly ALL_TYPES:string = 'ALL_TYPES';
  
  constructor() {
    super();
    this.model = CallReport;
    this.scheme = CallReportScheme;
    this.init();
    this.productsCollection = new ProductsCollection();
    this.mtpCollection = new MTPCollection();
  }

  prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
      return ConfigurationManager.getConfig('numberOfMonthsForCalls')
      .then ((numberOfMonthsForCalls) => {
        const minVisitDate = Utils.toSalesForceDateTimeFormat(moment().subtract(numberOfMonthsForCalls,'months').date(1));
        return this._mapFieldsList()
        .then((fields) => { return ConfigurationManager.getConfig().then((configs) => { return [fields, configs]}) })
        .then((res) =>
        {
          console.log("res at call report",res);
          let fields = this._fieldsWithExcludeProductFieldsByConfig(res[0], res[1].callReportProductsSettings);
          console.log("fields",fields);
          config.query = `SELECT ${fields.join(',')} FROM ${this.scheme.sfdcTable}`;
          if(res[1].isIndiaUser)
          {
            config.query += ` WHERE (${this.scheme.fields.dateTimeVisitStart.sfdc} >= ${minVisitDate} OR ${this.scheme.fields.dateTimePlanned.sfdc} > ${minVisitDate}) `;
          }
          else{
            config.query += ` WHERE (${this.scheme.fields.dateTimeVisitStart.sfdc} >= ${minVisitDate} OR ${this.scheme.fields.dateTimePlanned.sfdc} > ${minVisitDate}) AND Contact1__r.Id != Null AND Organisation__r.Id != Null`;
          }
          // config.query += ` WHERE (${this.scheme.fields.dateTimeVisitStart.sfdc} >= ${minVisitDate} OR ${this.scheme.fields.dateTimePlanned.sfdc} > ${minVisitDate}) AND Contact1__r.Id != Null AND Organisation__r.Id != Null`;
          return config;
        });
      });
    });
  }

  _fieldsWithExcludeProductFieldsByConfig(fields, config){
    const numberOfProducts = config.numberOfPromotedProducts+1;
    if(numberOfProducts >= this.scheme.MAX_PRODUCTS_NUMBER){ return fields;}
    else
    {
      const fieldsToExclude = []
      for(let i=1;i<=this.model.MAX_PRODUCTS_NUMBER;i++)
      {
        fieldsToExclude.push(`Prio_${i}_Product__c`);
        fieldsToExclude.push(`Note_for_Prio_${i}__c`);
        fieldsToExclude.push(`Prio_${i}_Marketing_Message_1__c`);
        fieldsToExclude.push(`Prio_${i}_Marketing_Message_2__c`);
        fieldsToExclude.push(`Prio_${i}_Marketing_Message_3__c`);
        fieldsToExclude.push(`Prio_${i}_Marketing_Message_4__c`);
        fieldsToExclude.push(`Prio_${i}_Marketing_Message_5__c`);
      }
      return _.difference(fields,fieldsToExclude)
    }
    
    }

  getDataByDateTime(contactId, startDate, endDate)
  {
    const startOfToday = Utils.originalStartOfDate(startDate);
    const endOfToday = Utils.originalEndOfDate(endDate);

    const whereContact = {};
    whereContact[this.scheme.fields.contactSfId.sfdc] = contactId;

    const startOfTodayCondition = {};
    startOfTodayCondition[this.scheme.fields.dateTimeVisitStart.sfdc] = startOfToday;
    const endOfTodayCondition = {};
    endOfTodayCondition[this.scheme.fields.dateTimeVisitStart.sfdc] = endOfToday;

    const startOfTodayAppointmentCondition = {};
    startOfTodayAppointmentCondition[this.scheme.fields.dateTimePlanned.sfdc] = startOfToday;
    const endOfTodayAppointmentCondition = {};
    endOfTodayAppointmentCondition[this.scheme.fields.dateTimePlanned.sfdc] = endOfToday;


    const query = this._fetchAllQuery()
      .where(whereContact)
      .and()
      .bracketStart()
      .where(startOfTodayCondition, Query.GRE)
      .and()
      .where(endOfTodayCondition, Query.LRE)
      .bracketEnd()
      .or()
      .bracketStart()
      .where(startOfTodayAppointmentCondition, Query.GRE)
      .and()
      .where(endOfTodayAppointmentCondition, Query.LRE)
      .bracketEnd()


    this.fetchWithQuery(query)
      .then(response=> { return this.getAllEntitiesFromResponse(response); })
      .then((records) => {
        const searchedAppointments = this.getAppointmentByDateRange(records, startDate, endDate);
        const searchedCallReport = this.getCallReportByDateRange(records, startDate, endDate);

        return searchedAppointments.concat(searchedCallReport);
    })
  }

  _getTimesWithDuration(date, duration){

    return Utils.originalDateTime(Utils.dateWithAddingOffsetMin(date, parseInt(duration)));
  }

  getAppointmentByDateRange(records, startDate, endDate){
    return records
      .filter((call) => { return call.type == this.scheme.TYPE_APPOINTMENT})
      .filter((appointment) => {
        const appointmentStartDate = appointment.dateTimePlanned;
        const appointmentEndDate = this._getTimesWithDuration(appointment.dateTimePlanned, appointment.duration);

        return this._getDateRangeOnDateRange(appointmentStartDate, appointmentEndDate, startDate, endDate);
      });
  }

  getCallReportByDateRange(records, startDate, endDate){
    return records
      .filter((call) => { return call.type == this.scheme.TYPE_ONE_TO_ONE})
      .filter((callReport) => {
        const callReportStartDate = callReport.dateTimeVisitStart;
        const callReportEndDate = callReport.dateTimeVisitEnd;

        return this._getDateRangeOnDateRange(callReportStartDate, callReportEndDate, startDate, endDate);
      });
  }

  _resolveEditabilityStatus(entity, editabilityPeriodSettingDays){
    if (!entity.isAvailableToEdit()) {
      return $.when(entity);
    }
    let currentDate:any = new Date;
    let visitStartDate:any = Utils.getDateByStr(entity.dateTimeVisitStart);
    if (currentDate < visitStartDate) {
      return $.when(entity);
    }
    const millisecondsInDay = 1000 * 60 * 60 * 24;
    let existDays = Math.floor((currentDate - visitStartDate) / millisecondsInDay);
    if (existDays > editabilityPeriodSettingDays) {
      entity.changeStatusToSubmitted();
      return this.updateEntity(entity).then(()=>{
        return entity;
      });
    } else {
      return $.when(entity);
    }
  }

  _getEditabilityPeriodSettingDays()
  {
    return ConfigurationManager.getConfig()
    .then ((config) => { return config.callReportValidationSettings.isCallReportEditEnabled});
  }

  checkCallsExpirationDate(entities)
  {
    return this._getEditabilityPeriodSettingDays()
    .then(editabilityPeriodSettingDays => {
      return Utils.runSequentially(entities.map(entity => {
        return this._resolveEditabilityStatus(entity, editabilityPeriodSettingDays)
      }))
      .then((entities) => { return  entities;})
    });
  }

  checkCallExpirationDate(entity)
  {
    return this._getEditabilityPeriodSettingDays()
    .then((editabilityPeriodSettingDays) => {
      return this._resolveEditabilityStatus(entity, editabilityPeriodSettingDays)
    });
  }
  

  _getDateRangeOnDateRange(start1, end1, start2, end2){
    const rangeResult1 = Utils.isBetweenDate(start1, start2, end2);
    const rangeResult2 = Utils.isBetweenDate(end1, start2, end2);
    const rangeResult3 = Utils.isBetweenDate(start2, start1, end1);
    const rangeResult4 = Utils.isBetweenDate(end2, start1, end1);

    return rangeResult1 || rangeResult2 || rangeResult3 || rangeResult4;
  }

  didFinishDownloading(records) {
    console.log("records at didFinishDownloading",records);
     return this._updateCallReports(records);
    //return records;
  }
  
  saveRecordToCache(cache, records) {
    return this.updateCalls(records)
      .then((updatedRecords) => {
        return super.saveRecordToCache(cache, updatedRecords);
      });
  }
  
  updateCalls(records) {
    const callsByContactIds = this.callsByContactIds(records);

    let contactsCollection = new ContactsCollection();
    
    return contactsCollection.fetchForContactIds(Object.keys(callsByContactIds))
      .then((contacts) => {
        return this.updateCallsByContactsIds(callsByContactIds, contacts);
      });
  }
  
  
  private callsByContactIds(calls) {
    let callsByContactIds = {};
  
    calls.forEach((call) => {
      const contactId = call[this.scheme.fields.contactSfId.sfdc];
      
      if(!callsByContactIds[contactId]) {
        callsByContactIds[contactId] = [] ;
      }
  
      callsByContactIds[contactId].push(call);
    });
    
    return callsByContactIds;
  }
  
  private updateCallsByContactsIds(callsByContactIds, contacts) {
    let updatedCalls = [];
  
    contacts.forEach((contact) => {
      let calls = callsByContactIds[contact.id];
    
      if(calls && calls.length){
        calls.forEach((call)=> {
          let updatedCall = this.updateBuSpecialty(call, contact);
          updatedCall = this.updatePriority(updatedCall, contact);
          
          updatedCalls.push(updatedCall);
        });
      }
    
      delete callsByContactIds[contact.id]
    });
  
    let leftRefsIds = Object.keys(callsByContactIds);
  
    updatedCalls = leftRefsIds.reduce(((container, refId) => container.concat(callsByContactIds[refId])), updatedCalls);
  
    return updatedCalls;
  }
  
  private updateBuSpecialty(call, contact){
    if(contact && contact.buSpecialty) {
      call.contactSpecialty = contact.buSpecialty;
    }
  
    return call;
  }
  
  private updatePriority(call, contact) {
    if(contact && contact.priority) {
      call.contactPriority = contact.priority;
    }
    
    return call;
  }
  
  _dataType()
  {
    return this.ALL_TYPES;
  }

  _fetchAllQuery() {
    const dataType = this._dataType();
    return new Query()
      .selectFrom(this.scheme.table);
  }

  parseModel(result){
    console.log("result before",result);
    if(result.User__r)
    {
      result[this.scheme.fields.userFirstName.local] = result.User__r.FirstName
      result[this.scheme.fields.userLastName.local] = result.User__r.LastName
    }

    if(result.Contact1__r)
    {
      result[this.scheme.fields.contactRecordType.local] = result.Contact1__r.Account?result.Contact1__r.Account.RecordType.Name:'';
      result[this.scheme.fields.contactFirstName.local] = result.Contact1__r.FirstName;
      result[this.scheme.fields.contactLastName.local] = result.Contact1__r.LastName;
    }
    if(result.Organisation__r)
    {
      result[this.scheme.fields.organizationName.local] = result.Organisation__r.Name;
      result[this.scheme.fields.organizationCity.local] = result.Organisation__r.BillingCity;
      result[this.scheme.fields.organizationAddress.local] = result.Organisation__r.BillingStreet;
    }
    if(result.Customer__r)
    {
      result[this.scheme.fields.contactFirstName.local] = result.Customer__r.Name;
      result[this.scheme.fields.indCustomerBU.local] = result.Customer__r.BU__c;
    }
    console.log("result after",result);
    return super.parseModel(result);
  }

  didPageFinishDownloading(records){
    console.log("records",records);
    return this._updateCallReports(records);
  }

  _updateCallReports(callReports){
    const updatedCallReports = callReports.map((callReport)=> {
      if(callReport.Contact1__r)
      {
        callReport.contactFirstName = callReport.Contact1__r.FirstName
        callReport.contactLastName = callReport.Contact1__r.LastName
      }
      else if(callReport.Customer__r)
      {
        callReport.contactFirstName = callReport.Customer__r.Name;
      }
      else
      {
        callReport.contactFirstName = ''
        callReport.contactLastName = ''
      }

      if(callReport.Organisation__r)
      {
        callReport.organizationName = callReport.Organisation__r.Name
      }

      if(callReport[this.scheme.fields.type.sfdc]==this.scheme.TYPE_ONE_TO_ONE)
      {
        if(!callReport[this.scheme.fields.dateTimeVisitStart.sfdc])
        {
          callReport[this.scheme.fields.dateTimeVisitStart.sfdc] = callReport[this.scheme.fields.dateTimePlanned.sfdc];
        }

        if(!callReport[this.scheme.fields.dateTimeVisitEnd.sfdc])
        {
          callReport[this.scheme.fields.dateTimeVisitEnd.sfdc] = callReport[this.scheme.fields.dateTimePlanned.sfdc]
        }
      }
      console.log("callReport",callReport);
      return callReport;
    });
    return this.cache.saveAll(updatedCallReports);
  }


  didStartUploading(records)
  {
    let brokenCalls = [];
    let callsToUpload = [];
    return this._removeEmptyProducts(records)
    .then(records=>{
      
      let splitBrokenCalls = _.groupBy(records, (record) => !!record.isSandbox);
      
      brokenCalls = splitBrokenCalls.true ? splitBrokenCalls.true : [];
      callsToUpload = splitBrokenCalls.false ? splitBrokenCalls.false : [];
      
      return this.mtpCollection.linkEntitiesToEntity(callsToUpload, 'mtp');
    })
  }

  // _cleanBrokenCallReportLocations: (brokenCalls)=>
  //   LocationCollection = require 'models/bll/location-collection'
  //   locationCollection = new LocationCollection()
  //   brokenCallsIds = brokenCalls.map (callReport) => callReport.attributes._soupEntryId
  //   locationCollection.removeRelatedToCallReport brokenCallsIds

  _removeEmptyProducts(records)
  {
    let callReportsWithEmptyProducts = []
    let callReportsWithEmptyProductsForUpdate = []
    let callReportsWithEmptyProductsForDelete = []
    
    return this.productsCollection.fetchAll()
    .then(res => { return this.productsCollection.getAllEntitiesFromResponse(res);})
    .then((products) =>
    {
      let productsMapped = products.map(product=> {return product.id;})
      
      records.forEach(record=>
      {
        Array(10).fill(1).map((x,i)=>{
          if(productsMapped.indexOf(record[`prio${i}ProductSfid`])==-1 && record[`prio${i}ProductSfid`])
          {
            if(callReportsWithEmptyProducts.indexOf(record)==-1)
            {
              callReportsWithEmptyProducts.push(record)
            }
          }
        })
      })
      
      callReportsWithEmptyProducts.map(record=> {
        let emptyProdCount = Array(10).fill(1).filter((x,i)=>{
          return productsMapped.indexOf(record[`prio${i}ProductSfid`]) == -1 && record[`prio${i}ProductSfid`];
        });
  
        let totalProdCount = Array(10).fill(1).filter((x,i)=>{
          return record[`prio${i}ProductSfid`];
        });
        
        if(emptyProdCount.length == totalProdCount.length) {
          callReportsWithEmptyProductsForDelete.push(record);
        }
        else {
          let updateIndexes = Array(10).fill(1).filter((x,i)=>{
            return (productsMapped.indexOf(record[`prio${i}ProductSfid`]) != -1 && record[`prio${i}ProductSfid`]);
          });
          
          let productsMessages = [];
          
          updateIndexes.forEach(updateIndex=>{
            productsMessages.push({
              'prioProductSfid': record['prio'+updateIndex+'ProductSfid'],
              'noteForPrio': record['noteForPrio'+updateIndex],
              'prioMarketingMessage1':  record['prio'+updateIndex+'MarketingMessage1'],
              'prioMarketingMessage2':  record['prio'+updateIndex+'MarketingMessage2'],
              'prioMarketingMessage3':  record['prio'+updateIndex+'MarketingMessage3'],
              'prioMarketingMessage4': record['prio'+updateIndex+'MarketingMessage4'],
              'prioMarketingMessage5':  record['prio'+updateIndex+'MarketingMessage5'],
              'patientProfile': record['patientProfile'+updateIndex],
              'prioClassification':  record['prio'+updateIndex+'Classification']
            });

            Array(10).fill(1).forEach((updateIndex, index) =>{
              if(productsMessages[index]) {
                record["prio"+updateIndex+"ProductSfid"] = productsMessages[index]["prioProductSfid"]
                record["noteForPrio"+updateIndex] = productsMessages[index]["noteForPrio"]
                record["prio"+updateIndex+"MarketingMessage1"] = productsMessages[index]["prioMarketingMessage1"]
                record["prio"+updateIndex+"MarketingMessage2"] = productsMessages[index]["prioMarketingMessage2"]
                record["prio"+updateIndex+"MarketingMessage3"] = productsMessages[index]["prioMarketingMessage3"]
                record["prio"+updateIndex+"MarketingMessage4"] = productsMessages[index]["prioMarketingMessage4"]
                record["prio"+updateIndex+"MarketingMessage5"] = productsMessages[index]["prioMarketingMessage5"]
                record["patientProfile"+updateIndex] = productsMessages[index]["patientProfile"]
                record["prio"+updateIndex+"Classification"] = productsMessages[index]["prioClassification"]
              } else {
                record["prio"+updateIndex+"ProductSfid"] = undefined
                record["noteForPrio"+updateIndex] = undefined
                record["prio"+updateIndex+"MarketingMessage1"] = undefined
                record["prio"+updateIndex+"MarketingMessage2"] = undefined
                record["prio"+updateIndex+"MarketingMessage3"] = undefined
                record["prio"+updateIndex+"MarketingMessage4"] = undefined
                record["prio"+updateIndex+"MarketingMessage5"] = undefined
                record["patientProfile"+updateIndex] = undefined
                record["prio"+updateIndex+"Classification"] = undefined
              }
            })
            callReportsWithEmptyProductsForUpdate.push(record);
          })
        }
      })
      
      return Utils.runSequentially(callReportsWithEmptyProductsForUpdate.map((record) =>{
        return this.updateEntity(record);
      }))
    })
    .then(()=>{
      return this.removeEntities(callReportsWithEmptyProducts);
    })
    .then(()=>{
      let shouldIgnoreDeleted = false;
      
      return this.fetchWithQuery(new Query().selectFrom(this.scheme.table).where({"__local__":true}),shouldIgnoreDeleted)
      .then((res)=>{ return this.getAllEntitiesFromResponse(res);})
    })
  }

  // _cleanBrokenAdjustments: (collection, callReports) =>
  //   @_cleanCollectionByCallsWithQueryFields collection, callReports, collection.model.sfdc.callReportSfId

  // _cleanBrokenCallReportData: (callReports) =>
  //   callReportDataCollection = @_createCallReportDataCollection()
  //   @_cleanCollectionByCallsWithQueryFields callReportDataCollection, callReports, callReportDataCollection.model.sfdc.callReportId

  // _cleanCollectionByCallsWithQueryFields: (collection, callReports, callReportIdField) =>
  //   @_runSimultaneouslyForCollectionByEachCallWithQueryField collection, callReports, callReportIdField, (entities, _) -> collection.removeEntities entities

  // _updateAdjustments: (collection, callReports) =>
  //    @_updateCollectionByCallsWithQueryFields collection, callReports, collection.model.sfdc.callReportSfId

  // _updateCallReportsData: (callReports) =>
  //   callReportDataCollection = @_createCallReportDataCollection()
  //   @_updateCollectionByCallsWithQueryFields callReportDataCollection, callReports, callReportDataCollection.model.sfdc.callReportId

  // _updateCollectionByCallsWithQueryFields: (collection, callReports, callReportIdField) =>
  //   @_runSimultaneouslyForCollectionByEachCallWithQueryField collection, callReports, callReportIdField, (entities, callReport) =>
  //     entities = entities.map (entity) =>
  //       entity.attributes[callReportIdField] = callReport[@model.sfdc.id]
  //       entity
  //     collection.upsertEntitiesSilently entities

  // _applyForTradeModuleAndEdetailingIfAnyEnabled: (records, applyForEdetailing, applyForTradeModule) =>
  //   SettingsManager.getValueByKey('isTradeModuleEnabled')
  //   .then (isTradeModuleEnabled) =>
  //     unless isTradeModuleEnabled then $.when(records)
  //     else applyForTradeModule records
  //   .then =>
  //     SettingsManager.getValueByKey('isEdetailingEnabled')
  //     .then (isEdetailingEnabled) => applyForEdetailing records if isEdetailingEnabled

  // _runSimultaneouslyForCollectionByEachCallWithQueryField: (collection, callReports, callReportIdField, forEach) =>
  //   Utils.runSimultaneously _(callReports).map (callReport) =>
  //     return unless callReport
  //     queryFields = {}
  //     queryFields[callReportIdField] = @_attributesFromEntity(callReport)._soupEntryId
  //     collection.fetchAllWhere(queryFields)
  //     .then collection.getAllEntitiesFromResponse
  //     .then (entities) -> forEach entities, callReport

  // _createPhotoAdjustmentsCollection: =>
  //   PhotoAdjustmentsCollection = require 'models/bll/photo-adjustments-collection'
  //   new PhotoAdjustmentsCollection

  // _createTaskAdjustmentsCollection: =>
  //   TaskAdjustmentsCollection = require 'models/bll/task-adjustments-collection'
  //   new TaskAdjustmentsCollection

  // _createMechanicAdjustmentsCollection: =>
  //   MechanicAdjustmentsCollection = require 'models/bll/mechanic-adjustments-collection'
  //   new MechanicAdjustmentsCollection

  // _createCallReportDataCollection: =>
  //   CallReportDataCollection = require 'models/bll/clm-call-report-data-collection'
  //   new CallReportDataCollection


}
