import { EntitiesCollection } from './EntitiesCollection';
import { ProfileProductInPortfolio } from './../models/ProductInPortfolio';
import { ProductInPortfolioScheme } from './../models/scheme/ProductInPortfolioScheme';



export class ProductInPortfoliosCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = ProfileProductInPortfolio;
    this.scheme = ProductInPortfolioScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
  
    // configPromise.then(config) =>{
    //   SforceDataContext.activeUser()
    //   .then (user) -> new TargetsCollection().getTargetByUser user
    //   .then (target) =>
    //     config.query += " " +
    //       "WHERE #{@model.sfdc.portfolioSfId} IN " +
    //       "(SELECT ProductPortfolio__c " +
    //       "FROM ProductPortfolioAssignment__c " +
    //       "WHERE Target__c = '#{target.id}') "
    //     config
    // }
  }

  didFinishDownloading(records) {
    return records;
  }
 


}
