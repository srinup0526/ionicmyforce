import { EntitiesCollection } from './EntitiesCollection';
import { PEAttendee } from './../models/PEAttendee';
import { PEAttendeeScheme } from './../models/scheme/PEAttendeeScheme';
import { Query } from '../services/common/Query';
import { PharmaEventsCollection } from './PharmaEventsCollection'


export class PEAttendeesCollection extends EntitiesCollection {
  public model;
  public scheme;
  public peEventCollection:PharmaEventsCollection;
 
  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = PEAttendee;
    this.scheme = PEAttendeeScheme;
    this.peEventCollection = new PharmaEventsCollection();
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }

  _fetchAllQuery(){
const  query = new Query().selectFrom(this.scheme.table)
return query;
  // if (this.model.pharmaEventSfId){
  //   const  whereFields = {}
  //   whereFields[this.scheme.fields.pharmaEventSfId.sfdc] = this.model.pharmaEventSfId;
  //   query.where(whereFields)
  // }
   
  }

 fetchAllWhere(fieldsValues, ignoreDeleted=true){
 const query = this._fetchAllQuery().where(fieldsValues)
 return this.fetchWithQuery(query, ignoreDeleted)
 }
 didStartUploading(records){
  return this.peEventCollection.linkEntitiesToEntity(records, 'pharmaEventSfId');
 }
 fetchEntityByPEId(pharmaEventSfId){
   const query = new Query().selectFrom(this.scheme.table)
  const  whereFields = {}
    whereFields[this.scheme.pharmaEventSfId.sfdc] = this.pharmaEventSfId 
    whereFields[this.scheme.attHadLunch.sfdc] = 'YES'

    query.where(whereFields)
    this.fetchWithQuery(query)
    .then(response=>{this.getAllEntitiesFromResponse});
 }

 fetchByPEId(PeId: string, soupPeId: string) {
   const query = this._fetchAllQuery();

   query.whereIn(this.scheme.fields.pharmaEventSfId.sfdc,[PeId, soupPeId]);

   return this.fetchWithQuery(query)
   .then(this.getAllEntitiesFromResponse.bind(this));
 }

  fetchEntityWithoutLunchByPEId(pharmaEventSfId){
    const idFieldValue = {};
    const orIdFieldValue: any = {};

    idFieldValue[this.scheme.fields.pharmaEventSfId.sfdc] = pharmaEventSfId;
    console.log("idFieldValue",idFieldValue);
    console.log("orIdFieldValue",orIdFieldValue);
    const query = new Query()
      .selectFrom(this.scheme.table)
      .where(idFieldValue)

    return this.fetchWithQuery(query)
      .then((response) => {
        console.log("response",response);
        return this.getAllEntitiesFromResponse(response);
      });
  }
  fetchEntityByPEventId(peId){
    const query = this._fetchAllQuery();
    const  whereFields = {}
    whereFields[this.scheme.fields.pharmaEventSfId.sfdc] = peId;
    query.where(whereFields);
    console.log("query",query);
    return this.fetchWithQuery(query).then(response=>{ return this.getAllEntitiesFromResponse(response);});
  }
 
  fetchEntityByAttendeeId(AttendeeId){
    const query = new Query().selectFrom(this.scheme.table)
    const whereFields = {}
    whereFields[this.scheme.fields.id.sfdc] = this.AttendeeId;
    query.where(whereFields)
    return this.fetchWithQuery(query)
    .then(response=>{ return this.getAllEntitiesFromResponse(response)});
  }
}
