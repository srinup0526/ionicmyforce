import { EntitiesCollection } from './EntitiesCollection';
import { MarketingMessage } from './../models/MarketingMessage';
import { MarketingMessageScheme } from './../models/scheme/MarketingMessageScheme';
import { Query } from '../services/common/Query';
import {ConfigurationManager} from './../services/db/ConfigurationManager';
import { SforceDataContext } from './SforceDataContext'


export class MarketingMessagesCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = MarketingMessage;
    this.scheme = MarketingMessageScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise.then(config=>{
      return SforceDataContext.getActiveUser()
      .then(activeUser=>{
        config.query +=` WHERE ${this.scheme.fields.status.sfdc} = true and ${this.scheme.fields.currencyIsoCode.sfdc} = '${activeUser.currency}'`;
        return config;
      })
    });
  }

  didFinishDownloading(records) {
    return records;
  }

  fetchEntityByProductId(produtSfId){
    const query = new Query().selectFrom(this.scheme.table)
   const  whereFields = {}
     whereFields[this.scheme.fields.produtSfId.sfdc] = produtSfId
     
     query.where(whereFields)
     return this.fetchWithQuery(query)
     .then(response=>{return this.getAllEntitiesFromResponse(response)});
  }
}
