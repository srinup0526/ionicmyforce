import { Query } from './../../services/common/Query';
import { MedicalQueriesCollection } from './../MedicalQueriesCollection/MedicalQueriesCollection';
export class QueriesSubmittedToMedical extends MedicalQueriesCollection
{
	_fetchAllQuery()
	{
		const submittedToMedicalQueries = {}
		submittedToMedicalQueries[this.scheme.fields.status.sfdc] = this.scheme.APPROVAL_STATUS_SUBMITTED_TO_MEDICAL;
		return super._fetchAllQuery()
		.where(submittedToMedicalQueries)
	}
}