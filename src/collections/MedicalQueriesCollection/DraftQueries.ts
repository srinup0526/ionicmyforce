import { Query } from './../../services/common/Query';
import { MedicalQueriesCollection } from './../MedicalQueriesCollection/MedicalQueriesCollection';
export class DraftQueries extends MedicalQueriesCollection
{
	_fetchAllQuery()
	{
		const draftQueries = {}
		draftQueries[this.scheme.fields.status.sfdc] = this.scheme.APPROVAL_STATUS_DRAFT;
		return super._fetchAllQuery()
		.where(draftQueries)
	}
}