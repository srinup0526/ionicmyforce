import { Query } from './../../services/common/Query';
import { MedicalQueriesCollection } from './../MedicalQueriesCollection/MedicalQueriesCollection';
export class QueriesSubmittedToRep extends MedicalQueriesCollection
{
	_fetchAllQuery()
	{
		const submittedToRepQueries = {}
		submittedToRepQueries[this.scheme.fields.status.sfdc] = this.scheme.APPROVAL_STATUS_SUBMITTED_TO_REP;
		return super._fetchAllQuery()
		.where(submittedToRepQueries)
	}
}