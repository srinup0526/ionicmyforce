import { Query } from './../../services/common/Query';
import { MedicalQueriesCollection } from './../MedicalQueriesCollection/MedicalQueriesCollection';
export class ClosedQueries extends MedicalQueriesCollection
{
	_fetchAllQuery()
	{
		const closedQueries = {}
		closedQueries[this.scheme.fields.status.sfdc] = this.scheme.APPROVAL_STATUS_CLOSED;
		return super._fetchAllQuery()
		.where(closedQueries)
	}
}