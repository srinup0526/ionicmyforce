import { EntitiesCollection } from './../EntitiesCollection';
import { MedicalQuery } from './../../models/MedicalQuery';
import { MedicalQueryScheme } from './../../models/scheme/MedicalQueryScheme';
import { Query } from './../../services/common/Query';


export class MedicalQueriesCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = MedicalQuery;
    this.scheme = MedicalQueryScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }
  public parseModel(result){
    console.log("before result",result);
    if(result.Customer__r)
    {
      result[this.scheme.fields.remoteCustomerName.sfdc] = result.Customer__r.Name;
      result[this.scheme.fields.remoteEmail.sfdc] = result.Customer__r.Email;
      result[this.scheme.fields.remoteMobile.sfdc] = result.Customer__r.Phone;
      result[this.scheme.fields.customerName.local] = result.Customer__r.Name;
      result[this.scheme.fields.email.local] = result.Customer__r.Email;
    }
    if(result.Organization__r)
    {
      result[this.scheme.fields.remoteOrganizationName.sfdc] = result.Organization__r.Name;
    }
    if(result.Mylan_Medical_Contact__r)
    {
      result[this.scheme.fields.remoteMedicalContactName.sfdc] = result.Mylan_Medical_Contact__r.Name;
      result[this.scheme.fields.medicalContactName.local] = result.Mylan_Medical_Contact__r.Name;
    }
    if(result.Pharma_Product__r)
    {
      result[this.scheme.fields.remotePharmaProductName.sfdc] = result.Pharma_Product__r.Name;
      result[this.scheme.fields.pharmaProductName.local] = result.Pharma_Product__r.Name;
    }
    if(result.User__r)
    {
      result[this.scheme.fields.remoteUserName.sfdc] = result.User__r.Name;
    }
    console.log(" after result",result);
    return super.parseModel(result);
  }

  didStartUploading(records)
  {
    return records.map(record => {
      record.status = (record.status==this.scheme.APPROVAL_STATUS_SUBMITTED_OFFLINE)?this.scheme.APPROVAL_STATUS_SUBMITTED_TO_MEDICAL:record.status;
      return record;
    })
  }

  didPageFinishDownloading(records)
  {
    return this._updateMedicalQueries(records);
  }

  _updateMedicalQueries(records)
  {
    this.cache.saveAll(records.map((record)=> {
      if(record.Customer__r)
      {
        record.customerName = record.Customer__r.Name
        record.email = record.Customer__r.Email
        record.mobile = record.Customer__r.Phone
      }
      if(record.Organization__r)
      {
        record.remoteOrganizationName = record.Organization__r.Name
      }
      if(record.Mylan_Medical_Contact__r)
      {
        record.medicalContactName = record.Mylan_Medical_Contact__r.Name
      }
      if(record.Pharma_Product__r)
      {
        record.pharmaProductName = record.Pharma_Product__r.Name
      }
      if(record.User__r)
      {
        record.userName = record.User__r.Name
      }
      return record;
    }))
  }
    

  didFinishDownloading(records) {
    return this._updateMedicalQueries(records);
  }
}
