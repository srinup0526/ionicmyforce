import { EntitiesCollection } from './EntitiesCollection';
import { PharmaEvent } from './../models/PharmaEvent';
import { PharmaEventScheme } from './../models/scheme/PharmaEventScheme';
import { SforceDataContext } from './SforceDataContext';
import { ConfigurationManager } from './../services/db/ConfigurationManager';
import { Utils } from './../utils/Utils';
import moment from 'moment';

export class PharmaEventsCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = PharmaEvent;
    this.scheme = PharmaEventScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise.then((config) =>{
      return ConfigurationManager.getConfig('numberOfMonthsForCalls')
        .then((numberOfMonthsForCalls) => {
          const minTotDate = Utils.originalDate(moment().subtract(numberOfMonthsForCalls,'months').date(1));
          console.log("minTotDate",minTotDate);
        //config.query += ` WHERE ${this.scheme.fields.endDate.sfdc} >= ${minTotDate}`;
        console.log("config.query",config.query);
          return config;
        })
      })

    //return configPromise;
  }

  public parseModel(result) {
    return super.parseModel(result);
  }

  didFinishDownloading(records) {
    return records;
  }

//   _updateOwner(pharmaEvents){
//     const updatedPharmaEvents = pharmaEvents.map(pharmaEvent=>{
//         if (this.scheme.fields.pharmaEvent.Owner)
//         (this.scheme.fields.pharmaEvent.ownerFirstName = this.scheme.fields.pharmaEvent.Owner.FirstName)
//         (this.scheme.fields.pharmaEvent.ownerLastName = this.scheme.fields.pharmaEvent.Owner.LastName);
    
//       else
//       this.scheme.fields.pharmaEvent.ownerFirstName = ''
//       this.scheme.fields.pharmaEvent.ownerLastName = ''
//       this.scheme.fields.pharmaEvent
//     this.cache.saveAll(response=>{this.updatedPharmaEvents});
// });
    
//   }
//   didStartUploading(records){
//  // this._removeEmptyProducts(records)
//   records.map((record)=>{
//     (record.status == this.scheme.APPROVAL_STATUS_SUBMITTED_OFFLINE)?record.status = this.scheme.fields.APPROVAL_STATUS_SUBMITTED:record.status;
//     return record;
//   });
//   }
 
  //_removeEmptyProducts(){

//     const ProductsCollection = require 'models/bll/products-collection'
//    const  productsCollection = new ProductsCollection
//   const   peWithEmptyProducts = []
//   const peWithEmptyProductsForDelete = []
//   const peWithEmptyProductsForUpdate = []
//     productsCollection.fetchAll()
//     .then(response=>{productsCollection.getAllEntitiesFromResponse})
//     .then((products)=>{
//         $ TODO REFACTOR!!!
//         productsMapped = products.map (product) {product.id}
//         records.forEach (record) =>
//           [1..4].forEach (index) =>
//     })
      
//           if (productsMapped.indexOf(record["productPrio#{index}SfId"]) is -1) and (record["productPrio#{index}SfId"])
//             peWithEmptyProducts.push record if peWithEmptyProducts.indexOf(record) is -1
//       peWithEmptyProducts.forEach (record) =>
//         emptyProdCount = [1..4].filter (index) =>
//           (productsMapped.indexOf(record["productPrio#{index}SfId"]) is -1) and (record["productPrio#{index}SfId"])
//         totalProdCount = [1..4].filter (index) => record["productPrio#{index}SfId"]
//         if emptyProdCount.length is totalProdCount.length
//           peWithEmptyProductsForDelete.push record
//         else
//           updateIndexes = [1..4].filter (index) =>
//             (productsMapped.indexOf(record["productPrio#{index}SfId"]) isnt -1) and (record["productPrio#{index}SfId"])
//           productsMessages = []
//           updateIndexes.forEach (index) =>
//             productsMessages.push {
//               "prioProductSfid": record["productPrio#{index}SfId"]
//             }
//           [1..4].forEach (updateIndex, index) =>
//             if productsMessages[index]
//               record["productPrio#{updateIndex}SfId"] = productsMessages[index]["prioProductSfid"]
//             else
//               record["productPrio#{updateIndex}SfId"] = undefined
//           peWithEmptyProductsForUpdate.push record
//       Utils.runSimultaneously peWithEmptyProductsForUpdate.map (record) =>
//         @updateEntity record
//     .then =>
//       @removeEntities peWithEmptyProductsForDelete
//     .then =>
//       shouldIgnoreDeleted = false
//       @fetchWithQuery(new Query().selectFrom(@model.table).where(__local__: true), shouldIgnoreDeleted)
//       .then(@getAllEntitiesFromResponse)
 // }    
  }
  

