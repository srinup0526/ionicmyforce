import { EntitiesCollection } from './EntitiesCollection';
import { ProductListingHistory } from './../models/ProductListingHistory';
import { ProductListingHistoryScheme } from './../models/scheme/ProductListingHistoryScheme';
//import { Query } from '../services/common/Query';


export class ProductListingHistoriesCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = ProductListingHistory;
    this.scheme = ProductListingHistoryScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    configPromise.then((config)=>{
      config.query += " WHERE ${@model.sfdc.isActive} = true";
   
      return config;
    });
  }

  didFinishDownloading(records) {
    return records;
  }

  parseModel(result){
//   if result[this.scheme.fields.phProductName.sfdc]
//     result[this.scheme.fields.phProductName.sfdc] = result.PharmaProduct__r?.Name

//   super result

  }

  fetchStatusHistoryById(parentId){
//   if (parentId)
//     const fieldsValues = {};
//     fieldsValues[this.scheme.fields.parentId.sfdc] = parentId;
//   const  sortFields = [this.scheme.fields.createdDate.sfdc]
//     return this.fetchAllWhereAndSortBy(fieldsValues, sortFields)
//     .then(response=>{this.getAllEntitiesFromResponse});
//   else
//     return $.when([]);
}
}
