import { EntitiesCollection } from './EntitiesCollection';
import { Survey } from './../models/Survey';
import { SurveyScheme } from './../models/scheme/SurveyScheme';
import { Query } from '../services/common/Query';
import { SforceDataContext } from './SforceDataContext';



export class SurveyCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = Survey;
    this.scheme = SurveyScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }


  modifyResponse(response){
    console.log("response",response);
    let resultantResponse = [];
    return SforceDataContext.getActiveUser()
    .then(activeUser=>{
      let atcClass = activeUser.atcClass.split(' ');
      if(!activeUser.businessUnit)
      {
        activeUser.businessUnit = '';
      }

      response.records = response.records.filter(record=>{
        if(record.bu && record.productAtcClass)
        {
          if(record.bu==activeUser.businessUnit && record.productAtcClass in atcClass)
          {
            resultantResponse.push(record);
          }
        }
        else if(record.bu && !record.productAtcClass)
        {
          if((record.bu).toLowerCase() == (activeUser.businessUnit).toLowerCase())
          {
            resultantResponse.push(record);
          }
        }
        else if(!record.bu && record.productAtcClass)
        {
          if((record.productAtcClass) in atcClass)
          {
            resultantResponse.push(record);
          }
        }
        else if(!record.bu && !record.productAtcClass)
        {
            resultantResponse.push(record);
        }
      })
      console.log("response after",response);
      console.log("resultantResponse",resultantResponse);
      return response;

    })
  }

  _fetchAllQuery() {
    const activeSurvey = {};

    activeSurvey[this.scheme.fields.isActive.sfdc] = true;
    return new Query()
      .selectFrom(this.scheme.table)
      .where(activeSurvey);
  }

  getCountQuery(paramsString = "") {
    const query = new Query(this.scheme.table);
    
    query.customQuery(
      `SELECT COUNT(*) ` +
      `FROM {${this.scheme.table}} ` +
      `WHERE {${this.scheme.table}:${this.scheme.fields.isActive.sfdc}} = ` + Query.TRUE +
      `${paramsString}`
    );
    
    return query;
  }
}
