import {EntitiesCollection} from './EntitiesCollection';
import {Query} from '../services/common/Query';
import {SforceDataContext} from './SforceDataContext';
import {Product} from '../models/Product';
import {ProductScheme} from './../models/scheme/ProductScheme';
import {ProductPresentationScheme} from './../models/scheme/ProductPresentationScheme';
import {PresentationScheme} from './../models/scheme/PresentationScheme';


export class ProductsCollection extends EntitiesCollection {
  public model;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = Product;
    this.scheme = ProductScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
      config.query += ` WHERE Local_Type__c = '${this.scheme.TYPE_LOCAL}'`;
      return config;
    });
  }

  didStartDownloading() {
    return this.fetchAll()
      .then(this.getAllEntitiesFromResponse.bind(this))
      .then(this.removeEntities.bind(this));
  }

  getPromotedProducts() {
    return SforceDataContext.getActiveUser()
      .then((activeUser) => {
        const atcClassArray = activeUser && activeUser.pinCode.split(' ');
        return this.fetchAllWhereIn(this.scheme.fields.atcClass.sfdc, atcClassArray);
      })
      .then((response) => response.records);
  }

  public fetchForProductIds (productIds) {
    const query = this._fetchAllQuery()
      .whereIn(this.scheme.fields.id.sfdc, productIds);
    
    return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }

  getProductsByIds(productsIds) {
    return this.fetchAllWhereIn(this.scheme.fields.id.sfdc, productsIds)
      .then((response) => response.records);
  }

  fetchAllProductsWithDownloadedPresentations() {
    const query = this._buildAllQueryProductsWithDownloadedPresentations();
    return this.fetchWithQuery(query);
  }

  private _buildAllQueryProductsWithDownloadedPresentations() {
    const products = this.scheme.table;
    const productId = this.scheme.fields.id.sfdc;
    const productName = this.scheme.fields.name.sfdc;
    const productPresentations = ProductPresentationScheme.table;
    const productPresentationId = ProductPresentationScheme.fields.presentation.sfdc;
    const productPresentationProductId = ProductPresentationScheme.fields.product.sfdc;
    const presentations = PresentationScheme.table;
    const presentationId = PresentationScheme.fields.id.sfdc;
    const customQuery =
      `SELECT DISTINCT {${products}:${Query.ALL}} ` +
      `FROM {${presentations}}, {${products}}, {${productPresentations}} ` +
      `WHERE {${presentations}:${presentationId}} = {${productPresentations}:${productPresentationId}} ` +
      `AND {${productPresentations}:${productPresentationProductId}} = {${products}:${productId}} ` +
      `AND {${presentations}:currentVersion} > 0  ORDER BY {${products}:${productName}}`;

    const query = new Query(presentations);
    query.customQuery(customQuery);
    return query;
  }

  didFinishDownloading(records) {
    return records;
  }

}
