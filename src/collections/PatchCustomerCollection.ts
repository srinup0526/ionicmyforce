import { EntitiesCollection } from './EntitiesCollection';
import SettingsManager from './../services/db/SettingsManager';
import {PatchCustomerScheme} from '../models/scheme/PatchCustomerScheme';
import {PatchCustomer} from './../models/PatchCustomer';
import {PicklistManager} from './../services/db/picklist-managers/base/PicklistManagers';
import {StoreConfig} from "../services/synchronisation/StoreConfig";
import {Query} from "./../services/common/Query";

declare function getForceSyncClient();


export class PatchCustomerCollection extends EntitiesCollection {
  public model;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = PatchCustomer;
    this.scheme = PatchCustomerScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  parseModel(result) {
    if (result.Patch__r) {
      result[this.scheme.fields.patchName.sfdc] = result.Patch__r.Name;
      result[this.scheme.fields.patchStation.sfdc] = result.Patch__r.Station__c;
    }

    if (result.Contact__r) {
      result[this.scheme.fields.contactName.sfdc] = result.Contact__r.Name;
    }
    if(result.RecordType) {
      //result[this.scheme.fields.recordTypeId.sfdc] = result.RecordType.Id;
      //result[this.scheme.fields.remoteRecordType.sfdc] = result.RecordType.Name;
      result[this.scheme.fields.recordType.sfdc] = result.RecordType.Name;
    }

    if(result.Account__r) {
      result[this.scheme.fields.accountName.sfdc] = result.Account__r.Name;
    }

    return super.parseModel(result);
  }

  preparePicklists() {
    console.log("preparePicklists called");
    console.log("this.scheme.sfdcTable",this.scheme.sfdcTable);
    PicklistManager.clearCache(this.scheme.sfdcTable);
   
    return PicklistManager.initSoup()
      .then(this._fetchUniqueRecordType.bind(this))
      .then((picklistObject) => {
        console.log("picklistObject",picklistObject);
        const recordTypeObject = {
          fieldName: this.scheme.RECORD_TYPE_FIELD_NAME,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.RECORD_TYPE_FIELD_NAME}`,
          picklistOptions: picklistObject.recordTypes.slice(0)
        };
        return getForceSyncClient().smartstoreClient.upsertSoupEntriesWithExternalId(StoreConfig.getConfig(), PicklistManager.soupName, [recordTypeObject], PicklistManager.identityKey);
      })
  }

  _distinctQuery(fieldName) {
    console.log("fieldName",fieldName);
    const maxValue = 99999999;
    const query = new Query().customQuery(`SELECT DISTINCT {${this.scheme.table}:${fieldName}} FROM {${this.scheme.table}} WHERE {${this.scheme.table}:${fieldName}} != 'null'`);
    const smartSql = this._createSmartSqlQuery(query, this.scheme.table, maxValue);
    console.log("query",query.query);
    
    return this.fetchRemoteObjectsFromCache(this.cache, smartSql, true);
  }

  _fetchUniqueRecordType() {
    let recordTypes = [];
    
    return this._distinctQuery(this.scheme.RECORD_TYPE_FIELD_NAME)
      .then((response) => {
        console.log(" RECORD_TYPE_FIELD_NAME response",response);
        recordTypes = this._mapPicklistObjectsArray(response.records);
        return {
          recordTypes: recordTypes
        };
      })
  }

  _mapPicklistObjectsArray(records) {
    return records
      .filter((record) => record)
      .map((record) => ({
        "label": record,
        "value": record
      }));
  }

  // public didStartDownloading() {
  //   return this.preparePicklists()
  // }

  didFinishDownloading(records) {
    return records;
    // this.preparePicklists()
    // .then(()=>{
    //   return records;
    // })
  }
}