import { EntitiesCollection } from './EntitiesCollection';
import { Document } from './../models/Document';
import { DocumentScheme } from './../models/scheme/DocumentScheme';
import { Query } from '../services/common/Query';


export class DocumentCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = Document;
    this.scheme = DocumentScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }
}
