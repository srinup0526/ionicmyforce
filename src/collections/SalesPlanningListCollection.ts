import { EntitiesCollection } from './EntitiesCollection';
import { SalesPlanList } from '../models/SalesPlanList';
import { SalesPlanningListScheme } from "../models/scheme/SalesPlanningListScheme";
import { Injectable } from "@angular/core";
import { Query } from "./../services/common/Query";
import { SalesPlanningCollection } from './SalesPlanningCollection'



@Injectable({
  providedIn: 'root'
})
export class SalesPlanningListCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = SalesPlanList;
    this.scheme = SalesPlanningListScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  // modifyResponse(salesPlanId='')
  // {
  //   console.log("salesPlanId",salesPlanId);
  //   const query = this._fetchAllQuery();
  //   const fetchData = {};
  //   if(salesPlanId!='')
  //   {
  //     fetchData[this.scheme.fields.salesplanning.sfdc] = salesPlanId;
  //   }
  //   console.log("fetchData",fetchData);
  //   query.where(fetchData);
  //   return this.fetchWithQuery(query);
  // }

  fetchEntityByCustomer(customerId,SalesPlanId)
  {
    const fieldValues = {};
    fieldValues[SalesPlanningListScheme.fields.customer.sfdc] = customerId;
    fieldValues[SalesPlanningListScheme.fields.salesplanning.sfdc] = SalesPlanId;
    return this.fetchAllWhere(fieldValues);
  }

  didStartUploading(entities:any)
  {
    let salesPlanningCollection =  new SalesPlanningCollection;
    return salesPlanningCollection.linkEntitiesToEntity(entities, 'salesplanning')
  }

  didFinishDownloading(records) {
    return records;
  }
}