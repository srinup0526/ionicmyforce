import {EntitiesCollection} from './EntitiesCollection';
import {IqviaQuestion} from './../models/IqviaQuestion';
import {IqviaQuestionScheme} from './../models/scheme/IqviaQuestionScheme';
import { Injectable } from '@angular/core';


@Injectable()
export class IqviaQuestionsCollection extends EntitiesCollection {

  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = IqviaQuestion;
    this.scheme = IqviaQuestionScheme;
    this.init();
  }

  public prepareServerConfig(configPromise) {
    return configPromise
      .then((config) => {
        return config;
      });
  }

  public fetchAllQuestions() {
    const whereFields = {};
    whereFields[this.scheme.fields.isActive.sfdc] = true;

    return this.fetchAllWhereAndSortBy(whereFields,[this.scheme.fields.order.sfdc], true)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }

  didFinishDownloading(records) {
    return records;
  }
}
