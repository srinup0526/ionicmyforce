import {Injectable} from '@angular/core';
import {EntitiesCollection} from './EntitiesCollection';
import {ProductSegmentation} from './../models/ProductSegmentation';
import {ProductSegmentationScheme} from './../models/scheme/ProductSegmentationScheme';


@Injectable({
  providedIn: 'root'
})
export class ProductSegmentationsCollection extends EntitiesCollection {
  public model;
  public scheme;
  
  constructor() {
    super();
    this.model = ProductSegmentation;
    this.scheme = ProductSegmentationScheme;
    this.init();
  }
  
  public prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
      config.query += ` WHERE ${this.scheme.fields.isActive.sfdc} = true `;
      
      return config;
    });
  }
  
  public didFinishDownloading(records) {
    return records;
  }
  
  public parseModel(result) {
    if(result.PhProduct__r) {
      result[this.scheme.fields.phProductName.sfdc] = result.PhProduct__r.Name;
    }
  
    return super.parseModel(result);
  }
  
  public getProductSegmentations(contactAccountId): Array<ProductSegmentation> {
    const fieldsWithValues = {};
    
    fieldsWithValues[this.scheme.fields.accountId.sfdc] = contactAccountId;
    
    return this.fetchAllWhere(fieldsWithValues)
      .then((response) => {
        return this.getAllEntitiesFromResponse(response);
      });
  }
  
  public getSegmentationByAccountIdQuery (contactAccountId: string): string {
    return ` ({${this.scheme.table}:${this.scheme.fields.accountId.sfdc}} = '${contactAccountId}') `;
  }
  
}
