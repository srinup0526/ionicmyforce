import { EntitiesCollection } from './EntitiesCollection';
import {Scenario} from "../models/Scenario";
import {ScenarioScheme} from "../models/scheme/ScenarioScheme";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class ScenariosCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = Scenario;
    this.scheme = ScenarioScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }


  didFinishDownloading(records) {
    return records;
  }
}
