import { EntitiesCollection } from './EntitiesCollection';
import { SalesPlan } from "../models/SalesPlan";
import { SalesPlanningScheme } from "../models/scheme/SalesPlanningScheme";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class SalesPlanningCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = SalesPlan;
    this.scheme = SalesPlanningScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  public parseModel(result) {
    if(result.CreatedBy) {
      result[this.scheme.fields.createdBy.sfdc] = result.CreatedBy.Name;
    }
  
    return super.parseModel(result);
  }

  didStartUploading(records){
     return records.map(record =>{
          record.status == this.scheme.APPROVAL_STATUS_SUBMITTED_OFFLINE?record.status=this.scheme.APPROVAL_STATUS_SUBMITTED:record.status;
          return record;
        });
    }
  didFinishDownloading(records) {
    return records;
  }
  findSalesPlanForMonth(month, year)
  {
    const whereClause = {}
    whereClause[this.scheme.fields.month.sfdc] = month
    whereClause[this.scheme.fields.year.sfdc] = year
    console.log("validateInput",whereClause);
    return this.fetchAllWhere(whereClause)
    .then(response=> { return this.getEntityFromResponse(response); });
  }
}