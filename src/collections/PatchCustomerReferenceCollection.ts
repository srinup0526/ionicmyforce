import { PatchCustomerReference } from './../models/PatchCustomerReference';
import { EntitiesCollection } from './EntitiesCollection';
import { Entity } from './../models/base/Entity';
import { PatchCustomerReferenceScheme } from './../models/scheme/PatchCustomerReferenceScheme';


export class PatchCustomerReferenceCollection extends EntitiesCollection {
  public model;
  public scheme;
  public onlyWithActiveStatus: boolean;
  
  constructor() {
    super();
    this.model = PatchCustomerReference;
    this.scheme = PatchCustomerReferenceScheme;
    this.init();
  
    this.onlyWithActiveStatus = false;
  }
  
  public prepareServerConfig (configPromise): Promise<any> {
    return configPromise;
  }
  
  public parseModel(result) {
  
    return super.parseModel(result);
  }
  
  public didFinishDownloading(records) {
    return records;
  }

  fetchByCustomerId(customerId: string): Promise<any> {
    const keyValue = {};
    
    keyValue[this.scheme.fields.customerSfId.sfdc] = customerId;
    
    return this.fetchAllWhere(keyValue)
      .then(this.getAllEntitiesFromResponse.bind(this));
  } 
  
}
