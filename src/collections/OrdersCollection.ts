import { EntitiesCollection } from './EntitiesCollection';
import { OrdersLineCollection } from './OrdersLineCollection';
import { Order } from './../models/Order';
import { OrderScheme } from './../models/scheme/OrderScheme';
import { Query } from '../services/common/Query';
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class OrdersCollection extends EntitiesCollection {
  public model;
  public scheme;
  public ordersLineCollection: OrdersLineCollection
  
  constructor() {
    super();
    this.model = Order;
    this.scheme = OrderScheme;
    this.init();

    this.ordersLineCollection = new OrdersLineCollection();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }
  
  parseModel(result) {
    if (result.Account__r) {
      result[this.scheme.fields.remoteAccountName.sfdc] = result.Account__r.Name;
    }

    if (result.RecordType) {
      result[this.scheme.fields.recordType.sfdc] = result.RecordType.Name;
    }
  
    result[this.scheme.fields.orderTotalQuantity.sfdc] = result[this.scheme.fields.orderTotalQuantity.sfdc] || 0;
    result[this.scheme.fields.orderSalesAmount.sfdc] = result[this.scheme.fields.orderSalesAmount.sfdc] || 0;
    result[this.scheme.fields.specialprice.sfdc] = result[this.scheme.fields.specialprice.sfdc] ? true : false;

    return super.parseModel(result);
  }

  didFinishDownloading(records) {
    return records;
  }
  
  didStartUploading(records) {
    return records.map((record) => {
      if(record.status == this.scheme.APPROVAL_STATUS_SUBMITTED_OFFLINE) {
        record.status = this.scheme.APPROVAL_STATUS_SUBMITTED
      }
      
      return record;
    });
  }
  
  saveRecordToCache(cache, records) {
    const updatedRecords = this.updateOrders(records);
    
    return super.saveRecordToCache(cache, updatedRecords);
  }
  
  fetchByOrderId(orderId: string): Promise<any> {
    const keyValue = {};
    
    keyValue[this.scheme.fields.id.sfdc] = orderId;
    
    return this.fetchAllWhere(keyValue)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }
  
  getOrderLineItems(orderData) {
    const query = this.ordersLineCollection
      ._fetchAllQuery()
      .where(this.ordersLineCollection.scheme.fields.orderId.sfdc, orderData.id);
    
    return this.ordersLineCollection.fetchWithQuery(query)
      .then((records) => {
        return this.ordersLineCollection.getAllEntitiesFromResponse(records);
      });
  }
  
  _fetchAllQuery() {
    return new Query()
      .selectFrom(this.scheme.table);
  }
  
  private updateOrders(records) {
    return records.map((record) => {
      
      if(record.Account__r) {
        record.accountName = record.Account__r.Name;
      }
      
      if(record.RecordType) {
        record.recordType = record.RecordType.Name;
      }
      
      return record;
    });
  }
  
  private updateOrderWithLineItems(orderEntity) {
    return this.ordersLineCollection.fetchByOrderId(orderEntity.id, orderEntity.attributes._soupEntryId)
      .then((orderLines) => {
        orderEntity.orderTotalQuantity = 0;
        orderEntity.orderSalesAmount = 0;
        
        orderLines.forEach((orderline) => {
          orderEntity.orderSalesAmount = orderEntity.orderSalesAmount - (-orderline.salesAmount);
          orderEntity.orderTotalQuantity = orderEntity.orderTotalQuantity - (-orderline.quantity);
        });
        
        return this.updateEntity(orderEntity);
      });
  }
  
}
