import { EntitiesCollection } from './EntitiesCollection';
import { Brick } from './../models/Brick';
import { BrickScheme } from './../models/scheme/BrickScheme';
import { Query } from '../services/common/Query';
import { ReferenceCollection } from './references/ReferenceCollection';
import {ConfigurationManager} from './../services/db/ConfigurationManager';
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class BricksCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    super();
    this.model = Brick;
    this.scheme = BrickScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
        return ConfigurationManager.getConfig('brickRecordTypeId')
            .then((brickRecordTypeId) => {
              config.query += ` WHERE RecordTypeId = '${brickRecordTypeId}'`;
              return config;
            });
        return config;
      });
  }

  _fetchAvailableBrickIds() {
    const referenceCollection: ReferenceCollection = new ReferenceCollection();
    return referenceCollection
      .fetchAll()
      .then((records) => {
        return referenceCollection.getAllEntitiesFromResponse(records);
      })
      .then((references) => {
        const brickIds = references.map((reference)=> { return reference.organizationBrick});
        //return _.uniq brickIds
        return brickIds;
      });
  }

  didFinishDownloading(records) {
    return records;
  }
}
