import { EntitiesCollection } from './EntitiesCollection';
import { Patch } from './../models/Patch';
import { PatchScheme } from './../models/scheme/PatchScheme';
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class PatchCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    super();
    this.model = Patch;
    this.scheme = PatchScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }
}
