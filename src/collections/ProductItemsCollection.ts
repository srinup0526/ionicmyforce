import { EntitiesCollection } from './EntitiesCollection';
import { ProductItem } from './../models/ProductItem';
import { ProductItemScheme } from './../models/scheme/ProductItemScheme';
import { SforceDataContext } from './SforceDataContext';
import { Query } from '../services/common/Query';





export class ProductItemsCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    super();
    this.model = ProductItem;
    this.scheme = ProductItemScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }

  fetchAllAvailableProducts()
  {
    return SforceDataContext.getActiveUser()
    .then(activeUser=>{
      console.log("activeUser",activeUser);
      let atcClass = activeUser.pinCode.split(' ') || [];
      const fieldValues = {};
      fieldValues[ProductItemScheme.fields.productTypeDetect.sfdc] = ProductItemScheme.PRODUCT_TYPE;
      fieldValues[ProductItemScheme.fields.productDivision.sfdc] = activeUser.businessUnit;
      const query = this._fetchAllQuery();
      query.where(fieldValues).whereIn(this.scheme.fields.productAtcClass.sfdc,atcClass);
      console.log("query",query);
      console.log("query",query.query);
      return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this));
    })
  }



}
