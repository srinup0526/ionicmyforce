import { EntitiesCollection } from './EntitiesCollection';
import { SurveyQuestionnaire } from './../models/SurveyQuestionnaire';
import { SurveyQuestionnaireScheme } from './../models/scheme/SurveyQuestionnaireScheme';
import { Query } from '../services/common/Query';


export class SurveyQuestionnaireCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = SurveyQuestionnaire;
    this.scheme = SurveyQuestionnaireScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }
  fetchBySurveyId(surveyId)
  {
    const fieldValues = {};
    fieldValues[this.scheme.fields.surveyid.sfdc] = surveyId;
    return this.fetchAllWhere(fieldValues).then(res=> { return this.getAllEntitiesFromResponse(res);});
  }
}
