import { EntitiesCollection } from './EntitiesCollection';
import { ProductSegmentationHistory } from './../models/ProductSegmentationHistory';
import { ProductSegmentationHistoryScheme } from './../models/scheme/ProductSegmentationHistoryScheme';
//import { Query } from '../services/common/Query';


export class ProductSegmentationsHistoriesCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = ProductSegmentationHistory;
    this.scheme = ProductSegmentationHistoryScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }

  parseModel(result){
    // if result[@model.sfdc.createdByName]
    //   result[@model.sfdc.createdByName] = result.CreatedBy?.Name

    // super result
  }

  fetchStatusHistoryById(parentId){
//   if (!parentId)
//     return $.when([]);

//  const fieldsValues = {};
//   fieldsValues[this.scheme.fields.parentId.sfdc] = parentId;
//   const sortFields = [this.scheme.fields.createdDate.sfdc];

//   return this.fetchAllWhereAndSortBy(fieldsValues,sortFields)
//     .then((response)=>{
//       return this.getAllEntitiesFromResponse(response);
//     })
//     .then((records) =>{
//       this._filterValues(records);
//      });

  }
  _filterValues(records){
//    const COUNT_OF_SYMBOLS_ON_ID_FIELD = 18;

//     return records.filter((record)=>{}

//       if(!record.newValue || record.newValue.length != COUNT_OF_SYMBOLS_ON_ID_FIELD)
//         return true;

//       return switch(record.changedField)
//         when 'PhProduct__c' then record.newValue.indexOf('a0I') != 0;
//         when 'Account__c' then record.newValue.indexOf('001') != 0;
//     )
//   }
  }
}
