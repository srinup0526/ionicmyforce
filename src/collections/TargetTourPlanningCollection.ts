import { EntitiesCollection } from './EntitiesCollection';
import { TourPlanningEntity } from './../models/TourPlanningEntity';
import { TourPlanningEntityScheme } from './../models/scheme/TourPlanningEntityScheme';
import { Query } from '../services/common/Query';


export class TargetTourPlanningCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = TourPlanningEntity;
    this.scheme = TourPlanningEntityScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }

  _fetchAllQuery() {
    const isStatusActive = {};

    isStatusActive[this.scheme.isActive.scheme] = true;
    return new Query()
      .selectFrom(this.scheme.table)
      .where(isStatusActive);
  }
}
