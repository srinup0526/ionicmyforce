import {EntitiesCollection} from './EntitiesCollection';
import {IqviaAccountSkuTask} from './../models/IqviaAccountSkuTask';
import {IqviaAccountSkuTaskScheme} from './../models/scheme/IqviaAccountSkuTaskScheme';
import {Utils} from './../utils/Utils';
import {Query} from './../services/common/Query';
import { Injectable } from '@angular/core';
import moment from 'moment';


@Injectable()
export class IqviaAccountSkuTasksCollection extends EntitiesCollection {

  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = IqviaAccountSkuTask;
    this.scheme = IqviaAccountSkuTaskScheme;
    this.init();
  }

  public prepareServerConfig(configPromise) {
    return configPromise
      .then((config) => {
        return config;
      });
  }

  public fetchAllForOrganizationByDate(organizationId: string, date: any) {
    const query = this._fetchAllQuery();

    date = moment(date);

    const whereFields = {};
    whereFields[this.scheme.fields.organization.sfdc] = organizationId;

    query.where(whereFields);

    const whereStartDate = {};
    whereStartDate[this.scheme.fields.startDate.sfdc] = Utils.originalDate(date);

    query.where(whereStartDate, Query.LRE);

    const whereEndDate = {};
    whereEndDate[this.scheme.fields.endDate.sfdc] = Utils.originalDate(date);

    query.where(whereEndDate, Query.GRE);

    query.orderBy([this.scheme.fields.skuName.sfdc, this.scheme.fields.taskName.sfdc], Query.ASC);

    return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }

  didFinishDownloading(records) {
    return records;
  }
}
