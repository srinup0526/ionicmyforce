import {EntitiesCollection} from './EntitiesCollection';
import {SforceDataContext} from './SforceDataContext';
import {FAQ} from './../models/Faq';
import {FaqScheme} from './../models/scheme/FaqScheme';
import { Injectable } from '@angular/core';


@Injectable()
export class FAQCollection extends EntitiesCollection {

  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = FAQ;
    this.scheme = FaqScheme;
    this.init();
  }

  public prepareServerConfig(configPromise) {
    return configPromise
      .then((config) => {
        return SforceDataContext.getActiveUser()
          .then((currentUser) => {
            config.query += ` WHERE ${this.scheme.fields.currencyIsoCode.sfdc} = '${currentUser.currency}'`;

            return config;
          });
      });
  }

  public fetchAllOrdered() {
    return this.fetchAllSortedBy([this.scheme.fields.order.sfdc], true)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }

  didFinishDownloading(records) {
    return records;
  }
}
