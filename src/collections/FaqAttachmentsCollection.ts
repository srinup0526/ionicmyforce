import {FaqAttachment} from './../models/FaqAttachment';
import {FaqScheme} from './../models/scheme/FaqScheme';
import {EntitiesCollection} from './EntitiesCollection';
import {FaqAttachmentScheme} from '../models/scheme/FaqAttachmentScheme';
import {SforceDataContext} from './SforceDataContext';
import { Injectable } from '@angular/core';
import { AttachmentLoadManager } from '../services/common/attachments/AttachmentLoadManager';



@Injectable()
export class FAQAttachmentCollection extends EntitiesCollection {
  public model;
  public scheme;

  private attachmentLoadManager: AttachmentLoadManager;

  constructor(attachmentLoadManager: AttachmentLoadManager) {
    // @ts-ignore
    super(...arguments);
    this.model = FaqAttachment;
    this.scheme = FaqAttachmentScheme;
    this.init();
    this.attachmentLoadManager = attachmentLoadManager;
  }

  public prepareServerConfig(configPromise): Promise<any> {
    return SforceDataContext.getActiveUser()
      .then((currentUser) => {
        return configPromise.then((config) => {
          config.query += ` WHERE ${this.scheme.fields.parentId.sfdc} IN (SELECT Id FROM FAQ__c WHERE ${FaqScheme.fields.currencyIsoCode.sfdc} = '${currentUser.currency}')`;

          return config;
        });
      });
  }

  public getAllAttachmentsForFaqId(faqId): Promise<Array<any>> {
    const promotionIdValue = {};

    promotionIdValue[this.scheme.fields.parentId.sfdc] = faqId;

    return this.fetchAllWhere(promotionIdValue)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }

  public didFinishDownloading(records) {
    return Promise.all(records.map(this.loadAttachment.bind(this)))
      .then( () => {
        return records;
      })
      .catch((error) => {
        console.error(error);
      });
  }

  private loadAttachment(record) {
    return new Promise((resolve, reject) => {
      const parsedRecord = this.parseModel(record);

      this.attachmentLoadManager.queueInvoke(parsedRecord, {
        onStateChange: this.onStateChange,
        onSuccess: resolve,
        onFail: resolve
      });
    });
  }

  private onStateChange() {
    console.log('onStateChange');
  }
}