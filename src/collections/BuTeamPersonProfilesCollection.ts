import { EntitiesCollection } from './EntitiesCollection';
import { BuTeamPersonProfile } from './../models/BuTeamPersonProfile';
import { BuTeamPersonProfileScheme } from './../models/scheme/BuTeamPersonProfileScheme';
import { Query } from '../services/common/Query';
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class BuTeamPersonProfilesCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    super();
    this.model = BuTeamPersonProfile;
    this.scheme = BuTeamPersonProfileScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
      return configPromise;
  }

  fetchForUserBU(userBu) {
    const fieldsValues = {}

    fieldsValues[this.scheme.fields.businessUnit.sfdc] = userBu;
    return new Query()
      .selectFrom(this.scheme.table)
      .where(fieldsValues);
  }

  fetchForUserUnitAndContacts(userBU, contactIds) {
    const buCondition = {}

    buCondition[this.scheme.fields.businessUnit.sfdc] = userBU;
    const query = this._fetchAllQuery().where(buCondition).and().whereIn(this.scheme.fields.organizationSfid.sfdc, contactIds);
    return this.fetchWithQuery(query)
    .then(records=> { return this.getAllEntitiesFromResponse(records) })
  }

  didFinishDownloading(records) {
    return records;
  }
}
