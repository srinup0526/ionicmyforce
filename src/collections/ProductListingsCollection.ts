import { EntitiesCollection } from './EntitiesCollection';
import { ProductListing } from './../models/ProductListing';
import { ProductListingScheme } from './../models/scheme/ProductListingScheme';
//import { Query } from '../services/common/Query';


export class RedFlagCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = ProductListing;
    this.scheme = ProductListingScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    configPromise.then((config)=>{
      config.query += " WHERE ${@model.sfdc.isActive} = true";
   
      return config;
    });
  }

  didFinishDownloading(records) {
    return records;
  }

  parseModel(result){
//   if result[this.scheme.fields.phProductName.sfdc]
//     result[this.scheme.fields.phProductName.sfdc] = result.PharmaProduct__r?.Name

//   super result

  }


}
