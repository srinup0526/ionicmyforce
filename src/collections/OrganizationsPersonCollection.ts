import {Injectable} from "@angular/core";
import {OrganizationsCollection} from "./OrganizationsCollection";
import {Query} from "../services/common/Query";

@Injectable({
  providedIn: 'root'
})
export class OrganizationsPersonCollection extends OrganizationsCollection {
  
  _fetchAllQuery() {
    const fieldValue = {};
    
    fieldValue[this.scheme.fields.isPersonAccount.sfdc] = true;
    
    return new Query()
      .selectFrom(this.scheme.table)
      .where(fieldValue);
  }
}
