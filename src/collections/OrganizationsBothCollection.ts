import {Injectable} from "@angular/core";
import {OrganizationsCollection} from "./OrganizationsCollection";
import {Query} from "../services/common/Query";

@Injectable({
  providedIn: 'root'
})
export class OrganizationsBothCollection extends OrganizationsCollection {
  
  _fetchAllQuery() {
    return new Query()
      .selectFrom(this.scheme.table);
  }
}
