import {EntitiesCollection} from './EntitiesCollection';
import {ProductPresentation} from '../models/ProductPresentation';
import {ProductsCollection} from './ProductsCollection';
import {ProductPresentationScheme} from './../models/scheme/ProductPresentationScheme';


export class ProductPresentationsCollection extends EntitiesCollection {
  public model;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = ProductPresentation;
    this.scheme = ProductPresentationScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
      return this._getProductsIdsByATC()
        .then(this._prepareProductsIdsStringForQuery.bind(this))
        .then((productsIdsString: string) => {
          config.query += ` WHERE ${this.scheme.fields.product.sfdc} IN (${productsIdsString})`;
          return config;
        });
    });
  }


  parseModel(result) {
    result[this.scheme.fields.product.sfdc] = result.Pharma_Product__c;
    result[this.scheme.fields.presentation.sfdc] = result.Presentation__c;
    if (result.Pharma_Product__r) {
      result[this.scheme.fields.productName.sfdc] = result.Pharma_Product__r.Name;
    }
    if (result.Presentation__r) {
      result[this.scheme.fields.presentationName.sfdc] = result.Presentation__r.Name;
    }

    return super.parseModel(result);
  }

  didFinishDownloading(records) {
    return records;
  }


  getProductsByPresentation(presentationId: string) {
    const where = {};
    where[this.scheme.fields.presentation.sfdc] = presentationId;
    return this.fetchAllWhere(where)
      .then((response) => {
        return this.getAllEntitiesFromResponse(response);
      });
  }


  private _getProductsIdsByATC() {
    const productsCollection = new ProductsCollection();
    return productsCollection.getPromotedProducts();
  }


  private _prepareProductsIdsStringForQuery(products) {
    return products.map((product) => `'${product.id}'`).join(',') || 'Null';
  }
}
