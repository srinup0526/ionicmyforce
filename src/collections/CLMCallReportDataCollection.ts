import { EntitiesCollection } from './EntitiesCollection';
import { CLMCallReportData } from './../models/CLMCallReportData';
import { CLMCallReportDataScheme } from './../models/scheme/CLMCallReportDataScheme';
import { CallReportsCollection } from './CallReportsCollection/CallReportsCollection';


export class CLMCallReportDataCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = CLMCallReportData;
    this.scheme = CLMCallReportDataScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  fetchAllByCallReportId(callReportId, soupEntryId)
  {
    this.fetchAllWhereIn(this.scheme.fields.callReportId.sfdc, [callReportId, soupEntryId])
      .then((response) => {
        this.getAllEntitiesFromResponse(response)
      }
      )
      .then((records) =>{
        //this.dataSource = records;
      }
      )
  }

  fetchAllWhere(fieldsValues, ignoreDeleted=true){
    const query = this._fetchAllQuery().where(fieldsValues);
    return this.fetchWithQuery(query, ignoreDeleted);
  }
    

  didStartUploading(entities){
    const сallReportsCollection = new CallReportsCollection()
    return сallReportsCollection.linkEntitiesToEntity(entities, 'callReportId');
  }

  didFinishDownloading(records) {
    return records;
  }
}
