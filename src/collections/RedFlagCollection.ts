import { EntitiesCollection } from './EntitiesCollection';
import { RedFlag } from './../models/RedFlag';
import { RedFlagScheme } from './../models/scheme/RedFlagScheme';
//import { Query } from '../services/common/Query';


export class RedFlagCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = RedFlag;
    this.scheme = RedFlagScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }

  fetchEntityBycontactSfId(customerName){
    const  keyValue = {}
    keyValue[this.scheme.fields.customerName.sfdc] = customerName;
    const query = this._fetchAllQuery();
    query.where(keyValue)
    return this.fetchWithQuery(query)
    .then(response=>{ return this.getAllEntitiesFromResponse(response)});
  }
}
