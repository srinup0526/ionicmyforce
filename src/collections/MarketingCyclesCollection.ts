import { EntitiesCollection } from './EntitiesCollection';
import { MarketingCycle } from './../models/MarketingCycle';
import { MarketingCycleScheme } from './../models/scheme/MarketingCycleScheme';
import { SforceDataContext } from './SforceDataContext';
import { Utils } from '../utils/Utils';

export class MarketingCyclesCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    super();
    this.model = MarketingCycle;
    this.scheme = MarketingCycleScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise
    .then((config) => {
      return SforceDataContext.getActiveUser()
        .then((currentUser) => {
          const today = Utils.currentDate();
          
          config.query += ` WHERE  ${this.scheme.fields.startDate.sfdc} <=  ${today} AND  ${this.scheme.fields.endDate.sfdc} >=  ${today} AND  ${this.scheme.fields.currencyIsoCode.sfdc} = '${currentUser.currency}'`;
          return config;
        });
      });
  }

  didFinishDownloading(records) {
    return records;
  }
}
