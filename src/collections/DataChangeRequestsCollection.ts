import {DataChangeRequest} from './../models/DataChangeRequest';
import {EntitiesCollection} from './EntitiesCollection';
import {DataChangeRequestScheme} from '../models/scheme/DataChangeRequestScheme';
import {Injectable} from "@angular/core";
import { Settings, SettingsImpl } from './../services/db/Settings';


@Injectable({
  providedIn: 'root'
})
export class DataChangeRequestsCollection extends EntitiesCollection {
  public model;
  public scheme;
  
  constructor() {
    super();
    this.model = DataChangeRequest;
    this.scheme = DataChangeRequestScheme;
    this.init();
  }
  
  public prepareServerConfig(configPromise, lastSyncTime) {
    const settings: SettingsImpl = Settings.getInstance();
  
    return configPromise.then((config) => {
      if(settings.isChinaUser()){
        return config;
      }

      config.query += ` WHERE ${this.scheme.fields.dcrType.sfdc} != '${DataChangeRequestScheme.TYPES.SEGMENTATION}'`;

      return config
    });
  }
  
  public didStartUploading(records) {
    return records.map((record)=> {
      
      if(record.dcrStatus == DataChangeRequestScheme.STATUSES.DRAFT) {
        record.dcrStatus = DataChangeRequestScheme.STATUSES.SENT_TO_SALESFORCE;
      }
      
      return record;
    });
  }
  
  public didFinishDownloading(records) {
    return records;
  }
}
