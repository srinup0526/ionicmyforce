import {Injectable} from '@angular/core';
import {Presentation} from '../models/Presentation';
import {PresentationScheme} from '../models/scheme/PresentationScheme';
import {PresentationFileManager} from '../pages/media/PresentationFileManager';
import {EntitiesCollection} from './EntitiesCollection';
import {ProductPresentationsCollection} from './ProductPresentationsCollection';

@Injectable({
  providedIn: 'root'
})
export class PresentationsCollection extends EntitiesCollection {
  public model;

  private loadedPresentations: Presentation[];


  constructor(private presentationFileManager: PresentationFileManager,) {
    super();
    this.model = Presentation;
    this.scheme = PresentationScheme;
    this.loadedPresentations = [];
    this.init();
  }


  prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
      return this._getProductPresentations()
        .then(this._preparePresentationIdsStringForQuery.bind(this))
        .then((presentationsIdsString) => {
          config.query += ` WHERE ${this.scheme.fields.id.sfdc} IN (${presentationsIdsString}) AND ${this.scheme.fields.active.sfdc} = true `;
          return config;
        });
    });
  }


  didStartDownloading() {
    this.loadedPresentations = [];
    return this._setLoadedPresentationIds()
      .then(this.removeEntities.bind(this))
  }


  didFinishDownloading(records) {
    return this.fetchAll()
      .then(this.getAllEntitiesFromResponse.bind(this))
      .then(this._removeInactivePresentations.bind(this))
      .then(this._updateDownloadedPresentations.bind(this))
  }


  fetchAllLoaded(): Promise<Presentation[]> {
    return this.fetchAll()
      .then(this.getAllEntitiesFromResponse.bind(this))
      .then(this._filterLoader.bind(this));
  }

  private _filterLoader(presentations: Presentation[] = []): Presentation[] {
    return presentations.filter((presentation: Presentation) => presentation.wasDownloaded())
  }


  private _getProductPresentations() {
    const productPresentationsCollection = new ProductPresentationsCollection();
    return productPresentationsCollection.fetchAll()
      .then(productPresentationsCollection.getAllEntitiesFromResponse.bind(productPresentationsCollection));
  }


  private _preparePresentationIdsStringForQuery(productPresentations) {
    return productPresentations
      .filter((productPresentation) => {
        return productPresentation.presentation;
      })
      .map((productPresentation) => {
        return `'${productPresentation.presentation}'`;
      })
      .join(',') || 'Null';
  }


  private _setLoadedPresentationIds() {
    return this.fetchAll()
      .then(this.getAllEntitiesFromResponse.bind(this))
      .then((presentations: Presentation[]) => {
        this.loadedPresentations = presentations
          .filter((presentation: Presentation) => presentation.currentVersion && presentation.currentVersion > 0);
        return presentations;
      });
  }


  private _removeInactivePresentations(currentPresentations: Presentation[] = []) {
    let presentationsToRemove: Presentation[] = [];
    let presentationsToUpdate: Presentation[] = [];

    this.loadedPresentations.forEach((loadedPresentation: Presentation) => {
      const isShouldUpdate = currentPresentations.some((currentPresentation: Presentation) => currentPresentation.id === loadedPresentation.id);
      (isShouldUpdate ? presentationsToUpdate : presentationsToRemove).push(loadedPresentation);
    });

    const promises = presentationsToRemove.map((presentation: Presentation) => {
      return this.presentationFileManager.removePresentationById(presentation.id);
    });

    return Promise.all(promises)
      .then(() => {
        return presentationsToUpdate
      });
  }


  private _updateDownloadedPresentations(presentationsToUpdate: Presentation[] = []) {
    const promises = presentationsToUpdate.map((presentation: Presentation) => {
      return this.fetchEntityById(presentation.id)
        .then((entity: Presentation) => {
          entity.iconPath = presentation.iconPath;
          entity.currentVersion = presentation.currentVersion;
          return this.updateEntity(entity);
        });
    });

    return Promise.all(promises);
  }
}



