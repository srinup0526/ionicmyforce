import {ReferenceCollection} from './ReferenceCollection';
import {Injectable} from "@angular/core";
import {Query} from "../../services/common/Query";


@Injectable()
export class NonTargetReferencesCollection extends ReferenceCollection {
  
  constructor(){
    super();
  }
  
  public getCountQuery(): Query {
    return this.getCountQueryWithTargetFilter(false);
  }
  
  public _fetchAllQuery(): Query  {
    return this.queryWithTargetFilter(false);
  }
  
}

