import {ReferenceCollection} from './ReferenceCollection';
import {Query} from "../../services/common/Query";

export class TargetReferencesCollection extends ReferenceCollection {
  
  constructor(){
    super();
  }
  
  public getCountQuery(): Query {
    return this.getCountQueryWithTargetFilter(true);
  }
  
  public _fetchAllQuery(): Query  {
    return this.queryWithTargetFilter(true);
  }
  
}
