import { LogManager as SyncLogManager } from '../../services/common/LogManager';
import { Utils } from '../../utils/Utils';
import { Reference } from './../../models/Reference';
import { EntitiesCollection } from '../EntitiesCollection';
import { Entity } from './../../models/base/Entity';
import { ReferenceScheme } from '../../models/scheme/ReferenceScheme';
import {Query} from "../../services/common/Query";
import {ContactScheme} from "../../models/scheme/ContactScheme";
import { ContactsCollection } from '../ContactsCollection';
import {OrganizationsCollection} from "../OrganizationsCollection";


export class ReferenceCollection extends EntitiesCollection {
  public model;
  public scheme;
  public onlyWithActiveStatus: boolean;
  
  constructor() {
    super();
    this.model = Reference;
    this.scheme = ReferenceScheme;
    this.init();
  
    this.onlyWithActiveStatus = false;
  }
  
  public prepareServerConfig (configPromise): Promise<any> {
    return configPromise.then((config) => {
      config.query += ` WHERE Customer__r.Id != Null AND Organisation__r.Id != Null AND Customer__r.Account.Status__c = '${ContactScheme.STATUS_ACTIVE}' `;
      return config;
    });
  }
  
  public parseModel(result) {
    if(result.Organisation__r) {
      result[this.scheme.fields.organizationName.sfdc] = result.Organisation__r.Name;
      result[this.scheme.fields.organizationCity.sfdc] = result.Organisation__r.BillingCity;
      result[this.scheme.fields.organizationAddress.sfdc] = result.Organisation__r.BillingStreet;
      result[this.scheme.fields.organizationBrick.sfdc] = result.Organisation__r.Brick__c;
    }
    
    if(result.Customer__r){
      if (result.Customer__r.Account) {
        result[this.scheme.fields.contactAccountId.sfdc] = result.Customer__r.Account.Id;
        result[this.scheme.fields.contactRecordType.sfdc] = result.Customer__r.Account.RecordType.Name;
      }
  
      result[this.scheme.fields.contactFirstName.sfdc] = result.Customer__r.FirstName;
      result[this.scheme.fields.contactLastName.sfdc] = result.Customer__r.LastName;
      result[this.scheme.fields.name.sfdc] = result.Customer__r.Name;
      result[this.scheme.fields.remoteSubtype.sfdc] = result.Customer__r.Person_Type__c;
    }
    
    
    result.priority = result.priority || '';
    result.buSpecialty = result.buSpecialty || '';
    result.specialty = result.specialty || '';
    result.atCalls = result.atCalls || '';
    result.lastCall = result.lastCall || '';
  
    return super.parseModel(result);
  }
  
  public getReference(contactId, organizationId): Promise<any> {
    const whereFields = {};
    
    whereFields[this.scheme.fields.contactSfId.sfdc] = contactId;
    whereFields[this.scheme.fields.organizationSfId.sfdc] = organizationId;
  
    const query = new Query();
    query.selectFrom(this.scheme.table);
    query.where(whereFields);
  
    return this.fetchWithQuery(query)
      .then((response) => {
        if (response.records.length > 0) {
          return response.records[0];
        }
      });
  }
  
  public getAvailableToCreateVisitCondition(): { [key: string]: string } {
    const where = {};
  
    where[this.scheme.fields.status.sfdc] = ReferenceScheme.STATUS_ACTIVE;
  
    return where;
  }
  
  public getReferenceByContact(contactId) {
    const whereFields = {};
    
    whereFields[this.scheme.fields.contactSfId.sfdc] = contactId;
  
    return this.fetchAllWhere(whereFields)
      .then((response) => {
        return this.getAllEntitiesFromResponse(response);
      });
  }

  public getReferenceByOrganization(organizationSfId) {
    const whereFields = {};
    
    whereFields[this.scheme.fields.organizationSfId.sfdc] = organizationSfId;
  
    return this.fetchAllWhere(whereFields)
      .then((response) => {
        return this.getAllEntitiesFromResponse(response);
      });
  }
  
  public queryWithTargetFilter(isTarget): Query {
    const references = this.scheme.table;
    const query = new Query(references);
    
    return query.customQuery(`SELECT {${references}:${Query.ALL}} ` + this.queryFromRefsWithTargetFilter(isTarget).toString())
  }
  
  private queryFromRefsWithTargetFilter(isTarget): Query {
    const contacts = ContactScheme.table;
    const contactsId = ContactScheme.fields.id.sfdc;
    const references = this.scheme.table;
    const status = this.scheme.fields.status.sfdc;
    const refsContactId = this.scheme.fields.contactSfId.sfdc;
  
    const query = new Query(references);
  
    return query.customQuery(
      ` FROM {${references}}, {${contacts}} ` +
      `WHERE {${references}:${refsContactId}} = {${contacts}:${contactsId}} ` +
      `AND {${contacts}:isTargetCustomer} = ${query.valueOf(isTarget)} ` +
      `AND {${references}:${status}} = ${this.scheme.STATUS_ACTIVE}` //for removing duplicate records
    )
  }
  
  public _fetchAllQuery() {
    const query = new Query()
      .selectFrom(this.scheme.table);
  
    if(this.onlyWithActiveStatus) {
      const whereFields = {};
      whereFields[this.scheme.fields.status.sfdc] = this.scheme.STATUS_ACTIVE;
      query.where(whereFields);
    }
    
    return query;
  }
  
  private countWithTargetFilter(isTarget) {
    const references = this.scheme.table;
    const query = new Query(references);
    
    query.customQuery("SELECT COUNT(*) " + this.queryFromRefsWithTargetFilter(isTarget).toString());
    
    return this.runCountQuery(query);
  }
  
  public getCountQueryWithTargetFilter(isTarget): Query {
    const references = this.scheme.table;
    const query = new Query(references);
  
    return query.customQuery("SELECT COUNT(*) " + this.queryFromRefsWithTargetFilter(isTarget).toString());
  }
  
  public didFinishDownloading(records) {
    return records;
  }
  
  public saveRecordToCache(cache, records) {
    records = this.updateSubType(records);
    
    return this.updateAtCalls(records)
      .then((updatedRecords) => {
        return super.saveRecordToCache(cache, updatedRecords);
      });
  }
  
  public didStartDownloading() {
    return this.prepareContactSpecialtiesPriorityPickLists()
      .then(this.prepareOrganizationRecordTypePickLists.bind(this));
  }
  
  private prepareOrganizationRecordTypePickLists() {
    return this.preparePickLists(new OrganizationsCollection());
  }
  
  private prepareContactSpecialtiesPriorityPickLists() {
    return this.preparePickLists(new ContactsCollection());
  }
  
  private preparePickLists(contactCollection) {
    console.log("contactCollection",contactCollection);
    console.log("contactCollection.preparePicklists()",contactCollection.preparePicklists());
    return contactCollection.preparePicklists();
  }
  
  private updateSubType(records) {
    return records.map((record) => {
      record.subtype = record.Customer__r.Person_Type__c;
      
      return record;
    });
    
    return records;
  }
  
  private updateAtCalls(references){
    const refsByContactIds = this.referencesByContactIds(references);
    
    const contactsCollection = new ContactsCollection();
    
    return contactsCollection.fetchForContactIds(Object.keys(refsByContactIds))
      .then((contacts) => {
        return this.updateReferencesByContactsIdsForContacts(refsByContactIds, contacts);
      })
  }
  
  private referencesByContactIds(references) {
    let refsByContactIds = {};
    
    references.forEach((reference) => {
      const contactId = reference[this.scheme.fields.contactSfId.sfdc];
      
      if(!refsByContactIds[contactId]){
        refsByContactIds[contactId] = [] ;
      }
      
      refsByContactIds[contactId].push(reference);
    });
    
  
    return refsByContactIds;
  }
  
  private updateReferencesByContactsIdsForContacts(refsByContactIds, contacts) {
    let updatedReferences = [];
    
    contacts.forEach((contact) => {
      let references = refsByContactIds[contact.id];
      
      if(references && references.length){
        references.forEach((reference)=> {
          let updatedReference = this.updateAtCallForReference(reference, contact);
          
          updatedReference = this.updatePriorityForReference(updatedReference, contact);
          updatedReference = this.updateBuSpecialtyForReference(updatedReference, contact);
          updatedReference = this.updateSpecialtyForReference(updatedReference, contact);
          
          updatedReferences.push(updatedReference);
        });
      }
      
      delete refsByContactIds[contact.id]
    });
    
    let leftRefsIds = Object.keys(refsByContactIds);
    
    updatedReferences = leftRefsIds.reduce(((container, refId) => container.concat(refsByContactIds[refId])), updatedReferences);
    
    return updatedReferences;
  }
  
  
  private updateAtCallForReference(reference, contact) {
    if (contact && contact.lastDateTargetFrequency) {
      const lastDateTargetFrequency = contact.lastDateTargetFrequency;
      
      reference.atCalls = lastDateTargetFrequency.atCalls();
      reference.lastCall = lastDateTargetFrequency.lastCall();
    }else{
      reference.atCalls = '';
      reference.lastCall = '';
      
    }
    return reference;
  }
  
  private updatePriorityForReference(reference, contact) {
    if(contact && contact.priority) {
      reference.priority = contact.priority;
    }
    
    return reference;
  }
  
  private updateBuSpecialtyForReference(reference, contact) {
    if(contact && contact.buSpecialty) {
      reference.buSpecialty = contact.buSpecialty;
    }
    
    return reference;
  }

  
  private updateSpecialtyForReference(reference, contact) {
    if(contact && contact.specialty) {
      reference.specialty = contact.specialty;
    }
    
    return reference;
  }
  
}
