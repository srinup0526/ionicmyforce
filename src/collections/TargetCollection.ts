import { EntitiesCollection } from './EntitiesCollection';
import { Target } from './../models/Target';
import { TargetScheme } from './../models/scheme/TargetScheme';
//import { Query } from '../services/common/Query';


export class TargetCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = Target;
    this.scheme = TargetScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }
  getTargetByUser(user){
    const userValue = {}
    userValue[this.scheme.fields.medrepSfIdsfdc] = user.id
    this.fetchAllWhere(userValue)
    .then(response=>{this.getEntityFromResponse});
  }
}
