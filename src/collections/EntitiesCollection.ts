import {Entity} from '../models/base/Entity';
import {Utils} from '../utils/Utils';
import { Query } from '../services/common/Query';
import SettingsManager from './../services/db/SettingsManager';
import { ConfigurationManager } from './../services/db/ConfigurationManager';
import {StoreConfig} from "./../services/synchronisation/StoreConfig";

declare var Force: any;
declare var _: any;
declare var navigator: any;

declare function getForceSyncClient();


export abstract class EntitiesCollection extends Force.SObjectCollection {
  static readonly TYPE_WARNING: string = 'Warning';

  public scheme: any;
  public model: any;
  public pageSize: number;
  public loadBatchSize: number;
  public readonly METHOD_CREATE: string = 'create';
  public readonly METHOD_UPDATE: string = 'update';
  public readonly METHOD_UPSERT: string = 'upsert';
  public readonly METHOD_DELETE: string = 'delete';
  public readonly IGNORE_LOCAL_RECORDS_ON_REMOVE_NON_ACTUAL_RECORDS: boolean = false;

  fetchRemoteObjectsFromCache;

  init() {
    this.pageSize = 10000;
    this.loadBatchSize = 10000;
  }
  
  get cache() {
    const isGlobalStore = StoreConfig.getConfig().isGlobalStore;
    const storeName = StoreConfig.getConfig().storeName;
    
    return new Force.StoreCache(this.scheme.table, this.scheme.indexSpec, 'Id', isGlobalStore, storeName);
  }

  // ### SERVER ###

  configForSync() {
    return this._mapFieldsList()
      .then((fields) => {
        const soql = `SELECT ${fields.join(',')} FROM ${this.scheme.sfdcTable}`;

        const config = {
          type: 'soql',
          query: soql,
          closeCursorImmediate: false,
          pageSize: 1000
        };

        return this.prepareServerConfig(Promise.resolve(config));
      });
  }

  serverConfig(onlyIds = false, loadDynamicLayout = false) {
    return onlyIds ? this.configForCheckPresence() : this.configForSync();
  }

  configForCheckPresence() {
    const config = this.getConfigWithSoql(`SELECT ${this.scheme.fields.id.sfdc} FROM ${this.scheme.sfdcTable}`);

    return this.prepareServerConfig(Promise.resolve(config));
  }

  getConfigWithSoql(soql) {
    return {
      type: 'soql',
      closeCursorImmediate: false,
      query: soql,
      pageSize: 1000
    };
  }


  abstract prepareServerConfig(configPromise, lastSyncTime?);

  _mapFieldsList() {
    return this._getConfigsFields()
      .then((configs) => {
        return _.chain(this.scheme.sfdcFields)
          .map((field) => {
            if (this._shouldExcludeFieldForConfigs(field, configs)) {
              return null;
            } else if (this._hasFieldIncludeSettings(field)) {
              return this._includedFieldForConfigs(field, configs);
            } else if (this.scheme.isToLabel[field]) {
              return `toLabel(${field})`;
            } else {
              return field;
            }
          })
          .compact()
          .value();
      });
  }

  _getConfigsFields() {
    return Promise.all([
      SettingsManager.getSettings(),
      ConfigurationManager.getConfig()
    ])
    .then(([settings, configurations]) => {
      return _.extend({}, settings, configurations);
    });
  }


  _shouldExcludeFieldForConfigs(field, configs) {
    const fields = _.keys(this.scheme.excludableFields);

    if (_.includes(fields, field)) {
      return !configs[this.scheme.excludableFields[field]];
    }

    return false;
  }


  _hasFieldIncludeSettings(field) {
    const fields = _.keys(this.scheme.includableFields);

    return _.includes(fields, field);
  }


  _includedFieldForConfigs(field, configs) {
    return configs[this.scheme.includableFields[field]] ? field : null;
  }


  _onDownloadingStarted() {
    return Promise.resolve(this.didStartDownloading.bind(this)());
  }


  didStartDownloading() {
    // Can be overridden to perform some action
  }


  _onDownloadingFinished(syncState) {
    return Promise.resolve(this.didFinishDownloading.bind(this)(syncState.records || []))
      .then((updatedRecords) => {
        return syncState.totalSize;
      });
  }


  abstract didFinishDownloading(records);


  _onUploadingStarted(entities) {
    return Promise.resolve(this.didStartUploading.bind(this)(entities));
  }


  didStartUploading(entities) {
    return entities;
    // Can be overridden to perform some action but should return (un-)modified entities to be uploaded !
  }


  _onUploadingFinished(records) {
    return Promise.resolve(() => this.didFinishUploading(records))
      .then(() => records);
  }


  didFinishUploading(records) {
    // Can be overridden to perform some action
  }

  _upsertEntitiesToServer(options) {
    const shouldIgnoreDeleted = false;
    const query = new Query()
      .selectFrom(this.scheme.table)
      .where({__local__: true, __locally_created__: true, __locally_updated__: true, __locally_deleted__: true}, Query.EQ, Query.OR);

    return this.fetchWithQuery(query, shouldIgnoreDeleted)
      .then(this.getAllEntitiesFromResponse.bind(this))
      .then((entities) => this._onUploadingStarted(entities))
      .then((entities) => this._upsertEntitiesToServerRecursively(entities, options.each))
      .then(options.success.bind(options), options.error.bind(options));
  }


  _upsertEntitiesToServerRecursively(entities, eachStep) {
    const syncedEntities = [];

    console.log("upload record entities",entities.length);
    if (!entities.length) {
      return Promise.resolve(syncedEntities);
    }

    const recursion: any = (entities, index) => {
      if (index == entities.length) {
        console.log("upload record syncedEntities",syncedEntities);
        return Promise.resolve(syncedEntities);
      }

      return this.upsertEntityToServer(entities[index])
        .then(([entity, errorObj]) => {
          if (!errorObj) {
            console.log("entities push",entity);
            syncedEntities.push(entity);
            return recursion(entities, index + 1);
          } else {
            const callback = () => {
              entities = _.without(entities, entity);
              return recursion(entities, index);
            };
            if (Array.isArray(errorObj)) {
              errorObj = _.first(errorObj);
            }
            return this._handleEntityUploadingErrorStep(entities[index], errorObj, eachStep)
            .then(callback, callback);
          }
        });
    };

    return recursion(entities, 0);
  }


  _handleEntityUploadingErrorStep(entity, errorObj, step) {
    let error;

    if (errorObj && errorObj.responseText && errorObj.details) {
      error = this._errorFromResponse(errorObj);
    } else {
      error = _.extend(errorObj, {details: ((errorObj && errorObj.responseText) || {}), responseText: (errorObj && errorObj.responseText || errorObj.message)});
    }

    return Promise.resolve(this.handleErrorForEntity.bind(this)(error, entity))
      .then(() => {
        const errorCode = error.errorCode;

        if (!_.includes(this.ignoreErrorCodes(), errorCode)) {
          if (_.includes(this.warningErrorCodes(), errorCode)) {
            error.type = EntitiesCollection.TYPE_WARNING;
          }

          if (step) {
            step(entity, error);
          }
        }
      });
  }


  _errorFromResponse(response) {
    let error = response;
    let errors;

    if (!_.isObject(response)) {
      errors = JSON.parse(response);
      error = _.isArray(errors) ? _.first(errors) : errors;
    }

    if (_.isObject(error.responseText)) {
      error.responseText = JSON.stringify(error.responseText);
    }

    return new Force.Error(error);
  }


  handleErrorForEntity(error, entity) {
    if (error && error.details && error.details.errorCode === 'ENTITY_IS_DELETED') {
      return this.removeEntity(entity);
    }
  }

  ignoreErrorCodes() {
    return ['ENTITY_IS_DELETED', 'ENTITY_IS_LOCKED'];
  }

  warningErrorCodes() {
    return [];
  }

  _updateClmToolId(entity, isForce?) {
    if ((!entity.clmToolId) || isForce) {
      // this.cache.isLocal(entity.Id || entity.id);
      if (entity.Id) {
        entity['CLM_Tool_Id__c'] = entity.Id + Math.random() + (new Date()).getTime();
        console.log('GENERATED new CLM tool Id:', entity['CLM_Tool_Id__c']);
        return this.updateEntity(entity, false, false);
      }

      entity.clmToolId = entity.id + Math.random() + (new Date()).getTime();
      console.log('GENERATED new CLM tool Id:', entity.clmToolId);

      return this.updateEntity(entity, false, true);
    } else {
      return Promise.resolve(entity);
    }
  }

  _uploadEntity(method, entity, options) {
    let forceSObject = new Force.SObject();

    forceSObject.sobjectType = entity.sobjectType;

    const id: string = entity.id || entity.Id;

    const entityToUpload = {
      id: id,
      attributes: entity
    };

    return forceSObject.sync(method, entityToUpload, options);
    // return entity.sync(method, entity, options);
  }


  _checkExistingEntityOnServer(method, entity, options) {
    if ((~options.fieldlist.indexOf('CLM_Tool_Id__c')) && (method !== this.METHOD_DELETE)) {
      return this._updateClmToolId(entity)
        .then((records) => {
          let record = _.first(records);
          
          const onSuccess = (response) => {
            if (response.totalSize) {
              record.id = response.records[0][this.scheme.id.sfdc];
              record.attributes = _.extend(record.attributes, {
                __local__: false,
                __locally_created__: false,
                __locally_updated__: false,
                __locally_deleted__: false
              });
              return options.success(record);
            } else {
              return this._uploadEntity(method, record, options);
            }
          };

          const query = `SELECT Id FROM ${this.scheme.sfdcTable} WHERE ${this.scheme.fields.clmToolId.sfdc} = '${record.clmToolId || record.CLM_Tool_Id__c}'`;

          return getForceSyncClient()
            .jsClient
            .query(query)
            .then(onSuccess, options.error);
        });
    }else{
      return this._uploadEntity(method, entity, options);
    }
  }


  _beforeCheckExistingEntityOnServer(entity, method) {
    if (method === this.METHOD_UPDATE) {
      return this._updateClmToolId(entity, true);
    }

    return Promise.resolve(true);
  }

  upsertEntityToServer(entityParsed) {
    const entity = this.convertLocalEntityToSfdc(entityParsed);
    console.log("entity",entity);
    return new Promise((resolve, reject) => {
      const method = this._syncMethodForEntity(entity);
      const options: any = {};

      options.cache = null;
      options.cacheForOriginals = null;
      options.cacheMode = getForceSyncClient().CACHE_MODE.SERVER_ONLY;
      options.mergeMode = getForceSyncClient().MERGE_MODE.OVERWRITE;
      options.fieldlist = this.scheme.uploadableFields;

      options.success = (record) => {
        return this.getActionPromise(record, method, entity)
          .then((record) => {
            resolve([record, null]);
          });
      };

      options.error = (jqXHR, textStatus, errorThrown) => {
        resolve([entity, jqXHR]);
      };

      return this._beforeCheckExistingEntityOnServer(entity, method)
        .then(() => this._checkExistingEntityOnServer(method, entity, options));
    });
  }



  private getActionPromise(record, method, oldEntity) {
    console.log("record",record);
    switch (method) {
      case this.METHOD_CREATE: {
        const notModifiedEntities = this.markEntityAsNotModified(record);

        return this.updateEntityWithoutOptions(notModifiedEntities);
      }
      case this.METHOD_UPDATE: {
        const notModifiedEntities = this.markEntityAsNotModified(record);

        return this.updateEntityWithoutOptions(notModifiedEntities);
      }
      case this.METHOD_DELETE:
        return this.removeEntity(oldEntity, false);
    }
  }


  _syncMethodForEntity(entity) {
    const attributes = entity;

    if (attributes.__locally_deleted__) {
      return this.METHOD_DELETE;
    } else if (attributes.__locally_created__) {
      return this.METHOD_CREATE;
    } else if (attributes.__locally_updated__ && !attributes.__locally_deleted__) {
      return this.METHOD_UPDATE;
    }
  }


  fetchRemoteObjectsFromServer(config) {
    return getForceSyncClient().fetchSObjectsFromServer(config)
      .then(this._loadNextBatchRecursively.bind(this));
  }


  _loadNextBatchRecursively(response) {
    if (response.hasMore()) {
      return response.getMore()
        .then(() => this._loadNextBatchRecursively(response));
    }

    return response.closeCursor()
      .then(() => response);
  }


/*
  CACHE
*/

  mapMoreUnparsedEntitiesFromResponse(response, step) {
    if (!step) {
      step = (response, records) => {
        return response;
      };
    }

    if (!response.hasMore()) {
      return Promise.resolve(response);
    }

    return this._convertResponseFunctionsToJQueryPromise(response)
      .getMore()
      .then((records) => step(response, records));
  }


  getMoreUnparsedEntitiesFromResponse(response) {
    return this.mapMoreUnparsedEntitiesFromResponse(response, null);
  }

  getMoreEntitiesFromResponse(response) {
    this.mapMoreUnparsedEntitiesFromResponse(response, this._responseWithParsedRecords);
  }

  mapAllUnparsedEntitiesFromResponse(response, step) {
    if (!step) {
      step = (response, records) => response;
    }

    const recursion = (response, step) => {
      if (!response.hasMore()) {
        return response.records;
      } else {
        return response //this._convertResponseFunctionsToJQueryPromise(response)
          .getMore()
          .then((records) => step(response, records))
          .then(() => recursion(response, step))
      }
    };

    return Promise.resolve(step(response, response.records))
      .then(() => recursion(response, step));
  }


  _convertResponseFunctionsToJQueryPromise(response) {
    const nativeToJQuery = (nativePromise) => {
      return new Promise((resolve, reject) => {
        nativePromise.then(resolve, reject);
      });
    };

    const getMore = () => {
      return Promise.resolve(response.getMore())
        .then(nativeToJQuery);
    };

    const closeCursor = () => {
      return Promise.resolve(response.closeCursor())
        .then(nativeToJQuery);
    };

    return {
      records: response.records,
      hasMore: response.hasMore,
      getMore: getMore,
      closeCursor: closeCursor
    };
  }


  getAllUnparsedEntitiesFromResponse(response) {
    return this.mapAllUnparsedEntitiesFromResponse(response, null);
  }


  getAllEntitiesFromResponse(response) {
    return this.mapAllUnparsedEntitiesFromResponse(response, this._responseWithParsedRecords.bind(this));
  }


  _responseWithParsedRecords(response, records) {
    records = this.parse(records);
    length = response.records.length;

    response.records.splice.apply(response.records, [length - records.length, records.length].concat(records));

    return response;
  }


  parse(records, options = {}) {
    return records.map((result) => this.parseEntity(result, options));
  }

  //  TODO: extract 'parseEntity' into 'Entity' class as 'parse' method (may be class method)

  parseEntity(record, options = {}) {
    if (this._isEntityParsed(record)) {
      return record;
    }

    const sobject: any = this.parseModel(record);
    sobject.sobjectType = this.scheme.sfdcTable;

    return sobject;
  }

  parseModel(result) {
    return new this.model(result);
  }

  fetchUnparsedWithQuery(query, ignoreDeleted = true) {
    const smartSql = this._createSmartSqlQuery(query, this.scheme.table, this.pageSize);

    return this.fetchRemoteObjectsFromCache(this.cache, smartSql, ignoreDeleted);
  }

  fetchWithQuery(query, ignoreDeleted = true) {
    return this.fetchUnparsedWithQuery(query, ignoreDeleted)
      .then((response) => {
        response.records = this.parse(response.records);
        return response;
      });
  }

  _createSmartSqlQuery(query, soup, pageSize) {
    query = query.toString();

    return {
      queryType: 'smart',
      soupName: soup,
      smartSql: query,
      pageSize: pageSize
    };
  }


  fetchEntityById(id) {
    const idFieldValue = {};
    const orIdFieldValue: any = {};

    idFieldValue[this.scheme.fields.id.sfdc] = id;
    orIdFieldValue._soupEntryId = id;

    const query = new Query()
      .selectFrom(this.scheme.table)
      .where(idFieldValue)
      .or()
      .where(orIdFieldValue);

    return this.fetchWithQuery(query)
      .then((response) => {
        const record = this.getEntityFromResponse(response);
        response.closeCursor();

        return record;
      });
  }

  getEntityFromResponse(response) {
    return _.first(response.records) || null;
  }

  _fetchAllQuery() {
    return new Query().selectFrom(this.scheme.table);
  }

  fetchAll() {
    const query = this._fetchAllQuery();

    return this.fetchWithQuery(query);
  }

  getCountQuery() {
    return new Query().selectCountFrom(this.scheme.table);
  }

  getCount() {
    const query = this.getCountQuery();
    return this.fetchUnparsedWithQuery(query)
      .then(this.getEntityFromResponse.bind(this));
  }


  fetchAllSortedBy(fields, isAsc) {
    const order = isAsc ? Query.ASC : Query.DESC;
    const query = this._fetchAllQuery().orderBy(fields, order);

    return this.fetchWithQuery(query);
  }


  fetchAllWhere(fieldsValues) {
    const query = this._fetchAllQuery().where(fieldsValues)
    return this.fetchWithQuery(query);
  }

  fetchAllWhereIn(field, values) {
    const query = this._fetchAllQuery().whereIn(field, values);
    return this.fetchWithQuery(query);
  }

  fetchAllLike(fieldsValues) {
    const query = this._fetchAllQuery().whereLike(fieldsValues);
    return this.fetchWithQuery(query);
  }

  fetchAllWhereLike(whereFieldsValues, likeFieldsValues) {
    const query = this._fetchAllQuery().where(whereFieldsValues).and().whereLike(likeFieldsValues);
    return this.fetchWithQuery(query);
  }

  fetchAllWhereAndSortBy(fieldsValues, sortFields, isAsc) {
    const order = isAsc ? Query.ASC : Query.DESC;
    const query = this._fetchAllQuery().where(fieldsValues).orderBy(sortFields, order);
    return this.fetchWithQuery(query);
  }


  fetchAllLikeAndSortBy(fieldsValues, sortFields, isAsc) {
    const order = isAsc ? Query.ASC : Query.DESC;
    const query = this._fetchAllQuery().whereLike(fieldsValues).orderBy(sortFields, order);
    return this.fetchWithQuery(query);
  }


  fetchAllWhereLikeAndSortBy(whereFieldsValues, likeFieldsValues, sortFields, isAsc) {
    const order = isAsc ? Query.ASC : Query.DESC;
    const query = this._fetchAllQuery().where(whereFieldsValues).and().whereLike(likeFieldsValues).orderBy(sortFields, order);
    return this.fetchWithQuery(query);
  }


  upsertEntitiesSilently(entities, keepUnparsed = false) {
    return this._recursiveForEachBy(entities, this.pageSize, (entitiesBatch) => {
      entitiesBatch = entitiesBatch.map((entity) => this.updateSearchableForEntity(entity, keepUnparsed));
  
      return getForceSyncClient().smartstoreClient
        .upsertSoupEntries(StoreConfig.getConfig(), this.scheme.table, entitiesBatch.map(this._attributesFromEntity.bind(this)));
    });
  }

  _recursiveForEachBy(collection, stepBy, callback, index = 0) {
    if (index >= collection.length)
      return Promise.resolve();

    const nextIndex = index + stepBy;

    return Promise.resolve(callback(collection.slice(index, nextIndex)))
      .then(() => this._recursiveForEachBy(collection, stepBy, callback, nextIndex));
  }


  updateSearchableForEntity(entity, keepUnparsed = false) {
    if (!this.scheme.hasSearchable()) {
      return entity;
    }

    entity = keepUnparsed ? entity : this.parseEntity(entity);

    entity.searchData = this.scheme.searchableSchema()
      .map((field) => keepUnparsed ? field.sfdc : field.local)
      .filter((field) => !!entity[field])
      .map((field) => entity[field])
      .join(' ');

    entity.searchData = entity.searchData.toLowerCase();

    return entity;
  }

  createEntity(entity, localAction = true) {
    const convertedEntity = this.convertLocalEntityToSfdc(entity);
    const attributes = this.updateSearchableForEntity(convertedEntity, true);

    const markedAttributes = localAction ? this.markEntityAsCreate(attributes) : attributes;

    return getForceSyncClient()
      .syncRemoteObjectWithCache(this.METHOD_CREATE, markedAttributes[this.scheme.fields.id.sfdc], markedAttributes, null, this.cache, localAction);
  }


  updateEntity(entity, localAction = true, parseEntity = true) {
    const convertedEntity = parseEntity ? this.convertLocalEntityToSfdc(entity) : entity;
    const attributes = this.updateSearchableForEntity(convertedEntity, true);

    const markedAttributes = localAction ? this.markEntityAsUpdate(attributes) : attributes;

    return new Promise((resolve, reject) => {
      return getForceSyncClient().smartstoreClient
        .upsertSoupEntriesWithExternalId(StoreConfig.getConfig(), this.scheme.table, [markedAttributes], '_soupEntryId', resolve, reject);
    });
  }

  public markEntityAsCreate(entity) {
    const local = {
      __local__: true,
      __locally_created__: true,
      __locally_updated__: false,
      __locally_deleted__: false
    };

    return _.extend({}, entity, local);
  }

  public markEntityAsUpdate(entity) {
    const local = {
      __local__: true,
      __locally_created__: this.cache.isLocalId(entity.id || entity.Id),
      __locally_updated__: true,
      __locally_deleted__: false
    };

    return _.extend({}, entity, local);
  }

  public markEntityAsNotModified(entity) {
    const nonModifiedValues = {
      __local__: false,
      __locally_created__: false,
      __locally_updated__: false,
      __locally_deleted__: false
    };

    return _.extend({}, entity, nonModifiedValues);
  }

  updateEntityWithoutOptions(newEntity) {
    return new Promise((resolve, reject) => {
      return getForceSyncClient().smartstoreClient
        .upsertSoupEntriesWithExternalId(StoreConfig.getConfig(), this.scheme.table, [newEntity], '_soupEntryId', resolve, reject);
    });
  }

  removeEntity(entity, localAction = true) {
    return getForceSyncClient()
      .syncRemoteObjectWithCache(this.METHOD_DELETE, entity.id || entity.Id, null, null, this.cache, localAction);
  }

  removeEntities(entities) {
    return getForceSyncClient().smartstoreClient
      .removeFromSoup(StoreConfig.getConfig(), this.scheme.table, entities.map((entity) => this._attributesFromEntity(entity)._soupEntryId));
  }


  _attributesFromEntity(entity) {
    const attributes = this._isEntityParsed(entity) ? entity.attributes : entity;
    // this._convertJoinedFieldsToLocal([attributes]);
    this._convertNumberTypeToString(attributes);
    return attributes;
  }


  _convertNumberTypeToString(attributes) {
    const fieldsName = Object.keys(attributes);

    fieldsName.forEach((fieldName) => {
      if (typeof attributes[fieldName] === 'number') {
        // durty hack cuz cant fetch records from local database by value with type number if it indexed as string
        attributes[fieldName] = attributes[fieldName].toString();
      }
    });
  }

  _isEntityParsed(entity) {
    if (entity) {
      return entity.hasOwnProperty('cid');
    }
  }


  runCountQuery(query) {
    const querySpec = navigator.smartstore.buildSmartQuerySpec(query.toString(), 1);
    const storeConfig = Utils.extend({soupName: this.scheme.table}, this.cache.storeConfig);

    return getForceSyncClient().smartstoreClient.runSmartQuery(storeConfig, querySpec)
      .then((count) => _.chain(count.currentPageOrderedEntries).flatten().first().value());
  }

  sync(method, model, options) {
    if (!options) {
      options = {};
    }

    if (method === this.METHOD_UPSERT) {
      return new Promise((resolve, reject) => {
        options.success = (records) => {
          return this._onUploadingFinished(records)
            .then(resolve);
        };
        options.error = ((error) => reject(error));

        this._upsertEntitiesToServer(options);
      });
    } else {
      return this._onDownloadingStarted()
      .then(() => {
        return this.syncDown(options)
          .then(this._onSyncDownFinished.bind(this));
      });
    }
  }

  syncDown(options) {
    return Force.fetchSObjectsFromServer(options.config, true)
      .then((response) => {
        return this._downloadRecordsRecursively(response, options);
      });
  }

  _downloadRecordsRecursively(response, options) {
    const records = response.records.map(_.clone);

    return this.saveRecordToCache(options.cache, records)
      .then((records) => Promise.resolve(this.didFinishDownloading.bind(this)(records)))
      .then(() => {

        if (response.hasMore()) {
          return response.getMore()
            .then(() => {
              return this._downloadRecordsRecursively(response, options);
            });
        }
        return response.closeCursor().then(() => {
          return Promise.resolve(() => this.didFinishAllRecordsDownload());
        })
          .then(() => response);
      });
  }
  
  saveRecordToCache(cache, records){
    return cache.saveAll(records || []);
  }

  fetchAllLocalWhereIn(recordsIds) {
    return Promise.resolve(this._fetchAllQuery.bind(this)())
      .then((query) => {
        query
          .where({__local__: true})
          .and()
          .whereIn(this.scheme.fields.id.sfdc, recordsIds);

        return this.fetchWithQuery(query)
          .then(this.getAllEntitiesFromResponse.bind(this));
      });
  }

  didFinishAllRecordsDownload() {
    console.log('Could be overwritten');
  }

  _onSyncDownFinished(syncState) {
    return this._fetchActualRecordsFromSF()
      .then(this._mapIdsFromRecords.bind(this))
      .then((actualSfRecordsId) => {
        return this._removeNonActualRecords(actualSfRecordsId)
          .then(() => {
            return this._fetchIdsOfActualEntities(actualSfRecordsId)
              .then((newRecordsIds) => {
                if (!syncState.totalSize) {
                  syncState.totalSize = 0;
                }

                syncState.totalSize += newRecordsIds.length;

                return this._downloadNewRecords(newRecordsIds);
              })
              .then(() => {
                    return this._onDownloadingFinished(syncState);
              });
          });
      });
  }

  _fetchActualRecordsFromSF() {
    return this.serverConfig(true)
        .then(Force.fetchSObjectsFromServer.bind(Force))
        .then(this._loadNextBatchRecursively.bind(this))
        .then((response) => response.records || []);
  }

  _mapIdsFromRecords(records, field = this.scheme.fields.id.sfdc) {
    return records.map((record) => record[field]);
  }

  _removeNonActualRecords(actualRecordsId) {
    const query = new Query();

    query.selectFrom(this.scheme.table);

    if (!this.IGNORE_LOCAL_RECORDS_ON_REMOVE_NON_ACTUAL_RECORDS) {
      query.where({ __local__: false }).and();
    }

    query.whereNotIn(this.scheme.fields.id.sfdc, actualRecordsId);

    return this.fetchUnparsedWithQuery(query)
      .then(this.getAllUnparsedEntitiesFromResponse.bind(this))
      .then(this.removeEntities.bind(this));
  }

  _fetchIdsOfActualEntities(actualSfRecordsIds = []) {
    const maxValue = 99999999;
    const allLocalQuery = new Query().selectFrom(this.scheme.table, [this.scheme.fields.id.sfdc]).where({__locally_created__: false});
    const smartSql = this._createSmartSqlQuery(allLocalQuery, this.scheme.table, maxValue);

    return this.fetchRemoteObjectsFromCache(this.cache, smartSql, true)
    .then((response) => {
      const actualLocalIds = (response && response.records) || [];
      const newRecords = _.difference(actualSfRecordsIds, actualLocalIds);
      return newRecords;
    });
  }

  _downloadNewRecords(newRecords) {
    const CHUNK_SIZE = 100;

    if (newRecords.length === 0) {
      return;
    }

    return this._mapFieldsList()
    .then ((fields) => {
      const newRecordsChunks = Utils.chunkArray(newRecords, CHUNK_SIZE);

      return this._recursiveDownloadRecordsArray(newRecordsChunks, fields);
    });
  }

  _recursiveDownloadRecordsArray(newRecordsChunks = [], fields) {
    const newRecords = newRecordsChunks.pop();

    if (newRecords) {
      const idsArray = newRecords.map((id) => `'${id}'`).join(',');

      const config = this.getConfigWithSoql(`SELECT ${fields.join(',')} FROM ${this.scheme.sfdcTable} WHERE Id IN (${idsArray})`);
      return this.syncDown({ config: config, cache: this.cache })
        .then(() => {
          return  this._recursiveDownloadRecordsArray(newRecordsChunks, fields);
        });
    }

    return Promise.resolve(true);
  }

  convertLocalEntityToSfdc(entity) {
    const originalEntity = {};

    Object.keys(entity).forEach((localKey) => {
      const field = this.scheme.fields[localKey];

      if (field) {
        originalEntity[field.sfdc || field.local] = entity[localKey];
      } else if (~Entity.DEFAULT_FIELDS.indexOf(localKey)) {
        originalEntity[localKey] = entity[localKey];
      }
    });

    return originalEntity;
  }
  
  linkEntitiesToEntity(entities, field, applyField = 'id') {
    const soupEntryIds = _.uniq(entities.map((entity) => entity[field]));
    
    return this.fetchAllWhereIn("_soupEntryId", soupEntryIds)
      .then(this.getAllEntitiesFromResponse.bind(this))
      .then((records) => {
        const entitiesMap = {};
        
        records.forEach((record) => {
          entitiesMap[record._soupEntryId] = {
            id: record.id,
            applyField: record[applyField]
          };
        });
        
        entities = entities.map((entity) => {
          if(!entity.__locally_deleted__) {
            if(entitiesMap[entity[field]] && entitiesMap[entity[field]].id.indexOf("local") == -1) {
              entity[field] = entitiesMap[entity[field]].applyField;
            }
          }
          
          return entity;
        });
        
        return entities;
      })
  }
}
