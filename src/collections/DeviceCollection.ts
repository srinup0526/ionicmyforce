import {DeviceManager} from '../services/common/DeviceManager';
import {LogManager} from '../services/common/LogManager';
import {Utils} from '../utils/Utils';
import {Device} from './../models/Device';
import {EntitiesCollection} from './EntitiesCollection';
import {Entity} from './../models/base/Entity';
import {DeviceScheme} from '../models/scheme/DeviceScheme';
import {Injectable} from "@angular/core";

declare let Force;

@Injectable()
export class DeviceCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = Device;
    this.scheme = DeviceScheme;
    this.init();
  }

  prepareServerConfig(configPromise, lastSyncTime) {
    return configPromise
      .then((config) => {
        const deviceId = DeviceManager.deviceId();

        config.query += ` where ${this.scheme.fields.deviceId.sfdc} = '${deviceId}'`;

        if (lastSyncTime) {
          config.query += ` AND ${this.scheme.fields.lastModifiedDate.sfdc} > ${lastSyncTime} `;
        }

        return config;
      });
  }


  private fillDeviceData(device) {
    device.deviceId = DeviceManager.deviceId();
    device.lastSyncronisation = Utils.originalDateTime(new Date());
    device.lastUserSfid = Force.userId;
    device.model = DeviceManager.deviceModel();
    device.osVersion = DeviceManager.osVersion();
    device.version = DeviceManager.appVersion();
    device.lastDebugLog = LogManager.log;
    device.erased = false;

    return Promise.resolve(device);
  }

  registerDevice() {
    return this.fillDeviceData({})
      .then((data) => {
        const device = new Device(data);

        return this.createEntity(device);
      });
  }

  updateDevice(device) {
    return this.fillDeviceData(device)
      .then(this.updateEntity.bind(this));
  }

  getDevice() {
    const deviceId = DeviceManager.deviceId();
    const whereQuery = {};
    whereQuery[this.scheme.fields.deviceId.sfdc] = deviceId;

    return this.fetchAllWhere(whereQuery)
      .then((response) => {
        const device = response.records.length ? this.parseEntity(response.records[0]) : null;
        return Promise.resolve(device);
    });
  }

  updateDeviceInfo() {
    return this.getDevice().then(device => {
      if (!device) {
        return this.registerDevice();
      } else {
        return this.updateDevice(device);
      }
    });
  }

  didFinishDownloading(records) {
    return records;
  }
}
