import { EntitiesCollection } from './EntitiesCollection';
import { CoachingPlan } from './../models/CoachingPlan';
import { CoachingPlanScheme } from './../models/scheme/CoachingPlanScheme';
import { Query } from '../services/common/Query';
import { ReferenceCollection } from './references/ReferenceCollection';
import {ConfigurationManager} from './../services/db/ConfigurationManager';
import {Injectable} from "@angular/core";
import {PicklistManager} from './../services/db/picklist-managers/base/PicklistManagers';
import {StoreConfig} from "./../services/synchronisation/StoreConfig";
declare function getForceSyncClient();

@Injectable({
  providedIn: 'root'
})
export class CoachingPlanCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    super();
    this.model = CoachingPlan;
    this.scheme = CoachingPlanScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;//.then((config) => {
        // return ConfigurationManager.getConfig('brickRecordTypeId')
        //     .then((brickRecordTypeId) => {
        //       config.query += ` WHERE RecordTypeId = '${brickRecordTypeId}'`;
        //       return config;
        //     });
      //   return config;
      // });
  }

  public preparePicklists() {
    PicklistManager.clearCache(this.scheme.sfdcTable);
    
    return PicklistManager.initSoup()
      .then(this.fetchUniqueDesignation.bind(this))
      .then((picklistObject) => {
      console.log("picklistObject",picklistObject);
        let desigObject = {
          fieldName: this.scheme.fields.designation.sfdc,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.fields.designation.sfdc}`,
          picklistOptions: picklistObject.designation.slice(0)
        };
      console.log("desigObject",desigObject);
        
        return getForceSyncClient().smartstoreClient.upsertSoupEntriesWithExternalId(
          StoreConfig.getConfig(),
          PicklistManager.soupName,
          [desigObject],
          PicklistManager.identityKey
        );
      });
  }

  private fetchUniqueDesignation() {
    let desigArray = [];
    return this.distinctQuery(this.scheme.fields.designation.sfdc)
    .then((response) => {
      desigArray = this.mapPicklistObjectsArray(response.records);
      return {
        designation: desigArray
      }
    })
      
  }

  private distinctQuery(fieldName) {
    console.log("fieldName",fieldName);
    const  maxValue = 99999999;
    const query = new Query()
      .customQuery(`SELECT DISTINCT {${this.scheme.table}:${fieldName}} FROM {${this.scheme.table}} WHERE {${this.scheme.table}:${fieldName}} != 'null'`);
    
    const smartSql = this._createSmartSqlQuery(query, this.scheme.table, maxValue);
    
    return this.fetchRemoteObjectsFromCache(this.cache, smartSql, false);
  }

  private mapPicklistObjectsArray(records) {
    console.log("records at mapping",records);
    return records
      .filter((record) => record)
      .map((record) => ({label: record, value: record}))
  }

  didFinishDownloading(records) {
    return records;
  }
}
