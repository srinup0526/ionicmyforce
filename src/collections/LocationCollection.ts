import { EntitiesCollection } from './EntitiesCollection';
import { Location } from './../models/Location';
import { LocationScheme } from './../models/scheme/LocationScheme';
import { Query } from '../services/common/Query';


export class LocationCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = Location;
    this.scheme = LocationScheme;
    this.init();
  }

  didStartUploading(entities){
    // CallReportsCollection = require 'models/bll/call-reports-collection/call-reports-collection'
    // callReportsCollection = new CallReportsCollection()
    // callReportsCollection.linkEntitiesToEntity entities, 'callReport'
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }

  // createLocationPoint: (callReportId, pointType) =>
  //   locationJson = GeolocationManager.getLastNearbyResult()
  //   location = {}
  //   location[Location.sfdc.callReport] = callReportId
  //   location[Location.sfdc.datetime] = locationJson.time
  //   location[Location.sfdc.gpsStatus] = locationJson.state
  //   location[Location.sfdc.latitude] = locationJson.position?.latitude
  //   location[Location.sfdc.longitude] = locationJson.position?.longitude
  //   location[Location.sfdc.type] = pointType
  //   location[Location.sfdc.result] = locationJson.result
  //   location['attributes'] = {type: Location.table}
  //   location

  // saveLocationPoint: (locationPoint) =>
  //   whereFields = {}
  //   whereFields[Location.sfdc.callReport] = locationPoint[Location.sfdc.callReport]
  //   whereFields[Location.sfdc.type] = locationPoint[Location.sfdc.type]
  //   @fetchAllWhere(whereFields)
  //   .then @getEntityFromResponse
  //   .then (existingLocationPoint) =>
  //     return if existingLocationPoint
  //     @createEntity(locationPoint)

  // removeRelatedToCallReport: (callReportIds)=>
  //   @fetchAllWhereIn Location.sfdc.callReport, callReportIds
  //   .then @getAllEntitiesFromResponse
  //   .then @removeEntities
    
}
