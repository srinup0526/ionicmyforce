import {EntitiesCollection} from './EntitiesCollection';
import {OrganizationScheme} from '../models/scheme/OrganizationScheme';
import {Organization} from './../models/Organization';
import {ConfigurationManager} from './../services/db/ConfigurationManager';
import {PicklistManager} from './../services/db/picklist-managers/base/PicklistManagers';
import {Query} from "./../services/common/Query";
import {Injectable} from "@angular/core";
import {StoreConfig} from "../services/synchronisation/StoreConfig";

declare function getForceSyncClient();


@Injectable({
  providedIn: 'root'
})
export class OrganizationsCollection extends EntitiesCollection {
  public model;
  
  constructor() {
    super();
    this.model = Organization;
    this.scheme = OrganizationScheme;
    this.init();
  }
  
  prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
      return ConfigurationManager.getConfig('brickRecordTypeId')
        .then((brickRecordTypeId) => {
          config.query += ` WHERE ${this.scheme.fields.recordTypeId.sfdc} <> '${brickRecordTypeId}'`;
          return config;
        });
    });
  }
  
  parseModel(result) {
    if (result.RecordType) {
      result[this.scheme.fields.recordTypeId.sfdc] = result.RecordType.Id;
      result[this.scheme.fields.remoteRecordType.sfdc] = result.RecordType.Name;
    }
    
    result[this.scheme.fields.city.sfdc] = result.BillingCity || '';
    result[this.scheme.fields.address.sfdc] = result.BillingStreet || '';
    result[this.scheme.fields.state.sfdc] = result.BillingState || '';
    result[this.scheme.fields.country.sfdc] = result.BillingCountry || '';
    result[this.scheme.fields.postalCode.sfdc] = result.BillingPostalCode || '';
    
    return super.parseModel(result);
  }
  
  _fetchAllQuery() {
    const fieldValue = {};
    
    fieldValue[this.scheme.fields.isPersonAccount.sfdc] = false;
    
    return super._fetchAllQuery().where(fieldValue);
  }
  
  _distinctQuery(fieldName) {
    const maxValue = 99999999;
    const query = new Query().customQuery(`SELECT DISTINCT {${this.scheme.table}:${fieldName}} FROM {${this.scheme.table}} WHERE {${this.scheme.table}:${fieldName}} != 'null' AND {${this.scheme.table}:${this.scheme.fields.isPersonAccount.sfdc}} = ` + Query.FALSE);
    const smartSql = this._createSmartSqlQuery(query, this.scheme.table, maxValue);
    console.log("query");
    console.log(query);
    console.log("smartSql");
    console.log(smartSql);
    
    return this.fetchRemoteObjectsFromCache(this.cache, smartSql, true);
  }
  
  _fetchUniqueSpecialtyPriority() {
    let recordTypes = [];
    let subtypes = [];
    
    return this._distinctQuery(this.scheme.RECORD_TYPE_FIELD_NAME)
      .then((response) => {
        console.log("response",response);
        recordTypes = this._mapPicklistObjectsArray(response.records);
        
        return this._distinctQuery(this.scheme.SUBTYPE_FIELD_NAME);
      })
      .then((response) => {
        subtypes = this._mapPicklistObjectsArray(response.records);
        
        return {
          subtypes: subtypes,
          recordTypes: recordTypes
        };
      });
  }
  
  _mapPicklistObjectsArray(records) {
    return records
      .filter((record) => record)
      .map((record) => ({
        "label": record,
        "value": record
      }));
  }
  
  
  fetchByOrgId(OrgId) {
    const whereFields = {};
    
    whereFields[this.scheme.fields.contactSfId.sfdc] = OrgId;
    
    return this.fetchAllWhere(whereFields)
      .then((response) => {
        return this.getAllEntitiesFromResponse(response);
      });
  }
  
  preparePicklists() {
    PicklistManager.clearCache(this.scheme.sfdcTable);
   
    return PicklistManager.initSoup()
      .then(this._fetchUniqueSpecialtyPriority.bind(this))
      .then((picklistObject) => {
        
        const recordTypeObject = {
          fieldName: this.scheme.RECORD_TYPE_FIELD_NAME,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.RECORD_TYPE_FIELD_NAME}`,
          picklistOptions: picklistObject.recordTypes.slice(0)
        };
        
        const subtypeObject = {
          fieldName: this.scheme.SUBTYPE_FIELD_NAME,
          objectFieldName: `${this.scheme.sfdcTable}:${this.scheme.SUBTYPE_FIELD_NAME}`,
          picklistOptions: picklistObject.subtypes.slice(0)
        };
        
        return getForceSyncClient().smartstoreClient.upsertSoupEntriesWithExternalId(StoreConfig.getConfig(), PicklistManager.soupName, [recordTypeObject, subtypeObject], PicklistManager.identityKey);
      })
  }
  
  getCountQuery(paramsString = "") {
    const query = new Query(this.scheme.table);
    
    query.customQuery(
      `SELECT COUNT(*) ` +
      `FROM {${this.scheme.table}} ` +
      `WHERE {${this.scheme.table}:${this.scheme.fields.isPersonAccount.sfdc}} = ` + Query.FALSE +
      `${paramsString}`
    );
    
    return query;
  }
  
  didFinishDownloading(records) {
    return records;
  }
  
  saveRecordToCache(cache, records) {
    const organizations = this.updateOrganizations(records);
  
    return super.saveRecordToCache(cache, organizations);
  }
  
  private updateOrganizations(organizations) {
    return organizations.map((organization) => {
      if(organization.RecordType) {
        organization.recordType = organization.RecordType.Name;
      }
      
      return organization;
    });
  }
}
