import { EntitiesCollection } from './EntitiesCollection';
import { OrdersCollection } from './OrdersCollection';
import { OrderLine } from './../models/OrderLine';
import { OrderLineScheme } from './../models/scheme/OrderLineScheme';
import { Query } from '../services/common/Query';
import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class OrdersLineCollection extends EntitiesCollection {
  public model;
  public scheme;
  
  constructor() {
    super();
    this.model = OrderLine;
    this.scheme = OrderLineScheme;
    this.init();
  }
  
  prepareServerConfig(configPromise) {
    return configPromise;
  }
  
  parseModel(result) {
    if(result.SKU_Name__r) {
      result[this.scheme.fields.productLot.sfdc] = result.SKU_Name__r.Name;
    }
    
    return super.parseModel(result);
  }
  
  
  didFinishDownloading(records) {
    return records;
  }
  
  didStartUploading(entities) {
    const ordersCollection: OrdersCollection = new OrdersCollection();
    
    return ordersCollection.linkEntitiesToEntity(entities, 'orderId');
  }
  
  _fetchAllQuery() {
    return new Query().selectFrom(this.scheme.table);
  }
  
  fetchByOrderId(OrderId: string, soupOrderId: string) {
    const query = this._fetchAllQuery();
    
    query.whereIn(this.scheme.fields.orderId.sfdc,[OrderId, soupOrderId]);
    
    return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this));
  }
  
  saveOrderLines(orderId, orderLines: Array<OrderLine>) {
    return Promise.all(orderLines.map((orderLine) => {

      orderLine.orderId = orderId;
  
      orderLine.quantity = orderLine.quantity.toString();
      orderLine.salesPrice = orderLine.salesPrice.toString();

      return orderLine.id ? this.updateEntity(orderLine) : this.createEntity(orderLine);
    }));
  }
  
  deleteLinesByIds(ids: Array<string>): Promise<any> {
    if(!ids.length) {
      return Promise.resolve();
    }
    
    const query = this._fetchAllQuery();
    
    query.whereIn(this.scheme.fields.id.sfdc,ids);
  
    return this.fetchWithQuery(query)
      .then(this.getAllEntitiesFromResponse.bind(this))
      .then((items) => {
        return Promise.all(items.map((item => {
          return this.removeEntity(item);
        })))
      })
  }
}
