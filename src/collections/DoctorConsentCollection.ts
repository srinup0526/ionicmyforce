import { EntitiesCollection } from './EntitiesCollection';
import { DoctorsConsent } from './../models/DoctorsConsent';
import { DoctorsConsentScheme } from './../models/scheme/DoctorsConsentScheme';
import { Query } from '../services/common/Query';


export class DoctorConsentCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = DoctorsConsent;
    this.scheme = DoctorsConsentScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  _fetchAllQuery(){
    const fieldValue = {}
    return new Query().selectFrom(this.scheme.table);
  }

  didFinishDownloading(records) {
    return records;
  }
}
