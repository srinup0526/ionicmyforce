import {RecordType} from "../models/RecordType";
import {EntitiesCollection} from './EntitiesCollection';
import {Injectable} from '@angular/core';
import {RecordTypeScheme} from "../models/scheme/RecordRypeScheme";


@Injectable({
	providedIn: 'root'
})
export class RecordTypeCollection extends EntitiesCollection {

	public model;
	public scheme;

	constructor() {
		super();
		this.model = RecordType;
		this.scheme = RecordTypeScheme;
		this.init();
	}

	public prepareServerConfig(configPromise) {
		return configPromise;
	}

	_fetchAllQuery() {
		const activeUser = {};

		activeUser[this.scheme.fields.isActive.sfdc] = true;

		return super._fetchAllQuery()
			.where(activeUser);
	}

	getRecordTypeId(developerName) {
		const query = this._fetchAllQuery();

		const fieldValue = {};

		fieldValue[this.scheme.developerName.sfdc] = developerName;
		fieldValue[this.scheme.objectType.sfdc] = this.scheme.orderObject;

		query.where(fieldValue);

		return this.fetchWithQuery(query)
			.then(this.getEntitiyFromResponse.bind(this));
	}

	getRecordTypesBysObjectTypeId(sObjectType) {
		const query = this._fetchAllQuery();

		const fieldValue = {};

		fieldValue[this.scheme.fields.objectType.sfdc] = sObjectType;
		query.where(fieldValue);
		return this.fetchWithQuery(query)
			.then(res=> { return this.getAllEntitiesFromResponse(res);});
	}

	_getRecordTypeId(developerName) {
		const fieldValue = {};

		fieldValue[this.scheme.developerName.sfdc] = developerName;
		fieldValue[this.scheme.objectType.sfdc] = this.scheme.sfdcTable;

		return super._getRecordTypeId()
			.where(fieldValue);
	}


	didFinishDownloading(records) {
		return records;
	}
}