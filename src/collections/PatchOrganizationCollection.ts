import { EntitiesCollection } from './EntitiesCollection';
import { PatchOrganizationScheme } from '../models/scheme/PatchOrganizationScheme';
import { PatchOrganization } from './../models/PatchOrganization';
import { ConfigurationManager } from './../services/db/ConfigurationManager';
import { PatchCustomerCollection } from './PatchCustomerCollection';
import { Query } from "./../services/common/Query";





export class PatchOrganizationCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = PatchOrganization;
    this.scheme = PatchOrganizationScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise.then((config) => {
      return ConfigurationManager.getConfig('patchHospitalRecordTypeId')
        .then((patchHospitalRecordTypeId) => {
          console.log("patchHospitalRecordTypeId",patchHospitalRecordTypeId);
          config.query += ` WHERE ${this.scheme.fields.recordTypeId.sfdc} = '${patchHospitalRecordTypeId}'`;
          console.log("config.query",config.query);
          return config;
        });
    });
  }

  // _fetchAllQuery() {
  //   return ConfigurationManager.getConfig('patchHospitalRecordTypeId')
  //   .then((patchHospitalRecordTypeId) => {
  //     console.log("patchHospitalRecordTypeId",patchHospitalRecordTypeId);
  //     const query = new Query()
  //     .selectFrom(this.scheme.table);
  //     const whereFields = {};
  //     whereFields[this.scheme.fields.recordTypeId.sfdc] = patchHospitalRecordTypeId;
  //     query.where(whereFields);
  //     return query;
  //   })
  // }

  parseModel(result) {
    if (result.Patch__r) {
      result[this.scheme.fields.patchName.sfdc] = result.Patch__r.Name;
      result[this.scheme.fields.patchStation.sfdc] = result.Patch__r.Station__c;
    }

    if (result.Contact__r) {
      result[this.scheme.fields.contactName.sfdc] = result.Contact__r.Name;
    }
    if(result.RecordType) {
      result[this.scheme.fields.recordTypeId.sfdc] = result.RecordType.Id;
      result[this.scheme.fields.remoteRecordType.sfdc] = result.RecordType.Name;
      result[this.scheme.fields.recordType.local] = result.RecordType.Name;
    }

    if(result.Account__r) {
      result[this.scheme.fields.accountName.sfdc] = result.Account__r.Name;
    }

    return super.parseModel(result);
  }

  didFinishDownloading(records) {
    return records;
  }
}