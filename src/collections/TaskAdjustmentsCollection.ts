import { EntitiesCollection } from './EntitiesCollection';
import {  TaskAdjustment} from '../models/TaskAdjustment';
import { TaskAdjustmentScheme } from '../models/scheme/TaskAdjustmentScheme';
import { Query } from '../services/common/Query';
export class TaskAdjustmentsCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = TaskAdjustment;
    this.scheme = TaskAdjustmentScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }

  getAllTaskAdjustmentsBySkusAndTasks(skus, tasks){
    const skuProductIds = skus.map(sku=> { return sku.productItemSfId});
    const taskIds = tasks.map (task=>{ return task.promotionTaskSfId});
   const query = this._fetchAllQuery().whereIn(this.scheme.fields.productItemSfId.sfdc, skuProductIds).and().whereIn('promotionTaskSfId', taskIds)
    this.fetchWithQuery(query).then((response)=> { return this.getAllEntitiesFromResponse });
  }
  getAllTaskAdjustmentsByCallReportAndTaskAccounts(callReport, promotionTaskAccountSfIds){
    const query = new Query().selectFrom(this.scheme.fields.table)
    .whereIn(this.scheme.fields.callReportSfId.sfdc, [callReport.id, this._attributesFromEntity(callReport)._soupEntryId]).and()
    .whereIn(this.scheme.fields.promotionTaskAccountSfId.sfdc, promotionTaskAccountSfIds)
    this.fetchWithQuery(query).then((response)=>{return this.getAllEntitiesFromResponse});
  }
  getAdjustmentsByCallReport(callReport){
    const callReportIdValue = {};
    callReportIdValue[this.scheme.fields.callReportSfId.sfdc] = this._attributesFromEntity(callReport)._soupEntryId
    this.fetchAllWhere(callReportIdValue)
    .then(response=>{this.getAllEntitiesFromResponse});
  }
  didStartUploading(records){
//    const splitBrokenAdjustments = _.groupBy records, (record=>{record.isModifiedInTrade is true} or {record.isModifiedInCall is true});
//    const   brokenAdjustments = splitBrokenAdjustments.true ? []
//     adjustmentsToUpload = splitBrokenAdjustments.false ? []
//     @removeEntities brokenAdjustments
//     .then -> adjustmentsToUpload
//     .then (entities) =>
//       CallReportsCollection = require 'models/bll/call-reports-collection/call-reports-collection'
//       сallReportsCollection = new CallReportsCollection()
//       сallReportsCollection.linkEntitiesToEntity entities, 'callReportSfId'
  }
  didPageFinishDownloading(records){
    return this._updateTasksAdjustments(records)
  }
  _updateTasksAdjustments(taskAdjustments){
    const updatedTaskAdjustments = taskAdjustments.map(taskAdjustment=>{
    taskAdjustment.promotionTaskSfId = taskAdjustment.PromotionTask_Account__r?taskAdjustment.PromotionTask_Account__r.Promotion_Task__c: '';
    return taskAdjustment;
    });
  this.cache.saveAll(updatedTaskAdjustments);
  }
}
