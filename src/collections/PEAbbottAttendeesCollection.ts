import { EntitiesCollection } from './EntitiesCollection';
import { PEAbbottAttendee } from './../models/PEAbbottAttendee';
import { PEAbbottAttendeeScheme } from './../models/scheme/PEAbbottAttendeeScheme';
import { Query } from '../services/common/Query';


export class PEAbbottAttendeesCollection extends EntitiesCollection {
  public model;
  public scheme;
 
  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = PEAbbottAttendee;
    this.scheme = PEAbbottAttendeeScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }

  fetchAllWhere(fieldsValues, ignoreDeleted=true){
  const query = this._fetchAllQuery().where(fieldsValues)
    return this.fetchWithQuery(query, ignoreDeleted)
  }
  didStartUploading(records){
//   this.PharmaEventsCollection = require 'models/bll/pharma-events-collection'
//   this.pharmaEventsCollection = new PharmaEventsCollection()
//   this.pharmaEventsCollection.linkEntitiesToEntity(records , 'pharmaEventSfId')
  }
}
