import { EntitiesCollection } from './EntitiesCollection';
import { PEEventExpense } from './../models/PEEventExpense';
import { PEEventExpenseScheme } from './../models/scheme/PEEventExpenseScheme';
import { Query } from '../services/common/Query';


export class PEEventExpenseCollection extends EntitiesCollection {
  public model;
  public scheme;
 
  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = PEEventExpense;
    this.scheme = PEEventExpenseScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }

  didFinishDownloading(records) {
    return records;
  }
  _fetchAllQuery(){
    const query = new Query().selectFrom(this.scheme.table)
    if (this.scheme.pharmaEventSfId)
    {
        const whereFields = {};
        whereFields[this.scheme.pharmaEventSfId.sfdc] = this.scheme.pharmaEventSfId
        query.where(whereFields)
    }
   
   return query
  }

  fetchAllWhere(fieldsValues, ignoreDeleted=true){
 const query = this._fetchAllQuery().where(fieldsValues)
  this.fetchWithQuery(response=>{this.query, ignoreDeleted});
  }

  didStartUploading(records){
    // this.PharmaEventsCollection = require 'models/bll/pharma-events-collection'
    // this.pharmaEventsCollection = new PharmaEventsCollection()
    // this.pharmaEventsCollection.linkEntitiesToEntity(records,'pharmaEventSfId');
  }
  fetchEntityByPEId(pharmaEventSfId, SoupId){
    const query = new Query().selectFrom(this.scheme.table);
    const whereFields = {};
    query.whereIn(this.scheme.pharmaEventSfId.sfdc,[this.pharmaEventSfId,SoupId]);
    this.fetchWithQuery(query)
    .then(response=>{this.getAllEntitiesFromResponse});
  }

  fetchEntityByPEventId(pharmaEventSfId){
  const query = new Query().selectFrom(this.scheme.table);
 const whereFields = {};
  whereFields[this.scheme.pharmaEventSfId.sfdc] = this.pharmaEventSfId;

  query.where(whereFields);
  this.fetchWithQuery(query)
  .then(response=>{this.getAllEntitiesFromResponse});
  }
}
