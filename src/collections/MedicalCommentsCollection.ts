import { EntitiesCollection } from './EntitiesCollection';
import { MedicalComment } from './../models/MedicalComment';
import { MedicalCommentScheme } from './../models/scheme/MedicalCommentScheme';
import { Query } from '../services/common/Query';
import {ConfigurationManager} from './../services/db/ConfigurationManager';
import { MedicalQueriesCollection } from './MedicalQueriesCollection/MedicalQueriesCollection'


export class MedicalCommentsCollection extends EntitiesCollection {
  public model;
  public scheme;

  constructor() {
    // @ts-ignore
    super(...arguments);
    this.model = MedicalComment;
    this.scheme = MedicalCommentScheme;
    this.init();
  }

  prepareServerConfig(configPromise) {
    return configPromise;
  }
  parseModel(result){
    console.log("mq result",result);
    return super.parseModel(result);
  }

  getMqByIds(mqId,soupId){
    const query = this._fetchAllQuery();
    query.whereIn(this.scheme.fields.mqId.sfdc,[mqId,soupId]);
    return this.fetchWithQuery(query)
    .then(response=> { return this.getAllEntitiesFromResponse(response); });
  }

  fetchBymqId(mqId){
    const query = this._fetchAllQuery();
    query.where(this.scheme.fields.mqId.sfdc,mqId);
    return this.fetchWithQuery(query)
    .then(response=> { return this.getAllEntitiesFromResponse(response); });
  }

  didStartUploading(entities:any)
  {
    let medicalQueryCollection =  new MedicalQueriesCollection;
    return medicalQueryCollection.linkEntitiesToEntity(entities, 'mqId')
  }
    

  didFinishDownloading(records) {
    return records;
  }
}
