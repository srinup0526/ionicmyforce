import {Component, ElementRef, EventEmitter, Input, Output, HostListener} from '@angular/core';
import {Button} from "ionic-angular";
import { ModalController, PopoverController } from 'ionic-angular';
import { SalesplanmodelPage } from '../../pages/salesplanmodel/salesplanmodel';
declare var $: any;
@Component({
  selector: 'salesplanfilter',
  templateUrl: 'salesplanfilter.html'
})
export class SalesplanfilterComponent {
 @Input() button: Button;
  @Input() content: Array<any>;
  
  buttunNativeEl: any;
  allPanelValues: Array<any>
  showBollean: boolean = false;
  @Output() filtersChangeEvent: EventEmitter<any>;
  
  items = [
    'Pokémon Yellow',
    'Super Metroid',
    'Mega Man X',
    'The Legend of Zelda',
    'Pac-Man',
    'Super Mario World',
    'Street Fighter II',
    'Half Life',
    'Final Fantasy VII'
  ];

 
  constructor(private elementRef: ElementRef,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController) {
    this.filtersChangeEvent = new EventEmitter();
    
  }
  
  ngOnInit() {
    if (this.button) {
      this.buttunNativeEl = this.button.getNativeElement();
      this.setOpenBtnListener(this.buttunNativeEl);
    }
    
   this.setStylesToContent();
  }
  itemSelected(item: string) {
    console.log("Selected Item", item);
    // const modal = this.modalCtrl.create(SalesplanmodelPage);
    // modal.present();
   
  
  }
  public onTapOpenBtnHandler() {
    this.togglePanel()
    this.showBollean = true;
  }
  
  public showPanel() {
    this.elementRef.nativeElement.classList.add('open');
    this.showBollean = true;
  }
  
  public hidePanel() {
    this.elementRef.nativeElement.classList.remove('open');
    this.showBollean = false;
  }
  
  private setOpenBtnListener(button) {
    button.addEventListener('click', this.onTapOpenBtnHandler.bind(this));
  }
  
  private setStylesToContent() {
    return this.content
      .map((element) => element.getNativeElement ? element.getNativeElement() : element)
      .forEach((element) => {
        element.style.transform = "translate3d(0,0,0)";
      });
  }
  
  private togglePanel() {
    this.toggleSideMenu();
    // this.showBollean = true;
  }
  
  private toggleSideMenu() {
    this.elementRef.nativeElement.classList.toggle('open');
    // this.showBollean = true;
  }
  
  onChangeFilterItemHandler() {
    console.log('filtered values generated');
  }
  

}
