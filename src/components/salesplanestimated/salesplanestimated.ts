import { Component } from '@angular/core';
import { SalesPlanningProductCollection } from '../../collections/SalesPlanningProductCollection';
import { ProductItemsCollection } from '../../collections/ProductItemsCollection';
import { SalesPlanProduct } from '../../models/SalesPlanProduct';
import { SalesPlan } from '../../models/SalesPlan';
import {  } from '../../collections/SalesPlanningListCollection';
import { IonicPage, LoadingController, ToastController, NavParams, NavController, ViewController } from 'ionic-angular';
/**
* Generated class for the SalesplanestimatedComponent component.
*
* See https://angular.io/api/core/Component for more info on Angular
* Components.
*/
@Component({
  selector: 'salesplanestimated',
  templateUrl: 'salesplanestimated.html'
})
export class SalesplanestimatedComponent {

  public doctorFirstName: any;
  public doctorLastName: any;
  public salesPlanProduct: any;
  public productList: any = [];
  public salesPlanProductWrapper:Array<SalesPlanProduct> = [];

  public slectedSalesPlanRecords: any;
  constructor(public salesPlanningProductCollection: SalesPlanningProductCollection,
    public productItemsCollection: ProductItemsCollection, public navPrams: NavParams, 
    public viewCtrl: ViewController) {
    console.log('Hello SalesplanestimatedComponent Component');

    this.slectedSalesPlanRecords = this.navPrams.data['customer'];
     console.log("selected customer record in popover", this.slectedSalesPlanRecords);
    if(this.navPrams.get('salesplanlist'))
    {
      this.salesPlanningProductCollection.fetchSalesPlanningListById(this.navPrams.get('salesplanlist'))
      .then(res=>{
        console.log("res",res);
        // this.productList = res;
        this.slectedSalesPlanRecords =  res;

        if(this.slectedSalesPlanRecords.length>0)
        {
          this.slectedSalesPlanRecords.map(salesPlanList=>{
            this.salesPlanProductWrapper.push(salesPlanList);
          })
          if(this.salesPlanProductWrapper.length<5)
          {
            for(let i=this.salesPlanProductWrapper.length;i<5;i++)

            {
              this.salesPlanProductWrapper.push(new SalesPlanProduct({}));
            }
          }
        }
        else{
          let salesPlanListId = this.navPrams.get('salesplanlist').id.includes('local_')?this.navPrams.get('salesplanlist')._soupEntryId:this.navPrams.get('salesplanlist').id;
          console.log("salesPlanListId",salesPlanListId);
          for(let i=0;i<5;i++)
          {
            this.salesPlanProductWrapper.push(new SalesPlanProduct({salesPlanningList:salesPlanListId}));
          }
        }
        console.log("Product list in sales planning hi hi hi qwqwqwqw",this.slectedSalesPlanRecords);

      })
    }
    else
    {
      for(let i=0;i<5;i++)
      {
        this.salesPlanProductWrapper.push(new SalesPlanProduct({}));
      }
    }


    this.productItemsCollection.fetchAllAvailableProducts()
    .then(res=>{
      console.log("res",res);
      console.log("Product list in sales planning >>>> ",this.productList);
      this.productList =  res;
    })
  }

  public dynamicPriceCalculation(product,salesPlanProduct): void { //here item is an object 
    console.log("salesPlanData",product);
    console.log("salesPlanProduct.product",salesPlanProduct);

    salesPlanProduct.price = product.price?product.price:salesPlanProduct.price;
    salesPlanProduct.plannedQuantity = product.plannedQuantity?product.plannedQuantity:1;
    salesPlanProduct.total = salesPlanProduct.plannedQuantity * salesPlanProduct.price;
    
  }
  cancelAction(){
    return this.viewCtrl.dismiss();
  }
  saveData(){
    this.salesPlanProductWrapper.map(salesPlanProduct=>{
      console.log("salesPlanProduct 1234567890",salesPlanProduct);

      if(salesPlanProduct.product && salesPlanProduct.product!='')
      {
        console.log('_soupEntryId existed or not',"_soupEntryId" in salesPlanProduct);
        if("_soupEntryId" in salesPlanProduct && salesPlanProduct._soupEntryId){
          console.log('Sales plan exxiting data with soap entry id',salesPlanProduct)
          this.salesPlanningProductCollection.updateEntity(salesPlanProduct)
          .then(createdRes=>{
            console.log("Updated Data",createdRes);
          return this.viewCtrl.dismiss();
          })
        }else{
          this.salesPlanningProductCollection.createEntity(salesPlanProduct)
          .then(createdRes=>{
            console.log("createdRes",createdRes);
            return this.viewCtrl.dismiss();
          })
        }
      }
    })
  }

}
