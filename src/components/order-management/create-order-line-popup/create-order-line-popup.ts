import {Component, ViewChild, EventEmitter, Output, ElementRef} from '@angular/core';
import { LazyTableOption } from './../../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../../components/table/lazy-table/lazy-table';
import {ProductItemsCollection} from "../../../collections/ProductItemsCollection";
import {ProductItemScheme} from "../../../models/scheme/ProductItemScheme";
import {Order} from "../../../models/Order";
import { NavParams, ToastController, ViewController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import {ProductItem} from "../../../models/ProductItem";
import {LocalizationManager} from './../../../services/common/localizations/LocalizationManager';
import { Settings, SettingsImpl } from './../../../services/db/Settings';
import {OrderLine} from "../../../models/OrderLine";
import {Utils} from "../../../utils/Utils";
import {Loader} from './../../../services/common/Loader';
import {OrderCalculator} from './../../../pages/order-management/OrderCalculator';
import {NumberValidator} from './../../../utils/NumberValidator';



@Component({
  selector: 'create-order-line-popup',
  templateUrl: 'create-order-line-popup.html'
})
export class CreateOrderLinePopupComponent {
  
  static readonly CSS_ACTIVE_CLASS = 'active-line';
  private searchString: string;
  private order: Order;
  private orderLine: OrderLine;
  private orderType: string;
  private collection: ProductItemsCollection;
  private tableOptions: LazyTableOption;
  private activeProduct: ProductItem;
  private activeProductElement: any;
  private formGroup: FormGroup;
  private isEditMode: boolean;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;

  constructor(private productItem: ProductItemsCollection,
              private formBuilder: FormBuilder,
              private params: NavParams,
              private toastCtrl: ToastController,
              private localizationManager: LocalizationManager,
              private viewCtrl: ViewController,
              private elementRef: ElementRef,
              private loader: Loader) {

    this.init();
  }
  
  public onTapRowHandler(event, product): void {
    this.setProduct(product);
    this.setProductDataToForm();
    this.updateSelectedElement(this.activeProduct.id);
  }
  
  public onSubmitFormHandler(): void {
    const isValid = this.validate();
    
    if(isValid) {
      this.calculateAndSetAmount();
      
      let orderLine;
      
      if(this.isEditMode) {
        orderLine = this.updateEntity();
      }else{
        orderLine = this.createNewEntity();
      }
      
      this.closeModalPopup({
        isEditMode: this.isEditMode,
        orderLine: orderLine
      });
    }
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onCheckActiveLineHandler(): void {
    if(this.activeProduct) {
      this.updateSelectedElement(this.activeProduct.id);
    }
  }
  
  public onBlurInputHandler(): void {
    this.calculateAndSetAmount();
  }

  public onTapCancelBtnHandler() {
    this.viewCtrl.dismiss();
  }
  
  public ngOnInit() {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
  
    this.lazyTable.reloadTable();
  }
  
  private init(){
    this.initParams();
    this.initCssClass();
    this.initForm();
    this.initTable();
  }
  
  private initParams(): void {
    this.order = this.params.get('order');
    this.orderLine = this.params.get('orderLine');
    this.activeProduct = this.params.get('product');
    this.isEditMode = this.params.get('isEditMode');
    this.orderType = this.order.recordTypeId;
  }
  
  private initForm(): void {
    let orderLineFormFields = this.initFormOrderLineFields();
    let productFormFields = this.initFormProductItemFields();

    this.formGroup = this.formBuilder.group(Utils.extend(orderLineFormFields, productFormFields));
  }
  
  private initFormOrderLineFields() {
    let salesOptions = new Array();
  
    salesOptions.push(this.orderLine.salesPrice || '0');
  
    if(Settings.getInstance().getOrderSalesRecordTypeId() == this.orderType) {
      salesOptions.push([Validators.required, NumberValidator.min(0.01)]);
    }
  
    return {
      skuName: [this.orderLine.SKUName || '', Validators.required],
      quantity: [this.orderLine.quantity || '1', Validators.required],
      salesPrice: salesOptions,
      salesAmount: [this.orderLine.salesAmount || '0'],
    }
  }
  
  private initFormProductItemFields() {
    if(!this.activeProduct){
      return {
        sales1MonthAgo: [''],
        sales2MonthAgo: [''],
        sales3MonthAgo: [''],
        sales4MonthAgo: [''],
        salesYTDC: [''],
      };
    }
    
    return {
      sales1MonthAgo: [this.activeProduct.sales1MonthAgo || ''],
      sales2MonthAgo: [this.activeProduct.sales2MonthAgo || ''],
      sales3MonthAgo: [this.activeProduct.sales3MonthAgo || ''],
      sales4MonthAgo: [this.activeProduct.sales4MonthAgo || ''],
      salesYTDC: [this.activeProduct.salesYTDC || ''],
    }
  }
  
  private initTable(): void {
    this.tableOptions = {
      headerItems: [
        {
          title: 'common.names.Contact',
          fields: [ProductItemScheme.fields.id]
        },
        {
          title: 'common.names.Contact',
          fields: [ProductItemScheme.fields.name]
        }
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
  
    this.collection = this.productItem;
  
    this.searchString = '';
  }
  

  
  private setProductElement(productElement): void {
    this.activeProductElement = productElement;
  }
  
  private setProduct(product: ProductItem): void {
    this.formGroup.controls.skuName.setValue(product.id);
    
    this.activeProduct = product;
  }
  
  private validate() {
    if(this.formGroup.valid){
      return this.formGroup.valid;
    }
    
    this.getValidateMsg()
      .then(this.showValidateToast.bind(this));
    
    return;
  }
  
  private getValidateMsg(): Promise<string> {
    return this.localizationManager.getSeveralLocales([
      'card.OrderLineItem.ToastMessage.RequiredFieldsHeader',
      'card.OrderLineItem.ToastMessage.RequiredProduct',
      'card.OrderLineItem.ToastMessage.RequiredQuantity',
      'card.OrderLineItem.ToastMessage.RequiredSalesPrice'
    ]).then((localization) => {
      
      const toastHeader = `${localization['card.OrderLineItem.ToastMessage.RequiredFieldsHeader']} \n`;
      let toastBody = '';
  
      if(!this.formGroup.controls.skuName.valid) {
        toastBody += `${localization['card.OrderLineItem.ToastMessage.RequiredProduct']}\n`;
      }
  
      if(!this.formGroup.controls.quantity.valid) {
        toastBody += `${localization['card.OrderLineItem.ToastMessage.RequiredQuantity']}\n`;
      }
  
      if(!this.formGroup.controls.salesPrice.valid && Settings.getInstance().getOrderSalesRecordTypeId() == this.orderType) {
        toastBody += `${localization['card.OrderLineItem.ToastMessage.RequiredSalesPrice']}\n`;
      }
  
      return toastHeader + toastBody;
    });
  }
  
  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton:true,
      cssClass:"toastClass",
      duration: 5000,
    });
    return toast.present();
  }
  
  private createNewEntity(): OrderLine {
    let orderLine = new OrderLine({});
  
    return this.setDataToEntity(orderLine);
  }
  
  private updateEntity(): OrderLine{
    return this.setDataToEntity(this.orderLine)
    
  }
  
  private setDataToEntity(orderLine) {
    orderLine.SKUName = this.activeProduct.id;
    orderLine.productLot = this.activeProduct.name;
    orderLine.orderId = this.order.id || this.order._soupEntryId;
    orderLine.quantity = parseInt(this.formGroup.controls.quantity.value);
    orderLine.salesPrice = parseFloat(this.formGroup.controls.salesPrice.value);
    orderLine.salesAmount = this.formGroup.controls.salesAmount.value;
  
    if(this.activeProduct) {
      orderLine.sales1MonthAgo = this.activeProduct.sales1MonthAgo;
      orderLine.sales2MonthAgo = this.activeProduct.sales2MonthAgo;
      orderLine.sales3MonthAgo = this.activeProduct.sales3MonthAgo;
      orderLine.sales4MonthAgo = this.activeProduct.sales4MonthAgo;
      orderLine.salesYTDC = this.activeProduct.salesYTDC;
    }
  
    return orderLine;
  }

  private closeModalPopup(options: { isEditMode: boolean, orderLine: OrderLine }) {
    this.viewCtrl.dismiss(options);
  }
  
  private updateSelectedElement(productId) {
    const element = document.querySelectorAll('.product-table table-row .cell:first-of-type span');
  
    [].slice.call(element)
      .filter((item) => {
        this.resetActiveElement(item.parentElement.parentElement);
        
        return item.innerText == productId;
      })
      .map((item) => this.setActiveElement(item.parentElement.parentElement))
  }
  
  private resetActiveElement(element): void{
    if (element && element.classList.contains(CreateOrderLinePopupComponent.CSS_ACTIVE_CLASS)) {
      element.classList.remove(CreateOrderLinePopupComponent.CSS_ACTIVE_CLASS);
    }
  }
  
  private setActiveElement(element): void{
    if (element) {
      element.classList.add(CreateOrderLinePopupComponent.CSS_ACTIVE_CLASS);
    }
  }
  
  private calculateAndSetAmount() {
    const amount = this.calculateAmount();
    
    return this.formGroup.controls.salesAmount.setValue(amount);
  }
  
  private calculateAmount(): number {
    return OrderCalculator.calculateAmount(
      this.formGroup.controls.quantity.value,
      this.formGroup.controls.salesPrice.value
    );
  }
  
  private setProductDataToForm() {
    this.formGroup.controls.sales1MonthAgo.setValue(this.activeProduct.sales1MonthAgo);
    this.formGroup.controls.sales2MonthAgo.setValue(this.activeProduct.sales2MonthAgo);
    this.formGroup.controls.sales3MonthAgo.setValue(this.activeProduct.sales3MonthAgo);
    this.formGroup.controls.sales4MonthAgo.setValue(this.activeProduct.sales4MonthAgo);
    this.formGroup.controls.salesYTDC.setValue(this.activeProduct.salesYTDC);
  }

  private initCssClass() {
    if(Settings.getInstance().getOrderSamplesRecordTypeId() == this.orderType) {
      return this.elementRef.nativeElement.classList.add('sample-type');
    }
    return this.elementRef.nativeElement.classList.remove('sample-type');
  }
}
