import {Component, Input, EventEmitter, Output, ChangeDetectorRef} from '@angular/core';
import {OrderLine} from "../../../models/OrderLine";
import {OrdersLineCollection} from "../../../collections/OrdersLineCollection";
import {Order} from "../../../models/Order";
import {ConfirmDeleteOrderPopup} from "./ConfirmDeleteOrderPopup";
import {AlertController, ModalController, ModalOptions} from 'ionic-angular';
import {LocalizationManager} from './../../../services/common/localizations/LocalizationManager';
import {Loader} from './../../../services/common/Loader';
import {CreateOrderLinePopupComponent} from './../create-order-line-popup/create-order-line-popup';
import {ProductItem} from "../../../models/ProductItem";
import {ProductItemScheme} from "../../../models/scheme/ProductItemScheme";
import {ProductItemsCollection} from "../../../collections/ProductItemsCollection";


@Component({
  selector: 'order-item-list',
  templateUrl: 'order-item-list.html'
})
export class OrderItemListComponent {
  
  @Input() isEditable: boolean;
  @Input() order: Order;
  @Output() changeLines: EventEmitter<Array<OrderLine>>;
  
  public orderLines: Array<OrderLine>;
  public editIndex: number;
  public deletedIds: Array<string>;
  
  protected isChanged: boolean;

  constructor(private productItem: ProductItemsCollection,
              private ordersLineCollection: OrdersLineCollection,
              private alertCtrl: AlertController,
              private localizationManager: LocalizationManager,
              private loader: Loader,
              private modalCtrl: ModalController,
              private changeDetectorRef: ChangeDetectorRef) {
    
    this.orderLines = [];
    this.deletedIds = [];
    this.changeLines = new EventEmitter();
    
    this.resetChanges();
  }
  
  public ngOnInit() {
    this.disableEditing();
  }
  
  public onAddNewLineHandler(orderLine: OrderLine) {
    this.addItem(orderLine);
    this.fireChangeEvent();
  }
  
  public onUpdateLineHandler(orderLine: OrderLine) {
    this.updateItem(orderLine);
    this.fireChangeEvent();
  }
  
  public onTapAddButtonHandler() {
    this.presentProfileModal({
      order: this.order,
      orderType: this.order.recordTypeId,
      orderLine: new OrderLine({})
    });
  }
  
  public onTapEditButtonHandler(orderLine, index) {
    this.getProduct(orderLine.SKUName)
      .then((product) => {
        this.editIndex = index;
        
        this.presentProfileModal({
          order: this.order,
          orderType: this.order.recordTypeId,
          orderLine: orderLine,
          product: product,
          isEditMode: true
        });
      })
  }
  
  public onTapDeleteButtonHandler(item, index) {
    this.loader.run(() => this.deleteItem(index));
  }
  
  public loadItems(): Promise<Array<OrderLine>> {
    return this.ordersLineCollection.fetchByOrderId(this.order.id, this.order._soupEntryId)
    .then((orderLines) => {
      this.orderLines = orderLines;
      
      return this.orderLines;
    })
  }
  
  public enableEditing() {
    this.isEditable = true;
    this.changeDetectorRef.markForCheck();
  }
  
  public disableEditing() {
    this.isEditable = false;
    this.changeDetectorRef.markForCheck();
  }
  
  public getItems() {
    return this.orderLines;
  }
  
  public getDeletedItemsIds() {
    return this.deletedIds;
  }
  
  private updateItem(orderLine: OrderLine) {
    this.orderLines.splice(this.editIndex, 1, orderLine);
    this.dataChanged();
  }
  
  private addItem(orderLine: OrderLine) {
    this.orderLines.push(orderLine);
    this.dataChanged();
  }
  
  public hasChanges() {
    return this.isChanged;
  }
  
  public dataChanged() {
    this.isChanged = true;
  }
  
  public resetChanges() {
    this.isChanged = false;
  }
  
  private deleteItem(index: number) {
    return this.showDeleteConfirmPopup()
      .then((confirmResult: { deleteItem: boolean }) => {
        if(confirmResult.deleteItem) {
          if(this.orderLines[index].id) {
            this.deletedIds.push(this.orderLines[index].id);
          }
          
          this.dataChanged();
          this.orderLines.splice(index, 1);
          this.fireChangeEvent();
        }
      });
  }
  
  private showDeleteConfirmPopup(): Promise<{ deleteItem: boolean }> {
    const popup: ConfirmDeleteOrderPopup = new ConfirmDeleteOrderPopup(this.alertCtrl, this.localizationManager);
    
    return popup.show();
  }
  
  private reloadItems() {
    return this.loadItems();
  }
  
  
  private presentProfileModal(componentOptions): Promise<any> {
    const popupOptions: ModalOptions = {
      cssClass: 'order-line-modal-popup',
      enterAnimation: 'modal-fade-in',
      leaveAnimation: 'modal-fade-out '
    };
    
    const profileModal = this.modalCtrl.create(CreateOrderLinePopupComponent,  componentOptions, popupOptions);

    profileModal.onDidDismiss((options: { isEditMode: boolean, orderLine: OrderLine }) => {
      if(!options) {
        return;
      }
      
      if(options.isEditMode){
        return this.onUpdateLineHandler(options.orderLine);
      }
      
      return this.onAddNewLineHandler(options.orderLine);
    });
    
    return profileModal.present();
  }
  
  private getProduct(productId: string): Promise<ProductItem> {
    let where = {};
    
    where[ProductItemScheme.fields.id.sfdc] = productId;
    
    return this.productItem.fetchAllWhere(where)
      .then(this.productItem.getEntityFromResponse.bind(this))
      .then((record) => {
        return record;
      });
  }
  
  private fireChangeEvent() {
    this.changeLines.next(this.orderLines);
  }
}
