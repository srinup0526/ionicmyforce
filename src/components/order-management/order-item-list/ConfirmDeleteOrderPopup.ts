import { AlertController, AlertOptions, Alert } from 'ionic-angular';
import {LocalizationManager} from './../../../services/common/localizations/LocalizationManager';

export class ConfirmDeleteOrderPopup {
  constructor(private alertCtrl: AlertController,
              private localizationManager: LocalizationManager) {
  }
  
  public show(): Promise<{ deleteItem: boolean }> {
    return this.localizationManager.getSeveralLocales([
      'OrdersManagement.ConfirmationPopup.DeleteLineItem.Caption',
      'OrdersManagement.ConfirmationPopup.DeleteLineItem.Question',
      'common.buttons.YesBtn',
      'common.buttons.NoBtn'
    ]).then((locales) => {
      return this.showConfirmDeletePopup(locales);
    });
  }
  
  private showConfirmDeletePopup(locales): Promise<{ deleteItem: boolean }> {
    const title: string = locales['OrdersManagement.ConfirmationPopup.DeleteLineItem.Caption'];
    const message: string = locales['OrdersManagement.ConfirmationPopup.DeleteLineItem.Question'];
    const yesLabel: string = locales['common.buttons.YesBtn'];
    const noLabel: string = locales['common.buttons.NoBtn'];
    
    return new Promise((resolve, reject) => {
      const options: AlertOptions = {
        title: title,
        message: message,
        cssClass: 'popup alert confirm',
        buttons: [
          {
            text: yesLabel,
            role: 'cancel',
            handler: () => {
              resolve({
                deleteItem: true,
         
              });
            }
          },
          {
            text: noLabel,
            role: 'cancel',
            cssClass: 'no',
            handler: () => {
              resolve({
                deleteItem: false,
              });
            }
          }
        ]
      };
      
      const alert: Alert = this.alertCtrl.create(options);
      
      alert.present();
    });
  }
}
