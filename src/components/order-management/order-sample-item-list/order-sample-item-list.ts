import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {OrderLine} from "../../../models/OrderLine";
import {OrdersLineCollection} from "../../../collections/OrdersLineCollection";
import {Order} from "../../../models/Order";
import {ProductItemsCollection} from "../../../collections/ProductItemsCollection";
import {AlertController, ModalController, ModalOptions} from "ionic-angular";
import {LocalizationManager} from "../../../services/common/localizations/LocalizationManager";
import {Loader} from "../../../services/common/Loader";
import {ConfirmDeleteOrderPopup} from "../order-item-list/ConfirmDeleteOrderPopup";
import {CreateOrderLinePopupComponent} from "../create-order-line-popup/create-order-line-popup";
import {ProductItem} from "../../../models/ProductItem";
import {ProductItemScheme} from "../../../models/scheme/ProductItemScheme";
import {OrderItemListComponent} from "../order-item-list/order-item-list";

@Component({
  selector: 'order-sample-item-list',
  templateUrl: 'order-sample-item-list.html'
})
export class OrderSampleItemListComponent extends OrderItemListComponent{
  @Input() isEditable: boolean;
  @Input() order: Order;
  @Output() changeLines: EventEmitter<Array<OrderLine>>;

  public orderLines: Array<OrderLine>;
  public editIndex: number;

  constructor(productItem: ProductItemsCollection,
              ordersLineCollection: OrdersLineCollection,
              alertCtrl: AlertController,
              localizationManager: LocalizationManager,
              loader: Loader,
              modalCtrl: ModalController,
              changeDetectorRef: ChangeDetectorRef) {

    super(productItem, ordersLineCollection, alertCtrl, localizationManager, loader, modalCtrl, changeDetectorRef);
  }
}
