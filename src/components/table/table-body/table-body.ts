import {Component, ElementRef, Input, ViewChild, SimpleChanges, Output, EventEmitter} from '@angular/core';
import {LoadingController, Scroll} from 'ionic-angular';
import {TableHeaderItemOption} from '../table-header/TableHeaderItemOption';
import {Content} from 'ionic-angular';
import {Query} from "../../../services/common/Query";
import {LazyTableOption} from '../lazy-table/LazyTableOption';
import {Utils} from "../../../utils/Utils";


@Component({
  selector: 'table-body',
  templateUrl: 'table-body.html'
})
export class TableBodyComponent {
  static BATCH_SIZE: number = 100;
  
  private headerItems: Array<TableHeaderItemOption>;
  private batchSize: number;
  private rowHandler: any;
  
  @Input() collection: any;
  @Input() customQuery: string;
  @Input() searchString: string;
  @Output() queryChange: EventEmitter<Query>;

  @ViewChild(Content) content: Content;
  
  private activeHeaderColumn: TableHeaderItemOption;
  private filterPanelFields: any;
  private fetchResponse: any;
  private currentNumberOfRows: number = 100;
  private cellsBatchSize: number = 100;
  
  private rowIndex: number = 0;
  
  
  public records: Array<any>;
  private checkInProgress: boolean;
  
  private readonly DEBOUNCE_DELAY: number = 200;
  
  constructor(private loadingCtrl: LoadingController,
              private elementRef: ElementRef) {
    this.resetAll();
    this.queryChange = new EventEmitter();
  }
  
  public doInfinite(event){
    if (this.checkInProgress || event.endIndex <= (this.records.length - 10) || !this.isHavingMore()) {
      return;
    }
    
    this.checkInProgress = true;
  
    let loader = this.getLoader();
    
    loader.present()
      .then(() => this.getNewRecords())
      .then(() => Utils.delay(2000))
      .then(() => loader.dismiss())
      .then(() => {
        this.checkInProgress = false;
        loader = null;
      })
      .catch((error) => {
        return loader.dismiss()
          .then(() => {
            this.checkInProgress = false;
            
            console.error(error);
            loader = null;
          });
      })
  }
  
  public setColumnOptions(options: LazyTableOption) {
    this.headerItems = options.headerItems;
    this.batchSize = options.batchSize;
    this.rowHandler = options.rowHandler;
  }
  
  public setSearchString(searchString) {
    this.searchString = searchString;
  }
  
  public setCustomQuery(customQuery) {
    this.customQuery = customQuery;
  }
  
  public setCollection(collection) {
    this.collection = collection;
  }
  
  
  public setFilterData(filterData) {
    this.filterPanelFields = filterData;
  }
  
  public resetAndReload(): Promise<any> {
    this.reset();
    return this.refresh();
  }
  
  public setActiveColumn(activeHeaderColumn: TableHeaderItemOption) {
    this.activeHeaderColumn = activeHeaderColumn;
  }

  public getTableQuery(query): Query {
    return this.getQuery(query);
  }
  
  private isHavingMore() {
    return this.isHavingMoreFetchedRecords() || (this.fetchResponse && this.fetchResponse.hasMore());
  }

  private getTableQueryAndFireEvent(query): Query {
    const fullQuery = this.getQuery(query);

    this.fireQueryChangeEvent(fullQuery);

    return fullQuery;
  }

  private getQuery(query): Query {
    if (this.customQuery) {
      query = query.whereCustomCondition(this.customQuery);
    }

    if (this.searchString) {
      query = this.getSearchCondition(query);
    }

    if (this.filterPanelFields) {
      this.filterPanelFields.map((filter) => {
        if (Object.keys(filter.fields).length) {
          if (filter.condition) {
            query = query[filter.query](filter.fields, filter.condition, filter.joinWith)
          } else {
            query = query[filter.query](filter.fields, filter.joinWith)
          }
        }
      });
    }

    if (this.activeHeaderColumn) {
      const order: string = this.activeHeaderColumn.isAsc ? Query.ASC : Query.DESC;
      query = query.orderBy(this.activeHeaderColumn.fields.map((field) => field.sfdc || field.local), order, this.activeHeaderColumn.isNumber);
    }
    console.log("query -> ", query);
    console.log(query.query);

    return query;
  }
  
  private getNewRecords() {
    if (this.isHavingMoreFetchedRecords()) {
      return this.renderMoreCells();
    }
    
    return this.fetchMoreRecords();
  }
  
  private refresh(): Promise<any> {
    if (this.checkInProgress) {
      return;
    }
  
    this.checkInProgress = true;
    const loader = this.getLoader();
  
    return loader.present()
      .then(() => this.reload())
      .then(() => {
        this.checkInProgress = false;
        return loader.dismiss();
      })
      .catch((error) => {
        this.checkInProgress = false;
        console.error(error);
        loader.dismiss()
      })
  }
  
  private isHavingMoreFetchedRecords() {
    return this.fetchResponse && this.currentNumberOfRows <= this.fetchResponse.records.length;
  }
  
  
  private getLoader() {
    return this.loadingCtrl.create({
      content: 'Please wait',
      duration: 100
    })
  }
  
  private getAllEntities(): Array<any> {
    return this.collection.getAllEntitiesFromResponse(this.fetchResponse);
  }
  
  private fetchMoreRecords() {
    if (this.fetchResponse.hasMore()) {
      
      return this.collection.getMoreEntitiesFromResponse(this.fetchResponse)
        .then((fetchResponse) => {
          this.rowIndex = 0;
          this.fetchResponse = fetchResponse;
          
          this.setRecords();
        })
    }
    
    return Promise.resolve();
  }
  
  private renderMoreCells() {
    this.currentNumberOfRows += this.cellsBatchSize;
    this.setRecords();
    
    return Promise.resolve();
  }
  
  reload() {
    const query = this.collection._fetchAllQuery();
    const updateQuery = this.getTableQueryAndFireEvent(query);
    
    return this.collection.fetchWithQuery(updateQuery)
      .then((response) => {
        this.rowIndex = 0;
        console.log("this.collection.modifyResponse",this.collection.modifyResponse);
       if(this.collection.modifyResponse){
          this.collection.modifyResponse(response)
          .then(res=>{
            console.log("res",res, res.records);
            response.records = res.records;
            this.fetchResponse = response;//res.__zone_symbol__value;
            console.log("this.fetchResponse",this.fetchResponse);
            this.setRecords();
          })
        }
          else
          {
            this.fetchResponse = response;
            console.log("this.fetchResponse",this.fetchResponse);
            this.setRecords();
          }
        
  
        return response;
      })
      .catch((error) => {
        console.error(error);
  
        return error;
      })
  }
  
  setRecords() {
    const numberOfRows = Math.min(this.currentNumberOfRows, this.fetchResponse.records.length);
    
    for (let i = this.rowIndex; i < numberOfRows; i++) {
      
      const parsedRecord = this.collection.parseEntity(this.fetchResponse.records[this.rowIndex]);
      
      this.rowIndex++;
      
      this.records.push(parsedRecord);
    }
  }
  
  private getSearchCondition(query: Query): Query {
    let whereLike: { [field: string]: string } = {};
    
    this.collection.scheme.searchableSchema()
      .map((field) => {
        whereLike[field.sfdc || field.local] = this.searchString;
      });
    
    return query.whereLike(whereLike);
  }
  
  private resetAll(): void {
    this.headerItems = [];
    this.records = [];
    this.rowIndex = 0;
  }
  
  private reset(): void {
    this.records = [];
    this.rowIndex = 0;
    this.currentNumberOfRows = 100;
  }
  
  private scrollToTop(): void {
    this.content.scrollToTop();
  }

  private fireQueryChangeEvent(query: Query): void {
    this.queryChange.next(query);
  }
  
}
