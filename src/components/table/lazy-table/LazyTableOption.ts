import { TableHeaderItemOption } from '../table-header/TableHeaderItemOption';


export interface LazyTableOption {
  headerItems: Array<TableHeaderItemOption>;
  batchSize: number;
  rowHandler?: any;
}
