import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import { TableHeaderItemOption } from '../table-header/TableHeaderItemOption';
import { TableBodyComponent } from '../table-body/table-body';
import { TableHeaderComponent } from '../table-header/table-header';
import { LazyTableOption } from './LazyTableOption';
import {Query} from "../../../services/common/Query";

@Component({
  selector: 'lazy-table',
  templateUrl: 'lazy-table.html'
})
export class LazyTableComponent {
  
  @ViewChild(TableBodyComponent) tableBody: TableBodyComponent;
  @ViewChild(TableHeaderComponent) tableHeader: TableHeaderComponent;

  @Output() queryChange: EventEmitter<Query>;
  
  public isTableInited: boolean;
  
  constructor() {
    this.queryChange = new EventEmitter();
  }
  
  public onTapHeaderColumnHandler(headerItem: TableHeaderItemOption): void {
    this.tableBody.setActiveColumn(headerItem);
    this.tableBody.resetAndReload();
  }

  public onChangeQueryHandler(query: Query): void {
    this.fireQueryChangeEvent(query);
  }
  
  public setTableOptions(options: LazyTableOption) {
    this.tableHeader.setHeaderItems(options.headerItems);
    this.tableBody.setColumnOptions(options);
  }
  
  public setSearchString(searchString) {
    this.tableBody.setSearchString(searchString);
  }
  
  public setCustomQuery(customQuery) {
    this.tableBody.setCustomQuery(customQuery);
  }
  
  public setCollection(collection) {
    this.tableBody.setCollection(collection);
  }
  
  public reloadTable(): Promise<any> {
    this.isTableInited = true;
    return this.tableBody.resetAndReload();
  }
  
  public reloadTableByFilterData(filterData): Promise<any> {
    this.tableBody.setFilterData(filterData);
    return this.tableBody.resetAndReload();
  }

  public getTableQueryWithConditions(query: Query): Query {
    return this.tableBody.getTableQuery(query);
  }

  private fireQueryChangeEvent(query: Query): void {
    this.queryChange.next(query);
  }
}
