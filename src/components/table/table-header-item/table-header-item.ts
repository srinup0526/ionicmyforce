import {Component, Input, HostListener, ElementRef, Output, EventEmitter} from '@angular/core';
import { TableHeaderItemOption } from './../table-header/TableHeaderItemOption';

@Component({
  selector: 'table-header-item',
  templateUrl: 'table-header-item.html'
})
export class TableHeaderItemComponent {
  
  @Input() options: TableHeaderItemOption;
  
  @Output() tapSort: EventEmitter<TableHeaderItemOption>;
  
  constructor(private elementRef: ElementRef) {
    this.tapSort = new EventEmitter();
  }
  
  ngOnInit() {
    if(this.options.isActive) {
      this.setSortData();
    }
  }
  
  @HostListener('click') onTapItemHandler() {
    this.setSortData();
  }
  
  private setSortData(){
    if(this.options.isSortable) {
      this.toggleDirection();
      this.toggleClassDirection();
      this.fireEvent();
    }
  }
  
  private fireEvent(): void {
    this.tapSort.next(this.options);
  }
  
  private toggleDirection(): void {
    this.options.isAsc = !this.options.isAsc;
  }
  
  private toggleClassDirection(): void {
    this.setCssClass(!this.options.isAsc,'desc');
  }
  
  private setCssClass(condition: boolean, cssClass: string): void {
    if(condition) {
      return this.elementRef.nativeElement.classList.add(cssClass);
    }
  
    return this.elementRef.nativeElement.classList.remove(cssClass);
  }

}
