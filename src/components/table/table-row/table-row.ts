import {Component, ElementRef, Input} from '@angular/core';
import { TableHeaderItemOption } from '../table-header/TableHeaderItemOption';

@Component({
  selector: 'table-row',
  templateUrl: 'table-row.html'
})
export class TableRowComponent {
  
  @Input() headerItems: Array<TableHeaderItemOption>;
  @Input() record: any;
  
  public columns: any;//Array<{value: string, cssClass: string}>;

  constructor(private element: ElementRef) {
    this.record = {};
    this.headerItems = [];
    this.columns = [];
  }
  
  public ngOnInit() {
    this.columns = this.headerItems.map((item) => {
      return {
        handler: item.handler,
        cssClass: item.cssClass,
        value: this.getCellData(item)
      };
    })
  }
  
  public getCellData(headerOption){
    if(headerOption.modelFunction) {
      return this.record[headerOption.modelFunction]();
    }
  
    if(!headerOption.fields.length) {
      return '';
    }
    
    if(headerOption.picklistValues && (Object.keys(headerOption.picklistValues).length)) {
      return headerOption.picklistValues[this.record[headerOption.fields[0].local] || ''];
    }
    
    return this.record[headerOption.fields[0].local];
  }

}
