import { Field } from './../../../models/base/Field';

export interface TableHeaderItemOption {
  title: string;
  fields?: Array<Field>;
  isSortable?: boolean;
  isAsc?: boolean;
  isActive?: boolean;
  picklistValues?: {[key: string]: string};
  modelFunction?: string;
  cssClass?: any; //{[condition: string]: string}, || Array<string>
  handler?: any,
  isNumber?: boolean
}
