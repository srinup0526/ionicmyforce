import {Component, EventEmitter, Input, Output} from '@angular/core';
import { TableHeaderItemOption } from './TableHeaderItemOption';

@Component({
  selector: 'table-header',
  templateUrl: 'table-header.html'
})
export class TableHeaderComponent {
  public headerItems: Array<TableHeaderItemOption>;
  
  @Output() headerItemTap: EventEmitter<TableHeaderItemOption>;
  
  public activeController: TableHeaderItemOption;
  
  constructor() {
    this.headerItems = [];
    this.headerItemTap = new EventEmitter();
  }
  
  ngOnInit() {
    console.log("table-header ngOnInit");
    console.log(this.headerItems);
  }
  
  public onTapSortColumnHandler(headerItem: TableHeaderItemOption): void {
    this.disActivateLastActiveColumn();
    this.activateHeader(headerItem);
  }
  
  public setHeaderItems(headerItems: Array<TableHeaderItemOption>) {
    this.headerItems = headerItems
  }
  
  private activateHeader(headerItem: TableHeaderItemOption): void {
    this.activeController = headerItem;
    this.activeController.isActive = true;
    
    this.fireHeaderTapEvent(headerItem);
  }
  
  
  private fireHeaderTapEvent(headerItem: TableHeaderItemOption){
    this.headerItemTap.next(headerItem);
  }
  
  private disActivateLastActiveColumn() {
    if(this.activeController) {
      this.activeController.isActive = false;
    }
  }
}
