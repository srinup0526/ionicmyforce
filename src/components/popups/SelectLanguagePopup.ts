import {AlertController} from "ionic-angular";
import {LocalizationManager} from './../../services/common/localizations/LocalizationManager';
import {Injectable} from "@angular/core";

@Injectable()
export class SelectLanguagePopup {
  
  constructor(public alertCtrl: AlertController,
              public localizationManager: LocalizationManager) {
  }
  
  public showPopup(): Promise<any> {
    const alert = this.alertCtrl.create();
    const currentLanguage = this.localizationManager.getCurrentLanguage();
    const languages = this.localizationManager.getLanguages();
    
    alert.setCssClass('popup alert list');
    
    return this.getTitleLocale()
      .then((title) => {
        alert.setTitle(title);
      })
      .then(() => {
        return this.getLanguageLocales(languages)
          .then((languageLocales) => {
            languages.forEach((language) => {
              alert.addInput({
                type: 'radio',
                label: languageLocales[`common.languages.${language.key}`],
                value: language.key,
                checked: currentLanguage == language.key,
                handler: (language) => {
                  this.localizationManager.setLanguage(language.value);
                  alert.dismiss();
                }
              });
            });
            
            return alert.present();
          });
      });
  }
  
  
  private getLanguageLocales(languages: Array<{ key: string, label: string }>): Promise<{ [key: string]: string }> {
    return this.localizationManager.getSeveralLocales(languages.map((language) => `common.languages.${language.key}`));
  }
  
  private getTitleLocale(): Promise<string> {
    return this.localizationManager.promiseLocale('homeMenu.Language');
  }
}
