import { AlertController, AlertOptions, Alert } from 'ionic-angular';
import {LocalizationManager} from './../../services/common/localizations/LocalizationManager';

export class ConfirmPopup {
  constructor(private alertCtrl: AlertController,
              private localizationManager: LocalizationManager) {
  }
  
  public showPopup(localizationKeys: {
    title?: string,
    message?: string,
    yesLabel: string,
    noLabel: string
  }): Promise<{ doAction: boolean }>{

    return this.getLocalization(localizationKeys)
      .then((localization) => this.showConfirmPopup(localization));
  }

  private getLocalization(localizationKeys) {
    return this.localizationManager.getSeveralLocales([
      localizationKeys.title,
      localizationKeys.message,
      localizationKeys.yesLabel,
      localizationKeys.noLabel
    ].filter((key) => !!key)
    ).then((localization) => {
      
      localization['title'] = localization[localizationKeys.title];
      localization['message'] = localization[localizationKeys.message];
      localization['yesLabel'] = localization[localizationKeys.yesLabel];
      localization['noLabel'] = localization[localizationKeys.noLabel];
      
      return localization;
    })
  }
  
  private showConfirmPopup(localization): Promise<{ doAction: boolean }> {
    let cssClasses = 'popup alert confirm';
    
    if(!localization.title){
      cssClasses += ' without-header';
    }
    
    if(!localization.message){
      cssClasses += ' without-msg';
    }
    
    return new Promise((resolve, reject) => {
      const options: AlertOptions = {
        title: localization.title,
        message: localization.message,
        cssClass: cssClasses,
        buttons: [
          {
            text: localization.yesLabel,
            role: 'done',
            handler: () => {
              resolve({
                doAction: true,
              });
            }
          },
          {
            text: localization.noLabel,
            role: 'done',
            cssClass: 'no',
            handler: () => {
              resolve({
                doAction: false,
              });
            }
          },
          {
            text: localization.noLabel,
            role: 'cancel',
            cssClass: 'no hidden',
            handler: () => {
              reject({
                doAction: false,
              });
            }
          }
        ]
      };
      
      const alert: Alert = this.alertCtrl.create(options);
      
      alert.present();
    });
  }
}
