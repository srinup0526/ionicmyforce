import {AlertController} from "ionic-angular";

import {PickListDatasource} from "../../services/db/picklist-managers/base/PicklistDatasource";


export class PicklistPopup {

  constructor(private alertCtrl: AlertController,
              private pickListDatasource: PickListDatasource,
              private title: string = '',
              private isHeaderEnabled = false){
  }
  
  showPopup(defaultId?: string, callback: any = this.updateItems, customCssClass = ''): Promise<any> {
    return this.pickListDatasource.getItems()
      .then((items) => {
        console.log("items",items);
        const updatedItems = callback(items);
        const cssClasses = this.getCssClasses(customCssClass);
        
        return new Promise((resolve, reject) => {
          const alert = this.alertCtrl.create();
  
          alert.setTitle(this.title);
          alert.setCssClass(cssClasses);
  
          updatedItems.forEach((item) => {
            alert.addInput({
              type: 'radio',
              label: item.description,
              value: item.id,
              checked: item.id == defaultId,
              handler: (picklist) => {
                resolve({
                  description: picklist.label,
                  id: picklist.value
                });
                alert.dismiss();
              }
            });
          });
  
          alert.onDidDismiss(() => reject());
  
          return alert.present();
        });
      });
  }
  
  private getCssClasses(customCssClasses: string): string {
    let cssClasses = 'popup alert list ';
    
    if(!this.isHeaderEnabled) {
      cssClasses += ' without-header ';
    }
  
    cssClasses += customCssClasses;
    
    return cssClasses;
  }
  
  private updateItems(items) {
    return items;
  }
  
}
