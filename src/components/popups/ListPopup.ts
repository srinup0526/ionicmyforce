import { AlertController } from 'ionic-angular';

export class ListPopup {
  constructor(private alertCtrl: AlertController,
              private items: any[],
              private title?: string) {
  }

  public showPopup(defaultId?: string): Promise<any> {

    return new Promise((resolve, reject) => {
      let cssClasses = 'popup alert list without-checkbox';

      const alert = this.alertCtrl.create();

      alert.setTitle(this.title || "");

      if(!this.title){
        cssClasses += ' without-header';
      }

      alert.setCssClass(cssClasses);

      this.items.forEach((item) => {
        alert.addInput({
          type: 'radio',
          label: item.description,
          value: item.id,
          checked: item.id == defaultId,
          handler: (item) => {
            resolve({
              description: item.label,
              id: item.value
            });
            alert.dismiss();
          }
        });
      });

      alert.onDidDismiss(() => reject());

      return alert.present();
    });

  }
}
