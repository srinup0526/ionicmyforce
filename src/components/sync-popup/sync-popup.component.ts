import {Component, OnInit, Input} from '@angular/core';
import {ISyncManager} from './../../services/synchronisation/ISyncManager';
import {SyncErrorAlert} from './sync-error-alert';
import {SyncSuccessAlert} from './sync-success-alert';
import { NavParams, ModalController, AlertController, ViewController, Events } from 'ionic-angular';
import {LocalizationManager} from './../../services/common/localizations/LocalizationManager';


@Component({
  selector: 'app-sync-popup.popup.sync',
  templateUrl: './sync-popup.component.html',
  // styleUrls: ['./sync-popup.component.scss'],
})
export class SyncPopupComponent {

  static readonly SYNC_POPUP_CLASSES = ['sync', 'popup'];

  private title: string;
  private statusProgress: number;
  private message: string;
  private syncManager: ISyncManager;

  constructor(private navParams: NavParams,
              private modalController: ModalController,
              private alertCtrl: AlertController,
              public viewCtrl: ViewController,
              public localizationManager: LocalizationManager,
              public events: Events) {

    this.title = 'Synchronization:';
    this.message = '';
    this.statusProgress = 0;
  }

  ionViewWillEnter() {
    this.message = this.navParams.get('message');
    this.statusProgress = this.navParams.get('statusProgress');
    this.syncManager = this.navParams.get('syncManager');
    this.startSynchronization();
  }

  public updateTitle(title: string): void {
    this.title = title;
  }

  public updateMessage(message: string, percentage: number = 0): void {
    this.setProgress(percentage);
    this.setMessage(message);
  }

  private startSynchronization() {
    this.syncManager.startLoading(this.updateMessage.bind(this))
      .then(() => {
        return this.closeModal();
      })
      .then(() => {
        if (this.syncManager.isErrorDuringSync) {
          return this.showSynchronisationSucceededWithErrors();
        }

        return this.showSynchronisationSucceededAlert();
      })
      .then((result) => {
        this.events.publish('synchronization:sync-end', result, Date.now());
      });
  }

  private setProgress(percentage: number): void {
    this.statusProgress = percentage;
  }

  private setMessage(message: string): void {
    this.message = message;
  }

  public closeModal() {
    this.viewCtrl.dismiss();
  }

  private showSynchronisationSucceededAlert() {
    const syncSuccessAlert = new SyncSuccessAlert(this.alertCtrl, this.localizationManager);

    return syncSuccessAlert.show();
  }

  private showSynchronisationSucceededWithErrors(): Promise<{ isError: boolean, showLog: boolean }> {
    const syncErrorAlert = new SyncErrorAlert(this.alertCtrl, this.localizationManager);

    return syncErrorAlert.show();
  }
}
