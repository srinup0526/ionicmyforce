import { AlertController, AlertOptions, Alert } from 'ionic-angular';
import {LocalizationManager} from './../../services/common/localizations/LocalizationManager';

export class SyncErrorAlert {
  constructor(private alertCtrl: AlertController,
              private localizationManager: LocalizationManager) {
  }

  public show(): Promise<{ isError: boolean, showLog: boolean }> {
    return this.localizationManager.getSeveralLocales([
      'synchronizationPopup.SynchronizationStatus',
      'synchronizationPopup.SyncWithErrorOnUpload',
      'common.buttons.YesBtn',
      'common.buttons.NoBtn'
    ]).then((locales) => {
      return this.showSynchronisationSucceededWithErrors(locales);
    });
  }

  private showSynchronisationSucceededWithErrors(locales): Promise<{ isError: boolean, showLog: boolean }> {
    const title: string = locales['synchronizationPopup.SynchronizationStatus'];
    const message: string = locales['synchronizationPopup.SyncWithErrorOnUpload'];
    const yesLabel: string = locales['common.buttons.YesBtn'];
    const noLabel: string = locales['common.buttons.NoBtn'];
    
    return new Promise((resolve, reject) => {
      const options: AlertOptions = {
        title: title,
        message: message,
        cssClass: 'popup alert confirm',
        buttons: [
          {
            text: yesLabel,
            role: 'cancel',
            handler: () => {
              resolve({
                isError: true,
                showLog: true
              });
            }
          },
          {
            text: noLabel,
            role: 'cancel',
            cssClass: 'no',
            handler: () => {
              console.log('Cancel clicked');
              resolve({
                isError: true,
                showLog: false
              });
            }
          }
        ]
      };

      const alert: Alert = this.alertCtrl.create(options);

      alert.present();
    });
  }
}
