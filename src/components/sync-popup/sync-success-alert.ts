import { AlertController, AlertOptions, Alert } from 'ionic-angular';
import {LocalizationManager} from './../../services/common/localizations/LocalizationManager';

export class SyncSuccessAlert {
  constructor(private alertCtrl: AlertController,
              private localizationManager: LocalizationManager) {
  }

  public show(): Promise<{ isError: boolean; showLog: boolean; }> {
    return this.localizationManager.getSeveralLocales([
      'synchronizationPopup.SynchronizationStatus',
      'synchronizationPopup.SynchronizationCompleted',
      'common.buttons.OkBtn'
    ])
      .then((locales) => {
      return this.showSynchronisationSucceededAlert(locales);
    });
  }

  private showSynchronisationSucceededAlert(locales): Promise<{ isError: boolean; showLog: boolean; }> {
    const title: string = locales['synchronizationPopup.SynchronizationStatus'];
    const message: string = locales['synchronizationPopup.SynchronizationCompleted'];
    const okLabel: string = locales['common.buttons.OkBtn'];
    
    return new Promise(async (resolve, reject) => {

      const options: AlertOptions = {
        title: title,
        message: message,
        cssClass: 'popup alert single-button',
        buttons: [
          {
            text: okLabel,
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
              resolve({
                isError: false,
                showLog: false
              });
            }
          }
        ]
      };

      const alert: Alert = await this.alertCtrl.create(options);

      alert.present();
    });
  }
}
