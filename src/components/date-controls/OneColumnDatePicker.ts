import {PickerController} from "ionic-angular";
import moment from 'moment';
import {Picker, PickerOptions, PickerColumn, PickerColumnOption} from "ionic-angular";
import {Injectable} from "@angular/core";
import { LocalizationManager } from './../../services/common/localizations/LocalizationManager';

@Injectable({
  providedIn: "root"
})
export class OneColumnDatePicker {
  
  static readonly BEFORE_DAYS: number = 180;
  static readonly AFTER_DAYS: number = 180;
  
  private format: string;
  
  constructor(private pickerCtrl: PickerController,
              private localizationManager: LocalizationManager) {
  }
  
  
  showPicker(format: string = 'dd MMM DD YYYY'): Promise<PickerColumn | null> {
    this.setFormat(format);
    
    return this.getLocalization()
      .then((localization) => {
        return new Promise((resolve, reject) => {
          
          let opts: PickerOptions = {
            cssClass: 'datetime-picker filter-datepicker',
            buttons: [
              {
                text: localization['common.buttons.CancelBtn'],
                role: 'cancel',
                handler: () => {
                  resolve(null);
                }
              },
              {
                text: localization['common.buttons.SaveBtn'],
                role: 'done',
                handler: (data: any) => {
                  resolve(data.date);
                }
              }
            ],
            columns: this.getPickerColumn()
          };
    
          let picker: Picker = this.pickerCtrl.create(opts);
    
          picker.present();
    
          picker.onDidDismiss(() => {
            resolve(null);
          })
        });
      })
  }
  
  private getLocalization(): Promise<{[key: string]: string}> {
    return this.localizationManager.getSeveralLocales([
      'common.buttons.SaveBtn',
      'common.buttons.CancelBtn'
    ]);
  }
  
  private setFormat(format: string) {
    this.format = format;
  }
  
  private getPickerColumn(): Array<PickerColumn> {
    const items = this.prepareListOfDate();
    const selectedIndex = this.getActiveIndex(items);
    
    return [
      {
        name: 'date',
        options: items,
        selectedIndex: selectedIndex
      }
    ];
  }
  
  private getActiveIndex(item: Array<PickerColumnOption>) {
    const currentDate = moment().format('dd MMM DD YYYY');
    
    return item.reduce((selected: number, currentColumn: PickerColumnOption, currentIndex: number) => {
      if (currentColumn.text == currentDate) {
        selected = currentIndex;
      }
      
      return selected;
    }, 0);
  }
  
  private prepareListOfDate(): Array<PickerColumnOption> {
    let items = this.generateItems();
    
    return items.map((date) => {
      const formatedDate = date.format('dd MMM DD YYYY');
      
      return {
        text: formatedDate,
        value: date
      }
    })
  }
  
  
  private generateItems(): Array<moment.Moment> {
    const daysBefore = new Array(OneColumnDatePicker.BEFORE_DAYS)
      .fill('')
      .map((item, index) => moment()
        .subtract(index, "days")
      );
    
    const currentDate = moment();
    
    const daysAfter = new Array(OneColumnDatePicker.AFTER_DAYS)
      .fill('')
      .map((item, index) => moment()
        .add(index, "days")
      );
    
    daysBefore.splice(0, 1);
    daysAfter.splice(0, 1);
    
    daysBefore.reverse();
    
    daysBefore.push(currentDate);
    
    return daysBefore.concat(daysAfter);
  }
  
}
