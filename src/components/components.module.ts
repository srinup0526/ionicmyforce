import { NgModule } from '@angular/core';
import { ProgressBarComponent } from './progress-bar/progress-bar';
import { PopoverComponent } from './popover/popover';
import { SyncPopupComponent } from './sync-popup/sync-popup.component';
import { BottomMenuComponent } from './bottom-menu/bottom-menu';
import { PinComponent } from './pin/pin/pin';
import { PinConfirmComponent } from './pin/pin-confirm/pin-confirm';
import { TableHeaderItemComponent } from './table/table-header-item/table-header-item';
import { TableHeaderComponent } from './table/table-header/table-header';
import { LazyTableComponent } from './table/lazy-table/lazy-table';
import { TableBodyComponent } from './table/table-body/table-body';
import { TableRowComponent } from './table/table-row/table-row';
import { FilterPanelComponent } from './filter/filter-panel/filter-panel';
import { FilterPanelDateComponent } from './filter/filter-panel-date/filter-panel-date';
import { FilterPanelDateRangeComponent } from './filter/filter-panel-date-range/filter-panel-date-range';
import { FilterPanelInputComponent } from './filter/filter-panel-input/filter-panel-input';
import { FilterPanelPicklistComponent } from './filter/filter-panel-picklist/filter-panel-picklist';
import { FilterPanelCheckboxComponent } from './filter/filter-panel-checkbox/filter-panel-checkbox';
import { TradeModuleComponent } from './trade-module/trade-module';
import { OrderItemListComponent } from './order-management/order-item-list/order-item-list';
import { OrderSampleItemListComponent } from './order-management/order-sample-item-list/order-sample-item-list';
import { CreateOrderLinePopupComponent } from './order-management/create-order-line-popup/create-order-line-popup';
import { ReferenceSelectPopupComponent } from './reference-select-popup/reference-select-popup';
// import { SalesplanpopoverComponent } from './salesplanpopover/salesplanpopover';
// import { SalesplanestimatedComponent } from './salesplanestimated/salesplanestimated';

@NgModule({
	declarations: [
	  ProgressBarComponent,
    PopoverComponent,
    SyncPopupComponent,
    BottomMenuComponent,
    PinComponent,
    PinConfirmComponent,
    TableHeaderItemComponent,
    TableHeaderComponent,
    LazyTableComponent,
    TableBodyComponent,
    TableRowComponent,
    FilterPanelComponent,
    FilterPanelComponent,
    FilterPanelDateComponent,
    FilterPanelDateRangeComponent,
    FilterPanelInputComponent,
    FilterPanelPicklistComponent,
    FilterPanelCheckboxComponent,
    TradeModuleComponent,
    OrderItemListComponent,
    OrderSampleItemListComponent,
    CreateOrderLinePopupComponent,
    ReferenceSelectPopupComponent,
    // SalesplanpopoverComponent,
    // SalesplanestimatedComponent,
  ],
	imports: [],
	exports: [
	  ProgressBarComponent,
    PopoverComponent,
    SyncPopupComponent,
    BottomMenuComponent,
    PinComponent,
    PinConfirmComponent,
    TableHeaderItemComponent,
    TableHeaderComponent,
    LazyTableComponent,
    TableBodyComponent,
    TableRowComponent,
    FilterPanelComponent,
    FilterPanelComponent,
    FilterPanelDateComponent,
    FilterPanelDateRangeComponent,
    FilterPanelInputComponent,
    FilterPanelPicklistComponent,
    FilterPanelCheckboxComponent,
    TradeModuleComponent,
    OrderItemListComponent,
    OrderSampleItemListComponent,
    CreateOrderLinePopupComponent,
    ReferenceSelectPopupComponent,
    // SalesplanpopoverComponent,
    // SalesplanestimatedComponent,

  ]
})
export class ComponentsModule {}
