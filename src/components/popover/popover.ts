import { Component } from '@angular/core';
import {  ViewController,ToastController,NavController, NavParams} from 'ionic-angular';
import moment from 'moment';
import { MTPCollection } from '../../collections/MTPCollection';
import { MTP } from '../../models/MTP';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { MtpPage } from '../../pages/mtp/mtp'
import { Utils } from '../../utils/Utils';
//import { SmartstoreServiceProvider } from '../../providers/smartstore-service/smartstore-service';
/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
declare var cordova: any;
@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class  PopoverComponent {
  Month__c: any = "";
  text: string;
  MTPPage:any;
  Year__c: any = "";
  public MTP: any;
  message = "";
  mtpRecords:any=[];
  mtpYears: any=[];
  public activeUser:any;
  // public smartStore(): any {
  //   return cordova.require("com.salesforce.plugin.smartstore")
  // }
  constructor(public viewCtrl: ViewController,
              public navCtrl: NavController,
              public toastCtrl: ToastController,
              public navParams: NavParams,
              private mtpCollection: MTPCollection) {
    console.log('Hello PopoverComponent Component');
    SforceDataContext.getActiveUser()
    .then((currentUser) => {
      console.log("currentUser",currentUser);
      this.activeUser = currentUser;
    });
  }
  // dismiss(isCancel) {
  //   if(isCancel)
  //   this.viewCtrl.dismiss(undefined);
  //   else
  //   this.viewCtrl.dismiss({Month:this.Month__c, Year:this.Year__c})
  // }
  monthfilter(){
   // console.log(this.filtermonthwise);
    }
   onCreate(){

   }
   ionViewDidLoad() {
    //this.MTPPage=this.navParams.get("parentPage");
    console.log("MTP edit",this.navParams.data['row']);
    if(this.navParams.data['row']) {
      this.Month__c = this.navParams.data['row'].month;
      this.Year__c = this.navParams.data['row'].year;
    } else {
      //this.mtpRecords = this.MTPPage.MTP;
      //console.log("this.mtpRecords",this.mtpRecords);
    }

    let start_year = new Date().getFullYear();
    for (var i = start_year; i < start_year +2; i++) {
      this.mtpYears.push(i);
    }
   }
  
validateInputData()
{



}


validateInput()
  {
    let isValid = true;
    this.message = "";

    console.log("MTP.Month",this.Month__c);
    if((this.Month__c=='' || this.Month__c==null))
    {
      isValid = false;
      this.message+="\n Month Mandatory";
    }
    if((this.Year__c=='' || this.Year__c==null))
    {
      isValid = false;
      this.message+="\n Year Mandatory";
    }

    if(!Utils.perviousMonthCheck(this.Month__c, this.Year__c))
    {
      isValid = false;
      this.message+="\n Not allowed to create MTP for previous month, you can try with future month";
    }

    this.mtpCollection.findMTPForMonth(this.Month__c,this.Year__c).then(
      res=>{
        if(res)
        {
          this.message+= " \n MTP already existed for chosen month, you can try with other";
          isValid =  false;
        }
        else {
          isValid = isValid;
        }
        if(isValid)
        {
          this.saveMTP();
        }
        else
        {
          const toast = this.toastCtrl.create({
            message: this.message,
            showCloseButton:true,
            cssClass:"toastClass",
            duration:7000,
          });
          toast.present();
        }
      })
    //console.log("return isvalid");
    //return isValid;
  }
  daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
}
cancelInput()
{
  this.viewCtrl.dismiss();
}

  saveMTP(){
    console.log("startDate",Utils.firstDateCurrentMonth(this.Month__c, this.Year__c), Utils.endDateCurrentMonth(this.Month__c, this.Year__c));
    let mtp;
    
    if(this.navParams.data['row']) {
      mtp = this.navParams.data['row'];
      mtp.month = this.Month__c;
      mtp.year = this.Year__c;
      mtp.startDate = Utils.firstDateCurrentMonth(this.Month__c, this.Year__c);
      mtp.endDate = Utils.endDateCurrentMonth(this.Month__c, this.Year__c);
    } else {
      mtp = new MTP({
        createdOffline: true,
        medRep: this.activeUser.id,
        userLastName: this.activeUser.firstName,
        userFirstName: this.activeUser.lastName,
        startDate: Utils.firstDateCurrentMonth(this.Month__c, this.Year__c),
        endDate: Utils.endDateCurrentMonth(this.Month__c, this.Year__c),
        Month__c: this.Month__c,
        Year__c: this.Year__c,
        Status__c: "Draft"
      });
    }
    
    
      console.log("savemtp", mtp);
      
      if(this.navParams.data['row']) {
        this.mtpCollection.updateEntity(mtp)
        .then(createdData=>{
          console.log("updateddata",createdData);
          if(createdData)
          {
            success(createdData);
            return this.closePopover(createdData[0]);
          }
        })
      } else {
        this.mtpCollection.createEntity(mtp)
        .then(createdData=>{
          console.log("createdData",createdData);
          if(createdData)
          {
            success(createdData);
            return this.closePopover(createdData);
          }
        })
      }
       
    
       let success = (data) => {
         console.log("Data Upserted successfully and created data is",data);
         //this.navParams.get("parentPage").reloadMTP();
         // this.navCtrl.pop();
         const toast = this.toastCtrl.create({
          message: "MTP Created Successfully!",
          showCloseButton:true,
          cssClass:"toastClass",
          position:'middle',
          duration:3000,
        });
        toast.present();
       }
       let failure = (error) => {
         console.log("Data Upsertion having an issue",error);
       }
   }
   
   private closePopover(params?): Promise<any> {
     return this.viewCtrl.dismiss(params);
   }
 }
