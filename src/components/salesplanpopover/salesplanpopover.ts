import { Component } from '@angular/core';
import { ViewController, ToastController, NavController, NavParams } from 'ionic-angular';
import moment from 'moment';
// import { MTPCollection } from '../../collections/MTPCollection';
// import { MTP } from '../../models/MTP';
import { SalesPlanningCollection } from '../../collections/SalesPlanningCollection';
import { SalesPlan } from '../../models/SalesPlan';
import { SforceDataContext } from '../../collections/SforceDataContext';
import { MtpPage } from '../../pages/mtp/mtp'
import { Utils } from '../../utils/Utils';
declare var cordova: any;
@Component({
  selector: 'salesplanpopover',
  templateUrl: 'salesplanpopover.html'
})
export class SalesplanpopoverComponent {
  Month__c: any;
  text: string;
  SPPage: any;
  Year__c: any;
  public salesplan: any;
  message = "";
  spRecords: any = [];
  spYears: any = [];
  public activeUser: any;
  // public smartStore(): any {
  //   return cordova.require("com.salesforce.plugin.smartstore")
  // }
  constructor(public viewCtrl: ViewController,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    private spCollection: SalesPlanningCollection) {
    console.log('Hello PopoverComponent Component');
    SforceDataContext.getActiveUser()
      .then((currentUser) => {
        console.log("currentUser", currentUser);
        this.activeUser = currentUser;
      });
  }

  monthfilter() {
    // console.log(this.filtermonthwise);
  }
  onCreate() {

  }
  ionViewDidLoad() {
    console.log("sales plan edit", this.navParams.data['row']);
  

    let start_year = new Date().getFullYear();
    for (var i = start_year; i < start_year +2; i++) {
      this.spYears.push(i);
    }
   }

  

  validateInputData() {



  }


  validateInput() {
    let isValid = true;
    this.message = "";

    console.log("SALES PLAN Month", this.Month__c);
    if ((this.Month__c == '' || this.Month__c == null)) {
      isValid = false;
      this.message += "\n Month Mandatory";
    }
    if ((this.Year__c == '' || this.Year__c == null)) {
      isValid = false;
      this.message += "\n Year Mandatory";
    }

    if (!Utils.perviousMonthCheck(this.Month__c, this.Year__c)) {
      isValid = false;
      this.message += "\n Not allowed to create Sales Plan for previous month, you can try with future month";
    }

    this.spCollection.findSalesPlanForMonth(this.Month__c, this.Year__c).then(
      res => {
        if (res) {
          this.message += " \n Sales Plan already existed for chosen month, you can try with other";
          isValid = false;
        }
        else {
          isValid = isValid;
        }
        if (isValid) {
          this.saveSalesPlanData();
        }
        else {
          const toast = this.toastCtrl.create({
            message: this.message,
            showCloseButton: true,
            cssClass: "toastClass",
            duration: 7000,
          });
          toast.present();
        }
      })
    //console.log("return isvalid");
    //return isValid;
  }
  daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }
  cancelInput() {
    this.viewCtrl.dismiss();
  }

  saveSalesPlanData() {
   
    let sp;
    if (this.navParams.data['row']) {
      sp = this.navParams.data['row'];
      sp.month = this.Month__c;
      sp.year = this.Year__c;
    } else {
      sp = new SalesPlan({
        month: this.Month__c,
        year: this.Year__c,
        status: "New"
      });
    }
 console.log("savesalesplan", sp);

    if (this.navParams.data['row']) {
      this.spCollection.updateEntity(sp)
        .then(createdData => {
          console.log("updateddata", createdData);
          if (createdData) {
            success(createdData);
            return this.closePopover(createdData[0]);
          }
        })
    } else {
      this.spCollection.createEntity(sp)
        .then(createdData => {
          console.log("createdData", createdData);
          if (createdData) {
            success(createdData);
            return this.closePopover(createdData);
          }
        })
    }


    let success = (data) => {
      console.log("Data Upserted successfully and created data is", data);
      //this.navParams.get("parentPage").reloadMTP();
      // this.navCtrl.pop();
      const toast = this.toastCtrl.create({
        message: "Sales Plan Created",
        showCloseButton: true,
        cssClass: "toastClass",
        position: 'middle',
        duration: 3000,
      });
      toast.present();
    }
    let failure = (error) => {
      console.log("Data Upsertion having an issue", error);
    }
  }

  private closePopover(params?): Promise<any> {
    return this.viewCtrl.dismiss(params);
  }
}
