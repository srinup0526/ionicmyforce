import {Component, ElementRef, Input} from '@angular/core';
import {Field} from "../../../models/base/Field";
import {Query} from "../../../services/common/Query";
import {PickerColumnOption, PickerOptions} from "ionic-angular";
import {Utils} from "../../../utils/Utils";
import moment from 'moment';


@Component({
  selector: 'filter-panel-date-range',
  templateUrl: 'filter-panel-date-range.html'
})
export class FilterPanelDateRangeComponent {
  
  static readonly format: string = 'dd MMM DD YYYY';
  static readonly dateTimeFormat: string = 'YYYY-MM-DDTHH:mm:ss.SSSZZ';
  
  @Input() label: string;
  @Input() value: string;
  @Input() field: Field;
  @Input() condition: string;
  @Input() joinWith: string;
  
  public pickerOptions: PickerOptions;
  
  constructor(private elementRef: ElementRef) {
    this.value = this.value || '';
    this.joinWith = this.joinWith || Query.AND;
  
    this.pickerOptions = {
      cssClass: 'datetime-picker'
    }
  }
  
  public onChangeValueHandler(event) {
    if(!event.year) {
      return;
    }
    
    const date = new Date(event.year,  event.month - 1, event.day);
    
    this.formDataAndDispatch(moment(date));
  }
  
  public onTapCancelBtnHandler() {
    this.value = '';
    this.formDataAndDispatch();
  }
  
  public maxDate() {
    const afterDays = 180;
    
    return moment().add(afterDays, 'days').format("YYYY-MM-DD");
  }
  
  public minDate() {
    const beforeDays = 180;
    
    return moment().subtract(beforeDays, 'days').format("YYYY-MM-DD");
  }
  
  private formDataAndDispatch(date?) {
    let fields = {};
  
    fields[this.field.sfdc || this.field.local] =  date ? this.formatDate(date) : null;
  
    this.dispathChange([{ fields: fields, query: 'where', joinWith: this.joinWith, condition: this.condition }]);
  }
  
  private formatDate(date) {
    if(this.condition == Query.LR || this.condition == Query.LRE) {
      return moment(Utils.localToUtc(date.endOf('day')))
        .format(FilterPanelDateRangeComponent.dateTimeFormat);
    }
    
    if(this.condition == Query.GR || this.condition == Query.GRE) {
      return moment(Utils.localToUtc(date.startOf('day')))
        .format(FilterPanelDateRangeComponent.dateTimeFormat);
    }
    
    return moment(Utils.localToUtc(date))
      .format(FilterPanelDateRangeComponent.dateTimeFormat)
  }
  
  private dispathChange(data: Array<{fields: {[key: string]: string}, query: string, joinWith: string, condition: string }>): void {
    const customEvent = new CustomEvent('changeFilter', { bubbles: true, detail: data });
    this.elementRef.nativeElement.dispatchEvent(customEvent);
  }
}
