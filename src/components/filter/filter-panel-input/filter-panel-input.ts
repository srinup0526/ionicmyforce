import {Component, EventEmitter, Input, Output, ElementRef, ViewChild} from '@angular/core';
import {Field} from "../../../models/base/Field";
import {Query} from "../../../services/common/Query";
import {IonInput} from "@ionic/angular";
import {TextInput} from "ionic-angular";


@Component({
  selector: 'filter-panel-input',
  templateUrl: 'filter-panel-input.html'
})
export class FilterPanelInputComponent {
  
  @Input() label: string;
  @Input() value: string;
  @Input() placeholder: string;
  @Input() field: Field;
  @Input() joinWith: string;
  @ViewChild('filterInput') input: TextInput;

  constructor(private elementRef: ElementRef) {}
  
  ngOnInit() {
    this.placeholder = this.placeholder || 'FilterPanel.TypeFilterText';
    this.value = this.value || '';
    this.joinWith = this.joinWith || Query.AND;

    this.input.blur
      .subscribe(() => this.onBlurHandler(this.value));
  }

  public onBlurHandler(value = '') {
    let fields = {};

    fields[this.field.sfdc || this.field.local] = value;
    
    console.log(fields);
    
    this.dispathChange([{ fields: fields, query: 'whereLike', joinWith: this.joinWith }]);
  }
  
  private dispathChange(data: Array<{fields: {[key: string]: string}, query: string, joinWith: string }>): void {
    const customEvent = new CustomEvent('changeFilter', { bubbles: true, detail: data });
    this.elementRef.nativeElement.dispatchEvent(customEvent);
  }
}
