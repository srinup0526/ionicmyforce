import {Component, ElementRef, EventEmitter, Input, Output, HostListener, ViewChild} from '@angular/core';
import {Button, ModalController} from "ionic-angular";
import { SalesplanmodelPage } from '../../../pages/salesplanmodel/salesplanmodel';
declare var $: any;

@Component({
  selector: 'filter-panel',
  templateUrl: 'filter-panel.html'
})
export class FilterPanelComponent {

  @Input() content: Array<any>;
  
  allPanelValues: Array<any>;

  @Output() filtersChangeEvent: EventEmitter<any>;
  
  constructor(private elementRef: ElementRef, public modalCtrl: ModalController) {
    this.filtersChangeEvent = new EventEmitter();
  }
  
  ngOnInit() {
    this.setStylesToContent();
  }
  
  public onTapOpenBtnHandler(event) {
    event.stopPropagation();
    this.togglePanel()
  }
  
  public showPanel() {
    this.elementRef.nativeElement.classList.add('open');
  }
  
  public hidePanel() {
    this.elementRef.nativeElement.classList.remove('open');
  }
  
  public onChangeFilterItemHandler(data) {
    this.fireFiltersChangeEvent(data.detail);
  }
  
  private setStylesToContent() {
    return this.content
      .map((element) => element.getNativeElement ? element.getNativeElement() : element)
      .forEach((element) => {
        element.style.transform = "translate3d(0,0,0)";
      });
  }
  
  private togglePanel() {
    this.toggleSideMenu();
  }
  
  private toggleSideMenu() {
    this.elementRef.nativeElement.classList.toggle('open');
  }

  // itemSelected(item: string) {
  //   console.log("Selected Item", item);
  //   const modal = this.modalCtrl.create(SalesplanmodelPage);
  //   modal.present();
  // }


  private fireFiltersChangeEvent(filterValuesList) {
    const filterValues = this.prepareFilterValues(filterValuesList);
    
    this.filtersChangeEvent.next(filterValues);
  }
  
  private prepareFilterValues(filterValuesList) {
    this.allPanelValues = this.allPanelValues || [];
    
    filterValuesList.forEach((filterValues) => {
      let existedFilterSchema = this.getExistedFilterSchemaForValue(filterValues);
      
      Object.keys(filterValues.fields).forEach((key) => {
        const value = filterValues.fields[key];
        
        if (value) {
          existedFilterSchema.fields[key] = value;
        }
        else {
          delete existedFilterSchema.fields[key];
        }
      })
      
    });
    
    return this.allPanelValues;
  }
  
  private getExistedFilterSchemaForValue(value) {
    let existedFilter = this.allPanelValues
      .filter((filter) => {
        return filter.query == value.query && filter.condition == value.condition && filter.joinWith == value.joinWith;
      })[0];
    
    if (!existedFilter) {
      this.allPanelValues.push(value);
      existedFilter = value;
    }
    
    return existedFilter;
  }
}
