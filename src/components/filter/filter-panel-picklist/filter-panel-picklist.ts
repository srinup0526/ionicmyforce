import {Component, EventEmitter, Input, Output, ElementRef, ViewChild, HostListener} from '@angular/core';
import {Field} from "../../../models/base/Field";
import {Query} from "../../../services/common/Query";
import {PicklistPopup} from "../../popups/PicklistPopup";
import {AlertController, TextInput} from "ionic-angular";
import {PickListDatasource} from "../../../services/db/picklist-managers/base/PicklistDatasource";


@Component({
  selector: 'filter-panel-picklist',
  templateUrl: 'filter-panel-picklist.html'
})
export class FilterPanelPicklistComponent {
  
  @Input() label: string;
  @Input() value: { id: string, description: string };
  @Input() placeholder: string;
  @Input() field: Field;
  @Input() picklistDatasource: PickListDatasource;
  @Input() joinWith: string;
  @ViewChild('filterInput') input: TextInput;
  
  constructor(private elementRef: ElementRef,
              private alertCtrl: AlertController) {
    this.value = {
      id: '',
      description: ''
    };
  }
  
  ngOnInit() {
    this.placeholder = this.placeholder || '';
    this.joinWith = this.joinWith || Query.AND;
  }
  
  @HostListener('click')
  onTapHandler() {
    this.showPopup()
      .then(() => {
        this.formQueryDataAndDispatch();
      });
  }
  
  private formQueryDataAndDispatch() {
    let fields = {};
    
    fields[this.field.sfdc || this.field.local] = this.value.id;
    
    console.log(fields);
    
    this.dispathChange([{fields: fields, query: 'whereLike', joinWith: this.joinWith}]);
  }
  
  private dispathChange(data: Array<{ fields: { [key: string]: string }, query: string, joinWith: string }>): void {
    const customEvent = new CustomEvent('changeFilter', {bubbles: true, detail: data});
    this.elementRef.nativeElement.dispatchEvent(customEvent);
  }
  
  public showPopup(): Promise<any> {
    const picklistPopup = new PicklistPopup(this.alertCtrl, this.picklistDatasource);
    
    return picklistPopup.showPopup(this.value.id)
      .then((result) => {
        if (result) {
          this.value = result;
        }
      });
  }
}

