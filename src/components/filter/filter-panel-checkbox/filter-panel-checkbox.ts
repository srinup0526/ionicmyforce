import {Component, EventEmitter, Input, Output, ElementRef} from '@angular/core';
import {Field} from "../../../models/base/Field";
import {Query} from "../../../services/common/Query";


@Component({
  selector: 'filter-panel-checkbox',
  templateUrl: 'filter-panel-checkbox.html'
})
export class FilterPanelCheckboxComponent {
  
  @Input() label: string;
  @Input() value: boolean;
  @Input() placeholder: string;
  @Input() field: Field;
  @Input() joinWith: string;
  
  @Output() changeFilter: EventEmitter<Array<{fields: {[key: string]: string}, query: string, joinWith: string }>>;
  
  constructor(private elementRef: ElementRef) {
    this.changeFilter = new EventEmitter();
  }
  
  ngOnInit() {
    this.placeholder = this.placeholder || '';
    this.value = this.value || false;
    this.joinWith = this.joinWith || Query.AND;
  }
  
  public onCheckboxChangeHandler(value) {
    let fields = {};
    
    fields[this.field.sfdc || this.field.local] = value;
    
    console.log(fields);
    
    this.dispathChange([{ fields: fields, query: 'whereLike', joinWith: this.joinWith }]);
  }
  
  private dispathChange(data: Array<{fields: {[key: string]: string}, query: string, joinWith: string }>): void {
    const customEvent = new CustomEvent('changeFilter', { bubbles: true, detail: data });
    this.elementRef.nativeElement.dispatchEvent(customEvent);
  }

}
