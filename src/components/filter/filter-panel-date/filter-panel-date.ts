import { Component } from '@angular/core';

/**
 * Generated class for the FilterPanelDateComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'filter-panel-date',
  templateUrl: 'filter-panel-date.html'
})
export class FilterPanelDateComponent {

  text: string;

  constructor() {
    console.log('Hello FilterPanelDateComponent Component');
    this.text = 'Hello World';
  }

}
