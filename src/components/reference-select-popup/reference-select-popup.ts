import { Component, ViewChild } from '@angular/core';
import { LazyTableOption } from './../../components/table/lazy-table/LazyTableOption';
import { LazyTableComponent } from './../../components/table/lazy-table/lazy-table';
import {ReferenceCollection} from "../../collections/references/ReferenceCollection";
import {Settings} from "../../services/db/Settings";
import {NonTargetReferencesCollection} from "../../collections/references/NonTargetReferencesCollection";
import {ReferenceScheme} from "../../models/scheme/ReferenceScheme";
import {NavParams, ToastController, ViewController} from 'ionic-angular';
import {Reference} from "../../models/Reference";
import {LocalizationManager} from "../../services/common/localizations/LocalizationManager";

@Component({
  selector: 'reference-select-popup',
  templateUrl: 'reference-select-popup.html'
})
export class ReferenceSelectPopupComponent {
  static readonly CSS_ACTIVE_CLASS = 'active';
  
  private searchString: string;
  private collection: ReferenceCollection;
  private tableOptions: LazyTableOption;
  private callback;
  private selectedReference: Reference;
  
  @ViewChild(LazyTableComponent) lazyTable: LazyTableComponent;

  constructor(private nonTargetReferencesCollection: NonTargetReferencesCollection,
              private navParams: NavParams,
              private viewCtrl: ViewController,
              private toastCtrl: ToastController,
              private localizationManager: LocalizationManager,
              private referenceCollection: ReferenceCollection) {
  }
  
  public onTapCreateBtnHandler() {
    if (this.selectedReference && this.selectedReference.id) {
      return this.closeModalPopup({
        reference: this.selectedReference
      });
    }
    
    return this.localizationManager.promiseLocale('ReferenceSelectPopup.SelectReference')
      .then((toastMsg) => {
        return this.showValidateToast(toastMsg);
      });
  }
  
  public onTapCancelBtnHandler() {
    this.closeModalPopup();
  }
  
  public onSearchHandler(searchBar: any) {
    this.searchString = searchBar.value || '';
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onClearHandler(ev: any): void {
    ev.target.value = '';
    this.searchString = ev.target.value;
    
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.reloadTable();
  }
  
  public onTapRowHandler(event, record): void {
    ReferenceScheme.activeRow = record.id;
    this.selectedReference = record;
  
    this.updateSelectedElement(record.id);
  }
  
  public ngOnInit() {
    this.initParams();
    this.initTable();
    this.prepareTable();
  }
  
  private prepareTable(): void {
    this.lazyTable.setTableOptions(this.tableOptions);
    this.lazyTable.setSearchString(this.searchString);
    this.lazyTable.setCollection(this.collection);
  
    this.lazyTable.reloadTable()
      .then(() => {
        this.updateSelectedElement(ReferenceScheme.activeRow);
      })
  }
  
  private initParams(): void {
    ReferenceScheme.activeRow = this.navParams.data['id'];
    
    this.callback = this.navParams.data['callback'];
  }
  
  private initTable(): void {
    this.setTableCollection();
    
    this.tableOptions = {
      headerItems: [
        {
          title: '',
          fields: [this.collection.scheme.fields.id],
          modelFunction: 'getIdForTableColumn'
        },
        {
          title: 'common.names.Contact',
          fields: [this.collection.scheme.fields.name],
          isSortable: true,
          isAsc: true,
          isActive: true
        },
        {
          title: 'common.names.Organization',
          fields: [this.collection.scheme.fields.organizationName],
          isSortable: true,
          isAsc: true,
          isActive: false
        }
      ],
      rowHandler: this.onTapRowHandler.bind(this),
      batchSize: 100
    };
    
    this.searchString = '';
  }
  
  private setTableCollection() {
    if (Settings.getInstance().isChinaUser()) {
      return this.collection = this.nonTargetReferencesCollection;
    }
  
    return this.collection = this.referenceCollection;
  }
  
  private updateSelectedElement(productId) {
    const element = document.querySelectorAll('.reference-table table-row .cell:first-of-type .reference-id');
    
    [].slice.call(element)
      .filter((item) => {
        this.resetActiveElement(item.parentElement);
        
        return item.innerText == productId;
      })
      .map((item) => this.setActiveElement(item.parentElement))
  }
  
  private resetActiveElement(element): void {
    if (element && element.classList.contains(ReferenceSelectPopupComponent.CSS_ACTIVE_CLASS)) {
      element.classList.remove(ReferenceSelectPopupComponent.CSS_ACTIVE_CLASS);
    }
  }
  
  private setActiveElement(element): void {
    if (element) {
      element.classList.add(ReferenceSelectPopupComponent.CSS_ACTIVE_CLASS);
    }
  }
  
  private closeModalPopup(options?) {
    this.viewCtrl.dismiss(options);
  }
  
  private showValidateToast(msg: string): Promise<any> {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton: true,
      cssClass: "toastClass",
      duration: 5000,
    });
    
    return toast.present();
  }
}
