import {Component, Input, Output, EventEmitter} from '@angular/core';
import {IqviaQuestion} from './../../models/IqviaQuestion';
import {IqviaAccountSkuTask} from './../../models/IqviaAccountSkuTask';
import {IqviaAccountSkuTasksCollection} from "../../collections/IqviaAccountSkuTasksCollection";
import {IqviaQuestionsCollection} from "../../collections/IqviaQuestionsCollection";
import {CallsCollection} from "../../collections/CallReportsCollection/CallsCollection"
import moment from 'moment';
import {IqviaTaskAdjustmentScheme} from "../../models/scheme/IqviaTaskAdjustmentScheme";
import {IqviaTaskAdjustmentsCollection} from "../../collections/IqviaTaskAdjustmentsCollection";

@Component({
  selector: 'trade-module',
  templateUrl: 'trade-module.html'
})
export class TradeModuleComponent {

  @Input('callReport') callReport: any = null;
  @Input('cantactId') cantactId: string;
  @Input('organizationId') organizationId: string;
  @Input('date') date: any;

  @Output() change = new EventEmitter();

  accountSkuTasks: IqviaAccountSkuTask[] = [];
  questions: IqviaQuestion[] = [];
  answers: any = {};
  previousAnswers: any = {};
  isPreviousDataMode: boolean = false;
  isViewMode: boolean = false;

  constructor() {
    console.log('TradeModuleComponent constructor');
  }


  private codeInput(event) {
    let pass = /[4][8-9]{1}/.test(event.charCode) || /[5][0-7]{1}/.test(event.charCode);
    if (!pass) {
      return false;
    }
  }


  private loadTradeData() {
    const iqviaAccountSkuTasksCollection = new IqviaAccountSkuTasksCollection;
    const iqviaQuestionsCollection = new IqviaQuestionsCollection;
    const iqviaTaskAdjustmentsCollection = new IqviaTaskAdjustmentsCollection;
    const callsCollection = new CallsCollection;

    let date = this.date || moment();
    console.log(this.organizationId, date);

    iqviaAccountSkuTasksCollection.fetchAllForOrganizationByDate(this.organizationId, date)
      .then((records) => {
        this.accountSkuTasks = records;
        console.log(this.accountSkuTasks);
      });

    iqviaQuestionsCollection.fetchAllQuestions()
      .then((records) => {
        this.questions = records;
        console.log(this.questions);
      });

    if (this.callReport) {
      this.isViewMode = true;

      iqviaTaskAdjustmentsCollection.fetchTaskAdjustmentsByCallReport(this.callReport)
        .then((taskAdjustments) => {
          console.log('taskAdjustments', taskAdjustments);

          this.preparePreviousAnswers(taskAdjustments);
        });
    } else {
      callsCollection.getLastCallReportForContactAndOrganization(this.cantactId, this.organizationId)
        .then((lastCallReport) => {
          console.log('last CR', lastCallReport);

          if (lastCallReport) {
            iqviaTaskAdjustmentsCollection.fetchTaskAdjustmentsByCallReport(lastCallReport)
              .then((taskAdjustments) => {
                console.log('last taskAdjustments', taskAdjustments);

                this.preparePreviousAnswers(taskAdjustments);
              });
          }
        })
    }
  };


  ngOnChanges() {
    console.log('TradeModuleComponent ngOnChanges');
    this.loadTradeData();
  };


  getAnswerId(question: IqviaQuestion, accountSkuTask: IqviaAccountSkuTask): string {
    return question.id + '_' + accountSkuTask.sku + '_' + accountSkuTask.task;
  }


  preparePreviousAnswers(taskAdjustments: any = []) {
    taskAdjustments.forEach((taskAdjustment) => {
      let id = taskAdjustment.question + '_' + taskAdjustment.sku + '_' + taskAdjustment.task;
      this.previousAnswers[id] = taskAdjustment.stringRealValue || taskAdjustment.numberRealValue;
    });

  }

  isPreviousAnswerAnswered(question: IqviaQuestion, accountSkuTask: IqviaAccountSkuTask): boolean {
    let answerId = this.getAnswerId(question, accountSkuTask);

    return this.previousAnswers[answerId] && this.previousAnswers[answerId].toString().length > 0
  }

  isAnswered(question: IqviaQuestion, accountSkuTask: IqviaAccountSkuTask): boolean {
    let answerId = this.getAnswerId(question, accountSkuTask);

    return this.answers[answerId] && this.answers[answerId].toString().length > 0
  }


  saveAnswer(question: IqviaQuestion, accountSkuTask: IqviaAccountSkuTask, value: any) {
    let answerId = this.getAnswerId(question, accountSkuTask);

    console.log('saveAnswer', question, accountSkuTask, value);
    if (!value && value !== 0) {
      delete this.answers[answerId];
    }

    console.log(this.answers);

    this.change.emit({
      isAllFieldsFilled: this.isAllFieldsFilled.bind(this),
      saveIqviaTaskAdjustments: this.saveIqviaTaskAdjustments.bind(this)
    });
  }


  public isAllFieldsFilled(): boolean {
    return this.accountSkuTasks.length * this.questions.length === Object.keys(this.answers).length;
  }

  public saveIqviaTaskAdjustments(callReport) {
    const iqviaTaskAdjustmentsCollection = new IqviaTaskAdjustmentsCollection;

    let iqviaTaskAdjustments = Object.keys(this.answers).map(function(answerId){
      return this.createTaskAdjustment(answerId, callReport, iqviaTaskAdjustmentsCollection);
    }.bind(this));

    return Promise.all(iqviaTaskAdjustments.map((iqviaTaskAdjustment) => {
      return iqviaTaskAdjustmentsCollection.createEntity(iqviaTaskAdjustment);
    }));
  }


  createTaskAdjustment (answerId, callReport, iqviaTaskAdjustmentsCollection) {
    let params = answerId.split('_');
    let questionId = params[0];
    let skuId = params[1];
    let taskId = params[2];
    let value = this.answers[answerId];
    let question = this.questions.filter((question) => {return question.id === questionId})[0];
    let iqviaTaskAdjustment = new iqviaTaskAdjustmentsCollection.model({});

    iqviaTaskAdjustment.callReport = callReport._soupEntryId.toString();
    iqviaTaskAdjustment.organization = this.organizationId;
    iqviaTaskAdjustment.question = questionId;
    iqviaTaskAdjustment.task = taskId;
    if (question.type == 'numeric') {
      iqviaTaskAdjustment.numberRealValue = value;
    }else{
      iqviaTaskAdjustment.stringRealValue = value;
    }
    iqviaTaskAdjustment.sku = skuId;

    console.log(iqviaTaskAdjustment);

    return iqviaTaskAdjustment;
  }

}
