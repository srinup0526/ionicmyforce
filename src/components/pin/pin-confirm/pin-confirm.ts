import {Component, ElementRef} from '@angular/core';
import { LockManager } from './../../../services/common/LockManager';
import { PinManager } from './../../../services/common/PinManager';
import { AlertController, AlertOptions, Alert } from 'ionic-angular';
import { LocalizationManager } from './../../../services/common/localizations/LocalizationManager';

@Component({
  selector: 'pin-confirm',
  templateUrl: 'pin-confirm.html'
})
export class PinConfirmComponent {
  
  static readonly passwordLength: number = 4;
  static readonly APP_LOCK_CSS_CLASS: string = 'lock';
  
  pin: string;
  confirmPin: string;
  message: string = '';
  pinAvailable: string = '';
  
  
  constructor(private element: ElementRef,
              private alertCtrl: AlertController,
              private lockManager: LockManager,
              private pinManager: PinManager,
              private localizationManager: LocalizationManager
  ) {
    this.hide();
  }
  
  public onTapSubmitButton() {
    this.validatePin();
  }
  
  public hide() {
    this.removeClassFromApp();
    this.element.nativeElement.classList.add('hide');
  }
  
  public show() {
    this.setClassToApp();
    this.clearPinField();
    this.element.nativeElement.classList.remove('hide');
  }
  
  private validatePin() {
    if(this.pin != this.confirmPin) {
      return this.showPinNotEqualAlert();
    }
    
    if(!this.isValidPinLength()) {
      return;
    }
    
    if(this.pin == this.confirmPin) {
      this.pinManager.setPin(this.pin);
      this.hide();
      return this.lockManager.unlock();
    }
  
    return this.showPinNotEqualAlert();
  }
  
  private isValidPinLength(): boolean {
    if(!this.isPin() || this.pin.length < PinConfirmComponent.passwordLength) {
      this.showSmallLengthAlert();
      return false;
    }
    return true;
  }
  
  private isPin(): boolean {
    return !!this.pin && !!this.confirmPin;
  }
  
  private async showSmallLengthAlert(): Promise<void> {
    const title: string = await this.localizationManager.promiseLocale('pin.AlertCaption');
    const message: string = await this.localizationManager.promiseLocale('pin.ErrorMessage.PinLength', {
      count: PinConfirmComponent.passwordLength
    });
    
    return this.showAlert(title, message);
  }
  
  private async showPinNotEqualAlert(): Promise<void> {
    const title: string = await this.localizationManager.promiseLocale('pin.AlertCaption');
    const message: string = await this.localizationManager.promiseLocale('pin.ErrorMessage.PinsAreNotEqual');
    
    return this.showAlert(title, message);
  }
  
  private clearPinField(): void {
    this.pin = '';
    this.confirmPin = '';
  }
  
  private async showAlert(title: string, message: string): Promise<any> {
    const okLabel: string = await this.localizationManager.promiseLocale('common.buttons.OkBtn');
    
    return new Promise(async (resolve) => {
      const options: AlertOptions = {
        title: title,
        message: message,
        cssClass: 'popup alert single-button pin-alert',
        buttons: [
          {
            text: okLabel,
            role: 'cancel',
            handler: () => {
              resolve();
            }
          }
        ]
      };
      
      const alert: Alert = await this.alertCtrl.create(options);
      
      return alert.present();
    });
  }
  
  private setClassToApp(): void {
    const root = document.querySelector('.app-root');
    
    root.classList.add(PinConfirmComponent.APP_LOCK_CSS_CLASS);
  }
  
  private removeClassFromApp(): void {
    const root = document.querySelector('.app-root');
    
    root.classList.remove(PinConfirmComponent.APP_LOCK_CSS_CLASS);
  }

}
