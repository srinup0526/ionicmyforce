import {Component, ElementRef} from '@angular/core';
import {LockManager} from './../../../services/common/LockManager';
import {PinManager} from './../../../services/common/PinManager';
import {Alert, AlertController, AlertOptions, Events} from 'ionic-angular';
import { LocalizationManager } from './../../../services/common/localizations/LocalizationManager';

declare var cordova: any;

@Component({
  selector: 'pin',
  templateUrl: 'pin.html'
})
export class PinComponent {
  
  static readonly APP_LOCK_CSS_CLASS: string = 'lock';
  
  public pin: string;
  private allAttempts: string;
  private remainingAttempts;
  
  
  constructor(private element: ElementRef,
              private alertCtrl: AlertController,
              private lockManager: LockManager,
              private pinManager: PinManager,
              private events: Events,
              private localizationManager: LocalizationManager
  ) {
    this.allAttempts = '3';
    this.hide();

    this.initPinDataThenDBLoaded();
  }
  
  public onTapSubmitButton() {
    this.validatePin();
  }
  
  public hide() {
    this.removeClassFromApp();
    this.element.nativeElement.classList.add('hide');
  }
  
  public show() {
    this.setClassToApp();
    this.clearPinField();
    this.setAttemps()
      .then(() => {
        this.element.nativeElement.classList.remove('hide');
      })
  }
  
  private validatePin() {
    this.pinManager.isPinMatch(this.pin)
      .then((isMatch) => {
        
        if (isMatch) {
          this.resetPinAttemptsAndUnlock();
        } else {
          
          this.reduceAttempts();
          
          if(this.remainingAttempts){
            this.showIncorrectPinAlert();
          } else {
            this.showWillBeRessetPinAlert()
              .then(() => {
                this.clearPinField();
                this.pinManager.removePin()
                  .then(() => {
                    return this.logout();
                  })
              })
          }
        }
      });
  }
  
  private setAttemps(): Promise<any> {
    return this.pinManager.getPinAttempts()
      .then((remainingAttempts) => {
        this.remainingAttempts = remainingAttempts;
        
        if(!this.remainingAttempts) {
          return this.resetPinAttempts();
        }
      })
  }
  
  private reduceAttempts(){
    this.remainingAttempts--;
    this.pinManager.setPinAttempts(this.remainingAttempts);
  }
  
  private resetPinAttemptsAndUnlock() {
    this.resetPinAttempts();
    this.clearPinField();
    this.lockManager.unlock();
  }
  
  private resetPinAttempts() {
    this.pinManager.setPinAttempts(this.allAttempts);
  }
  
  private clearPinField(): void {
    this.pin = '';
    this.remainingAttempts = 0;
  }
  
  private async showIncorrectPinAlert(): Promise<void> {
    const title: string = await this.localizationManager.promiseLocale('pin.AlertCaption');
    const message: string = await this.localizationManager.promiseLocale('pin.ErrorMessage.IncorrectPin', {
      count: this.remainingAttempts
    });
    
    return this.showAlert(title, message);
  }
  
  private async showWillBeRessetPinAlert(): Promise<void> {
    const title: string = await this.localizationManager.promiseLocale('pin.AlertCaption');
    const message: string = await this.localizationManager.promiseLocale('pin.ErrorMessage.PinWillBeReset');
    
    return this.showAlert(title, message);
  }
  
  private async showAlert(title: string, message: string): Promise<any> {
    const okLabel: string = await this.localizationManager.promiseLocale('common.buttons.OkBtn');
    
    return new Promise(async (resolve) => {
      
      const options: AlertOptions = {
        title: title,
        message: message,
        cssClass: 'popup alert single-button pin-alert',
        buttons: [
          {
            text: okLabel,
            role: 'cancel',
            handler: () => {
              resolve();
            }
          }
        ]
      };
      
      const alert: Alert = await this.alertCtrl.create(options);
      
      return alert.present();
    });
  }
  
  private logout() {
    const sfOAuthPlugin = cordova.require("com.salesforce.plugin.oauth");
    
    sfOAuthPlugin.logout();
  }
  
  private setClassToApp(): void {
    const root = document.querySelector('.app-root');
    
    root.classList.add(PinComponent.APP_LOCK_CSS_CLASS);
  }
  
  private removeClassFromApp(): void {
    const root = document.querySelector('.app-root');
  
    root.classList.remove(PinComponent.APP_LOCK_CSS_CLASS);
  }
  
  
  private initPinDataThenDBLoaded() {
    this.events.subscribe('common:dataBaseReady', () => {
      this.setAttemps();
    });
  }
}
