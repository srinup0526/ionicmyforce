import {Component, ElementRef, Input} from '@angular/core';
import {Events, Nav} from 'ionic-angular';
import { HelpdeskPage } from '../../pages/helpdesk/helpdesk';
import { SelectLanguagePopup } from './../popups/SelectLanguagePopup';
import { LockManager } from './../../services/common/LockManager';
import {DynamicAgendaPage} from "../../pages/dynamic-agenda/dynamic-agenda";

declare var cordova;

const enum MENU_STATUS {
  OPEN = 'open'
}

@Component({
  selector: 'bottom-menu',
  templateUrl: 'bottom-menu.html'
})
export class BottomMenuComponent {
  public readonly PAGES = {
    HELPDESK: HelpdeskPage,
    DYNAMIC_AGENDA: DynamicAgendaPage
  };

  @Input()
  nav: Nav;

  private _isOpened: boolean;

  constructor(private elementRef: ElementRef,
              private events: Events,
              private lockManager: LockManager,
              private selectLanguagePopup: SelectLanguagePopup) {
    this.listenEvents();
  }
  
  ngOnInit() {
    this.listenWillLeave();
  }

  public onTapOpenLanguagePopupHandler(): void {
    this.close();
    this.openLanguagePopup();
  }

  public onCloseBottomMenuHandler(): void {
    this.close();
  }
  
  public onTapLogoutBtnHandler(): void {
    this.logout();
  }

  public onTapLockBtnPopupHandler(): void {
    this.close();
    this.pinLock();
  }
  
  public onToggleBottomMenuHandler(): void {
    // add setTimeout because conflicting with outside click event
    // and sometimes toggle event handles before outside click event
    setTimeout(() => this.toggle(), 100);
  }

  public onTapGotoBtnHandler(page): void {
    this.gotoPage(page);
  }
  
  public onTapOutsideHandler(): void {
    if (this._isOpened) {
      this.close();
    }
  }

  public toggle(): void {
    this._isOpened = !this._isOpened;
    this.elementRef.nativeElement.classList.toggle(MENU_STATUS.OPEN);
  }

  public close(): void {
    this._isOpened = false;
    this.elementRef.nativeElement.classList.remove(MENU_STATUS.OPEN);
  }

  private openLanguagePopup(): void {
    this.selectLanguagePopup.showPopup();
  }

  private listenEvents(): void {
    this.events.subscribe('bottomMenu:toggle', () => {
      this.onToggleBottomMenuHandler();
    });
    this.events.subscribe('bottomMenu:close', () => {
      this.onCloseBottomMenuHandler();
    });
  }
  
  private gotoPage(page): void {
    this.close();
    this.nav.push(page);
  }
  
  private pinLock(): void {
    this.lockManager.lock();
  }
  

  private logout(): void {
    let  OAuth = cordova.require("com.salesforce.plugin.oauth");
    
    OAuth.logout();
  }
  
  private listenWillLeave() {
    this.nav.viewWillLeave.subscribe(() => {
      this.close();
    });
  }
}
