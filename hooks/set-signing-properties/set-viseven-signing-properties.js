console.log("Start copy release-signing.properties for viseven script");

var path = require('path');
var shelljs;

try {
    shelljs = require('shelljs');

} catch(e) {
    console.log('The node package shelljs is required to use this script. Run \'npm install shelljs@0.7.0\' before running this script.');
    process.exit(1);
}


var visevenPathSigningProperties = path.join('hooks', 'set-signing-properties', 'visevensigning.properties');
var androidPathSigningProperties = path.join('platforms', 'android', 'release-signing.properties');
var visevenPathKeystore = path.join('hooks', 'set-signing-properties', 'viseven.keystore');
var androidPathKeystore = path.join('platforms', 'android', 'viseven.keystore');

shelljs.cp(visevenPathSigningProperties, androidPathSigningProperties);
shelljs.cp(visevenPathKeystore, androidPathKeystore);
