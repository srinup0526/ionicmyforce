//
//  PresentationViewController.m
//  AbbottMobile
//
//  Created by Alexander Voronov on 4/8/14.
//  Copyright (c) 2014 Qap, Inc. All rights reserved.
//

#import "PresentationViewController.h"
#import "PDFViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface PresentationViewController ()

- (void)stylizeUIElements;
- (IBAction)onPresentationDoubleTap:(id)sender;
- (IBAction)onToolbarTap:(id)sender;
- (void)toggleToolbar;
- (IBAction)onCompleteTap:(id)sender;
- (IBAction)onKPICollectionToggle:(id)sender;

@end


@implementation PresentationViewController

@synthesize webView;
@synthesize toolbarMode;
@synthesize delegate;
@synthesize btnComplete;

- (void)viewDidLoad
{
    self.isKPISuspended = NO;
    [self setToggleKPIStateButtonText];
    [self setCompleteCaption];
    [self setSuspendedLabel];
    [super viewDidLoad];
    [self stylizeUIElements];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}
-(BOOL) shouldAutorotate{
    return YES;
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)stylizeUIElements
{
    NSInteger fontSize = [self isIpad] ? 15 : 10;
    self.btnKPIStateCollectionToggle.layer.cornerRadius = 4;
    self.btnKPIStateCollectionToggle.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:fontSize];
    self.btnComplete.layer.cornerRadius = 4;
    self.btnComplete.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:fontSize];
    self.btnContact.layer.cornerRadius = 4;
    self.btnContact.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:fontSize];
}

- (void)setTranslationsObject:(NSDictionary*) translations
{
    self.translations = translations;
    [self setCompleteCaption];
    [self setToggleKPIStateButtonText];
    [self setSuspendedLabel];
}

- (void)setSuspendedLabel
{
    NSString* caption = [self.translations objectForKey:@"SuspendedLabel"] ?: @"Paused";
    [self.suspendedLabel setText:caption];
}

- (void)setCompleteCaption
{
    NSString* completeCaption = [self.translations objectForKey:@"Complete"] ?: @"Complete";
    [self.btnComplete setTitle: completeCaption forState:UIControlStateNormal];
}

- (BOOL)isIpad
{
    return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
}

- (void)loadURL:(NSString *)url
{
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.webView loadRequest:request];
}

- (void)resetPresentationView
{
    self.toolbarMode.hidden = YES;
    [self.webView loadHTMLString:@"<html><head></head><body></body></html>" baseURL:nil];
    [self clearKPI];
}

- (NSString *)getKPI
{
    if(self.isKPISuspended){
        [self.webView resumeKPI];
    }
    [self.webView suspendKPI];
    return [self.webView getKPI];
}

- (void)clearKPI
{
    [self.webView clearKPI];
}

- (void)toggleKpiCollectionState
{
    if(self.isKPISuspended){
        [self.webView resumeKPI];
        self.toolbarMode.hidden = YES;
        self.suspendOverlay.hidden = YES;
    }else{
        [self.webView suspendKPI];
        self.suspendOverlay.hidden = NO;
    }
    self.isKPISuspended = !self.isKPISuspended;
    [self setToggleKPIStateButtonText];
}

-(void)setToggleKPIStateButtonText{
    NSString* caption;
    if(self.isKPISuspended){
        caption = [self.translations objectForKey:@"Resume"] ?: @"Resume";
    }else{
        caption = [self.translations objectForKey:@"Pause"] ?: @"Pause";
    }
    [self.btnKPIStateCollectionToggle setTitle:caption forState:UIControlStateNormal];
}

- (void)resumeKPI
{
    [self.webView resumeKPI];
}

- (IBAction)onPresentationDoubleTap:(id)sender
{
    [self toggleToolbar];
}

- (IBAction)onToolbarTap:(id)sender
{
    [self toggleToolbar];
}

- (void)toggleToolbar
{
    if(!self.isKPISuspended){
        self.toolbarMode.hidden = !self.toolbarMode.hidden;
    }
}

- (IBAction)onCompleteTap:(id)sender
{
    [self.delegate presentationViewControllerOnComplete:self];
}

- (IBAction)onKPICollectionToggle:(id)sender {
    [self toggleKpiCollectionState];
}

- (IBAction)onOpenContact:(id)sender {
    [self.delegate presentationViewControllerOnShowContact:self];
}

#pragma mark - UIWebView delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"URL: %@", request);
    if ([self requestIsForOpeningPdfFromURL:request])
    {
        [self openPdfViewerWithURL:request.URL];
        return NO;
    }
    return YES;
}

- (void)openPdfViewerWithURL:(NSURL *)url
{
    NSString *nibName = [self isIpad] ? @"PDFViewController" : @"PDFViewController_iPhone";
    PDFViewController *pdfViewer = [[PDFViewController alloc] initWithNibName:nibName bundle:nil];
    [pdfViewer setUrlToPDF:url];
    [self presentViewController:pdfViewer animated:YES completion:NULL];
}

- (BOOL)requestIsForOpeningPdfFromURL:(NSURLRequest *)request
{
    return ([request.URL.scheme isEqualToString:@"file"] && [request.URL.lastPathComponent.pathExtension caseInsensitiveCompare:@"pdf"] == NSOrderedSame);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.delegate presentationViewControllerDidFinishLoading:self];
    [self scalePresentationForIPhone];
}

- (void)scalePresentationForIPhone
{
    if (![self isIpad])
    {
        [self.webView scaleForIPhone];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.delegate presentationViewController:self didFailLoadingWithError:error];
}

#pragma mark - UIGestureRecognizer delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark -

- (void)dealloc
{
    self.toolbarMode = nil;
    self.webView = nil;
    self.delegate = nil;
    self.btnComplete = nil;
}

@end
