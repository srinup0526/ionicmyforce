cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-ionic-keyboard.keyboard",
    "file": "plugins/cordova-plugin-ionic-keyboard/www/ios/keyboard.js",
    "pluginId": "cordova-plugin-ionic-keyboard",
    "clobbers": [
      "window.Keyboard"
    ]
  },
  {
    "id": "cordova-plugin-ionic-webview.IonicWebView",
    "file": "plugins/cordova-plugin-ionic-webview/src/www/util.js",
    "pluginId": "cordova-plugin-ionic-webview",
    "clobbers": [
      "Ionic.WebView"
    ]
  },
  {
    "id": "cordova-plugin-ionic-webview.ios-wkwebview-exec",
    "file": "plugins/cordova-plugin-ionic-webview/src/www/ios/ios-wkwebview-exec.js",
    "pluginId": "cordova-plugin-ionic-webview",
    "clobbers": [
      "cordova.exec"
    ]
  },
  {
    "id": "cordova-plugin-splashscreen.SplashScreen",
    "file": "plugins/cordova-plugin-splashscreen/www/splashscreen.js",
    "pluginId": "cordova-plugin-splashscreen",
    "clobbers": [
      "navigator.splashscreen"
    ]
  },
  {
    "id": "cordova-plugin-statusbar.statusbar",
    "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
    "pluginId": "cordova-plugin-statusbar",
    "clobbers": [
      "window.StatusBar"
    ]
  },
  {
    "id": "cordova-plugin-camera.Camera",
    "file": "plugins/cordova-plugin-camera/www/CameraConstants.js",
    "pluginId": "cordova-plugin-camera",
    "clobbers": [
      "Camera"
    ]
  },
  {
    "id": "cordova-plugin-camera.CameraPopoverOptions",
    "file": "plugins/cordova-plugin-camera/www/CameraPopoverOptions.js",
    "pluginId": "cordova-plugin-camera",
    "clobbers": [
      "CameraPopoverOptions"
    ]
  },
  {
    "id": "cordova-plugin-camera.camera",
    "file": "plugins/cordova-plugin-camera/www/Camera.js",
    "pluginId": "cordova-plugin-camera",
    "clobbers": [
      "navigator.camera"
    ]
  },
  {
    "id": "cordova-plugin-camera.CameraPopoverHandle",
    "file": "plugins/cordova-plugin-camera/www/ios/CameraPopoverHandle.js",
    "pluginId": "cordova-plugin-camera",
    "clobbers": [
      "CameraPopoverHandle"
    ]
  },
  {
    "id": "cordova-plugin-email-composer.EmailComposer",
    "file": "plugins/cordova-plugin-email-composer/www/email_composer.js",
    "pluginId": "cordova-plugin-email-composer",
    "clobbers": [
      "cordova.plugins.email",
      "plugin.email"
    ]
  },
  {
    "id": "cordova-plugin-file.DirectoryEntry",
    "file": "plugins/cordova-plugin-file/www/DirectoryEntry.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.DirectoryEntry"
    ]
  },
  {
    "id": "cordova-plugin-file.DirectoryReader",
    "file": "plugins/cordova-plugin-file/www/DirectoryReader.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.DirectoryReader"
    ]
  },
  {
    "id": "cordova-plugin-file.Entry",
    "file": "plugins/cordova-plugin-file/www/Entry.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.Entry"
    ]
  },
  {
    "id": "cordova-plugin-file.File",
    "file": "plugins/cordova-plugin-file/www/File.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.File"
    ]
  },
  {
    "id": "cordova-plugin-file.FileEntry",
    "file": "plugins/cordova-plugin-file/www/FileEntry.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.FileEntry"
    ]
  },
  {
    "id": "cordova-plugin-file.FileError",
    "file": "plugins/cordova-plugin-file/www/FileError.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.FileError"
    ]
  },
  {
    "id": "cordova-plugin-file.FileReader",
    "file": "plugins/cordova-plugin-file/www/FileReader.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.FileReader"
    ]
  },
  {
    "id": "cordova-plugin-file.FileSystem",
    "file": "plugins/cordova-plugin-file/www/FileSystem.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.FileSystem"
    ]
  },
  {
    "id": "cordova-plugin-file.FileUploadOptions",
    "file": "plugins/cordova-plugin-file/www/FileUploadOptions.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.FileUploadOptions"
    ]
  },
  {
    "id": "cordova-plugin-file.FileUploadResult",
    "file": "plugins/cordova-plugin-file/www/FileUploadResult.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.FileUploadResult"
    ]
  },
  {
    "id": "cordova-plugin-file.FileWriter",
    "file": "plugins/cordova-plugin-file/www/FileWriter.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.FileWriter"
    ]
  },
  {
    "id": "cordova-plugin-file.Flags",
    "file": "plugins/cordova-plugin-file/www/Flags.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.Flags"
    ]
  },
  {
    "id": "cordova-plugin-file.LocalFileSystem",
    "file": "plugins/cordova-plugin-file/www/LocalFileSystem.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.LocalFileSystem"
    ],
    "merges": [
      "window"
    ]
  },
  {
    "id": "cordova-plugin-file.Metadata",
    "file": "plugins/cordova-plugin-file/www/Metadata.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.Metadata"
    ]
  },
  {
    "id": "cordova-plugin-file.ProgressEvent",
    "file": "plugins/cordova-plugin-file/www/ProgressEvent.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.ProgressEvent"
    ]
  },
  {
    "id": "cordova-plugin-file.fileSystems",
    "file": "plugins/cordova-plugin-file/www/fileSystems.js",
    "pluginId": "cordova-plugin-file"
  },
  {
    "id": "cordova-plugin-file.requestFileSystem",
    "file": "plugins/cordova-plugin-file/www/requestFileSystem.js",
    "pluginId": "cordova-plugin-file",
    "clobbers": [
      "window.requestFileSystem"
    ]
  },
  {
    "id": "cordova-plugin-file.resolveLocalFileSystemURI",
    "file": "plugins/cordova-plugin-file/www/resolveLocalFileSystemURI.js",
    "pluginId": "cordova-plugin-file",
    "merges": [
      "window"
    ]
  },
  {
    "id": "cordova-plugin-file.isChrome",
    "file": "plugins/cordova-plugin-file/www/browser/isChrome.js",
    "pluginId": "cordova-plugin-file",
    "runs": true
  },
  {
    "id": "cordova-plugin-file.iosFileSystem",
    "file": "plugins/cordova-plugin-file/www/ios/FileSystem.js",
    "pluginId": "cordova-plugin-file",
    "merges": [
      "FileSystem"
    ]
  },
  {
    "id": "cordova-plugin-file.fileSystems-roots",
    "file": "plugins/cordova-plugin-file/www/fileSystems-roots.js",
    "pluginId": "cordova-plugin-file",
    "runs": true
  },
  {
    "id": "cordova-plugin-file.fileSystemPaths",
    "file": "plugins/cordova-plugin-file/www/fileSystemPaths.js",
    "pluginId": "cordova-plugin-file",
    "merges": [
      "cordova"
    ],
    "runs": true
  },
  {
    "id": "cordova-plugin-file-transfer.FileTransferError",
    "file": "plugins/cordova-plugin-file-transfer/www/FileTransferError.js",
    "pluginId": "cordova-plugin-file-transfer",
    "clobbers": [
      "window.FileTransferError"
    ]
  },
  {
    "id": "cordova-plugin-file-transfer.FileTransfer",
    "file": "plugins/cordova-plugin-file-transfer/www/FileTransfer.js",
    "pluginId": "cordova-plugin-file-transfer",
    "clobbers": [
      "window.FileTransfer"
    ]
  },
  {
    "id": "com.qapint.cordova.zip.Zip",
    "file": "plugins/com.qapint.cordova.zip/www/zip.js",
    "pluginId": "com.qapint.cordova.zip",
    "clobbers": [
      "window.Zip"
    ]
  },
  {
    "id": "com.qapint.cordova.pdf-viewer.PdfViewer",
    "file": "plugins/com.qapint.cordova.pdf-viewer/www/pdf-viewer.js",
    "pluginId": "com.qapint.cordova.pdf-viewer",
    "clobbers": [
      "window.PdfViewer"
    ]
  },
  {
    "id": "com.qapint.cordova.attachments-viewer.AttachmentsViewer",
    "file": "plugins/com.qapint.cordova.attachments-viewer/www/attachments-viewer.js",
    "pluginId": "com.qapint.cordova.attachments-viewer",
    "clobbers": [
      "window.AttachmentsViewer"
    ]
  },
  {
    "id": "com.qapint.cordova.presentation-viewer.PresentationViewer",
    "file": "plugins/com.qapint.cordova.presentation-viewer/www/presentation-viewer.js",
    "pluginId": "com.qapint.cordova.presentation-viewer",
    "clobbers": [
      "window.PresentationViewer"
    ]
  },
  {
    "id": "com.salesforce.plugin.oauth",
    "file": "plugins/com.salesforce/www/com.salesforce.plugin.oauth.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.plugin.network",
    "file": "plugins/com.salesforce/www/com.salesforce.plugin.network.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.plugin.sdkinfo",
    "file": "plugins/com.salesforce/www/com.salesforce.plugin.sdkinfo.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.plugin.smartstore",
    "file": "plugins/com.salesforce/www/com.salesforce.plugin.smartstore.js",
    "pluginId": "com.salesforce",
    "clobbers": [
      "navigator.smartstore"
    ]
  },
  {
    "id": "com.salesforce.plugin.smartstore.client",
    "file": "plugins/com.salesforce/www/com.salesforce.plugin.smartstore.client.js",
    "pluginId": "com.salesforce",
    "clobbers": [
      "navigator.smartstoreClient"
    ]
  },
  {
    "id": "com.salesforce.plugin.sfaccountmanager",
    "file": "plugins/com.salesforce/www/com.salesforce.plugin.sfaccountmanager.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.plugin.smartsync",
    "file": "plugins/com.salesforce/www/com.salesforce.plugin.smartsync.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.util.bootstrap",
    "file": "plugins/com.salesforce/www/com.salesforce.util.bootstrap.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.util.event",
    "file": "plugins/com.salesforce/www/com.salesforce.util.event.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.util.exec",
    "file": "plugins/com.salesforce/www/com.salesforce.util.exec.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.util.logger",
    "file": "plugins/com.salesforce/www/com.salesforce.util.logger.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.util.promiser",
    "file": "plugins/com.salesforce/www/com.salesforce.util.promiser.js",
    "pluginId": "com.salesforce"
  },
  {
    "id": "com.salesforce.util.push",
    "file": "plugins/com.salesforce/www/com.salesforce.util.push.js",
    "pluginId": "com.salesforce"
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-device": "2.0.3",
  "cordova-plugin-ionic-keyboard": "2.1.3",
  "cordova-plugin-ionic-webview": "3.1.2",
  "cordova-plugin-splashscreen": "5.0.3",
  "cordova-plugin-statusbar": "2.4.3",
  "cordova-plugin-whitelist": "1.3.4",
  "cordova-plugin-camera": "4.1.0",
  "cordova-plugin-email-composer": "0.8.15",
  "cordova-plugin-compat": "1.2.0",
  "cordova-plugin-file": "4.3.3",
  "cordova-plugin-file-transfer": "1.6.3",
  "com.qapint.cordova.zip": "1.0.1",
  "com.qapint.cordova.pdf-viewer": "1.0.0",
  "com.qapint.cordova.attachments-viewer": "1.0.0",
  "com.qapint.cordova.presentation-viewer": "1.1.0",
  "cordova-plugin-enable-multidex": "0.2.0",
  "com.salesforce": "6.2.0"
};
// BOTTOM OF METADATA
});