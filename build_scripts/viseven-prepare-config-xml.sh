### You should past 1 if you use jenkins for build
### $1 - build number
### $2 - isFeature



##Set current build version

CURRENT_BUILD_NUMBER="1"
if
[ -n $1 ]
then
  CURRENT_BUILD_NUMBER=$1
fi


BUNDLE_ID="com.qapint.myforceionic"
if
[[ "$2" = "true" ]]
then
  BUNDLE_ID="com.qapint.myforceionic.feature"
fi


## Update config file regarding to build configuration

CONFIG_FILE="config.xml"
CURRENT_VERSION=$(grep -o '\([0-9]*\.[0-9]*\.[0-9]*\)' config.xml | head -1)
IOS_BUNDLE_VERSION="$CURRENT_VERSION.$CURRENT_BUILD_NUMBER"
ANDROID_VERSION_CODE="$CURRENT_BUILD_NUMBER"

sed -i -e "s/android-versionCode=\"\([0-9]*\)\"/android-versionCode=\"$ANDROID_VERSION_CODE\"/g" $CONFIG_FILE
sed -i -e "s/ios-CFBundleVersion=\"\([0-9]*\.[0-9]*\.[0-9]*\)\"/ios-CFBundleVersion=\"$IOS_BUNDLE_VERSION\"/g" $CONFIG_FILE

sed -i -e "s/id=\"com.mylan.myforceIONIC\"/id=\"$BUNDLE_ID\"/g" $CONFIG_FILE
sed -i -e "s/<name>IonicLatestSalesforce/<name>MyForce Ionic/g" $CONFIG_FILE

rm -f "config.xml-e"
